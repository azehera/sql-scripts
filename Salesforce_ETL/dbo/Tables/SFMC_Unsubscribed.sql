﻿CREATE TABLE [dbo].[SFMC_Unsubscribed] (
    [Id]               INT             NULL,
    [Client_id]        INT             NULL,
    [emailaddress]     NVARCHAR (2000) NULL,
    [Status]           NVARCHAR (2000) NULL,
    [bu_tsfl]          NVARCHAR (2000) NULL,
    [bu_direct]        NVARCHAR (2000) NULL,
    [unsubscribeddate] DATETIME        NULL,
    [email_join_date]  NVARCHAR (2000) NULL
);

