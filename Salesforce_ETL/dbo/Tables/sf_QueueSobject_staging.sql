﻿CREATE TABLE [dbo].[sf_QueueSobject_staging] (
    [Id]             NVARCHAR (18) NULL,
    [QueueId]        NVARCHAR (18) NULL,
    [SobjectType]    NVARCHAR (40) NULL,
    [CreatedById]    NVARCHAR (18) NULL,
    [SystemModstamp] DATETIME      NULL
);

