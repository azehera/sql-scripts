﻿CREATE TABLE [dbo].[sf_recordtype] (
    [Id]                NVARCHAR (18)  NULL,
    [Name]              NVARCHAR (80)  NULL,
    [DeveloperName]     NVARCHAR (80)  NULL,
    [NamespacePrefix]   NVARCHAR (15)  NULL,
    [Description]       NVARCHAR (255) NULL,
    [BusinessProcessId] NVARCHAR (18)  NULL,
    [SobjectType]       NVARCHAR (40)  NULL,
    [IsActive]          BIT            NULL,
    [CreatedById]       NVARCHAR (18)  NULL,
    [CreatedDate]       DATETIME       NULL,
    [LastModifiedById]  NVARCHAR (18)  NULL,
    [LastModifiedDate]  DATETIME       NULL,
    [SystemModstamp]    DATETIME       NULL
);

