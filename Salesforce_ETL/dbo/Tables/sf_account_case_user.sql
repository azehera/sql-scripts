﻿CREATE TABLE [dbo].[sf_account_case_user] (
    [Id]                                        NVARCHAR (255)  NOT NULL,
    [Client_ID__c]                              NVARCHAR (255)  NULL,
    [IsDeleted]                                 BIT             NULL,
    [CaseNumber]                                NVARCHAR (255)  NULL,
    [ContactId]                                 NVARCHAR (255)  NULL,
    [AccountId]                                 NVARCHAR (255)  NULL,
    [ParentId]                                  NVARCHAR (255)  NULL,
    [SuppliedName]                              NVARCHAR (255)  NULL,
    [SuppliedEmail]                             NVARCHAR (255)  NULL,
    [SuppliedPhone]                             NVARCHAR (255)  NULL,
    [SuppliedCompany]                           NVARCHAR (255)  NULL,
    [Type]                                      NVARCHAR (255)  NULL,
    [RecordTypeId]                              NVARCHAR (255)  NULL,
    [Status]                                    NVARCHAR (255)  NULL,
    [Reason]                                    NVARCHAR (255)  NULL,
    [Origin]                                    NVARCHAR (255)  NULL,
    [Subject]                                   NVARCHAR (255)  NULL,
    [Priority]                                  NVARCHAR (255)  NULL,
    [Description]                               NVARCHAR (MAX)  NULL,
    [IsClosed]                                  BIT             NULL,
    [ClosedDate]                                DATETIME        NULL,
    [IsEscalated]                               BIT             NULL,
    [CurrencyIsoCode]                           NVARCHAR (255)  NULL,
    [OwnerId]                                   NVARCHAR (255)  NULL,
    [CreatedDate]                               DATETIME        NULL,
    [CreatedById]                               NVARCHAR (255)  NULL,
    [LastModifiedDate]                          DATETIME        NULL,
    [LastModifiedById]                          NVARCHAR (255)  NULL,
    [SystemModstamp]                            DATETIME        NULL,
    [ContactPhone]                              NVARCHAR (255)  NULL,
    [ContactMobile]                             NVARCHAR (255)  NULL,
    [ContactEmail]                              NVARCHAR (255)  NULL,
    [ContactFax]                                NVARCHAR (255)  NULL,
    [LastViewedDate]                            DATETIME        NULL,
    [LastReferencedDate]                        DATETIME        NULL,
    [Last_Received_Email__c]                    DATETIME        NULL,
    [Counter__c]                                NUMERIC (18)    NULL,
    [Is_Case_Open__c]                           BIT             NULL,
    [Resolution_SLA_Start_Date_Time__c]         DATETIME        NULL,
    [Resolution_SLA_Target_Business_Hours__c]   NUMERIC (18, 2) NULL,
    [Resolution_SLA_Target_End_Date_Time__c]    DATETIME        NULL,
    [Resolution_SLA_Business_Hours_ID__c]       NVARCHAR (255)  NULL,
    [Resolution_SLA_Met__c]                     NVARCHAR (255)  NULL,
    [Resolution_SLA_Actual_End_Date_Time__c]    DATETIME        NULL,
    [Resolution_SLA_Actual_Business_Hours__c]   NUMERIC (18, 2) NULL,
    [Initial_Queue_Assigned_to_Case__c]         NVARCHAR (255)  NULL,
    [Case_Owned_by_Queue__c]                    NVARCHAR (1300) NULL,
    [of_Cases_Presently_Open__c]                NUMERIC (18)    NULL,
    [of_Cases_Opened_Ever__c]                   NUMERIC (18)    NULL,
    [of_Times_Case_Re_Opened__c]                NUMERIC (16)    NULL,
    [Last_Public_Case_Comment__c]               NVARCHAR (MAX)  NULL,
    [Channel__c]                                NVARCHAR (255)  NULL,
    [Account_Type__c]                           NVARCHAR (1300) NULL,
    [Activation_Date__c]                        DATE            NULL,
    [Best_time_to_contact__c]                   NVARCHAR (255)  NULL,
    [Case_Root_Cause__c]                        NVARCHAR (255)  NULL,
    [Claim_Number__c]                           NVARCHAR (255)  NULL,
    [Supervisor_Approver__c]                    NVARCHAR (255)  NULL,
    [Contact_Preference__c]                     NVARCHAR (MAX)  NULL,
    [Client_Division__c]                        NVARCHAR (MAX)  NULL,
    [Date_of_Ocurrence__c]                      DATE            NULL,
    [Days_Since_Activation__c]                  NUMERIC (18)    NULL,
    [First_contact_with_Client__c]              DATETIME        NULL,
    [I_Agree__c]                                BIT             NULL,
    [Date_Case_Moved_into_Pending__c]           DATETIME        NULL,
    [Item_Name__c]                              NVARCHAR (255)  NULL,
    [Item_SKU__c]                               NVARCHAR (255)  NULL,
    [Lot_Number__c]                             NVARCHAR (255)  NULL,
    [Marital_Status__c]                         NVARCHAR (255)  NULL,
    [Add_to_Recognition_Name__c]                BIT             NULL,
    [Most_recent_contact_with_Client__c]        DATETIME        NULL,
    [Most_Recent_Order_Date__c]                 DATE            NULL,
    [Number_of_Accounts_to_be_Transferred__c]   NUMERIC (18)    NULL,
    [Order_Date__c]                             DATE            NULL,
    [Quantity_Affected__c]                      REAL            NULL,
    [Refund_Amount__c]                          NUMERIC (18, 2) NULL,
    [MRC__c]                                    BIT             NULL,
    [Transfer_to_One_Coach__c]                  BIT             NULL,
    [Transfer_to_Multiple_Coaches__c]           BIT             NULL,
    [Tracking_Number__c]                        NVARCHAR (255)  NULL,
    [SLA_Violation__c]                          BIT             NULL,
    [Return_Tracking__c]                        BIT             NULL,
    [SRA_Return_Order_Number__c]                NVARCHAR (255)  NULL,
    [What_did_you_do_for_the_Client__c]         NVARCHAR (255)  NULL,
    [Reported_By__c]                            NVARCHAR (255)  NULL,
    [Order_Status__c]                           NVARCHAR (255)  NULL,
    [Lead_Status__c]                            NVARCHAR (255)  NULL,
    [Order_Type__c]                             NVARCHAR (255)  NULL,
    [Date_of_Charge__c]                         DATE            NULL,
    [Card_Type__c]                              NVARCHAR (255)  NULL,
    [Last_4_digits_of_Card__c]                  NVARCHAR (255)  NULL,
    [Order_Amount__c]                           NUMERIC (18, 2) NULL,
    [Autoship_Status__c]                        NVARCHAR (255)  NULL,
    [Account_Status__c]                         NVARCHAR (255)  NULL,
    [Intercept_Action__c]                       NVARCHAR (255)  NULL,
    [Due_Date_for_Refund__c]                    DATE            NULL,
    [Payment_Action__c]                         NVARCHAR (255)  NULL,
    [Order_Volume__c]                           NUMERIC (18, 2) NULL,
    [Volume_Actions__c]                         NVARCHAR (255)  NULL,
    [Admin_Actions__c]                          NVARCHAR (255)  NULL,
    [Final_Disposition__c]                      NVARCHAR (255)  NULL,
    [Request_Reason__c]                         NVARCHAR (255)  NULL,
    [Is_the_Account_Found__c]                   BIT             NULL,
    [Is_volume_more_than__c]                    NVARCHAR (255)  NULL,
    [Is_commission_more_than__c]                NVARCHAR (255)  NULL,
    [Will_the_rank_change__c]                   NVARCHAR (255)  NULL,
    [Manager_Approval__c]                       BIT             NULL,
    [Commission_Amount__c]                      NUMERIC (18, 2) NULL,
    [If_Other_Selected_Please_Briefly_List__c]  NVARCHAR (255)  NULL,
    [If_Other_Selected_Please_Briefly_List2__c] NVARCHAR (255)  NULL,
    [Rank_Changed_To__c]                        NVARCHAR (255)  NULL,
    [Sub_Type__c]                               NVARCHAR (255)  NULL,
    [Unit_of_Measure__c]                        NVARCHAR (255)  NULL,
    [Resolution_SLA_Business_Hours_Name__c]     NVARCHAR (255)  NULL,
    [ComplaintType__c]                          NVARCHAR (255)  NULL,
    [Complaint_Origin__c]                       NVARCHAR (255)  NULL,
    [Major_Category__c]                         NVARCHAR (255)  NULL,
    [Adverse_Event_Description__c]              NVARCHAR (MAX)  NULL,
    [Adverse_Event_Status__c]                   NVARCHAR (255)  NULL,
    [Age__c]                                    REAL            NULL,
    [Amount_of_Weight_Lost__c]                  REAL            NULL,
    [Any_known_allergies__c]                    NVARCHAR (255)  NULL,
    [BLS_Flag__c]                               BIT             NULL,
    [Bulk_Transfer_Type__c]                     NVARCHAR (255)  NULL,
    [Business_City__c]                          NVARCHAR (255)  NULL,
    [Business_Email__c]                         NVARCHAR (255)  NULL,
    [Business_Mobile__c]                        NVARCHAR (255)  NULL,
    [Business_State__c]                         NVARCHAR (255)  NULL,
    [Business_Street_Address__c]                NVARCHAR (255)  NULL,
    [Business_Telephone__c]                     NVARCHAR (255)  NULL,
    [Business_Zip_Code__c]                      NVARCHAR (255)  NULL,
    [Case_Enrollment_Issue__c]                  NVARCHAR (255)  NULL,
    [Case_URL__c]                               NVARCHAR (1300) NULL,
    [Change_on_Optavia_Path_to_Achievement__c]  NVARCHAR (255)  NULL,
    [Charge_Assigned_To__c]                     NVARCHAR (255)  NULL,
    [Client_Coach_report_of_severity__c]        NVARCHAR (255)  NULL,
    [Client_Email_Address__c]                   NVARCHAR (255)  NULL,
    [Client_ID_Number__c]                       NVARCHAR (255)  NULL,
    [Client_Phone_Number__c]                    NVARCHAR (255)  NULL,
    [Client_Relationship_Background__c]         NVARCHAR (255)  NULL,
    [Client_Shipping_Address_City__c]           NVARCHAR (255)  NULL,
    [Client_Shipping_Address_Country__c]        NVARCHAR (255)  NULL,
    [Client_Shipping_Address_State_Province__c] NVARCHAR (255)  NULL,
    [Client_Shipping_Address_Street__c]         NVARCHAR (255)  NULL,
    [Client_Shipping_Address_Zip_Postal__c]     NVARCHAR (255)  NULL,
    [Co_Applicant_First_Name__c]                NVARCHAR (255)  NULL,
    [Co_Applicant_Last_Name__c]                 NVARCHAR (255)  NULL,
    [Co_Applicant_Relationship__c]              NVARCHAR (255)  NULL,
    [Current_Sponsor_Coach_ID__c]               NVARCHAR (255)  NULL,
    [Current_Sponsor_Email__c]                  NVARCHAR (255)  NULL,
    [Current_Sponsor_First_Name__c]             NVARCHAR (255)  NULL,
    [Current_Sponsor_Last_Name__c]              NVARCHAR (255)  NULL,
    [Did_symptom_result_in_a_medical_visit__c]  NVARCHAR (255)  NULL,
    [Does_the_Client_Require_Follow_up__c]      NVARCHAR (255)  NULL,
    [Federal_Tax_ID_of_Applicant_Entity__c]     NVARCHAR (255)  NULL,
    [Full_Recognition_Name__c]                  NVARCHAR (255)  NULL,
    [Gender__c]                                 NVARCHAR (255)  NULL,
    [Height_feet__c]                            REAL            NULL,
    [Height_inches__c]                          REAL            NULL,
    [History_of_this_symptom_or_condition__c]   NVARCHAR (255)  NULL,
    [Ingredients__c]                            NVARCHAR (MAX)  NULL,
    [Last_assigned_queue__c]                    NVARCHAR (255)  NULL,
    [List_of_Products_Needed__c]                BIT             NULL,
    [Medifast_OPTAVIA_Supplement__c]            BIT             NULL,
    [NTF_Director_Count__c]                     NUMERIC (18)    NULL,
    [NTF_Director__c]                           BIT             NULL,
    [NTF_Legal_Count__c]                        NUMERIC (18)    NULL,
    [NTF_Legal__c]                              BIT             NULL,
    [NTF_NST_Supervisor_Safety_Nurse__c]        BIT             NULL,
    [NTF_NST_Supv_Safety_Nurse_Count__c]        NUMERIC (18)    NULL,
    [NTF_Other_Count__c]                        NUMERIC (18)    NULL,
    [NTF_Other__c]                              BIT             NULL,
    [NTF_QA_Count__c]                           NUMERIC (18)    NULL,
    [NTF_QA__c]                                 BIT             NULL,
    [NTF_VP_Count__c]                           NUMERIC (18)    NULL,
    [NTF_VP__c]                                 BIT             NULL,
    [Name_of_Business_Entity__c]                NVARCHAR (255)  NULL,
    [Nature_of_Inquiry_Description__c]          NVARCHAR (4000) NULL,
    [Nature_of_Inquiry__c]                      NVARCHAR (MAX)  NULL,
    [New_Coach_First_Name__c]                   NVARCHAR (255)  NULL,
    [New_Coach_ID_Number__c]                    NVARCHAR (255)  NULL,
    [New_Coach_Last_Name__c]                    NVARCHAR (255)  NULL,
    [Optavia_Coach_ID__c]                       NVARCHAR (255)  NULL,
    [Order_Number__c]                           NVARCHAR (255)  NULL,
    [Outcome_Description__c]                    NVARCHAR (4000) NULL,
    [Outcome__c]                                NVARCHAR (MAX)  NULL,
    [Personal_Info_Consent__c]                  BIT             NULL,
    [Primary_Participant_Address__c]            NVARCHAR (255)  NULL,
    [Primary_Participant_Date_of_Birth__c]      DATE            NULL,
    [Primary_Participant_Mobile_Number__c]      NVARCHAR (255)  NULL,
    [Primary_Participant_Name__c]               NVARCHAR (255)  NULL,
    [Primary_Participant_Phone_Number__c]       NVARCHAR (255)  NULL,
    [Primary_Participant_Title__c]              NVARCHAR (255)  NULL,
    [Primary_Symptom__c]                        NVARCHAR (255)  NULL,
    [Product_or_Program_Related__c]             NVARCHAR (255)  NULL,
    [Reason_for_Bulk_Transfer_Request__c]       NVARCHAR (255)  NULL,
    [Requested_By__c]                           NVARCHAR (255)  NULL,
    [Requested_Sponsor_Coach_ID__c]             NVARCHAR (255)  NULL,
    [Requested_Sponsor_Email__c]                NVARCHAR (255)  NULL,
    [Requested_Sponsor_First_Name__c]           NVARCHAR (255)  NULL,
    [Requested_Sponsor_Last_Name__c]            NVARCHAR (255)  NULL,
    [Secondary_Symptom_Category__c]             NVARCHAR (255)  NULL,
    [Secondary_Symptoms__c]                     NVARCHAR (MAX)  NULL,
    [Sponsor_Transfer_within_30_Days__c]        NVARCHAR (255)  NULL,
    [Starting_Weight__c]                        REAL            NULL,
    [Symptoms__c]                               NVARCHAR (MAX)  NULL,
    [Type_of_Entity__c]                         NVARCHAR (255)  NULL,
    [Volume__c]                                 NUMERIC (18)    NULL,
    [What_Products__c]                          NVARCHAR (255)  NULL,
    [What_Program__c]                           NVARCHAR (255)  NULL,
    [When_did_symptoms_start__c]                NVARCHAR (255)  NULL,
    [For_a_New_or_Amended_Business_Entity__c]   NVARCHAR (255)  NULL,
    [Impacted_Systems__c]                       NVARCHAR (MAX)  NULL,
    [Issue_Type__c]                             NVARCHAR (255)  NULL,
    [What_did_you_do_for_the_Coach__c]          NVARCHAR (255)  NULL,
    [How_long_have_they_had_symptom_s__c]       NVARCHAR (255)  NULL,
    [Cases__c]                                  NUMERIC (18)    NULL,
    [Number_of_Times_Transferred__c]            NUMERIC (18)    NULL,
    [Authorized_Approver__c]                    NVARCHAR (255)  NULL,
    [Compensations_Case__c]                     BIT             NULL,
    [Completed_By__c]                           NVARCHAR (255)  NULL,
    [Completed_Date__c]                         DATETIME        NULL,
    [Details_for_Request__c]                    NVARCHAR (MAX)  NULL,
    [General_Case_Description__c]               NVARCHAR (MAX)  NULL,
    [Information_Requested__c]                  NVARCHAR (255)  NULL,
    [Refund_Issue__c]                           NVARCHAR (255)  NULL,
    [Request_Status__c]                         NVARCHAR (255)  NULL,
    [Transaction_Request__c]                    NVARCHAR (255)  NULL,
    [Product__c]                                NVARCHAR (255)  NULL,
    [Authorized_Approver2__c]                   NVARCHAR (255)  NULL,
    [Requested_By2__c]                          NVARCHAR (255)  NULL,
    [Commissions_Adjustment_Needed__c]          BIT             NULL,
    [Contact_Name_support_site__c]              NVARCHAR (255)  NULL,
    [Owner_Active__c]                           BIT             NULL,
    [Do_Not_Refund__c]                          BIT             NULL,
    [vq_Name__c]                                NVARCHAR (255)  NULL,
    [vq_AreYouACaliforniaResident__c]           NVARCHAR (255)  NULL,
    [DoYouHaveAMedifastOrOPTAVIA_Account__c]    NVARCHAR (255)  NULL,
    [IfYesWhatIsTheAccountNumber__c]            NVARCHAR (255)  NULL,
    [IfNoWhatIsYourPhysicalAddress__c]          NVARCHAR (255)  NULL,
    [WhatIsYourEmailAddress__c]                 NVARCHAR (255)  NULL,
    [Whatisyourphonenumber__c]                  NVARCHAR (255)  NULL,
    [Whatareyourequesting__c]                   NVARCHAR (255)  NULL,
    [OwnerName]                                 NVARCHAR (121)  NULL,
    [CreatedByName]                             NVARCHAR (121)  NULL,
    [GrpOwnerName]                              NVARCHAR (40)   NULL,
    [RequestedbyUser]                           NVARCHAR (255)  NULL,
    [ContactMarket]                             NVARCHAR (255)  NULL,
    [AccountMarket]                             NVARCHAR (255)  NULL,
    [AccountName]                               NVARCHAR (255)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_sf_account_case_user_CaseNumber]
    ON [dbo].[sf_account_case_user]([CaseNumber] ASC);

