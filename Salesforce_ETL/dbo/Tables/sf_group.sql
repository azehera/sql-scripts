﻿CREATE TABLE [dbo].[sf_group] (
    [Id]                     NVARCHAR (18)  NULL,
    [Name]                   NVARCHAR (40)  NULL,
    [DeveloperName]          NVARCHAR (80)  NULL,
    [RelatedId]              NVARCHAR (18)  NULL,
    [Type]                   NVARCHAR (40)  NULL,
    [Email]                  NVARCHAR (255) NULL,
    [QueueRoutingConfigId]   NVARCHAR (18)  NULL,
    [OwnerId]                NVARCHAR (18)  NULL,
    [DoesSendEmailToMembers] BIT            NULL,
    [DoesIncludeBosses]      BIT            NULL,
    [CreatedDate]            DATETIME       NULL,
    [CreatedById]            NVARCHAR (18)  NULL,
    [LastModifiedDate]       DATETIME       NULL,
    [LastModifiedById]       NVARCHAR (18)  NULL,
    [SystemModstamp]         DATETIME       NULL
);

