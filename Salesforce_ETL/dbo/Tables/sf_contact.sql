﻿CREATE TABLE [dbo].[sf_contact] (
    [Id]                     NVARCHAR (18)    NULL,
    [IsDeleted]              BIT              NULL,
    [MasterRecordId]         NVARCHAR (18)    NULL,
    [AccountId]              NVARCHAR (18)    NULL,
    [LastName]               NVARCHAR (80)    NULL,
    [FirstName]              NVARCHAR (40)    NULL,
    [Salutation]             NVARCHAR (40)    NULL,
    [MiddleName]             NVARCHAR (40)    NULL,
    [Suffix]                 NVARCHAR (40)    NULL,
    [Name]                   NVARCHAR (121)   NULL,
    [MailingStreet]          NVARCHAR (255)   NULL,
    [MailingCity]            NVARCHAR (40)    NULL,
    [MailingState]           NVARCHAR (80)    NULL,
    [MailingPostalCode]      NVARCHAR (20)    NULL,
    [MailingCountry]         NVARCHAR (80)    NULL,
    [MailingStateCode]       NVARCHAR (10)    NULL,
    [MailingCountryCode]     NVARCHAR (10)    NULL,
    [MailingLatitude]        NUMERIC (18, 15) NULL,
    [MailingLongitude]       NUMERIC (18, 15) NULL,
    [MailingGeocodeAccuracy] NVARCHAR (40)    NULL,
    [Phone]                  NVARCHAR (40)    NULL,
    [Fax]                    NVARCHAR (40)    NULL,
    [MobilePhone]            NVARCHAR (40)    NULL,
    [OtherPhone]             NVARCHAR (40)    NULL,
    [ReportsToId]            NVARCHAR (18)    NULL,
    [Email]                  NVARCHAR (80)    NULL,
    [Title]                  NVARCHAR (128)   NULL,
    [Department]             NVARCHAR (80)    NULL,
    [CurrencyIsoCode]        NVARCHAR (3)     NULL,
    [OwnerId]                NVARCHAR (18)    NULL,
    [CreatedDate]            DATETIME         NULL,
    [CreatedById]            NVARCHAR (18)    NULL,
    [LastModifiedDate]       DATETIME         NULL,
    [LastModifiedById]       NVARCHAR (18)    NULL,
    [SystemModstamp]         DATETIME         NULL,
    [LastActivityDate]       DATE             NULL,
    [LastCURequestDate]      DATETIME         NULL,
    [LastCUUpdateDate]       DATETIME         NULL,
    [LastViewedDate]         DATETIME         NULL,
    [LastReferencedDate]     DATETIME         NULL,
    [EmailBouncedReason]     NVARCHAR (255)   NULL,
    [EmailBouncedDate]       DATETIME         NULL,
    [IsEmailBounced]         BIT              NULL,
    [PhotoUrl]               NVARCHAR (255)   NULL,
    [Jigsaw]                 NVARCHAR (20)    NULL,
    [JigsawContactId]        NVARCHAR (20)    NULL,
    [Client_ID__c]           NVARCHAR (50)    NULL,
    [Contact_Type__c]        NVARCHAR (255)   NULL,
    [BLS_Flag__c]            BIT              NULL,
    [External_ID__c]         NVARCHAR (50)    NULL,
    [Market__c]              NVARCHAR (255)   NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_sf_contact_Id]
    ON [dbo].[sf_contact]([Id] ASC)
    INCLUDE([Client_ID__c]) WITH (FILLFACTOR = 90);

