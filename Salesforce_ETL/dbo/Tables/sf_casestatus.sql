﻿CREATE TABLE [dbo].[sf_casestatus] (
    [Id]               NVARCHAR (18)  NULL,
    [MasterLabel]      NVARCHAR (255) NULL,
    [ApiName]          NVARCHAR (255) NULL,
    [SortOrder]        INT            NULL,
    [IsDefault]        BIT            NULL,
    [IsClosed]         BIT            NULL,
    [CreatedById]      NVARCHAR (18)  NULL,
    [CreatedDate]      DATETIME       NULL,
    [LastModifiedById] NVARCHAR (18)  NULL,
    [LastModifiedDate] DATETIME       NULL,
    [SystemModstamp]   DATETIME       NULL
);

