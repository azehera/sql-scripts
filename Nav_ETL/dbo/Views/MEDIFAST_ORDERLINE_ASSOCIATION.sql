﻿






/*******************************************************************************************************************
Developer: Vick S
Deescription: Create View for MEDIFAST_ORDERLINE_ASSOCIATION ERFC 1027
Modification: Vick S - REF:DEHA-70 added join with reference table.

***********************************************************************************************************************/
CREATE VIEW [dbo].[MEDIFAST_ORDERLINE_ASSOCIATION] AS 

SELECT [MOA_ID]
      ,[MOA_SEQ]
      ,[MOA_ASSOC_SEQ]
      ,[MOA_ORH_ID]
      ,[MOA_ORDER_NBR]
      ,[MOA_ORL_ID]
      ,[MOA_SKU_ID]
      ,[MOA_SKU]
      ,[MOA_ASSOC_SKU]
      ,[MOA_SKU_UOM]
      ,[MOA_ASSOC_SKU_UOM]
      ,[MOA_QTY]
      ,[MOA_ASSOC_QTY]
      ,[MOA_ORDER_TYPE]
      ,[MOA_ASSOC_TYPE]
      ,[MOA_TAX_RATE]
      ,[MOA_TAX_AMT]
      ,[MOA_SALE_PRICE_AMT]
      ,[MOA_EXT_PRICE_AMT]
      ,[MOA_LIST_PRICE_AMT]
      ,[MOA_SYS_TIMESTAMP]
      ,[MOA_REWARD_EARNED_AMT]
      ,[DocumentNo]
  FROM [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION]

