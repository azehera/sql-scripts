﻿

/*******************************************************************************************************************
Developer: Vick S
Deescription:
Modification:03/18/2020  Vick S - REF:DEHA-70 added join with reference table.
Modification:04/28/2020  Vick S - REF:DEHA-129 Replace linked server with local server database
*******************************************************************************************************************/

CREATE VIEW [dbo].[Jason Pharm$Customer] AS 
SELECT 
RGCA.Medifast_customer_id AS [No_],
C.Canonical_Value AS [Blocked],--DEHA-70
RGCA.FirstName+' '+RGCA.LastName AS [Name] 
FROM [MDM_Reltio].dbo.[RELTIO_GROUP_MDM_CUSTOMERS_ALL] RGCA
LEFT JOIN [MDM_RELTIO].[MDM_Reltio].[dbo].[RELTIO_GROUP_MDM_CUSTOMERS_ALL] RGCA2 ON RGCA.Medifast_customer_id=RGCA2.Medifast_customer_id and RGCA2.Sources='Navision'
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C on RGCA2.ERPStatus=C.Source_Value and C.Source='Reltio' and C.Context='Jason Pharm$Customer'--DEHA-70
where RGCA.Sources='Ecomm'
