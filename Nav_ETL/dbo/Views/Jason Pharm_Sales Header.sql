﻿






/*******************************************************************************************************************
Developer: Vick S
Deescription:
Modification: Vick S - REF:DEHA-70 added join with reference table.
Modification:04/28/2020 Vick S - REF:DEHA-208 added ISNULL function ot Shortcut dimension 1 code from Oracle_ETL
to replace NULL with 'CC'

***********************************************************************************************************************/

CREATE VIEW [dbo].[Jason Pharm$Sales Header] AS 
SELECT
	Amount_Authorized AS [Amount Authorized],
	C2.Canonical_Value AS [Document Type],--DEHA-70
	[External_Document_No_] COLLATE DATABASE_DEFAULT AS [External Document No_],
	[Sell_to_Customer_No_] COLLATE DATABASE_DEFAULT AS [Sell-to Customer No_],
	[Document_Date] AS [Document Date],
	Return_Type AS [Return Type],
	Sell_to_Customer_Name COLLATE DATABASE_DEFAULT AS [Sell-to Customer Name],
	Your_Reference COLLATE DATABASE_DEFAULT AS [Your Reference],
	Bill_to_County COLLATE DATABASE_DEFAULT AS [Bill-to County],
	Bill_to_Post_Code COLLATE DATABASE_DEFAULT AS [Bill-to Post Code],
	Coupon_Promotion_Code COLLATE DATABASE_DEFAULT AS [Coupon_Promotion Code],
	C1.Canonical_Value COLLATE DATABASE_DEFAULT AS [Customer Posting Group],--DEHA-70
	EDI_Internal_DocNo_ COLLATE DATABASE_DEFAULT AS [EDI Internal Doc_ No_],
	C.Canonical_Value COLLATE DATABASE_DEFAULT AS [Location Code],--DEHA-70
	[No_] COLLATE DATABASE_DEFAULT AS [No_],
	Order_Date AS [Order Date],
	Order_Time AS [Order Time],
	Posting_Date AS [Posting Date],
	Shipment_Date AS [Shipment Date],
	C3.Canonical_Value COLLATE DATABASE_DEFAULT AS [Shipment Method Code],--DEHA-70
	Ship_to_County COLLATE DATABASE_DEFAULT AS [Ship-to County],
	Ship_to_Post_Code COLLATE DATABASE_DEFAULT AS [Ship-to Post Code],
	ISNULL(C4.Canonical_Value,'CC') COLLATE DATABASE_DEFAULT AS [Shortcut Dimension 1 Code]--DEHA-70,DEHA-208
from ORACLE_ETL.[dbo].[Oracle_ETL_Sales_Header] SH 
left join ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C on SH.Location_Code=C.Source_Value and C.Source='Oracle' and C.Context='Oracle_ETL_Sales_Header'--DEHA-70
left join ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C1 on SH.Customer_Posting_Group=C1.Source_Value and C1.Source='Oracle' and C1.Context='Oracle_ETL_Sales_Header'--DEHA-70
left join ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C2 on SH.Document_Type=C2.Source_Value and C2.Source='Oracle' and C2.Context='Oracle_ETL_Sales_Header' and C2.Reference_Data='Document_Type'--DEHA-70
left join ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C3 on SH.Shipment_Method_Code=C3.Source_Value and C3.Source='Oracle' and C3.Context='Oracle_ETL_Sales_Header'--DEHA-70
left join ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C4 on SH.Shortcut_Dimension_1_Code=C4.Source_Value and C4.Source='Oracle' and C4.Context='Oracle_ETL_Sales_Header'--DEHA-70


