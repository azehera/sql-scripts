﻿






/*******************************************************************************************************************
Developer: Vick S
Deescription:
Modification: Vick S - REF:DEHA-70 added join with reference table.
Modification: 
Notes: In order to match signed value with NAVISION multple following columns with -1
Amount
Line_Amount 
Amount_Including_VAT
Unit_Cost_LCY

***********************************************************************************************************************/
CREATE VIEW [dbo].[Jason Pharm$Sales Cr_Memo Line_v2] AS 

SELECT
C1.Canonical_Value COLLATE DATABASE_DEFAULT AS [Type],--DEHA-70
C2.Canonical_Value COLLATE DATABASE_DEFAULT AS [Item Category Code],--DEHA-70
[Document_No_] COLLATE DATABASE_DEFAULT AS [Document No_],
Line_Discount_Amount AS [Line Discount Amount],
Inv_Discount_Amount AS [Inv_ Discount Amount],
[Quantity],
[Qty_per_Unit_of_Measure] AS [Qty_ per Unit of Measure],
Quantity_Base AS [Quantity (Base)],
[Amount]*-1 AS [Amount],
(Line_Amount)*-1 AS [Line Amount],
(Amount_Including_VAT)*-1 AS [Amount Including VAT],
C3.Canonical_Value COLLATE DATABASE_DEFAULT AS [Tax Group Code],--DEHA-70
(Unit_Cost_LCY)*-1 AS [Unit Cost (LCY)],
C.Canonical_Value COLLATE DATABASE_DEFAULT AS [Location Code],--DEHA-70
[No_] COLLATE DATABASE_DEFAULT AS [No_],
[Description] COLLATE DATABASE_DEFAULT AS [Description],
[Unit_of_Measure] COLLATE DATABASE_DEFAULT AS [Unit of Measure],
[Unit_Price],
[Order_Item_No_] COLLATE DATABASE_DEFAULT AS [Order Item No_]
FROM ORACLE_ETL.[dbo].[Oracle_ETL_Sales_Cr_Memo_Line] SCML
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C ON SCML.Location_Code=C.Source_Value AND C.Source='Oracle' AND C.Context='Oracle_ETL_Sales_Cr_Memo_Line'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C1 ON SCML.Type=C1.Source_Value AND C1.Source='Oracle' AND C1.Context='Oracle_ETL_Sales_Cr_Memo_Line'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C2 ON SCML.Item_Category_Code=C2.Source_Value AND C2.Source='Oracle' AND C2.Context='Oracle_ETL_Sales_Cr_Memo_Line'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C3 ON SCML.Tax_Group_Code=C3.Source_Value AND C3.Source='Oracle' AND C3.Context='Oracle_ETL_Sales_Cr_Memo_Line'--DEHA-70


