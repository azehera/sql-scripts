﻿


/*******************************************************************************************************************
Developer: Vick S
Deescription:
Modification:03/18/2020  Vick S - REF:DEHA-70 added join with reference table.
Modification:04/15/2020 Vick S - REF: add union to include KIT related products for inventory posting group
*******************************************************************************************************************/
CREATE VIEW [dbo].[Jason Pharm$Item] AS 
SELECT
SKU.ItemId AS [No_],
SKU.Name AS [Description],
C.Canonical_Value AS [Blocked],
C1.Canonical_Value AS [Discontinued],
C2.Canonical_Value AS [Inventory Posting Group]
FROM MDM_Reltio.dbo.vw_mdm_sku_master SKU
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C on SKU.IsItemBlocked=C.Source_Value and C.Source='Reltio' and C.Context='Jason Pharm$Item'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C1 on SKU.IsItemdiscontinued=C1.Source_Value and C1.Source='Reltio' and C1.Context='Jason Pharm$Item'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C2 on SKU.InventoryPostingGroup=C2.Source_Value and C2.Source='Reltio' and C2.Context='Jason Pharm$Item'--DEHA-70
UNION
SELECT
[No_] COLLATE DATABASE_DEFAULT AS [No_],
[Description] COLLATE DATABASE_DEFAULT AS [Description],
[Blocked],
[Discontinued],
[Inventory Posting Group] COLLATE DATABASE_DEFAULT AS [Inventory Posting Group]
FROM NAV_ETL_ARCH.dbo.[Jason Pharm$Item]
