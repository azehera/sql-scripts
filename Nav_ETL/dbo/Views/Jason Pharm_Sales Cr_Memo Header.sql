﻿/*******************************************************************************************************************
Developer: Vick S
Deescription:
Modification:03/18/2020 Vick S - REF:DEHA-70 added join with reference table.
Modification:03/20/2020 Vick S - REF:DEHA-71 added date filter for posting date.
Modification:03/20/2020 Vick S - REF:DEHA-147 added Document Date column.
Modification:04/28/2020 Vick S - REF:DEHA-208 added ISNULL function ot Shortcut dimension 1 code from Oracle_ETL
to replace NULL with 'CC'
Modification: 05/04/2020 Micah W - ERFC 1027 replace Union with Union all
Modification:05/05/2020 Vick S - REF:DEHA-233 added field 'ERPsystem' that identifies which ERP system the data is coming from.

***********************************************************************************************************************/
CREATE VIEW [dbo].[Jason Pharm$Sales Cr_Memo Header] AS 
SELECT
[Customer Posting Group] COLLATE DATABASE_DEFAULT AS [Customer Posting Group], 
[No_] COLLATE DATABASE_DEFAULT AS [No_],
[Posting Date],
[Sell-to Address] COLLATE DATABASE_DEFAULT AS [Sell-to Address],
[Sell-to Customer Name] COLLATE DATABASE_DEFAULT AS [Sell-to Customer Name],
[Sell-to Customer No_] COLLATE DATABASE_DEFAULT AS [Sell-to Customer No_],
[Sell-to Post Code] COLLATE DATABASE_DEFAULT AS [Sell-to Post Code],
[Ship-to Country_Region Code] COLLATE DATABASE_DEFAULT AS [Ship-to Country_Region Code],
[Ship-to Name] COLLATE DATABASE_DEFAULT AS [Ship-to Name],
[Shortcut Dimension 1 Code] COLLATE DATABASE_DEFAULT AS [Shortcut Dimension 1 Code],
[External Document No_] COLLATE DATABASE_DEFAULT AS [External Document No_],
[Sell-to Address 2] COLLATE DATABASE_DEFAULT AS [Sell-to Address 2],
[Ship-to Post Code] COLLATE DATABASE_DEFAULT AS [Ship-to Post Code],
[Amount Authorized],
[Posting Description] COLLATE DATABASE_DEFAULT AS [Posting Description],
[Bill-to Customer No_] COLLATE DATABASE_DEFAULT AS [Bill-to Customer No_],
[Escalate Original Order No_] COLLATE DATABASE_DEFAULT AS [Escalate Original Order No_],
[Document Date],--DEHA-147
'NAV' AS 'ERPsystem'--DEHA-233
FROM NAV_ETL_ARCH.dbo.[Jason Pharm$Sales Cr_Memo Header]
WHERE [Posting Date]<'2020-05-01 00:00:00.000'--DEHA-71
UNION ALL
SELECT
C.Canonical_Value COLLATE DATABASE_DEFAULT AS [Customer Posting Group],--DEHA-70
[No_] COLLATE DATABASE_DEFAULT AS [No_],
Posting_Date AS [Posting Date],
[Sell_to_Address] COLLATE DATABASE_DEFAULT AS [Sell-to Address],
[Sell_to_Customer_Name] COLLATE DATABASE_DEFAULT AS [Sell-to Customer Name],
[Sell_to_Customer_No_] COLLATE DATABASE_DEFAULT AS [Sell-to Customer No_],
[Sell_to_Post_Code] COLLATE DATABASE_DEFAULT AS [Sell-to Post Code],
[Ship_to_Country_Region_Code] COLLATE DATABASE_DEFAULT AS [Ship-to Country_Region Code],
[Ship_to_Name] COLLATE DATABASE_DEFAULT AS [Ship-to Name],
ISNULL(C1.Canonical_Value,'CC') COLLATE DATABASE_DEFAULT AS [Shortcut Dimension 1 Code],--DEHA-70,DEHA-208
[External_Document_No_] COLLATE DATABASE_DEFAULT AS [External Document No_],
[Sell_to_Address_Two] COLLATE DATABASE_DEFAULT AS [Sell-to Address 2],
[Ship_to_Post_Code] COLLATE DATABASE_DEFAULT AS [Ship-to Post Code],
Amount_Authorized AS [Amount Authorized],
[Posting_Description] COLLATE DATABASE_DEFAULT AS [Posting Description],
[Bill_to_Customer_No_] COLLATE DATABASE_DEFAULT AS [Bill-to Customer No_],
[Escalate_Original_Order_No_] COLLATE DATABASE_DEFAULT AS [Escalate Original Order No_],
Document_Date AS [Document Date], --DEHA-147
'ORACLE' AS 'ERPsystem'--DEHA-233
FROM ORACLE_ETL.[dbo].[Oracle_ETL_Sales_Cr_Memo_Header] SCMH
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C ON SCMH.Customer_Posting_Group=C.Source_Value AND C.Source='Oracle' AND C.Context='Oracle_ETL_Sales_Cr_Memo_Header'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C1 ON SCMH.Shortcut_Dimension_1_Code=C1.Source_Value AND C1.Source='Oracle' AND C1.Context='Oracle_ETL_Sales_Cr_Memo_Header'--DEHA-70
WHERE [Posting_Date]>='2020-05-01 00:00:00.000'--DEHA-71
