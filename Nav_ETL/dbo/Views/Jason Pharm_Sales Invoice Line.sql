﻿
/*******************************************************************************************************************
Developer: Vick S
Deescription:
Modification: Vick S - REF:DEHA-70 added join with reference table.
Modification: Vick S - REF:DEHA-71 added posting date filter.
Modification: 
Notes: In order to match signed (+/-) with NAVISION multple following columns with -1
Line_Discount_Amount
Inv_Discount_Amount
Modification: 05/04/2020 Micah W - ERFC 1027 replace Union with Union all
Modification:05/05/2020 Vick S - REF:DEHA-233 added field 'ERPsystem' that identifies which ERP system the data is coming from.
***********************************************************************************************************************/
CREATE VIEW [dbo].[Jason Pharm$Sales Invoice Line] AS 
SELECT
CAST([Type] AS NVARCHAR(10)) COLLATE DATABASE_DEFAULT AS [Type],
[Item Category Code] COLLATE DATABASE_DEFAULT AS [Item Category Code],
[Document No_] COLLATE DATABASE_DEFAULT AS [Document No_],
[Line Discount Amount],
[Inv_ Discount Amount] ,
[Quantity],
[Qty_ per Unit of Measure],
[Quantity (Base)],
[Amount],
[Line Amount],
[Amount Including VAT],
[Tax Group Code] COLLATE DATABASE_DEFAULT AS [Tax Group Code],
[Unit Cost (LCY)],
[Location Code] COLLATE DATABASE_DEFAULT AS [Location Code],
[No_] COLLATE DATABASE_DEFAULT AS [No_],
[Order Item No_] COLLATE DATABASE_DEFAULT AS [Order Item No_],
[Description] COLLATE DATABASE_DEFAULT AS [Description],
[Unit of Measure] COLLATE DATABASE_DEFAULT AS [Unit of Measure],
[Unit Price],
[Shortcut Dimension 2 Code] COLLATE DATABASE_DEFAULT AS [Shortcut Dimension 2 Code],
[Posting Date],
[Shipment Date],
[Sell-to Customer No_] COLLATE DATABASE_DEFAULT AS [Sell-to Customer No_],
'NAV' AS 'ERPsystem'--DEHA-233
FROM NAV_ETL_ARCH.dbo.[Jason Pharm$Sales Invoice Line]
WHERE [Posting Date]<'2020-05-01 00:00:00.000'--DEHA-71
UNION ALL
SELECT
C2.Canonical_Value COLLATE DATABASE_DEFAULT AS [Type],--DEHA-70
C1.Canonical_Value COLLATE DATABASE_DEFAULT AS [Item Category Code],--DEHA-70
Document_No_ COLLATE DATABASE_DEFAULT AS [Document No_],
(Line_Discount_Amount) *-1  AS [Line Discount Amount],
(Inv_Discount_Amount)*-1 AS [Inv_Discount Amount],
[Quantity],
[Qty_per_Unit_of_Measure] AS [Qty_ per Unit of Measure],
Quantity_Base AS [Quantity (Base)],
[Amount],
Line_Amount AS [Line Amount],
Amount_Including_VAT AS [Amount Including VAT],
C4.Canonical_Value COLLATE DATABASE_DEFAULT AS [Tax Group Code],--DEHA-70
Unit_Cost_LCY AS [Unit Cost (LCY)],
c.Canonical_Value COLLATE DATABASE_DEFAULT AS [Location Code],--DEHA-70
[No_] COLLATE DATABASE_DEFAULT AS [No_],
[Order_Item_No_] COLLATE DATABASE_DEFAULT AS [Order Item No_],
[Description] COLLATE DATABASE_DEFAULT AS [Description],
Unit_of_Measure COLLATE DATABASE_DEFAULT AS [Unit of Measure],
Unit_Price AS [Unit Price],
C3.Canonical_Value COLLATE DATABASE_DEFAULT AS [Shortcut Dimension 2 Code],--DEHA-70
[Posting_Date],
[Shipment_Date],
[Sell_to_Customer_No_] COLLATE DATABASE_DEFAULT AS [Sell-to Customer No_],
'ORACLE' AS 'ERPsystem'--DEHA-233
FROM ORACLE_ETL.dbo.[Oracle_ETL_Sales_Invoice_Line] SIL
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C ON SIL.Location_Code=C.Source_Value AND C.Source='Oracle' AND C.Context='Oracle_ETL_Sales_Invoice_Line'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C1 ON SIL.Item_Category_Code=C1.Source_Value AND C1.Source='Oracle' AND C1.Context='Oracle_ETL_Sales_Invoice_Line'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C2 ON SIL.Type=C2.Source_Value AND C2.Source='Oracle' AND C2.Context='Oracle_ETL_Sales_Invoice_Line'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C3 ON SIL.Shortcut_Dimension_2_Code=C3.Source_Value AND C3.Source='Oracle' AND C3.Context='Oracle_ETL_Sales_Invoice_Line'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C4 ON SIL.Tax_Group_Code=C4.Source_Value AND C4.Source='Oracle' AND C4.Context='Oracle_ETL_Sales_Invoice_Line'--DEHA-70
WHERE SIL.Posting_Date>='2020-05-01 00:00:00.000'--DEHA-71
