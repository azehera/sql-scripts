﻿


/*******************************************************************************************************************
Developer: Vick S
Deescription:
Modification: Vick S - REF:DEHA-70 added join with reference table.
Enhancement : To support Recon report 
Date : 04/25/2020 
Add 3 more fields to this view 
	WMS_INTERFACE_FLAG
	RELEASED_STATUS
	INVENTORY_RESERVATION

***********************************************************************************************************************/

CREATE VIEW [dbo].[Jason Pharm$Sales Line] AS 
SELECT
	Document_No_ COLLATE DATABASE_DEFAULT AS [Document No_],
	[Quantity_Base] AS [Quantity (Base)],
	[No_] COLLATE DATABASE_DEFAULT AS [No_],
	C.Canonical_Value COLLATE DATABASE_DEFAULT AS [Location Code],--DEHA-70
	[Shipment_Date] AS [Shipment Date],
	C1.Canonical_Value COLLATE DATABASE_DEFAULT AS [Shortcut Dimension 2 Code],--DEHA-70
	[Description] COLLATE DATABASE_DEFAULT AS [Description],
	C2.Canonical_Value AS [Document Type],--DEHA-70
	[Return_Qty_Received] AS [Return Qty_ Received],
	[Qty_to_Invoice] AS [Qty_ to Invoice],
	[Line_Amount] AS [Line Amount],
	[Amount_Including_VAT] AS [Amount Including VAT],
	[Quantity],
	[Qty_per_Unit_of_Measure] AS [Qty_ per Unit of Measure],
	[Amount],
	[Unit_of_Measure_Code] COLLATE DATABASE_DEFAULT AS [Unit of Measure Code],
	SL.WMS_INTERFACE_FLAG,
	SL.RELEASED_STATUS,
	SL.INVENTORY_RESERVATION
from ORACLE_ETL.[dbo].[Oracle_ETL_Sales_Line] SL
left join ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C on SL.Location_Code=C.Source_Value and C.Source='Oracle' and C.Context='Oracle_ETL_Sales_Line'--DEHA-70
left join ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C1 on SL.Shortcut_Dimension_2_Code=C1.Source_Value and C1.Source='Oracle' and C1.Context='Oracle_ETL_Sales_Line'--DEHA-70
left join ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C2 on SL.Document_Type=C2.Source_Value and C2.Source='Oracle' and C2.Context='Oracle_ETL_Sales_Line' and C2.Reference_Data='Document_Type'--DEHA-70
