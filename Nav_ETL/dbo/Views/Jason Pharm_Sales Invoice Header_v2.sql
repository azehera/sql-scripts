﻿









/*******************************************************************************************************************
Developer: Vick S
Deescription:
Modification:03/18/2020  Vick S - REF:DEHA-70 added join with reference table.
Modification:03/20/2020 Vick S - REF:DEHA-71 added date filter for posting date.
Modification:04/28/2020 Vick S - REF:DEHA-208 added ISNULL function ot Shortcut dimension 1 code from Oracle_ETL
to replace NULL with 'CC'


***********************************************************************************************************************/
CREATE VIEW [dbo].[Jason Pharm$Sales Invoice Header_v2] AS 

SELECT
[Coupon_Promotion_Code] COLLATE DATABASE_DEFAULT AS [Coupon_Promotion Code],
C1.Canonical_Value COLLATE DATABASE_DEFAULT AS [Customer Posting Group],--DEHA-70
SIH.[No_] COLLATE DATABASE_DEFAULT AS [No_],
[Posting_Date] AS [Posting Date],
[Sell_to_Address] COLLATE DATABASE_DEFAULT AS [Sell-to Address],
[Sell_to_Customer_Name] COLLATE DATABASE_DEFAULT AS [Sell-to Customer Name],
[Sell_to_Customer_No_] COLLATE DATABASE_DEFAULT AS [Sell-to Customer No_],
[Sell_to_Post_Code] COLLATE DATABASE_DEFAULT AS [Sell-to Post Code],
[Ship_to_Country_Region_Code] COLLATE DATABASE_DEFAULT AS [Ship-to Country_Region Code],
[Ship_to_Name] COLLATE DATABASE_DEFAULT AS [Ship-to Name],
ISNULL(C2.Canonical_Value,'CC') COLLATE DATABASE_DEFAULT AS [Shortcut Dimension 1 Code],
[External_Document_No_] COLLATE DATABASE_DEFAULT AS [External Document No_],
[Order_Date] AS [Order Date],
[Ship_to_Post_Code] COLLATE DATABASE_DEFAULT AS [Ship-to Post Code],
[Sell_to_Address_2] COLLATE DATABASE_DEFAULT AS [Sell-to Address 2],
[Amount_Authorized] AS [Amount Authorized],
C3.Canonical_Value COLLATE DATABASE_DEFAULT AS [Shipment Method Code],
C.Canonical_Value COLLATE DATABASE_DEFAULT AS [Location Code],--DEHA-70
[Ship_to_County] COLLATE DATABASE_DEFAULT AS [Ship-to County],
[Order_No] COLLATE DATABASE_DEFAULT AS [Order No_]
FROM ORACLE_ETL.dbo.[Oracle_ETL_Sales_Invoice_Header] SIH
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C ON SIH.Location_Code=C.Source_Value AND C.Source='Oracle' AND C.Context='Oracle_ETL_Sales_Invoice_Header'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C1 ON SIH.Customer_Posting_Group=C1.Source_Value AND C1.Source='Oracle' AND C1.Context='Oracle_ETL_Sales_Invoice_Header'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C2 ON SIH.Shortcut_Dimension_1_Code=C2.Source_Value AND C2.Source='Oracle' AND C2.Context='Oracle_ETL_Sales_Invoice_Header'--DEHA-70
LEFT JOIN ORACLE_ETL.dbo.Oracle_ETL_Crosswalk C3 ON SIH.Shipment_Method_Code=C3.Source_Value AND C3.Source='Oracle' AND C3.Context='Oracle_ETL_Sales_Invoice_Header'--DEHA-70

WHERE SIH.Posting_Date>='2020-05-01 00:00:00.000'--DEHA-71
