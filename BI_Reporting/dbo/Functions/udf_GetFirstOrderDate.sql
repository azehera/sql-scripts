﻿


-- ==========================================================
-- Author:		Derald Smith
-- Create date: 09/19/2014
-- Description:	Returns first Order date for a customer
--Parameters:	@USA_CUST_NBR NVARCHAR(40)
-- ===========================================================


CREATE function [dbo].[udf_GetFirstOrderDate] (@USA_CUST_NBR NVARCHAR(50))
RETURNS  SMALLDATETIME
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @FirstOrderDate SMALLDATETIME;
	
	
	SET @FirstOrderDate = (SELECT 
		MIN(ORH_CREATE_DT) 
		FROM ECOMM_ETL .dbo.V_ORDER_HEADER OH (NOLOCK)
		JOIN ECOMM_ETL .dbo.USER_ACCOUNT UA ON UA.USA_id = OH.ORH_CREATED_FOR
	WHERE UA.USA_CUST_NBR = @USA_CUST_NBR AND ORH_STATUS_CD NOT IN ('X','R'));
	IF @FirstOrderDate IS NOT null
		BEGIN
			SET @FirstOrderDate = @FirstOrderDate     
		END   
	ELSE
		BEGIN
			SET @FirstOrderDate = NULL
		END		   



	RETURN(@FirstOrderDate);
END;














