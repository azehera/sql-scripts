﻿
-- ==================================================================================
-- Author:		Ron Baldwin
-- Create date: 02/18/2015
-- Description:	Converted from the iCentris Oracle 'get_leg_count' function.
-- ==================================================================================

CREATE FUNCTION [dbo].[udf_get_leg_count] (@p_businesscenter_id integer, @p_commission_period datetime, @p_type VARCHAR(10))
RETURNS integer
AS

BEGIN
Declare @I_return integer

  SELECT @I_return = SUM(
    CASE WHEN @p_type = 'SC' THEN d.is_sc_leg
    ELSE
      CASE WHEN @p_type = 'ED' THEN d.is_ed_leg
      ELSE
          CASE WHEN @p_type = 'FIBC' THEN d.IS_FIBC_LEG ELSE 0 END
      END
    END)
  FROM (SELECT
    CASE WHEN (b2.is_senior_coach_leg = 1 OR b2.is_exec_dir_leg = 1 OR b2.IS_FIBC_LEG = 1) THEN 1 ELSE 0 END is_sc_leg,
    CASE WHEN ( b2.is_exec_dir_leg = 1 OR b2.IS_FIBC_LEG = 1) THEN 1 ELSE 0 END is_ed_leg,
    CASE WHEN ( b2.IS_FIBC_LEG = 1 ) THEN 1 ELSE 0 END IS_FIBC_LEG
    FROM [ODYSSEY_ETL].dbo.ODYSSEY_businesscenter b2
    WHERE b2.parent_bc = @p_businesscenter_id AND b2.commission_period = @p_commission_period) d

return @I_return

END

