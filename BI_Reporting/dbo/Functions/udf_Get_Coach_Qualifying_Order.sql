﻿
/***** This function is invoked by the EORO TSFL incentive program report.
   It returns the health coach ID, order number, order date and order amount sum
   for the orders that push the Health Coach over the 150 PV requirement. *****/

CREATE FUNCTION udf_Get_Coach_Qualifying_Order (@HCNo varchar(20), @StartDate datetime)
RETURNS @CustData TABLE
(
	HCNo varchar(20),
	HCOrderNo varchar(20),
	HCOrderDate datetime,
	HCPVAmount money
)
AS
BEGIN

Declare @EndDate datetime
Declare @TotalPV money
Declare @OrderNum varchar(20)
Declare @OrderDate datetime
Declare @PVAmount money
Declare @OrderNo varchar(20)
Declare @HCID varchar(20)

Set @EndDate = DATEADD(m, 1, @StartDate)
Set @TotalPV = 0
Set @OrderNo = ''

DECLARE HealthCoachPV CURSOR FOR

select distinct c.CUSTOMER_NUMBER, o.MASTER_ID, o.ENTRY_DATE, o.TOTAL_VOLUME
from [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c WITH (NOLOCK)
inner join [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER_STATUS cs WITH (NOLOCK) on cs.CUSTOMER_ID = c.CUSTOMER_ID
inner join [ODYSSEY_ETL].[dbo].ODYSSEY_ORDERS o WITH (NOLOCK) on o.CUSTOMER_ID = c.CUSTOMER_ID
where cs.STATUS_ID ='ACTIV' and o.ENTRY_DATE >= @StartDate and c.CUSTOMER_NUMBER = @HCNo order by o.ENTRY_DATE

OPEN HealthCoachPV
FETCH NEXT FROM HealthCoachPV into @HCID, @OrderNum, @OrderDate, @PVAmount
WHILE @@FETCH_STATUS = 0
BEGIN
	Set @TotalPV = @TotalPV + @PVAmount
	IF @TotalPV >= 150 break;
	FETCH NEXT FROM HealthCoachPV into @HCID, @OrderNum, @OrderDate, @PVAmount
END
CLOSE HealthCoachPV
DEALLOCATE HealthCoachPV

BEGIN
	INSERT @CustData
	SELECT @HCNo, @OrderNum, @OrderDate, @TotalPV
END

RETURN

END