﻿
-- ==================================================================================
-- Author:		Ron Baldwin
-- Create date: 01/13/2015
-- Description:	Determines if an ACCOUNT row is a reward.  Used to compensate for 
--				missing ACT_NOTES string 'Rewards earned on order:
-- ==================================================================================

CREATE function [dbo].[udf_isRewardAccount] (@ActId bigint)
RETURNS CHAR(1)
AS

BEGIN

	DECLARE @isReward CHAR(1)	-- Return Variable
	DECLARE @ReturnAmount money
	DECLARE @UnusedAmount money

	SET @isReward = 'N' -- Default to False

	SELECT @ReturnAmount = CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END
	from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) where ACA_ACT_ID = @ActId and ISNULL(ACA_NOTES, '') LIKE 'Rtn Order:%'
	
	SELECT @UnusedAmount = CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END
	from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) where ACA_ACT_ID = @ActId and 
	(ISNULL(ACA_NOTES, '') = 'Unused rewards' OR ISNULL(ACA_NOTES, '') = 'CC_TXT_UNUSED_REWARDS')

	-- Return the result of the function
	IF ISNULL(@ReturnAmount, 0) <> 0 or ISNULL(@UnusedAmount, 0) <> 0
		BEGIN
			SET @isReward = 'Y'
		END
	
	RETURN @isReward

END












