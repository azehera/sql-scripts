﻿
-- ==================================================================================
-- Author:		Ron Baldwin
-- Create date: 02/18/2015
-- Description:	Converted from the iCentris Oracle 'get_order_entities' function.
-- ==================================================================================

CREATE FUNCTION [dbo].[udf_get_order_entities] (@p_customer_id integer, @p_commission_period datetime)
RETURNS integer
AS

BEGIN
Declare @v_order_entities integer

    select @v_order_entities = count(distinct(b.BUSINESSCENTER_ID))
    from [ODYSSEY_ETL].dbo.ODYSSEY_BUSINESSCENTER b WITH (NOLOCK), [ODYSSEY_ETL].dbo.ODYSSEY_volume v WITH (NOLOCK)
    where (v.VOLUME_TYPE = 'GV' or v.VOLUME_TYPE = 'PV') and v.VOLUME_VALUE > 0
    and v.NODE_ID = b.BUSINESSCENTER_ID
    and v.PERIOD_END_DATE = b.COMMISSION_PERIOD
    and b.COMMISSION_PERIOD = @p_commission_period
    and b.PARENT_BC = @p_customer_id

return @v_order_entities;

END
