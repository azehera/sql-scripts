﻿/***************************************************************************
Object: usp_Get_Coach_Personal_Metrics
Date: 6/2/2015
Developer(s): M. Haile
Description: Used to check wheather a coach maintains its Rank.
=============================================================================
MODIFICATIONS:
=============================================================================


******************************************************************************/
CREATE FUNCTION [dbo].[udf_Get_Coach_Rank_History]
    (
      -- Add the parameters for the function here
     @CustNum NVARCHAR(255)
	,@AdvanceDate Datetime
    )
RETURNS INT
    WITH EXECUTE AS CALLER
AS
BEGIN
	Declare @CommPeriod Date   
	Declare  @CurrentRankID int, @StartingRankID int
	Declare  @CustID nvarchar(255) = (Select customer_ID from [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] where CUSTOMER_NUMBER = @CustNum)

	Declare @Count int = 1
	Declare @MaintainedRank int = 0

	Set @CommPeriod = (Select  convert (date,DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())- @Count, -1)))

	Set @CurrentRankID = (Select [Rank_ID] 
							from [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
							Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] R ON R.RANK_NAME = BC.[RANK] 
							Where CUSTOMER_ID = @CustID and Cast(COMMISSION_PERIOD as DATE) = Cast(@CommPeriod as Date) )

	Set @StartingRankID =  (Select [Rank_ID] 
							from [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
							Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] R ON R.RANK_NAME = BC.[RANK] 
							Where CUSTOMER_ID = @CustID and Cast(COMMISSION_PERIOD as DATE) = Cast(@AdvanceDate as Date) )
	 
	 IF (@StartingRankID  < @CurrentRankID)
			Begin
				Set @MaintainedRank =  1
			End

	--Declare @RankDuration Table (CurrentRank nvarchar(255) null,[Rank Duration] nvarchar(255) null,[Rank Duration in 12 Months] nvarchar(255) null)
	WHILE @StartingRankID  <= @CurrentRankID and @MaintainedRank = 1 and Cast(@AdvanceDate as Date) < Cast(@CommPeriod as Date) 
	BEGIN
		Set @Count = @COUNT + 1
		Set @CommPeriod = (Select  convert (date,DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())- @Count, -1)))
		Set @CurrentRankID = (Select [Rank_ID] 
							from [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
							Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] R ON R.RANK_NAME = BC.[RANK] 
							Where CUSTOMER_ID = @CustID and Cast(COMMISSION_PERIOD as DATE) = Cast(@CommPeriod as Date) )
	    
		IF (@StartingRankID  < @CurrentRankID and @CurrentRankID > = 3 )
			Begin
				Set @MaintainedRank = 1
			
			End
	END
--REturn the maintained rank Status
  Return(@MaintainedRank)

END
