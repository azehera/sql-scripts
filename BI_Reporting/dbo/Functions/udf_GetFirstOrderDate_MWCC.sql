﻿


-- ==========================================================
-- Author:		Derald Smith
-- Create date: 09/22/2014
-- Description:	Returns first Order date for an MWCC customer
--Parameters:	@Customer_ID NVARCHAR(40)
-- ===========================================================


Create FUNCTION [dbo].[udf_GetFirstOrderDate_MWCC] (@Customer_ID nvarchar(50))
RETURNS  SMALLDATETIME
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @FirstOrderDate SMALLDATETIME;
	
	
	SET @FirstOrderDate = (SELECT 
		MIN(transaction_date) 
		FROM Book4Time_ETL.dbo.B4T_transaction_log_header OH (NOLOCK)
		WHERE OH.customer_id = Customer_ID)
	IF @FirstOrderDate IS NOT null
		BEGIN
			SET @FirstOrderDate = @FirstOrderDate     
		END   
	ELSE
		BEGIN
			SET @FirstOrderDate = NULL
		END		   



	RETURN(@FirstOrderDate);
END;













