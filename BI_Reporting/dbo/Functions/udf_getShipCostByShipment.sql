﻿





CREATE function [dbo].[udf_getShipCostByShipment] (
	@sid	int
	)
	returns money
	as begin
	
	-- 1. get the orderid for the shipment.  Value is found in Reference2
	-- 2. check the bill_type field.  if set to '3' this is a third party billing record.
		-- 2.a. if type 3, get cost from the aggregate of the elements of the flex13 record in flexdata
		-- 2.b. done.
	-- 3. get all records associated with this orderid.
		-- 3.a. aggregate billing cost of all records.
		-- 3.b. done
	declare @orderid varchar(255)
	declare @billtype varchar(50) 
	declare @retval money

	select @orderid=Reference2, @billtype=bill_type from FLAGSHIP_WMS_ETL.dbo.shipment_header where sid=@sid
	
	if @billtype <> '3'
		select @retval = total_chg from FLAGSHIP_WMS_ETL.dbo.shipment_header where sid=@sid --where Reference2 = @orderid
	else begin
		declare @pkgs varchar(255)
		select @pkgs =keyValue from FLAGSHIP_WMS_ETL.dbo.flexdata where keyName = 'FLEX13' and sid = @sid
		if substring(@pkgs,len(@pkgs)-1,1) = '~'
			select @pkgs = substring(@pkgs,1,len(@pkgs)-1)
		declare @mytable table (value money) 
		insert into @mytable select strval from dbo.ufn_split(@pkgs,'~') where patindex('%.%',strval) > 0
		select @retval = sum(value) from @mytable
	end
	return @retval
		
end






