﻿


-- ==========================================================
-- Author:		Derald Smith
-- Create date: 09/19/2014
-- Description:	Returns Last Order date for a customer
--Parameters:	@USA_CUST_NBR NVARCHAR(40)
-- ===========================================================


CREATE function [dbo].[udf_GetLastOrderDate] (@USA_CUST_NBR NVARCHAR(50))
RETURNS  SMALLDATETIME
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @LastOrderDate SMALLDATETIME;
	
	
	SET @LastOrderDate = (SELECT 
		MAX(ORH_CREATE_DT) 
		FROM ECOMM_ETL .dbo.V_ORDER_HEADER OH (NOLOCK)
		JOIN ECOMM_ETL .dbo.USER_ACCOUNT UA ON UA.USA_id = OH.ORH_CREATED_FOR
	WHERE UA.USA_CUST_NBR = @USA_CUST_NBR AND ORH_STATUS_CD NOT IN ('X','R'));
	
	IF @LastOrderDate IS NOT NULL
		BEGIN
			SET @LastOrderDate = @LastOrderDate     
		END   
	ELSE
		BEGIN
			SET @LastOrderDate = NULL
		END		   



	RETURN(@LastOrderDate);
END;














