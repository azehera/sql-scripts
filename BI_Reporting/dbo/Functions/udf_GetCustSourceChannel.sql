﻿

-- =============================================
-- Author:		Derald Smith
-- Create date: 1/16/2012
-- Description:	Used to the source channel for a customer
-- =============================================
CREATE function [dbo].[udf_GetCustSourceChannel] (@Customer_number nvarchar(40), @Case_id VARCHAR(256))

RETURNS  NVARCHAR(10)

WITH EXECUTE AS CALLER
AS
BEGIN
	-- Declare the return variable here
	DECLARE @CustSourceChannel NVARCHAR(10)
	
    
	-- Add the T-SQL statements to compute the return value here
	SET @CustSourceChannel = (SELECT CASE [ORG_ACCOUNT_NBR] 
										WHEN 'MEDIFAST' THEN 'MED1'
										WHEN 'TSFL' THEN 'TSFL'
										WHEN 'DOCTORS' THEN 'DOCTORS'
										END
										FROM ECOMM_ETL .[dbo].[ORGANIZATION] 
							WHERE [ORG_ID] = (SELECT CT.[CTK_ORG_ID] FROM ECOMM_ETL .dbo.CALL_TRACKING CT 
													WHERE CT.CTK_CREATED_FOR = @Customer_number AND CT.CTK_SOURCE_NBR = @Case_id))
	
	-- Return the result of the function
	RETURN @CustSourceChannel

END










