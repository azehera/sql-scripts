﻿
CREATE FUNCTION [dbo].[udf_handle_nulls] (@Input varchar(20))
RETURNS varchar(20)
AS

BEGIN
Declare @Output varchar(20)

Set @Output = ISNULL(@Input, '')

Return @Output

END


