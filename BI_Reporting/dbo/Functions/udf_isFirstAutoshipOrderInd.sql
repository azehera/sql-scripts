﻿


/* ==========================================================================================
-- Author:		Derald Smith
-- Create date: 12/06/2012
-- Description:	Determines if this order is first Autoship Order
-- Parameters:	@Order_number nvarchar(40)
--*******************************************************************************************
--							MODIFICATIONS
--*******************************************************************************************
-- D. Smith - 3/1/2013 - Modification to change logic per code review. Will take in parameter of 
--						order_num instead of customer_number. WIll use the following logic...
						1. Check to see if the order passed to function is an autoship order
							A. If False then:
								 1. Immediately return 0 (False) for the return value as it could 
									not be the first 
							B. If True then:
								1. check if this order is the first of the autoship orders for 
									this customer based upon the created for customer ID.
									a. If True then: 
										i. Return 1 (True) for the return value.
									b. If False then:
										i. Return 0 (False) for the return value

-- ==========================================================================================*/
CREATE function [dbo].[udf_isFirstAutoshipOrderInd] (@Order_number nvarchar(40))
RETURNS INT
WITH EXECUTE AS CALLER
AS

BEGIN
	-- Declare the return variable here
	DECLARE @isAutoShip INT;
	DECLARE @USA_ID BIGINT
	DECLARE @isFirstAutoshipOrderInd INT
	DECLARE @FirstAutoShipCartID BIGINT
	DECLARE @FirstAutoshipDate SMALLDATETIME  
	DECLARE @Order_date SMALLDATETIME
	DECLARE @CTH_ID BIGINT

	--check that the order is an autoship order:
	SET @isAutoShip = dbo.udf_isAutoShip_Order(@Order_number)

	IF @isAutoShip = 0
		BEGIN
			SET @isFirstAutoshipOrderInd = 0
		END
	ELSE	
		BEGIN
		--Get customer user_id to use later
			SET @USA_ID = (SELECT OH.ORH_CREATED_FOR 
				FROM prdmartini_STORE_repl.dbo.vw_Autoship A (NOLOCK) LEFT JOIN prdmartini_STORE_repl.dbo.V_ORDER_HEADER OH (NOLOCK) ON A.CTH_ID = oh.ORH_CTH_ID	
				WHERE oh.ORH_SOURCE_NBR = @Order_number)
		
			--get the cart id for the order in question
			SET @CTH_ID = (SELECT OH.ORH_CTH_ID FROM prdmartini_STORE_repl.dbo.V_ORDER_HEADER OH WHERE OH.ORH_SOURCE_NBR = @Order_number)
		
			--get the cart id of the first autoship order
			SET @FirstAutoShipCartID = (SELECT CTH_ID FROM prdmartini_STORE_repl.dbo.vw_Autoship (NOLOCK) WHERE CTH_First_Order_Date = (
				SELECT MIN(CTH_First_Order_Date) FROM prdmartini_STORE_repl.dbo.vw_Autoship (NOLOCK) WHERE USA_ID = @USA_ID)
				AND USA_ID = @USA_ID AND CTH_TYPE_CD IN ('VIP', 'BSL'))

			IF @CTH_ID = @FirstAutoShipCartID
				BEGIN
					SET @isFirstAutoshipOrderInd  = 1
				END 
			ELSE IF @CTH_ID <> @FirstAutoShipCartID 
				BEGIN
					SET @isFirstAutoshipOrderInd  = 0
				END 
		END

	-- Return the result of the function
	RETURN @isFirstAutoshipOrderInd
END















