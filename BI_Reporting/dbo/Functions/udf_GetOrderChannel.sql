﻿


-- ==========================================================
-- Author:		Derald Smith
-- Create date: 08/11/2014
-- Description:	Returns the sales Channel for an order given the 
--				order number
-- ===========================================================
CREATE function [dbo].[udf_GetOrderChannel] (@Order_number nvarchar(40))
RETURNS NVARCHAR(50)
WITH EXECUTE AS CALLER
AS

BEGIN

DECLARE @OrgID BIGINT
DECLARE @OrderChannel NVARCHAR(50)

SET @OrgID  =(SELECT ORH_ORG_ID FROM ECOMM_ETL .dbo.ORDER_HEADER WHERE ORH_SOURCE_NBR = @Order_number)

SET @OrderChannel = (SELECT
CASE @OrgID
		WHEN '12105623021813764' THEN 'WHOLESALE'
		WHEN '12105623021813765' THEN 'MEDIFAST'
		WHEN '12105623021813766' THEN 'TSFL'
		WHEN '12105623021813778' THEN 'DIRECT CA'
		WHEN '12105623021813780' THEN 'WHOLESALE CA'
		ELSE ''
	END 
	AS OrderChannel)

RETURN @OrderChannel
	
end

