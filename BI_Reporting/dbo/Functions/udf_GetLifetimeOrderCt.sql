﻿


-- ==========================================================
-- Author:		Derald Smith
-- Create date: 08/11/2014
-- Description:	Returns the lifetime count of orders for a customer
--Parameters:	@USA_ID bigint
-- ===========================================================


CREATE function [dbo].[udf_GetLifetimeOrderCt] (@USA_id bigint)
RETURNS  int
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @LifeTimeOrderCt int;
	
	
	SET @LifeTimeOrderCt = (SELECT 
		COUNT(OH.ORH_ID)
		FROM ECOMM_ETL .dbo.V_ORDER_HEADER OH (NOLOCK)
	WHERE ORH_CREATED_FOR = @USA_id AND ORH_STATUS_CD NOT IN ('X','R'));
		--WHERE ORH_CREATED_FOR = @USA_id) AND ORH_STATUS_CD NOT IN ('X'));
	
	--if order count is less than 1 then make it 1
	IF @LifeTimeOrderCt < 1 
		BEGIN
			SET @LifeTimeOrderCt = 1      
		END      
	RETURN(@LifeTimeOrderCt);
END;














