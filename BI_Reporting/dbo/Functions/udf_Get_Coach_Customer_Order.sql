﻿
/***** This function is invoked by the EORO TSFL incentive program report.
   It returns the customer number, name, order number and order amount
   for the order that pushes the Health Coach over the 250 PV requirement. *****/	   

CREATE FUNCTION udf_Get_Coach_Customer_Order (@HCNo varchar(20), @StartDate datetime)
RETURNS @CustData TABLE
(
	HCNo varchar(20),
	ClientID varchar(20),
	ClientFirst varchar(30),
	ClientLast varchar(30),
	ClientEntry datetime,
	ClientOrderNo varchar(20),
	ClientOrderDate datetime,
	ClientPVAmount money
)
AS
BEGIN

Declare @EndDate datetime
Declare @TotalPV money
Declare @LastCustNo varchar(20)
Declare @CustNo varchar(20)
Declare @FirstName varchar(30)
Declare @LastName varchar(30)
Declare @ClientEntry datetime -- First Order Date
Declare @OrderNum varchar(20)
Declare @OrderDate datetime
Declare @PVAmount money
Declare @OrderNo varchar(20)

Set @EndDate = DATEADD(m, 1, @StartDate)
Set @ClientEntry = NULL
Set @TotalPV = 0
Set @OrderNo = ''

DECLARE CustomerPV CURSOR FOR

select distinct c1.CUSTOMER_NUMBER, c1.FIRST_NAME, c1.LAST_NAME, o.MASTER_ID, DATEADD(hour, 3, o.ENTRY_DATE) [ENTRY_DATE], o.TOTAL_VOLUME
from [ODYSSEY_ETL].[dbo].ODYSSEY_BUSINESSCENTER b1 WITH (NOLOCK) inner join [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c1 WITH (NOLOCK) on b1.CUSTOMER_ID = c1.CUSTOMER_ID
inner join [ODYSSEY_ETL].[dbo].ODYSSEY_BUSINESSCENTER b2 WITH (NOLOCK) on b1.PARENT_BC = b2.BUSINESSCENTER_ID and b1.COMMISSION_PERIOD = b2.COMMISSION_PERIOD
inner join [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c2 WITH (NOLOCK) on b2.CUSTOMER_ID = c2.CUSTOMER_ID 
inner join [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER_STATUS cs WITH (NOLOCK) on cs.CUSTOMER_ID = c1.CUSTOMER_ID
inner join [ODYSSEY_ETL].[dbo].ODYSSEY_ORDERS o WITH (NOLOCK) on o.CUSTOMER_ID = c1.CUSTOMER_ID
where cs.STATUS_ID ='ACTIV' and b1.COMMISSION_PERIOD >= @StartDate and c2.CUSTOMER_NUMBER = @HCNo and DATEADD(hour, 3, o.ENTRY_DATE) >= @StartDate AND 
(select MIN(DATEADD(hour, 3, o.ENTRY_DATE)) from [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c WITH (NOLOCK)
inner join [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER_STATUS cs WITH (NOLOCK) on cs.CUSTOMER_ID = c.CUSTOMER_ID
inner join [ODYSSEY_ETL].[dbo].ODYSSEY_ORDERS o WITH (NOLOCK) on o.CUSTOMER_ID = c.CUSTOMER_ID 
where cs.STATUS_ID ='ACTIV' and c.CUSTOMER_ID = c1.CUSTOMER_ID) > @StartDate
order by c1.CUSTOMER_NUMBER, DATEADD(hour, 3, o.ENTRY_DATE)

OPEN CustomerPV
FETCH NEXT FROM CustomerPV into @CustNo, @FirstName, @LastName, @OrderNum, @OrderDate, @PVAmount
Set @LastCustNo = @CustNo
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @CustNo <> @LastCustNo 
	BEGIN
		Set @TotalPV = 0
		Set @ClientEntry = NULL
	END
	IF @ClientEntry IS NULL Set @ClientEntry = @OrderDate
	Set @TotalPV = @TotalPV + @PVAmount
	IF @TotalPV >= 250 break;
	FETCH NEXT FROM CustomerPV into @CustNo, @FirstName, @LastName, @OrderNum, @OrderDate, @PVAmount
END
CLOSE CustomerPV
DEALLOCATE CustomerPV

BEGIN
	INSERT @CustData
	SELECT @HCNo, @CustNo, @FirstName, @LastName, @ClientEntry, @OrderNum, @OrderDate, @TotalPV
END

RETURN

END