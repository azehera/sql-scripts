﻿


-- ==========================================================
-- Author:		Derald Smith
-- Create date: 08/10/2014
-- Description:	Determines if the order is an autoship or On Demand
-- ===========================================================
CREATE function [dbo].[udf_GetOrderType] (@Order_number nvarchar(40))
RETURNS NVARCHAR(12)
WITH EXECUTE AS CALLER
AS

BEGIN
	-- Declare the return variable here
	DECLARE @isAutoShip INT;
	DECLARE @Cart_Type_CD NVARCHAR(3)
	DECLARE @OrderType NVARCHAR(12)

	-- Add the T-SQL statements to compute the return value here
	set @Cart_Type_CD = (
		SELECT CH.CTH_TYPE_CD 
		FROM [ECOMM_ETL ].dbo.ORDER_HEADER OH LEFT JOIN [ECOMM_ETL ].dbo.CART_HEADER CH
		ON OH.ORH_CTH_ID = CH.CTH_ID 
		WHERE OH.ORH_SOURCE_NBR = @Order_number );

	-- Return the result of the function
	IF @Cart_Type_CD IN ('BSL','VIP')
		begin
			SET @OrderType = 'AutoShip'
		END
	ELSE
		Begin
			SET @OrderType = 'OnDemand'
		End
	
	RETURN @OrderType
END













