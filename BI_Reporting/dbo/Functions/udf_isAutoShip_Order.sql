﻿








-- ==========================================================
-- Author:		Derald Smith
-- Create date: 11/27/2012
-- Description:	Determines if the order is an autoship order
-- ===========================================================
CREATE function [dbo].[udf_isAutoShip_Order] (@Order_number nvarchar(40))
RETURNS INT
WITH EXECUTE AS CALLER
AS

BEGIN
	-- Declare the return variable here
	DECLARE @isAutoShip INT;
	DECLARE @Cart_Type_CD NVARCHAR(3)

	-- Add the T-SQL statements to compute the return value here
	set @Cart_Type_CD = (
		SELECT CH.CTH_TYPE_CD 
		FROM [ECOMM_ETL ].dbo.ORDER_HEADER OH LEFT JOIN [ECOMM_ETL ].dbo.CART_HEADER CH
		ON OH.ORH_CTH_ID = CH.CTH_ID 
		WHERE OH.ORH_SOURCE_NBR = @Order_number );

	-- Return the result of the function
	IF @Cart_Type_CD IN ('BSL','VIP')
		begin
			SET @isAutoShip = 1
		END
	ELSE
		Begin
			SET @isAutoShip = 0
		End
	
	RETURN @isAutoShip
END












