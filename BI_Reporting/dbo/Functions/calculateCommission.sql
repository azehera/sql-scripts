﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================

CREATE FUNCTION [dbo].[calculateCommission]
 (
 @ConsumableSales money

 )
 RETURNS money
 
 AS
 BEGIN


 DECLARE @commission money

 
 IF  @ConsumableSales < 1500
       SET @commission = @ConsumableSales * (0.15)

 IF  @ConsumableSales > = 1500 and @ConsumableSales < 2999
 SET @commission = @ConsumableSales * (0.18)

 IF  @ConsumableSales > = 3000 and @ConsumableSales < 4999
 SET @commission = @ConsumableSales * (0.21)
 
 IF  @ConsumableSales > = 5000 and @ConsumableSales < 7499
 SET @commission = @ConsumableSales * (0.24)
 
 IF  @ConsumableSales > = 7500
 SET @commission = @ConsumableSales * (0.18)
 
 RETURN @commission

 END

