﻿





CREATE function [dbo].[udf_getShipCostByPackage] (
	  @sid int
	, @pid int
	)
	returns @retval table (packageId int, cost money)
	as begin
	
	-- 1. fedex (fdx) can contain multiple records and total cost will be split across the records (call udf_getShipCostByShipment for total)
	-- 2. ups (ups) & usps (usps) can contain multiple records and each record will have a cost associated. -- total_chg
	-- 3. 3rd party billing packages will enumerate properly, but the total_chg will be 0.  pull these values from flexdata
	
	-- get bill type, carrier from shipment_header
	declare @billtype varchar(50)
	declare @carrier varchar(50)
	select @billtype=bill_type, @carrier=carrier from FLAGSHIP_WMS_ETL.dbo.shipment_header where sid = @sid
	
	if @billtype = '3' begin
		declare @pkgs varchar(255)
		select @pkgs = keyValue from FLAGSHIP_WMS_ETL.dbo.flexdata where keyName = 'FLEX13' and sid = @sid
		if substring(@pkgs,len(@pkgs)-1,1) = '~'
			select @pkgs = substring(@pkgs,1,len(@pkgs)-1)
		--find pairs of numbers
		declare @index varchar(255)
		declare @cost varchar(255)
		declare @tmpchar varchar(1)
		declare @tmpstr varchar(255)
		set @tmpstr = ''
		while @pkgs <> '' begin
			set @tmpchar = substring(@pkgs,1,1)
			while @tmpchar<>'~' begin
				set @pkgs = right(@pkgs, len(@pkgs) -1)
				set @tmpstr = @tmpstr + @tmpchar
				set @tmpchar = substring(@pkgs,1,1)
			end
			set @index = @tmpstr
			set @tmpstr = ''
			set @pkgs = right(@pkgs, len(@pkgs) -1)
			set @tmpchar = substring(@pkgs,1,1)
			while @tmpchar<>'~' begin
				set @pkgs = right(@pkgs, len(@pkgs) -1)
				set @tmpstr = @tmpstr + @tmpchar
				set @tmpchar = substring(@pkgs,1,1)
			end
			set @cost = @tmpstr
			set @tmpstr = ''
			set @pkgs = right(@pkgs, len(@pkgs) -1)
			insert into @retval values(@index, @cost)
			delete from @retval where packageId <> @pid
		end			
		return
	end
	if @carrier = 'FDX' begin
		declare @totalcost money
		select @totalcost = dbo.udf_getShipCostByShipment(@sid)
		declare @packagecount int
		select @packagecount = count(1) from FLAGSHIP_WMS_ETL.dbo.packages where sid = @sid
		insert into @retval select pid, @totalcost/@packagecount from FLAGSHIP_WMS_ETL.dbo.packages where sid=@sid
		delete from @retval where packageId <> @pid
		return
	end
	insert into @retval select pid, total_chg from FLAGSHIP_WMS_ETL.dbo.packages where sid = @sid
	delete from @retval where packageId <> @pid
	return
end
		




