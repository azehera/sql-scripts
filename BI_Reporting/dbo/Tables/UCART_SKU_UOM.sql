﻿CREATE TABLE [dbo].[UCART_SKU_UOM] (
    [SKU Code]       NVARCHAR (128) NOT NULL,
    [Attribute Name] NVARCHAR (128) NOT NULL,
    [Description]    NVARCHAR (512) NULL,
    [Value]          VARCHAR (40)   NULL
);

