﻿CREATE TABLE [dbo].[UniqueCustomer] (
    [CustomerPostingGroup] CHAR (15)        NULL,
    [UniqueId]             INT              NULL,
    [Units]                DECIMAL (38, 20) NULL,
    [Amount]               DECIMAL (38, 20) NULL,
    [CalYear]              INT              NULL,
    [CalMonth]             INT              NULL
);

