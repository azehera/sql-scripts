﻿CREATE TABLE [dbo].[CartRepAccomondation] (
    [Customer Posting Group]    VARCHAR (10)     NULL,
    [External Document No_]     VARCHAR (20)     NULL,
    [Document No_]              VARCHAR (20)     NULL,
    [No_]                       VARCHAR (20)     NULL,
    [Description]               VARCHAR (50)     NULL,
    [Shortcut Dimension 1 Code] VARCHAR (20)     NULL,
    [CalYear]                   INT              NULL,
    [CalMonth]                  INT              NULL,
    [CalendarMonthName]         CHAR (15)        NULL,
    [CalendarQuarter]           INT              NULL,
    [Quantity]                  DECIMAL (38, 20) NULL,
    [Unit of Measure]           VARCHAR (10)     NULL,
    [Quantity (Base)]           DECIMAL (38, 20) NULL,
    [Unit Price]                DECIMAL (38, 20) NULL,
    [Discount Amount]           DECIMAL (38, 20) NULL,
    [Standard Cost]             DECIMAL (38, 20) NULL,
    [RetailCost]                DECIMAL (38, 20) NULL,
    [Cost]                      DECIMAL (38, 6)  NULL,
    [NavCategory]               VARCHAR (10)     NULL,
    [CartCategory]              CHAR (30)        NULL
);

