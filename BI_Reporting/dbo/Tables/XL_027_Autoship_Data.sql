﻿CREATE TABLE [dbo].[XL_027_Autoship_Data] (
    [USA_ID]            BIGINT      NOT NULL,
    [CTH_ID]            BIGINT      NOT NULL,
    [CartStatus]        NCHAR (1)   NOT NULL,
    [TypeName]          VARCHAR (8) NOT NULL,
    [CalendarDate]      DATETIME    NULL,
    [CalendarYear]      INT         NOT NULL,
    [CalendarMonthName] CHAR (15)   NULL,
    [CalendarMonth]     INT         NOT NULL,
    [Amount]            MONEY       NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_XL_027_Autoship_Data_CartStatus_TypeName_CalendarDate]
    ON [dbo].[XL_027_Autoship_Data]([CartStatus] ASC, [TypeName] ASC, [CalendarDate] ASC)
    INCLUDE([Amount]) WITH (FILLFACTOR = 90);

