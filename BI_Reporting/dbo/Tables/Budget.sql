﻿CREATE TABLE [dbo].[Budget] (
    [Channel]         VARCHAR (50)     NULL,
    [CalendarMonthID] INT              NULL,
    [MonthlyBudget]   NUMERIC (38, 18) NULL,
    [CalendarQuarter] INT              NULL,
    [MonthlyForecast] NUMERIC (38, 18) NULL
);

