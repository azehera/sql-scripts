﻿CREATE TABLE [dbo].[Active Earning Coaches Annually] (
    [CalendarYear]              VARCHAR (20)    NULL,
    [Active Earners]            NUMERIC (15)    NULL,
    [Average Revenue Per Coach] NUMERIC (15, 2) NULL
);

