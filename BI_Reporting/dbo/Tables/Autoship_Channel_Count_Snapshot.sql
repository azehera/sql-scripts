﻿CREATE TABLE [dbo].[Autoship_Channel_Count_Snapshot] (
    [Snapshot_Date]   DATETIME     NOT NULL,
    [Expectation_Day] DATE         NULL,
    [Channel]         VARCHAR (13) NOT NULL,
    [4 or 7]          VARCHAR (17) NOT NULL,
    [Expected]        INT          NULL,
    [Customer_Type]   VARCHAR (17) NULL
);

