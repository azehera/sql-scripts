﻿CREATE TABLE [dbo].[XL_023_MonthlyForecastView_Backup] (
    [TSFLAuto]                  NUMERIC (15, 2) NULL,
    [MedifastAuto]              NUMERIC (15, 2) NULL,
    [TsflCalcPercent]           NUMERIC (15, 6) NULL,
    [MedifastCalcPercent]       NUMERIC (15, 6) NULL,
    [AutoPercentTSFL]           NUMERIC (15, 6) NULL,
    [AutoPercentMedifast]       NUMERIC (15, 6) NULL,
    [AutoForecast$TSFL]         NUMERIC (15, 2) NULL,
    [AutoForecast$Medifast]     NUMERIC (15, 2) NULL,
    [OnDemandForecast$TSFL]     NUMERIC (15, 2) NULL,
    [OnDemandForecast$Medifast] NUMERIC (15, 2) NULL,
    [TotalForecast$]            NUMERIC (15, 2) NULL,
    [OtherChannel$Forecast]     NUMERIC (15, 2) NULL,
    [Actual$]                   NUMERIC (15, 2) NULL,
    [OpenOrder$]                NUMERIC (15, 2) NULL,
    [OtherChannel$]             NUMERIC (15, 2) NULL,
    [MonthlyForecast$]          NUMERIC (15, 2) NULL,
    [OnDemandNew]               NUMERIC (15, 2) NULL,
    [OtherChannelNew]           NUMERIC (15, 2) NULL
);

