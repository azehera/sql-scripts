﻿CREATE TABLE [dbo].[XL_035_IncomeStatementCenters] (
    [LocationCode]                VARCHAR (30)     NOT NULL,
    [Region]                      VARCHAR (30)     NOT NULL,
    [CalendarYear]                INT              NOT NULL,
    [CalendarMonth]               INT              NOT NULL,
    [CalendarQuarter]             INT              NOT NULL,
    [CalendarMonthName]           CHAR (15)        NULL,
    [CalendarMonthID]             INT              NULL,
    [Total Income]                NUMERIC (38, 18) NULL,
    [Total Cost of Goods Sold]    NUMERIC (38, 18) NULL,
    [Total Direct Costs]          NUMERIC (38, 18) NULL,
    [GrossProfit]                 NUMERIC (38, 18) NULL,
    [Total Operating Costs]       NUMERIC (38, 18) NULL,
    [Total Personnel Expenses]    NUMERIC (38, 18) NULL,
    [Total Sales & Marketing Exp] NUMERIC (38, 18) NULL,
    [Total Communication]         NUMERIC (38, 18) NULL,
    [Total Office Expenses]       NUMERIC (38, 18) NULL,
    [Total Other Expenses]        NUMERIC (38, 18) NULL,
    [Total Operating Income]      NUMERIC (38, 18) NULL
);

