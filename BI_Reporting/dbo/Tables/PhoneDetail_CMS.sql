﻿CREATE TABLE [dbo].[PhoneDetail_CMS] (
    [VDN]             NVARCHAR (10) NOT NULL,
    [CallDate]        DATE          NOT NULL,
    [StartTime]       TIME (7)      NOT NULL,
    [StopTime]        TIME (7)      NOT NULL,
    [VDNDesc]         NVARCHAR (50) NULL,
    [InboundCalls]    INT           NULL,
    [Abandoned]       INT           NULL,
    [AvgAbandonSecs]  INT           NULL,
    [AbandonPercent]  INT           NULL,
    [FlowOutCalls]    INT           NULL,
    [FlowOutPercent]  INT           NULL,
    [AvgVDNSecs]      INT           NULL,
    [ForcedDiscCalls] INT           NULL,
    CONSTRAINT [PK_PhoneDetail_CMS] PRIMARY KEY CLUSTERED ([VDN] ASC, [CallDate] ASC, [StartTime] ASC, [StopTime] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_PhoneDetail_CMS_CallDate]
    ON [dbo].[PhoneDetail_CMS]([CallDate] ASC) WITH (FILLFACTOR = 90);

