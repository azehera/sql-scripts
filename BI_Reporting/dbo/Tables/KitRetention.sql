﻿CREATE TABLE [dbo].[KitRetention] (
    [GINID]                      BIGINT        NULL,
    [ItemCodeSubcategory]        VARCHAR (255) NULL,
    [DocumentNo]                 VARCHAR (255) NULL,
    [Posting Date]               DATETIME      NULL,
    [# of Times Kit Ordered_GIN] INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_GINID]
    ON [dbo].[KitRetention]([GINID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ItemCodeSubcategory]
    ON [dbo].[KitRetention]([ItemCodeSubcategory] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Date]
    ON [dbo].[KitRetention]([Posting Date] ASC) WITH (FILLFACTOR = 90);

