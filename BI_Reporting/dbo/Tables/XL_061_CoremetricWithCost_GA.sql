﻿CREATE TABLE [dbo].[XL_061_CoremetricWithCost_GA] (
    [DayOfWeekName]                      VARCHAR (10)    NOT NULL,
    [CalDate]                            DATETIME        NULL,
    [Visitors]                           NUMERIC (15)    NULL,
    [TotalSales]                         NUMERIC (15, 2) NULL,
    [ConversionRate(%)]                  NUMERIC (15, 4) NULL,
    [TotalOrders]                        NUMERIC (15)    NULL,
    [TotalAov]                           NUMERIC (15)    NULL,
    [TotalFreeShipping]                  NUMERIC (15, 2) NULL,
    [TotalFreeGood]                      NUMERIC (15, 2) NULL,
    [TotalDiscounts]                     NUMERIC (15, 2) NULL,
    [TotalListPriceDeviation]            NUMERIC (15, 2) NULL,
    [Total]                              NUMERIC (15, 2) NULL,
    [TotalMarketingPercent]              DECIMAL (6, 4)  NULL,
    [SalesOnDemand]                      NUMERIC (15, 2) NULL,
    [OrdersOnDemand]                     NUMERIC (15)    NULL,
    [AovOnDemand]                        NUMERIC (15)    NULL,
    [FreeShippingOnDemand]               NUMERIC (15, 2) NULL,
    [FreeGoodOnDemand]                   NUMERIC (15, 2) NULL,
    [DiscountsOnDemand]                  NUMERIC (15, 2) NULL,
    [ListPriceDeviationOndemand]         NUMERIC (15, 2) NULL,
    [OnDemandTotal]                      NUMERIC (15, 2) NULL,
    [MarketingPercentOnDemand(%)]        DECIMAL (6, 4)  NULL,
    [SalesAutoship]                      NUMERIC (15, 2) NULL,
    [OrdersAutoship]                     NUMERIC (15)    NULL,
    [AovAutoship]                        NUMERIC (15, 2) NULL,
    [FreeShippingAutoship]               NUMERIC (15, 2) NULL,
    [FreeGoodAutoship]                   NUMERIC (15, 2) NULL,
    [DiscountsAutoship]                  NUMERIC (15, 2) NULL,
    [ListPriceDeviationAutoShip]         NUMERIC (15, 2) NULL,
    [AutoshipTotal]                      NUMERIC (15, 2) NULL,
    [MarketingPercentAutoship(%)]        DECIMAL (6, 4)  NULL,
    [GrandTotal]                         NUMERIC (15, 2) NULL,
    [MarketingPercent(%)]                DECIMAL (6, 4)  NULL,
    [CalendarMonthName]                  CHAR (15)       NULL,
    [CalendarQuarter]                    INT             NOT NULL,
    [<=150SalesAutoShip]                 NUMERIC (15, 2) NULL,
    [<=150OrdersAutoship]                NUMERIC (15)    NULL,
    [<=150AovAutoship]                   NUMERIC (15, 2) NULL,
    [<=150FreeShippingAutoShip]          NUMERIC (15, 2) NULL,
    [<=150FreeGoodAutoShip]              NUMERIC (15, 2) NULL,
    [<=150DiscountsAutoShip]             NUMERIC (15, 2) NULL,
    [<=150ListPriceDeviationAutoShip]    NUMERIC (15, 2) NULL,
    [<=150AutoShipTotal]                 NUMERIC (15, 2) NULL,
    [<=150MarketingPercentAutoship(%)]   DECIMAL (18, 2) NULL,
    [150-250SalesAutoShip]               NUMERIC (15, 2) NULL,
    [150-250OrdersAutoship]              NUMERIC (15)    NULL,
    [150-250AovAutoship]                 NUMERIC (15, 2) NULL,
    [150-250FreeShippingAutoShip]        NUMERIC (15, 2) NULL,
    [150-250FreeGoodAutoShip]            NUMERIC (15, 2) NULL,
    [150-250DiscountsAutoShip]           NUMERIC (15, 2) NULL,
    [150-250ListPriceDeviationAutoShip]  NUMERIC (15, 2) NULL,
    [150-250AutoShipTotal]               NUMERIC (15, 2) NULL,
    [150-250MarketingPercentAutoship(%)] DECIMAL (6, 5)  NULL,
    [>250SalesAutoShip]                  NUMERIC (15, 2) NULL,
    [>250OrdersAutoship]                 NUMERIC (15)    NULL,
    [>250AovAutoship]                    NUMERIC (15, 2) NULL,
    [>250FreeShippingAutoShip]           NUMERIC (15, 2) NULL,
    [>250FreeGoodAutoShip]               NUMERIC (15, 2) NULL,
    [>250DiscountsAutoShip]              NUMERIC (15, 2) NULL,
    [>250ListPriceDeviationAutoShip]     NUMERIC (15, 2) NULL,
    [>250AutoShipTotal]                  NUMERIC (15, 2) NULL,
    [>250MarketingPercentAutoship(%)]    DECIMAL (6, 4)  NULL
);

