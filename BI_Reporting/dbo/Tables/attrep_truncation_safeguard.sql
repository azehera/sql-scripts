﻿CREATE TABLE [dbo].[attrep_truncation_safeguard] (
    [latchTaskName]    VARCHAR (128) NOT NULL,
    [latchMachineGUID] VARCHAR (40)  NOT NULL,
    [LatchKey]         CHAR (1)      NOT NULL,
    [latchLocker]      DATETIME      NOT NULL,
    PRIMARY KEY CLUSTERED ([latchTaskName] ASC, [latchMachineGUID] ASC, [LatchKey] ASC)
);

