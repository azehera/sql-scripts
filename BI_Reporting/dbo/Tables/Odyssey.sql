﻿CREATE TABLE [dbo].[Odyssey] (
    [CUSTOMER_NUMBER]        NVARCHAR (255) NULL,
    [PARENTCUSTOMERNUMBER]   FLOAT (53)     NULL,
    [FIRSTNAME]              NVARCHAR (255) NULL,
    [LASTNAME]               NVARCHAR (255) NULL,
    [CUSTOMER_TYPE]          NVARCHAR (255) NULL,
    [HAR]                    NVARCHAR (255) NULL,
    [ENTRY]                  DATETIME       NULL,
    [ACTIVATION]             DATETIME       NULL,
    [REVERSION]              DATETIME       NULL,
    [ACCRUED_EARNINGS_TOTAL] MONEY          NULL,
    [ADJUSTMENT]             FLOAT (53)     NULL,
    [Earnings Less Adj]      MONEY          NULL,
    [DIRECTDEPOSIT]          NVARCHAR (255) NULL,
    [LAST_PAY_DATE]          DATETIME       NULL,
    [EMAIL]                  NVARCHAR (255) NULL,
    [PHONE]                  NVARCHAR (255) NULL,
    [F17]                    NVARCHAR (255) NULL
);

