﻿CREATE TABLE [dbo].[ReturnPostedMisMatched] (
    [Customer Number]   NVARCHAR (40) NULL,
    [Customer Name]     NVARCHAR (81) NULL,
    [ORH_ORG_NBR]       NVARCHAR (40) NULL,
    [Cart SRA Num]      NVARCHAR (40) NULL,
    [ORH_TYPE_CD]       NVARCHAR (3)  NULL,
    [ORH_STATUS_CD]     NCHAR (1)     NOT NULL,
    [ORH_CREATE_DT]     DATETIME      NOT NULL,
    [ORH_TOTAL_AMT]     MONEY         NULL,
    [Status]            CHAR (6)      NULL,
    [NavCustomerNumber] VARCHAR (25)  NULL,
    [Match]             CHAR (1)      NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ReturnPostedMisMatched_idx1]
    ON [dbo].[ReturnPostedMisMatched]([Cart SRA Num] ASC) WITH (FILLFACTOR = 90);

