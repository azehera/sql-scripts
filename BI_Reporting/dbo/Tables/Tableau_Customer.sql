﻿CREATE TABLE [dbo].[Tableau_Customer] (
    [CalendarYear]         VARCHAR (4)  NULL,
    [Month]                CHAR (15)    NULL,
    [CustomerPostingGroup] VARCHAR (10) NULL,
    [CustomerCount]        INT          NULL
);

