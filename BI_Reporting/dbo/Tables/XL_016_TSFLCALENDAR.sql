﻿CREATE TABLE [dbo].[XL_016_TSFLCALENDAR] (
    [CalendarDate]  DATETIME   NULL,
    [CalendarWeek]  FLOAT (53) NULL,
    [CalendarMonth] FLOAT (53) NULL,
    [CalendarYear]  FLOAT (53) NULL
);

