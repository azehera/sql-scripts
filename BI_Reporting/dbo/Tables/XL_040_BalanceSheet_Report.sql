﻿CREATE TABLE [dbo].[XL_040_BalanceSheet_Report] (
    [Entity]          VARCHAR (35)     NULL,
    [G_L Account No_] VARCHAR (20)     NULL,
    [G_L Name]        VARCHAR (100)    NULL,
    [RollUpName]      VARCHAR (200)    NULL,
    [January]         NUMERIC (38, 20) NULL,
    [February]        NUMERIC (38, 20) NULL,
    [March]           NUMERIC (38, 20) NULL,
    [April]           NUMERIC (38, 20) NULL,
    [May]             NUMERIC (38, 20) NULL,
    [June]            NUMERIC (38, 20) NULL,
    [July]            NUMERIC (38, 20) NULL,
    [August]          NUMERIC (38, 20) NULL,
    [September]       NUMERIC (38, 20) NULL,
    [October]         NUMERIC (38, 20) NULL,
    [November]        NUMERIC (38, 20) NULL,
    [December]        NUMERIC (38, 20) NULL,
    [2012Q4]          NUMERIC (15, 2)  NULL
);

