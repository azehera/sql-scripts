﻿CREATE TABLE [dbo].[Autoship_Snapshots] (
    [mediast_customer_number] NVARCHAR (255)  NULL,
    [user_id]                 BIGINT          NULL,
    [template_pk]             BIGINT          NOT NULL,
    [template_status]         SMALLINT        NULL,
    [channel]                 CHAR (255)      NULL,
    [next_autoshiporder_date] DATETIME2 (0)   NULL,
    [order_total_price]       DECIMAL (30, 8) NULL,
    [snapshot_date_time]      DATETIME        NOT NULL,
    [AutoshipSnapshots_PK]    INT             IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [pk_AutoshipSnapshots3] PRIMARY KEY CLUSTERED ([AutoshipSnapshots_PK] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Autoship_Snapshots_mediast_customer_number]
    ON [dbo].[Autoship_Snapshots]([mediast_customer_number] ASC)
    INCLUDE([user_id], [template_pk], [template_status], [channel], [next_autoshiporder_date], [order_total_price], [snapshot_date_time]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Autoship_Snapshots_snapshot_date_time]
    ON [dbo].[Autoship_Snapshots]([snapshot_date_time] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Autoship_Snapshots_template_status_next_autoshiporder_date]
    ON [dbo].[Autoship_Snapshots]([template_status] ASC, [next_autoshiporder_date] ASC)
    INCLUDE([template_pk], [order_total_price]);


GO
CREATE NONCLUSTERED INDEX [IX_Autoship_Snapshots_mediast_customer_number_template_status_snapshot_date_time]
    ON [dbo].[Autoship_Snapshots]([mediast_customer_number] ASC, [template_status] ASC, [snapshot_date_time] ASC)
    INCLUDE([next_autoshiporder_date]);

