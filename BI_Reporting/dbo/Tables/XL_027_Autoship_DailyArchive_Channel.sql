﻿CREATE TABLE [dbo].[XL_027_Autoship_DailyArchive_Channel] (
    [ReportDate]        VARCHAR (10) NULL,
    [CartStatus]        NCHAR (1)    NOT NULL,
    [CalendarDate]      VARCHAR (10) NULL,
    [CalendarYear]      INT          NOT NULL,
    [CalendarMonth]     INT          NOT NULL,
    [CalendarMonthName] CHAR (15)    NULL,
    [TypeName]          VARCHAR (8)  NULL,
    [Amount]            MONEY        NULL
);

