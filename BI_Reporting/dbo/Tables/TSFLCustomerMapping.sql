﻿CREATE TABLE [dbo].[TSFLCustomerMapping] (
    [CustomerNumber]     NVARCHAR (20) NOT NULL,
    [TSFLCustomerNumber] NVARCHAR (20) NULL,
    [CustomerName]       NVARCHAR (50) NULL,
    CONSTRAINT [PK_TSFLCustomerMapping] PRIMARY KEY CLUSTERED ([CustomerNumber] ASC) WITH (FILLFACTOR = 95)
);

