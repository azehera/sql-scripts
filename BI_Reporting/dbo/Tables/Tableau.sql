﻿CREATE TABLE [dbo].[Tableau] (
    [Posting Date]         DATETIME         NOT NULL,
    [CustomerPostingGroup] VARCHAR (10)     NULL,
    [SalesChannel]         VARCHAR (20)     NOT NULL,
    [Ship-to County]       VARCHAR (30)     NULL,
    [SalesType]            INT              NULL,
    [CustomerCount]        INT              NULL,
    [OrderCount]           INT              NULL,
    [LineDiscount]         DECIMAL (38, 20) NULL,
    [InvoiceDiscount]      DECIMAL (38, 20) NULL,
    [Units]                DECIMAL (38, 20) NULL,
    [Amount]               DECIMAL (38, 20) NULL
);

