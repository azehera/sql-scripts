﻿CREATE TABLE [dbo].[XL_023_MonthlySalesForecast] (
    [CalendarDate]           DATETIME        NULL,
    [TSFL]                   NUMERIC (15, 2) NULL,
    [Medifast]               NUMERIC (15, 2) NULL,
    [Total]                  NUMERIC (15, 2) NULL,
    [AutoShipActualTSFL]     NUMERIC (15, 2) NULL,
    [AutoShipActualMedifast] NUMERIC (15, 2) NULL
);

