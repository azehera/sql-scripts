﻿CREATE TABLE [dbo].[CartCustomer] (
    [Cart]      VARCHAR (50)  NULL,
    [Email]     VARCHAR (100) NULL,
    [FirstName] VARCHAR (50)  NULL,
    [LastName]  VARCHAR (50)  NULL,
    [State]     VARCHAR (5)   NULL,
    [Zip]       VARCHAR (5)   NULL,
    [Date]      DATETIME      NULL,
    [Status]    VARCHAR (10)  NULL,
    [UcartID]   VARCHAR (50)  NULL
);

