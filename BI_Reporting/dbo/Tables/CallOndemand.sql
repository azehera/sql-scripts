﻿CREATE TABLE [dbo].[CallOndemand] (
    [USA_CUST_NBR]      NVARCHAR (255) NULL,
    [Usa_ID]            NVARCHAR (255) NULL,
    [USA_First_Nm]      NVARCHAR (255) NULL,
    [USA_Last_Nm]       NVARCHAR (255) NULL,
    [USA_HOME_PH]       NVARCHAR (255) NULL,
    [Last Invoice Date] DATETIME       NULL,
    [Days Elasped]      FLOAT (53)     NULL
);

