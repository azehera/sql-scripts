﻿CREATE TABLE [dbo].[TealiumTotalBucket] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [customer_id]           NVARCHAR (255)  NULL,
    [customer_email]        NVARCHAR (255)  NULL,
    [customer_city]         NVARCHAR (255)  NULL,
    [customer_state]        NVARCHAR (3)    NULL,
    [customer_zip]          NVARCHAR (40)   NULL,
    [customer_type]         VARCHAR (8)     NOT NULL,
    [Order_Type]            VARCHAR (8)     NOT NULL,
    [order_id]              NVARCHAR (100)  NULL,
    [order_date]            NVARCHAR (4000) NULL,
    [sales_source]          NVARCHAR (255)  NULL,
    [autoship_number]       NVARCHAR (255)  NULL,
    [order_discount_amount] DECIMAL (30, 8) NULL,
    [promo_code]            NVARCHAR (40)   NULL,
    [order_subtotal]        DECIMAL (30, 8) NULL,
    [order_total]           DECIMAL (30, 8) NULL,
    [customer_order_count]  INT             NULL
);

