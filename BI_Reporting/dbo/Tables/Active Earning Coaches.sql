﻿CREATE TABLE [dbo].[Active Earning Coaches] (
    [Last Day of Month]     VARCHAR (10)     NULL,
    [CalendarYear]          INT              NOT NULL,
    [CalendarMonth]         VARCHAR (9)      NULL,
    [Active Earners]        INT              NULL,
    [TSFL Sales]            NUMERIC (38, 20) NULL,
    [TSFL Shipping Revenue] NUMERIC (38, 20) NULL,
    [Total Revenue]         NUMERIC (38, 20) NULL,
    [Revenue Per Coach]     NUMERIC (38, 20) NULL
);

