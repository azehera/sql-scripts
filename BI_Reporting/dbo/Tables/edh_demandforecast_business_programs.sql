﻿CREATE TABLE [dbo].[edh_demandforecast_business_programs] (
    [id]                       INT  NOT NULL,
    [Date]                     DATE NULL,
    [Essential_Start]          INT  NULL,
    [Business_Builder_Bonus]   INT  NULL,
    [Dash]                     INT  NULL,
    [Forward_in_Health]        INT  NULL,
    [FIBC_Summit]              INT  NULL,
    [Accelerate]               INT  NULL,
    [Incentive_Trip]           INT  NULL,
    [Go_Global]                INT  NULL,
    [Tis_the_Season]           INT  NULL,
    [Leap_Ahead]               INT  NULL,
    [ILAT]                     INT  NULL,
    [Double_CAB_Double_Assist] INT  NULL
);

