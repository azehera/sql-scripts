﻿CREATE TABLE [dbo].[KitRetention_New] (
    [GINID]                      BIGINT        NULL,
    [ItemCodeSubcategory]        VARCHAR (255) NULL,
    [DocumentNo]                 VARCHAR (255) NULL,
    [Posting Date]               DATETIME      NULL,
    [# of Times Kit Ordered_GIN] INT           NULL
);

