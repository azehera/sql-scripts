﻿CREATE TABLE [dbo].[Matrix_AutoShip] (
    [CalendarMonthName] CHAR (15)        NULL,
    [GroosSales]        NUMERIC (38, 18) NULL,
    [OrderShipped]      NUMERIC (38)     NULL,
    [AutoshipOrder]     NUMERIC (38)     NULL,
    [UcartAuto]         NUMERIC (18)     NULL
);

