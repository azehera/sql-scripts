﻿CREATE TABLE [dbo].[CallAutoship] (
    [Usa_ID]                 NVARCHAR (255) NULL,
    [USA_CUST_NBR]           NVARCHAR (255) NULL,
    [USA_First_Nm]           NVARCHAR (255) NULL,
    [USA_Last_Nm]            NVARCHAR (255) NULL,
    [USA_HOME_PH]            NVARCHAR (255) NULL,
    [TypeName]               NVARCHAR (255) NULL,
    [CustomerJoinedAutoship] DATETIME       NULL,
    [1stAutoshipDate]        DATETIME       NULL,
    [1stAutoshipAmount]      MONEY          NULL,
    [2stAutoshipDate]        NVARCHAR (255) NULL,
    [2stAutoshipAmount]      NVARCHAR (255) NULL
);

