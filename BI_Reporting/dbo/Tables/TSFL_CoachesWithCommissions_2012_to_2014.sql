﻿CREATE TABLE [dbo].[TSFL_CoachesWithCommissions_2012_to_2014] (
    [CommPeriod] VARCHAR (10) NULL,
    [CustNo]     VARCHAR (20) NULL
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TSFL_CoachesWithCommissions_2012_to_2014]
    ON [dbo].[TSFL_CoachesWithCommissions_2012_to_2014]([CommPeriod] ASC, [CustNo] ASC) WITH (FILLFACTOR = 85);

