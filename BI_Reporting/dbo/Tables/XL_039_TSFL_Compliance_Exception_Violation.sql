﻿CREATE TABLE [dbo].[XL_039_TSFL_Compliance_Exception_Violation] (
    [DocumentNumber]            VARCHAR (20)     NULL,
    [Salesperson Code]          VARCHAR (10)     NULL,
    [CoachName]                 NVARCHAR (101)   NULL,
    [Rank]                      VARCHAR (6)      NULL,
    [Sell-to Customer No_]      VARCHAR (20)     NULL,
    [Sell-to Customer Name]     VARCHAR (50)     NULL,
    [Bill-to Customer No_]      VARCHAR (20)     NULL,
    [Bill-to Name]              VARCHAR (50)     NULL,
    [Ship-to Name]              VARCHAR (50)     NULL,
    [Credit Card Holder Name]   NVARCHAR (80)    NULL,
    [Brand]                     NVARCHAR (20)    NULL,
    [CCD_NUMBER_EXT]            NVARCHAR (8)     NULL,
    [CreditCardRules]           VARCHAR (9)      NULL,
    [Document Date]             DATETIME         NULL,
    [CalMonth]                  INT              NULL,
    [Shortcut Dimension 1 Code] VARCHAR (20)     NULL,
    [Customer Posting Group]    VARCHAR (10)     NULL,
    [External Document No_]     VARCHAR (20)     NULL,
    [Authorization No_]         VARCHAR (50)     NULL,
    [Amount Authorized]         DECIMAL (38, 20) NULL
);

