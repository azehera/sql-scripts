﻿CREATE TABLE [dbo].[MDM_With_Gin] (
    [Posting Date]                  DATETIME        NULL,
    [Year]                          INT             NULL,
    [Customer Posting Group]        VARCHAR (20)    NULL,
    [Sell-to Customer No_]          VARCHAR (20)    NULL,
    [Document No_]                  VARCHAR (20)    NULL,
    [SellToMdmKey]                  VARCHAR (20)    NULL,
    [Rowid_ASC1]                    BIGINT          NULL,
    [Amount]                        NUMERIC (38, 6) NULL,
    [Units]                         NUMERIC (38, 6) NULL,
    [OrderCount]                    NUMERIC (15)    NULL,
    [OrderCountAOV]                 INT             NOT NULL,
    [GinNo]                         INT             NULL,
    [Gin1]                          INT             NOT NULL,
    [RowId_ASC]                     BIGINT          NULL,
    [Order Cycle Time Elapsed]      INT             NOT NULL,
    [Cumulative Cycle Time Elapsed] INT             NULL,
    [Cumulative Time Bucket]        VARCHAR (9)     NOT NULL
);

