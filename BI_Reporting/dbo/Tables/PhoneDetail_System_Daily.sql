﻿CREATE TABLE [dbo].[PhoneDetail_System_Daily] (
    [SystemDailyDate] DATE           NOT NULL,
    [OrigOrder]       INT            NOT NULL,
    [SplitSkill]      VARCHAR (50)   NOT NULL,
    [AvgSpeedAnswer]  INT            NULL,
    [AvgAbandonTime]  INT            NULL,
    [ACDCalls]        INT            NULL,
    [AvgACDTime]      INT            NULL,
    [AvgACWTime]      INT            NULL,
    [AbandonCalls]    INT            NULL,
    [MaxDelay]        INT            NULL,
    [FlowIn]          INT            NULL,
    [FlowOut]         INT            NULL,
    [ExtnOutCalls]    INT            NULL,
    [AvgExtnOutTime]  INT            NULL,
    [PercentACDTime]  DECIMAL (7, 2) NULL,
    [PercentAnsCalls] DECIMAL (7, 2) NULL,
    CONSTRAINT [PK_PhoneDetail_System_Daily] PRIMARY KEY CLUSTERED ([SystemDailyDate] ASC, [OrigOrder] ASC, [SplitSkill] ASC) WITH (FILLFACTOR = 85)
);

