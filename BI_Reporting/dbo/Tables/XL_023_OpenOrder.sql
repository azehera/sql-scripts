﻿CREATE TABLE [dbo].[XL_023_OpenOrder] (
    [LastDate]                  DATETIME         NULL,
    [Total$Open]                NUMERIC (38, 20) NULL,
    [BusinessDayOfFiscalPeriod] NUMERIC (2)      NULL
);

