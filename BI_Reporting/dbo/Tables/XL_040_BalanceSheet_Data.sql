﻿CREATE TABLE [dbo].[XL_040_BalanceSheet_Data] (
    [Entity]          VARCHAR (35)     NULL,
    [G_L Account No_] VARCHAR (20)     NOT NULL,
    [G_L Name]        VARCHAR (100)    NULL,
    [RollUpName]      VARCHAR (200)    NULL,
    [January]         DECIMAL (38, 20) NULL,
    [February]        DECIMAL (38, 20) NULL,
    [March]           DECIMAL (38, 20) NULL,
    [April]           DECIMAL (38, 20) NULL,
    [May]             DECIMAL (38, 20) NULL,
    [June]            DECIMAL (38, 20) NULL,
    [July]            DECIMAL (38, 20) NULL,
    [August]          DECIMAL (38, 20) NULL,
    [September]       DECIMAL (38, 20) NULL,
    [October]         DECIMAL (38, 20) NULL,
    [November]        DECIMAL (38, 20) NULL,
    [December]        DECIMAL (38, 20) NULL
);

