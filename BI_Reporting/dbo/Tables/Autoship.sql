﻿CREATE TABLE [dbo].[Autoship] (
    [USA_ID]            VARCHAR (50) NULL,
    [CTH_ID]            VARCHAR (50) NULL,
    [CartStatus]        VARCHAR (50) NULL,
    [TypeName]          VARCHAR (50) NULL,
    [CalendarDate]      VARCHAR (50) NULL,
    [CalendarYear]      VARCHAR (50) NULL,
    [CalendarMonthName] VARCHAR (50) NULL,
    [CalendarMonth]     VARCHAR (50) NULL,
    [Amount]            VARCHAR (50) NULL
);

