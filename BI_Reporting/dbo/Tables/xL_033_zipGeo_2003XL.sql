﻿CREATE TABLE [dbo].[xL_033_zipGeo_2003XL] (
    [ID]           FLOAT (53)     NULL,
    [CPG]          NVARCHAR (255) NULL,
    [ShipToKey]    NVARCHAR (255) NULL,
    [RegionName]   NVARCHAR (255) NULL,
    [ShipToName]   NVARCHAR (255) NULL,
    [CenterName]   NVARCHAR (255) NULL,
    [Address]      NVARCHAR (255) NULL,
    [City]         NVARCHAR (255) NULL,
    [State]        NVARCHAR (255) NULL,
    [Zip5]         FLOAT (53)     NULL,
    [Latitude]     FLOAT (53)     NULL,
    [Longitude]    FLOAT (53)     NULL,
    [MilePayOut]   FLOAT (53)     NULL,
    [Opening Date] DATETIME       NULL,
    [Closing Date] NVARCHAR (255) NULL
);

