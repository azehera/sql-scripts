﻿CREATE TABLE [dbo].[Tableau_ActiveEarningCoaches] (
    [CalendarYear]      INT           NOT NULL,
    [CalendarMonthName] CHAR (15)     NULL,
    [MonthYear]         NVARCHAR (20) NULL,
    [CalendarMonthID]   INT           NOT NULL,
    [State]             NVARCHAR (5)  NULL,
    [Coach]             INT           NULL
);

