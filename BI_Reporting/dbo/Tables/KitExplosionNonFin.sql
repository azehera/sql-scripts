﻿CREATE TABLE [dbo].[KitExplosionNonFin] (
    [No_]                  VARCHAR (20)     NOT NULL,
    [KitId]                VARCHAR (20)     COLLATE Latin1_General_CS_AS NULL,
    [ItemInKit]            VARCHAR (20)     COLLATE Latin1_General_CS_AS NULL,
    [Description]          VARCHAR (30)     COLLATE Latin1_General_CS_AS NULL,
    [Quantity]             NUMERIC (38, 20) NULL,
    [Unit of Measure Code] VARCHAR (10)     COLLATE Latin1_General_CS_AS NULL,
    [Customer Price Group] VARCHAR (10)     NOT NULL,
    [Unit Price]           NUMERIC (15, 2)  NULL
);

