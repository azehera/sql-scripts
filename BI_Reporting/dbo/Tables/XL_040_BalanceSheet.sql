﻿CREATE TABLE [dbo].[XL_040_BalanceSheet] (
    [Entity]            VARCHAR (35)     NULL,
    [G_L Account No_]   VARCHAR (20)     NOT NULL,
    [G_L Name]          VARCHAR (100)    NULL,
    [RollUpName]        VARCHAR (200)    NULL,
    [CalendarMonth]     INT              NULL,
    [CalendarMonthName] CHAR (15)        NULL,
    [Amount]            DECIMAL (38, 20) NULL
);

