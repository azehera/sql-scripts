﻿CREATE TABLE [dbo].[MWCC Locations] (
    [No ]                      FLOAT (53)     NULL,
    [Market]                   NVARCHAR (255) NULL,
    [Site]                     NVARCHAR (255) NULL,
    [Store No]                 FLOAT (53)     NULL,
    [Size (sf)]                FLOAT (53)     NULL,
    [Shopping Center/Building] NVARCHAR (255) NULL,
    [Address]                  NVARCHAR (255) NULL,
    [City]                     NVARCHAR (255) NULL,
    [State]                    NVARCHAR (255) NULL,
    [Zip]                      FLOAT (53)     NULL,
    [Landlord]                 NVARCHAR (255) NULL,
    [Latitude]                 NVARCHAR (255) NULL,
    [Longitude]                NVARCHAR (255) NULL,
    [Deliverable]              NVARCHAR (255) NULL
);

