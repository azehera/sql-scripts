﻿CREATE TABLE [dbo].[OrderType] (
    [DocNo]     VARCHAR (20) NOT NULL,
    [OrderType] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_OrderType] PRIMARY KEY CLUSTERED ([DocNo] ASC, [OrderType] ASC) WITH (FILLFACTOR = 80)
);

