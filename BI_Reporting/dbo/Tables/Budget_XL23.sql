﻿CREATE TABLE [dbo].[Budget_XL23] (
    [Channel]         NVARCHAR (255) NULL,
    [CalendarMonthID] FLOAT (53)     NULL,
    [MonthlyBudget]   FLOAT (53)     NULL,
    [CalendarQuarter] FLOAT (53)     NULL,
    [MonthlyForecast] FLOAT (53)     NULL
);

