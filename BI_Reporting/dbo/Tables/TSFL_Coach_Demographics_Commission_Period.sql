﻿CREATE TABLE [dbo].[TSFL_Coach_Demographics_Commission_Period] (
    [CID]                               VARCHAR (50) NULL,
    [COMMISSION_PERIOD]                 VARCHAR (50) NULL,
    [POSTAL_CODE]                       VARCHAR (50) NULL,
    [Start Date]                        VARCHAR (50) NULL,
    [INACTIVE DATE]                     VARCHAR (50) NULL,
    [ORIGINAL SPONSOR CID]              VARCHAR (50) NULL,
    [Current_Upline]                    VARCHAR (50) NULL,
    [Top of Line CID]                   VARCHAR (50) NULL,
    [Highest Title]                     VARCHAR (50) NULL,
    [Current Title]                     VARCHAR (50) NULL,
    [NUMBER OF NEW COACHES SPONSORED]   INT          NULL,
    [NUMBER OF NEW CUSTOMERS SPONSORED] INT          NULL,
    [Relation Type]                     VARCHAR (50) NULL
);

