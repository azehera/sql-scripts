﻿CREATE TABLE [dbo].[F1091v6] (
    [Month-Year]                         DATE             NULL,
    [Medifast Customer Number]           NVARCHAR (20)    NULL,
    [Count of Orders]                    INT              NULL,
    [Total Spend]                        DECIMAL (38, 20) NULL,
    [Total Eaches]                       DECIMAL (38, 20) NULL,
    [Count of HoH Orders - 74881]        INT              NOT NULL,
    [Spend on HoH- 74881]                DECIMAL (38, 20) NOT NULL,
    [HOH Eaches - 74881]                 DECIMAL (38, 20) NOT NULL,
    [Count of HoH Orders - 76997]        INT              NOT NULL,
    [Spend on HoH -76997]                DECIMAL (38, 20) NOT NULL,
    [HOH Eaches - 76997]                 DECIMAL (38, 20) NOT NULL,
    [Count of HoH Print Material Orders] INT              NOT NULL,
    [Spend on HoH Print Material]        DECIMAL (38, 20) NOT NULL,
    [HOH Eaches Print Material]          DECIMAL (38, 20) NOT NULL
);

