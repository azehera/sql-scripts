﻿CREATE TABLE [dbo].[XL_040_BalanceSheetLY] (
    [Entity]                         VARCHAR (30)     NOT NULL,
    [Current Year Retained Earnings] DECIMAL (38, 20) NULL
);

