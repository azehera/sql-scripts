﻿CREATE TABLE [dbo].[XL_023_MonthlySalesTracker_TY] (
    [BusinessDayOfFiscalPeriod] INT              NULL,
    [Posting Date]              DATETIME         NULL,
    [Year]                      INT              NULL,
    [ActualRevenue]             NUMERIC (38, 20) NULL,
    [CummulativeRevenue]        NUMERIC (38, 20) NULL,
    [MonthlyBudget]             NUMERIC (38, 18) NULL,
    [CalendarMonthID]           INT              NULL,
    [RemainingDays]             NUMERIC (16)     NULL,
    [RealDate]                  VARCHAR (50)     NULL,
    [CountBusinessDay]          INT              NULL,
    [Month]                     INT              NULL,
    [AvgDayRemaining]           NUMERIC (38, 18) NULL,
    [Total$Open]                NUMERIC (38, 18) NULL
);

