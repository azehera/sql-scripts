﻿CREATE TABLE [dbo].[PossibleLapsedCustomerMiles] (
    [USA_ID]             BIGINT          NULL,
    [USA_FIRST_NM]       NVARCHAR (40)   NULL,
    [USA_LAST_NM]        NVARCHAR (40)   NULL,
    [USA_EMAIL]          NVARCHAR (250)  NULL,
    [USA_HOME_PH]        NVARCHAR (20)   NULL,
    [Ship-to Address]    VARCHAR (50)    NOT NULL,
    [Ship-to Address 2]  VARCHAR (50)    NOT NULL,
    [Ship-to City]       VARCHAR (30)    NOT NULL,
    [Ship-to County]     VARCHAR (30)    NOT NULL,
    [Ship-to Post Code]  VARCHAR (20)    NOT NULL,
    [PostingDate]        DATETIME        NULL,
    [Latitude]           NUMERIC (18, 6) NULL,
    [Longitude]          NUMERIC (18, 6) NULL,
    [BaseDistance]       INT             NULL,
    [CenterZip]          VARCHAR (20)    NULL,
    [Miles]              FLOAT (53)      NULL,
    [Region]             CHAR (35)       NULL,
    [ShipToKey]          VARCHAR (10)    NULL,
    [ShipToName]         VARCHAR (35)    NULL,
    [FranchiseLatitude]  DECIMAL (18, 6) NULL,
    [FranchiseLongitude] DECIMAL (18, 6) NULL
);

