﻿CREATE TABLE [dbo].[OpenOrdersSnapshotDE] (
    [Country]          VARCHAR (10)    NULL,
    [Parent_SKU]       NVARCHAR (255)  NULL,
    [Location_Code]    NVARCHAR (255)  NULL,
    [order_date]       DATETIME2 (0)   NULL,
    [Units_Sold]       NUMERIC (38, 6) NULL,
    [snapshotDateTime] DATETIME        NULL,
    [pk]               INT             IDENTITY (1, 1) NOT NULL,
    PRIMARY KEY CLUSTERED ([pk] ASC)
);

