﻿CREATE TABLE [dbo].[TSFL_monthly_snapshot] (
    [Last Day of Month]    DATETIME        NULL,
    [LastDateOfQuarter]    DATETIME        NULL,
    [CalendarYear]         VARCHAR (20)    NULL,
    [CalendarMonth]        VARCHAR (20)    NULL,
    [Active Earners]       NUMERIC (15)    NULL,
    [Total Health Coaches] NUMERIC (15)    NULL,
    [% of Earning Coaches] NUMERIC (15, 4) NULL,
    [Sponsoring]           NUMERIC (15)    NULL,
    [Attrition]            NUMERIC (15)    NULL,
    [Attrition %]          NUMERIC (15, 4) NULL,
    [New Clients]          NUMERIC (15)    NULL,
    [Orders per Coach]     NUMERIC (15, 2) NULL,
    [Revenue Per Coach]    NUMERIC (15, 2) NULL
);

