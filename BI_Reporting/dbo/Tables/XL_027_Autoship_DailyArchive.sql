﻿CREATE TABLE [dbo].[XL_027_Autoship_DailyArchive] (
    [ReportDate]        VARCHAR (10) NULL,
    [CartStatus]        NCHAR (1)    NOT NULL,
    [CalendarDate]      VARCHAR (10) NULL,
    [CalendarYear]      INT          NOT NULL,
    [CalendarMonth]     INT          NOT NULL,
    [CalendarMonthName] CHAR (15)    NULL,
    [TypeName]          VARCHAR (8)  NULL,
    [Amount]            MONEY        NULL,
    [OrderCount]        INT          NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_XL_027_Autoship_DailyArchive_CartStatus]
    ON [dbo].[XL_027_Autoship_DailyArchive]([CartStatus] ASC)
    INCLUDE([ReportDate], [CalendarDate], [Amount], [OrderCount]) WITH (FILLFACTOR = 90);

