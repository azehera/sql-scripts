﻿CREATE TABLE [dbo].[RS_019_Pricing_Calculator] (
    [CustomerPostingGroup]      VARCHAR (10)     NULL,
    [Product Group Code]        VARCHAR (100)    NULL,
    [ItemCategoryCode]          VARCHAR (20)     NOT NULL,
    [ItemCode]                  VARCHAR (50)     NULL,
    [Description]               VARCHAR (255)    NULL,
    [CodeWithDescription]       VARCHAR (306)    NULL,
    [Expenses]                  INT              NULL,
    [CustomerPostingGroupTotal] INT              NULL,
    [TotalUnits]                NUMERIC (38, 20) NULL,
    [TotalAmount]               NUMERIC (38, 20) NULL,
    [ASP]                       NUMERIC (38, 6)  NULL
);

