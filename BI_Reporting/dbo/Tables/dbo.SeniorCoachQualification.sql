﻿CREATE TABLE [dbo].[dbo.SeniorCoachQualification] (
    [CUSTOMER_ID]       FLOAT (53)     NULL,
    [FIRST_NAME]        NVARCHAR (255) NULL,
    [LAST_NAME]         NVARCHAR (255) NULL,
    [RECOGNITION_NAME]  NVARCHAR (255) NULL,
    [STATE]             NVARCHAR (255) NULL,
    [RANK_ADVANCE_DATE] DATETIME       NULL,
    [COMMISSION_PERIOD] DATETIME       NULL,
    [RANK_ID]           FLOAT (53)     NULL,
    [RANK]              NVARCHAR (255) NULL,
    [Rank up Month]     FLOAT (53)     NULL,
    [CUSTOMER_NUMBER]   FLOAT (53)     NULL
);

