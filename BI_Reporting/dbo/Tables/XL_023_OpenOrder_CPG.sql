﻿CREATE TABLE [dbo].[XL_023_OpenOrder_CPG] (
    [Customer Posting Group]    VARCHAR (20)     NULL,
    [LastDate]                  DATETIME         NULL,
    [Total$Open]                NUMERIC (38, 20) NULL,
    [BusinessDayOfFiscalPeriod] NUMERIC (2)      NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_XL_023_OpenOrder_CPG_LastDate]
    ON [dbo].[XL_023_OpenOrder_CPG]([LastDate] ASC)
    INCLUDE([Customer Posting Group], [Total$Open]) WITH (FILLFACTOR = 90);

