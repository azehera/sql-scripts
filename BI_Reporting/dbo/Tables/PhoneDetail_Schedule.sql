﻿CREATE TABLE [dbo].[PhoneDetail_Schedule] (
    [schedule_id]     INT           NOT NULL,
    [action_abbrev]   NVARCHAR (20) NULL,
    [catchup_date]    DATE          NULL,
    [last_execute]    DATETIME      NULL,
    [frequency_hours] INT           NULL,
    [next_execute]    DATETIME      NULL,
    CONSTRAINT [PK_PhoneDetail_Schedule] PRIMARY KEY CLUSTERED ([schedule_id] ASC) WITH (FILLFACTOR = 95)
);

