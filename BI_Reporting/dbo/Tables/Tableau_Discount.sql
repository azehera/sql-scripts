﻿CREATE TABLE [dbo].[Tableau_Discount] (
    [Posting Date]         DATETIME         NULL,
    [CustomerPostingGroup] VARCHAR (10)     NULL,
    [SalesChannel]         VARCHAR (20)     NULL,
    [SalesType]            INT              NULL,
    [Discount]             DECIMAL (38, 20) NULL,
    [DiscountType]         VARCHAR (15)     NULL
);

