﻿CREATE TABLE [dbo].[SKU_Shippable_Consumable_Mappings] (
    [parent_sku]                   NVARCHAR (50)  NOT NULL,
    [parent_sku_description]       NVARCHAR (150) NOT NULL,
    [consumable_vs_non_consumable] NVARCHAR (50)  NOT NULL,
    [shippable_vs_non_shippable]   NVARCHAR (50)  NOT NULL
);

