﻿CREATE TABLE [dbo].[XL_040_LY_Q4] (
    [G_L Account No_] VARCHAR (20)    NULL,
    [G_L Name]        VARCHAR (35)    NULL,
    [RollUpName]      VARCHAR (35)    NULL,
    [Amount]          NUMERIC (15, 2) DEFAULT ((0)) NULL,
    [Entity]          VARCHAR (35)    NULL
);

