﻿CREATE TABLE [dbo].[ScrubEmailList] (
    [EMAIL]             VARCHAR (100) NULL,
    [CustomerName]      VARCHAR (101) NULL,
    [TSFL]              INT           NULL,
    [MED-DIRECT]        INT           NULL,
    [MWCC]              INT           NULL,
    [TotalCPG]          INT           NULL,
    [Last Invoice Date] DATETIME      NULL,
    [LastCPG]           VARCHAR (20)  NULL
);

