﻿CREATE TABLE [dbo].[REPACOMM] (
    [Group]          VARCHAR (3)    NOT NULL,
    [CsrFirstName]   NVARCHAR (40)  NULL,
    [CsrLastName]    NVARCHAR (40)  NULL,
    [CsrUserID]      NVARCHAR (128) NULL,
    [CustomerUserID] VARCHAR (1)    NOT NULL,
    [CustomerNumber] VARCHAR (1)    NOT NULL,
    [OrderNumber]    VARCHAR (1)    NOT NULL,
    [RAC]            MONEY          NULL,
    [Date]           DATE           NULL
);

