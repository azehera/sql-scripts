﻿CREATE TABLE [dbo].[Exigo_ItemPRICE] (
    [CompanyID]            INT              NULL,
    [ItemID]               INT              NULL,
    [ItemCode]             NVARCHAR (128)   NULL,
    [CurrencyCode]         VARCHAR (3)      NOT NULL,
    [PriceTy]              VARCHAR (1)      NOT NULL,
    [Price]                NUMERIC (18, 4)  NULL,
    [BusinessVolume]       NUMERIC (18, 4)  NULL,
    [CommissionableVolume] NUMERIC (18, 4)  NULL,
    [TaxablePrice]         DECIMAL (38, 20) NOT NULL,
    [ShippingPrice]        DECIMAL (38, 20) NOT NULL
);

