﻿CREATE TABLE [dbo].[ProductRetention] (
    [GINID]                          BIGINT        NULL,
    [ItemCode]                       VARCHAR (255) NULL,
    [DocumentNo]                     VARCHAR (255) NULL,
    [Posting Date]                   DATETIME      NULL,
    [# of Times Product Ordered_GIN] INT           NULL
);

