﻿CREATE TABLE [dbo].[SalesInvoiceLineTest] (
    [Document No_]    VARCHAR (20)     NOT NULL,
    [MfgPartNo]       INT              NULL,
    [No_]             VARCHAR (20)     NOT NULL,
    [Quantity]        DECIMAL (38, 20) NOT NULL,
    [Unit of Measure] VARCHAR (10)     NOT NULL,
    [Unit Price]      DECIMAL (38, 20) NOT NULL,
    [Amount]          DECIMAL (38, 20) NOT NULL,
    [TaxAmt]          INT              NOT NULL
);

