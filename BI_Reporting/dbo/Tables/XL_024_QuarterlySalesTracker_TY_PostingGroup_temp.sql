﻿CREATE TABLE [dbo].[XL_024_QuarterlySalesTracker_TY_PostingGroup_temp] (
    [BusinessDayOfFiscalPeriod] INT              NULL,
    [BusinessDayOfFiscalYear]   INT              NULL,
    [CustomerPostingGroup]      NVARCHAR (10)    NOT NULL,
    [Posting Date]              DATETIME         NULL,
    [Year]                      INT              NULL,
    [ActualRevenue]             DECIMAL (38, 20) NULL,
    [CummulativeRevenue]        DECIMAL (38, 20) NULL,
    [MonthlyBudget]             FLOAT (53)       NOT NULL,
    [CalendarYearQuarter]       FLOAT (53)       NULL,
    [RemainingDays]             NUMERIC (16)     NULL,
    [RealDate]                  VARCHAR (50)     NULL,
    [CountBusinessDay]          INT              NULL,
    [Month]                     INT              NULL,
    [AvgDayRemaining]           FLOAT (53)       NULL
);

