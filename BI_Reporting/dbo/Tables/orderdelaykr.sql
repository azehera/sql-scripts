﻿CREATE TABLE [dbo].[orderdelaykr] (
    [Order]            NVARCHAR (255) NULL,
    [Creation Date]    DATETIME       NULL,
    [Requested Date]   DATETIME       NULL,
    [Item]             FLOAT (53)     NULL,
    [Customer]         NVARCHAR (255) NULL,
    [Ship-To Location] NVARCHAR (255) NULL
);

