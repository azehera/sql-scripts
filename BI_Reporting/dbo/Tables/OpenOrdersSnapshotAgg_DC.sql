﻿CREATE TABLE [dbo].[OpenOrdersSnapshotAgg_DC] (
    [Location_Code]    NVARCHAR (255)   NULL,
    [OpenOrderCount]   INT              NULL,
    [OpenOrderAmount]  DECIMAL (38, 20) NULL,
    [snapshotDateTime] DATETIME         NULL,
    [pk]               INT              IDENTITY (1, 1) NOT NULL
);

