﻿CREATE TABLE [dbo].[All Centers] (
    [#]                    FLOAT (53)     NULL,
    [Code]                 NVARCHAR (255) NULL,
    [Owner]                NVARCHAR (255) NULL,
    [Entity]               NVARCHAR (255) NULL,
    [Center Number]        NVARCHAR (255) NULL,
    [Center Name]          NVARCHAR (255) NULL,
    [Address]              NVARCHAR (255) NULL,
    [City]                 NVARCHAR (255) NULL,
    [State]                NVARCHAR (255) NULL,
    [Zip Code]             FLOAT (53)     NULL,
    [DMA]                  NVARCHAR (255) NULL,
    [Opening Date]         DATETIME       NULL,
    [Center Age]           NVARCHAR (255) NULL,
    [2013 Invoice Revenue] MONEY          NULL
);

