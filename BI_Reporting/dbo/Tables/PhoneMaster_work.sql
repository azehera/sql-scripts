﻿CREATE TABLE [dbo].[PhoneMaster_work] (
    [PhoneNumber] NVARCHAR (20) NOT NULL,
    [VDN]         NVARCHAR (10) NULL,
    [Description] NVARCHAR (50) NULL,
    [CatchupDate] DATE          NULL
);

