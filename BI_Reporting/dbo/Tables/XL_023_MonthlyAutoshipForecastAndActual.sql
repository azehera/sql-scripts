﻿CREATE TABLE [dbo].[XL_023_MonthlyAutoshipForecastAndActual] (
    [CalendarDate]     DATETIME        NULL,
    [TsflForecast]     NUMERIC (15, 2) NULL,
    [MedifastForecast] NUMERIC (15, 2) NULL,
    [TsflActual]       NUMERIC (15, 2) NULL,
    [MedifastActual]   NUMERIC (15, 2) NULL
);

