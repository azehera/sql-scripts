﻿CREATE TABLE [dbo].[B4T_location] (
    [location_id]   BIGINT       NOT NULL,
    [location_code] VARCHAR (10) NULL,
    [location_name] VARCHAR (50) NOT NULL,
    [OpenStores]    INT          NOT NULL
);

