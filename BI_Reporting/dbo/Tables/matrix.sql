﻿CREATE TABLE [dbo].[matrix] (
    [CalendarYear]      INT              NULL,
    [CalendarMonthName] CHAR (15)        NULL,
    [SalesChannel]      VARCHAR (15)     NULL,
    [Accounts]          VARCHAR (8)      NOT NULL,
    [OrderCount]        NUMERIC (15)     NULL,
    [CustomerCount]     NUMERIC (15)     NULL,
    [Dollars]           NUMERIC (38, 18) NULL
);

