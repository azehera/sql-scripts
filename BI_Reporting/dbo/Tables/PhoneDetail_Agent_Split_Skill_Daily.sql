﻿CREATE TABLE [dbo].[PhoneDetail_Agent_Split_Skill_Daily] (
    [ActivityDate] DATE         NOT NULL,
    [AgentName]    VARCHAR (50) NOT NULL,
    [SplitSkill]   VARCHAR (50) NOT NULL,
    [ACDCalls]     INT          NULL,
    [ACDTime]      INT          NULL,
    [ACWTime]      INT          NULL,
    [ExtnInCalls]  INT          NULL,
    [ExtnInTime]   INT          NULL,
    [ExtnOutCalls] INT          NULL,
    [ExtnOutTime]  INT          NULL,
    [Assists]      INT          NULL,
    [HeldCalls]    INT          NULL,
    [HoldTime]     INT          NULL,
    [TransOut]     INT          NULL,
    CONSTRAINT [PK_PhoneDetail_Agent_Split_Skill_Daily] PRIMARY KEY CLUSTERED ([ActivityDate] ASC, [AgentName] ASC, [SplitSkill] ASC) WITH (FILLFACTOR = 85)
);

