﻿CREATE TABLE [dbo].[XL_027_AutoshipAsOf] (
    [AutoshipAsOf]      VARCHAR (10) NULL,
    [USA_ID]            BIGINT       NOT NULL,
    [CTH_ID]            BIGINT       NOT NULL,
    [CartStatus]        NCHAR (1)    NOT NULL,
    [TypeName]          VARCHAR (8)  NOT NULL,
    [CalendarDate]      DATETIME     NULL,
    [CalendarYear]      INT          NOT NULL,
    [CalendarMonthName] CHAR (15)    NULL,
    [CalendarMonth]     INT          NOT NULL,
    [Amount]            MONEY        NULL
);

