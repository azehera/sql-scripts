﻿CREATE TABLE [dbo].[CartCustomerTest] (
    [UAD_USA_ID]        BIGINT        NULL,
    [UAD_STATUS_CD]     NVARCHAR (3)  NOT NULL,
    [UAD_ADDR1]         NVARCHAR (80) NOT NULL,
    [UAD_ADDR2]         NVARCHAR (80) NULL,
    [UAD_ADDR3]         NVARCHAR (80) NULL,
    [UAD_CITY]          NVARCHAR (40) NULL,
    [UAD_STATE_CD]      NVARCHAR (3)  NULL,
    [UAD_COUNTRY_CD]    NVARCHAR (3)  NULL,
    [UAD_PCODE]         NVARCHAR (40) NULL,
    [UAD_CTC_FIRST_NM]  NVARCHAR (40) NULL,
    [UAD_CTC_MIDDLE_NM] NVARCHAR (40) NULL,
    [UAD_CTC_LAST_NM]   NVARCHAR (40) NULL,
    [DATE_]             VARCHAR (10)  NULL,
    [No]                BIGINT        NULL
);

