﻿CREATE TABLE [dbo].[KitProduction] (
    [LatestPrice]          DATETIME        NULL,
    [KitId]                VARCHAR (20)    COLLATE Latin1_General_CS_AS NOT NULL,
    [ItemInKit]            VARCHAR (20)    COLLATE Latin1_General_CS_AS NOT NULL,
    [Description]          VARCHAR (30)    COLLATE Latin1_General_CS_AS NOT NULL,
    [Units]                DECIMAL (38, 6) NULL,
    [Unit of Measure Code] VARCHAR (10)    COLLATE Latin1_General_CS_AS NOT NULL,
    [Unit Price]           NUMERIC (38, 6) NULL,
    [Currency]             VARCHAR (3)     NOT NULL,
    [Sales Code]           VARCHAR (20)    COLLATE Latin1_General_CS_AS NOT NULL
);

