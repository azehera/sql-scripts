﻿CREATE TABLE [dbo].[PhoneMaster] (
    [PhoneNumber] NVARCHAR (20) NOT NULL,
    [VDN]         NVARCHAR (10) NULL,
    [Description] NVARCHAR (50) NULL,
    [CatchupDate] DATE          NULL,
    CONSTRAINT [PK_PhoneMaster] PRIMARY KEY CLUSTERED ([PhoneNumber] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PhoneMaster]
    ON [dbo].[PhoneMaster]([VDN] ASC) WITH (FILLFACTOR = 90);

