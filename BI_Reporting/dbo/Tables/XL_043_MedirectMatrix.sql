﻿CREATE TABLE [dbo].[XL_043_MedirectMatrix] (
    [CalDate]                     DATETIME        NULL,
    [GET STARTED NOW]             NUMERIC (15)    NULL,
    [ADDED TO CART]               NUMERIC (15)    NULL,
    [SHOPPING CONFIRMATION]       NUMERIC (15)    NULL,
    [WEIGHT LOSS PLAN]            NUMERIC (15)    NULL,
    [CHECKOUT]                    NUMERIC (15)    NULL,
    [MEDIFAST HOME PAGE]          NUMERIC (15)    NULL,
    [BEST SELLERS]                NUMERIC (15)    NULL,
    [WEIGHT-LOSS SUCCESS STORIES] NUMERIC (15)    NULL,
    [SHOPPING CART]               NUMERIC (15)    NULL,
    [SALES]                       NUMERIC (38, 2) NULL,
    [ORDER COUNT]                 NUMERIC (15)    NULL,
    [AOV]                         NUMERIC (15, 2) NULL,
    [SESSION_ID]                  NUMERIC (38)    NULL,
    [ORDER/SESSION]               NUMERIC (15, 2) NULL,
    [ABANDONMENT RATE]            NUMERIC (15, 1) NULL,
    [TRACK VISITORS]              NUMERIC (15)    NULL,
    [NEW VISITORS]                NUMERIC (15)    NULL,
    [REPEAT VISITORS]             NUMERIC (15)    NULL
);

