﻿CREATE TABLE [dbo].[TSFL_monthlyactive_data] (
    [Calendar Month]       DATETIME     NULL,
    [Total Health Coaches] NUMERIC (15) NULL,
    [Sponsoring]           NUMERIC (15) NULL,
    [New Clients]          NUMERIC (15) NULL,
    [Orders]               NUMERIC (15) NULL
);

