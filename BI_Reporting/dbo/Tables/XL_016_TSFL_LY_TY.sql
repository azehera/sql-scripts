﻿CREATE TABLE [dbo].[XL_016_TSFL_LY_TY] (
    [Posting Date]           DATETIME         NULL,
    [Calendar Week]          FLOAT (53)       NULL,
    [Calendar Year]          FLOAT (53)       NULL,
    [Customer Posting Group] VARCHAR (10)     NULL,
    [Orders]                 VARCHAR (20)     NULL,
    [No_]                    VARCHAR (20)     NULL,
    [Type]                   VARCHAR (20)     NULL,
    [Dollars]                NUMERIC (38, 20) NULL,
    [Units]                  NUMERIC (38, 20) NULL,
    [OrderCount]             NUMERIC (15)     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Posting_Date]
    ON [dbo].[XL_016_TSFL_LY_TY]([Posting Date] DESC);


GO
CREATE NONCLUSTERED INDEX [IX_XL_016_TSFL_LY_TY_idx1]
    ON [dbo].[XL_016_TSFL_LY_TY]([Customer Posting Group] ASC, [Posting Date] ASC)
    INCLUDE([OrderCount]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_XL_016_TSFL_LY_TY_idx2]
    ON [dbo].[XL_016_TSFL_LY_TY]([Customer Posting Group] ASC, [Posting Date] ASC)
    INCLUDE([Dollars]) WITH (FILLFACTOR = 90);

