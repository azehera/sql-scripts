﻿CREATE TABLE [dbo].[BusinessCalendar_2021a] (
    [DWDateKey]                  DATE          NULL,
    [DayDate]                    INT           NULL,
    [DayOfWeekName]              NVARCHAR (30) NULL,
    [WeekNumber]                 INT           NULL,
    [MonthNumber]                INT           NULL,
    [MonthName]                  NVARCHAR (30) NULL,
    [MonthShortName]             NVARCHAR (4)  NULL,
    [Year]                       INT           NULL,
    [QuarterNumber]              NVARCHAR (30) NULL,
    [QuarterName]                NVARCHAR (30) NULL,
    [RealDate]                   VARCHAR (50)  NULL,
    [businessDay]                BIT           NULL,
    [BusinessDayOfFiscalYear]    INT           NULL,
    [BusinessDayOfFiscalPeriod]  INT           NULL,
    [BusinessDayOfFiscalQuarter] INT           NULL,
    [Posting Date]               DATETIME      NULL,
    [CalendarYearQuarter]        VARCHAR (50)  NULL
);

