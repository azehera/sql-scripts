﻿CREATE TABLE [dbo].[Jira_5544] (
    [Customer Type]             VARCHAR (8)     NOT NULL,
    [Order_Date]                DATE            NULL,
    [FIRST POSTING DATE]        DATE            NULL,
    [Posting Date]              DATE            NULL,
    [PROMO CODE]                NVARCHAR (255)  NULL,
    [ORDER_ID]                  NVARCHAR (255)  NULL,
    [Sales]                     NUMERIC (30, 8) NULL,
    [New vs. Existing COMPANY]  VARCHAR (8)     NULL,
    [MEDD FIRST POSTING DATE]   DATE            NULL,
    [New vs. Existing MEDD]     VARCHAR (8)     NULL,
    [MEDD LAST POSTING DATE]    DATETIME        NULL,
    [COMPANY LAST POSTING DATE] DATETIME        NULL
);

