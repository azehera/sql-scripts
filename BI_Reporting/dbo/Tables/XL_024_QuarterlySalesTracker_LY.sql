﻿CREATE TABLE [dbo].[XL_024_QuarterlySalesTracker_LY] (
    [BusinessDayOfFiscalPeriod] INT              NULL,
    [BusinessDayOfFiscalYear]   INT              NULL,
    [Posting Date]              DATETIME         NULL,
    [Year]                      INT              NULL,
    [ActualRevenue]             NUMERIC (38, 20) NULL,
    [CummulativeRevenue]        NUMERIC (38, 20) NULL,
    [MonthlyBudget]             NUMERIC (38, 18) NOT NULL,
    [CalendarYearQuarter]       VARCHAR (50)     NULL,
    [RemainingDays]             NUMERIC (16)     NULL,
    [RealDate]                  VARCHAR (50)     NULL,
    [CountBusinessDay]          INT              NULL,
    [Month]                     INT              NULL,
    [AvgDayRemaining]           NUMERIC (38, 18) NULL
);

