﻿CREATE TABLE [dbo].[TSFL_Upline_Presidential] (
    [CustId]       NUMERIC (9)  NULL,
    [CustNo]       VARCHAR (20) NULL,
    [CommPeriod]   DATETIME     NULL,
    [UplineCustNo] VARCHAR (20) NULL,
    [HighRank]     VARCHAR (50) NULL
);

