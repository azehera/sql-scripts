﻿CREATE TABLE [dbo].[XL_015_Monthly_AP_ALL] (
    [Entity]                  VARCHAR (25)    NULL,
    [Source Code]             VARCHAR (10)    NULL,
    [Posting Date]            DATETIME        NULL,
    [Document Date]           DATETIME        NULL,
    [G_L Account No_]         VARCHAR (20)    NULL,
    [G_L Name]                VARCHAR (100)   NULL,
    [Vendor No_]              VARCHAR (20)    NULL,
    [1099 Code]               VARCHAR (10)    NULL,
    [1099 Amount]             DECIMAL (38, 4) NULL,
    [VendorName]              VARCHAR (50)    NULL,
    [Address]                 VARCHAR (250)   NULL,
    [Address 2]               VARCHAR (250)   NULL,
    [City]                    VARCHAR (250)   NULL,
    [State]                   VARCHAR (250)   NULL,
    [Zip Code]                VARCHAR (20)    NULL,
    [Country_Region Code]     VARCHAR (250)   NULL,
    [Vendor Posting Group]    VARCHAR (10)    NULL,
    [Document No_]            VARCHAR (20)    NULL,
    [Description]             VARCHAR (50)    NULL,
    [Global Dimension 2 Code] VARCHAR (20)    NULL,
    [Transaction No_]         INT             NULL,
    [Source No_]              VARCHAR (20)    NULL,
    [Entry No_]               INT             NULL,
    [External Document No_]   VARCHAR (35)    NULL,
    [User ID]                 VARCHAR (50)    NULL,
    [Dimension Value Code]    VARCHAR (20)    NULL,
    [ProjectName]             VARCHAR (50)    NULL,
    [Debit Amount]            DECIMAL (38, 4) NULL,
    [Credit Amount]           DECIMAL (38, 4) NULL,
    [NetTotal]                DECIMAL (38, 4) NULL,
    [Check#]                  VARCHAR (20)    NULL,
    [CheckDate]               DATETIME        NULL,
    [DaysElasped]             NUMERIC (15)    NULL,
    [Dimension Set ID]        INT             NULL
);

