﻿CREATE TABLE [dbo].[OpenOrdersSnapshotAgg] (
    [OpenOrderCount]   INT              NULL,
    [OpenOrderAmount]  DECIMAL (38, 20) NULL,
    [snapshotDateTime] DATETIME         NULL,
    [pk]               INT              IDENTITY (1, 1) NOT NULL
);

