﻿CREATE TABLE [dbo].[XL_029_TSFL_NewMarketDevelopment] (
    [Category]          VARCHAR (20)     NULL,
    [CalMonth]          INT              NULL,
    [CalendarMonthName] CHAR (15)        NULL,
    [DMA Code]          VARCHAR (50)     NULL,
    [DMA Name]          VARCHAR (50)     NULL,
    [Number Of Order]   INT              NULL,
    [Amount]            NUMERIC (38, 20) NULL
);

