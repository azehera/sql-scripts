﻿CREATE TABLE [dbo].[SmartyStreetsLocations] (
    [ID]        VARCHAR (20)    NULL,
    [Street]    VARCHAR (100)   NULL,
    [City]      VARCHAR (50)    NULL,
    [State]     VARCHAR (10)    NULL,
    [ZipCode]   VARCHAR (10)    NULL,
    [Latitude]  NUMERIC (18, 6) NULL,
    [Longitude] NUMERIC (18, 6) NULL
);

