﻿CREATE TABLE [dbo].[XL_023_MonthlySalesForecast_Backup] (
    [CalendarDate]   DATETIME        NULL,
    [TSFL]           NUMERIC (15, 2) NULL,
    [Medifast]       NUMERIC (15, 2) NULL,
    [Total]          NUMERIC (15, 2) NULL,
    [AutoShipActual] NUMERIC (15, 2) NULL
);

