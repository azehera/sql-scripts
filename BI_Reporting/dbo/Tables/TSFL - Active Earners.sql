﻿CREATE TABLE [dbo].[TSFL - Active Earners] (
    [Dist ID]           NVARCHAR (255) NULL,
    [Name]              NVARCHAR (255) NULL,
    [Rank]              FLOAT (53)     NULL,
    [# SC Legs]         FLOAT (53)     NULL,
    [# ED Legs]         FLOAT (53)     NULL,
    [Month]             NVARCHAR (255) NULL,
    [Month #]           FLOAT (53)     NULL,
    [Year]              FLOAT (53)     NULL,
    [Last Day of Month] DATETIME       NULL
);

