﻿CREATE TABLE [dbo].[XL_039_TSFL_Compliance_Exception_Sales_Above_$1000] (
    [Sell-to Customer No_]  VARCHAR (20)     NOT NULL,
    [Sell-to Customer Name] VARCHAR (50)     NOT NULL,
    [Salesperson Code]      VARCHAR (10)     NOT NULL,
    [CoachName]             NVARCHAR (101)   NULL,
    [CoachEmail]            NVARCHAR (50)    NULL,
    [CalMonth]              INT              NULL,
    [GrossSales]            DECIMAL (38, 20) NULL,
    [OrderCounts]           INT              NULL
);

