﻿CREATE TABLE [dbo].[CUSTOMER_LIFE_MDM] (
    [MedifastCustomerNumber]    VARCHAR (50)     NULL,
    [Blocked]                   INT              NULL,
    [LifeTimeValue]             DECIMAL (38, 20) NULL,
    [FirstInvoiceDate]          DATETIME         NULL,
    [Life Invoice]              INT              NULL,
    [CustomerDraftClass]        VARCHAR (6)      NULL,
    [LifeMonth]                 INT              NULL,
    [FIBC]                      VARCHAR (5)      NULL,
    [FIBL]                      VARCHAR (5)      NULL,
    [Created_Date]              DATETIME         NULL,
    [Updated_Date]              DATETIME         NULL,
    [LastInvoiceDate]           DATETIME         NULL,
    [Coach Tenure]              INT              NULL,
    [First Order Posting Group] NVARCHAR (10)    NULL,
    [Last Order Posting Group]  NVARCHAR (10)    NULL,
    [First Order Type]          VARCHAR (20)     NULL,
    [Last Order Type]           VARCHAR (20)     NULL,
    [GINID]                     BIGINT           NULL,
    [LifeTimeReturn]            DECIMAL (38, 20) NULL,
    [CoachDraftClass]           INT              NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CUSTOMER_LIFE_MDM_CoachDraftClass]
    ON [dbo].[CUSTOMER_LIFE_MDM]([CoachDraftClass] ASC)
    INCLUDE([MedifastCustomerNumber]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_CUSTOMER_LIFE_MDM_GINID]
    ON [dbo].[CUSTOMER_LIFE_MDM]([GINID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_CUSTOMER_LIFE_MDM_LastInvoiceDate]
    ON [dbo].[CUSTOMER_LIFE_MDM]([LastInvoiceDate] ASC)
    INCLUDE([MedifastCustomerNumber], [Last Order Posting Group], [Last Order Type]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_CUSTOMER_LIFE_MDM_Updated_Date]
    ON [dbo].[CUSTOMER_LIFE_MDM]([Updated_Date] ASC)
    INCLUDE([MedifastCustomerNumber], [Blocked], [LifeTimeValue], [FirstInvoiceDate], [Life Invoice], [CustomerDraftClass], [LifeMonth], [FIBC], [FIBL], [Created_Date], [LastInvoiceDate], [Coach Tenure], [First Order Posting Group], [Last Order Posting Group], [First Order Type], [Last Order Type], [GINID], [LifeTimeReturn], [CoachDraftClass]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_CUSTOMER_LIFE_MDM_CoachDraftClass1]
    ON [dbo].[CUSTOMER_LIFE_MDM]([CoachDraftClass] ASC)
    INCLUDE([MedifastCustomerNumber], [Updated_Date]);


GO
CREATE NONCLUSTERED INDEX [IX_CUSTOMER_LIFE_MDM_FirstInvoiceDate]
    ON [dbo].[CUSTOMER_LIFE_MDM]([FirstInvoiceDate] ASC)
    INCLUDE([MedifastCustomerNumber], [Updated_Date], [First Order Posting Group], [First Order Type]);

