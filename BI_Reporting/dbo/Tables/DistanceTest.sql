﻿CREATE TABLE [dbo].[DistanceTest] (
    [CustomerPostingGroup] VARCHAR (50)    NULL,
    [SelltoCustomerNo]     VARCHAR (50)    NULL,
    [SelltoCustomerName]   VARCHAR (50)    NULL,
    [SelltoAddress]        VARCHAR (100)   NULL,
    [SelltoAddress2]       VARCHAR (50)    NULL,
    [SelltoCity]           VARCHAR (50)    NULL,
    [State]                VARCHAR (5)     NULL,
    [SelltoPostCode]       VARCHAR (50)    NULL,
    [ShiptoPostCode]       VARCHAR (50)    NULL,
    [CalYear]              VARCHAR (5)     NULL,
    [CalMonth]             INT             NULL,
    [ToatNetSales]         DECIMAL (18, 2) NULL,
    [Latitude]             DECIMAL (18, 6) NULL,
    [Longitude]            DECIMAL (18, 6) NULL
);

