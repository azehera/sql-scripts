﻿CREATE TABLE [dbo].[Jason Pharm$Sales Price] (
    [ReportDate]                   VARCHAR (10)     NULL,
    [timestamp]                    ROWVERSION       NOT NULL,
    [Item No_]                     VARCHAR (20)     COLLATE Latin1_General_CS_AS NOT NULL,
    [Sales Type]                   INT              NOT NULL,
    [Sales Code]                   VARCHAR (20)     COLLATE Latin1_General_CS_AS NOT NULL,
    [Starting Date]                DATETIME         NOT NULL,
    [Currency Code]                VARCHAR (10)     COLLATE Latin1_General_CS_AS NOT NULL,
    [Variant Code]                 VARCHAR (10)     COLLATE Latin1_General_CS_AS NOT NULL,
    [Unit of Measure Code]         VARCHAR (10)     COLLATE Latin1_General_CS_AS NOT NULL,
    [Minimum Quantity]             NUMERIC (38, 20) NOT NULL,
    [Unit Price]                   NUMERIC (38, 20) NOT NULL,
    [Price Includes VAT]           TINYINT          NOT NULL,
    [Allow Invoice Disc_]          TINYINT          NOT NULL,
    [VAT Bus_ Posting Gr_ (Price)] VARCHAR (10)     COLLATE Latin1_General_CS_AS NOT NULL,
    [Ending Date]                  DATETIME         NOT NULL,
    [Allow Line Disc_]             TINYINT          NOT NULL
);

