﻿CREATE TABLE [dbo].[mwcc] (
    [customer_id]      BIGINT          NULL,
    [Dollars]          DECIMAL (38, 2) NULL,
    [class_desc]       VARCHAR (50)    NOT NULL,
    [parent_id]        BIGINT          NULL,
    [ParentOfParentId] BIGINT          NULL,
    [CustomerName]     CHAR (50)       NULL,
    [email]            VARCHAR (100)   NULL,
    [Group_id]         BIGINT          NULL
);

