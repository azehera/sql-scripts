﻿CREATE TABLE [dbo].[Bank_A] (
    [Entity]           VARCHAR (19)     NOT NULL,
    [Bank Account No_] VARCHAR (20)     COLLATE Latin1_General_CS_AS NOT NULL,
    [AccountNumber]    VARCHAR (12)     NULL,
    [CheckNo]          VARCHAR (20)     COLLATE Latin1_General_CS_AS NOT NULL,
    [Amount]           DECIMAL (38, 20) NOT NULL,
    [CheckDate]        DATETIME         NOT NULL,
    [Vendor]           VARCHAR (20)     COLLATE Latin1_General_CS_AS NOT NULL
);

