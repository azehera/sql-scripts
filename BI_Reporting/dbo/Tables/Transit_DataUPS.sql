﻿CREATE TABLE [dbo].[Transit_DataUPS] (
    [Location]               VARCHAR (50) NULL,
    [Tracking Number]        VARCHAR (50) NULL,
    [Sales Order]            VARCHAR (50) NULL,
    [Status]                 VARCHAR (50) NULL,
    [Date Delivered]         VARCHAR (50) NULL,
    [Ship To City]           VARCHAR (50) NULL,
    [Ship To State Province] VARCHAR (50) NULL,
    [Service]                VARCHAR (50) NULL,
    [Ship To Postal Code]    VARCHAR (50) NULL
);

