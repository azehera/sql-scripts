﻿CREATE TABLE [dbo].[PossibleLapsedCustomers] (
    [USA_ID]            BIGINT          NOT NULL,
    [USA_FIRST_NM]      NVARCHAR (40)   NULL,
    [USA_LAST_NM]       NVARCHAR (40)   NULL,
    [USA_EMAIL]         NVARCHAR (250)  NULL,
    [USA_HOME_PH]       NVARCHAR (40)   NULL,
    [Ship-to Address]   VARCHAR (50)    NOT NULL,
    [Ship-to Address 2] VARCHAR (50)    NOT NULL,
    [Ship-to City]      VARCHAR (30)    NOT NULL,
    [Ship-to County]    VARCHAR (30)    NOT NULL,
    [Ship-to Post Code] VARCHAR (20)    NOT NULL,
    [PostingDate]       DATETIME        NULL,
    [Latitude]          NUMERIC (18, 6) NULL,
    [Longitude]         NUMERIC (18, 6) NULL
);

