﻿CREATE TABLE [dbo].[MEDIFAST_800_VDN_Grouping] (
    [VDN_ID]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [VDN_NBR]         NCHAR (4)     NOT NULL,
    [VDN_CALL_TO_NBR] NVARCHAR (50) NOT NULL,
    [VDN_GROUP]       NVARCHAR (50) NULL,
    CONSTRAINT [XPKMEDIFAST_800_VDN] PRIMARY KEY CLUSTERED ([VDN_ID] ASC) WITH (FILLFACTOR = 90),
    UNIQUE NONCLUSTERED ([VDN_NBR] ASC) WITH (FILLFACTOR = 90)
);

