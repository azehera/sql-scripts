﻿CREATE TABLE [dbo].[XL_023_MonthlySalesTracker_TY_PostingGroup_Backup] (
    [BusinessDayOfFiscalPeriod] INT              NULL,
    [CustomerPostingGroup]      VARCHAR (10)     NOT NULL,
    [Posting Date]              DATETIME         NULL,
    [Year]                      INT              NULL,
    [ActualRevenue]             DECIMAL (38, 20) NULL,
    [CummulativeRevenue]        DECIMAL (38, 20) NULL,
    [MonthlyBudget]             NUMERIC (38, 18) NOT NULL,
    [CalendarMonthID]           INT              NULL,
    [RemainingDays]             NUMERIC (16)     NULL,
    [RealDate]                  VARCHAR (50)     NULL,
    [CountBusinessDay]          INT              NULL,
    [Month]                     INT              NULL,
    [AvgDayRemaining]           NUMERIC (38, 18) NULL
);

