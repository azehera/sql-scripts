﻿CREATE TABLE [dbo].[ODYSSEY_RETURNS] (
    [RETURN_ID]                  NUMERIC (9)     NOT NULL,
    [USER_ID]                    NVARCHAR (25)   NULL,
    [ORDER_ID]                   NUMERIC (9)     NULL,
    [BUSINESS_CENTER]            NUMERIC (22)    NULL,
    [SAVED]                      NCHAR (1)       NULL,
    [FINISHED]                   NUMERIC (30)    NULL,
    [STATUS]                     NVARCHAR (30)   NULL,
    [RETURNED_DATE]              DATETIME2 (7)   NULL,
    [LAST_CHANGED]               DATETIME2 (7)   NULL,
    [REASON_CODE]                NVARCHAR (30)   NULL,
    [COST_CENTER]                NVARCHAR (30)   NULL,
    [CUSTOMER_ID]                NUMERIC (9)     NULL,
    [CURRENCY_CODE]              NVARCHAR (3)    NULL,
    [BC_SIDE]                    NUMERIC (1)     NULL,
    [EXCHANGE_RATE]              NUMERIC (18, 4) NULL,
    [SHIP_TO_GEOCODE]            NVARCHAR (11)   NULL,
    [SHIP_TO_IN_CITY]            NUMERIC (1)     NULL,
    [SHIP_FROM_GEOCODE]          NVARCHAR (11)   NULL,
    [SHIP_FROM_IN_CITY]          NUMERIC (1)     NULL,
    [ACCEPTANCE_GEOCODE]         NVARCHAR (11)   NULL,
    [ACCEPTANCE_IN_CITY]         NUMERIC (1)     NULL,
    [FREIGHT_AMOUNT]             NUMERIC (18, 4) NULL,
    [HANDLING_AMOUNT]            NUMERIC (18, 4) NULL,
    [FREIGHT_TAX_AMOUNT]         NUMERIC (18, 4) NULL,
    [HANDLING_TAX_AMOUNT]        NUMERIC (18, 4) NULL,
    [WAREHOUSE_ID]               NVARCHAR (10)   NULL,
    [CREDIT_TYPE]                NVARCHAR (10)   NULL,
    [CREDIT_AMOUNT]              NUMERIC (18, 4) NULL,
    [RETURN_LIMIT_TOTAL]         NUMERIC (18, 4) NULL,
    [TAX_CALC_METHOD]            NVARCHAR (1)    NULL,
    [TAX_COUNTRY]                NVARCHAR (2)    NULL,
    [RETURN_TYPE]                NVARCHAR (5)    NULL,
    [EXEMPT_FROM_PV]             NUMERIC (1)     NULL,
    [MAJOR_REASON_CODE]          NVARCHAR (2)    NULL,
    [MINOR_REASON_CODE]          NVARCHAR (10)   NULL,
    [TAX_REFUND_DATE]            DATETIME2 (7)   NULL,
    [TOTAL_REFUND_AMOUNT]        NUMERIC (18, 4) NULL,
    [TOTAL_VOLUME_AMOUNT]        NUMERIC (18, 4) NULL,
    [TOTAL_BONUS_AMOUNT]         NUMERIC (18, 4) NULL,
    [FINISHED_USER_ID]           NVARCHAR (10)   NULL,
    [TOTAL_TAX_AMOUNT]           NUMERIC (18, 4) NULL,
    [QA_REASON_CODE]             NVARCHAR (5)    NULL,
    [RECEIVE_USER_ID]            NVARCHAR (10)   NULL,
    [CREDIT_USER_ID]             NVARCHAR (10)   NULL,
    [PAYMENT_USER_ID]            NVARCHAR (10)   NULL,
    [EXPORTED_RETURN_ID]         NUMERIC (18)    NULL,
    [MASTER_ID]                  NVARCHAR (30)   NULL,
    [TOTAL_RETAIL_VOLUME_AMOUNT] NUMERIC (12, 2) NULL
);

