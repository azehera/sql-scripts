﻿CREATE TABLE [dbo].[WorkCalendar] (
    [CalendarDate]       DATE           NOT NULL,
    [CalendarYear]       INT            NOT NULL,
    [CalendarMonth]      INT            NOT NULL,
    [CalendarQuarter]    INT            NOT NULL,
    [CalendarMonthID]    INT            NOT NULL,
    [CalendarDay]        INT            NOT NULL,
    [CalendarWeek]       INT            NOT NULL,
    [DayOfYear]          INT            NOT NULL,
    [DayOfWeekName]      VARCHAR (10)   NOT NULL,
    [FirstDateOfWeek]    DATETIME       NOT NULL,
    [LastDateOfWeek]     DATETIME       NOT NULL,
    [FirstDateOfMonth]   DATETIME       NOT NULL,
    [LastDateOfMonth]    DATETIME       NOT NULL,
    [FirstDateOfQuarter] DATETIME       NOT NULL,
    [LastDateOfQuarter]  DATETIME       NOT NULL,
    [FirstDateOfYear]    DATETIME       NOT NULL,
    [LastDateOfYear]     DATETIME       NOT NULL,
    [Weekend]            BIT            NOT NULL,
    [Weekday]            BIT            NOT NULL,
    [Workday]            DECIMAL (3, 2) NULL,
    [CalendarMonthName]  CHAR (15)      NULL,
    CONSTRAINT [PK_WorkCalendar] PRIMARY KEY CLUSTERED ([CalendarDate] ASC) WITH (FILLFACTOR = 90)
);

