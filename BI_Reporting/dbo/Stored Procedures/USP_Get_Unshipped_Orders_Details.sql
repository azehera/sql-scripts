﻿

/*
--==================================================================================
--Author:         Daniel D.
--Create date: 05/29/2019

--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--[NAVPROD]		        [Jason Pharm$Sales Header]				  Select                     
--==================================================================================
--REVISION LOG
--Date           Name               Change
------------------------------------------------------------------------------------
06-18-2019		Micah Williams		
6-26-2019 - Per Judy's request, removed DNS shipment status
08-24-2021 - Askahay H - Updates as per ITSD1429 - Changes for FTW
--======================================================================================
--NOTES:
----------------------------------------------------------------------------------------
This sproc is used to pull unshipped ordes' count from different distribution center.
--======================================================================================
*/

CREATE PROCEDURE [dbo].[USP_Get_Unshipped_Orders_Details]
AS
SET NOCOUNT ON;

SELECT DISTINCT (No_) AS 'Sales Order Number',
       [Order Date],
       [Location Code] DC,
    [External Document No_] AS 'Order Number',
    CASE WHEN [Sell-to Customer No_] LIKE 'TSFL-%' THEN SUBSTRING([Sell-to Customer No_],6,20)
                     WHEN [Sell-to Customer No_] LIKE 'TSFL%' THEN SUBSTRING([Sell-to Customer No_],5,20)
                                  ELSE [Sell-to Customer No_] END AS [Sell-to Customer No_],
  C.Email
  ,sh.[Customer Posting Group]
   ,sh.[Ship-to County] AS 'Ship State'
   ,sh.[Ship-to Post Code] AS 'Ship Zip'
   ,z1.Latitude [Ship Latitude]
   ,z1.Longitude [Ship Longitude]
   ,CASE WHEN sh.[Location Code] ='MDCWMS' THEN '21660' WHEN sh.[Location Code] = 'RDCWMS' THEN '89434' WHEN sh.[Location Code] = 'HDGWMS' THEN '21660' WHEN sh.[Location Code] = 'FTWWMS' THEN '76006' END AS 'WarehouseZip' 
   ,z2.Latitude  [Warehouse Latitude]
   ,z2.Longitude [Warehouse Longitude]
   ,z1.State AS ZipDeluxeState
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Header] SH (NOLOCK)
LEFT JOIN EXIGO.ExigoSyncSQL_PRD.CUSTOM.Order_Header OH ON OH.Order_No = SH.[External Document No_] COLLATE DATABASE_DEFAULT
LEFT JOIN EXIGO.ExigoSyncSQL_PRD.dbo.customers C ON C.Field1 = OH.Medifast_Customer_Number
 LEFT JOIN [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] z1 (NOLOCK) ON LEFT(sh.[Ship-to Post Code],5) COLLATE DATABASE_DEFAULT  = z1.[ZipCode] AND sh.[Ship-to County] COLLATE DATABASE_DEFAULT  = z1.[State] AND z1.PrimaryRecord = 'P'
 LEFT JOIN Zip_codes_deluxe.dbo.ZipCodeDatabase_DELUXE z2 (NOLOCK) ON z2.ZipCode = CASE WHEN sh.[Location Code] ='MDCWMS' THEN '21660' WHEN sh.[Location Code] = 'RDCWMS' THEN '89434' WHEN sh.[Location Code] = 'HDGWMS' THEN '21660' WHEN sh.[Location Code] = 'FTWWMS' THEN '76006' END  AND z2.PrimaryRecord = 'P'
WHERE [Document Type] = 1 AND SH.[Shipment Method Code] <> 'DNS'
ORDER BY [Location Code],
         [Order Date];

