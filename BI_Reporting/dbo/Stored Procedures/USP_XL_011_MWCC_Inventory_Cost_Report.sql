﻿




/*
--==================================================================================
Author:         Luc Emond
Create date: 08/14/2012
---------------------------[USP_XL_011_MWCC_Inventory_Cost_Report]------------------
Provide Daily Sales Transaction by MWCC location with their cost-----
--=================================================================================    
REFERENCES
Database              Table/View/UDF                       Action            
------------------------------------------------------------------------------------
BI_Reporting			   [Calendar_TSFL]				              Select
NAV-ETL            [Jason Pharm$Sales Invoice Line]		  Select 
NAV-ETL            [Jason Pharm$Sales Invoice Header]	  Select
NAV-ETL            [Jason Pharm$Item]	            	  Select  
NAV-ETL            [Jason Pharm$Sales Cr_Memo Line]		  Select 
NAV-ETL            [Jason Pharm$Sales Cr_Memo Header]	  Select
Book4Time_ETL      [B4T_product_master]                   Select
                        
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------
2012-12-04   Luc Emond                Removed Prefix [DP-BIDB]
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
--*/

CREATE PROCEDURE [dbo].[USP_XL_011_MWCC_Inventory_Cost_Report] As
SET NOCOUNT ON;

--EXECUTE AS LOGIN = 'ReportViewer'
------DECLARE DATE FOR REPORT-----------------------
DECLARE @DATE1 DATETIME, @DATE2 DATETIME

SET @DATE1 = (SELECT FirstDateOfYear FROM dbo.calendar where CalendarDate = (select Convert (varchar(10), GetDate()-1,121)))
SET @DATE2 = (SELECT CalendarDate FROM dbo.calendar where CalendarDate = (select Convert (varchar(10), GetDate()-1,121)))
IF object_id('tempdb..#A') is not null DROP TABLE #A

------MASTER TABLE------
IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master
CREATE TABLE #Master(
	[Posting Date] [varchar](10) NULL,
	[CalendarMonth] [int] NULL,
	[CalendarQuarter] [int] NULL,
	[Customer Posting Group] [varchar](10) NULL,
	[Sell-to Customer No_] [varchar](20) NULL,
	[Sell-to Customer Name] [varchar](50) NULL,
	[External Document No_] [varchar](20) NULL,
	[Document No_] [varchar](20) NULL,
	[Category] [char](25) NULL,
	[No_] [varchar](20) NULL,
	[Description] [varchar](30) NULL,
	[Units] [numeric](38, 18) NULL,
	[StandardCost] [numeric](38, 20) NULL,
	[Debit Extended Cost] [numeric](38, 6) NULL,
	[Credit Extended Cost] [numeric](38, 6) NULL,
	[Net Extended Cost] [numeric](38, 6) NULL
)

--------------------------------------PULL SALES ACTIVITY--------------------
INSERT INTO #MASTER
SELECT 
 Convert (varchar(10), A.[Posting Date],121) As [Posting Date]
,DATEPART(MM,A.[Posting Date]) As CalendarMonth
,DATEPART(QQ,A.[Posting Date]) As CalendarQuarter
,B.[Customer Posting Group]
,B.[Sell-to Customer No_]
,B.[Sell-to Customer Name]
,B.[External Document No_]
,A.[Document No_]
,'Inventory' As Category
,A.[No_]
,C.[Description]
,A.[Quantity (Base)] As Units
,C.[Unit Cost] As StandardCost
,A.[Quantity (Base)] * C.[Unit Cost] As [Debit Extended Cost]
,0 As [Credit Extended Cost]
,A.Type
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A 
INNER JOIN 
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 
      ON A.[Document No_]= B.[No_]
LEFT OUTER JOIN 
    [NAV_ETL].[dbo].[Jason Pharm$Item] C
    ON A.[No_] COLLATE Latin1_General_CI_AS = C.[No_] COLLATE Latin1_General_CI_AS
WHERE A.[Posting Date]>= @Date1 
  and A.[Posting Date] <= @Date2 
  and B.[Customer Posting Group] ='CORPCLINIC'

UNION ALL
---------------------------------PULL CREDIT ACTIVITY-----------------------------
SELECT 
 Convert (varchar(10), A.[Posting Date],121) As [Posting Date]
,DATEPART(MM,A.[Posting Date]) As CalendarMonth
,DATEPART(QQ,A.[Posting Date]) As CalendarQuarter
,B.[Customer Posting Group]
,B.[Sell-to Customer No_]
,B.[Sell-to Customer Name]
,B.[External Document No_]
,A.[Document No_]
,'Inventory' As Category
,A.[No_]
,C.[Description]
,A.[Quantity (Base)] *-1 As Units
,C.[Unit Cost] As StandardCost
,0 As [Debit Extended Cost]
,(A.[Quantity (Base)] * C.[Unit Cost])*-1 As [Credit Extended Cost]
,A.Type
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line] A 
INNER JOIN 
     [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] B 
     ON A.[Document No_]= B.[No_]
LEFT OUTER JOIN 
    [NAV_ETL].[dbo].[Jason Pharm$Item] C
    ON A.[No_] COLLATE Latin1_General_CI_AS = C.[No_] COLLATE Latin1_General_CI_AS
WHERE A.[Posting Date]>= @Date1 
  and A.[Posting Date] <= @Date2 
  and B.[Customer Posting Group] ='CORPCLINIC'

----UPDATE PASS DATA TO CORRECT STORE ID NAME----	
Update #Master Set [Sell-to Customer Name] = 'MWCC - Rialto #203' where [Sell-to Customer No_] ='203'

----FIX THE TRANSCATION TO BE INVENTORY OR NON-INVENTORY--AS PER LEI EVERYTHING AT "0" FOR STANDARD COST IN B4T ARE CONSIDERED NON-INVENTORY ITEMS------------- 
IF object_id('Tempdb..#AA') Is Not Null DROP TABLE #AA
Select sku, retail_price 
Into #AA
From [Book4Time_ETL].[dbo].[B4T_product_master] 
Where retail_price =0

UPDATE #Master SET Category ='Non-Inventory'
Where #Master.[No_] in (Select Sku from #AA)

-----final view for report----
Select
[Posting Date]
,CalendarMonth
,CalendarQuarter
,[Customer Posting Group]
,[Sell-to Customer No_]
,[Sell-to Customer Name]
,[External Document No_]
,[Document No_]
,[Category]
,[No_]
,[Description]
,[Units]
,[StandardCost]
,[Debit Extended Cost]
,[Credit Extended Cost]
,[Debit Extended Cost] + [Credit Extended Cost] As [Net Extended Cost]
From #Master
Order By [Posting Date]





