﻿


/*
=============================================================================================
Author:         Menkir Haile
Create date: Date,10/31/2014
-------------------------[usp_APAudit$25KPlus] -----------------------------
---Sales Invoice Equal or Greater than $25,000.00 which Require VP’s signature----
=============================================================================================   
REFERENCES
Database              Table/View/UDF								Action            
---------------------------------------------------------------------------------------------
[NAV_ETL]       [dbo].[Jason Pharm$Purch_ Inv_ Header]				Select   
[NAV_ETL]       [dbo].[Jason Pharm$Purch_ Inv_ Line]				Select
[NAV_ETL]		[dbo].[Jason Pharm$Vendor Ledger Entry]				Select
[NAV_ETL]		[dbo].[Jason Properties$Purch_ Inv_ Header]			Select
[NAV_ETL]		[dbo].[Jason Properties$Purch_ Inv_ Line]			Select
[NAV_ETL]		[dbo].[Jason Properties$Vendor Ledger Entry]		Select
[NAV_ETL]		[dbo].[Medifast Inc$Purch_ Inv_ Header]				Select
[NAV_ETL]		[dbo].[Medifast Inc$Purch_ Inv_ Line]				Select
[NAV_ETL]		[dbo].[Medifast Inc$Vendor Ledger Entry]	        Select
[NAV_ETL]		[dbo].[Take Shape For Life$Purch_ Inv_ Header]		Select
[NAV_ETL]		[dbo].[Take Shape For Life$Purch_ Inv_ Line]		Select
[NAV_ETL]		[dbo].[Take Shape For Life$Vendor Ledger Entry]		Select
[NAV_ETL]		[dbo].[Medifast Nutrition$Purch_ Inv_ Header]		Select
[NAV_ETL]		[dbo].[Medifast Nutrition$Purch_ Inv_ Line]			Select
[NAV_ETL]		[dbo].[Medifast Nutrition$Vendor Ledger Entry]		Select

===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------

==========================================================================================
*/
CREATE PROCEDURE [dbo].[usp_APAudit$25KPlus] 

AS
SET NOCOUNT ON;

BEGIN

SELECT 
	  'Jason Pharm' COLLATE DATABASE_DEFAULT AS Entity 
	  ,CAST(H.[Posting Date]  AS DATE) AS [Posting Date]
	  ,H.[Buy-from Vendor Name]COLLATE DATABASE_DEFAULT  AS [Vendor Name]
	  ,CAST(H.[Document Date] AS DATE) AS [Document Date]
	  ,V.[External Document No_] COLLATE DATABASE_DEFAULT AS [External Document No]
      ,CAST(SUM(L.Amount) AS MONEY) AS [ Invoice Total]
		  
FROM [NAV_ETL].[dbo].[Jason Pharm$Purch_ Inv_ Header] H
JOIN [NAV_ETL].dbo.[Jason Pharm$Purch_ Inv_ Line] L
    ON L.[Document No_] = H.[No_]
JOIN [NAV_ETL].[dbo].[Jason Pharm$Vendor Ledger Entry] V
	ON V.[Document No_] COLLATE DATABASE_DEFAULT = L.[Document No_]
WHERE CAST(H.[Posting Date] AS DATE) = CAST(GETDATE() - 1 AS DATE)
GROUP BY H.[Posting Date]
			,H.[Buy-from Vendor Name]
			,H.[Document Date]
			,V.[External Document No_]
HAVING SUM(L.Amount) >= 25000

UNION

 SELECT 
	  'Jason Properties' COLLATE DATABASE_DEFAULT as Entity 
	  ,Cast(H.[Posting Date] as date) as [Posting Date]
	  ,H.[Buy-from Vendor Name] COLLATE DATABASE_DEFAULT AS [Vendor Name]
	  ,CAST(H.[Document Date] AS Date) AS [Document Date]
	  ,V.[External Document No_] COLLATE DATABASE_DEFAULT as [External Document No]
      ,cast(SUM(L.Amount) as Money) as [ Invoice Total]
	    
FROM [NAV_ETL].[dbo].[Jason Properties$Purch_ Inv_ Header] H
JOIN [NAV_ETL].[dbo].[Jason Properties$Purch_ Inv_ Line] L
	ON L.[Document No_] = H.[No_]
Left Join [NAV_ETL].[dbo].[Jason Properties$Vendor Ledger Entry] V
	ON V.[Document No_] collate database_default = L.[Document No_]
Where cast(H.[Posting Date] as date) = cast(getdate() - 1 as date)
Group by H.[Posting Date]
			,H.[Buy-from Vendor Name]
			,H.[Document Date]
			,V.[External Document No_]
HAVING SUM(L.Amount) >= 25000

UNION

SELECT 
      'Medifast Inc' COLLATE DATABASE_DEFAULT as Entity 
	  ,Cast(H.[Posting Date] as date) as [Posting Date]
	  ,H.[Buy-from Vendor Name] COLLATE DATABASE_DEFAULT AS [Vendor Name]
	  ,CAST(H.[Document Date] AS Date) AS [Document Date]
	  ,V.[External Document No_] COLLATE DATABASE_DEFAULT as [External Document No]
      ,cast(SUM(L.Amount) as Money) as [ Invoice Total]
	    
FROM [NAV_ETL].[dbo].[Medifast Inc$Purch_ Inv_ Header] H
JOIN [NAV_ETL].[dbo].[Medifast Inc$Purch_ Inv_ Line] L
	ON L.[Document No_] = H.[No_]
Left Join [NAV_ETL].[dbo].[Medifast Inc$Vendor Ledger Entry] V
	ON V.[Document No_] collate database_default = L.[Document No_]
Where cast(H.[Posting Date] as date) = cast(getdate() - 1 as date)
Group by H.[Posting Date]
			,H.[Buy-from Vendor Name]
			,H.[Document Date]
			,V.[External Document No_]
HAVING SUM(L.Amount) >= 25000

UNION

SELECT 
      'TSFL' COLLATE DATABASE_DEFAULT as Entity 
	  ,Cast(H.[Posting Date] as date) as [Posting Date]
	  ,H.[Buy-from Vendor Name] COLLATE DATABASE_DEFAULT AS [Vendor Name]
	  ,CAST(H.[Document Date] AS Date) AS [Document Date]
	  ,V.[External Document No_] COLLATE DATABASE_DEFAULT as [External Document No]
      ,cast(SUM(L.Amount) as Money) as [ Invoice Total]
	    
FROM [NAV_ETL].[dbo].[Take Shape For Life$Purch_ Inv_ Header] H
JOIN [NAV_ETL].[dbo].[Take Shape For Life$Purch_ Inv_ Line] L
	ON L.[Document No_] = H.[No_]
Left Join [NAV_ETL].[dbo].[Take Shape For Life$Vendor Ledger Entry] V
	ON V.[Document No_] collate database_default = L.[Document No_]
Where cast(H.[Posting Date] as date) = cast(getdate() - 1 as date)
Group by H.[Posting Date]
			,H.[Buy-from Vendor Name]
			,H.[Document Date]
			,V.[External Document No_]
HAVING SUM(L.Amount) >= 25000

UNION

SELECT 
	  'Medifast Nutrition' COLLATE DATABASE_DEFAULT as Entity 
	  ,Cast(H.[Posting Date] as date) as [Posting Date]
	  ,H.[Buy-from Vendor Name] COLLATE DATABASE_DEFAULT AS [Vendor Name]
	  ,CAST(H.[Document Date] AS Date) AS [Document Date]
	  ,V.[External Document No_] COLLATE DATABASE_DEFAULT as [External Document No]
      ,cast(SUM(L.Amount) as Money) as [ Invoice Total]
	    
FROM [NAV_ETL].[dbo].[Medifast Nutrition$Purch_ Inv_ Header] H
JOIN [NAV_ETL].[dbo].[Medifast Nutrition$Purch_ Inv_ Line] L
      ON L.[Document No_] = H.[No_]
Left Join [NAV_ETL].[dbo].[Medifast Nutrition$Vendor Ledger Entry] V
		ON V.[Document No_] collate database_default = L.[Document No_]
Where cast(H.[Posting Date] as date) = cast(getdate() - 1 as date)
Group by H.[Posting Date]
			,H.[Buy-from Vendor Name]
			,H.[Document Date]
			,V.[External Document No_]
HAVING SUM(L.Amount) >= 25000

ORDER BY Entity
		,[Vendor Name]
	         
END

