﻿/*
--=======================================================================================
Author:         Luc Emond
Create date: 04/15/2013
----------------------------------[USP_XL_034_P&L_Trends_Data]---------------------------

--=======================================================================================  
REFERENCES
Database              Table/View/UDF                            Action            
-----------------------------------------------------------------------------------------
[BI_SSAS_Cubes]       [dbo].[FactGL_cube]                       Select      
[BI_SSAS_Cubes]       [dbo].[DimEntity]                         Select
[BI_SSAS_Cubes]       dbo.DimGLCategory                         Select
[BI_Reporting]        [dbo].[XL_034_P&L_Trends_Data]            Insert
[BI_Reporting]        [dbo].[XL_034_P&L_Trends_DataWOAllocation]Insert

========================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------------

========================================================================================
NOTES:
----------------------------------------------------------------------------------------
2013-10-09   Luc Emond               Added AccountsCategory = 2 to provide P&L Only
========================================================================================	
*/	


CREATE Procedure [dbo].[USP_XL_034_P&L_Trends_Data] As 

Set NoCount On;

-----------Begin----------
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT 
Distinct[CalendarYear]
,CalendarQuarter
,[CalendarMonthID]
,[CalendarMonthName]
Into #A
From [BI_SSAS_Cubes].[dbo].[DimCalendar]


IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
SELECT
 [EntityName]
,a.[CalendarMonthID]
,[CalendarYear]
,CalendarQuarter
,[CalendarMonthName]
,a.[G_L Account No_]
,[G_L Name]
,RollUpName
,[Source Code]
,a.[Global Dimension 2 Code]
,Case When [Global Dimension 2 Code] in (Select BusinessUnits from BI_SSAS_Cubes.dbo.DimBusinessUnits  where BusinessCategory='CHANNEL') then [Global Dimension 2 Code] else 'Other' end as Channel
,Sum([NetTotal]) as Actual
,Sum([Budget$]) as Budget
Into #B
From [BI_SSAS_Cubes].[dbo].[FactGL_cube] a
Join [BI_SSAS_Cubes].[dbo].[DimEntity] b
     on a.[EntryNO]=b.EntityID
Join [BI_SSAS_Cubes].dbo.DimGLCategory c
     on a.[G_L Account No_]=c.[G_L Account No_]
Join #A e
    on a.CalendarMonthID=e.[CalendarMonthID]
Where a.CalendarMonthID>=201201
  And AccountsCategory=2-----Added Luc Emond October 09 2013----
Group By
 [EntityName]
,a.[CalendarMonthID]
,a.[G_L Account No_]
,[Source Code]
,[G_L Name]
,RollUpName
,[Global Dimension 2 Code]
,[CalendarYear]
,CalendarQuarter
,[CalendarMonthName]
Order By 
 a.[G_L Account No_]
,a.[CalendarMonthID]

----------------------------------------------------------------------------------------------------------------
Truncate Table [BI_Reporting].[dbo].[XL_034_P&L_Trends_Data]
Declare @FirstDateOfLastYear varchar(10)
Declare @FirstDateOfThisYear Varchar(10)
Declare @LastDateOfLastYear Varchar(10)
Declare @LastDateOfThisYear Varchar(10)
 
Set  @FirstDateOfLastYear=(SELECT distinct CalendarMonthID
From [BI_SSAS_Cubes].[dbo].[DimCalendar]
Where CalendarDate=(SELECT DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0)))
  
Set @FirstDateOfThisYear=(SELECT distinct CalendarMonthID
From [BI_SSAS_Cubes].[dbo].[DimCalendar]
Where CalendarDate=(SELECT DATEADD(yy, DATEDIFF(yy,0,getdate()), 0)))
  
Set @LastDateOfLastYear=(SELECT distinct [CalendarMonthID]
FROM [BI_SSAS_Cubes].[dbo].[DimCalendar]
Where CalendarDate=(Select DATEADD(MONTH, DATEDIFF(MONTH, '19000101', GETDATE()-365), '18991231')))---365
  
Set @LastDateOfThisYear=(SELECT distinct [CalendarMonthID]
From [BI_SSAS_Cubes].[dbo].[DimCalendar]
Where CalendarDate=(Select DATEADD(MONTH, DATEDIFF(MONTH, '19000101', GETDATE()), '18991231')))---Remove -30
  

  
Insert  into [BI_Reporting].[dbo].[XL_034_P&L_Trends_Data]
Select
[EntityName]
,[CalendarMonthID]
,[CalendarYear]
,CalendarQuarter
,[CalendarMonthName]
,[G_L Account No_]
,Case When [G_L Account No_] between '40150' and '43499' OR [G_L Account No_] between '43501' and '45099' then 'INVOICED SALES'
      When [G_L Account No_] between '49951' and '49955' OR [G_L Account No_] between '62601' and '63000'  then 'NET SHIPPING REVENUE'
      When [G_L Account No_] between '47001' and '49950' OR [G_L Account No_] between '49970' and '49975' then 'NET DISCOUNTS'
      When [G_L Account No_] between '49956' and '49969' OR [G_L Account No_] between '49976' and '49998' then 'NET RETURNS'
      When [G_L Account No_] between '45100' and '47000' OR [G_L Account No_] between '40001' and '40149' OR [G_L Account No_]= '43500'   then 'OTHER REVENUE'
      When [G_L Account No_] between '50001' and '51999' then 'Cost AT STANDARD'
      When [G_L Account No_] between '53000' and '57999' OR [G_L Account No_] between '59000' and '59987' then 'VARIANCES'
      When [G_L Account No_] between '58000' and '58999' OR [G_L Account No_] between '52000' and '52999' then 'INVENTORY ADJUSTMENTS'
      When [G_L Account No_] between '60002' and '62499' OR [G_L Account No_] between '63001' and '64998'  or [G_L Account No_]='60001' then 'SHIPPING COST'
      When [G_L Account No_] in ('62500') then 'PROGRAM EXPENSES'
      When [G_L Account No_] between '65000' and '69996' then 'OTHER MANUF EXP'
      When [G_L Account No_] between '70126' and '70699' or [G_L Account No_] between '70701' and '74998' OR [G_L Account No_] in ('70001','97002') then 'PERSONNEL COSTS'
      When [G_L Account No_] between '70001' and '70125' or [G_L Account No_] IN ('70700') then 'COMMISSION EXP'
      When [G_L Account No_] between '75000' and '75998' then 'SALES & MARKETING'
      When [G_L Account No_] between '76000' and '79998'  then 'COMMUNICATION EXP'
      When [G_L Account No_] between '86800' and '86850' OR [G_L Account No_] In ('86200') then 'AMORT & DEPRECIATION EXP'
      When [G_L Account No_] between '83000' and '85998' then 'TOTAL OFFICE EXP'
      When [G_L Account No_]between '80000' and '82998' OR [G_L Account No_] between '86000' and '86199' or [G_L Account No_] between '86201' and '86799' or [G_L Account No_] between '86851' and '89996'   then 'TOTAL OTHER EXPENSES'
      When [G_L Account No_] between '92000' and '93000' or [G_L Account No_] between '97003' and '99997' then 'TOTAL INTREST INCOME(EXP)'
      When  [G_L Account No_] between '90001' and '91999' or [G_L Account No_] between '93001' and '94998' then 'TOTAL OTHER INCOME(EXP)'
      When  [G_L Account No_] between '95001' and '97001' or [G_L Account No_] between '97003' and '99997' then 'TOTAL TAX EXPENSE' else 'Unknown' end as AccountGroupName                    
,[G_L Name]
,RollUpName
,[Source Code]
,Channel
,Actual
,Budget
,Case When CalendarMonthID between @FirstDateOfLastYear and @LastDateOfLastYear then Actual else 0 end  AS ActualLastYear
,Case when CalendarMonthID between @FirstDateOfThisYear and @LastDateOfThisYear then Actual else 0 end  AS ActualThisYear 
,Case when CalendarMonthID between @FirstDateOfLastYear and @LastDateOfLastYear then Budget else 0 end  AS BudgetLastYear
,Case when CalendarMonthID between @FirstDateOfThisYear and @LastDateOfThisYear then Budget else 0 end  AS BudgetThisYear 
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='January' Then Actual else 0 end as JanuaryTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='January' Then Actual else 0 end as JanuaryLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='February' Then Actual else 0 end as FebruaryTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='February' Then Actual else 0 end as FebruaryLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='March' Then Actual else 0 end as MarchTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='March' Then Actual else 0 end as MarchLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='April' Then Actual else 0 end as AprilTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='April' Then Actual else 0 end as AprilLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='May' Then Actual else 0 end as MayTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='May' Then Actual else 0 end as MayLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='June' Then Actual else 0 end as JuneTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='June' Then Actual else 0 end as JuneLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='July' Then Actual else 0 end as JulyTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='July' Then Actual else 0 end as JulyLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='August' Then Actual else 0 end as AugustTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='August' Then Actual else 0 end as AugustLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='September' Then Actual else 0 end as SeptemberTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='September' Then Actual else 0 end as SeptemberLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='October' Then Actual else 0 end as OctoberTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='October' Then Actual else 0 end as OctoberLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='November' Then Actual else 0 end as NovemberTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='November' Then Actual else 0 end as NovemberLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='December' Then Actual else 0 end as DecemberTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='December' Then Actual else 0 end as DecemberLY
From #B
Where CalendarMonthID between @FirstDateOfLastYear and @LastDateOfLastYear 
or CalendarMonthID between @FirstDateOfThisYear and @LastDateOfThisYear


----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
Truncate Table [BI_Reporting].[dbo].[XL_034_P&L_Trends_DataWOAllocation]
Insert Into [BI_Reporting].[dbo].[XL_034_P&L_Trends_DataWOAllocation]
Select
 [EntityName]
,[CalendarMonthID]
,[CalendarYear]
,CalendarQuarter
,[CalendarMonthName]
,[G_L Account No_]
,Case When [G_L Account No_] between '40150' and '43499' OR [G_L Account No_] between '43501' and '45099' then 'INVOICED SALES'
      When [G_L Account No_] between '49951' and '49955' OR [G_L Account No_] between '62601' and '63000'  then 'NET SHIPPING REVENUE'
      When [G_L Account No_] between '47001' and '49950' OR [G_L Account No_] between '49970' and '49975' then 'NET DISCOUNTS'
      When [G_L Account No_] between '49956' and '49969' OR [G_L Account No_] between '49976' and '49998' then 'NET RETURNS'
      When [G_L Account No_] between '45100' and '47000' OR [G_L Account No_] between '40001' and '40149' OR [G_L Account No_]= '43500'   then 'OTHER REVENUE'
      When [G_L Account No_] between '50001' and '51999' then 'Cost AT STANDARD'
      When [G_L Account No_] between '53000' and '57999' OR [G_L Account No_] between '59000' and '59987' then 'VARIANCES'
      When [G_L Account No_] between '58000' and '58999' OR [G_L Account No_] between '52000' and '52999' then 'INVENTORY ADJUSTMENTS'
      When [G_L Account No_] between '60002' and '62499' OR [G_L Account No_] between '63001' and '64998'  or [G_L Account No_]='60001' then 'SHIPPING COST'
      When [G_L Account No_] in ('62500') then 'PROGRAM EXPENSES'
      When [G_L Account No_] between '65000' and '69996' then 'OTHER MANUF EXP'
      When [G_L Account No_] between '70126' and '70699' or [G_L Account No_] between '70701' and '74998' OR [G_L Account No_] in ('70001','97002') then 'PERSONNEL COSTS'
      When [G_L Account No_] between '70001' and '70125' or [G_L Account No_] IN ('70700') then 'COMMISSION EXP'
      When [G_L Account No_] between '75000' and '75998' then 'SALES & MARKETING'
      When [G_L Account No_] between '76000' and '79998'  then 'COMMUNICATION EXP'
      When [G_L Account No_] between '86800' and '86850' OR [G_L Account No_] In ('86200') then 'AMORT & DEPRECIATION EXP'
      When [G_L Account No_] between '83000' and '85998' then 'TOTAL OFFICE EXP'
      When [G_L Account No_]between '80000' and '82998' OR [G_L Account No_] between '86000' and '86199' or [G_L Account No_] between '86201' and '86799' or [G_L Account No_] between '86851' and '89996'   then 'TOTAL OTHER EXPENSES'
      When [G_L Account No_] between '92000' and '93000' or [G_L Account No_] between '97003' and '99997' then 'TOTAL INTREST INCOME(EXP)'
      When  [G_L Account No_] between '90001' and '91999' or [G_L Account No_] between '93001' and '94998' then 'TOTAL OTHER INCOME(EXP)'
      When  [G_L Account No_] between '95001' and '97001' or [G_L Account No_] between '97003' and '99997' then 'TOTAL TAX EXPENSE' else 'Unknown' end as AccountGroupName                  
,[G_L Name]
,RollUpName
,[Source Code]
,Channel
,Actual
,Budget
,Case When CalendarMonthID between @FirstDateOfLastYear and @LastDateOfLastYear then Actual else 0 end  AS ActualLastYear
,Case When CalendarMonthID between @FirstDateOfThisYear and @LastDateOfThisYear then Actual else 0 end  AS ActualThisYear 
,Case when CalendarMonthID between @FirstDateOfLastYear and @LastDateOfLastYear then Budget else 0 end  AS BudgetLastYear
,Case when CalendarMonthID between @FirstDateOfThisYear and @LastDateOfThisYear then Budget else 0 end  AS BudgetThisYear 
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='January' Then Actual else 0 end as JanuaryTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='January' Then Actual else 0 end as JanuaryLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='February' Then Actual else 0 end as FebruaryTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='February' Then Actual else 0 end as FebruaryLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='March' Then Actual else 0 end as MarchTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='March' Then Actual else 0 end as MarchLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='April' Then Actual else 0 end as AprilTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='April' Then Actual else 0 end as AprilLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='May' Then Actual else 0 end as MayTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='May' Then Actual else 0 end as MayLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='June' Then Actual else 0 end as JuneTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='June' Then Actual else 0 end as JuneLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='July' Then Actual else 0 end as JulyTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='July' Then Actual else 0 end as JulyLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='August' Then Actual else 0 end as AugustTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='August' Then Actual else 0 end as AugustLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='September' Then Actual else 0 end as SeptemberTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='September' Then Actual else 0 end as SeptemberLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='October' Then Actual else 0 end as OctoberTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='October' Then Actual else 0 end as OctoberLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='November' Then Actual else 0 end as NovemberTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='November' Then Actual else 0 end as NovemberLY
,Case When CalendarYear = Year(Getdate()) and CalendarMonthName ='December' Then Actual else 0 end as DecemberTY
,Case When CalendarYear = Year(Getdate())-1 and CalendarMonthName ='December' Then Actual else 0 end as DecemberLY
From #B
Where CalendarMonthID between @FirstDateOfLastYear and @LastDateOfLastYear 
or CalendarMonthID between @FirstDateOfThisYear and @LastDateOfThisYear


Delete From [BI_Reporting].[dbo].[XL_034_P&L_Trends_DataWOAllocation]
Where [G_L Account No_] in ('59980','64998','69996','74998','75998','79998','85998', '89996')



  


  
  





