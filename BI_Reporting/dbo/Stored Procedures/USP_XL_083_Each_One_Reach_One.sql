﻿
CREATE procedure [dbo].[USP_XL_083_Each_One_Reach_One] as

Declare @Sql1 varchar(2000)
Declare @Sql2 varchar(2000)
Declare @Today date
Declare @StartDate date
Declare @EndDate date
Declare @HCID varchar(20)
Declare @LastHCID varchar(20)
Declare @HCNo varchar(20)
Declare @LastHCNo varchar(20)
Declare @FirstName varchar(30)
Declare @LastFirstName varchar(30)
Declare @LastName varchar(30)
Declare @LastLastName varchar(30)
Declare @CustNo varchar(20)
Declare @LastCustNo varchar(20)
Declare @OrderNum varchar(20)
Declare @LastOrderNum varchar(20)
Declare @OrderDate datetime
Declare @LastOrderDate datetime
Declare @TotalPV money
Declare @PVAmount money
Declare @GoToNext char(1)
Declare @OrderNo varchar(20)
Declare @ClientEntry datetime
Declare @LastClientEntry datetime
Set @Today = GETDATE()
Set @StartDate = '02/01/2015'

-- Create the table to receive the results for each month
IF object_id('Tempdb..#EOROFinal') Is Not Null DROP TABLE #EOROFinal
CREATE TABLE #EOROFinal (EarnedMonth varchar(7), [BC ID] varchar(20), [BC First Name] varchar(30), [BC Last Name] varchar(30), [BC Activation] varchar(10), 
[BC Recognition Name] varchar(50), [BC Email] varchar(200), [HC ID] varchar(20), [HC First Name] varchar(30), [HC Last Name] varchar(30), [HC Activation] varchar(10),
[HCOrderDate] varchar(10), [HCOrderNo] varchar(20), [HCPVAmount] money, [HCCustCount] integer, [ClientID] varchar(20), [ClientFirst] varchar(30), [ClientLast] varchar(30), 
[ClientEntry] varchar(10), [ClientOrderNo] varchar(20), [ClientOrderDate] varchar(20), [ClientPVAmount] money)

RAISERROR('#EOROFinal was created', 0, 1) WITH NOWAIT

-- #OldOrder gets TSFL orders from UCart prior to 02/01/2015, the start of the program
IF object_id('Tempdb..#OldOrder') Is Not Null DROP TABLE #OldOrder
CREATE TABLE #OldOrder (CustNo varchar(255), OrderNo nvarchar(30), OrderDate datetime, Channel varchar(20), OrderType varchar(10))

RAISERROR('Populating #OldOrder table...', 0, 1) WITH NOWAIT

INSERT INTO #OldOrder
SELECT ISNULL(ua.USA_CUST_NBR, 0), oh.ORH_SOURCE_NBR, oh.ORH_CREATE_DT, 
CASE oh.ORH_ORG_ID 
	WHEN '12105623021813764' THEN 'WHOLESALE'
    WHEN '12105623021813765' THEN 'DIRECT'
    WHEN '12105623021813766' THEN 'TSFL'
    WHEN '12105623021813778' THEN 'DIRECT CA'
    WHEN '12105623021813780' THEN 'WHOLESALE CA'
    ELSE '' END AS CHANNEL, h.CTH_TYPE_CD
from [ECOMM_ETL ].dbo.V_USER_ACCOUNT ua WITH (NOLOCK)
INNER JOIN [ECOMM_ETL ].dbo.V_ORDER_HEADER oh WITH (NOLOCK) on ua.USA_ID = oh.ORH_CREATED_FOR
inner join [ECOMM_ETL ].dbo.[CART_HEADER] h (NOLOCK) on h.CTH_ID = oh.ORH_CTH_ID
where oh.ORH_CREATE_DT < @StartDate AND oh.ORH_TOTAL_AMT > 0 AND oh.ORH_ORG_ID = '12105623021813766'

RAISERROR('#OldOrder was populated', 0, 1) WITH NOWAIT

WHILE @StartDate < @Today

BEGIN
Set @EndDate = DATEADD(MONTH, 1, @StartDate)

-- #KitOrdered gets orders containing SKU 31100 or 31110 from UCart after 01/31/2015
IF object_id('Tempdb..#KitOrdered') Is Not Null DROP TABLE #KitOrdered
CREATE TABLE #KitOrdered (CustNo varchar(255))

-- Kit 31100: SKU_ID = '1689949371893131' and Kit 31110: SKU_ID = '1689949371893133'
INSERT INTO #KitOrdered
SELECT distinct ISNULL(ua.USA_CUST_NBR, 0)
from [ECOMM_ETL ].dbo.V_USER_ACCOUNT ua WITH (NOLOCK)
INNER JOIN [ECOMM_ETL ].dbo.V_ORDER_HEADER oh WITH (NOLOCK) on ua.USA_ID = oh.ORH_CREATED_FOR
INNER JOIN [ECOMM_ETL ].dbo.ORDER_LINE ol WITH (NOLOCK) on oh.ORH_ID = ol.ORL_ORH_ID
where oh.ORH_CREATE_DT >= @StartDate AND oh.ORH_CREATE_DT < @EndDate AND 
ORL_SKU_ID IN ('1689949371893131', '1689949371893133') AND oh.ORH_STATUS_CD IN ('C', '1', '2') -- 414

RAISERROR('#KitOrdered was populated',0,1) WITH NOWAIT

-- #EligibleOrder gets possible qualifying TSFL orders
IF object_id('Tempdb..#EligibleOrder') Is Not Null DROP TABLE #EligibleOrder
CREATE TABLE #EligibleOrder (CustId numeric(9,0), CustNo varchar(255), LastName varchar(50), FirstName varchar(50), 
	OrderNo varchar(30), OrderDate datetime, OrderTotal money, PV money)

set @Sql1 = 'SELECT o.CUSTOMER_ID, c.CUSTOMER_NUMBER, c.LAST_NAME, c.FIRST_NAME, o.MASTER_ID OrderNo, o.ENTRY_DATE OrderDate, '
set @Sql1 = @Sql1 + 'o.ORDER_TOTAL OrderTotal, o.TOTAL_VOLUME PV FROM tsfl_odsy1.ORDERS o '
set @Sql1 = @Sql1 + 'INNER JOIN tsfl_odsy1.CUSTOMER c on O.CUSTOMER_ID = c.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'WHERE o.ENTRY_DATE >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' AND '
set @Sql1 = @Sql1 + 'o.ENTRY_DATE < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' AND o.TOTAL_VOLUME > 0 '
set @Sql1 = @Sql1 + 'AND o.SOURCE_CODE = ''BESLIM'' ORDER BY o.CUSTOMER_ID, c.CUSTOMER_NUMBER'
set @Sql2 = 'insert into #EligibleOrder '
set @Sql2 = @Sql2 + 'select ody.* from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody '
set @Sql2 = @Sql2 + 'LEFT JOIN #OldOrder oo on ody.CUSTOMER_NUMBER = oo.CustNo WHERE oo.CustNo IS NULL'
EXEC(@Sql2)

RAISERROR('#EligibleOrder was populated',0,1) WITH NOWAIT

-- #HealthCoachPV gets total PV for the coaches
IF object_id('Tempdb..#HealthCoachPV') Is Not Null DROP TABLE #HealthCoachPV
CREATE TABLE #HealthCoachPV (CustId numeric(9,0), CustNo varchar(255), LastName varchar(50), FirstName varchar(50), PV money)

set @Sql1 = 'SELECT o.CUSTOMER_ID, c.CUSTOMER_NUMBER, c.LAST_NAME, c.FIRST_NAME, SUM(o.TOTAL_VOLUME) FROM tsfl_odsy1.ORDERS o '
set @Sql1 = @Sql1 + 'INNER JOIN tsfl_odsy1.CUSTOMER c on O.CUSTOMER_ID = c.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'WHERE o.ENTRY_DATE >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' AND '
set @Sql1 = @Sql1 + 'o.ENTRY_DATE < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' AND '
set @Sql1 = @Sql1 + 'o.TOTAL_VOLUME > 0 AND o.SOURCE_CODE = ''BESLIM'' '
set @Sql1 = @Sql1 + 'GROUP BY o.CUSTOMER_ID, c.CUSTOMER_NUMBER, c.LAST_NAME, c.FIRST_NAME '
set @Sql1 = @Sql1 + 'ORDER BY o.CUSTOMER_ID, c.CUSTOMER_NUMBER, c.LAST_NAME, c.FIRST_NAME '
set @Sql2 = 'insert into #HealthCoachPV '
set @Sql2 = @Sql2 + 'select ody.* from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody '
EXEC(@Sql2)

RAISERROR('#HealthCoachPV was populated',0,1) WITH NOWAIT

-- Get Customer's Health Coach and the Health Coach's Business Coach
IF object_id('Tempdb..#UplineCoaches') Is Not Null DROP TABLE #UplineCoaches
CREATE TABLE #UplineCoaches (ID numeric(9,0), CustNo varchar(20), CustLastName varchar(50), CustFirstName varchar(50), 
HCID numeric(9,0), HCCustNo varchar(20), LastName varchar(50), FirstName varchar(50), Email varchar(200), RecognitionName varchar(100), ActivationDate datetime)

set @Sql1 = 'select distinct c1.CUSTOMER_ID CI1, c1.CUSTOMER_NUMBER CN1, c1.LAST_NAME CL1, c1.FIRST_NAME CF1, c2.CUSTOMER_ID CI2, c2.CUSTOMER_NUMBER CN2, '
set @Sql1 = @Sql1 + 'c2.LAST_NAME CL2, c2.FIRST_NAME CF2, c2.EMAIL1_ADDRESS, c2.RECOGNITION_NAME, c2.EFFECTIVE_DATE '
set @Sql1 = @Sql1 + 'from tsfl_odsy1.BUSINESSCENTER b1 inner join tsfl_odsy1.CUSTOMER c1 on b1.CUSTOMER_ID = c1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.BUSINESSCENTER b2 on b1.PARENT_BC = b2.BUSINESSCENTER_ID and b1.COMMISSION_PERIOD = b2.COMMISSION_PERIOD '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER c2 on b2.CUSTOMER_ID = c2.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER_STATUS cs on cs.CUSTOMER_ID = c1.CUSTOMER_ID where cs.STATUS_ID =''ACTIV'' '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql2 = 'insert into #UplineCoaches '
set @Sql2 = @Sql2 + 'select ody.* from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody '
EXEC(@Sql2)

RAISERROR('#UplineCoaches was populated',0,1) WITH NOWAIT

IF object_id('Tempdb..#EORO') Is Not Null DROP TABLE #EORO
CREATE TABLE #EORO ([BC ID] varchar(20), [BC First Name] varchar(30), [BC Last Name] varchar(30), [BC Activation] varchar(10), [BC Recognition Name] varchar(50), 
[BC Email] varchar(200), [HC ID] varchar(20), [HC First Name] varchar(30), [HC Last Name] varchar(30), [HC Activation] varchar(10))

INSERT INTO #EORO
select bc.HCCustNo [BC ID], bc.FirstName [BC First Name], bc.LastName [BC Last Name], convert(varchar(10), bc.ActivationDate, 121) [BC Activation],
bc.RecognitionName [BC Recognition Name], bc.Email [BC Email], 
hc.HCCustNo [HC ID], hc.FirstName [HC First Name], hc.LastName [HC Last Name], convert(varchar(10), hc.ActivationDate, 121) [HC Activation]
from #EligibleOrder e inner join #UplineCoaches hc on e.CustId = hc.ID inner join #UplineCoaches bc on hc.HCID = bc.ID inner join #HealthCoachPV pv on pv.CustId = hc.HCID 
left join #OldOrder o on e.CustNo = o.CustNo left join #KitOrdered k on hc.HCCustNo = k.CustNo 
where o.CustNo IS NULL and NOT k.CustNo IS NULL and hc.ActivationDate >= @StartDate and pv.PV >= 150
-- Where removes existing customers and coaches not ordering kits and old activation dates and coaches with PV < 150
group by bc.HCCustNo, bc.FirstName, bc.LastName, convert(varchar(10), bc.ActivationDate, 121), bc.RecognitionName, bc.Email,
hc.HCCustNo, hc.FirstName, hc.LastName, convert(varchar(10), hc.ActivationDate, 121)
order by bc.HCCustNo, bc.FirstName, bc.LastName, convert(varchar(10), bc.ActivationDate, 121), bc.RecognitionName, bc.Email,
hc.HCCustNo, hc.FirstName, hc.LastName, convert(varchar(10), hc.ActivationDate, 121)

RAISERROR('#EORO was populated',0,1) WITH NOWAIT

IF object_id('Tempdb..#QualifyOrder') Is Not Null DROP TABLE #QualifyOrder
CREATE TABLE #QualifyOrder(HCNo varchar(20), HCOrderNo varchar(20), HCOrderDate datetime, HCPVAmount money, RecType char(1))

set @Sql1 = 'select distinct c.CUSTOMER_NUMBER, o.MASTER_ID, o.ENTRY_DATE, o.TOTAL_VOLUME '
set @Sql1 = @Sql1 + 'from tsfl_odsy1.CUSTOMER c '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER_STATUS cs on cs.CUSTOMER_ID = c.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.ORDERS o on o.CUSTOMER_ID = c.CUSTOMER_ID where cs.STATUS_ID = ''ACTIV'' and o.TOTAL_VOLUME > 0 '
set @Sql1 = @Sql1 + 'and o.ENTRY_DATE >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and o.ENTRY_DATE < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql1 = @Sql1 + 'order by c.CUSTOMER_NUMBER, o.ENTRY_DATE '
set @Sql2 = 'DECLARE HealthCoachPV CURSOR FOR select ody.* from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody '
EXEC(@Sql2)

Set @GoToNext = 'N'
Set @TotalPV = 0

OPEN HealthCoachPV
FETCH NEXT FROM HealthCoachPV into @HCID, @OrderNum, @OrderDate, @PVAmount
Set @LastHCID = @HCID
Set @LastOrderNum = @OrderNum
Set @LastOrderDate = @OrderDate
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @GoToNext = 'N'
	BEGIN
		Set @TotalPV = @TotalPV + @PVAmount
		IF @TotalPV >= 150
		BEGIN
			INSERT INTO #QualifyOrder
			SELECT @LastHCID, @OrderNum, @OrderDate, @TotalPV, 1
			Set @GoToNext = 'Y'
			Set @TotalPV = 0
		END
		Set @LastHCID = @HCID
		Set @LastOrderNum = @OrderNum
		Set @LastOrderDate = @OrderDate
	END
	FETCH NEXT FROM HealthCoachPV into @HCID, @OrderNum, @OrderDate, @PVAmount
	IF @HCID > @LastHCID 
	BEGIN
		IF @TotalPV > 0
		BEGIN
			INSERT INTO #QualifyOrder
			SELECT @LastHCID, @LastOrderNum, @LastOrderDate, @TotalPV, 2
		END
		Set @LastHCID = @HCID
		Set @LastOrderNum = @OrderNum
		Set @LastOrderDate = @OrderDate 
		Set @GoToNext = 'N'
		Set @TotalPV = 0
	END
END
CLOSE HealthCoachPV
DEALLOCATE HealthCoachPV

RAISERROR('#QualifyOrder was populated',0,1) WITH NOWAIT

IF object_id('Tempdb..#CustomerOrder') Is Not Null DROP TABLE #CustomerOrder
CREATE TABLE #CustomerOrder(HCNo varchar(20), ClientID varchar(20), ClientFirst varchar(30), ClientLast varchar(30), 
	ClientEntry datetime, ClientOrderNo varchar(20), ClientOrderDate datetime, ClientPVAmount money, RecType char(1))

IF object_id('Tempdb..#FirstOrderDate') Is Not Null DROP TABLE #FirstOrderDate
CREATE TABLE #FirstOrderDate(CustID numeric(9,0), OrderDate datetime)

set @Sql1 = 'select c.CUSTOMER_ID, MIN(o.ENTRY_DATE) from tsfl_odsy1.CUSTOMER c '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER_STATUS cs on cs.CUSTOMER_ID = c.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.ORDERS o on o.CUSTOMER_ID = c.CUSTOMER_ID where cs.STATUS_ID = ''ACTIV'' group by c.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'having MIN(o.ENTRY_DATE) >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and MIN(o.ENTRY_DATE) < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql2 = 'insert into #FirstOrderDate select * from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') '
EXEC(@Sql2) -- 10100 rows in 30 seconds

RAISERROR('#FirstOrderDate was populated',0,1) WITH NOWAIT

IF object_id('Tempdb..#CustCount') Is Not Null DROP TABLE #CustCount
CREATE TABLE #CustCount(HCNo varchar(20), CustCount integer)

set @Sql1 = 'select c2.CUSTOMER_NUMBER HCNo, c1.CUSTOMER_NUMBER, SUM(o.TOTAL_VOLUME) TOTAL_VOLUME '
set @Sql1 = @Sql1 + 'from tsfl_odsy1.BUSINESSCENTER b1 inner join tsfl_odsy1.CUSTOMER c1 on b1.CUSTOMER_ID = c1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.BUSINESSCENTER b2 on b1.PARENT_BC = b2.BUSINESSCENTER_ID and b1.COMMISSION_PERIOD = b2.COMMISSION_PERIOD '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER c2 on b2.CUSTOMER_ID = c2.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER_STATUS cs on cs.CUSTOMER_ID = c1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.ORDERS o on o.CUSTOMER_ID = c1.CUSTOMER_ID where cs.STATUS_ID = ''ACTIV'' and o.TOTAL_VOLUME > 0 '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and o.ENTRY_DATE >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and o.ENTRY_DATE < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql1 = @Sql1 + 'group by c2.CUSTOMER_NUMBER, c1.CUSTOMER_NUMBER having SUM(o.TOTAL_VOLUME) >= 250 '
set @Sql2 = 'INSERT INTO #CustCount '
set @Sql2 = @Sql2 + 'select ody.HCNo, COUNT(CUSTOMER_NUMBER) from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody group by ody.HCNo'
EXEC(@Sql2)

RAISERROR('#CustCount was populated',0,1) WITH NOWAIT

set @Sql1 = 'select c2.CUSTOMER_NUMBER HCNO, c1.CUSTOMER_ID, c1.CUSTOMER_NUMBER, c1.FIRST_NAME, c1.LAST_NAME, o.MASTER_ID, o.ENTRY_DATE, o.TOTAL_VOLUME '
set @Sql1 = @Sql1 + 'from tsfl_odsy1.BUSINESSCENTER b1 inner join tsfl_odsy1.CUSTOMER c1 on b1.CUSTOMER_ID = c1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.BUSINESSCENTER b2 on b1.PARENT_BC = b2.BUSINESSCENTER_ID and b1.COMMISSION_PERIOD = b2.COMMISSION_PERIOD '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER c2 on b2.CUSTOMER_ID = c2.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER_STATUS cs on cs.CUSTOMER_ID = c1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.ORDERS o on o.CUSTOMER_ID = c1.CUSTOMER_ID where cs.STATUS_ID = ''ACTIV'' and o.TOTAL_VOLUME > 0 '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and o.ENTRY_DATE >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and o.ENTRY_DATE < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql2 = 'DECLARE CustomerPV CURSOR FOR select ody.HCNo, ody.CUSTOMER_NUMBER, ody.FIRST_NAME, ody.LAST_NAME, f.OrderDate, ody.MASTER_ID, ody.ENTRY_DATE, ody.TOTAL_VOLUME '
set @Sql2 = @Sql2 + 'from #FirstOrderDate f inner join openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody on f.CustID = ody.CUSTOMER_ID '
set @Sql2 = @Sql2 + 'order by ody.HCNo, ody.CUSTOMER_NUMBER, ody.ENTRY_DATE '
EXEC(@Sql2) -- 78774 rows in 2 mins 30 secs

RAISERROR('CustomerPV Cursor was populated',0,1) WITH NOWAIT

Set @ClientEntry = NULL
Set @GoToNext = 'N'
Set @OrderNo = ''
Set @TotalPV = 0

OPEN CustomerPV
FETCH NEXT FROM CustomerPV into @HCNo, @CustNo, @FirstName, @LastName, @ClientEntry, @OrderNum, @OrderDate, @PVAmount
Set @LastHCNo = @HCNo
Set @LastCustNo = @CustNo
Set @LastFirstName = @FirstName
Set @LastLastName = @LastName
Set @LastClientEntry = @ClientEntry
Set @LastOrderNum = @OrderNum
Set @LastOrderDate = @OrderDate
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @GoToNext = 'N'
	BEGIN
		Set @TotalPV = @TotalPV + @PVAmount
		IF @TotalPV >= 250
		BEGIN
			INSERT INTO #CustomerOrder
			SELECT @HCNo, @CustNo, @FirstName, @LastName, @ClientEntry, @OrderNum, @OrderDate, @TotalPV, 1
			Set @GoToNext = 'Y'
			Set @TotalPV = 0
		END
		Set @LastHCNo = @HCNo
		Set @LastCustNo = @CustNo
		Set @LastFirstName = @FirstName
		Set @LastLastName = @LastName
		Set @LastClientEntry = @ClientEntry
		Set @LastOrderNum = @OrderNum
		Set @LastOrderDate = @OrderDate
	END
	FETCH NEXT FROM CustomerPV into @HCNo, @CustNo, @FirstName, @LastName, @ClientEntry, @OrderNum, @OrderDate, @PVAmount
	IF @HCNo > @LastHCNo
	BEGIN
		If @TotalPV > 250
		BEGIN
			INSERT INTO #CustomerOrder
			SELECT @LastHCNo, @LastCustNo, @LastFirstName, @LastLastName, @LastClientEntry, @LastOrderNum, @LastOrderDate, @TotalPV, 2
		END
		Set @LastHCNo = @HCNo
		Set @LastCustNo = @CustNo
		Set @LastFirstName = @FirstName
		Set @LastLastName = @LastName
		Set @LastClientEntry = @ClientEntry
		Set @LastOrderNum = @OrderNum
		Set @LastOrderDate = @OrderDate
		Set @GoToNext = 'N'
		Set @TotalPV = 0
	END
END
CLOSE CustomerPV
DEALLOCATE CustomerPV

INSERT INTO #EOROFinal
select CONVERT(varchar(4), DATEPART(yyyy, @StartDate)) + '-' + RIGHT('0' + CONVERT(varchar(2), DATEPART(MONTH, @StartDate)), 2),
f.*, CONVERT(varchar(10), q.HCOrderDate, 121), q.HCOrderNo, q.HCPVAmount, cc.CustCount, c.ClientID, c.ClientFirst, c.ClientLast, 
CONVERT(varchar(10), c.ClientEntry, 121), c.ClientOrderNo, CONVERT(varchar(10), c.ClientOrderDate, 121), c.ClientPVAmount 
from #EORO f inner join #QualifyOrder q on f.[HC ID] = q.HCNo inner join #CustomerOrder c on f.[HC ID] = c.HCNo 
inner join #CustCount cc on f.[HC ID] = cc.HCNo

RAISERROR('#EOROFinal was populated',0,1) WITH NOWAIT

-- Add additional months order data to #OldOrder as necessary
INSERT INTO #OldOrder
SELECT ISNULL(ua.USA_CUST_NBR, 0), oh.ORH_SOURCE_NBR, oh.ORH_CREATE_DT, 
CASE oh.ORH_ORG_ID 
	WHEN '12105623021813764' THEN 'WHOLESALE'
    WHEN '12105623021813765' THEN 'DIRECT'
    WHEN '12105623021813766' THEN 'TSFL'
    WHEN '12105623021813778' THEN 'DIRECT CA'
    WHEN '12105623021813780' THEN 'WHOLESALE CA'
    ELSE '' END AS CHANNEL, h.CTH_TYPE_CD
from [ECOMM_ETL ].dbo.V_USER_ACCOUNT ua WITH (NOLOCK)
INNER JOIN [ECOMM_ETL ].dbo.V_ORDER_HEADER oh WITH (NOLOCK) on ua.USA_ID = oh.ORH_CREATED_FOR
inner join [ECOMM_ETL ].dbo.[CART_HEADER] h (NOLOCK) on h.CTH_ID = oh.ORH_CTH_ID
where oh.ORH_CREATE_DT >= @StartDate AND oh.ORH_CREATE_DT < DateAdd(MONTH, 1, @StartDate) 
	AND oh.ORH_TOTAL_AMT > 0 AND oh.ORH_ORG_ID = '12105623021813766'

RAISERROR('#OldOrder was updated',0,1) WITH NOWAIT

-- Increment @StartDate for additional iterations
Set @StartDate = DateAdd(MONTH, 1, @StartDate)

RAISERROR('End Of Iteration',0,1) WITH NOWAIT

END

-- Return all data to the calling report
select * from #EOROFinal where ClientPVAmount >= 250 order by [EarnedMonth], [BC Last Name], [BC First Name]

