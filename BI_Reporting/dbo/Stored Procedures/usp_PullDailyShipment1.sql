﻿


-- =============================================
-- Author:		Kalpesh Patel
-- Create date: 07/09/2013
-- Description:	Daily Shipment Report
-- =============================================
CREATE PROCEDURE [dbo].[usp_PullDailyShipment1] 
	@DistCenter varchar(50),
	@Dept varchar(150),
	@StartDate datetime, 
	@EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @StartValue varchar(10)
	declare @EndValue varchar(10)

	set @StartValue=cast(year(@StartDate) as varchar(4)) + '-' + right('0' + cast(month(@StartDate) as varchar(2)),2) + '-' + right('0' + cast(day(@StartDate) as varchar(2)),2)
	set @EndValue=cast(year(@EndDate) as varchar(4)) + '-' + right('0' + cast(month(@EndDate) as varchar(2)),2) + '-' + right('0' + cast(day(@EndDate) as varchar(2)),2)

	print @StartValue
	Print @EndValue

	SELECT		case shipment_header.keydata1
					when 1 then 'MDCWMS'
					when 2 then 'TDCWMS'
				end as dc
				, [reference3] as KeyValue
				, count([ui_sales_order_no]) as PCount
	FROM    FLAGSHIP_WMS_ETL.dbo.shipment_header
	WHERE     (shipment_header.ship_date >= @StartValue and shipment_header.ship_date<=@EndValue)
				and shipment_header.status ='UPLD'				
				and shipment_header.keydata1 in (select * from dbo.ufn_SPLIT(@DistCenter, ','))
	group by shipment_header.keydata1, [reference3]

	



END














