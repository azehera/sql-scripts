﻿Create Procedure [dbo].[USP_XL_027_AutoshipAsOf_SSRS] AS 

SET NOCOUNT ON;


/*
=============================================================================================
Author:         Menkir Haile
Create date: 01/18/2016
-------------------------[dbo].[USP_XL_027_AutoshipAsOf] -----------------------------
Archive Monthly Autoship Detail as of first of month.
Table populated by usp [dbo].[USP_XL_027_AutoshipAsOf]
=============================================================================================   
REFERENCES
Database              Table/View/UDF                                    Action            
---------------------------------------------------------------------------------------------
[BI_Reporting]     [dbo].[XL_023_MonthlyAutoshipForecastAndActual]      Select                         
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------

==========================================================================================
*/


select * from [dbo].[XL_023_MonthlyAutoshipForecastAndActual]
