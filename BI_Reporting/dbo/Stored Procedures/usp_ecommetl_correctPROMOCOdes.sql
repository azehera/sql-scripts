﻿

-- ==============================================================================================
-- Author:		Micah Williams
-- Create date: 4/7/19
-- Description:	Update promo codes in ecomm_etl_hybris Order Header with correct promo codes from Hybris
--====================================== MODIFICATIONS ==========================================
-- Modified By:		Daniel Dagnachew
-- Modified date: 8/13/19
-- Description:	commented out open query and pointed to Hybris_Etl
-- ==============================================================================================
CREATE PROCEDURE [dbo].[usp_ecommetl_correctPROMOCOdes]
   
AS

----Get Correct PROMO CODES from Hybris
--IF OBJECT_ID('Tempdb..#hybVoc') IS NOT NULL DROP TABLE #hybVoc;
--SELECT * 
--INTO #hybVoc
--FROM OPENQUERY (ECOMM, 'Select ord.p_code, ord.createdTS, v.P_code as ''promo_code''
--from orders ord 
--left join voucherinvalidations v on v.p_order = ord.pk
--where cast(ord.createdTS as Date) >= ''2019-02-23'' and v.P_code is not null');  

--Get Correct PROMO CODES from Hybris
IF OBJECT_ID('Tempdb..#hybVoc') IS NOT NULL DROP TABLE #hybVoc;
SELECT ord.p_code, ord.createdTS, v.P_code AS 'promo_code'
INTO #hybVoc
FROM [Hybris_ETL].[mdf_prd_database].[orders] ord 
LEFT JOIN [Hybris_ETL].[mdf_prd_database].[voucherinvalidations] v ON v.p_order = ord.pk
WHERE CAST(ord.createdTS AS DATE) >= '2019-02-23' 
AND v.P_code IS NOT NULL  


--NULL out the promo code values in the order header table
UPDATE ecomm_etl_hybris.dbo.order_header 
SET promo_code = NULL
WHERE orderdate >='02-23-2019'


--update the order header table with the correct hybris promo code
UPDATE oh
SET oh.promo_code = H.promo_code
FROM ecomm_etl_hybris.dbo.order_header oh 
JOIN #hybVoc H ON oh.ordernumber = H.p_code
WHERE oh.orderdate >='02-23-2019'

