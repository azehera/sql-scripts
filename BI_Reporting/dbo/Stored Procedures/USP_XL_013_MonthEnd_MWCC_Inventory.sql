﻿


/*
--==================================================================================
Author:         Luc Emond
Create date: 08/27/2012
----------------------[dbo].[USP_XL_013_MonthEnd_MWCC_Inventory]-------------------
Provide Month End Inventory for all MWCC location with their cost-----
--=================================================================================    
REFERENCES
Database              Table/View/UDF                       Action            
------------------------------------------------------------------------------------
BI_REPORTING       [Calendar]				              Select
NAV-ETL            [Snapshot$NAV_Inventory]     		  Select 
NAV-ETL            [Jason Pharm$Item]	            	  Select  
Book4Time_ETL      [B4T_product_master]                   Select
Book4Time_ETL      [B4T_location]                         Select
                        
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------
4/15/2015     Menkir Haile                 EXECUTE AS LOGIN = 'ReportViewer' is commented out
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
--*/  

CREATE PROCEDURE [dbo].[USP_XL_013_MonthEnd_MWCC_Inventory] As
SET NOCOUNT ON;

----EXECUTE AS LOGIN = 'ReportViewer'
----DECLARE TIME PERIOD------
Declare @Date1 Datetime, @Date2 Datetime

Set @Date1 = (SELECT CalendarDate FROM dbo.Calendar where CalendarDate = (select Convert (varchar(10), GetDate(),121)))
Set @Date2 = (SELECT FirstDateOfMonth FROM dbo.Calendar where CalendarDate = @Date1)
--Set @Date2 ='2014-04-29'----for adhoc purpose-----
--Select @date1, @Date2


-------DATA----
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT 
DISTINCT(RunDate)
,Yesterday as InventoryDate
,InvLocation AS LocationId
,l.legal_name As Location#
,Case When InvLocationName ='' Then InvLocation Else InvLocationName end As InvLocationName
,B4T_ItemNo
,NavItemNo
,j.Description
,InvPostClass
,InvPostGroup
,ItemCategoryCode
,[Product Group Code]
,[Standard Cost]
,OnHandQtyDay As OnHand_Bx
,QtyPerUnitOfMeasure
,OnHandQtyBaseDay As OnHand_Ea
,OnHandAmtDay
Into #A
FROM [NAV_ETL].[dbo].[Snapshot$NAV_Inventory] I
Left Outer join [NAV_ETL].[dbo].[Jason Pharm$Item] J 
On I.ItemNo COLLATE Latin1_General_CI_AS = J.[No_]  COLLATE Latin1_General_CI_AS
Left Outer Join [Book4Time_ETL].[dbo].[B4T_location] l
On I.InvLocation = l.Location_Id
WHERE ((NavItemNo <> '') AND (NavItemNo IS NOT NULL)) AND InvLocationType ='MWCC'
and RunDate=@Date2
ORDER BY RUNDATE

----ADDING A NEW FIELD----- Category----
ALTER TABLE #A ADD Category Char(25)

UPDATE #A SET Category ='Inventory'

----FIX THE TRANSCATION TO BE INVENTORY OR NON-INVENTORY--AS PER LEI EVERYTHING AT "0" FOR STANDARD COST IN B4T ARE CONSIDERED NON-INVENTORY ITEMS------------- 
IF object_id('Tempdb..#AA') Is Not Null DROP TABLE #AA
Select sku, retail_price 
Into #AA
From [Book4Time_ETL].[dbo].[B4T_product_master] 
Where retail_price =0

UPDATE #A SET Category ='Non-Inventory'
Where #A.NavItemNo in (Select Sku from #AA)


-----delete from table from meeting with MWCC finance ----remove manual process---
Delete #A
Where NavItemNo in (
'0158',
'0167',
'36080',
'36081',
'36084',
'36085',
'36086',
'36150',
'36151',
'36290',
'36658',
'36680',
'36702',
'36706',
'36707',
'36708',
'41040',
'50150',
'72410',
'72420',
'72860',
'74900')


--UPDATE #A SET RunDate = RunDate-1

----FIANL DATA----
SELECT * FROM #A






