﻿
/***** This stored procedure is invoked by the EORO TSFL incentive program report.
   It returns the health coach ID, order number, order date and order amount sum
   for the orders that push the Health Coach over the 150 PV requirement. *****/

CREATE PROCEDURE [dbo].[usp_Get_Coach_Qualifying_Order] (@HCNo varchar(20), @StartDate datetime) AS

Declare @Sql1 varchar(2000)
Declare @Sql2 varchar(2000)
Declare @EndDate datetime
Declare @TotalPV money
Declare @OrderNum varchar(20)
Declare @OrderDate datetime
Declare @PVAmount money
Declare @OrderNo varchar(20)
Declare @HCID varchar(20)

Set @EndDate = DATEADD(m, 1, @StartDate)
Set @TotalPV = 0
Set @OrderNo = ''

set @Sql1 = 'select distinct c.CUSTOMER_NUMBER, o.MASTER_ID, o.ENTRY_DATE, o.TOTAL_VOLUME '
set @Sql1 = @Sql1 + 'from tsfl_odsy1.CUSTOMER c '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER_STATUS cs on cs.CUSTOMER_ID = c.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.ORDERS o on o.CUSTOMER_ID = c.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'where cs.STATUS_ID = ''ACTIV'' and o.ENTRY_DATE >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and c.CUSTOMER_NUMBER = '' + @HCNo + '' order by o.ENTRY_DATE '
set @Sql2 = 'DECLARE HealthCoachPV CURSOR FOR select ody.* from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody '
EXEC(@Sql2)

OPEN HealthCoachPV
FETCH NEXT FROM HealthCoachPV into @HCID, @OrderNum, @OrderDate, @PVAmount
WHILE @@FETCH_STATUS = 0
BEGIN
	Set @TotalPV = @TotalPV + @PVAmount
	IF @TotalPV >= 150 break;
	FETCH NEXT FROM HealthCoachPV into @HCID, @OrderNum, @OrderDate, @PVAmount
END
CLOSE HealthCoachPV
DEALLOCATE HealthCoachPV

SELECT @HCNo, @OrderNum, @OrderDate, @TotalPV
