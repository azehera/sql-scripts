﻿

/*
=============================================================================================
Author:      Luc Emond
Create date: 04/17/2013
-------------------------[dbo].[USP_XL_036_MWCC_FoodOrderAvg_byTechnician] -----------------------------
Provide Daily Sales Info with Average for all MWCC Centers and technicians                 
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[Book4Time_ETL]       B4T_transaction_log_header                Select
[Book4Time_ETL]       B4T_transaction_log_detail                Select
[Book4Time_ETL]       B4T_person                                Select
[Book4Time_ETL]       B4T_location                              Select 
[Book4Time_ETL]       B4T_resource                              Select
[Book4Time_ETL]       B4T_TransactionSummaryList                Select
[Book4Time_ETL]       B4T_product_master                        Select
[Book4Time_ETL]       B4T_product_class                         Select

===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------

==========================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_036_MWCC_FoodOrderAvg_byTechnician] AS

SET NOCOUNT ON;


----------BEGIN-------
Select 
Convert(Varchar(10),th.transaction_date,121) as CalDate
,YEAR(th.transaction_date) as CalYear
,MONTH(th.transaction_date) as CalMonth
,th.transaction_id
,l.location_code
,l.location_name
,Technician
,1 as TechOrderCount
,Case When ROW_NUMBER() OVER(PARTITION BY th.transaction_id ORDER BY th.transaction_id asc) =1 then 1 else 0 end As OrderCount
,SUM( sold_price * qty ) as Amount
FROM [Book4Time_ETL].Dbo.B4T_transaction_log_header th
Left Join [Book4Time_ETL].Dbo.B4T_transaction_log_detail AS tl 
       on th.transaction_id = tl.transaction_id 
Left Join [Book4Time_ETL].Dbo.B4T_person p 
       on p.person_id = th.customer_id 
Left Join [Book4Time_ETL].Dbo.B4T_location l 
       on l.location_id = th.location_id AND l.cid = th.cid
Left Join [Book4Time_ETL].Dbo.B4T_resource s 
       on tl.resource_id = s.resource_id
Left Join [Book4Time_ETL].Dbo.B4T_person q 
       on s.employee_id = q.person_id
Left Join [Book4Time_ETL].[dbo].[B4T_TransactionSummaryList] a
       on th.transaction_id=a.transaction_id
     and a.EMPLOYEE_ID=s.employee_id
Left Join[Book4Time_ETL].[dbo].[B4T_product_master] B
       on tl.item_code = B.sku
Left Join [Book4Time_ETL].[dbo].[B4T_product_class] C
       on B.class_id = C.class_id
WHERE th.transaction_date >=DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0)
      and C.Class_id in('3620027', '3620043','3622634') ---"3620027=Non-Product (Tools)", "3620043 = Food","3622634=Counseling Visits"---
      and technician is not null
      and th.transaction_type in ('R', 'S')
Group by 
Convert(Varchar(10),th.transaction_date,121)
,YEAR(th.transaction_date)
,MONTH(th.transaction_date)
,l.location_code
,l.location_name
,Technician
,th.transaction_id




