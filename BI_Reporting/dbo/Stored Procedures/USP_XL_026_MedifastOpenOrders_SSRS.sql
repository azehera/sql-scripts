﻿/*
==================================================================================
Author:         Luc Emond
Create date: 06/26/2013
---------------------------[USP_XL_026_MedifastOpenOrders]-------------------
---Provide open orders----
==================================================================================
REFERENCES
Database              Table/View/UDF                       Action
----------------------------------------------------------------------------------
[NAVISION_PROD]      [Jason Pharm$Sales Header]            SELECT
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------
08/25/2015		Menkir Haile				[NAVISION_PROD] reference changed to NAV_ETL
08/25/2020		Daniel Dagnachew			ORACLE_ETL.dbo.Oracle_ETL_Sales_Header
==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_XL_026_MedifastOpenOrders_SSRS]
AS
SET NOCOUNT ON;
SELECT GETDATE() AS ReportDateTime,
       SI.[Location Code],
       [Customer Posting Group],
       UNITS,
       CAST([Order Date] AS DATE) AS [Order Date],
       [Amount Authorized],
       SI.[Shortcut Dimension 1 Code]
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Header] SI WITH (NOLOCK)
    JOIN
    (
        SELECT [Document No_],
               SUM(([Quantity] * [Qty_ per Unit of Measure])) UNITS
        FROM NAV_ETL.[dbo].[Jason Pharm$Sales Line] WITH (NOLOCK)
        GROUP BY [Document No_]
    ) SL
        ON SI.[No_] = SL.[Document No_]
      AND ([Coupon_Promotion Code] <> 'EXCHANGE' OR [Coupon_Promotion Code] IS NULL )
      AND [Customer Posting Group] IN ( 'MEDIFAST', 'TSFL', 'DOCTORS', 'SPRTNTR', 'FRANCHISE', 'HK', 'SG' )
	  AND SI.[Amount Authorized] >=0
	left join ORACLE_ETL.dbo.Oracle_ETL_Sales_Header oh WITH (NOLOCK)
	on oh.External_Document_No_ = si.[External Document No_]
where Customer_Posting_Group NOT LIKE '%RETURN%'
and (External_Document_No_ NOT LIKE 'RMA%' or External_Document_No_ NOT LIKE 'SRA%')