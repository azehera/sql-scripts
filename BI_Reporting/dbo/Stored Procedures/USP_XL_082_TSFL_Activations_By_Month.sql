﻿
CREATE procedure [dbo].[USP_XL_082_TSFL_Activations_By_Month] as 

select YEAR([EFFECTIVE_DATE]) As [Year], MONTH([EFFECTIVE_DATE]) As [Month], COUNT(*) As [Count]
from [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] 
Where convert(varchar(10),[EFFECTIVE_DATE], 121) >= '2012-01-01'
And convert(varchar(10),[EFFECTIVE_DATE], 121) <= GETDATE()
Group By YEAR([EFFECTIVE_DATE]), MONTH([EFFECTIVE_DATE])
Order By YEAR([EFFECTIVE_DATE]), MONTH([EFFECTIVE_DATE])
