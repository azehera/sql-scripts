﻿
/*
--==================================================================================
Author:         Kalpesh Patel
Create date: 01/14/2014
-----------------------------dbo.BI_NAV_Channel_SalesCube-------------------
Create Daily Flash reports from Cube data
--==================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------
[BI_SSAS_Cubes]       [dbo].[FactSales]                        Select
[BI_SSAS_Cubes]       [dbo].[Snapshot$NAV_Channel_SalesCube]   Insert
                        
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/



/****** Script for SelectTopNRows command from SSMS  ******/
CREATE Procedure [dbo].[BI_NAV_Channel_SalesCube](@DateEnd AS DATETIME)
as 
 

DECLARE @DateMonthBegin DATETIME
DECLARE @DateMonthEnd DATETIME
DECLARE @DateYearBegin DATETIME
DECLARE @DateYearEnd DATETIME
DECLARE @DateYesterday DATETIME
DECLARE @DatePriorYrMonthBegin DATETIME
DECLARE @DatePriorYrYearBegin DATETIME
DECLARE @DatePriorYrYesterday DATETIME
DECLARE @DaysMonth INT
DECLARE @DaysMonthElapsed INT
DECLARE @DaysMonthRemaining INT
DECLARE @DaysYear INT
DECLARE @DaysYearElapsed INT
DECLARE @DaysYearRemaining INT
DECLARE @ProcessTimeBegin DATETIME
DECLARE @ProcessTimeEnd DATETIME

SET @ProcessTimeBegin = GetDate()
SET @DateEnd =  CAST(FLOOR(CAST(@DateEnd AS float)) AS DATETIME)
SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1




DELETE FROM [Snapshot$NAV_Channel_SalesCube]
WHERE (RunDate = @DateEnd)

---- Calculate Yesterday Sales,Units,Cost and Orders info.---------------

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,isnull(Sum([Amount]),0) as [Invcd. Sales Yesterday]
      ,isnull(SUM(Units),0) as [Invcd. Units Yesterday]
      ,isnull(SUM([Unit Cost (LCY)] *Units),0) as InvExtCostYesterday
      ,isnull(COUNT(Distinct DocumentNo),0) as [Invcd. Orders Yesterday]
      into #A FROM [BI_SSAS_Cubes].[dbo].[FactSales]
where [Posting Date]=@DateYesterday
Group by [CustomerPostingGroup]
         ,[SalesChannel]
  
---- Calculate MTD Sales,Units,Cost and Orders info.---------------      

IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B    
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,isnull(Sum([Amount]),0) as [Invcd. Sales MTD]
      ,isnull(SUM(Units),0) as [Invcd. Units MTD]
      ,isnull(SUM([Unit Cost (LCY)]*Units),0) as InvExtCostMTD
      ,isnull(COUNT(Distinct DocumentNo),0) as [Invcd. Orders MTD]
      into #B FROM [BI_SSAS_Cubes].[dbo].[FactSales]
where [Posting Date] between @DateMonthBegin and @DateYesterday 
Group by [CustomerPostingGroup]
         ,[SalesChannel]

---- Calculate Prior Year MTD Sales,Units,Cost and Orders info.---------------      
 
IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C 
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,isnull(Sum([Amount]),0) as [Invcd. Sales Prior Year MTD]
      ,isnull(Sum([Units]),0) as [Invcd. Units Prior Year MTD]
      ,isnull(COUNT(Distinct DocumentNo),0) as [Invcd. Orders Prior Year MTD]
      into #C FROM [BI_SSAS_Cubes].[dbo].[FactSales]
where [Posting Date] between @DatePriorYrMonthBegin and @DatePriorYrYesterday
Group by [CustomerPostingGroup]
         ,[SalesChannel]
 
---- Calculate YTD Sales,Units,Cost and Orders info.---------------     
  
IF object_id('Tempdb..#D') Is Not Null DROP TABLE #D      
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,isnull(Sum([Amount]),0) as [Invcd. Sales YTD]
      ,isnull(SUM(Units),0) as [Invcd. Units YTD]
      ,isnull(SUM([Unit Cost (LCY)] * Units),0) as InvExtCostYTD
      ,isnull(COUNT(Distinct DocumentNo),0) as [Invcd. Orders YTD]
      into #D FROM [BI_SSAS_Cubes].[dbo].[FactSales]
where [Posting Date] between @DateYearBegin and  @DateYesterday 
Group by [CustomerPostingGroup]
         ,[SalesChannel]

---- Calculate Prior Year YTD Sales,Units,Cost and Orders info.---------------     
  
IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E       
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,isnull(Sum([Amount]),0) as [Invcd. Sales Prior Year YTD]
      ,isnull(Sum([Units]),0) as [Invcd. Units Prior Year YTD]
      ,isnull(COUNT(Distinct DocumentNo),0) as [Invcd. Orders Prior Year YTD]
      into #E FROM [BI_SSAS_Cubes].[dbo].[FactSales]
where [Posting Date] between @DatePriorYrYearBegin and  @DatePriorYrYesterday
Group by [CustomerPostingGroup]
         ,[SalesChannel]
      
---- Calculate Shipping Revenue for Yesterday--------------- 
       
IF object_id('Tempdb..#F') Is Not Null DROP TABLE #F
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,isnull(Sum([Amount]),0) as [Net Shipping Revenue Yesterday]
      into #F FROM [BI_SSAS_Cubes].[dbo].[FactSales]
where [Posting Date]=@DateYesterday and ItemCode='63000'
Group by [CustomerPostingGroup]
         ,[SalesChannel]
      
---- Calculate Shipping Revenue for MTD--------------- 
      
IF object_id('Tempdb..#G') Is Not Null DROP TABLE #G    
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,isnull(Sum([Amount]),0) as [Net Shipping Revenue MTD]
      into #G FROM [BI_SSAS_Cubes].[dbo].[FactSales]
where [Posting Date] between @DateMonthBegin and @DateYesterday and ItemCode='63000'
Group by [CustomerPostingGroup]
         ,[SalesChannel]

---- Calculate Sales Credit for Yesterday--------------- 
 
IF object_id('Tempdb..#I') Is Not Null DROP TABLE #I
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,isnull(Sum([Amount]),0) as [Invcd. SalesCredit Yesterday]
      into #I FROM [BI_SSAS_Cubes].[dbo].[FactSales]
where [Posting Date]=@DateYesterday and SalesType=2
Group by [CustomerPostingGroup],[SalesChannel]
      
---- Find all Channel and Customer Posting Group ---------------      
IF object_id('Tempdb..#H') Is Not Null DROP TABLE #H     
Select distinct CustomerPostingGroup as [Customer Posting Group]
      ,SalesChannel as  Channel
      into #H FROM [BI_SSAS_Cubes].[dbo].[FactSales] 
      where [Posting Date] between @DatePriorYrYearBegin and @DateYesterday

----Insert Data into [dbo].[Snapshot$NAV_Channel] for Flash----
insert into [dbo].[Snapshot$NAV_Channel_SalesCube]
  Select @DateEnd as RunDate
         ,@DateYesterday as [Yesterday]
         ,[Customer Posting Group] as CPG
         ,Channel as [SalesSrce] 
         ,Convert(DECIMAL(18,2),isnull([Invcd. Sales Yesterday],0)) as [InvSalesDay]
         ,isnull([Invcd. Units Yesterday],0) as [InvUnitsDay]
         ,isnull([Invcd. Orders Yesterday],0) as [InvOrdersDay]
         ,Convert(DECIMAL(18,2),isnull([Net Shipping Revenue Yesterday],0)) as [NetShipRevDay]
         ,Convert(DECIMAL(18,2),isnull([Invcd. Sales MTD],0)) as [InvSalesMTD]
         ,Convert(DECIMAL(18,2),isnull([Invcd. Sales Prior Year MTD],0)) as [InvSalesPriorYrMTD]
         ,Convert(DECIMAL(18,2),isnull([Net Shipping Revenue MTD],0)) as [NetShipRevMTD]
         ,Convert(DECIMAL(18,2),isnull(([Invcd. Sales MTD]/NULLIF(@DaysMonthElapsed,0)) * @DaysMonth,0)) as [InvSalesMoProj]
         ,Convert(DECIMAL(18,2),isnull(([Net Shipping Revenue MTD]/NULLIF(@DaysMonthElapsed,0)) * @DaysMonth,0))as [NetShipRevMoProj]
         ,isnull([Invcd. Units MTD],0) as [InvUnitsMTD]
         ,isnull([Invcd. Units Prior Year MTD],0) as [InvUnitsPriorYrMTD]
         ,isnull([Invcd. Orders MTD],0) as [InvOrdersMTD]
         ,isnull([Invcd. Orders Prior Year MTD],0)as [InvOrdersPriorYrMTD]
         ,Convert(DECIMAL(18,2), isnull([Invcd. Sales YTD],0)) as [InvSalesYTD]
         ,Convert(DECIMAL(18,2),isnull([Invcd. Sales Prior Year YTD],0)) as [InvSalesPriorYrYTD]
         ,Convert(DECIMAL(18,2),isnull(([Invcd. Sales YTD]/NULLIF(@DaysYearElapsed,0)) * @DaysYear,0)) as [InvSalesProjYE]
         ,isnull([Invcd. Units YTD],0) as [InvUnitsYTD]
         ,isnull([Invcd. Units Prior Year YTD],0) as [InvUnitsPriorYrYTD]
         ,isnull([Invcd. Orders YTD],0) as [InvOrdersYTD]
         ,isnull([Invcd. Orders Prior Year YTD],0) as [InvOrdersPriorYrYTD]
         ,@DateYearBegin as [StartDateYear]
         ,@DateMonthBegin as [StartDateMonth]
         ,@DatePriorYrYearBegin as [StartDateYearPriorYr]
         ,@DatePriorYrMonthBegin as [StartDateMonthPriorYr]
         ,@DatePriorYrYesterday as [YesterdayPriorYr]
         ,Convert(DECIMAL(18,2),isnull(InvExtCostYesterday,0)) as [InvExtCostDay]
         ,Convert(DECIMAL(18,2),isnull(InvExtCostMTD,0)) as [InvExtCostMTD]
         ,Convert(DECIMAL(18,2),isnull(InvExtCostYTD,0)) as [InvExtCostYTD]
         ,Convert(DECIMAL(18,2),isnull([Invcd. SalesCredit Yesterday],0)) as [InvCreditsDay]
         ,0 as [SortOrder]
         ,0 as [SortOrder2]
         ,0 as [VarianceMTD]
         ,0 as [VarianceYTD]
         from #H h
              left join #A a
              on h.[Customer Posting Group]=a.[CustomerPostingGroup]
              and h.Channel=a.[SalesChannel]
              left join #F f
              on h.[Customer Posting Group]=f.[CustomerPostingGroup]
              and h.Channel=f.[SalesChannel]
              left join #B b
              on h.[Customer Posting Group]=b.[CustomerPostingGroup]
              and h.Channel=b.[SalesChannel]
              left join #C c
              on h.[Customer Posting Group]=c.[CustomerPostingGroup]
              and h.Channel=c.[SalesChannel]
              left join #G g
              on h.[Customer Posting Group]=g.[CustomerPostingGroup]
              and h.Channel=g.[SalesChannel]
              left join #D d
              on h.[Customer Posting Group]=d.[CustomerPostingGroup]
              and h.Channel=d.[SalesChannel]
              left join #E e
              on h.[Customer Posting Group]=e.[CustomerPostingGroup]
              and h.Channel=e.[SalesChannel]
              left join #I i
              on h.[Customer Posting Group]=i.[CustomerPostingGroup]
              and h.Channel=i.[SalesChannel]
      where ([Invcd. Sales Yesterday] is not null or
             [Invcd. Units Yesterday] is not null or 
             [Invcd. Orders Yesterday] is not null or
             [Net Shipping Revenue Yesterday] is not null or
             [Invcd. Sales MTD] is not null or 
             [Invcd. Sales Prior Year MTD] is not null or 
             [Net Shipping Revenue MTD] is not null or
             [Invcd. Units MTD] is not null or
             [Invcd. Orders MTD] is not null or  
             [Invcd. Sales YTD] is not null or
             [Invcd. Sales Prior Year YTD] is not null or
             [Invcd. Units YTD] is not null or 
             [Invcd. Orders YTD] is not null or 
             [Invcd. SalesCredit Yesterday] is not null)
      


UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder = 1000
WHERE (CPG = 'TSFL') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder = 900
WHERE (CPG = 'MEDIFAST') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder = 800
WHERE (CPG = 'MWCC') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder = 700
WHERE (CPG = 'FRANCHISE') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder = 600
WHERE (CPG = 'DOCTORS') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder = -100
WHERE (CPG = 'CORPHEALTH') AND (SortOrder = 0)



/****** SECTION 8:  HARDCODED SORT ORDER OF CHANNEL PER DOMINIC ****************/


UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 1000
WHERE (SalesSrce = 'AUTO') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 900
WHERE (SalesSrce = 'CC') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 800
WHERE (SalesSrce = 'WEB') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 700
WHERE (SalesSrce = 'POS') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 600
WHERE (SalesSrce = 'FRANCHISE') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 500
WHERE (SalesSrce = 'WEST') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 400
WHERE (SalesSrce = 'CCDOC') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 300
WHERE (SalesSrce = 'WEBDOC') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel_SalesCube]
SET SortOrder2 = 200
WHERE (SalesSrce = 'TSFL') AND (SortOrder2 = 0)


/****** SECTION 9:  REPORT DURATION OF QUERY ***********************************/

SET @ProcessTimeEnd = GetDate()
PRINT 'Elapsed time: '
PRINT DATEDIFF(SS,@ProcessTimeBegin,@ProcessTimeEnd)
   
      
      

      
      

      





