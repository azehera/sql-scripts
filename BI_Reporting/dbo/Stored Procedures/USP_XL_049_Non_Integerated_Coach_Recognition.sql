﻿
/*
===============================================================================
Author:         Menkir Haile
Create date: 09/08/2017

==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[ODYSSEY_ETL]       [dbo].[ODYSSEY_BUSINESSCENTER] 				Select   
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                    Select  
[ODYSSEY_ETL]       [dbo].[ODYSSEY_RANK_SCHEME_DETAILS]         Select
=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
              

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/


CREATE procedure [dbo].[USP_XL_049_Non_Integerated_Coach_Recognition] 
 @M1EndDate Date
 as 
set nocount on ;

DECLARE @StartDate Date
--Declare @M1EndDate DATE = '7/31/2017'

Declare @PrevMonthStartDate Date
Declare @PrevMonthEndDate DATE

Declare @LastMonthStartDate Date
Declare @LastMonthEndDate Date

Set @StartDate=(select DATEADD(MONTH, DATEDIFF(MONTH, 0,  @M1EndDate), 0)) 
--Set @EndDate=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)) 

Set @PrevMonthStartDate=(select DATEADD(MONTH, DATEDIFF(MONTH, 0,  @M1EndDate)-1, 0))
Set @PrevMonthEndDate=(select DATEADD(MONTH, DATEDIFF(MONTH, -1,  @M1EndDate)-1, -1)) 

Set @LastMonthStartDate=(select DATEADD(MONTH, DATEDIFF(MONTH, 0,  @M1EndDate)-2, 0))
Set @LastMonthEndDate=(select DATEADD(MONTH, DATEDIFF(MONTH, -1,  @M1EndDate)-2, -1))

------------------------------Pull only active Co-Apps into a Co-App Temp Table-----------------------------------
IF OBJECT_ID('Tempdb..#Coapp') IS NOT NULL DROP TABLE #Coapp
Select *
Into #Coapp
	From [ODYSSEY_ETL].[dbo].[ODYSSEY_CO_APPLICANT]
		Where [IS_OBSOLETE] is null		
-----------------------Pull Rank ups with contact info---------------------------------------------
IF OBJECT_ID('Tempdb..#NonIntegratedCoach') IS NOT NULL DROP TABLE #NonIntegratedCoach
SELECT	A.[CUSTOMER_NUMBER] as [Coach ID]
		,A.[CUSTOMER_ID]
		,A.[FIRST_NAME]as [First Name]
		,A.[LAST_NAME]as [Last Name]
		,isnull(Co.[LAST_NAME],'')  as [Co-App Last Name]
		,isnull(Co.[FIRST_NAME],'') as [Co-App First Name]
		,[Recognition_Name]
		,A.[EMAIL1_ADDRESS] as [Coach_Email]
		,A.[MAIN_PHONE]
		,CA.[ADDRESS1]
		,CA.[ADDRESS2]
		,CA.[CITY]
		,CA.[STATE]
		,CA.[POSTAL_CODE] 
		,CASE WHEN C.[RANK_ADVANCE_DATE] = C.[COMMISSION_PERIOD] THEN CONVERT(Varchar(10),C.[COMMISSION_PERIOD],110) END as [Month 1 Rank Advance]
		,C.[RANK] as [Month 1 Rank]
		,Rs.[RANK_ID] as [Month 1 Rank #]
		,CASE WHEN C2.[RANK_ADVANCE_DATE] = C2.[COMMISSION_PERIOD] THEN CONVERT(Varchar(10),C2.[COMMISSION_PERIOD],110) END as [Month 2 Rank Advance]
		,C2.[RANK] AS [Month 2 Rank]
		,Rs2.RANK_ID AS [Month 2 Rank #]
		,CASE WHEN C3.[RANK_ADVANCE_DATE] = C3.[COMMISSION_PERIOD] THEN CONVERT(Varchar(10),C3.[COMMISSION_PERIOD],110) END as [Month 3 Rank Advance]
		,C3.[RANK] AS [Month 3 Rank]
		,Rs3.RANK_ID AS [Month 3 Rank #]

INTO #NonIntegratedCoach
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] A
	Inner JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] C
			ON A.[CUSTOMER_ID] = C.[CUSTOMER_ID] 
				AND CAST(C.[COMMISSION_PERIOD] AS DATE) between @StartDate and @M1EndDate
	Left JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] C2
			ON A.[CUSTOMER_ID] = C2.[CUSTOMER_ID] 
				AND CAST(C2.[COMMISSION_PERIOD] AS DATE) between @PrevMonthStartDate AND @PrevMonthEndDate
	Left JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] C3
			ON A.[CUSTOMER_ID] = C3.[CUSTOMER_ID] 
				AND CAST(C3.[COMMISSION_PERIOD] AS DATE) between @LastMonthStartDate AND @LastMonthEndDate
	Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] RS
			ON C.[RANK] = RS.[RANK_NAME]
	Left Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] RS2
			ON C2.[RANK] = RS2.[RANK_NAME]
	Left Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] RS3
			ON C3.[RANK] = RS3.[RANK_NAME]
	Inner Join 	[ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CA
			ON  A.[MAIN_ADDRESS_ID] = ca.[ADDRESS_ID]
	left join #Coapp Co
			ON A.[CUSTOMER_ID] = Co.[CUSTOMER_ID]
Where  (C.[RANK_ADVANCE_DATE] = C.[COMMISSION_PERIOD] OR C2.[RANK_ADVANCE_DATE] = C2.[COMMISSION_PERIOD] OR C3.[RANK_ADVANCE_DATE] = C3.[COMMISSION_PERIOD])
		and Rs.RANK_ID IN (9, 9.5,10,10.5,11,11.5) and Rs2.RANK_ID IN (9, 9.5,10,10.5,11,11.5) and Rs3.RANK_ID IN (9,9.5,10,10.5,11,11.5) 
		 
	
SELECT * FROM #NonIntegratedCoach