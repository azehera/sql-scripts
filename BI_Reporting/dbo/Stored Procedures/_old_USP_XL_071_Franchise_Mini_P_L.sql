﻿
CREATE Procedure [dbo].[_old_USP_XL_071_Franchise_Mini_P&L] as
Set NoCount On;


/*
==================================================================================
Author:     Kalpesh Patel
Create date: 06/09/2014
---------------------------[USP_XL_071_Franchise_Mini_P&L-------------------
Provide Daily Franchise GL Account Transactions-----
==================================================================================   
REFERENCES
Database              Table/View/UDF                       Action            
----------------------------------------------------------------------------------
[Nav_Etl]            [Jason Pharm$G_L Entry]                Select
[BI_SSAS_Cubes]      [DimCalendar]              		    Select  
[BI_SSAS_Cubes]      [DimGLCategory]                        Select
[BI_Reporting]       XL_033_Finance_FranchiseZipGeoInfo     Select               
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------
2014-06-09    Luc Emond                Convert Script into Store Procedure

==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================
*/

-------DECLARE TIME FRAME FOR THE REPORT---
Declare @Period int
Set @Period = (Select CalendarMonthId from [BI_SSAS_Cubes].[dbo].[DimCalendar] where CalendarDate = DATEADD(yy,-1,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0))) -----First Day of Last Year


  
-----------RETRIEVE INFORMATION------


SELECT  
 1 AS EntryNO
,c.CalendarMonthID 
,a.[Source No_]
,b.[ShipToName]
,a.[G_L Account No_] COLLATE DATABASE_DEFAULT AS [G_L Account No_] 
,a.[Document No_] COLLATE DATABASE_DEFAULT + '' + a.[G_L Account No_]COLLATE DATABASE_DEFAULT AS DocumentID 
,a.[Document No_] COLLATE DATABASE_DEFAULT AS [Document No_] 
,a.[Global Dimension 1 Code] COLLATE DATABASE_DEFAULT AS [Global Dimension 1 Code] 
,a.[Source Code] COLLATE DATABASE_DEFAULT AS [Source Code] 
,CASE WHEN a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'FRANCHISE' THEN 'JASON PHARM FRANCHISE'
      WHEN a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'TSFL' THEN 'TSFL'
      WHEN a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'SALES' THEN 'WHOLESALE'
      ELSE a.[Global Dimension 2 Code] END AS [Global Dimension 2 Code] 
,a.[Transaction No_]
,b.[Center Name]
,SUM(a.[Amount]) * -1 AS NetTotal 
,NULL AS Budget$ 
,CASE WHEN a.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
      WHEN a.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
      ELSE 3 END AS AccountsCategory
,e.[G_L Name]
,e.[RollUpName] 
,b.[RegionName]

--FROM [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] a
FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$G_L Entry] a

   Left Join [BI_SSAS_Cubes].[dbo].[DimCalendar] c 
                   on DATEADD(D, 0,DATEDIFF(D, 0,a.[Posting Date])) = c.CalendarDate
        Join [BI_Reporting].[dbo].[XL_033_Finance_FranchiseZipGeoInfo] b
                   on case when len(b.[ShipToKey])=5 then LEFT(b.[ShipToKey],3)
                           when len(b.[ShipToKey])=6 then LEFT(b.[ShipToKey],4) else LEFT(b.[ShipToKey],4) end =a.[Source No_] COLLATE DATABASE_DEFAULT
        Join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
                   on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]
Where [Global Dimension 2 Code] = 'FRANCHISE'
          and   CalendarMonthID >= @Period
          and a.[G_L Account No_] >= '10000'
GROUP BY
 c.CalendarMonthID 
,a.[G_L Account No_] 
,a.[Document No_] 
,a.[Global Dimension 1 Code] 
,a.[Global Dimension 2 Code] 
,a.[Transaction No_] 
,b.[Center Name]
,a.[Source Code]
,a.[Source No_]
,b.[ShipToName]
,e.[G_L Name]
,e.[RollUpName] 
,b.[RegionName]

