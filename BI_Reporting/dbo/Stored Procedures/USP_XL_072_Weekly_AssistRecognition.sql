﻿




/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 01/20/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER_ADDRESS]		       Select   
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                     Select  
[ODYSSEY_ETL]       [dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]       Select

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
2014-01-22           Luc Emond        Added a new column in Numeric for Coach Id for V_Lookup  
2016-01-08			 Micah Williams   Created #results temp table to filter for ASSIST_BONUS >= 100
====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/

CREATE procedure [dbo].[USP_XL_072_Weekly_AssistRecognition] as 
set nocount on ;

Declare @StartDate Datetime
Declare @EndDate Datetime

Set @StartDate=(Select dateadd(wk, datediff(wk, 0, getdate()) - 1, 0))
Set @EndDate=(Select dateadd(wk, datediff(wk, 0, getdate()), -1))

------------------------------Pull only active Co-Apps into a Co-App Temp Table-----------------------------------
Select *
Into #Coapp
	From [ODYSSEY_ETL].[dbo].[ODYSSEY_CO_APPLICANT]
		Where [IS_OBSOLETE] is null
-----------------------------------------Pull Assist Bonuses----------------------------------
Select
	 A.[CUSTOMER_NUMBER] as [Coach ID]
	,Convert(numeric(36,0),(A.[CUSTOMER_NUMBER])) as V_LookupCoachID
	,A.[FIRST_NAME]as [First Name]
	,A.[LAST_NAME]as [Last Name]
	,isnull(Co.[LAST_NAME],'')  as [Co-App Last Name]
	,isnull(Co.[FIRST_NAME],'') as [Co-App First Name]
	,A.[RECOGNITION_NAME]
	,CA.[CITY]
    ,CA.[STATE]
    ,A.[EMAIL1_ADDRESS] as [Coach_Email]
	,E.ASSIST_BONUS
	,E.CAB_BONUS
	,convert(varchar (10),E.COMMISSION_DATE,110) as [Earn Date]
INTO #results

From [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]E
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
		on E.[CUSTOMER_ID] = A.[CUSTOMER_ID]
	--Inner join [BI_Reporting].[dbo].[calendar_TSFL] C 
	--	on convert(varchar (10),E.COMMISSION_DATE,121)= C.[CalendarDate]
	left join  #Coapp Co
		on A.[CUSTOMER_ID] = Co.[CUSTOMER_ID]
	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CA
		on A.[CUSTOMER_ID] = CA.[CUSTOMER_ID]
	Where E.ASSIST_BONUS > '0'
		and convert(varchar (10),E.COMMISSION_DATE,121) between @StartDate and @EndDate
		and CA.[ADDRESS_TYPE] = 'Main'
		--or E.CAB_BONUS > '0'
		--and convert(varchar (10),E.COMMISSION_DATE,121) between @StartDate and @EndDate
		--and CA.[ADDRESS_TYPE] = 'Main'
  Order by E.ASSIST_BONUS
		,E.CAB_BONUS
	
SELECT * 
FROM #results		
WHERE ASSIST_BONUS >= 100
 -------------------------Clean up Temp tables created-------------------
 Drop table #Coapp
 DROP TABLE #results


