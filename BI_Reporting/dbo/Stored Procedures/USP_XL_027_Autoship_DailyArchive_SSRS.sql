﻿
/*
=============================================================================================
Author:         Menkir Haile
Create date: 01/18/2015
-------------------------[dbo].[USP_XL_027_Autoship_DailyArchive] -----------------------------
Sales Tracker by Month----
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------

[BI_Reporting]        [USP_XL_027_Autoship_DailyArchive]          Select                         
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------

==========================================================================================
*/
Create PROCEDURE [dbo].[USP_XL_027_Autoship_DailyArchive_SSRS] AS 

SET NOCOUNT ON;

Select COUNT(*) as CountRows, GETDATE() as Date from [bi_reporting].DBO.XL_027_Autoship_DailyArchive






