﻿ 
----Pulling address, customer number,start date and original sponsor
CREATE PROCEDURE [dbo].[usp_TSFL_GetCoachDemographics_ByDate]

@CommPeriodStart AS DATETIME, @CommPeriodEnd AS DATETIME


AS

/***************************************************************************
Developer: Daniel Dagnachew
Date: 03/06/2015
Description: Used to get TSFL Health Coach demographics data for each commission
			Period.
---------------------------------------------------------------------------
REFERENCES
Database			Table/View/UDF								Action            
----------------------------------------------------------------------------------
ODYSSEY_ETL			[dbo].[ODYSSEY_BUSINESSCENTER]				Select
ODYSSEY_ETL			[dbo].[ODYSSEY_CUSTOMER]					Select
ODYSSEY_ETL			[dbo].[ODYSSEY_CUSTOMER_ADDRESS]			Select
ODYSSEY_ETL			[dbo].[ODYSSEY_CUSTOMER_TYPE]				Select
ODYSSEY_ETL			[dbo].[ODYSSEY_CUSTOMER_HISTORY]			Select
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------

==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================

******************************************************************************/


SET NOCOUNT ON	

  
----Pulling address, customer number,start date and original sponsor
set nocount on;
select name from sys.objects;

 IF object_id('Tempdb..#Address') Is Not Null DROP TABLE #Address   
  SELECT distinct
       C.[CUSTOMER_ID]
      ,case when CUSTADD.[ADDRESS_TYPE] in ('SHIPPING','SHIPPING2') then Left([POSTAL_CODE],5)  else null end as [POSTAL_CODE]
	  ,convert(varchar(10),[EFFECTIVE_DATE] , 121) as [Start Date]
	  ,C.[ORIGINAL_SPONSOR]
	  ,C.CUSTOMER_NUMBER [CUSTOMER_NUMBER]
	  ,CAST(BC.[COMMISSION_PERIOD] AS DATE) COMMISSION_PERIOD
  Into #Address
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CUSTADD
		on CUSTADD.[CUSTOMER_ID] = C.[CUSTOMER_ID] and CUSTADD.[ADDRESS_TYPE] in ('SHIPPING','SHIPPING2')
	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_TYPE] CUTY
		on CUTY.[CUSTOMER_TYPE] = C.[CUSTOMER_TYPE] 
  WHERE [COMMISSION_PERIOD] > = @CommPeriodStart
		and [COMMISSION_PERIOD] < = @CommPeriodEnd  
		and CUTY.[CUSTOMER_TYPE] = 'D'
		
------Pulling Sponsor_id	 
IF object_id('Tempdb..#UPLINE10') Is Not Null DROP TABLE #UPLINE10
SELECT distinct C.[CUSTOMER_ID]
      ,[SPONSOR_ID] as Upline 
 into #UPLINE10  
 FROM  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
	 left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID] 
	inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CUSTADD
		on CUSTADD.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_TYPE] CUTY
		on CUTY.[CUSTOMER_TYPE] = C.[CUSTOMER_TYPE]
	WHERE CUTY.[CUSTOMER_TYPE] = 'D'
	   AND [COMMISSION_PERIOD] >  @CommPeriodStart
	   AND [COMMISSION_PERIOD] < = @CommPeriodEnd

-------Converting Sponsor_id to Customer_id
IF object_id('Tempdb..#UPLINE11') Is Not Null DROP TABLE #UPLINE11
 SELECT distinct #UPLINE10.CUSTOMER_ID , BC.CUSTOMER_ID Sponsor_ID
 into #Upline11
 FROM #UPLINE10 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
 on BC.BUSINESSCENTER_ID = #UPLINE10.Upline


-------Converting Sponsor_id to Customer_Number
IF object_id('Tempdb..#UPLINE') Is Not Null DROP TABLE #UPLINE
 SELECT distinct up10.CUSTOMER_ID , c.customer_number [current upline],CAST(BC.COMMISSION_PERIOD AS DATE) COMMISSION_PERIOD
 into #Upline
 FROM #UPLINE11 up10 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_customer] c
 on up10.sponsor_ID = c.customer_id
 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
 on BC.CUSTOMER_ID = c.CUSTOMER_ID
 
 
-------Pulling PRESIDENTIAL_DIRECTORS and FI_PRESIDENTIAL_DIRECTORS
IF object_id('Tempdb..#Presidential') Is Not Null DROP TABLE #Presidential 
Select * into #Presidential from (
select distinct #UPLINE11.CUSTOMER_ID ,isnull(C.Customer_number,null) President_Number,CAST(BC2.[COMMISSION_PERIOD] AS DATE)  COMMISSION_PERIOD
 From #UPLINE11 INNER JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
ON #UPLINE11.Sponsor_ID = BC.Customer_id
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC2
ON BC.SPONSOR_ID =BC2.BUSINESSCENTER_ID
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_Customer] C
ON C.Customer_ID =BC2.Customer_ID
WHERE BC2.[RANK] = ('FI_PRESIDENTIAL_DIRECTOR') 
and BC2.[RANK] <> ('PRESIDENTIAL_DIRECTOR') 
and cast(BC2.COMMISSION_PERIOD as date)> @CommPeriodStart 
and cast(BC2.COMMISSION_PERIOD as date) < = @CommPeriodEnd 

union all

Select distinct #UPLINE11.CUSTOMER_ID,isnull(C.Customer_number,null) President_Number,CAST(BC2.[COMMISSION_PERIOD] AS DATE)  COMMISSION_PERIOD
From #UPLINE11 INNER JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
ON #UPLINE11.Sponsor_ID = BC.Customer_id
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC2
ON BC.SPONSOR_ID =BC2.BUSINESSCENTER_ID
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_Customer] C
ON C.Customer_ID =BC2.Customer_ID
WHERE BC2.[RANK] <> ('FI_PRESIDENTIAL_DIRECTOR') 
and BC2.[RANK] = ('PRESIDENTIAL_DIRECTOR') 
and cast(BC2.COMMISSION_PERIOD as date)> @CommPeriodStart 
and cast(BC2.COMMISSION_PERIOD as date) < = @CommPeriodEnd ) as temp

-------Pulling current rank and highest achieved rank 
IF object_id('Tempdb..#ALL') Is Not Null DROP TABLE #ALL
	SELECT distinct C.[CUSTOMER_ID]
       ,[HIGHEST_ACHIEVED_RANK] [Highest Title]
       ,[RANK] [Current Title]
       ,CAST(BC.[COMMISSION_PERIOD] AS DATE)  COMMISSION_PERIOD
  Into #ALL
  FROM  #Address Ad
     inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		on Ad.[CUSTOMER_ID] = BC.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CUSTADD
		on CUSTADD.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_TYPE] CUTY
		on CUTY.[CUSTOMER_TYPE] = C.[CUSTOMER_TYPE]
	 Where BC.[COMMISSION_PERIOD]>@CommPeriodStart 
	   and BC.[COMMISSION_PERIOD]< =@CommPeriodEnd 
	   
-------Pulling new coaches for the specified date range
IF object_id('Tempdb..#NEWCOACH') Is Not Null DROP TABLE #NEWCOACH
SELECT distinct C.[CUSTOMER_ID]
      ,MIN([EFFECTIVE_DATE]) DRAFT_DATE,DATEADD(month, ((YEAR([EFFECTIVE_DATE]) - 1900) * 12) + MONTH([EFFECTIVE_DATE]), -1) COMMISSION_PERIOD
into #NEWCOACH  
 FROM  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CUSTADD
		on CUSTADD.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_TYPE] CUTY
		on CUTY.[CUSTOMER_TYPE] = C.[CUSTOMER_TYPE]
	 Where CUTY.[CUSTOMER_TYPE] = 'D'
	 GROUP BY C.[CUSTOMER_ID],[EFFECTIVE_DATE]
	 HAVING MIN([EFFECTIVE_DATE]) > @CommPeriodStart 
			AND MIN([EFFECTIVE_DATE]) < @CommPeriodEnd 

-------Pulling the count of new coaches for the specified date range
IF object_id('Tempdb..#COUNTNEWCOACH') Is Not Null DROP TABLE #COUNTNEWCOACH
SELECT distinct BC.[SPONSOR_ID] 
      ,COUNT(BC.[CUSTOMER_ID]) [NUMBER OF NEW COACHES SPONSORED],CAST([COMMISSION_PERIOD] AS DATE) [COMMISSION_PERIOD]
into #COUNTNEWCOACH  
 FROM  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CUSTADD
		on CUSTADD.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_TYPE] CUTY
		on CUTY.[CUSTOMER_TYPE] = C.[CUSTOMER_TYPE]
	 Where CUTY.[CUSTOMER_TYPE] = 'D'
	       AND BC.[CUSTOMER_ID] IN (SELECT #NEWCOACH.CUSTOMER_ID FROM #NEWCOACH  )
		   AND BC.[SPONSOR_ID] IN (SELECT DISTINCT BC.[BUSINESSCENTER_ID]  FROM  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC  WHERE BC.CUSTOMER_ID IN(SELECT #Address.CUSTOMER_ID FROM #Address ))
		   AND [COMMISSION_PERIOD] > = @CommPeriodStart
		   AND [COMMISSION_PERIOD] < = @CommPeriodEnd
	 GROUP BY BC.[SPONSOR_ID],CAST([COMMISSION_PERIOD] AS DATE)

-------Pulling the count of new coaches SPONSORED for the specified date range
IF object_id('Tempdb..#COUNTNEWCOACHESPONSORED') Is Not Null DROP TABLE #COUNTNEWCOACHESPONSORED
Select DISTINCT BC.CUSTOMER_ID,CNC.SPONSOR_ID,CNC.[NUMBER OF NEW COACHES SPONSORED],CNC.[COMMISSION_PERIOD]
INTO #COUNTNEWCOACHESPONSORED
FROM #COUNTNEWCOACH CNC
INNER JOIN  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		on CNC.[SPONSOR_ID] = BC.[BUSINESSCENTER_ID]
	
		

-------Pulling new customers for the specified date range
IF object_id('Tempdb..#NEWCUSTOMER') Is Not Null DROP TABLE #NEWCUSTOMER
SELECT distinct C.[CUSTOMER_ID]
      ,MIN([EFFECTIVE_DATE]) DRAFT_DATE,DATEADD(month, ((YEAR([EFFECTIVE_DATE]) - 1900) * 12) + MONTH([EFFECTIVE_DATE]), -1) COMMISSION_PERIOD
 into #NEWCUSTOMER  
 FROM  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CUSTADD
		on CUSTADD.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_TYPE] CUTY
		on CUTY.[CUSTOMER_TYPE] = C.[CUSTOMER_TYPE]
	 Where CUTY.[CUSTOMER_TYPE] = 'R'
	 GROUP BY C.[CUSTOMER_ID],[EFFECTIVE_DATE]
	 HAVING MIN([EFFECTIVE_DATE]) > @CommPeriodStart 
			AND MIN([EFFECTIVE_DATE]) < @CommPeriodEnd


-------Pulling the count of new customers for the specified date range			
IF object_id('Tempdb..#COUNTNEWCUSTOMER') Is Not Null DROP TABLE #COUNTNEWCUSTOMER
SELECT distinct BC.[SPONSOR_ID] 
      ,COUNT(BC.[CUSTOMER_ID]) [NUMBER OF NEW CUSTOMERS SPONSORED],CAST([COMMISSION_PERIOD] AS DATE) [COMMISSION_PERIOD]
 into #COUNTNEWCUSTOMER  
 FROM  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CUSTADD
		on CUSTADD.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_TYPE] CUTY
		on CUTY.[CUSTOMER_TYPE] = C.[CUSTOMER_TYPE]
	 Where CUTY.[CUSTOMER_TYPE] = 'R'
	       AND BC.[CUSTOMER_ID] IN (SELECT #NEWCUSTOMER.CUSTOMER_ID FROM #NEWCUSTOMER  )
		   AND BC.[SPONSOR_ID] IN (SELECT DISTINCT BC.[BUSINESSCENTER_ID]  FROM  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC  WHERE BC.CUSTOMER_ID IN(SELECT #Address.CUSTOMER_ID FROM #Address ))
		  AND  CAST([COMMISSION_PERIOD]AS DATE) > @CommPeriodStart
		and  CAST([COMMISSION_PERIOD]AS DATE) < @CommPeriodEnd
	 GROUP BY BC.[SPONSOR_ID],CAST([COMMISSION_PERIOD] AS DATE)
	 ORDER BY [COMMISSION_PERIOD]

-------Pulling the count of new customers SPONSORED for the specified date range	  
IF object_id('Tempdb..#COUNTNEWCUSTOMERSPONSORED') Is Not Null DROP TABLE #COUNTNEWCUSTOMERSPONSORED
Select DISTINCT BC.CUSTOMER_ID,CRC.[NUMBER OF NEW CUSTOMERS SPONSORED],CRC.[COMMISSION_PERIOD]
INTO #COUNTNEWCUSTOMERSPONSORED
FROM #COUNTNEWCUSTOMER CRC
INNER JOIN  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		on CRC.[SPONSOR_ID] = BC.[BUSINESSCENTER_ID]
	
		


-------Pulling the inactive date
IF object_id('Tempdb..#INACTIVEDATE') Is Not Null DROP TABLE #INACTIVEDATE
		SELECT distinct BC.[CUSTOMER_ID]
        ,MAX(CHO.CHANGED_DATE) [INACTIVE DATE]
  Into #INACTIVEDATE
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CUSTADD
		on CUSTADD.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_TYPE] CUTY
		on CUTY.[CUSTOMER_TYPE] = C.[CUSTOMER_TYPE]
	 inner join [ODYSSEY_ETL].dbo.ODYSSEY_CUSTOMER_HISTORY CHO
		on BC.[CUSTOMER_ID] = CHO.[CUSTOMER_ID]
	 WHERE [COMMISSION_PERIOD] > = @CommPeriodStart
		and[COMMISSION_PERIOD] < = @CommPeriodEnd
		and CHO.[CUSTOMER_TYPE] = 'I'
GROUP BY BC.[CUSTOMER_ID]

		
-----Joining all the temp tables to get the fields on the requirement
Select distinct  Ad.[CUSTOMER_NUMBER] CID
	   ,Ad.[COMMISSION_PERIOD]
	   ,Ad.[POSTAL_CODE]
	   ,Ad.[Start Date]
	   ,CAST(INAC.[INACTIVE DATE] AS DATE) [INACTIVE DATE]
	   ,Ad.[ORIGINAL_SPONSOR] [ORIGINAL SPONSOR CID]
	   ,Up.[current upline] [Current_Upline]
	   ,isnull(Pr.President_Number,null) as [Top of Line CID]
       ,Al.[Highest Title]
       ,Al.[Current Title]
	   ,ISNULL(CNH.[NUMBER OF NEW COACHES SPONSORED],0) [NUMBER OF NEW COACHES SPONSORED]
	   ,ISNULL(CNC.[NUMBER OF NEW CUSTOMERS SPONSORED],0) [NUMBER OF NEW CUSTOMERS SPONSORED]
	   ,'Coach' as [Relation Type]

From  #Address Ad
left join #Upline Up
	 on Up.[CUSTOMER_ID] = Ad.[CUSTOMER_ID]
	and Up.[COMMISSION_Period] = Ad.[COMMISSION_Period]
INNER join #All Al
	 on Up.[CUSTOMER_ID] = Al.[CUSTOMER_ID]
	and Up.[COMMISSION_Period] = AL.[COMMISSION_Period]
left join #Presidential Pr
 	  on Pr.[CUSTOMER_ID] = Al.[CUSTOMER_ID]
 	 and PR.[COMMISSION_Period] = AL.[COMMISSION_Period]
LEFT join #COUNTNEWCOACHESPONSORED CNH
	 on Pr.[CUSTOMER_ID] = CNH.[CUSTOMER_ID]
	 and Pr.[COMMISSION_Period] = CNH.[COMMISSION_Period]
LEFT join #COUNTNEWCUSTOMERSPONSORED CNC
	 on CNC.[CUSTOMER_ID] = CNH.[CUSTOMER_ID]
	 and CNC.[COMMISSION_Period] = CNH.[COMMISSION_Period]
LEFT join #INACTIVEDATE INAC
	 on INAC.[CUSTOMER_ID] = AD.[CUSTOMER_ID]
ORDER BY Ad.[COMMISSION_PERIOD]
 


 

