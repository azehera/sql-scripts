﻿
/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/24/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CORE_COMMISSION_ENTRY]     Select 
[ODYSSEY_ETL]          [dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY]     Select 
[ODYSSEY_ETL]          [dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]    Select 
[ODYSSEY_ETL]          [dbo].[ODYSSEY_BUSINESSCENTER]            Select 
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER_ADDRESS]          Select 

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
 --2015-04-17     Menkir Haile                DB-BIDB server reference is removed          

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================*/
CREATE  procedure [dbo].[USP_XL_077_Active_By_State] as 
set nocount on ;


IF object_id('tempdb..#A') is not null DROP TABLE #A


Declare @StartDatePrev Datetime
Declare @EndDatePrev Datetime

Set @StartDatePrev=(select DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0))
Set @EndDatePrev=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))

-------------------------------------Pulling Monthly Earners with Earnings--------------------------------------------
SELECT 
	   A.[CUSTOMER_ID]
      ,A.[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,isnull([TOTAL_ADJUSTMENT],0) + isnull([GROWTH_BONUS],0) + isnull([GENERATION_BONUS],0)+ isnull([OVERRIDE_BONUS],0) + isnull([CSC_AMOUNT],0) 
       + isnull([CAB_BONUS],0) + isnull([ASSIST_BONUS],0) + ISNULL([ROLLING_BONUS],0)+ isnull([OVERRIDE_BONUS_GED],0)+ isnull([OVERRIDE_BONUS_PED],0)
       + isnull([CERTIFIED_BONUS],0)+ Isnull([CUSTOMER_SUPPORT_BONUS],0)+ Isnull([FIBC],0)+ Isnull([ANSITION_ADJ],0) as [Total Earnings]
    Into #a
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] A
	inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] B
		on A.[COMMISSION_ENTRY_ID] = B.[COMMISSION_ENTRY_ID]
	Inner join [BI_Reporting].[dbo].[calendar] C 
		on Convert(varchar(10),A.[COMMISSION_DATE],121) = C.[CalendarDate]
	Where Convert(varchar(10),A.[COMMISSION_DATE],121) between @StartdatePrev and @EndDatePrev
	Order by [COMMISSION_DATE] asc
	

--------------------------Pulling Weekly Earnings for Active Earning Coaches--------------------------
Insert into #a
SELECT 
	  [CUSTOMER_ID]
	 ,[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,([CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]+[TOTAL_ADJUSTMENT]) as [Total Earnings]
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY] A
  Inner join [BI_Reporting].[dbo].[calendar] B 
		on Convert(varchar(10),A.[COMMISSION_DATE],121) = B.[CalendarDate]
  Where Convert(varchar(10),A.[COMMISSION_DATE],121) between @StartdatePrev and @EndDatePrev
	Order by [COMMISSION_DATE] asc

----------------------------------Select All Weekly and Monthly Earnings---------------------------------
Select
	 [STATE]
	 ,count(distinct([CUSTOMER_NUMBER])) as [Coaches]
From #a A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		on C.[CUSTOMER_ID] = BC.[CUSTOMER_ID]
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] B
		on C.[CUSTOMER_ID] = B. [CUSTOMER_ID]
	Where [Total Earnings] > '0'
		and [CUSTOMER_NUMBER] not in('2','6','6957001','16654601','9027001','16918401','30006445','101','3','16918501')
		and CONVERT(varchar(10),[Commission_Period],121) =@EndDatePrev
		and [ADDRESS_TYPE] = 'Main'
		and [LANGUAGE_CODE] = 'English'
	Group by [STATE]

