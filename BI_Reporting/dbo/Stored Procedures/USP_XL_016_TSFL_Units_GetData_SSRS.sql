﻿
--/*
--===============================================================================
--Author:         Emily Trainor
--Create date: 01/10/2012
---------------------------TSFL Tracker TSFL Sponsoring-------------------------
-------Pull data for TSFL Sponsoring Business Week Graph------------------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--BI_Reporting			XL_016_TSFL_LY_TY			               Select                       
--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------
--05/25/2012	  Emily Trainor					Updated to pull by order not SKU
--06/22/2012	  Emily Trainor					Updated to pull Business Week
--08/05/2015	  Menkir Haile                  Divide Current and Previous year
--												Units into separate Columns for SSRS
--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--===============================================================================
--*/

CREATE procedure [dbo].[USP_XL_016_TSFL_Units_GetData_SSRS] as 
set nocount on ;
-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = '01/01/' + Convert(varchar(4), DatePart(yyyy, DateAdd(yyyy, -1, GETDATE())))

---------------------Pulling TSFL Units Data ------------------------------------
IF object_id('tempdb..#Temp') is not null DROP TABLE #Temp
SELECT 
 [Calendar Week]
,[Calendar Year]
,sum([Units]) as Units
INTO #Temp
FROM [BI_Reporting].dbo.XL_016_TSFL_LY_TY
where [Posting Date] >= @DATE
and [Customer Posting Group] ='TSFL'
and [Type] = '2'
Group By 
 [Calendar Year]
,[Calendar Week]


---------------------Previous Year Data--------------------------
Select 
A.[Calendar Week]
,A.Units 
INTO #Prev
From #Temp A 
Where A.[Calendar Year] = YEAR(getdate()) -1

---------------------Current Year Data------------------------------
Select 
A.[Calendar Week]
,A.Units 
INTO #Curr
From #Temp A 
Where A.[Calendar Year] = YEAR(getdate())

-------------------SCombined Data for SSRS--------------------------------------------
Select P.[Calendar Week]
		,P.Units AS PreviousUnits
		,C.Units AS CurrentUnits
 from #Prev P 
Left Join #Curr C ON P.[Calendar Week] = C.[Calendar Week]

