﻿
--/*
--==================================================================================
--Author:         Emily Trainor
--Create date: 02/09/2012
---------------------------TSFL New Coach Business Volume Analysis------------------
--Calculate volume figures for New Coach Business in start-up 6 months of business--
--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--ODYSSEY_ETL			ODYSSEY_CUSTOMER					      Select
--ODYSSEY_ETL			ODYSSEY_BUSINESSCENTER				      Select   
--ODYSSEY_ETL			ODYSSEY_VOLUME						      Select    
--BI_Reporting				CALENDAR						      Select                          
--==================================================================================
--REVISION LOG
--Date           Name                          Change
------------------------------------------------------------------------------------

--==================================================================================
--NOTES:
------------------------------------------------------------------------------------
--==================================================================================
--*/

CREATE procedure [dbo].[usp_TSFL_NewCoachBiz] as 
set nocount on ;

SELECT 
 A.[CUSTOMER_ID]
,A.[CUSTOMER_TYPE]
,Convert (Varchar (10),A.[EFFECTIVE_DATE],121)as [EFFECTIVE_DATE]
,A.[CUSTOMER_NUMBER]
,B.[BUSINESSCENTER_ID]
,C.[NODE_ID]
,C.[VOLUME_TYPE]
,Sum(C.[VOLUME_VALUE]) as VolumeTotal
,Convert (Varchar (10),C.[PERIOD_END_DATE],121) As PERIOD_END_DATE 
INTO #a
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
     Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
      on A.[CUSTOMER_ID] = B.[CUSTOMER_ID]
     Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]C
      on B.[BUSINESSCENTER_ID] = C.[NODE_ID]
      and b.[COMMISSION_PERIOD]= c.[PERIOD_END_DATE]
Where [EFFECTIVE_DATE]>= (Select Convert (varchar(10), FirstDateOfYear,121) From [BI_Reporting].[dbo].[calendar] WHERE CalendarYear = (Select Datepart(yyyy, getdate())-3) Group By FirstDateOfYear)
      and C.[VOLUME_TYPE] in('FV', 'GV')
Group by 
 A.[CUSTOMER_NUMBER]
,C.[VOLUME_TYPE]
,A.[CUSTOMER_ID]
,A.[CUSTOMER_TYPE]
,B.[BUSINESSCENTER_ID]
,C.[NODE_ID]
,A.[EFFECTIVE_DATE]
,C.[PERIOD_END_DATE]
Order by A.[CUSTOMER_NUMBER]


			
Select 
 Customer_Id
,Customer_Type
,Effective_Date
,Customer_Number
,Businesscenter_Id
,Node_Id
,Volume_Type
,VolumeTotal
,Period_End_Date
,c.CalendarMonthId
,c1.CalendarMonthId As PeriodEndId
,Case When Right(c.CalendarMonthId,2)<= 7 Then c.CalendarMonthId +5
      When Right(c.CalendarMonthId,2) >=8 Then c.CalendarMonthId +93
      Else Null End As  [MonthId+6Months]       
Into #b
From #a a 
Inner Join [BI_Reporting].dbo.calendar c
      On a.Effective_date = c.CalendarDate
Inner Join [BI_Reporting].[dbo].[calendar_TSFL] c1
      On a.Period_End_Date = c1.CalendarDate



Select 
 Customer_Id
,Customer_Type
,Effective_Date
,Customer_Number
,Businesscenter_Id
,Node_Id
,Volume_Type
,VolumeTotal
,Period_End_Date
,CalendarMonthId
,PeriodEndId
,PeriodEndId - CalendarMonthId As Past
,[MonthId+6Months]
,DatePart(YYYY,Period_End_Date) As CalendarYear
,SUM(VolumeTotal) As Sales
--,Case When PeriodEndId >= CalendarMonthID And PeriodEndId <= [MonthId+6Months] Then SUM(VolumeTotal) Else '0' End As [Sales Last 6 Months]
--,Case When PeriodEndId < CalendarMonthID And PeriodEndId < [MonthId+6Months]   Then SUM(VolumeTotal) 
--      When PeriodEndId > CalendarMonthID And PeriodEndId > [MonthId+6Months]   Then SUM(VolumeTotal) Else '0' End As [Remaining Sales]
Into #c
From #b
Group By
 Customer_Id
,Customer_Type
,Effective_Date
,Customer_Number
,Businesscenter_Id
,Node_Id
,Volume_Type
,VolumeTotal
,Period_End_Date
,CalendarMonthId
,PeriodEndId
,[MonthId+6Months]
Order By  Effective_Date, Period_End_Date

      						
Select 
 Customer_Id
,Customer_Type
,Effective_Date
,Customer_Number
,Businesscenter_Id
,Node_Id
,Volume_Type
,VolumeTotal
,Period_End_Date
,CalendarMonthId
,PeriodEndId
,Case When (PeriodEndId - CalendarMonthId)+1 <=12 And PeriodEndId >= CalendarMonthID Then (PeriodEndId - CalendarMonthId)+1 
      When (PeriodEndId - CalendarMonthId)+1 > 12 And PeriodEndId >= CalendarMonthID Then (PeriodEndId - CalendarMonthId)-87  Else 0 End  As MonthlySales
,[MonthId+6Months]
,CalendarYear
,[Sales]
From #c

--------------------------Clean up of Temporary Tables Created--------------------------------
Drop table #a
Drop table #b
Drop table #c


