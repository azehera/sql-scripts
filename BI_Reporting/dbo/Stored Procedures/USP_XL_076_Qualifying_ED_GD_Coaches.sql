﻿
/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/24/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
--2015-04-17     Menkir Haile                DB-BIDB server reference is removed           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================*/
CREATE  procedure [dbo].[USP_XL_076_Qualifying_ED_GD_Coaches] as 
set nocount on ;


IF object_id('tempdb..#A') is not null DROP TABLE #A
SELECT 
	   CONVERT(varchar(10),[Commission_Period],121) as [Month]
	  ,count(distinct([CUSTOMER_NUMBER])) as [Qualifying GD or Higher]
Into #a
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
			on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Where CONVERT(varchar(10),[COMMISSION_PERIOD],121) >= GETDATE()-365
		and [RANK] in ('Global_Director','FI_Global_Director','Presidential_Director','FI_Presidential_Director')
			Group by 
				 [Commission_Period]
			Order by 
				[Commission_Period] asc

IF object_id('tempdb..#B') is not null DROP TABLE #B				
SELECT 
	   CONVERT(varchar(10),[Commission_Period],121) as [Month]
	  ,count(distinct([CUSTOMER_NUMBER])) as [Qualifying ED or Higher]
 into #b
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
			on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Where CONVERT(varchar(10),[COMMISSION_PERIOD],121) >= GETDATE()-365
		and [RANK] not in ('NONE','HEALTH_COACH','FT_HEALTH_COACH','Senior_Coach','MANAGER','ASSOCIATE_DIRECTOR','DIRECTOR')
			Group by 
				 [Commission_Period]
			Order by 
				[Commission_Period] asc
				
Select 
	A.[Month]
	,[Qualifying GD or Higher]
	,[Qualifying ED or Higher]
From #a A
	join #b B
	on A.Month = B.Month