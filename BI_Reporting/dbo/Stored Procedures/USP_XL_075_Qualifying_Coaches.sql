﻿/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/24/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  
[ODYSSEY_ETL]          [dbo].[ODYSSEY_BUSINESSCENTER]            Select
=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================*/
CREATE  procedure [dbo].[USP_XL_075_Qualifying_Coaches] as 
set nocount on ;	
Select
	convert(varchar(10),[COMMISSION_PERIOD],121) as [Bonus Month]
	,COUNT(distinct([Customer_Number])) as [Qualifying Coaches]
	FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
		on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]
Where  convert(varchar(10),[COMMISSION_PERIOD],121)>= convert(varchar(10),GETDATE()-365,121)
	and [RANK] not in ('NONE','HEALTH_COACH','FT_HEALTH_COACH')
	Group by 
		[COMMISSION_PERIOD]
	Order by[COMMISSION_PERIOD]
