﻿







CREATE Procedure [dbo].[USP_XL_040_LY_Q4_MWCC]
as

declare @EndYear Datetime

Set @EndYear=(Select distinct [LastDateOfYear] from BI_Reporting.dbo.calendar
where CalendarYear=(YEAR(Getdate()-1)-1))


EXECUTE AS LOGIN = 'ReportViewer'

Truncate table BI_Reporting.dbo.[XL_040_LY_Q4_MWCC]



-----------------------------------------------Jason Prop--------------------------------------------------------------

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4_MWCC](Entity,[G_L Account No_], [G_L Name], [RollUpName],[Global Dimension 1 Code], [Amount]) 
SELECT 
'Jason Prop' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[Global Dimension 1 Code]
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Properties$G_L Entry]  a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]  
Where [Posting Date] <= @EndYear
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[Global Dimension 1 Code]


UPDATE BI_Reporting.dbo.[XL_040_LY_Q4_MWCC] 
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].dbo.[Jason Properties$G_L Account] d
WHERE [XL_040_LY_Q4_MWCC].[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.[XL_040_LY_Q4_MWCC] where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.[XL_040_LY_Q4_MWCC] SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4_MWCC] SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4_MWCC] SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4_MWCC] SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4_MWCC](Entity,[G_L Account No_], [G_L Name], [RollUpName],[Global Dimension 1 Code], [Amount]) 
SELECT
Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,' ' as  [Global Dimension 1 Code]
,[Current Year Retained Earnings] as Amount
from BI_Reporting.dbo.XL_040_BalanceSheetLY
where Entity='Jason Prop'




delete from BI_Reporting.dbo.[XL_040_LY_Q4_MWCC]
where [G_L Account No_]='34400' and RollUpName is null




