﻿

/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 01/14/2014
-----------------------------dbo.BeSlim_Cancellation-------------------
Create Daily Flash reports from Cube data
--==================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------
prdmartini_STORE_repl    [dbo].[v_cart_header]                    Select
                        
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------
8/17/2016      Menkir Haile					Database reference is changed from Ecomm_ETL 
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE procedure [dbo].[USP_BeSlim_Cancellation]
as 
Declare @yesterday datetime
Declare @FirstdayOfTheMonth datetime
set @FirstdayOfTheMonth = DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)
set @yesterday =  dateadd(day,datediff(day,1,GETDATE()),0)

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
select count(cst.USA_CUST_NBR) Web,CAST(A.CTH_MODIFY_DT as DATE)[Date]
into #A 
from prdmartini_STORE_repl.dbo.v_cart_header A (nolock) 
join prdmartini_STORE_repl.dbo.USER_ACCOUNT cst (nolock) on cst.USA_ID = CTH_CREATED_FOR
join prdmartini_STORE_repl.dbo.USER_ACCOUNT chgBy (nolock) on chgBy.USA_ID = CTH_MODIFIED_BY
where A.CTH_MODIFY_DT < = @yesterday and A.CTH_MODIFY_DT  > = @FirstdayOfTheMonth and A.CTH_TYPE_CD = 'BSL' and chgBy.USA_TYPE_CD = 'B2B'
and A.CTH_STATUS_CD = 'I' 
group by CAST(A.CTH_MODIFY_DT as DATE)


IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
select count(cst.USA_CUST_NBR) CCA,CAST(A.CTH_MODIFY_DT as DATE)[Date]
into #B 
from prdmartini_STORE_repl.dbo.v_cart_header A (nolock) 
join prdmartini_STORE_repl.dbo.USER_ACCOUNT cst (nolock) on cst.USA_ID = CTH_CREATED_FOR
join prdmartini_STORE_repl.dbo.USER_ACCOUNT chgBy (nolock) on chgBy.USA_ID = CTH_MODIFIED_BY
where A.CTH_MODIFY_DT < = @yesterday and A.CTH_MODIFY_DT  > = @FirstdayOfTheMonth and A.CTH_TYPE_CD = 'BSL' and chgBy.USA_TYPE_CD = 'CSR'
and A.CTH_STATUS_CD = 'I' 
group by CAST(A.CTH_MODIFY_DT as DATE)


Select #A.[Date], #A.Web as Web,#B.CCA as CCA 
from #A join #B
on #A.[Date]= #B.[Date]
