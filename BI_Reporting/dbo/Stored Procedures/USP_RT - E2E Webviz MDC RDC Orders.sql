﻿





/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                                      Action            
------------------------------------------------------------------------------------

NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line]            Select 
NAV_ETL.[dbo].[Jason Pharm$Sales Header]				  Select
 NAV_ETL.[dbo].[Jason Pharm$Sales Line]					  Select
 [DP-WMSPROD01\WMSPROD].[rpwbvprd].[dbo].[lens_ord]						  Select
 [DP-WMSPROD01\WMSPROD].[rpwmsprd].[dbo].[shipment]						  Select
 [DP-WMSPROD01\WMSPROD].[rpwmsprd].[dbo].[ord]							  Select
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - E2E Webviz MDC RDC Orders]
AS
DECLARE @DATE DATE = CONVERT(DATE, GETDATE() - 31);

IF OBJECT_ID('Tempdb..#temptable') IS NOT NULL
    DROP TABLE #temptable;
SELECT [Document No_],
       MIN([Shortcut Dimension 2 Code]) AS 'Type',
       CAST([Shipment Date] AS DATE) AS [Date]
INTO #temptable
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] WITH (NOLOCK)
WHERE [Location Code] = 'RDCWMS'
      AND [Shipment Date] >= @DATE
GROUP BY [Document No_],
         [Shipment Date];

INSERT INTO #temptable
SELECT b.[Document No_],
       MIN(b.[Shortcut Dimension 2 Code]) AS 'Type',
       CAST(a.[Shipment Date] AS DATE) AS [Date]
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Header] a WITH (NOLOCK)
    JOIN NAV_ETL.[dbo].[Jason Pharm$Sales Line] b
        ON a.No_ = b.[Document No_]
           AND a.[Document Type] = 1
WHERE b.[Location Code] = 'RDCWMS'
      AND b.[Shipment Date] >= @DATE
GROUP BY b.[Document No_],
         a.[Shipment Date];




SELECT 'RDC' AS System,
       [Document No_] COLLATE DATABASE_DEFAULT AS [Document No],
       Type,
       [Date]
FROM #temptable
UNION ALL
SELECT DISTINCT
       [System] = 'Webviz',
       stcust AS [ID],
       CASE
           WHEN cpotyp = 'TSFL' THEN
               'TSFL_Orders'
           WHEN cpotyp = 'MEDI' THEN
               'Direct_Orders'
           ELSE
               cpotyp COLLATE DATABASE_DEFAULT
       END AS 'Type',
       CAST(adddte AS DATE) AS [Date]
FROM [DP-WMSPROD01\WMSPROD].[rpwbvprd].[dbo].[lens_ord] WITH (NOLOCK)
WHERE CAST(adddte AS DATE) >= @DATE
UNION ALL
SELECT DISTINCT
       [System] = 'MDC',
       sh.ship_id [ID],
       CASE
           WHEN ord.cpotyp = 'TSFL' THEN
               'TSFL_Orders'
           WHEN ord.cpotyp = 'MEDI' THEN
               'Direct_Orders'
           ELSE
               ord.cpotyp COLLATE DATABASE_DEFAULT
       END AS 'Type',
       CAST(sh.adddte AS DATE) AS [Date]
FROM [DP-WMSPROD01\WMSPROD].[rpwmsprd].[dbo].[shipment] sh WITH (NOLOCK)
    JOIN [DP-WMSPROD01\WMSPROD].[rpwmsprd].[dbo].[ord] ord WITH (NOLOCK)
        ON ord.ordnum = sh.ship_id
WHERE CAST(sh.adddte AS DATE) >= @DATE
UNION ALL
SELECT DISTINCT
       [System] = 'MDC',
       sh.ship_id [ID],
       CASE
           WHEN ord.cpotyp = 'TSFL' THEN
               'TSFL_Orders'
           WHEN ord.cpotyp = 'MEDI' THEN
               'Direct_Orders'
           ELSE
               ord.cpotyp COLLATE DATABASE_DEFAULT
       END AS 'Type',
       CAST(sh.adddte AS DATE) AS [Date]
FROM [DP-WMSPROD01\WMSPROD].[rpwmsprdmdc].[dbo].[shipment] sh WITH (NOLOCK)
    JOIN [DP-WMSPROD01\WMSPROD].[rpwmsprdmdc].[dbo].[ord] ord WITH (NOLOCK)
        ON ord.ordnum = sh.ship_id
WHERE CAST(sh.adddte AS DATE) >= @DATE;

/*
 
DECLARE @DATE DATE = CONVERT(DATE, GETDATE()-31)

IF OBJECT_ID('Tempdb..#temptable') IS NOT NULL DROP TABLE #temptable;
SELECT 	[Document No_],
	    min([Shortcut Dimension 2 Code]) as 'Type',
		CAST([Shipment Date] AS DATE) AS [Date]
  INTO  #temptable
  FROM  NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] with (nolock)
 WHERE  [Location Code] = 'RDCWMS'
   AND  [Shipment Date] >= @DATE
GROUP BY [Document No_] ,[Shipment Date] 

INSERT  INTO #temptable
SELECT  b.[Document No_],
	    min(b.[Shortcut Dimension 2 Code]) as 'Type',
		CAST(a.[Shipment Date] AS DATE) AS [Date]
  FROM  NAV_ETL.[dbo].[Jason Pharm$Sales Header] a with (nolock)
  JOIN  NAV_ETL.[dbo].[Jason Pharm$Sales Line] b ON a.No_ = b.[Document No_] AND a.[Document Type] = 1
 WHERE  b.[Location Code] = 'RDCWMS'
   AND  b.[Shipment Date] >= @DATE
GROUP BY b.[Document No_],a.[Shipment Date]




SELECT  'RDC' AS System ,
        [Document No_] COLLATE DATABASE_DEFAULT AS [Document No],
        Type ,
        [Date]
FROM    #temptable


UNION ALL

SELECT  DISTINCT [System] = 'Webviz' ,
        stcust AS [ID] ,
        CASE WHEN cpotyp = 'TSFL' THEN 'TSFL_Orders'
             WHEN cpotyp = 'MEDI' THEN 'Direct_Orders'
             WHEN cpotyp NOT IN ( 'MEDI', 'TSFL' ) THEN 'Other_Orders'
        END AS 'Type',
		CAST(adddte AS DATE) AS [Date]
FROM    [DP-WMSPROD01\WMSPROD].[rpwbvprd].[dbo].[lens_ord] with (nolock)
WHERE   CAST(adddte AS DATE) >= @DATE
UNION ALL
SELECT DISTINCT [System] = 'MDC', 
		sh.ship_id [ID],
		CASE WHEN ord.cpotyp = 'TSFL' THEN 'TSFL_Orders'
             WHEN ord.cpotyp = 'MEDI' THEN 'Direct_Orders'
             WHEN ord.cpotyp NOT IN ( 'MEDI', 'TSFL' ) THEN 'Other_Orders'
        END AS 'Type',
		CAST(sh.adddte AS DATE) AS [Date]
FROM  [DP-WMSPROD01\WMSPROD].[rpwmsprd].[dbo].[shipment] sh with (nolock)
JOIN [DP-WMSPROD01\WMSPROD].[rpwmsprd].[dbo].[ord] ord with (nolock) ON ord.ordnum = sh.ship_id
 WHERE 	CAST(sh.adddte as date) >= @DATE

UNION ALL
SELECT DISTINCT [System] = 'MDC', 
		sh.ship_id [ID],
		CASE WHEN ord.cpotyp = 'TSFL' THEN 'TSFL_Orders'
             WHEN ord.cpotyp = 'MEDI' THEN 'Direct_Orders'
             WHEN ord.cpotyp NOT IN ( 'MEDI', 'TSFL' ) THEN 'Other_Orders'
        END AS 'Type',
		CAST(sh.adddte AS DATE) AS [Date]
FROM  [DP-WMSPROD01\WMSPROD].[rpwmsprdmdc].[dbo].[shipment] sh with (nolock)
JOIN [DP-WMSPROD01\WMSPROD].[rpwmsprdmdc].[dbo].[ord] ord with (nolock) ON ord.ordnum = sh.ship_id
 WHERE 	CAST(sh.adddte as date) >= @DATE


 */



