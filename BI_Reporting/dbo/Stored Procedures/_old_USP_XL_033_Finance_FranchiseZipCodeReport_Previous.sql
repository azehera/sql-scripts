﻿
/*
=============================================================================================
Author: Kalpesh Patel    
Create date: 06/21/2013
-------------------------[USP_XL_033_Finance_FranchiseZipCodeReport]-----------------------------
Account Payable information ytd by Month---part of the closing process----
=============================================================================================   
*/

CREATE PROCEDURE [dbo].[_old_USP_XL_033_Finance_FranchiseZipCodeReport_Previous] As
SET NOCOUNT ON;	

----BRING IN THE DATA FROM SALES CUBE FACT TABLE-----	
Truncate table dbo.XL_033_Finance_Franchise_Customers_Miles

DECLARE @StartId Int, @EndID Int, @Latitude Decimal(18,6), @Longitude Decimal(18,6), @Region Char(35),@InputZipCode varchar(20),
@ShipToKey Varchar(10),@ShipToName varchar (35), @Radius int

Set @StartId = (Select MIN(ID) from [dbo].[XL_033_Finance_FranchiseZipGeoInfo])
Set @EndID =   (Select MAX(ID) from [dbo].[XL_033_Finance_FranchiseZipGeoInfo])

Set @StartId = @StartId
while @EndId = @EndId

Begin
Begin 

Set @Latitude = (Select Latitude from [dbo].[XL_033_Finance_FranchiseZipGeoInfo] where  ID = @StartId)
Set @Longitude = (Select Longitude from [dbo].[XL_033_Finance_FranchiseZipGeoInfo] where  ID = @StartId)
Set @InputZipCode = (Select [Zip5]from [dbo].[XL_033_Finance_FranchiseZipGeoInfo] where  ID = @StartId)
Set @Region=(Select [RegionName] from [dbo].[XL_033_Finance_FranchiseZipGeoInfo] where  ID = @StartId)
Set @ShipToKey =(Select ShipToKey from [dbo].[XL_033_Finance_FranchiseZipGeoInfo] where  ID = @StartId)
Set @ShipToName=(Select ShipToName from [dbo].[XL_033_Finance_FranchiseZipGeoInfo] where ID = @StartId)
Set @Radius = (Select [MilePayOut]  FROM [dbo].[XL_033_Finance_FranchiseZipGeoInfo] where ID = @StartId)


----BRING IN THE SALES INFO WITH THE CALCULATION OF MILES FROM THE LONG AND LAT------
Insert into dbo.XL_033_Finance_Franchise_Customers_Miles
SELECT 
Distinct [Ship-to Address]
,[Ship-to City]
,[State]
,[Zip5]
,[Country]
,[Latitude]
,[Longitude]
,@Radius  as BaseDistance
,@InputZipCode as CenterZip
,(3958*3.1415926*
sqrt((Latitude-(@Latitude))*
(Latitude-(@Latitude)) + 
cos(Latitude/57.29578)*
cos((@Latitude)/57.29578)*
(Longitude-(@Longitude))*
(Longitude-(@Longitude)))/180) as Miles
,@Region as Region
,@ShipToKey AS ShipToKey
,@ShipToName as ShipToName
,@Latitude as FranchiseLatitude
,@Longitude as FranchiseLongitude
FROM dbo.XL_033_Finance_Franchise_Customers
WHERE 
(3958*3.1415926*
sqrt((Latitude-(@Latitude))*
(Latitude-(@Latitude)) + 
cos(Latitude/57.29578)*
cos((@Latitude)/57.29578)*
(Longitude-(@Longitude))*
(Longitude-(@Longitude)))/180) 
<= @Radius 


Set @StartId = @StartId+1
end 

if (@StartId>@Endid)
break 
else 
Continue
end

------------------------------------------------MasterData---------------------------------------------------------
--EXECUTE AS LOGIN = 'ReportViewer'
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select LEFT ([Ship-to Post Code],5) as Zip5
,Sum(Amount) as Amount
,Calendar.CalendarYear
,Calendar.CalendarMonthName
,Calendar.CalendarQuarter
,Calendar.CalendarMonthID
,'INVOICE' As Type
,Case when SalesInvLine2.Type=0 then Sum([Amount])else 0 end as Type0
,Case when SalesInvLine2.Type=1 then Sum([Amount])else 0 end as Type1
,Case when SalesInvLine2.Type=2 then Sum([Amount])else 0 end as Type2
,Case when SalesInvLine2.Type=3 then Sum([Amount])else 0 end as Type3
into #A 
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] SalesInvLine2 with (nolock)
       INNER JOIN NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Header] SalesInvHeader2 with (nolock)
       ON SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
     join dbo.calendar as Calendar
     on  SalesInvHeader2.[Posting Date] = Calendar.CalendarDate
where LEFT ([Ship-to Post Code],5) in (Select distinct Zip5 from dbo.XL_033_Finance_Franchise_Customers_Miles where state in ('AZ','CA','LA','MD','MN','PA','TX','VA','WI'))
and [Customer Posting Group]='Medifast'
and SalesInvHeader2.[Posting Date]>=(SELECT DATEADD(YY, DATEDIFF (YY,0,GETDATE()-1),0))---First Of Year----
Group by LEFT ([Ship-to Post Code],5),Calendar.CalendarYear, Calendar.CalendarMonthName, Calendar.CalendarQuarter, Calendar.CalendarMonthID,[Type]


Insert into #A
Select LEFT ([Ship-to Post Code],5) as Zip5
,Sum((Amount * -1)) as Amount
,Calendar.CalendarYear
,Calendar.CalendarMonthName
,Calendar.CalendarQuarter
,Calendar.CalendarMonthID
,'CREDIT' As [Type] 
,Case when SalesInvLine2.Type=0 then Sum((Amount * -1))else 0 end as Type0
,Case when SalesInvLine2.Type=1 then Sum((Amount * -1))else 0 end as Type1
,Case when SalesInvLine2.Type=2 then Sum((Amount * -1))else 0 end as Type2
,Case when SalesInvLine2.Type=3 then Sum((Amount * -1))else 0 end as Type3
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Cr_Memo Line] SalesInvLine2 with (nolock)
    INNER JOIN NAV_ETL.[dbo].[Jason Pharm$Sales Cr_Memo Header] SalesInvHeader2 with (nolock)
     ON SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
join dbo.calendar as Calendar
on  SalesInvHeader2.[Posting Date] = Calendar.CalendarDate
where LEFT ([Ship-to Post Code],5) in (Select distinct Zip5 from dbo.XL_033_Finance_Franchise_Customers_Miles where state in ('AZ','CA','LA','MD','MN','PA','TX','WI'))
and [Customer Posting Group]='Medifast'
and SalesInvHeader2.[Posting Date]>=(SELECT DATEADD(YY, DATEDIFF (YY,0,GETDATE()-1),0))---First Of Year----
Group by LEFT ([Ship-to Post Code],5),Calendar.CalendarYear, Calendar.CalendarMonthName, Calendar.CalendarQuarter, Calendar.CalendarMonthID,[Type]

----temp table for all the zip code-----
IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
Select distinct Zip5,Region,State 
into #B 
from dbo.XL_033_Finance_Franchise_Customers_Miles

----master data------
IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master
Select
 a.Zip5
,Region
,State as ShipToState
,CalendarYear
,CalendarMonthName
,CalendarQuarter
,CalendarMonthID
,SUM(Type2) As Amount
into #Master
from #A a
join #B b
on a.Zip5=b.Zip5
Group by 
a.Zip5
,Region
,CalendarYear
,CalendarMonthName
,CalendarQuarter
,CalendarMonthID
,State

insert into #master
SELECT 
ZIP5
,Region
,ShipToState
,b.CalendarYear
,b.CalendarMonthName
,b.CalendarQuarter
,b.CalendarMonthID
,0 as Amount
FROM #Master a
cross JOIN dbo.calendar b
where b.CalendarYear=(select DATEPART(year, getdate()-1))---First Of Year----
group by
ZIP5
,Region
,b.CalendarYear
,b.CalendarMonthName
,b.CalendarQuarter
,b.CalendarMonthID
,ShipToState

--------FINAL REPORT---------
TRUNCATE TABLE dbo.XL_033_Finance_Franchise_PayoutReport 
INSERT INTO dbo.XL_033_Finance_Franchise_PayoutReport (
ZIP5
,Region
,ShipToState
,CalendarYear
,CalendarMonthName
,CalendarQuarter
,CalendarMonthID
,Amount)
select 
ZIP5
,Region
,ShipToState
,CalendarYear
,CalendarMonthName
,CalendarQuarter
,CalendarMonthID
,Amount
from #Master  



	









