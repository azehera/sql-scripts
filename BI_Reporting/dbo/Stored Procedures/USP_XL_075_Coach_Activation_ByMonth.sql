﻿/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/24/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
--2015-04-17     Menkir Haile                DB-BIDB server reference is removed           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================*/
CREATE  procedure [dbo].[USP_XL_075_Coach_Activation_ByMonth] as 
set nocount on ;
---------------------Pulling TSFL Coach Activation Data ------------------
Select 
 Convert (Varchar (10),[EFFECTIVE_DATE],121)as [EFFECTIVE_DATE]
,Count([CUSTOMER_ID])as COACHES_ACTIVATED
INTO #a 
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]
Where convert(varchar(10),[EFFECTIVE_DATE],121)>= convert(varchar(10),GETDATE()-365,121)
Group By [EFFECTIVE_DATE]
Order By [EFFECTIVE_DATE] asc

--------------Aggregation to Business Week and Year for Excel Template (Business Graphs)------
Select 
  [CalendarMonth] 
,[CalendarYear]
,sum([COACHES_ACTIVATED]) as COACHES_ACTIVATED
From #a A
inner join 
	[BI_Reporting].[dbo].[Calendar] B 
	on A.[EFFECTIVE_DATE]= B.[CalendarDate]
Group By 
 [CalendarYear]
,[CalendarMonth]
Order by 
 [CalendarYear]
,[CalendarMonth]

---clean up---
drop table #A