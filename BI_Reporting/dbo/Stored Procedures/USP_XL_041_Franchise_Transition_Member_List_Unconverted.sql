﻿


CREATE PROCEDURE [dbo].[USP_XL_041_Franchise_Transition_Member_List_Unconverted] as 

SET NOCOUNT ON;

/*
=============================================================================================
Author:      Luc Emond
Create date: 12/04/2013
-------------------------[dbo].[USP_XL_041_Franchise_Transition_Member_List_Unconverted] -----------------------------
Provide Monthly Unconverted Customer Information to Marketing Group                
===========================================================================================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------------------------------------------------------------------------------
[Book4Time_ETL]       [B4T_transaction_log_header]              Select
[Book4Time_ETL]       [B4T_transaction_log_Detail]              Select
[Book4Time_ETL]       [B4T_location]                            Select
[Book4Time_ETL]       [B4T_region_locations]                    Select
[Book4Time_ETL]       [B4T_region]                              Select
[Book4Time_ETL]       [B4T_Customer]                            Select
[Book4Time_ETL]       [B4T_Person]                              Select
[Book4Time_ETL]       [B4T_Address]                             Select
[Zip_codes_deluxe]    [ZipCodeDatabase_DELUXE]                  Select  
=============================================================================================================================================================
REVISION LOG
Date           Name                  Change
----------------------------------------------------------------------------------------------------------------------------------------------------------
08/21/2014	D. SMith				Added Via Mail to t-sql per ticket #1440178
08/21/2014	D. Smith				Made changes to allow for groupping on Customer ID. Removed the join to														dbo.B4T_Phone at the end and instead added updates to phone1, phone2, 
									and	phone3 after the grouping so that only one line would be displayed
11/30/2015  M.Haile					Duplicate Column(Phone2) is removed to as SSRS does not allow
==============================================================================================================================================================
NOTES:
--------------------------------------------------------------------------------

==================================================================================
*/

----DECLARE FOR DATE NEEDE FOR REPORT-----
Declare @Date1 datetime 
Set @Date1 ='2011-01-01' ----DATE PROVIDED BY JENNIFER CRUISE------ 


----MASTER LIST OF CUSTOMERS------
IF object_id('tempdb..#CustList') is not null DROP TABLE #CustList
Select 
 Person_Id As Customer_id
,Parent_Id
,First_Name
,Last_Name
,Email
,Via_Email
,via_mail
Into #CustList 
From [Book4Time_ETL].[dbo].[B4T_person]  h
Left Outer Join [Book4Time_ETL].[dbo].[B4T_Customer] c
    on h.Person_id = c.customer_id 
Group by 
 Person_Id
,Parent_id
,First_Name
,Last_Name
,Email
,Via_Email
,via_mail

----delete non customer info----
Delete #custList Where email like '%choose%' 
Delete #custList Where email like '%book%'
Delete #custList Where email like '%medifast%'
Delete #custList Where Customer_id Not in (select Customer_id from [Book4Time_ETL].[dbo].[B4T_Customer])


-----SALES ACTIVITY--------
IF object_id('tempdb..#A') is not null DROP TABLE #A
Select
h.Customer_id
,Parent_id
Into #a
From [Book4Time_ETL].[dbo].[B4T_transaction_log_header] h
Join [Book4Time_ETL].[dbo].[B4T_transaction_log_Detail] d
on h.transaction_id = d.transaction_id
Left Outer Join [Book4Time_ETL].[dbo].[B4T_Customer] c
    on h.Customer_id = c.customer_id     
WHERE (Select Convert(Varchar (10),Transaction_Date, 121))>= @Date1 and [Transaction_Type] In ('S', 'R') 
group by
h.Customer_id
,Parent_id

----DELETE UN NEEDED INFO----
Delete #custList Where customer_id In (Select customer_id From #A Group By Customer_id)

----ALTER TABLE AND UPDATES TO ADD CUSTOMER INFO----
ALTER TABLE #Custlist
ADD [Parent1] bigint, Street Char(50), City Char(25), Country Char(3), Postal_Code Char(15), State Char(25)

Update #Custlist
set [Parent1] = parent_id 
where #Custlist.customer_id = #Custlist.parent_id

UPDATE #Custlist
SET [Parent1] = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
where #Custlist.parent_id = p.customer_id
and [Parent1] is null

Update #CustList
Set Street = n.Street, City = N.City, Country = N.Country, Postal_Code = N.Postal_Code
From [Book4Time_ETL].[dbo].[B4T_Address] N
Where #custList.[Parent1] = N.address_id
and N.address_type='4'

Update #custList
Set State = z.State
From [Zip_codes_deluxe].dbo.[ZipCodeDatabase_DELUXE] Z
Where Left(#custList.Postal_Code,5)= z.[ZipCode] and z.primaryRecord ='p'

UPDATE #custlist
SET First_Name = pe.first_name, Last_Name = pe.Last_Name, Email = pe.Email
from [Book4Time_ETL].[dbo].[B4T_person]  pe
   where #custlist.[parent1] = pe.person_id
   
-----Clean Up Unwanted info-------
Delete #custList Where email like '%choose%' 
Delete #custList Where email like '%book%'
Delete #custList Where email like '%medifast%'

----FINAL----
IF object_id('tempdb..#B') is not null DROP TABLE #B
Select 
cu.Customer_Id
,cu.Parent_Id
,cu.[Parent1] As Parent_id_of_Parent_id
,cu.First_Name
,cu.Last_Name
,cu.Email
,'          ' as Phone1
,'          ' as Phone2
,'          ' AS Phone3
,cu.Street
,cu.City
,cu.State
,cu.Postal_Code
,lo.Location_Name As LocationName_FromAppointment
,Case When Via_Email =0 Then 'No' Else 'Yes' End Via_Email
,Case When Via_Mail =0 Then 'No' Else 'Yes' End as Via_Mail
into #B From #custList cu
Left Outer join [Book4Time_ETL].[dbo].[B4T_appointment] ap
on cu.Customer_id = ap.customer_id
Left outer Join [Book4Time_ETL].[dbo].[B4T_location]  Lo
    ON ap.location_id = Lo.location_id  
   Group by
cu.Customer_Id
,cu.Parent_Id
,cu.[Parent1] 
,cu.First_Name
,cu.Last_Name
,cu.Email
,cu.Street
,cu.City
,cu.State
,cu.Postal_Code
,lo.Location_Name 
,Via_Email
,via_mail
Order by customer_id ASC


--08/21/2014 - D.Smith  - Added update sttaemenst for Phone1, Phone2, and Phone3
UPDATE #B
SET Phone1 = LEFT(phone_number,10)
From #B 
Left Outer Join [Book4Time_ETL].dbo.B4T_Phone d
   on d.phone_id=Customer_id
WHERE phone_type=1

UPDATE #B
SET Phone2 = LEFT(phone_number,10)
From #B 
Left Outer Join [Book4Time_ETL].dbo.B4T_Phone d
   on d.phone_id=Customer_id
WHERE phone_type=2

UPDATE #B
SET Phone3 = LEFT(phone_number,10)
From #B 
Left Outer Join [Book4Time_ETL].dbo.B4T_Phone d
   on d.phone_id=Customer_id
WHERE phone_type=3   


Select cu.Customer_Id
,cu.Parent_Id
,Parent_id_of_Parent_id
,cu.First_Name
,cu.Last_Name
,cu.Email
,cu.Phone1
,cu.Phone2
--,cu.Phone2
,cu.Street
,cu.City
,cu.State
,cu.Postal_Code
,LocationName_FromAppointment
,Via_Email
,Via_mail
/* D. Smith 08/21/2014 Commeneted out as replaced by update statements */
--,Case when phone_type=1 then phone_number else ' ' end as Phone1
--,Case when phone_type=2 then phone_number else ' ' end as Phone2
--,Case when phone_type=3 then phone_number else ' ' end as Phone3
from #B cu
/* D. Smith 08/21/2014 Commeneted out as replaced by update statements */
 --Left Outer Join [Book4Time_ETL].dbo.B4T_Phone d
 --  on d.phone_id=cu.Customer_id

 
 

 



