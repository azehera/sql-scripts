﻿Create PROCEDURE [dbo].[usp_InventoryView]
AS

SELECT * FROM [NAV_ETL].[dbo].[Snapshot$NAV_Inventory] NavInv
  WHERE (NavInv.RunDate = CAST(FLOOR(CAST(GETDATE() AS float)) AS DATETIME)) 
  AND (NavInv.NavItemNo IS NOT NULL)  
  and [InvLocationType] <> 'MWCC'

