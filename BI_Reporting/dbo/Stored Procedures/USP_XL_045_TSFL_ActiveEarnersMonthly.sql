﻿
--/*
--==================================================================================
--Author:         Emily Trainor
--Create date: 05/04/2012
------------------------TSFL Active Earning Coaches - Monthly-----------------------
-------Calculate number of TSFL Coaches earning Compensation in a given month-------
--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--ODYSSEY_ETL			ODYSSEY_TSFL_COMM_ENTRY_WEEKLY		      Select   
--ODYSSEY_ETL			ODYSSEY_CORE_COMMISSION_ENTRY		      Select 
--ODYSSEY_ETL			ODYSSEY_TSFL_COMMISSION_ENTRY			  Select
--BI_Reporting				CALENDAR_TSFL								  Select
--NAV_ETL				Jason Pharm$Sales Invoice Line			  Select
--NAV_ETL				Jason Pharm$Sales Invoice Header		  Select
--NAV_ETL				Jason Pharm$Sales Cr_Memo Line            Select
--NAV_ETL				Jason Pharm$Sales Cr_Memo Header          Select            
--==================================================================================
--REVISION LOG
--Date           Name                          Change
------------------------------------------------------------------------------------
--11/11/2013	Emily Trainor			Added new ICP data fields to Earnings calculation
--==================================================================================
--NOTES: Added the ICP Data Fields [CERTIFIED_BONUS],[CUSTOMER_SUPPORT_BONUS],[FIBC],
-- and [ANSITION_ADJ] to the calculation of Monthly Earnings 
--
------------------------------------------------------------------------------------
--==================================================================================
--*/

CREATE procedure [dbo].[USP_XL_045_TSFL_ActiveEarnersMonthly] as 
set nocount on ;

-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = '2013-01-01'

-----------------------Delete Current Year Data--------------------------
Delete dbo.[Active Earning Coaches]
Where [Last Day of Month] >= @Date
-------------------------------------Pulling Monthly Earners with Earnings--------------------------------------------
SELECT 
	   A.[CUSTOMER_ID]
      ,A.[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),A.[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,CalendarYear
      ,CalendarMonth
      ,isnull([TOTAL_ADJUSTMENT],0) + isnull([GROWTH_BONUS],0) + isnull([GENERATION_BONUS],0)+ isnull([OVERRIDE_BONUS],0) + isnull([CSC_AMOUNT],0) 
       + isnull([CAB_BONUS],0) + isnull([ASSIST_BONUS],0) + ISNULL([ROLLING_BONUS],0)+ isnull([OVERRIDE_BONUS_GED],0)+ isnull([OVERRIDE_BONUS_PED],0)
       + isnull([CERTIFIED_BONUS],0)+ Isnull([CUSTOMER_SUPPORT_BONUS],0)+ Isnull([FIBC],0)+ Isnull([ANSITION_ADJ],0) as [Total Earnings]
    Into #a
 FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] A (NOLOCK)
	inner join[ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] B (NOLOCK)
		on A.[COMMISSION_ENTRY_ID] = B.[COMMISSION_ENTRY_ID]
	Inner join [BI_Reporting].[dbo].[calendar_TSFL] C (NOLOCK)
		on Convert(varchar(10),A.[COMMISSION_DATE],121) = C.[CalendarDate]
	  Where Convert(Varchar(10),A.[COMMISSION_DATE],121) >= @DATE
	  	and A.[CUSTOMER_ID] not in('9602206','9602208','9663528','9747036','9681355','9749475','9856645','9602209','9749476','9602205')
	Order by A.[COMMISSION_DATE] asc
	

--------------------------Pulling Weekly Earnings for Active Earning Coaches--------------------------
Insert into #a
SELECT 
	  [CUSTOMER_ID]
	  ,[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,CalendarYear
      ,CalendarMonth
      ,([CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]+[TOTAL_ADJUSTMENT]) as [Total Earnings]
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY] A (NOLOCK)
  Inner join [BI_Reporting].[dbo].[calendar_TSFL] B (NOLOCK)
		on Convert(varchar(10),A.[COMMISSION_DATE],121) = B.[CalendarDate]
  Where Convert(Varchar(10),[COMMISSION_DATE],121) >= @DATE
  	and A.[CUSTOMER_ID] not in('9602206','9602208','9663528','9747036','9681355','9749475','9856645','9602209','9749476','9602205')
	Order by [COMMISSION_DATE] asc
		 
-----------------------------------Create Temp Table--------------------------
Create table #b (
	[Posting Date] [datetime]  NULL,
	[Dollars] [numeric](38, 20) NULL)
	
------------------------Pull Sales Information (ALL)--------------------------------
Insert into #b
SELECT 
	A. [Posting Date]
	,Sum(A.[Amount]) as Dollars
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A (NOLOCK)
	Inner Join 
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B (NOLOCK)
     on A.[Document No_]= B.[No_]
WHERE A.[Posting Date] >= @DATE
	and B.[Customer Posting Group] ='TSFL'
GROUP BY 
	A. [Posting Date]

Union all

--------------------------Pull Credit Information(ALL)--------------------------------
SELECT 
 A. [Posting Date]
,Sum(A.[Amount])*-1 as Dollars        
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line] A (NOLOCK)
Inner Join 
	 [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] B (NOLOCK)
     on A.[Document No_]= B.[No_]

WHERE A.[Posting Date] >= @DATE
	and B.[Customer Posting Group] ='TSFL'
GROUP BY 
	A. [Posting Date]

-------------------------------Aggregation to Business Month and Quarter-------------------------------
Select 
	 Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
	,sum([Dollars]) as [TSFL Sales]
  Into #c
From #b B 
  Inner join [BI_Reporting].[dbo].[calendar_TSFL] C (NOLOCK)
		on B.[Posting Date] = C.[CalendarDate]
Group By 
	[LastDateOfMonth]
Order by 
  [LastDateOfMonth]
 
 -----------------------------------Create Temp Table--------------------------
Create table #shipping (
	[Posting Date] [datetime]  NULL,
	[Shipping Revenue] [numeric](38, 20) NULL)
	
------------------------Pull Sales Information (Shipping)--------------------------------
Insert into #shipping
SELECT 
	 A. [Posting Date]
	,Sum([Unit Price]) as [Shipping Revenue]
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A (NOLOCK)
	Inner Join 
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B (NOLOCK)
     on A.[Document No_]= B.[No_]
WHERE A.[Posting Date] >= @DATE
	and B.[Customer Posting Group] ='TSFL'
	and A.[Type] = '1'
	and A.[No_] = '63000'
GROUP BY 
	A. [Posting Date]

Union all

--------------------------Pull Credit Information (Shipping)--------------------------------
SELECT 
      A. [Posting Date]
     ,Sum([Unit Price])*-1 as [Shipping Revenue]       
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line] A (NOLOCK)
Inner Join 
	 [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] B (NOLOCK)
     on A.[Document No_]= B.[No_]

WHERE A.[Posting Date] >= @DATE
	and B.[Customer Posting Group] ='TSFL'
	and A.[Type] = '1'
	and A.[No_] = '63000'
GROUP BY 
	A. [Posting Date]

-------------------------------Aggregation to Business Month and Quarter-------------------------------
Select 
	 Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
	,sum([Shipping Revenue]) as [TSFL Shipping Revenue]
  Into #d
From #shipping S 
  Inner join [BI_Reporting].[dbo].[calendar_TSFL] C (NOLOCK)
		on S.[Posting Date] = C.[CalendarDate]
Group By 
	[LastDateOfMonth]
Order by 
  [LastDateOfMonth]

------------------------Counting Unique Coaches for Active Earners by Month------------------------	
Select 
	   C.[Last Day of Month]
	  ,[CalendarYear]
      ,Case [CalendarMonth]
	when '1' then 'January'
	when '2' then 'February'
	when '3' then 'March'
	when '4' then 'April'
	when '5' then 'May'
	when '6' then 'June'
	when '7' then 'July'
	when '8' then 'August'
	when '9' then 'September'
	when '10' then 'October'
	when '11' then 'November'
	when '12' then 'December'
	end as [CalendarMonth]
      ,count(distinct([CUSTOMER_ID])) as [Active Earners]
      ,[TSFL Sales]
      ,[TSFL Shipping Revenue]
      ,([TSFL Sales]-[TSFL Shipping Revenue]) as [Total Revenue]
      ,(([TSFL Sales]-[TSFL Shipping Revenue])/count(distinct([CUSTOMER_ID]))) as [Revenue Per Coach]
Into #z
   From #a A
	Inner join #c C
		on A.[Last Day of Month] = C.[Last Day of Month]
	Inner Join #d D
		on C.[Last Day of Month] = D.[Last Day of Month]
	Where [Total Earnings]> '0'
		Group by 
		  C.[Last Day of Month]
		 ,[TSFL Sales]
		 ,[TSFL Shipping Revenue]
		 ,[CalendarYear]
         ,[CalendarMonth]
		Order by 
		 [Last Day of Month] asc

------------Insert information into table---------------

Insert into dbo.[Active Earning Coaches]
Select * from #z		 
----------------------------------Clean up Temp Tables-------------------------------------		 
Drop table #a
Drop table #b
Drop table #c
Drop table #d
Drop table #z
Drop table #shipping
