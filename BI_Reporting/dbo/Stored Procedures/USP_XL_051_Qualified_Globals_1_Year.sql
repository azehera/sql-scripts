﻿



--/*
--===============================================================================
--Author:         Kalpesh Patel
--Create date: 01/20/2014

--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--[ODYSSEY_ETL]       [dbo].[ODYSSEY_BUSINESSCENTER]		       Select   
--[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                     Select  
--[ODYSSEY_ETL]       [dbo].[ODYSSEY_RANK_SCHEME_DETAILS]          Select

--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------
--2015-04-17     Menkir Haile                DB-BIDB server reference is removed

--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--===============================================================================
--*/
CREATE procedure [dbo].[USP_XL_051_Qualified_Globals_1_Year]
	@M1EndDate Date
as 
set nocount on ;
Declare @StartDate Datetime
Declare @EndDate Datetime

Set @StartDate=(select DATEADD(MONTH, DATEDIFF(MONTH, 0, @M1EndDate)-11, 0))
--Set @EndDate=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT 
	  A.[CUSTOMER_NUMBER] as [Coach ID]
	 ,A.[FIRST_NAME]as [First Name]
	 ,A.[LAST_NAME]as [Last Name]
     --,A.[EMAIL1_ADDRESS] as [Coach_Email]
     --,A.[MAIN_PHONE] as [Phone]
     ,convert(varchar(10),C.[COMMISSION_PERIOD],121) as [Month]
     ,C.[RANK] as [Qualified Rank]
Into #a
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
	Inner join[ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
			on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] RS
		on C.[RANK] = RS.[RANK_NAME]
Where  CAST(C.[COMMISSION_PERIOD] AS DATE) between @StartDate and @M1EndDate
		and [RANK] in ('Presidential_Director','Global_Director','FI_Presidential_Director','FI_Global_Director') 

Select
	 [Coach ID]
	 ,Convert(numeric(36,0),([Coach ID])) as V_LookupCoachID
	,[First Name]
	,[Last Name]
	,max([Month]) as [Last Month Global or higher]

		From #a A
Group by 	 [Coach ID]
	,[First Name]
	,[Last Name]
Order by max([Month])desc


