﻿
/*
--================================================================================
Author:         Daniel Dagnachew
Create date: 09/14/2015
-----------------------------dbo.Odyssey------------------
Create monthly snapshot coach from Odyssey and TSFL_Reporting data 
--================================================================================   
REFERENCES
Database              Table/View/UDF                               Action            
----------------------------------------------------------------------------------
[Odyssey_ETL]       [dbo].[ODYSSEY_CUSTOMER_STATUS]                Select 
[Odyssey_ETL]       [dbo].[Odyssey_CUSTOMER_TYPE]                  Select 
[Odyssey_ETL]       [dbo].[ODYSSEY_BUSINESSCENTER]                 Select 
[Odyssey_ETL]       [dbo].[ODYSSEY_CUSTOMER]                       Select 
[Odyssey_ETL]       [dbo].[ODYSSEY_CUSTOMER_ADDRESS]               Select 
[TSFL_Reporting]    [dbo].[Coach_Client_RELATIONSHIP]              Select 
                        
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------

==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================
*/



/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE Procedure [dbo].[USP_HC_Snapshot]
as 

SET NOCOUNT ON

DECLARE @Msg VARCHAR(200)
Declare @EndLastMonth datetime
set @EndLastMonth = (SELECT DATEADD(D, -1, DATEADD(MONTH, DATEDIFF(MONTH, '19000101', GETDATE()), '19000101')))

  IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A -----Pull customers which were coaches at a certain point in time without any time filter
  Select distinct CUSTOMER_ID
  into #A
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
  where RANK is not null
  and RANK <> 'NONE'

  IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B------All fields without time filter that are pulled from customer tables(not time dependant)
  SELECT DISTINCT BC.CUSTOMER_ID
      ,C.[CUSTOMER_NUMBER]
      ,C.[FIRST_NAME]
      ,C.[LAST_NAME]
      ,C.[CUSTOMER_TYPE] 
      ,TY.[NAME] Customer_Type_Desc
      ,CAST(C.ENTRY_DATE as date) ENTRY_DATE
      ,cAST(C.LAST_ACTIVE_DATE AS DATE) REVERSION_DATE
	  ,cAST(C.REINSTATEMENT_DATE AS DATE) REINSTATEMENT_DATE
	  ,CAST (C.RENEWAL_DATE AS DATE) RENEWAL_DATE
	  ,CAST (C.TERMINATION_DATE AS DATE) TERMINATION_DATE
	  ,CAST(C.EFFECTIVE_DATE AS DATE) ACTIVATION_DATE
 into #B  
 FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
 join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
 on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
 join [ODYSSEY_ETL].[dbo].[Odyssey_CUSTOMER_TYPE] TY
 on C.[CUSTOMER_TYPE] = TY.[CUSTOMER_TYPE]
 Where BC.Customer_ID  in (Select distinct CUSTOMER_ID from #A)
 order by C.[CUSTOMER_NUMBER]


 IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C------All fields that are pulled from businesscenter table(time dependant)
 Select DISTINCT 
             BC.[RANK] Current_Rank
            ,BC.[HIGHEST_ACHIEVED_RANK]
            ,BC.Customer_ID
			,BC2.CUSTOMER_ID Sponsor_ID
            ,C.CUSTOMER_NUMBER as Sponsor_NUMBER 
            ,ST.PENDING_HEALTH_COACH_DATE  Pending_Date
  into #C
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
  join #A a on BC.CUSTOMER_ID = a.CUSTOMER_ID
  join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC2
  on BC.SPONSOR_ID = BC2.BUSINESSCENTER_ID
    join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
  on BC2.[CUSTOMER_ID] = C.[CUSTOMER_ID]
    JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_STATUS] ST
  ON ST.[CUSTOMER_ID] = BC.[CUSTOMER_ID]
  Where convert(datetime,convert(varchar,BC.COMMISSION_PERIOD ,101)) = @EndLastMonth 

   IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E--------Address temp table

   SELECT DISTINCT 
                B.[CUSTOMER_ID]
               ,[STATE]
               ,LEFT([POSTAL_CODE],5) [POSTAL_CODE]
  INTO #E
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] ADR
  JOIN #B B ON B.CUSTOMER_ID = ADR.CUSTOMER_ID
  WHERE [ADDRESS_TYPE] = 'MAIN'

-----------------Loop table to get the presidents of the above Customers--------------
   IF object_id('Tempdb..#President') Is Not Null DROP TABLE #President

  Create table #President (Customer_ID Numeric(9,0),President_ID Numeric(9,0))

  DECLARE @CustomerPresident TABLE (
	Customer_id Numeric(9,0) PRIMARY KEY
)

 IF object_id('Tempdb..#OO') Is Not Null DROP TABLE #OO
 SELECT distinct  b.Customer_ID, b2.Customer_ID Sponsor_ID,b2.RANK
 into #OO
 FROM ODYSSEY_ETL.dbo.ODYSSEY_BUSINESSCENTER B
 inner join #A a
 on a.CUSTOMER_ID = B.CUSTOMER_ID
 inner join ODYSSEY_ETL.dbo.ODYSSEY_BUSINESSCENTER B2  
 on B.Sponsor_ID = B2.BUSINESSCENTER_ID
 where convert(datetime,convert(varchar,B.COMMISSION_PERIOD ,101)) = @EndLastMonth 
 and convert(datetime,convert(varchar,B2.COMMISSION_PERIOD ,101)) = @EndLastMonth 


CREATE CLUSTERED INDEX IDX_C_Customer_Customer_ID ON #OO(Customer_ID)------Index for the temp table so that it can fasten up the recursive query process
    
CREATE INDEX IDX_Sponsors_SponsorID ON #OO(Sponsor_ID)------Index for the temp table so that it can fasten up the recursive query process


 INSERT @CustomerPresident 
 SELECT distinct Customer_ID
 FROM #A

 SET NOCOUNT ON

 DECLARE @CurrentCustomer Numeric(9,0)
 SELECT @CurrentCustomer = MIN(Customer_Id) FROM @CustomerPresident

 WHILE @CurrentCustomer IS NOT NULL

 Begin 

   ;WITH Emp_CTE (Customer_ID, Sponsor_ID, EmpLevel)
  As  (
 SELECT Customer_ID, Sponsor_ID,1 
 FROM #OO 
 WHERE CUSTOMER_ID = @CurrentCustomer
 UNION ALL
 SELECT c.CUSTOMER_ID, c.Sponsor_ID, EmpLevel + 1
 FROM #OO  c
 INNER JOIN Emp_CTE ecte ON c.CUSTOMER_ID = ecte.Sponsor_ID
 ), RepCte as
  (SELECT distinct M.Customer_ID, M.Sponsor_ID,EmpLevel
 FROM Emp_CTE M)
 insert into #President
 Select distinct 
  (Select Customer_id from RepCte where EmpLevel = 1) as Customer_id
 ,(Select Sponsor_ID from RepCte
 where EmpLevel = (Select min(EmpLevel) From RepCte 
 inner join ODYSSEY_ETL.dbo.ODYSSEY_BUSINESSCENTER B2
 on RepCte.Sponsor_ID = B2.CUSTOMER_ID
 where B2.RANK in ('FI_Presidential_Director'))
 ) as President_id
 From RepCte 

 Delete @CustomerPresident
 where Customer_id = @CurrentCustomer
 
 Select @CurrentCustomer = min(Customer_id) from  @CustomerPresident

 SELECT @Msg = CAST(@@ROWCOUNT AS VARCHAR(10)) + 'rows affected'
 RAISERROR (@Msg, 0, 1) WITH NOWAIT

 end 

  IF object_id('Tempdb..#FinalPresident') Is Not Null DROP TABLE #FinalPresident

  Select P.[Customer_ID],[President_ID],C.Customer_Number President_Number 
  into #FinalPresident
  FROM #President P
  left join ODYSSEY_ETL.dbo.ODYSSEY_Customer C
  on P.President_ID = C.Customer_ID

  --  IF object_id('Tempdb..#FinalPresident') Is Not Null DROP TABLE #FinalPresident

  --Select [CUSTOMER_SYSTEM_ID][CUSTOMER_ID] ,[President_SYSTEM_ID][President_ID],Customer_Number President_Number 
  --into #FinalPresident
  --FROM [TSFL_CUBE_RDW].[dbo].[TSFL_Fact_Final]
  --WHERE [CUSTOMER_SYSTEM_ID] IN (SELECT distinct Customer_ID FROM #A)
  --AND COMMISSION_PERIOD = Cast(@EndLastMonth as date)

 


  IF object_id('Tempdb..#Status') Is Not Null DROP TABLE #Status

  Select B.CUSTOMER_ID,
  case when c.customer_type in ('P','U') and c.EFFECTIVE_DATE is null and c.last_active_date is null and c.termination_date is null
  then 'Pending Active'
  when c.customer_type in ('P','U') and c.EFFECTIVE_DATE is null and (c.last_active_date is not null or c.termination_date is not null)
  then 'PendingCancelled'
  when c.customer_type in ('D','C','M') and c.EFFECTIVE_DATE is not null
  then 'Active'
  when c.customer_type not in ('D','C','M') and c.EFFECTIVE_DATE is not null and (c.last_active_date is not null or c.termination_date is not null )
  then 'Inactive'
  else 'Inactive'
  end as Customer_Status
  into #Status
  from #B B 
  join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
  on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]


----Tying The temp tables

  Select DISTINCT 
       B.[CUSTOMER_NUMBER]
      ,B.[FIRST_NAME]
      ,B.[LAST_NAME]
      ,B.[CUSTOMER_TYPE] 
      ,B.Customer_Type_Desc
	  ,S.Customer_Status
	  ,C.Current_Rank
      ,C.[HIGHEST_ACHIEVED_RANK]
	  ,C.Sponsor_Number
	  ,FP.President_Number 
	  ,B.ENTRY_DATE
	  ,B.Reversion_Date
      ,B.REINSTATEMENT_DATE 
      ,B.RENEWAL_DATE
      ,B.TERMINATION_DATE
      ,B.ACTIVATION_DATE
	  ,C.Pending_Date
	  ,AD.[STATE]
	  ,AD.[POSTAL_CODE]
	  ,Case when B.[Activation_Date] is null then 'Never a Coach'
            when S.[Customer_Status] = 'Active' then 'Active Coach'
            else 'Inactive Coach' end as [Cube Status]

  From #B B
  join #Status S
  on B.CUSTOMER_ID = S.CUSTOMER_ID
  join #C C
  on C.CUSTOMER_ID = B.CUSTOMER_ID
  join #E AD
  on AD.CUSTOMER_ID = B.CUSTOMER_ID
   join #FinalPresident FP
  on FP.CUSTOMER_ID = B.CUSTOMER_ID
  
   order by B.[CUSTOMER_NUMBER]
