﻿
/*
--================================================================================
Author:         Daniel Dagnachew
Create date: 09/14/2015
-----------------------------dbo.Odyssey------------------
Create Monthly coach reports from Odyssey and TSFL_Reporting data 
--================================================================================   
REFERENCES
Database              Table/View/UDF                               Action            
----------------------------------------------------------------------------------
[Odyssey_ETL]       [dbo].[ODYSSEY_ORDERS]                         Select 
[Odyssey_ETL]       [dbo].[ODYSSEY_ORDER_PRODUCT]                  Select 
[Odyssey_ETL]       [dbo].[ODYSSEY_BUSINESSCENTER]                 Select 
[Odyssey_ETL]       [dbo].[ODYSSEY_CUSTOMER]                       Select 
[Odyssey_ETL]       [dbo].[ODYSSEY_VOLUME]                         Select 
[TSFL_Reporting]    [dbo].[Coach_Client_RELATIONSHIP]              Select 
                        
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------

==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================
*/



/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE Procedure [dbo].[USP_HC_Monthly_Activity](@DateAnalysis AS DATE)
as 
 
--DECLARE @DateAnalysis DATE
DECLARE @DateMonthBegin DATE
DECLARE @DateMonthEnd DATE

--sET @DateAnalysis = '8/31/2015'

SET @DateMonthBegin = ( Select DATEADD(MM, DATEDIFF(MM, 0, @DateAnalysis ), 0))
SET @DateMonthEnd = (Select DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin)))

IF object_id('Tempdb..#NewCoaches') Is Not Null DROP TABLE #NewCoaches
;With Cte as (
SELECT CC.SPONSOR_ID, Count(*) AS SponsoredNewHC
  FROM [TSFL_Reporting].[dbo].[Coach_Client_RELATIONSHIP] CC
  where cc.COMMISSION_PERIOD = @DateMonthEnd  and  CC.HC_KIT_PURCHASE = 1
  Group By cc.SPONSOR_ID
)
Select DISTINCT BC.Customer_ID,C.Customer_Number,SponsoredNewHC
INTO #NewCoaches
from Cte 
    join [ODYSSEY_ETL].dbo.ODYSSEY_BUSINESSCENTER BC
on Cte.Sponsor_id = BC.BusinessCenter_ID
    join [ODYSSEY_ETL].dbo.ODYSSEY_CUSTOMER C
on BC.CUSTOMER_id = C.CUSTOMER_ID


IF object_id('Tempdb..#NewCustomers') Is Not Null DROP TABLE #NewCustomers

;With Cte as (
SELECT CC.SPONSOR_ID, Count(*) AS SponsoredNewClient
  FROM [TSFL_Reporting].[dbo].[Coach_Client_RELATIONSHIP] CC
  where cc.COMMISSION_PERIOD = @DateMonthEnd and Cast(cc.Entry_Date as date)  between @DateMonthBegin and @DateMonthEnd
  AND PV > 0
  Group By cc.SPONSOR_ID
)
Select DISTINCT BC.Customer_ID,C.Customer_Number,SponsoredNewClient
INTO #NewCustomers
from Cte 
    join [ODYSSEY_ETL].dbo.ODYSSEY_BUSINESSCENTER BC
on Cte.Sponsor_id = BC.BusinessCenter_ID
    join [ODYSSEY_ETL].dbo.ODYSSEY_CUSTOMER C
on BC.CUSTOMER_id = C.CUSTOMER_ID



IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
  Select [CUSTOMER_NUMBER],[PV],[FLV] ,[GV]
  into #B
    FROM [TSFL_Reporting].[dbo].[Coach_Client_RELATIONSHIP]
	where [COMMISSION_PERIOD] = @DateMonthEnd
	and RANK <> 'NONE'


IF object_id('Tempdb..#D') Is Not Null DROP TABLE #D
;With Cte as (
SELECT distinct A.[CUSTOMER_ID]
      ,Convert(varchar(10),A.[COMMISSION_DATE],121) as [Commission Date]
      ,isnull([TOTAL_ADJUSTMENT],0) + isnull([GROWTH_BONUS],0) + isnull([GENERATION_BONUS],0)+ isnull([OVERRIDE_BONUS],0) + isnull([CSC_AMOUNT],0) 
       + isnull([CAB_BONUS],0) + isnull([ASSIST_BONUS],0) + ISNULL([ROLLING_BONUS],0)+ isnull([OVERRIDE_BONUS_GED],0)+ isnull([OVERRIDE_BONUS_PED],0)
       + isnull([CERTIFIED_BONUS],0)+ Isnull([CUSTOMER_SUPPORT_BONUS],0)+ Isnull([FIBC],0)+ Isnull([ANSITION_ADJ],0) as [Total Earnings]
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] A (NOLOCK)
inner join[ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] B (NOLOCK)
 on A.[COMMISSION_ENTRY_ID] = B.[COMMISSION_ENTRY_ID]
Inner join [BI_Reporting].[dbo].[calendar_TSFL] C (NOLOCK)
 on Convert(varchar(10),A.[COMMISSION_DATE],121) = C.[CalendarDate]
Where Convert(date,A.[COMMISSION_DATE]) BETWEEN  @DateMonthBegin  AND  @DateMonthEnd
 --and A.[CUSTOMER_ID] not in('9602206','9602208','9663528','9747036','9681355','9749475','9856645','9602209','9749476','9602205')

union all

SELECT distinct [CUSTOMER_ID]
	 ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
     ,([CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]+[TOTAL_ADJUSTMENT]) as [Total Earnings]
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY] A (NOLOCK)
Inner join [BI_Reporting].[dbo].[calendar_TSFL] B (NOLOCK)
 on Convert(date,A.[COMMISSION_DATE]) = B.[CalendarDate]
Where Convert(date,[COMMISSION_DATE]) BETWEEN  @DateMonthBegin  AND  @DateMonthEnd 
 --and A.[CUSTOMER_ID] not in('9602206','9602208','9663528','9747036','9681355','9749475','9856645','9602209','9749476','9602205')
	)

Select Cte.CUSTOMER_ID,C.CUSTOMER_NUMBER, sum([Total Earnings]) as [Total Earnings]
into #D
from Cte
join [ODYSSEY_ETL].dbo.ODYSSEY_CUSTOMER C
  ON Cte.CUSTOMER_ID = C.CUSTOMER_ID
group by Cte.CUSTOMER_ID,C.CUSTOMER_NUMBER



SELECT B.CUSTOMER_NUMBER,
      datename(M,@DateAnalysis) + cast(year(@DateAnalysis) as varchar(4)) as [Month], 
	   ISNULL(D.[Total Earnings],0) [Total Earnings],
	   ISNULL(B.GV,0) GV,
	   ISNULL(B.PV,0) PV,
	   ISNULL(B.FLV,0) FLV,
	   ISNULL(SponsoredNewHC,0) SponsoredNewHC,
	   ISNULL(SponsoredNewClient,0) SponsoredNewClient
FROM #B B
left join #NewCoaches NC
on B.CUSTOMER_NUMBER = NC.CUSTOMER_NUMBER
left join #NewCustomers NCS
on B.CUSTOMER_NUMBER = NCS.CUSTOMER_NUMBER
left join #D D
on B.Customer_Number = D.CUSTOMER_NUMBER