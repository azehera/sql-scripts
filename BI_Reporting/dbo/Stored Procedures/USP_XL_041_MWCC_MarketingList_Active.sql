﻿
CREATE PROCEDURE [dbo].[USP_XL_041_MWCC_MarketingList_Active] as 

SET NOCOUNT ON;

/*
=============================================================================================
Author:      Luc Emond
Create date: 12/04/2013
-------------------------[dbo].[USP_XL_041_MWCC_MarketingList_Active] -----------------------------
Provide Monthly Active Customer Information to Marketing Group                
===========================================================================================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
-----------------------------------------------------------------------------------------------------------------------------------------------------------
[Book4Time_ETL]       [B4T_transaction_log_header]              Select
[Book4Time_ETL]       [B4T_transaction_log_Detail]              Select
[Book4Time_ETL]       [B4T_location]                            Select
[Book4Time_ETL]       [B4T_region_locations]                    Select
[Book4Time_ETL]       [B4T_region]                              Select
[Book4Time_ETL]       [B4T_Customer]                            Select
[Book4Time_ETL]       [B4T_Person]                              Select
[Book4Time_ETL]       [B4T_Address]                             Select
[Zip_codes_deluxe]    [ZipCodeDatabase_DELUXE]                  Select  
==========================================================================================================================================================
REVISION LOG
Date           Name                  Change
----------------------------------------------------------------------------------------------------------------------------------------------------------
12/19/2014     Ron Baldwin			Correct LastTransaction (date)
==========================================================================================================================================================
NOTES:

==========================================================================================================================================================
*/

-----DATE RANGE------------------
DECLARE @Date1 DateTime, @Date2 DateTime

set @Date1 =(Select Convert(Varchar(10), Getdate()-35,121))------5 weeks ago define by Marketing Group-----
set @Date2 =(Select Convert(Varchar(10), Getdate(),121))-----Today

----LAST TRANSACTION----
IF object_id('tempdb..#LastTran') is not null DROP TABLE #LastTran
Select 
Customer_id, MAX(Convert(Varchar (10),Transaction_Date, 121)) As LastTransaction
Into #LastTran 
From [Book4Time_ETL].[dbo].[B4T_transaction_log_header] h
WHERE [Transaction_Type] In ('S', 'R') 
group by customer_id

----MASTER LIST OF SALES--------
IF object_id('tempdb..#A') is not null DROP TABLE #A
SELECT
Region_name
,Location_Name
,Parent_Id
,h.Customer_id
,First_Name
,Last_Name
,Email
,Via_Email
INTO #A
From [Book4Time_ETL].[dbo].[B4T_transaction_log_header] h
Join [Book4Time_ETL].[dbo].[B4T_transaction_log_Detail] d
on h.transaction_id = d.transaction_id
Left Outer Join [Book4Time_ETL].[dbo].[B4T_location]  L
    ON h.location_id = L.location_id  
Left Outer Join [Book4Time_ETL].[dbo].[B4T_person]  p
    on h.customer_id = p.person_id
Left Outer Join [Book4Time_ETL].[dbo].[B4T_region_locations] rl
    on h.location_id= rl.location_id
Left Outer Join [Book4Time_ETL].[dbo].[B4T_region] r
    on rl.region_id = r.region_id 
Left Outer Join [Book4Time_ETL].[dbo].[B4T_Customer] c
    on h.Customer_id = c.customer_id     
WHERE (Select Convert(Varchar (10),Transaction_Date, 121))>= @Date1 and  (Select Convert(Varchar (10),Transaction_Date, 121)) <= @Date2 and [Transaction_Type] In ('S', 'R') 
and h.location_id not in ('3621151','3621158','3621174','3621198','3621292','3621300','3621310','3621320','3621327','3621334','3621341','3621348',
'3621362','3673472','3673472','3711827','3711835','3711848','3800950','3801355','4268511','4282917','4283450','4283739')
group by
Region_name
,Location_Name
,h.Customer_id
,First_Name
,Last_Name
,Email
,Parent_Id
,via_email

----ALTER TABLE AND UPDATES TO ADD CUSTOMER INFO----
ALTER TABLE #A
ADD [Parent1] bigint, Street Char(50), City Char(25), Country Char(3), Postal_Code Char(15), State Char(25), LastTransaction date

Update #A
set [Parent1] = parent_id 
where #A.customer_id = #a.parent_id

UPDATE a
set a.LastTransaction = Convert(Varchar(10), l.LastTransaction, 121)
FROM #A a inner join #LastTran l on a.customer_id = l.customer_id 

UPDATE #A
SET [Parent1] = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
where #a.parent_id = p.customer_id
and [Parent1] is null

UPDATE #A
SET First_Name = pe.first_name, Last_Name = pe.Last_Name, Email = pe.Email
from [Book4Time_ETL].[dbo].[B4T_person]  pe
   where #a.[parent1] = pe.person_id

Update #A
Set Street = n.Street, City = N.City, Country = N.Country, Postal_Code = N.Postal_Code
From [Book4Time_ETL].[dbo].[B4T_Address] N
Where #A.[Parent1] = n.Address_id
and N.address_type='4'

Update #a
set State = z.State
From [Zip_codes_deluxe].dbo.[ZipCodeDatabase_DELUXE] Z
where Left(#a.Postal_Code,5)= z.[ZipCode] and z.primaryRecord ='p'

--select * from #A

----FINAL----
Select 
 Region_Name
,Location_Name
,Customer_Id
,Parent_Id
,[Parent1]
,LastTransaction
,First_Name
,Last_Name
,Street
,City
,State
,Postal_Code
,Email
,Case When Via_Email =0 Then 'No' Else 'Yes' End Via_Email
From #a order by Customer_Id

