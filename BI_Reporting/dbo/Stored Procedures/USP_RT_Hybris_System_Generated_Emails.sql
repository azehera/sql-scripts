﻿


/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 10/11/2019
------------------------------------------------
Future Autoship orders
--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------
Hybris_ETL            mdf_prd_database.processess            Select
                 
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------
2/26/2020     Daniel D.                 added logic to capture zero count
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_RT_Hybris_System_Generated_Emails]  
AS

BEGIN 


--IF OBJECT_ID('Tempdb..#HybProc') IS NOT NULL DROP TABLE #HybProc;
--CREATE TABLE #HybProc (p_processdefinitionname NVARCHAR(255));
--INSERT INTO #HybProc----Populate the processes for email analytics
--(p_processdefinitionname)
--VALUES
--( 'customerRegistrationEmailProcess'),( 'medifastAccountInactivityProcess'),( 'healthCoachAccountInactivityProcess'),( 'medifastCustomEmailProcess'),( 'medifastSubscriptionReminderEmailProcess'),( 'order-process'),( 'orderConfirmationEmailProcess'),( 'hcOrderConfirmationEmailProcess'),( 'forgottenPasswordEmailProcess'),( 'sendHCAutoshipReminderEmailProcess'),( 'medifastSubscriptionOrderNotProcessedEmailProcess'),( 'autoshipOrderNotProcessedToHCEmailProcess'),( 'medifastUpdatePaymentInfoProcess'),( 'updatedPasswordEmailProcess'),( 'updateAccountShippingAddressEmailProcess'),( 'medifastUpdatePreferencesProcess'),( 'consignment-process'),( 'createReturnRequestProcess'),( 'medifastReturnRequestRefundProcess'),( 'medifastReturnProcessedEmailProcess'),( 'sendDeliveryEmailProcess'),( 'sendHCDeliveryEmailProcess'),( 'medAdvantageOrderUpdateEmailProcess'),( 'autoshipOrderUpdateToHealthCoachEmailProcess'),( 'medifastSubscriptionModificationEmailProcess'),( 'medifastCancelOrderEmailProcess'),( 'healthCoachCancelOrderEmailProcess'),( 'optaviaHealthCoachLeadPoolEmailProcess'),( 'medifastCancelOrderCockpitEmailProcess'),( 'medifastUpdateUserGroupProcess')
--IF OBJECT_ID('Tempdb..#Channel') IS NOT NULL DROP TABLE #Channel;
--CREATE TABLE #Channel (Channel VARCHAR(20));
--INSERT INTO #Channel----Populate the channel for email analytics
--(Channel)
--VALUES ('Optavia - US '),('Medifast'),('Optavia - HK'),('Optavia - SG'),('ALL')
--IF OBJECT_ID('Tempdb..#Process') IS NOT NULL
--    DROP TABLE #Process; ----Pull all the processes for the current year and cross join it with order dates in the last 33 days
--    ;
--WITH CTE AS (SELECT CONVERT(DATE, GETDATE()) OrderDate
--             UNION ALL
--             SELECT DATEADD(DAY, -1, OrderDate)FROM CTE WHERE OrderDate>GETDATE()-32)
--SELECT * INTO #Process FROM CTE CROSS JOIN #HybProc CROSS JOIN #Channel;
-------Only pick exception channels for ALL(remove channels with null p_site) 
--DELETE FROM #Process
--WHERE p_processdefinitionname IN ('consignment-process','hcOrderConfirmationEmailProcess','orderConfirmationEmailProcess','order-process','sendDeliveryEmailProcess','sendHCDeliveryEmailProcess')
--AND Channel <> 'ALL'

-------Only pick exception channels for Optavia 
--DELETE FROM #Process
--WHERE p_processdefinitionname IN ('healthCoachAccountInactivityProcess','sendHCAutoshipReminderEmailProcess','autoshipOrderNotProcessedToHCEmailProcess','autoshipOrderUpdateToHealthCoachEmailProcess','healthCoachCancelOrderEmailProcess','optaviaHealthCoachLeadPoolEmailProcess')
--AND Channel NOT IN ('Optavia - US ','Optavia - HK','Optavia - SG')

-------Only pick exception channels for Medifast 
--DELETE FROM #Process
--WHERE p_processdefinitionname IN ('medifastCancelOrderCockpitEmailProcess')
--AND Channel <> ('Medifast')

------pull the process count in the last 33 days
--IF OBJECT_ID('Tempdb..#stg') IS NOT NULL DROP TABLE #stg;
--SELECT CAST(createdTS AS DATE) AS OrderDate, p_processdefinitionname, CASE WHEN p_site IN (8796093088808, 8796125922344) THEN 'Optavia - US '
--                                                                      WHEN p_site IN (8796093056040, 8796093056040) THEN 'Medifast'
--                                                                      WHEN p_site=8796125856808 THEN 'Optavia - HK'
--                                                                      WHEN p_site=8796125824040 THEN 'Optavia - SG' ELSE 'other' END AS 'channel', COUNT(PK) AS 'Count', CAST(GETDATE() AS DATE) AS 'CurrentDate', CASE WHEN (CAST(GETDATE() AS DATE)=CAST(createdTS AS DATE)) THEN 'Yes' ELSE 'No' END AS 'IsToday?'
--INTO #stg
--FROM Hybris_ETL.mdf_prd_database.processes
--WHERE CAST(createdTS AS DATE)> CAST(GETDATE()-33 AS DATE)AND p_processdefinitionname IN(SELECT DISTINCT p_processdefinitionname FROM #HybProc)
--GROUP BY CAST(createdTS AS DATE), p_processdefinitionname, p_site;
--IF OBJECT_ID('Tempdb..#temp') IS NOT NULL DROP TABLE #temp;------Adding 0 count if a process doesn't exit for channels and date in the temp table #stg
--SELECT * INTO #temp FROM #stg
--UNION ALL
--SELECT OrderDate, p_processdefinitionname, Channel, 0 AS 'Count', CAST(GETDATE() AS DATE) AS 'CurrentDate', CASE WHEN CAST(GETDATE() AS DATE)=OrderDate THEN 'Yes' ELSE 'No' END AS 'IsToday?'
--FROM(SELECT DISTINCT OrderDate, p_processdefinitionname, Channel
--     FROM #Process
--     EXCEPT
--     SELECT DISTINCT OrderDate, p_processdefinitionname, channel
--     FROM #stg) a;
--IF OBJECT_ID('Tempdb..#sum') IS NOT NULL DROP TABLE #sum;
--SELECT p_processdefinitionname, channel, OrderDate, SUM(Count) AS 'SumPerDay'
--INTO #sum
--FROM #temp
--GROUP BY p_processdefinitionname, channel, OrderDate;
--IF OBJECT_ID('Tempdb..#avgCount') IS NOT NULL DROP TABLE #avgCount;
--SELECT p_processdefinitionname, channel, SUM(SumPerDay)/ COUNT(SumPerDay) AS 'AVG_Count', ((SUM(SumPerDay)/ COUNT(SumPerDay))* .10) AS 'AVG_Count_10per', ((SUM(SumPerDay)/ COUNT(SumPerDay))* .90) AS 'AVG_Count_90per'
--INTO #avgCount
--FROM #sum
--GROUP BY p_processdefinitionname, channel;
--SELECT T.*, CASE WHEN (T.Count=0 AND((A.channel='Optavia - US ')OR(A.channel='Medifast')))OR(T.Count<A.AVG_Count_10per AND T.Count > 0) THEN 'YES' ELSE 'No' END AS [Zero Count or Less than 10% Threshold], CASE WHEN (T.Count=0 AND((A.channel='Optavia - US ')OR(A.channel='Medifast'))) THEN 'YES' ELSE 'No' END AS [Zero Count], CASE WHEN (T.Count<A.AVG_Count_10per AND T.Count > 0) THEN 'YES' ELSE 'No' END AS [Less than 10% Threshold],A.AVG_Count, A.AVG_Count_10per, A.AVG_Count_90per, CASE WHEN Count<A.AVG_Count_10per THEN 'YES' ELSE 'NO' END AS 'IsAVGCountLessThan10Per', CASE WHEN Count>A.AVG_Count_90per THEN 'YES' ELSE 'NO' END AS 'IsAVGCountgreaterThan90Per'
--FROM #temp T
--     LEFT JOIN #avgCount A ON A.p_processdefinitionname=T.p_processdefinitionname AND A.channel=T.channel;

IF OBJECT_ID('Tempdb..#Process') IS NOT NULL DROP TABLE #Process; ----Pull all the processes for the current year and cross join it with order dates in the last 33 days

;WITH CTE AS (SELECT CONVERT(DATE, GETDATE()) OrderDate
             UNION ALL
             SELECT DATEADD(DAY, -1, OrderDate)FROM CTE WHERE OrderDate>GETDATE()-32)
SELECT * INTO #Process FROM CTE 
CROSS JOIN (SELECT DISTINCT p_processdefinitionname 
FROM Hybris_ETL.mdf_prd_database.processes WITH (NOLOCK) WHERE p_processdefinitionname NOT IN ('order-return-process','fraudReportEmailProcess'))a CROSS JOIN (select ('Optavia - US ') Channel union SELECT ('Medifast') UNION SELECT ('Optavia - HK') UNION SELECT ('Optavia - SG') UNION SELECT('ALL' ))c ;

-----Only pick exception channels for ALL(remove channels with null p_site) 
DELETE FROM #Process
WHERE p_processdefinitionname IN ('consignment-process','hcOrderConfirmationEmailProcess','orderConfirmationEmailProcess','order-process','sendDeliveryEmailProcess','sendHCDeliveryEmailProcess')
AND Channel <> 'ALL'

-----Only pick exception channels for Optavia 
DELETE FROM #Process
WHERE p_processdefinitionname IN ('healthCoachAccountInactivityProcess','sendHCAutoshipReminderEmailProcess','autoshipOrderNotProcessedToHCEmailProcess','autoshipOrderUpdateToHealthCoachEmailProcess','healthCoachCancelOrderEmailProcess','optaviaHealthCoachLeadPoolEmailProcess')
AND Channel NOT IN ('Optavia - US ','Optavia - HK','Optavia - SG')

-----Only pick exception channels for Medifast 
DELETE FROM #Process
WHERE p_processdefinitionname IN ('medifastCancelOrderCockpitEmailProcess','medifastCustomEmailProcess')
AND Channel <> ('Medifast')

----pull the process count in the last 33 days
IF OBJECT_ID('Tempdb..#stg') IS NOT NULL DROP TABLE #stg;
SELECT CAST(createdTS AS DATE) AS OrderDate, p_processdefinitionname, CASE WHEN p_site IN (8796093088808, 8796125922344) THEN 'Optavia - US '
                                                                      WHEN p_site IN (8796093056040, 8796093056040) THEN 'Medifast'
                                                                      WHEN p_site=8796125856808 THEN 'Optavia - HK'
                                                                      WHEN p_site=8796125824040 THEN 'Optavia - SG' ELSE 'other' END AS 'channel', COUNT(PK) AS 'Count', CAST(GETDATE() AS DATE) AS 'CurrentDate', CASE WHEN (CAST(GETDATE() AS DATE)=CAST(createdTS AS DATE)) THEN 'Yes' ELSE 'No' END AS 'IsToday?'
INTO #stg
FROM Hybris_ETL.mdf_prd_database.processes
WHERE CAST(createdTS AS DATE)> CAST(GETDATE()-33 AS DATE)AND p_processdefinitionname IN(SELECT DISTINCT p_processdefinitionname FROM #Process)
GROUP BY CAST(createdTS AS DATE), p_processdefinitionname, p_site;
IF OBJECT_ID('Tempdb..#temp') IS NOT NULL DROP TABLE #temp;------Adding 0 count if a process doesn't exit for channels and date in the temp table #stg
SELECT * INTO #temp FROM #stg
UNION ALL
SELECT OrderDate, p_processdefinitionname, Channel, 0 AS 'Count', CAST(GETDATE() AS DATE) AS 'CurrentDate', CASE WHEN CAST(GETDATE() AS DATE)=OrderDate THEN 'Yes' ELSE 'No' END AS 'IsToday?'
FROM(SELECT DISTINCT OrderDate, p_processdefinitionname, Channel
     FROM #Process
     EXCEPT
     SELECT DISTINCT OrderDate, p_processdefinitionname, channel
     FROM #stg) a;
IF OBJECT_ID('Tempdb..#sum') IS NOT NULL DROP TABLE #sum;
SELECT p_processdefinitionname, channel, OrderDate, SUM(Count) AS 'SumPerDay'
INTO #sum
FROM #temp
GROUP BY p_processdefinitionname, channel, OrderDate;
IF OBJECT_ID('Tempdb..#avgCount') IS NOT NULL DROP TABLE #avgCount;
SELECT p_processdefinitionname, channel, SUM(SumPerDay)/ COUNT(SumPerDay) AS 'AVG_Count', ((SUM(SumPerDay)/ COUNT(SumPerDay))* .10) AS 'AVG_Count_10per', ((SUM(SumPerDay)/ COUNT(SumPerDay))* .90) AS 'AVG_Count_90per'
INTO #avgCount
FROM #sum
GROUP BY p_processdefinitionname, channel;

SELECT T.OrderDate,
       --CASE WHEN T.p_processdefinitionname = 'autoshipOrderNotProcessedToHCEmailProcess'  THEN 'OptaviaCoachAutoshipOrderCancel'
	   Case WHEN T.p_processdefinitionname = 'autoshipOrderUpdateToHealthCoachEmailProcess'  THEN 'OptaviaCoachAutoshipChange'
			WHEN T.p_processdefinitionname = 'createReturnRequestProcess'  THEN 'OptaviaClientReturnRequestProcess'
			WHEN T.p_processdefinitionname = 'customerRegistrationEmailProcess'  THEN 'OptaviaClientAccountRegistration'
			WHEN T.p_processdefinitionname = 'expiringCardEmailProcess'  THEN 'OptaviaClientCardExpiration'
			--WHEN T.p_processdefinitionname = 'forgottenPasswordEmailProcess'  THEN 'OptaviaClientPasswordReset'
			WHEN T.p_processdefinitionname = 'hcOrderConfirmationEmailProcess'  THEN 'OptaviaCoachOrderConfirm'
			WHEN T.p_processdefinitionname = 'healthCoachAccountInactivityProcess'  THEN 'OptaviaCoachRewardExpire10'
			WHEN T.p_processdefinitionname = 'healthCoachCancelOrderEmailProcess'  THEN 'OptaviaCoachOrderCancel'
			WHEN T.p_processdefinitionname = 'medAdvantageOrderUpdateEmailProcess'  THEN 'MedifastOrderModify'
			WHEN T.p_processdefinitionname = 'medifastAccountInactivityProcess'  THEN 'MedifastRewardExpire10'
			--WHEN T.p_processdefinitionname = 'medifastCancelOrderEmailProcess'  THEN 'MedifastOrderCancel'
			WHEN T.p_processdefinitionname = 'medifastReturnProcessedEmailProcess'  THEN 'MedifastReturnProcessed'
			WHEN T.p_processdefinitionname = 'medifastReturnRequestRefundProcess'  THEN 'MedifastRefundProcessed'
	        WHEN T.p_processdefinitionname = 'medifastSubscriptionModificationEmailProcess'  THEN 'MedifastAutoshipChange'
			--WHEN T.p_processdefinitionname = 'medifastSubscriptionOrderNotProcessedEmailProcess'  THEN 'MedifastOrderCancel2'
			WHEN T.p_processdefinitionname = 'medifastSubscriptionReminderEmailProcess'  THEN 'MedifastSubcriptionReminder7'
			WHEN T.p_processdefinitionname = 'medifastUpdatePaymentInfoProcess'  THEN 'MedifastPaymentInfoUpdate'
			WHEN T.p_processdefinitionname = 'medifastUpdatePreferencesProcess'  THEN 'MedifastAccountPreferenceUpdated'
			WHEN T.p_processdefinitionname = 'optaviaHealthCoachLeadPoolEmailProcess'  THEN 'OptaviaCoachNewLead'
			WHEN T.p_processdefinitionname = 'orderConfirmationEmailProcess'  THEN 'OptaviaClientOrderConfirm'
			WHEN T.p_processdefinitionname = 'sendDeliveryEmailProcess'  THEN 'OptaviaClientOrderShipped'
			WHEN T.p_processdefinitionname = 'sendHCAutoshipReminderEmailProcess'  THEN 'OptaviaCoach4And7DayOrder'
			WHEN T.p_processdefinitionname = 'sendHCDeliveryEmailProcess'  THEN 'OptaviaCoachOrderShipped'
			WHEN T.p_processdefinitionname = 'updateAccountShippingAddressEmailProcess'  THEN 'OptaviaClientShipAddressUpdate'
			--WHEN T.p_processdefinitionname = 'updatedPasswordEmailProcess'  THEN 'updateOptaviaClientPasswordReset'
			ELSE T.p_processdefinitionname END AS p_processdefinitionname ,
	   T.channel,
	   T.Count,
	   T.CurrentDate,
	   T.[IsToday?],
CASE WHEN (T.Count=0 AND((A.channel='Optavia - US ')OR(A.channel='Medifast')))OR(T.Count<A.AVG_Count_10per AND T.Count > 0) THEN 'YES' ELSE 'No' END AS [Zero Count or Less than 10% Threshold], CASE WHEN (T.Count=0 AND((A.channel='Optavia - US ')OR(A.channel='Medifast'))) THEN 'YES' ELSE 'No' END AS [Zero Count], CASE WHEN (T.Count<A.AVG_Count_10per AND T.Count > 0) THEN 'YES' ELSE 'No' END AS [Less than 10% Threshold],A.AVG_Count, A.AVG_Count_10per, A.AVG_Count_90per, CASE WHEN Count<A.AVG_Count_10per THEN 'YES' ELSE 'NO' END AS 'IsAVGCountLessThan10Per', CASE WHEN Count>A.AVG_Count_90per THEN 'YES' ELSE 'NO' END AS 'IsAVGCountgreaterThan90Per'
FROM #temp T
     LEFT JOIN #avgCount A ON A.p_processdefinitionname=T.p_processdefinitionname AND A.channel=T.channel;


END



