﻿



/*
=======================================================================================================
Author:         Menkir Haile
Create date: 08/25/2017

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]					Select   
[ODYSSEY_ETL]       [dbo].[ODYSSEY_ORDERS]						Select  
[ODYSSEY_ETL]       [dbo].[ODYSSEY_BUSINESSCENTER]				Select
[ODYSSEY_ETL]		[dbo].[ODYSSEY_ORDER_PRODUCT]				Select

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
             

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/

CREATE procedure [dbo].[USP_Weekly_Coach_Sponsoring_New_Coach] as 
set nocount on ;

Declare @StartDate Datetime
Declare @EndDate Datetime

Set @StartDate=(Select dateadd(wk, datediff(wk, 0, getdate()) - 1, 0))
Set @EndDate=(Select dateadd(wk, datediff(wk, 0, getdate()), -1))

------------------------------Pull All Customers who purchases Health Coache Kits-----------------------------------

SELECT C2.CUSTOMER_NUMBER AS Coach_ID, C2.LAST_NAME, C2.FIRST_NAME, C.CUSTOMER_ID AS CoachID
INTO #Sponsors
FROM   [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] (NOLOCK) C
JOIN ODYSSEY_ETL.dbo.ODYSSEY_ORDERS O ( NOLOCK ) 
		ON O.CUSTOMER_ID = C.CUSTOMER_ID 		
JOIN ODYSSEY_ETL.dbo.ODYSSEY_ORDER_PRODUCT P ( NOLOCK ) 
		ON P.ORDER_ID = O.ORDER_ID
JOIN ODYSSEY_ETL.dbo.ODYSSEY_BUSINESSCENTER BC ( NOLOCK )
		ON BC.CUSTOMER_ID = C.CUSTOMER_ID AND CAST(BC.COMMISSION_PERIOD AS DATE) = EOMonth (@EndDate)
JOIN ODYSSEY_ETL.dbo.ODYSSEY_BUSINESSCENTER BC2 ( NOLOCK )
		ON BC2.BUSINESSCENTER_ID = BC.SPONSOR_ID
JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] (NOLOCK) C2
		ON BC2.CUSTOMER_ID = C2.CUSTOMER_ID AND CAST(BC2.COMMISSION_PERIOD AS DATE) = EOMonth (@EndDate)
WHERE   O.ENTRY_DATE BETWEEN @StartDate and @EndDate AND 
		LEFT(P.PRODUCT_ID, 5) IN ( '31011','31012', '32051','31100', '31105','31110','31002' )

-------------------Aggregate Health Coach Kit Purchasesrs by their Sponsors-----------------------------------------------
SELECT Coach_ID, LAST_NAME, FIRST_NAME,COUNT(CoachID) AS Sponsored_Coach
FROM #Sponsors
GROUP BY Coach_ID, LAST_NAME, FIRST_NAME
ORDER BY 
		 COUNT(*) DESC, LAST_NAME, FIRST_NAME