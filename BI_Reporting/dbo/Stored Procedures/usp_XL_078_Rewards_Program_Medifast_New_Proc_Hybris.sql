﻿


/*
=======================================================================================
Author:         Derald Smith &  Ronak Shah 
Create date: Date 01/11/2019
-------------------------[usp_XL_078_Rewards_Program_Medifast_New_Proc_Hybris] -------------------------
---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx----------------------
=======================================================================================   
REFERENCES
Database             Table/View/UDF		             Action            
---------------------------------------------------------------------------------------
						orders						Select
						basestore					Select 
						earnedreward				Select
========================================================================================                 
REVISION LOG
Date           Name              Change
----------------------------------------------------------------------------------------
02/19/2019		Ronak			Modify SQL to user open query against production environment
03/18/2019		Ronak			Modify SQL to Join Customer rewards(Daily Delta Process)
========================================================================================
*/
CREATE PROCEDURE [dbo].[usp_XL_078_Rewards_Program_Medifast_New_Proc_Hybris]  

AS
SET NOCOUNT ON;

 
IF OBJECT_ID('Tempdb..#B1') IS NOT NULL DROP TABLE #B1
SELECT 
    ER.Tran_dt,
    ER.Channel,
    SUM(ER.Reward_Earned) AS Reward_Earned,
    SUM(ER.Reward_Used) AS Reward_Used,
    ISNULL(Reward_Forfeited, 0.00) AS Reward_Forfeited
	INTO #B1
FROM
    [ecomm_etl_hybris].dbo.EarnedRewardOrders ER
        LEFT OUTER JOIN
    (SELECT 
        p_expirationdate, SUM(Reward_Forfeited) AS Reward_Forfeited
    FROM
        (SELECT 
        CR.*, ER.Channel
    FROM
        [ecomm_etl_hybris].dbo.CustomerRewards CR
    INNER JOIN [ecomm_etl_hybris].dbo.EarnedRewardOrders ER ON CR.ownerpkstring = ER.userpk
        AND CR.p_expirationdate = DATEADD(DAY, 61, ER.Tran_dt)
    WHERE
        ER.Channel = 'medifast') A --This subquery will merge channel to customerrewards 
    GROUP BY p_expirationdate) B --This Subquery will sum up rewards forefitted at date level 
	ON ER.Tran_dt = B.p_expirationdate
WHERE
    Tran_dt >= '2019-02-22' AND Tran_Dt < cast(GETDATE() AS Date)
        AND Channel = 'medifast'
GROUP BY Tran_dt , Channel , Reward_Forfeited
ORDER BY 1

select
    Channel,
	Tran_dt,
	 lag([Closing_Balance],1,0) over (partition by Channel order by Tran_dt) [Beginning_Balance],
    Reward_Earned,
    Reward_Used,
	Reward_Forfeited AS [Forfeited],
    [Closing_Balance]
from (
    select 
        Channel,
        Tran_dt,
        sum(Reward_Earned) Reward_Earned,
        sum(Reward_Used) Reward_Used,
		sum(Reward_Forfeited) Reward_Forfeited,
        sum(sum(Reward_Earned-Reward_Used - Reward_Forfeited)) over (partition by Channel order by Tran_dt) [Closing_Balance]
    from #B1
    group by Channel,Tran_dt
) t;


