﻿





--/*
--===============================================================================
--Author:  Kalepsh Patel
--Create date: 08/11/2013
---------------------------USP_XL_024_QuarterlySalesTracker_CPG-----------------------------
-----------------------------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--[BI_SSAS_Cubes]         dbo.DimJason Pharm$Customer Posting Group    Select 
--[BI_SSAS_Cubes]         dbo.BusinessCalendar                         Select
--[BI_SSAS_Cubes]         dbo.FactSales                                Select
--[BI_Reporting]         dbo.Budget                                   Select 
--BI_Reporting            dbo.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup  Insert
--BI_Reporting            dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup      Insert
--BI_Reporting            dbo.dbo.XL_024_QuarterlySalesTracker_LY               Insert
--BI_Reporting            dbo.dbo.XL_024_QuarterlySalesTracker_TY               Insert
--[BI_SSAS_Cubes]          dbo.BusinessCalendar_Five                            Select------Added by Daniel Dagnachew bc. data in [BI_SSAS_Cubes].dbo.BusinessCalendar is expired.
                                                                       -- Query for the table definition is saved in BI repository.(BI Repository\Daniel\Business Calendar Query)   
--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------
--
--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--===============================================================================
--*/
CREATE Procedure [dbo].[USP_XL_024_QuarterlySalesTracker_CPG]
 as 
	
Declare @LastYear int
Declare @ThisYear int

Set @LastYear =(Select CalendarYearQuarter from [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] where RealDate=DateAdd(yy, -1, Convert (Varchar(10),GETDATE()-1,121))) 
Set @ThisYear = (Select CalendarYearQuarter  from [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five]  where RealDate= DateAdd(yy, 0, Convert (Varchar(10),GETDATE()-1,121)))
             
    

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
IF object_id('Tempdb..#Z') Is Not Null DROP TABLE #Z
IF object_id('Tempdb..#CustPost') Is Not Null DROP TABLE #CustPost
	
Select
      [Customer Posting Group]
	  ,[Posting Date]
	  ,CalendarYearQuarter
	  ,CalendarYearMonth
into #CustPost 
from  BI_SSAS_Cubes.dbo.[DimJason Pharm$Customer Posting Group] a
Cross join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] b
where CalendarYearQuarter in(@LastYear,@ThisYear)

Select 
      [CustomerPostingGroup]
	  ,Sum(Amount) as Amount
	  ,a.[Posting Date]
			
into #Z 
FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five]  b
on a.[Posting Date]=b.[Posting Date]
where b.CalendarYearQuarter in (@LastYear,@ThisYear)
group by a.[Posting Date]
        ,[CustomerPostingGroup]
Order by a.[Posting Date]			
				
Select  
      [Customer Posting Group] as [CustomerPostingGroup]
	  ,isnull((Amount),0) as ActualRevenue
	  ,CalendarYearQuarter
	  ,CalendarYearMonth
	  ,a.[Posting Date]
into #A			
FROM  #CustPost a			
left join #Z b
on a.[Customer Posting Group]=b.[CustomerPostingGroup]
and a.[Posting Date]=b.[Posting Date]
where a.[Posting Date]<=DateAdd(dd, DateDiff(dd,0,GetDate()-1), 0)
Order by a.[Posting Date]
                  
             
  
IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
 
SELECT 
     [CalendarMonthID]
	 ,CalendarQuarter
	 ,Channel as CustomerPostingGroup
	 ,Sum(MonthlyBudget) as MonthlyBudget
into #B
FROM [BI_Reporting].dbo.Budget_XL23
where [CalendarMonthID]>=201201
Group by [CalendarMonthID]
         ,Channel
		 ,CalendarQuarter
Order by CalendarMonthID	
            
        
IF object_id('Tempdb..#Budget1') Is Not Null DROP TABLE #Budget1
               
Select 
      CalendarQuarter
	  ,CustomerPostingGroup
	  ,Sum(MonthlyBudget) as MonthlyBudget  
into #Budget1 
from #B 
where CalendarQuarter= @LastYear
Group by CalendarQuarter
        ,CustomerPostingGroup  
      
            
IF object_id('Tempdb..#Budget2') Is Not Null DROP TABLE #Budget2                 
       
Select 
       CalendarQuarter
	  ,CustomerPostingGroup
	  ,Sum(MonthlyBudget) as MonthlyBudget 
into #Budget2 
from #B 
where CalendarQuarter = @ThisYear
Group by CalendarQuarter
        ,CustomerPostingGroup                   
                   

IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C
  
   SELECT convert (int,[BusinessDayOfFiscalQuarter]) as [BusinessDayOfFiscalYear]
          ,Convert(int,[BusinessDayOfFiscalPeriod]) as[BusinessDayOfFiscalPeriod]
          ,a.[Posting Date]
          ,ActualRevenue
          ,MonthlyBudget
          ,a.CustomerPostingGroup
          ,a.CalendarYearMonth
          ,b.CalendarQuarter as CalendarYearQuarter
          into #C from #A a
                  join #Budget1  b
						on a.CalendarYearQuarter=b.CalendarQuarter
                  and a.CustomerPostingGroup COLLATE DATABASE_DEFAULT=b.CustomerPostingGroup
                  join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] c
						on a.[Posting Date]=c.[Posting Date]
						
						--Select * from #C
  
   
 IF object_id('Tempdb..#D') Is Not Null DROP TABLE #D 
      
      Select [BusinessDayOfFiscalPeriod]
      ,[BusinessDayOfFiscalYear]
             ,[Posting Date]
             ,ActualRevenue
             ,MonthlyBudget
             ,CalendarYearMonth 
             ,CalendarYearQuarter
             ,CustomerPostingGroup
             ,row_number() over (Partition by [BusinessDayOfFiscalYear] order by [Posting Date]  ) as No 
             into #D 
             from #C
             where CalendarYearQuarter=@LastYear
           
        
           

  
IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E
   
   ;with CTE as (Select [BusinessDayOfFiscalPeriod],[BusinessDayOfFiscalYear],CustomerPostingGroup
                        ,case when no=11 OR No=12 OR No=13 or No=14 or No=15 or No=16 OR No=17 or No=18 or No=19 or No=20  then dateadd(day,-1,[Posting Date])
					          when no=21 OR No=22 OR No=23 or No=24 or No=25 or No=26 OR No=27 or No=28 or No=29 or No=30 then dateadd(day,-2,[Posting Date])
                              when no=31 OR No=32 OR No=33 or No=34 or No=35 or No=36 OR No=37 or No=38 or No=39 or No=40  then dateadd(day,-3,[Posting Date]) 
                              when no=41 OR No=42 OR No=43 or No=44 or No=45 or No=46 OR No=47 or No=48 or No=49 or No=50 then dateadd(day,-4,[Posting Date])
                              when no=51 OR No=52 OR No=53 or No=54 or No=55 or No=56 OR No=57 or No=58 or No=59 or No=60 then dateadd(day,-5,[Posting Date]) else [Posting Date] end as [Posting Date] 
                        ,ActualRevenue as ActualRevenue
                        ,MonthlyBudget
                        ,CalendarYearMonth 
                        ,CalendarYearQuarter
                         from #D)
                Select [BusinessDayOfFiscalPeriod],[BusinessDayOfFiscalYear],CustomerPostingGroup
                       ,[Posting Date]
                       ,SUM(ActualRevenue) as ActualRevenue
                       ,MonthlyBudget,CalendarYearMonth 
                        ,CalendarYearQuarter  
                       into #E from CTE 
                       group by [BusinessDayOfFiscalPeriod],[Posting Date],MonthlyBudget,CalendarYearMonth, CalendarYearQuarter,CustomerPostingGroup,[BusinessDayOfFiscalYear]
                       order by [Posting Date]
      
IF object_id('Tempdb..#G') Is Not Null DROP TABLE #G
  
     Select 
           c.[BusinessDayOfFiscalPeriod],c.[BusinessDayOfFiscalYear],c.CustomerPostingGroup
           ,c.[Posting Date]
           ,Year(c.[Posting Date]) as Year
           ,c.ActualRevenue
           ,Sum(c1.ActualRevenue) as CummulativeRevenue
           ,isnull(c.MonthlyBudget,0) as MonthlyBudget
           ,c.CalendarYearQuarter
           into #G 
           from #E c
                   inner join #E c1 on c.[Posting Date]>=c1.[Posting Date]
                   and c.CustomerPostingGroup=c1.CustomerPostingGroup
                   group by c.[BusinessDayOfFiscalPeriod],c.[Posting Date],c.ActualRevenue,c.MonthlyBudget,c.CalendarYearQuarter,c.CustomerPostingGroup,c.[BusinessDayOfFiscalYear]
                   order by c.[Posting Date]
                   
                   --Select * from #G
      

IF object_id('Tempdb..#H') Is Not Null DROP TABLE #H  
      
       SELECT convert (int,[BusinessDayOfFiscalQuarter]) as [BusinessDayOfFiscalYear]
          ,Convert(int,[BusinessDayOfFiscalPeriod]) as[BusinessDayOfFiscalPeriod]
          ,a.[Posting Date]
          ,ActualRevenue
          ,MonthlyBudget
          ,a.CustomerPostingGroup
          ,a.CalendarYearMonth
          ,b.CalendarQuarter as CalendarYearQuarter
          into #H from #A a
                  join #Budget2  b
						on a.CalendarYearQuarter=b.CalendarQuarter
                  and a.CustomerPostingGroup COLLATE DATABASE_DEFAULT=b.CustomerPostingGroup
                  join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] c
						on a.[Posting Date]=c.[Posting Date]
    
     
IF object_id('Tempdb..#I') Is Not Null DROP TABLE #I
      
      Select [BusinessDayOfFiscalPeriod]
      ,[BusinessDayOfFiscalYear]
             ,[Posting Date]
             ,ActualRevenue
             ,MonthlyBudget
             ,CalendarYearMonth 
             ,CalendarYearQuarter
             ,CustomerPostingGroup
             ,row_number() over (Partition by [BusinessDayOfFiscalYear] order by [Posting Date]  ) as No 
             into #I 
             from #H
             where CalendarYearQuarter=@ThisYear
     
--Select * from #I
IF object_id('Tempdb..#J') Is Not Null DROP TABLE #J
   

	        ;with CTE as (Select [BusinessDayOfFiscalPeriod],[BusinessDayOfFiscalYear],CustomerPostingGroup
			            ,case when no=11 OR No=12 OR No=13 or No=14 or No=15 or No=16 OR No=17 or No=18 or No=19 or No=20  then dateadd(day,-1,[Posting Date])
                              when no=21 OR No=22 OR No=23 or No=24 or No=25 or No=26 OR No=27 or No=28 or No=29 or No=30 then dateadd(day,-2,[Posting Date])
            --            ,case when (no=1 OR No=2 OR No=3 or No=4 or No=5 or No=6 OR No=7 or No=8 or No=9 or No=10) and (BusinessDayOfFiscalYear = 22 )then dateadd(day,1,[Posting Date]) ------- not working if the first day of the month falls on non-business days DDagnachew 05/03/2016
						      --when (no=11 OR No=12 OR No=13 or No=14 or No=15 or No=16 OR No=17 or No=18 or No=19 or No=20) and (BusinessDayOfFiscalYear <> 22 ) then dateadd(day,-1,[Posting Date])------- not working if the first day of the month falls on non-business days DDagnachew 05/03/2016
                              when no=21 OR No=22 OR No=23 or No=24 or No=25 or No=26 OR No=27 or No=28 or No=29 or No=30 then dateadd(day,-2,[Posting Date])
                              when no=31 OR No=32 OR No=33 or No=34 or No=35 or No=36 OR No=37 or No=38 or No=39 or No=40  then dateadd(day,-3,[Posting Date]) 
                              when no=41 OR No=42 OR No=43 or No=44 or No=45 or No=46 OR No=47 or No=48 or No=49 or No=50 then dateadd(day,-4,[Posting Date])
                              when no=51 OR No=52 OR No=53 or No=54 or No=55 or No=56 OR No=57 or No=58 or No=59 or No=60 then dateadd(day,-5,[Posting Date]) else [Posting Date] end as [Posting Date] 
                        ,ActualRevenue as ActualRevenue
                        ,MonthlyBudget
                        ,CalendarYearMonth 
                        ,CalendarYearQuarter
                         from #I)
                Select [BusinessDayOfFiscalPeriod],[BusinessDayOfFiscalYear],CustomerPostingGroup
                       ,[Posting Date]
                       ,SUM(ActualRevenue) as ActualRevenue
                       ,MonthlyBudget,CalendarYearMonth 
                        ,CalendarYearQuarter  
                       into #J 
					   from CTE 
                       group by [BusinessDayOfFiscalPeriod],[Posting Date],MonthlyBudget,CalendarYearMonth, CalendarYearQuarter,CustomerPostingGroup,[BusinessDayOfFiscalYear]
                       order by [Posting Date]
                    
               
      
 
Insert into #G 
       Select 
           c.[BusinessDayOfFiscalPeriod],c.[BusinessDayOfFiscalYear],c.CustomerPostingGroup
           ,c.[Posting Date]
           ,Year(c.[Posting Date]) as Year
           ,c.ActualRevenue
           ,Sum(c1.ActualRevenue) as CummulativeRevenue
           ,isnull(c.MonthlyBudget,0) as MonthlyBudget
           ,c.CalendarYearQuarter
          
           from #J c
                   inner join #J c1 on c.[Posting Date]>=c1.[Posting Date]
                   and c.CustomerPostingGroup=c1.CustomerPostingGroup
                   group by c.[BusinessDayOfFiscalPeriod],c.[Posting Date],c.ActualRevenue,c.MonthlyBudget,c.CalendarYearQuarter,c.CustomerPostingGroup,c.[BusinessDayOfFiscalYear]
                   order by c.[Posting Date]
      
IF object_id('Tempdb..#K') Is Not Null DROP TABLE #K

             SELECT 
             Count(distinct [BusinessDayOfFiscalQuarter]) as [CountBusinessDay] 
             ,CalendarYearQuarter 
             into #K 
             FROM [BI_SSAS_Cubes].[dbo].[BusinessCalendar_Five] a
                     where CalendarYearQuarter  in ( @LastYear, @ThisYear  )--(@LastYear,@ThisYear)
                     Group by CalendarYearQuarter 
                     
                     --select * from #K
  
IF object_id('Tempdb..#L') Is Not Null DROP TABLE #L
  
  Select 
       distinct Convert(Numeric(15,0),[BusinessDayOfFiscalQuarter])as [BusinessDayOfFiscalYear]
       ,[CountBusinessDay]
       ,RealDate 
       into #L 
       FROM [BI_SSAS_Cubes].[dbo].[BusinessCalendar_Five] a
           Join #K b
           on a.CalendarYearQuarter =b.CalendarYearQuarter 
           where a.CalendarYearQuarter  in ( @LastYear, @ThisYear  )---(@LastYear,@ThisYear)
  
IF object_id('Tempdb..#M') Is Not Null DROP TABLE #M
  
  Select ([CountBusinessDay]-[BusinessDayOfFiscalYear]) as RemainingDays
          ,RealDate
          ,[CountBusinessDay]
          into #M from #L
          
         
  

--IF object_id('Tempdb..BI_Reporting.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup') Is Not Null DROP TABLE BI_Reporting.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup    
truncate table  BI_Reporting.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup 
Insert into  BI_Reporting.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup    

      Select * 
          ,Month([Posting Date])as Month
          ,Case when RemainingDays=0 then (MonthlyBudget-CummulativeRevenue) else  (MonthlyBudget-CummulativeRevenue)/(RemainingDays) end  as AvgDayRemaining
              from #G g
              join #M l
                  on g.[Posting Date]=l.RealDate
                  
                 where CalendarYearQuarter = @LastYear  
      
 --IF object_id('Tempdb.BI_Reporting.dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup') Is Not Null DROP TABLE BI_Reporting.dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup
Truncate  table BI_Reporting.dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup
Insert into BI_Reporting.dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup
     Select * 
          ,Month([Posting Date])as Month
          ,Case when RemainingDays=0 then (MonthlyBudget-CummulativeRevenue) else  (MonthlyBudget-CummulativeRevenue)/(RemainingDays) end  as AvgDayRemaining
              from #G g
              join #M l
                  on g.[Posting Date]=l.RealDate
                  
                 where CalendarYearQuarter = @ThisYear  
      
      
  ----------------------------------------------------dbo.XL_024_QuarterlySalesTracker_LY ---------------------------- 
Truncate table  dbo.XL_024_QuarterlySalesTracker_LY 
insert into dbo.XL_024_QuarterlySalesTracker_LY
      
SELECT [BusinessDayOfFiscalPeriod]
      ,BusinessDayOfFiscalYear
      ,[Posting Date]
      ,[Year]
      ,Sum([ActualRevenue]) as [ActualRevenue]
      ,Sum([CummulativeRevenue]) as [CummulativeRevenue]
      ,Sum([MonthlyBudget]) as [MonthlyBudget]
      ,CalendarYearQuarter
      ,[RemainingDays]
      ,[RealDate]
      ,[CountBusinessDay]
      ,[Month]
      ,Sum([AvgDayRemaining]) as [AvgDayRemaining]
FROM [BI_Reporting].[dbo].[XL_024_QuarterlySalesTracker_LY_PostingGroup]
Group by [BusinessDayOfFiscalPeriod]
      ,BusinessDayOfFiscalYear
      ,[Posting Date]
      ,[Year]
      ,CalendarYearQuarter
      ,[RemainingDays]
      ,[RealDate]
      ,[CountBusinessDay]
      ,[Month]
      Order by BusinessDayOfFiscalYear
 ---------------------------------------------------dbo.XL_024_QuarterlySalesTracker_TY-------------------------------
Truncate table  dbo.XL_024_QuarterlySalesTracker_TY 
insert into dbo.XL_024_QuarterlySalesTracker_TY

SELECT [BusinessDayOfFiscalPeriod]
      ,BusinessDayOfFiscalYear
      ,[Posting Date]
      ,[Year]
      ,Sum([ActualRevenue]) as [ActualRevenue]
      ,Sum([CummulativeRevenue]) as [CummulativeRevenue]
      ,Sum([MonthlyBudget]) as [MonthlyBudget]
      ,CalendarYearQuarter
      ,[RemainingDays]
      ,[RealDate]
      ,[CountBusinessDay]
      ,[Month]
      ,Sum([AvgDayRemaining]) as [AvgDayRemaining]
FROM [BI_Reporting].[dbo].[XL_024_QuarterlySalesTracker_TY_PostingGroup]
Group by [BusinessDayOfFiscalPeriod]
      ,BusinessDayOfFiscalYear
      ,[Posting Date]
      ,[Year]
      ,CalendarYearQuarter
      ,[RemainingDays]
      ,[RealDate]
      ,[CountBusinessDay]
      ,[Month]
       Order by BusinessDayOfFiscalYear
      
      
      
      
      
      
      

      
      
      
      
      
      
      
      
      

      
      
      
      
      
      
      
      
      















