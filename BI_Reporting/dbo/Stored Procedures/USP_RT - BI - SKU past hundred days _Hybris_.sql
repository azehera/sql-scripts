﻿







/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------
HYBRIS orders last 100 days
--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------
ecomm_etl_hybris     [dbo].[order_line]                     Select
ecomm_etl_hybris     [dbo].[[order_Header]                  Select                     
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------
8/14/2019	   Daniel D.				pointed to hybris_ETL
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - BI - SKU past hundred days (Hybris)]
AS

/* Script to find Customers who have placed certain sku in their orders */

--SELECT DISTINCT 
--	OH.order_type,
--	U.useremail,
--	U.cartcustnumber,
--	OL.sku,
--	OH.channel,
--	CAST(OH.orderdate AS DATE) orderdate
--FROM ECOMM_ETL_Hybris.dbo.order_line OL
--INNER JOIN ECOMM_ETL_Hybris.dbo.order_header OH
--ON OL.ordernumber = OH.ordernumber
--INNER JOIN ECOMM_ETL_Hybris.dbo.users U
--ON U.pk = OH.user_pk
----WHERE OH.channel ='medifast' 
--WHERE OH.orderdate >= GETDATE()-100
----AND OL.sku LIKE '57600%'

SELECT DISTINCT
       ot.Code order_type,
       U.p_uid useremail,
       U.p_cartcustnumber cartcustnumber,
       CAST(p.p_sku AS CHAR(255)) AS 'sku',
       CASE
           WHEN OH.p_site IN ( 8796093088808, 8796125922344 ) THEN
               'Optavia - US '
           WHEN OH.p_site IN ( 8796093056040, 8796093056040 ) THEN
               'Medifast'
           WHEN OH.p_site = 8796125856808 THEN
               'Optavia - HK'
           WHEN OH.p_site = 8796125824040 THEN
               'Optavia - SG'
           ELSE
               'other'
       END AS 'channel',
       CAST(OH.createdTS AS DATE) orderdate
FROM Hybris_ETL.mdf_prd_database.orderentries OL WITH (NOLOCK)
    INNER JOIN Hybris_ETL.mdf_prd_database.orders OH WITH (NOLOCK)
        ON OL.p_order = OH.PK
    INNER JOIN Hybris_ETL.mdf_prd_database.users U WITH (NOLOCK)
        ON U.PK = OH.p_user
    LEFT JOIN Hybris_ETL.mdf_prd_database.enumerationvalues ot WITH (NOLOCK)
        ON OH.p_ordertype = ot.PK
    INNER JOIN Hybris_ETL.mdf_prd_database.products AS p WITH (NOLOCK)
        ON p.PK = OL.p_product
--WHERE OH.channel ='medifast' 
WHERE OH.createdTS >= GETDATE() - 100;


