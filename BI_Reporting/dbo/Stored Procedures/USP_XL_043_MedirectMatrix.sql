﻿create procedure [dbo].[USP_XL_043_MedirectMatrix] as 

Set Nocount on;

declare @date varchar(10)
set @date = '2013-09-01'

-------PAGE ID------
IF object_id('tempdb..#PageId') is not null DROP TABLE #PageId
SELECT
 [SESSION_ID]
,[COOKIE_ID]
,CONVERT(varchar(10),TIMESTAMP,121) CalDate
,Case When PAGE_ID IN
('LOGIN', 'YOUR INFORMATION', 'SHIPPING ADDRESS', 'PURCHASE & SHIPPING OPTIONS', 
 'PAYMENT METHOD', 'ORDER REVIEW') Then 'CHECKOUT' 
      When PAGE_ID IN
('WEIGHT-LOSS SUCCESS STORIES: UNDER 50 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: UNDER 30 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 80-99 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 70-79 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 60-69 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 50-59 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 50 - 99 LBS',
'WEIGHT-LOSS SUCCESS STORIES: 40-49 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 30-39 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 100+ POUNDS') Then   'WEIGHT-LOSS SUCCESS STORIES'         
      When PAGE_ID ='ADDED TO CART' then 'ADDED TO CART'
Else Page_id end As PAGE_ID 
into #PageId
FROM [DDF].[dbo].[PageView]

where PAGE_ID in
('MEDIFAST HOME PAGE', 'BEST SELLERS', 'WEIGHT LOSS PLAN',
'SHOPPING CART', 'GET STARTED NOW', 'SHOPPING CONFIRMATION',

'LOGIN', 'YOUR INFORMATION', 'SHIPPING ADDRESS', 'PURCHASE & SHIPPING OPTIONS', 
'PAYMENT METHOD', 'ORDER REVIEW',


'WEIGHT-LOSS SUCCESS STORIES: UNDER 50 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: UNDER 30 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 80-99 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 70-79 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 60-69 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 50-59 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 50 - 99 LBS',
'WEIGHT-LOSS SUCCESS STORIES: 40-49 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 30-39 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 100+ POUNDS',

'ADDED TO CART')
and  Convert(Varchar(10),SSISTimestamp,121)>= @date
GROUP BY
[SESSION_ID]
,[COOKIE_ID]
,Case When PAGE_ID IN
('LOGIN', 'YOUR INFORMATION', 'SHIPPING ADDRESS', 'PURCHASE & SHIPPING OPTIONS', 
 'PAYMENT METHOD', 'ORDER REVIEW') Then 'CHECKOUT' 
      When PAGE_ID IN
('WEIGHT-LOSS SUCCESS STORIES: UNDER 50 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: UNDER 30 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 80-99 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 70-79 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 60-69 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 50-59 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 50 - 99 LBS',
'WEIGHT-LOSS SUCCESS STORIES: 40-49 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 30-39 POUNDS',
'WEIGHT-LOSS SUCCESS STORIES: 100+ POUNDS') Then   'WEIGHT-LOSS SUCCESS STORIES'         
      When PAGE_ID ='ADDED TO CART' then 'ADDED TO CART'
Else Page_id end
,CONVERT(varchar(10),TIMESTAMP,121)

--------SALES------
IF object_id('tempdb..#Sales') is not null DROP TABLE #Sales
SELECT 
 CONVERT(varchar(10),TIMESTAMP,121) as CalDate
,Count(Order_id) as OrderCount
,Sum(convert(money,[ORDER_TOTAL])) as [ORDER_TOTAL]
Into #Sales    
FROM [DDF].[dbo].[Order]
Where Convert(Varchar(10),SSISTimestamp,121)>= @date
Group By CONVERT(varchar(10),TIMESTAMP,121)

-------SESSION----
IF object_id('tempdb..#Session') is not null DROP TABLE #Session
Select 
CONVERT(Varchar(10),[TIMESTAMP],121) as CalDate
,Convert(Decimal(18,0),COUNT(Distinct [SESSION_ID]))  as [Order/Session]
into #Session
From [DDF].[dbo].[SessionFirstPageView]
where Convert(Varchar(10),SSISTimestamp,121)>= @date
group by CONVERT(Varchar(10),[TIMESTAMP],121)

--select * from #Session

--------TRACK VISITOR-------
IF object_id('tempdb..#Visitor') is not null DROP TABLE #Visitor
Select 
 CONVERT(Varchar(10),[TIMESTAMP],121) as CalDate
,COUNT(Distinct(Cookie_id)) as tot
Into #Visitor
From [DDF].[dbo].[SessionFirstPageView]
Where Convert(Varchar(10),SSISTimestamp,121)>= @date
Group By CONVERT(Varchar(10),[TIMESTAMP],121)

------NEW VISITORS----
IF object_id('tempdb..#NewVisitor') is not null DROP TABLE #NewVisitor
Select
  convert(varchar(10),TIMESTAMP,121) as CalDate
,COUNT(Distinct(cookie_id)) as NewVisitors
into #NewVisitor
FROM [DDF].[dbo].[SessionFirstPageView]
 where  Convert(Varchar(10),SSISTimestamp,121)>= @date
  and convert(varchar(10),TIMESTAMP,121) = convert(varchar(10),FIRST_TIMESTAMP,121)
 group by convert(varchar(10),TIMESTAMP,121)
 
--select * from #NewVisitor order by CalDate desc	
 
------GROUPING-------
IF object_id('tempdb..#Group') is not null DROP TABLE #Group
select
 CalDate 
,SESSION_ID
,CASE WHEN PAGE_ID ='GET STARTED NOW' THEN 1 ELSE 0 END AS [GET STARTED NOW]
,CASE WHEN PAGE_ID ='SHOPPING CONFIRMATION' THEN 1 ELSE 0 END AS [SHOPPING CONFIRMATION]
,CASE WHEN PAGE_ID ='WEIGHT LOSS PLAN' THEN 1 ELSE 0 END AS [WEIGHT LOSS PLAN]
,CASE WHEN PAGE_ID ='CHECKOUT' THEN 1 ELSE 0 END AS [CHECKOUT]
,CASE WHEN PAGE_ID ='MEDIFAST HOME PAGE' THEN 1 ELSE 0 END AS [MEDIFAST HOME PAGE]
,CASE WHEN PAGE_ID ='BEST SELLERS' THEN 1 ELSE 0 END AS [BEST SELLERS]
,CASE WHEN PAGE_ID ='WEIGHT-LOSS SUCCESS STORIES' THEN 1 ELSE 0 END AS [WEIGHT-LOSS SUCCESS STORIES]
,CASE WHEN PAGE_ID ='SHOPPING CART' THEN 1 ELSE 0 END AS [SHOPPING CART]
,CASE WHEN PAGE_ID ='ADDED TO CART' THEN 1 ELSE 0 END AS [ADDED TO CART]
into #Group 
from #PageId

IF object_id('tempdb..#MASTER') is not null DROP TABLE #MASTER
CREATE TABLE #MASTER (
[CalDate] [datetime] NULL,
[GET STARTED NOW] [numeric] (15,0) NULL,
[ADDED TO CART][numeric] (15,0) NULL,
[SHOPPING CONFIRMATION] [numeric] (15,0) NULL,
[WEIGHT LOSS PLAN] [numeric] (15,0) NULL,
[CHECKOUT] [numeric] (15,0) NULL,
[MEDIFAST HOME PAGE] [numeric] (15,0) NULL,
[BEST SELLERS] [numeric] (15,0) NULL,
[WEIGHT-LOSS SUCCESS STORIES] [numeric] (15,0) NULL,
[SHOPPING CART] [numeric] (15,0) NULL,
[SALES] [numeric](38, 2) NULL,
[ORDER COUNT] [numeric] (15,0) NULL,
[AOV] [numeric] (15,2) NULL,
[SESSION_ID] [numeric](38, 0) NULL,
[ORDER/SESSION] [numeric] (15,2) NULL,
[ABANDONMENT RATE] [numeric] (15,1) NULL,
[TRACK VISITORS] [numeric] (15,0) NULL,
[NEW VISITORS] [numeric] (15,0) NULL,
[REPEAT VISITORS] [numeric] (15,0) NULL)

INSERT INTO #MASTER
select
 CalDate 
,SUM([GET STARTED NOW]) as [GET STARTED NOW]
,SUM([ADDED TO CART]) AS [ADDED TO CART]
,SUM([SHOPPING CONFIRMATION]) AS [SHOPPING CONFIRMATION]
,SUM([WEIGHT LOSS PLAN]) AS [WEIGHT LOSS PLAN]
,SUM([CHECKOUT]) AS [CHECKOUT]
,SUM([MEDIFAST HOME PAGE]) AS [MEDIFAST HOME PAGE]
,SUM([BEST SELLERS]) AS [BEST SELLERS]
,SUM([WEIGHT-LOSS SUCCESS STORIES]) AS [WEIGHT-LOSS SUCCESS STORIES]
,SUM([SHOPPING CART]) AS [SHOPPING CART]
,0.00 AS SALES
,0 AS [ORDER COUNT]
,0.00 AS [AOV] 
,Count ((SESSION_ID)) as Session_Id
,0.00 AS [ORDER/SESSION]
,0.0 AS [ABANDONMENT RATE]
,0 AS [TRACK VISITOR] 
,0 AS [NEW VISITORS] 
,0 AS [REPEAT VISITORS] 
FROM #Group
GROUP BY
CalDate 

UPDATE #MASTER
SET SESSION_ID = S.[Order/Session]
FROM #Session S
WHERE #MASTER.CalDate =S.CalDate

UPDATE #MASTER
SET SALES = S.[ORDER_TOTAL], [ORDER COUNT] = S.OrderCount, AOV = S.[ORDER_TOTAL]/S.OrderCount
FROM #Sales S
where #MASTER.CalDate =s.CalDate

UPDATE #MASTER
SET [TRACK VISITORS]= s.tot
FROM #Visitor S
where #MASTER.CalDate =s.CalDate

UPDATE #MASTER
SET [ORDER/SESSION] = ([ORDER COUNT]/SESSION_ID)*100, [ABANDONMENT RATE] =(1-([SHOPPING CONFIRMATION]/([ADDED TO CART]+[SHOPPING CONFIRMATION])))*100

UPDATE #MASTER
SET [NEW VISITORS] = S.NewVisitors
FROM #NewVisitor S
where #MASTER.CalDate =s.CalDate

UPDATE #MASTER
SET [REPEAT VISITORS] = [TRACK VISITORS]-[NEW VISITORS]

truncate table dbo.XL_043_MedirectMatrix
insert into dbo.XL_043_MedirectMatrix
select * 
from #MASTER
ORDER BY CalDate DESC

select * from dbo.XL_043_MedirectMatrix

---------PAGE_ID in('MEDIFAST HOME PAGE', 'BEST SELLERS', 'WEIGHT LOSS PLAN',
------'SHOPPING CART', 'GET STARTED NOW', 'SHOPPING CONFIRMATION')

------CheckOut= 
------LOGIN,
------YOUR INFORMATION,
------SHIPPING ADDRESS,
------PURCHASE & SHIPPING OPTIONS,
------PAYMENT METHOD,
------ORDER REVIEW,

------WEIGHT-LOSS SUCCESS STORIES=
------WEIGHT-LOSS SUCCESS STORIES: UNDER 50 POUNDS
------WEIGHT-LOSS SUCCESS STORIES: UNDER 30 POUNDS
------WEIGHT-LOSS SUCCESS STORIES: 80-99 POUNDS
------WEIGHT-LOSS SUCCESS STORIES: 70-79 POUNDS
------WEIGHT-LOSS SUCCESS STORIES: 60-69 POUNDS
------WEIGHT-LOSS SUCCESS STORIES: 50-59 POUNDS
------WEIGHT-LOSS SUCCESS STORIES: 50 - 99 LBS
------WEIGHT-LOSS SUCCESS STORIES: 40-49 POUNDS
------WEIGHT-LOSS SUCCESS STORIES: 30-39 POUNDS
------WEIGHT-LOSS SUCCESS STORIES: 100+ POUNDS
