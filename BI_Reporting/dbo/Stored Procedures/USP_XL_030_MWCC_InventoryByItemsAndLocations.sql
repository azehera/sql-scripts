﻿
/*
--==================================================================================
Author:         Luc Emond
Create date: 05/08/2013
----------------------[dbo].[USP_XL_030_MWCC_InventoryByItemsAndLocations]-------------------
Provide MWCC Weekly Inventory for all Items and locations with their cost-----
--=================================================================================    
REFERENCES
Database              Table/View/UDF                       Action            
------------------------------------------------------------------------------------
NAV-ETL            [Snapshot$NAV_Inventory]     		  Select 
NAV-ETL            [Jason Pharm$Item]	            	  Select  
Book4Time_ETL      [B4T_product_master]                   Select
Book4Time_ETL      [B4T_location]                         Select
                        
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
--*/ 

CREATE PROCEDURE [dbo].[USP_XL_030_MWCC_InventoryByItemsAndLocations] AS

SET NOCOUNT ON;

----DECLATE TABLE----
DECLARE @tblWorkTable TABLE (
	[RunDate] [datetime] NULL,
	[InventoryDate] [datetime] NULL,
	[LocationId] [varchar](10) NULL,
	[Location#] [varchar](50) NULL,
	[InvLocationName] [varchar](50) NULL,
	[B4T_ItemNo] [varchar](20)NULL,
	[NavItemNo] [varchar](20) NULL,
	[Description] [varchar](30) NULL,
	[InvPostClass] [varchar](10) NULL,
	[InvPostGroup] [varchar](10) NULL,
	[ItemCategoryCode] [varchar](10) NULL,
	[Product Group Code] [varchar](10) NULL,
	[Standard Cost] [numeric](38, 20) NULL,
	[OnHand_Bx] [int] NULL,
	[QtyPerUnitOfMeasure] [numeric](18, 6) NULL,
	[OnHand_Ea] [int] NULL,
	[OnHandAmtDay] [numeric](18, 2) NULL,
	[Category] [char](25) NULL
) 

INSERT INTO @tblWorkTable
SELECT 
DISTINCT(RunDate)
,Yesterday as InventoryDate
,InvLocation AS LocationId
,l.legal_name As Location#
,Case When InvLocationName ='' Then InvLocation Else InvLocationName end As InvLocationName
,B4T_ItemNo
,NavItemNo
,j.Description
,InvPostClass
,InvPostGroup
,ItemCategoryCode
,[Product Group Code]
,[Standard Cost]
,OnHandQtyDay As OnHand_Bx
,QtyPerUnitOfMeasure
,OnHandQtyBaseDay As OnHand_Ea
,OnHandAmtDay
,Case When m.retail_price =0 Then 'Non-Inventory' Else 'Inventory' End as Category
FROM [NAV_ETL].[dbo].[Snapshot$NAV_Inventory] I
Left Outer join 
     [NAV_ETL].[dbo].[Jason Pharm$Item] J 
     On I.ItemNo COLLATE Latin1_General_CI_AS = J.[No_]  COLLATE Latin1_General_CI_AS
Left Outer Join 
     [Book4Time_ETL].[dbo].[B4T_location] l
     On I.InvLocation = l.Location_Id
Left Outer Join 
     [Book4Time_ETL].[dbo].[B4T_product_master] m
     On I.NavItemNo COLLATE Latin1_General_CI_AS = m.[Sku]  
WHERE ((NavItemNo <> '') 
  AND (NavItemNo IS NOT NULL)) 
  AND InvLocationType ='MWCC'
  AND Yesterday = (Select Max([Yesterday]) from [NAV_ETL].[dbo].[Snapshot$NAV_Inventory])
ORDER BY RunDate


---SELECT OF EXCEL TABLE------
SELECT * FROM @tblWorkTable where onhand_ea <>0