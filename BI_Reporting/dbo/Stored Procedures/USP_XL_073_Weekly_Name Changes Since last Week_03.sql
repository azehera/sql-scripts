﻿






/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/10/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CO_APPLICANT] 		     Select   
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  


=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/

CREATE  procedure [dbo].[USP_XL_073_Weekly_Name Changes Since last Week_03] as 
set nocount on ;


IF object_id('Tempdb..#Coapp') Is Not Null DROP TABLE #Coapp
--------------------Pull Name Changes Since last Week-------------------------		
----------------Pulling only Active Co-App Information------------------
Select * 
 Into #Coapp
	From [ODYSSEY_ETL].[dbo].[ODYSSEY_CO_APPLICANT] CO
	Where [IS_OBSOLETE] is null
	
-----------------------Pulling Name Change info-------------------------	
Select
	 [Customer_Number] 
	,C.[First_Name] as [Coach First Name]
	,C.[Last_Name] as [Coach Last Name]
	,CO.[FIRST_NAME] as [Co-App First Name]
	,CO.[LAST_NAME] as [Co-App Last Name]
	,[RECOGNITION_NAME]
	,convert(varchar(10),C.[Last_Changed],121) as [Last_Changed]
	From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
			Left Join #Coapp CO
				on C.[CUSTOMER_ID] = CO.[CUSTOMER_ID]
		Where convert(varchar(10),C.[Last_Changed],121) >= GETDATE() - 8
			and (C.[First_Name]+' '+C.[Last_Name]) <> [RECOGNITION_NAME]
		Order by [Last_Changed]



