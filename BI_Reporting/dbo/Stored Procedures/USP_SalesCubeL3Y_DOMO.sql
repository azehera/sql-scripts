﻿





/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                  Action            
------------------------------------------------------------------------------------

DOMO               dbo.V_Sales_Cube_DOMO_TEMP         Select 

REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_SalesCubeL3Y_DOMO]
AS
SELECT *
FROM DOMO.dbo.V_Sales_Cube_DOMO_TEMP
WHERE   [Posting Date] >= '01-01-2016'
--where [Posting Date] >= CAST(GETDATE()-1 AS DATE) --'01-01-2016' 

