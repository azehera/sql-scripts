﻿
CREATE procedure [dbo].[USP_XL_078_BeSlim_Club_Rewards] @Channel varchar(8) as

DECLARE @StartDate Date
DECLARE @EndDate Date
SET @StartDate = '08/01/2014'	-- Startup Date For The Rewards Program
SET @EndDate = '01/01/2016'

-- Notes: ACT_TYPE_CD contains 'ASR' while ACA_TYPE_CD contains 'DBT' or 'CRT'

-- Earned rewards order numbers
IF OBJECT_ID('tempdb..#OrderChannel') IS NOT NULL DROP TABLE #OrderChannel
CREATE TABLE #OrderChannel (OrderNo varchar(20), Channel varchar(20))

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACT_NOTES, 'Rewards earned on order:', '')) [OrderNo], '' [Channel] from [ECOMM_ETL ].dbo.ACCOUNT (nolock) where
ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
AND ISNULL(ACT_NOTES, '') LIKE 'Rewards earned on order:%' AND ACT_ORIG_BALANCE > 0

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACT_NOTES, 'Rtn Order:', '')) [OrderNo], '' [Channel] from [ECOMM_ETL ].dbo.ACCOUNT (nolock) where
ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACT_NOTES, '') LIKE 'Rtn Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'Rtn Order:', '')) [OrderNo], '' [Channel] 
from [ECOMM_ETL ].dbo.ACCOUNT (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) on ACA_ACT_ID = ACT_ID 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Rtn Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'Canc Order:', '')) [OrderNo], '' [Channel] 
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate and ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') LIKE 'Canc Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACT_NOTES, 'Canc Order:', '')) [OrderNo], '' [Channel] from [ECOMM_ETL ].dbo.ACCOUNT (nolock) 
where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACT_NOTES, '') LIKE 'Canc Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'Rwd on Order:', '')) [OrderNo], '' [Channel] 
from [ECOMM_ETL ].dbo.ACCOUNT (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) on ACA_ACT_ID = ACT_ID 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Rwd on Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'Reward on Order:', '')) [OrderNo], '' [Channel] 
from [ECOMM_ETL ].dbo.ACCOUNT (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) on ACA_ACT_ID = ACT_ID 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Reward on Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'CC_TXT_REWARD_ON_ORDER', '')) [OrderNo], '' [Channel] 
from [ECOMM_ETL ].dbo.ACCOUNT (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) on ACA_ACT_ID = ACT_ID 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'CC_TXT_REWARD_ON_ORDER%'

INSERT INTO #OrderChannel
SELECT ORH_SOURCE_NBR [OrderNo], '' [Channel]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
inner join [ECOMM_ETL ].dbo.PAYMENT (nolock) on ACA_PAY_ID = PAY_ID inner join [ECOMM_ETL ].dbo.V_ORDER_HEADER (nolock) on PAY_ORH_ID = ORH_ID
left join #OrderChannel c on ORH_SOURCE_NBR = c.OrderNo
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate and ACT_TYPE_CD = 'ASR' and NOT ACA_PAY_ID IS NULL and c.OrderNo IS NULL

INSERT INTO #OrderChannel
SELECT 'NONE', 'TSFL'

-- Update #OrderChannel with Channel
update #OrderChannel set Channel = o.ORG_ACCOUNT_NBR  
from #OrderChannel c inner join [ECOMM_ETL ].dbo.V_ORDER_HEADER h (nolock) on c.OrderNo = h.ORH_SOURCE_NBR
inner join [ECOMM_ETL ].dbo.ORGANIZATION o on h.ORH_ORG_ID = o.ORG_ID

IF OBJECT_ID('tempdb..#AccountChannel') IS NOT NULL DROP TABLE #AccountChannel
CREATE TABLE #AccountChannel (AccountId bigint, Channel varchar(20))
INSERT INTO #AccountChannel 
select a.ACT_ID, org.ORG_ACCOUNT_NBR from [ECOMM_ETL ].dbo.ACCOUNT a 
left join [ECOMM_ETL ].dbo.V_USER_ACCOUNT ua on a.ACT_OWNED_BY = ua.USA_ID
left join [ECOMM_ETL ].dbo.USER_ORG uso with (nolock) on uso.USO_ID = ua.USA_DEFAULT_USO_ID 
left join [ECOMM_ETL ].dbo.ORGANIZATION org with (nolock) on org.org_id = uso.uso_org_id
where org.ORG_ACCOUNT_NBR in ('MEDIFAST', 'TSFL')

IF OBJECT_ID('tempdb..#Trans') IS NOT NULL DROP TABLE #Trans
CREATE TABLE #Trans (Channel varchar(20), TranDate varchar(10), OrderNo varchar(20), OrderCount int, Category varchar(20), TranAmt money)

-- Earned rewards - Column Name = RewardEarned
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACT_NOTES, 'Rewards earned on order:', '')) [OrderNo], 
1 [OrderCount], 'RewardEarned' [Category], ACT_ORIG_BALANCE [TranAmt] from [ECOMM_ETL ].dbo.ACCOUNT (nolock) where
ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
AND ISNULL(ACT_NOTES, '') LIKE 'Rewards earned on order:%' AND ACT_ORIG_BALANCE > 0

-- Earned rewards - Column Name = RewardEarned
INSERT INTO #Trans
SELECT c.Channel [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], 'Missing' [OrderNo], 
1 [OrderCount], 'RewardEarned' [Category], ACT_ORIG_BALANCE [TranAmt] 
from [ECOMM_ETL ].dbo.ACCOUNT a (nolock) inner join #AccountChannel c on a.ACT_ID = c.AccountId 
where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
AND ISNULL(ACT_NOTES, '') = '' AND dbo.udf_isRewardAccount (ACT_ID) = 'Y' AND ACT_ORIG_BALANCE > 0

-- Applied rewards taken back for returned orders - Column Name = ReclaimedReturn
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'Rtn Order:', '')) [OrderNo],
0 [OrderCount], 'ReclaimedReturn' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID where
ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Rtn Order:%'

-- Balances created today for reclaimed rewards that could not be subtracted from existing rewards accounts - Column Name = UnReclaimedReturn
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACT_NOTES, 'Rtn Order:', '')) [OrderNo],
0 [OrderCount], 'UnReclaimedReturn' [Category], ACT_ORIG_BALANCE [TranAmt] from [ECOMM_ETL ].dbo.ACCOUNT (nolock) where
ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACT_NOTES, '') LIKE 'Rtn Order:%' AND ACT_ORIG_BALANCE < 0

-- Applied rewards that were taken back for cancelled orders - Column Name = Cancelled
INSERT INTO #Trans
SELECT '' [Channel], CONVERT (varchar(10), ACT_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACT_NOTES, 'Rewards applied to cancelled order:', '')) [OrderNo],
0 [OrderCount], 'Cancelled' [Category], 0 - ACT_ORIG_BALANCE [TranAmt] from [ECOMM_ETL ].dbo.ACCOUNT (nolock)
where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACT_NOTES, '') LIKE 'Rewards applied to cancelled order%'

-- Amounts removed from rewards accounts due to cancellations - Column Name = ReclaimedCancel
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'Canc Order:', '')) [OrderNo],
0 [OrderCount], 'ReclaimedCancel' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Canc Order:%'

-- Balances created because rewards for a cancelled order could not be fully applied to the rewards account - Column Name = UnReclaimedCancel
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACT_NOTES, 'Canc Order:', '')) [OrderNo],
0 [OrderCount], 'UnReclaimedCancel' [Category], ACT_ORIG_BALANCE [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT (nolock) where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate and ACT_TYPE_CD = 'ASR' and ACT_ORIG_BALANCE < 0 
and ISNULL(ACT_NOTES, '') LIKE 'Canc Order:%'

-- Wipe out any remaining referral credit account balances - Column Name = Eliminate
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], 'NONE' [OrderNo], 
0 [OrderCount], 'Eliminate' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') = 'Elim Ref Cred Bal'

-- Forfeited rewards - Column Name = Forfeited
INSERT INTO #Trans
SELECT ac.Channel, CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], '' [OrderNo], 
0 [OrderCount], 'Forfeited' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
left join #AccountChannel ac on ACT_ID = ac.AccountId 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and (ISNULL(ACA_NOTES, '') = 'Unused rewards' OR
ISNULL(ACA_NOTES, '') = 'CC_TXT_UNUSED_REWARDS')

-- Apply rewards to any negative rewards - Column Name = OffsetNegReward - Method 1
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'Rwd on Order:', '')) [OrderNo],
0 [OrderCount], 'OffsetNegReward' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') LIKE 'Rwd on Order:%'

-- Apply rewards to any negative rewards - Column Name = OffsetNegReward - Method 2
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'Reward on Order:', '')) [OrderNo],
0 [OrderCount], 'OffsetNegReward' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') LIKE 'Reward on Order:%'

-- Apply rewards to any negative rewards - Column Name = OffsetNegReward - Method 3
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'CC_TXT_REWARD_ON_ORDER', '')) [OrderNo],
0 [OrderCount], 'OffsetNegReward' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') LIKE 'CC_TXT_REWARD_ON_ORDER%'

-- Rewards used - Column Name = RewardUsed
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], ORH_SOURCE_NBR [OrderNo],
0 [OrderCount], 'RewardUsed' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from [ECOMM_ETL ].dbo.ACCOUNT_ACTIVITY (nolock) inner join [ECOMM_ETL ].dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
inner join [ECOMM_ETL ].dbo.PAYMENT (nolock) on ACA_PAY_ID = PAY_ID inner join [ECOMM_ETL ].dbo.V_ORDER_HEADER (nolock) on PAY_ORH_ID = ORH_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate and ACT_TYPE_CD = 'ASR' and NOT ACA_PAY_ID IS NULL

-- Update Channel for all #Trans rows
update #Trans set Channel = o.Channel from #Trans t inner join #OrderChannel o on t.OrderNo = o.OrderNo

IF OBJECT_ID('tempdb..#TransCounts') IS NOT NULL DROP TABLE #TransCounts
create table #TransCounts (Channel varchar(20), TranDate varchar(10), OrderCount int)
insert into #TransCounts
select Channel, TranDate, sum(OrderCount) from #Trans group by Channel, TranDate

IF OBJECT_ID('tempdb..#TransPivot') IS NOT NULL DROP TABLE #TransPivot
CREATE TABLE #TransPivot (Channel varchar(20), TranDate varchar(10), Category varchar(20), TranAmt money)
insert into #TransPivot
select Channel, TranDate, Category, TranAmt from #Trans

IF OBJECT_ID('tempdb..#Amounts') IS NOT NULL DROP TABLE #Amounts
create table #Amounts (Channel varchar(20), TranDate Date, RewardEarned money, RewardUsed money, Forfeited money, ReclaimedReturn money, 
UnReclaimedReturn money, ReclaimedCancel money, UnReclaimedCancel money, Cancelled money, Eliminate money, OffsetNegReward money)

insert into #Amounts
select * from #TransPivot
pivot (sum([TranAmt]) for Category in ([RewardEarned], [RewardUsed], [Forfeited], [ReclaimedReturn], 
[UnReclaimedReturn], [ReclaimedCancel], [UnReclaimedCancel], [Cancelled], [Eliminate], [OffsetNegReward])) as DistAmounts
order by Channel, TranDate

update #Amounts set RewardEarned = ISNULL(RewardEarned,0), RewardUsed = ISNULL(RewardUsed,0), 
Forfeited = ISNULL(Forfeited,0), ReclaimedReturn = ISNULL(ReclaimedReturn,0), 
UnReclaimedReturn = ISNULL(UnReclaimedReturn,0), ReclaimedCancel = ISNULL(ReclaimedCancel,0), 
UnReclaimedCancel = ISNULL(UnReclaimedCancel,0), Cancelled = ISNULL(Cancelled,0), 
Eliminate = ISNULL(Eliminate, 0), OffsetNegReward = ISNULL(OffsetNegReward, 0)

IF OBJECT_ID('tempdb..#OrderAmounts') IS NOT NULL DROP TABLE #OrderAmounts
create table #OrderAmounts (Channel varchar(20), TranDate Date, OrderSubtotal money, OrderDisc money, OrderShip money, OrderShipDisc money, OrderTax money, OrderTotal money)
insert into #OrderAmounts
select t.Channel, t.TranDate, Sum(h.ORH_SUBTOTAL_AMT), Sum(h.ORH_DISCOUNT_AMT), Sum(h.ORH_TOTAL_SHIP_AMT), Sum(h.ORH_SHIP_DISC_AMT), sum(h.ORH_TOTAL_TAX_AMT), sum(h.ORH_TOTAL_AMT)
from #Trans t inner join [ECOMM_ETL ].dbo.V_ORDER_HEADER h (nolock) on t.OrderNo = h.ORH_SOURCE_NBR where t.Category = 'RewardEarned'
group by Channel, TranDate order by Channel, TranDate

select a.*, c.OrderCount, o.OrderSubtotal, o.OrderDisc, o.OrderShip, o.OrderShipDisc, o.OrderTax, o.OrderTotal
from #Amounts a left join #TransCounts c on a.Channel = c.Channel and a.TranDate = c.TranDate
left join #OrderAmounts o on a.Channel = o.Channel and a.TranDate = o.TranDate where a.Channel = @Channel order by a.TranDate


