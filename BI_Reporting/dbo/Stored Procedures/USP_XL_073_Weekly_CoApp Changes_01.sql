﻿







/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/10/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CO_APPLICANT] 		     Select   
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  


=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/

CREATE  procedure [dbo].[USP_XL_073_Weekly_CoApp Changes_01] as 
set nocount on ;

---------------------------Pull All CoApp Changes in Last 7 Days--------------------------------------
Select 
	[CUSTOMER_NUMBER] as [ID]
	,B.[FIRST_NAME] as [Coach First Name]
	,B.[LAST_NAME] as [Coach Last Name]
	,convert(varchar(10),A.[LAST_CHANGED],121) as [Change Date]
	,A.[FIRST_NAME] as [CoApp First Name]
	,A.[LAST_NAME] as [CoApp Last Name]
	,case when [IS_OBSOLETE] = 1 then 'Removed'
		else 'Change' end as [Status]
	From [ODYSSEY_ETL].[dbo].[ODYSSEY_CO_APPLICANT] A
		Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] B
			on A.[CUSTOMER_ID] = B.[CUSTOMER_ID]
		Where A.[LAST_CHANGED] > = GETDATE () - 8




