﻿

CREATE PROCEDURE [dbo].[USP_XL_042_IntraDayInvoicing_v2] As

SET NOCOUNT ON;



--EXECUTE AS LOGIN = 'ReportViewer'
DECLARE @ly MONEY
SELECT @ly = SUM(Amount)--AS ly_Total$
FROM BI_SSAS_Cubes.dbo.FactSales
WHERE [Posting Date] =  DATEADD(yy,-1,DATEDIFF(d,0,GETDATE())) 
GROUP BY [Posting Date]


SELECT  'NAVPROD' AS [SERVER] ,
        [Customer Posting Group] ,
        b.[Posting Date] ,
        MONTH(b.[Posting Date]) CalMonth ,
        COUNT(DISTINCT ( [Document No_] )) AS OrderCounts ,
        SUM(Amount) Total$ ,
        COUNT(*) AS RowCounts
		,@ly AS [LY_Total$]
FROM    NAVISION_PROD.[NAVPROD].[dbo].[Jason Pharm$Sales Invoice Line] a
        JOIN NAVISION_PROD.[NAVPROD].[dbo].[Jason Pharm$Sales Invoice Header] b ON a.[Document No_] = b.[No_]
WHERE   b.[Posting Date] BETWEEN CONVERT(VARCHAR(10), GETDATE(), 121)
                         AND     CONVERT(VARCHAR(10), GETDATE() + 1, 121)
        AND [Customer Posting Group] <> 'CORPCLINIC'
GROUP BY [Customer Posting Group] ,
        b.[Posting Date] ,
        MONTH(b.[Posting Date]);






