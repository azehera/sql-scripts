﻿


/*
=======================================================================================
Author:         Daniel Dagnachew
Create date: Date,4/17/2015
-------------------------[usp_XL_032_WholesaleNewAccounts] -----------------------------
---Comparison of 2014 and 2015 Wholesale------------------------
=======================================================================================   
REFERENCES
Database              Table/View/UDF						  Action            
---------------------------------------------------------------------------------------
NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] 			      Select   
NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Header]			  Select
NAV_ETL.[dbo].[Jason Pharm$Customer] 			              Select   
BI_Reporting.[dbo].[calendar]			                      Select
=======================================================================================
REVISION LOG
Date           Name              Change
---------------------------------------------------------------------------------------
7/19/2015      Menkir Haile		DayOfWeekNumber and CalendarMonth columns are added
=======================================================================================
*/
CREATE PROCEDURE [dbo].[usp_XL_032_WholesaleNewAccounts] 

AS
SET NOCOUNT ON;

BEGIN


select Distinct
 e.CalendarMonthID
,e.CalendarYear
,e.CalendarMonthName
,e.CalendarMonth
,e.DayOfWeekName
,datepart(dw, CalendarDate) AS DayOfWeekNumber
,b.[Customer Posting Group]
,a.[Sell-to Customer No_]
,c.Name as CustomerName
,c.[Date Added]

From [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A With (nolock)
LEFT OUTER JOIN 
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B With (nolock)
      On A.[Document No_]= B.[No_]
LEFT OUTER JOIN       
     [NAV_ETL].[dbo].[Jason Pharm$Customer] C With (nolock)
      On  A.[Sell-to Customer No_]  COLLATE SQL_Latin1_General_CP1_CI_AS = c.[No_]
LEFT OUTER JOIN
     BI_Reporting.dbo.calendar e
     on c.[Date Added]= e.CalendarDate
  AND B.[Customer Posting Group] IN('DOCTORS')
  AND A.[Sell-to Customer No_] <>''
Where c.[Date Added] >= (SELECT DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0))
  AND B.[Customer Posting Group] IN('DOCTORS')
  AND A.[Sell-to Customer No_] <>''

Order By c.[Date Added]

END