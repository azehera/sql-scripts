﻿

--/*
--==================================================================================
--Author:         Emily Trainor
--Create date: 02/08/2012
---------------------------TSFL New Coach Activation Counts------------------------
--Create a count of Activated Health Coaches by Month for Start-up Coach Analysis--
--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--ODYSSEY_ETL			ODYSSEY_CUSTOMER					      Select                         
--==================================================================================
--REVISION LOG
--Date           Name                          Change
------------------------------------------------------------------------------------

--==================================================================================
--NOTES:
------------------------------------------------------------------------------------
--==================================================================================
--*/

CREATE procedure [dbo].[usp_TSFL_ActivatedCoachCount] as 
set nocount on ;


-------------------------------Capture Activated Coach count by Day-----------------------------------------
Select 
 Convert (Varchar (10),[EFFECTIVE_DATE],121)as [EFFECTIVE_DATE]
,Count([CUSTOMER_ID])as COACHES_ACTIVATED
INTO #a 
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]
Where [EFFECTIVE_DATE]>= (Select Convert (varchar(10), FirstDateOfYear,121) From [BI_Reporting].[dbo].[calendar_TSFL]WHERE CalendarYear = (Select Datepart(yyyy, getdate())-2) Group By FirstDateOfYear)
Group By [EFFECTIVE_DATE]
Order By [EFFECTIVE_DATE] asc

--------------Aggregation to Month and Year for Count of Coaches in each Activation Month Grouping-----------
Select 
  [CalendarMonth]
,[CalendarYear]
,sum([COACHES_ACTIVATED]) as COACHES_ACTIVATED
From #a A
inner join 
	[BI_Reporting].[dbo].[calendar_TSFL] B 
	on A.[EFFECTIVE_DATE]= B.[CalendarDate]
Group By 
 [CalendarYear]
,[CalendarMonth]
Order by 
 [CalendarYear]
,[CalendarMonth]

Drop table #a
