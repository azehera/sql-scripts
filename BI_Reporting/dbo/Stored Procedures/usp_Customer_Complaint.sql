﻿/*
============================================================================================================   
Author:         Daniel Dagnachew
Create date: 09/30/2014
---------------------------[usp_Customer_Complaint]-----------------------------------------------------
Returns a list of MED DIRECT customer details for marketing and CSC where customer has placed their first 
Medifast Order---------------------
============================================================================================================   
REFERENCES
Database                            Table/View/UDF													 Action            
-------------------------------------------------------------------------------------------------------------
 NAV_ETL								dbo.Jason Pharm$Sales Invoice Line							Select					
 WMS_Shipment_ETL						dbo.TDC_ord													Select
 WMS_Shipment_ETL						dbo.TDC_shipment_line										Select
 WMS_Shipment_ETL						dbo.TDC_invdtl												Select
 WMS_Shipment_ETL						dbo.MDC_shipment_line										Select
 WMS_Shipment_ETL						dbo.MDC_invdtl												Select                                
=============================================================================================================
*/
CREATE procedure [dbo].[usp_Customer_Complaint]
as
begin

Select distinct j.[Entry No_]
      ,j.[Contact No_]
      ,j.[Date] 
	  ,case when j.[Complaint Date] = '1753-01-01 00:00:00.000' then Cast((null) as datetime) else j.[Complaint Date] end as [Complaint Date]
      ,j.[Description]
      ,j.[User ID]
      ,j.[Interaction Template Code] AS [Reason Code]
      ,j.[Salesperson Code]
      ,j.[Order No_]
      ,case when j.[Contact Via] in ('*') then 'unknown' else j.[Contact Via] end as [Lot #/Expiration]
      ,j.[Item No_]as [Item No.]
      ,o.[cponum] as [WMS Document No.]
      ,case when I.[lotnum] in ('----') then 'unknown' else I.[lotnum] end as [Lot #]
  from NAV_ETL.[dbo].[Jason Pharm$Interaction Log Entry] j
  inner join [WMS_Shipment_ETL].[dbo].[TDC_ord] o
  on j.[Order No_]COLLATE DATABASE_DEFAULT= cast(o.[cponum] as varchar(20)) 
  inner JOIN [WMS_Shipment_ETL].dbo.TDC_shipment_line sl
  ON sl.ordnum = o.ordnum
  inner JOIN [WMS_Shipment_ETL].[dbo].TDC_invdtl I
  ON I.ship_line_id = sl.ship_line_id
  and I.prtnum COLLATE DATABASE_DEFAULT = j.[Item No_] 
  and I.[lotnum] is not null

  
         union all
       
       
Select distinct j.[Entry No_]
      ,j.[Contact No_]
      ,j.[Date]
	  ,case when j.[Complaint Date] = '1753-01-01 00:00:00.000' then cast((null) as datetime)  else j.[Complaint Date] end as [Complaint Date]
      ,j.[Description]
      ,j.[User ID]
      ,j.[Interaction Template Code] AS [Reason Code]
      ,j.[Salesperson Code]
      ,j.[Order No_]
      ,case when j.[Contact Via] in ('*') then 'unknown' else j.[Contact Via] end as [Lot #/Expiration]
      ,j.[Item No_]as [Item No.]
      ,o.[cponum] as [WMS Document No.]
      ,case when I.[lotnum] in ('----') then 'unknown' else I.[lotnum] end as [Lot #]
  from NAV_ETL.[dbo].[Jason Pharm$Interaction Log Entry] j
  inner join [WMS_Shipment_ETL].[dbo].[MDC_ord] o
  on j.[Order No_]COLLATE DATABASE_DEFAULT= cast(o.[cponum] as varchar(20)) 
  inner JOIN [WMS_Shipment_ETL].dbo.MDC_shipment_line sl
  ON sl.ordnum = o.ordnum
  inner JOIN [WMS_Shipment_ETL].[dbo].MDC_invdtl I
  ON I.ship_line_id = sl.ship_line_id
  and I.prtnum COLLATE DATABASE_DEFAULT = j.[Item No_]
  and I.[lotnum] is not null

  
  order by [Entry No_]
end