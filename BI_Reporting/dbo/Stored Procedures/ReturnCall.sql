﻿


CREATE procedure [dbo].[ReturnCall]

@ItemNo varchar(15),
 @LotNo Varchar(15)
as 


IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select [Item No_],[Document No_],[Lot No_],[Source No_] into #A from [NAV_ETL].[dbo].[Jason Pharm$Item Ledger Entry]
where [Item No_] =@ItemNo 
and [Lot No_] =@LotNo
and [Entry Type]=5
and [Source No_] in (Select [No_] COLLATE DATABASE_DEFAULT from [NAV_ETL].dbo.[Jason Pharm$Item] where [Inventory Posting Group]='FIN')

IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
Select [Item No_],[Document No_],[Lot No_],[Source No_] into #B from [NAV_ETL].[dbo].[Jason Pharm$Item Ledger Entry]
where [Item No_] =@ItemNo 
and [Lot No_] =@LotNo
and [Entry Type]=5
and [Source No_] not in (Select [No_] COLLATE DATABASE_DEFAULT from [NAV_ETL].dbo.[Jason Pharm$Item] where [Inventory Posting Group]='FIN')




IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C
Select [Item No_],[Document No_],[Lot No_],[Source No_] into #C  from [NAV_ETL].[dbo].[Jason Pharm$Item Ledger Entry]
where [Entry Type]=6
and [Document No_] in (Select [Document No_] from #B)



IF object_id('Tempdb..#D') Is Not Null DROP TABLE #D
Select * into #D from #A 
Insert into #D 
select [Item No_],[Document No_],[Lot No_],[Source No_] from [NAV_ETL].[dbo].[Jason Pharm$Item Ledger Entry]
where [Item No_] in (Select [Source No_] from #C)
and [Document No_] in (Select [Lot No_] from #C)
and [Entry Type]=5
and [Source No_] in (Select [No_] COLLATE DATABASE_DEFAULT from [NAV_ETL].dbo.[Jason Pharm$Item] where [Inventory Posting Group]='FIN')


--Select * from #C
IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E


select [Item No_],[Document No_],[Lot No_],[Source No_] into #E from [NAV_ETL].[dbo].[Jason Pharm$Item Ledger Entry]
where [Item No_] in (Select [Item No_] from #C)
and [Lot No_] in (Select [Lot No_] from #C)
and [Entry Type]=5
and [Source No_] not in (Select [No_] COLLATE DATABASE_DEFAULT from [NAV_ETL].dbo.[Jason Pharm$Item] where [Inventory Posting Group]='FIN')

IF object_id('Tempdb..#F') Is Not Null DROP TABLE #F
Select [Item No_],[Document No_],[Lot No_],[Source No_] into #F  from [NAV_ETL].[dbo].[Jason Pharm$Item Ledger Entry]
where [Item No_] in (Select [Item No_] from #E)
and [Entry Type]=5
and [Lot No_] in (Select [Lot No_] from #E)



Insert into #D 
select [Item No_],[Document No_],[Lot No_],[Source No_] from [NAV_ETL].[dbo].[Jason Pharm$Item Ledger Entry]
where [Item No_] in (Select [Item No_] from #C)
and [Lot No_] in (Select [Lot No_] from #C)
and [Entry Type]=5
and [Source No_]  in (Select [No_] COLLATE DATABASE_DEFAULT from [NAV_ETL].dbo.[Jason Pharm$Item] where [Inventory Posting Group]='FIN')

Insert into #D
select [Item No_],[Document No_],[Document No_],[Source No_] from #D
where [Document No_]<>[Lot No_]

select MDC_invdtl.prtnum as "item number",
                MDC_invdtl.lotnum as "lot number",
                MDC_invdtl.ship_line_id as "WMS shipment line id",
                MDC_shipment_line.ship_id as "WMS ship id",
                MDC_shipment_line.ordnum as "NAV order number",
                MDC_shipment_line.wh_id as "warehouse id",
                MDC_shipment.shpsts as "shipment status",
                MDC_ord.ordtyp as "order type",
                MDC_ord.adddte as "order creation date",
                MDC_shipment.loddte as "order shipment date",
                MDC_ord.btcust as "bill-to customer no",
                MDC_adrmst.adrnam as "bill-to customer name",
                sum(MDC_invdtl.untqty) as "quantity shipped"
from WMS_Shipment_ETL.dbo.MDC_invdtl
join WMS_Shipment_ETL.dbo.MDC_shipment_line
                on MDC_invdtl.ship_line_id = MDC_shipment_line.ship_line_id
join WMS_Shipment_ETL.dbo.MDC_shipment
                on MDC_shipment_line.ship_id = MDC_shipment.ship_id
join WMS_Shipment_ETL.dbo.MDC_ord
                on MDC_shipment_line.ordnum = MDC_ord.ordnum
join WMS_Shipment_ETL.dbo.MDC_adrmst
                on MDC_ord.bt_adr_id = MDC_adrmst.adr_id
where MDC_invdtl.lotnum  in (Select [Lot No_] from #D) 
group by MDC_invdtl.prtnum,
                MDC_invdtl.lotnum,
                MDC_invdtl.ship_line_id,
                MDC_shipment_line.ship_id,
                MDC_shipment_line.ordnum,
                MDC_shipment_line.wh_id,
                MDC_shipment.shpsts,
                MDC_ord.ordtyp,
                MDC_ord.adddte,
                MDC_shipment.loddte,
                MDC_ord.btcust,
                MDC_adrmst.adrnam

Union all

select TDC_invdtl.prtnum as "item number",
                TDC_invdtl.lotnum as "lot number",
                TDC_invdtl.ship_line_id as "WMS shipment line id",
                TDC_shipment_line.ship_id as "WMS ship id",
                TDC_shipment_line.ordnum as "NAV order number",
                TDC_shipment_line.wh_id as "warehouse id",
                TDC_shipment.shpsts as "shipment status",
                TDC_ord.ordtyp as "order type",
                TDC_ord.adddte as "order creation date",
                TDC_shipment.loddte as "order shipment date",
                TDC_ord.btcust as "bill-to customer no",
                TDC_adrmst.adrnam as "bill-to customer name",
                sum(TDC_invdtl.untqty) as "quantity shipped"
from WMS_Shipment_ETL.dbo.TDC_invdtl
join WMS_Shipment_ETL.dbo.TDC_shipment_line
                on TDC_invdtl.ship_line_id = TDC_shipment_line.ship_line_id
join WMS_Shipment_ETL.dbo.TDC_shipment
                on TDC_shipment_line.ship_id = TDC_shipment.ship_id
join WMS_Shipment_ETL.dbo.TDC_ord
                on TDC_shipment_line.ordnum = TDC_ord.ordnum
join WMS_Shipment_ETL.dbo.TDC_adrmst
                on TDC_ord.bt_adr_id = TDC_adrmst.adr_id
where TDC_invdtl.lotnum in (Select [Lot No_] from #D) 
group by TDC_invdtl.prtnum,
                TDC_invdtl.lotnum,
                TDC_invdtl.ship_line_id,
                TDC_shipment_line.ship_id,
                TDC_shipment_line.ordnum,
                TDC_shipment_line.wh_id,
                TDC_shipment.shpsts,
                TDC_ord.ordtyp,
                TDC_ord.adddte,
                TDC_shipment.loddte,
                TDC_ord.btcust,
                TDC_adrmst.adrnam

Union all

select ARCH_TDC_invdtl.prtnum as "item number",
                ARCH_TDC_invdtl.lotnum as "lot number",
                ARCH_TDC_invdtl.ship_line_id as "WMS shipment line id",
                ARCH_TDC_shipment_line.ship_id as "WMS ship id",
                ARCH_TDC_shipment_line.ordnum as "NAV order number",
                ARCH_TDC_shipment_line.wh_id as "warehouse id",
                ARCH_TDC_shipment.shpsts as "shipment status",
                ARCH_TDC_ord.ordtyp as "order type",
                ARCH_TDC_ord.adddte as "order creation date",
                ARCH_TDC_shipment.loddte as "order shipment date",
                ARCH_TDC_ord.btcust as "bill-to customer no",
                ARCH_TDC_adrmst.adrnam as "bill-to customer name",
                sum(ARCH_TDC_invdtl.untqty) as "quantity shipped"
from WMS_Shipment_ETL.dbo.ARCH_TDC_invdtl
join WMS_Shipment_ETL.dbo.ARCH_TDC_shipment_line
                on ARCH_TDC_invdtl.ship_line_id = ARCH_TDC_shipment_line.ship_line_id
join WMS_Shipment_ETL.dbo.ARCH_TDC_shipment
                on ARCH_TDC_shipment_line.ship_id = ARCH_TDC_shipment.ship_id
join WMS_Shipment_ETL.dbo.ARCH_TDC_ord
                on ARCH_TDC_shipment_line.ordnum = ARCH_TDC_ord.ordnum
join WMS_Shipment_ETL.dbo.ARCH_TDC_adrmst
                on ARCH_TDC_ord.bt_adr_id = ARCH_TDC_adrmst.adr_id
where ARCH_TDC_invdtl.lotnum in (Select [Lot No_] from #D) 
group by ARCH_TDC_invdtl.prtnum,
                ARCH_TDC_invdtl.lotnum,
                ARCH_TDC_invdtl.ship_line_id,
                ARCH_TDC_shipment_line.ship_id,
                ARCH_TDC_shipment_line.ordnum,
                ARCH_TDC_shipment_line.wh_id,
                ARCH_TDC_shipment.shpsts,
                ARCH_TDC_ord.ordtyp,
                ARCH_TDC_ord.adddte,
                ARCH_TDC_shipment.loddte,
                ARCH_TDC_ord.btcust,
                ARCH_TDC_adrmst.adrnam               

Union all

select ARCH_MDC_invdtl.prtnum as "item number",
                ARCH_MDC_invdtl.lotnum as "lot number",
                ARCH_MDC_invdtl.ship_line_id as "WMS shipment line id",
                ARCH_MDC_shipment_line.ship_id as "WMS ship id",
                ARCH_MDC_shipment_line.ordnum as "NAV order number",
                ARCH_MDC_shipment_line.wh_id as "warehouse id",
                ARCH_MDC_shipment.shpsts as "shipment status",
                ARCH_MDC_ord.ordtyp as "order type",
                ARCH_MDC_ord.adddte as "order creation date",
                ARCH_MDC_shipment.loddte as "order shipment date",
                ARCH_MDC_ord.btcust as "bill-to customer no",
                ARCH_MDC_adrmst.adrnam as "bill-to customer name",
                sum(ARCH_MDC_invdtl.untqty) as "quantity shipped"
from WMS_Shipment_ETL.dbo.ARCH_MDC_invdtl
join WMS_Shipment_ETL.dbo.ARCH_MDC_shipment_line
                on ARCH_MDC_invdtl.ship_line_id = ARCH_MDC_shipment_line.ship_line_id
join WMS_Shipment_ETL.dbo.ARCH_MDC_shipment
                on ARCH_MDC_shipment_line.ship_id = ARCH_MDC_shipment.ship_id
join WMS_Shipment_ETL.dbo.ARCH_MDC_ord
                on ARCH_MDC_shipment_line.ordnum = ARCH_MDC_ord.ordnum
join WMS_Shipment_ETL.dbo.ARCH_MDC_adrmst
                on ARCH_MDC_ord.bt_adr_id = ARCH_MDC_adrmst.adr_id
where ARCH_MDC_invdtl.lotnum in (Select [Lot No_] from #D) 
group by ARCH_MDC_invdtl.prtnum,
                ARCH_MDC_invdtl.lotnum,
                ARCH_MDC_invdtl.ship_line_id,
                ARCH_MDC_shipment_line.ship_id,
                ARCH_MDC_shipment_line.ordnum,
                ARCH_MDC_shipment_line.wh_id,
                ARCH_MDC_shipment.shpsts,
                ARCH_MDC_ord.ordtyp,
                ARCH_MDC_ord.adddte,
                ARCH_MDC_shipment.loddte,
                ARCH_MDC_ord.btcust,
                ARCH_MDC_adrmst.adrnam  


