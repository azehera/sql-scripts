﻿
CREATE procedure [dbo].[UPS_RS_019_Pricing_Calculator]
as

DECLARE @StartMonthID int, @EndMonthID int,@StartDate datetime,@EndDate datetime
SET @StartMonthID=(Select CalendarMonthID-101 from BI_Reporting.dbo.calendar where CalendarDate=Convert(Varchar(10),Getdate()-1,121))
SET @EndMonthID= (Select CalendarMonthID-1 from BI_Reporting.dbo.calendar where CalendarDate=Convert(Varchar(10),Getdate()-1,121))
SET @StartDate=(Select FirstDateOfMonth from BI_Reporting.dbo.calendar where CalendarDate=dateadd(yyyy,-1, Convert(varchar(10),getdate()-1,121)) )
SET @EndDate=(Select LastDateOfMonth from BI_Reporting.dbo.calendar where CalendarDate=dateadd(mm,-1, Convert(varchar(10),getdate()-1,121)) )
 

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A


SELECT  
case when [Global Dimension 2 Code]='TSFL' then 'TSFL'
            when [Global Dimension 2 Code]='WholeSale' then 'DOCTORS'
            when [Global Dimension 2 Code]='INT' then 'INTL'
            when [Global Dimension 2 Code]='MWCC' then 'MWCC'
            when [Global Dimension 2 Code]='FRANCHISE'then 'FRANCHISE' 
            when [Global Dimension 2 Code]='DR MKTG' then 'MEDIFAST' end AS CustomerPostingGroup
       ,Sum([NetTotal]) as expenses
into #A
FROM [BI_SSAS_Cubes].dbo.FactGL_cube a
  join [BI_SSAS_Cubes].dbo.DimGLCategory b
  on a.[G_L Account No_]=b.[G_L Account No_]
  where CalendarMonthID>=@StartMonthID and CalendarMonthID<= @EndMonthID
  and RollUpName<>'Total Income'
  and [NetTotal] is not null
  and [Global Dimension 2 Code] in ('TSFL','WholeSale','INT','MWCC','FRANCHISE','DR MKTG')
  and [Source Code]<>'CLSINCOME'
  group by case when [Global Dimension 2 Code]='TSFL' then 'TSFL'
            when [Global Dimension 2 Code]='WholeSale' then 'DOCTORS'
            when [Global Dimension 2 Code]='INT' then 'INTL'
            when [Global Dimension 2 Code]='MWCC' then 'MWCC'
            when [Global Dimension 2 Code]='FRANCHISE'then 'FRANCHISE' 
            when [Global Dimension 2 Code]='DR MKTG' then 'MEDIFAST' end
   order by case when [Global Dimension 2 Code]='TSFL' then 'TSFL'
            when [Global Dimension 2 Code]='WholeSale' then 'DOCTORS'
            when [Global Dimension 2 Code]='INT' then 'INTL'
            when [Global Dimension 2 Code]='MWCC' then 'MWCC'
            when [Global Dimension 2 Code]='FRANCHISE'then 'FRANCHISE' 
            when [Global Dimension 2 Code]='DR MKTG' then 'MEDIFAST' end

IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B

select CustomerPostingGroup, SUM(Amount) as Total  into #B from BI_SSAS_Cubes.dbo.FactSales
 where [Posting Date] between @StartDate and @EndDate
 group by CustomerPostingGroup


/****** Script for SelectTopNRows command from SSMS  ******/
Truncate table BI_Reporting.dbo.RS_019_Pricing_Calculator

Insert into  BI_Reporting.dbo.RS_019_Pricing_Calculator

SELECT  [CustomerPostingGroup]
      ,[Product Group Code]
      ,[ItemCategoryCode]
      ,a.[ItemCode]
      ,Description
      ,a.[ItemCode]+' '+Description as CodeWithDescription
      ,null  as [Expenses],
      null as [CustomerPostingGroupTotal]
      ,SUm([Units]) as TotalUnits
      ,Sum([Amount]) as TotalAmount
      ,case when SUm([Units])=0 then 0 else Sum([Amount])/SUm([Units])end as ASP
        FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
                join [BI_SSAS_Cubes].dbo.DimItemCode b
                     on a.Itemcode=b.Itemcode
          where [Posting Date] between @StartDate and @Enddate
                Group by [CustomerPostingGroup]
                        ,[Product Group Code]
                        ,[ItemCategoryCode]
                        ,a.[ItemCode]
                        ,Description
                order by CustomerPostingGroup
                
                
 update a
 set a.Expenses= b.expenses
 from BI_Reporting.dbo.RS_019_Pricing_Calculator a
 join #A b
 on a.CustomerPostingGroup=b.CustomerPostingGroup
                
                
 update a
 set a.CustomerPostingGroupTotal= b.Total
 from BI_Reporting.dbo.RS_019_Pricing_Calculator a
 join #B b
      on a.CustomerPostingGroup=b.CustomerPostingGroup



