﻿

CREATE PROCEDURE [dbo].[USP_XL_044_Vendor_1099_Detail] as 

SET NOCOUNT ON;
/*
==================================================================================
Author:         Luc Emond
Create date: 12/09/2013
-----------------------------[USP_XL_042_Vendor_1099_Detail]-------------------
----Provide 1099 info to Accounting-----
==================================================================================   
REFERENCES
Database              Table/View/UDF                                  Action            
------------------------------------------------------------------------------------
[NAV_ETL]             [7 Crondall$Vendor Ledger Entry]                Select 
[NAV_ETL]             [7 Crondall$Vendor]                             Select 
[NAV_ETL]             [7 Crondall$IRS 1099 Form-Box]                  Select
[NAV_ETL]             [Jason Enterprises$Vendor Ledger Entry]         Select 
[NAV_ETL]             [Jason Enterprises$Vendor]                      Select 
[NAV_ETL]             [Jason Enterprises$IRS 1099 Form-Box]           Select
[NAV_ETL]             [Jason Properties$Vendor Ledger Entry]          Select 
[NAV_ETL]             [Jason Properties$Vendor]                       Select 
[NAV_ETL]             [Jason Properties$IRS 1099 Form-Box]            Select
[NAV_ETL]             [Medifast Inc$Vendor Ledger Entry]              Select 
[NAV_ETL]             [Medifast Inc$Vendor]                           Select 
[NAV_ETL]             [Medifast Inc$IRS 1099 Form-Box]                Select
[NAV_ETL]             [Jason Pharm$Vendor Ledger Entry]               Select 
[NAV_ETL]             [Jason Pharm$Vendor]                            Select 
[NAV_ETL]             [Jason Pharm$IRS 1099 Form-Box]                 Select
[NAV_ETL]             [Take Shape For Life$Vendor Ledger Entry]       Select 
[NAV_ETL]             [Take Shape For Life$Vendor]                    Select 
[NAV_ETL]             [Take Shape For Life$IRS 1099 Form-Box]         Select
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------
4/10/2015		D. Smith				Change the reefernces to NAVPROD to NAV_ETL
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/

--EXECUTE AS LOGIN = 'ReportViewer'

DECLARE @FirstDate DateTime, @LastDate DateTime
Set @FirstDate = '2013-01-01'
Set @LastDate = '2013-12-31'
 
DECLARE @TblWorkTable TABLE (
	[Entity] [varchar](25) NULL,
	[Vendor No_] [varchar](20) NULL,
	[VendorName] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[Address 2] [varchar](50) NULL,
	[City] [varchar](30) NULL,
	[County] [varchar](30) NULL,
	[Post Code] [varchar](20) NULL,
	[Federal ID No_] [varchar](30) NULL,
	[Document Date] [varchar](10) NULL,
	[Document Type] [int] NULL,
	[External Document No_] [varchar] (20) NULL,
	[1099 Code][varchar] (10) NULL,
	[1099 Code Description]  [varchar](50) NULL,
	[1099 Amount] [decimal](38, 2) NULL,
    [Closed at Date] [varchar](10) NULL
    )
    
-----7 CRONDALL-------		
Insert Into  @TblWorkTable
Select 
'7 Crondall' AS Entity
,[Vendor No_]
,[Name] as VendorName 
,[Address]
,[Address 2]
,[City]
,[County]  
,[Post Code] 
,[Federal ID No_]
,Convert(Varchar(10),[Document Date],121) as [Document Date]
,[Document Type]
,[External Document No_]
,a.[1099 Code]
,c.[Description] as [1099 Code Description]
,a.[1099 Amount]
,Convert (Varchar(10),[Closed at Date],121) as [Closed at Date]
From [NAV_ETL].[dbo].[7 Crondall$Vendor Ledger Entry] a
Join [NAV_ETL].[dbo].[7 Crondall$Vendor] b
  on a.[Vendor No_] = b.No_
join [NAV_ETL].[dbo].[7 Crondall$IRS 1099 Form-Box] c
  on a.[1099 Code] = c.Code
Where [Posting Date]Between @FirstDate and @LastDate
  And a.[1099 Code]<>'' 
  And [Document Type] ='2'

-----Jason Enterprises-------
Insert Into  @TblWorkTable
SELECT 
'Jason Enterprises' AS Entity
,[Vendor No_]
,[Name] as VendorName 
,[Address]
,[Address 2]
,[City]
,[County]  
,[Post Code] 
,[Federal ID No_]
,Convert(Varchar(10),[Document Date],121) as [Document Date]
,[Document Type]
,[External Document No_]
,a.[1099 Code]
,c.[Description] as [1099 Code Description]
,a.[1099 Amount]
,Convert (Varchar(10),[Closed at Date],121) as [Closed at Date]
From [NAV_ETL].[dbo].[Jason Enterprises$Vendor Ledger Entry] a
Join [NAV_ETL].[dbo].[Jason Enterprises$Vendor] b
  on a.[Vendor No_] = b.No_
join [NAV_ETL].[dbo].[Jason Enterprises$IRS 1099 Form-Box] c
  on a.[1099 Code] = c.Code
Where [Posting Date]Between @FirstDate and @LastDate
  And a.[1099 Code]<>'' 
  And [Document Type] ='2'

-----Jason Properties-------
Insert Into  @TblWorkTable
SELECT 
'Jason Properties' AS Entity
,[Vendor No_]
,[Name] as VendorName 
,[Address]
,[Address 2]
,[City]
,[County]  
,[Post Code] 
,[Federal ID No_]
,Convert(Varchar(10),[Document Date],121) as [Document Date]
,[Document Type]
,[External Document No_]
,a.[1099 Code]
,c.[Description] as [1099 Code Description]
,a.[1099 Amount]
,Convert (Varchar(10),[Closed at Date],121) as [Closed at Date]
From [NAV_ETL].[dbo].[Jason Properties$Vendor Ledger Entry] a
Join [NAV_ETL].[dbo].[Jason Properties$Vendor] b
  on a.[Vendor No_] COLLATE DATABASE_DEFAULT = b.No_
Join [NAV_ETL].[dbo].[Jason Properties$IRS 1099 Form-Box] c
  on a.[1099 Code] = c.Code
Where [Posting Date]Between @FirstDate and @LastDate
  And a.[1099 Code]<>'' 
  And [Document Type] ='2'

-----Medifast Inc-------
Insert Into  @TblWorkTable
SELECT 
'Medifast Inc' AS Entity
,[Vendor No_]
,[Name] as VendorName 
,[Address]
,[Address 2]
,[City]
,[County]  
,[Post Code] 
,[Federal ID No_]
,Convert(Varchar(10),[Document Date],121) as [Document Date]
,[Document Type]
,[External Document No_]
,a.[1099 Code]
,c.[Description] as [1099 Code Description]
,a.[1099 Amount]
,Convert (Varchar(10),[Closed at Date],121) as [Closed at Date]
From [NAV_ETL].[dbo].[Medifast Inc$Vendor Ledger Entry] a
Join [NAV_ETL].[dbo].[Medifast Inc$Vendor] b
  on a.[Vendor No_] = b.No_
Join [NAV_ETL].[dbo].[Medifast Inc$IRS 1099 Form-Box] c
  on a.[1099 Code] = c.Code
Where [Posting Date]Between @FirstDate and @LastDate
  And a.[1099 Code]<>'' 
  And [Document Type] ='2'

-----TSFL-------
Insert Into  @TblWorkTable
SELECT 
'TSFL' AS Entity
,[Vendor No_]
,[Name] as VendorName 
,[Address]
,[Address 2]
,[City]
,[County]  
,[Post Code] 
,[Federal ID No_]
,Convert(Varchar(10),[Document Date],121) as [Document Date]
,[Document Type]
,[External Document No_]
,a.[1099 Code]
,c.[Description] as [1099 Code Description]
,a.[1099 Amount]
,Convert (Varchar(10),[Closed at Date],121) as [Closed at Date]
From [NAV_ETL].[dbo].[Take Shape For Life$Vendor Ledger Entry] a
Join [NAV_ETL].[dbo].[Take Shape For Life$Vendor] b
  on a.[Vendor No_] = b.No_
Join [NAV_ETL].[dbo].[Take Shape For Life$IRS 1099 Form-Box] c
  on a.[1099 Code] = c.Code
Where [Posting Date]Between @FirstDate and @LastDate
  And a.[1099 Code]<>'' 
  And [Document Type] ='2'

-----Jason Pharm-------
Insert Into  @TblWorkTable
SELECT 
'Jason Pharm' AS Entity
,[Vendor No_]
,[Name] as VendorName 
,[Address]
,[Address 2]
,[City]
,[County]  
,[Post Code] 
,[Federal ID No_]
,Convert(Varchar(10),[Document Date],121) as [Document Date]
,[Document Type]
,[External Document No_]
,a.[1099 Code]
,c.[Description] as [1099 Code Description]
,a.[1099 Amount]
,Convert (Varchar(10),[Closed at Date],121) as [Closed at Date]
From [NAV_ETL].[dbo].[Jason Pharm$Vendor Ledger Entry] a
Join [NAV_ETL].[dbo].[Jason Pharm$Vendor] b
  on a.[Vendor No_] = b.No_
Join [NAV_ETL].[dbo].[Jason Pharm$IRS 1099 Form-Box] c
  on a.[1099 Code] = c.Code
Where [Posting Date]Between @FirstDate and @LastDate
  And a.[1099 Code]<>'' 
  And [Document Type] ='2'   
  
----FINAL DATA FOR REPORT-----
Select
 Entity
,VendorName 
,[Address]
,[Address 2]
,[City]
,[County]  
,[Post Code] 
,[Federal ID No_]
,[Document Date]
,[Document Type]
,[External Document No_]
,[1099 Code]
,[1099 Code Description]
,[1099 Amount]*-1 as [1099 Amount]
,[Closed at Date]
From @TblWorkTable a
Order By VendorName, [Document Date]


