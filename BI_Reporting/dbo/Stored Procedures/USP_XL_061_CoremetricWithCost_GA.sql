﻿
/*
=============================================================================================
Author:         Luc Emond
Create date: 03/07/2014
-------------------------[USP_XL_061_CoremetricWithCost]-----------------------------
Daily Coremetric info with cost
=============================================================================================   
REFERENCES
Database                          Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[BI_Reporting]                     dbo.Calendar                             Select 
[WebAnalytics_MEDD]                [dbo].[Order]                             Select
[WebAnalytics_MEDD]                [dbo].[CartItemPurchase]                  Select
[WebAnalytics_MEDD]                [dbo].[SessionFirstPageView]              Select
[NAV_ETL]                         [dbo].[Jason Pharm$Sales Price]           Select
[NAV_ETL]                         [dbo].[Jason Pharm$Item]                  Select
[ECOMM_ETL ]           [dbo].[ORDER_HEADER]                      Select   
[ECOMM_ETL ]           [dbo].[ORDER_LINE]                        Select
[prdmartini_MAIN_repl]            [dbo].[SKU]                               Select
[prdmartini_MAIN_repl]            [dbo].[PRODUCT]                           Select
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------
09-07-2016                  -Micah W.
							-Changed DDF connections to WebAnalytics_MEDD
==========================================================================================
*/
CREATE procedure [dbo].[USP_XL_061_CoremetricWithCost_GA] as

set nocount on;

-----DECLARE AND SET DATE FOR REPORT-----
DECLARE @Date DateTime

Set @Date =(Select FirstDateOfYear from dbo.calendar where CalendarDate=convert(varchar(10),Getdate()-1,121))

------CREATE MASTER TABLE-----
IF object_id('tempdb..#MASTER') is not null DROP TABLE #MASTER
CREATE TABLE #MASTER (
[CalDate] DateTime NUll,
[ConversionRate]  Numeric(15,4) NULL,
[Visitors]  Numeric(15,0) NULL,
[TotalSales] Numeric(15,2) NULL, 
[TotalOrders]  Numeric(15,0) NULL,
[TotalAov] Numeric(15,0) NULL,
[TotalFreeShipping]  Numeric(15,2) DEFAULT '0',
[TotalFreeGood]  Numeric(15,2) DEFAULT '0',
[TotalDiscounts]  Numeric(15,2) DEFAULT '0',
[TotalListPriceDeviation] Numeric(15,2) DEFAULT '0',
[Total] Numeric(15,2) NULL,
[TotalMarketingPercent] Decimal(6,4) NULL,
[SalesOnDemand] Numeric(15,2) NULL,
[OrdersOnDemand]  Numeric(15,0) NULL,
[AovOnDemand] Numeric(15,0) NULL,
[FreeShippingOnDemand]  Numeric(15,2) DEFAULT '0',
[FreeGoodOnDemand]  Numeric(15,2) DEFAULT '0',
[DiscountsOnDemand]  Numeric(15,2) DEFAULT '0',
[ListPriceDeviationOnDemand] Numeric(15,2) DEFAULT '0',
[OnDemandTotal] Numeric(15,2) NULL,
[MarketingPercentOnDemand] Decimal(6,4) NULL,

[SalesAutoShip] Numeric(15,2) NULL,
[OrdersAutoship]  Numeric(15,0) NULL,
[AovAutoship]  Numeric(15,2) NULL,
[FreeShippingAutoShip]  Numeric(15,2) DEFAULT '0',
[FreeGoodAutoShip]  Numeric(15,2) DEFAULT '0',
[DiscountsAutoShip]  Numeric(15,2) DEFAULT '0',
[ListPriceDeviationAutoShip] Numeric(15,2) DEFAULT '0',
[AutoShipTotal]  Numeric(15,2) NULL,
[MarketingPercentAutoship] Decimal(6,4) NULL,
[GrandTotal] Numeric(15,2) NULL,
[MarketingPercent] Decimal(6,4) NULL,
[Session]  Numeric(15,0) NULL,
[<=150SalesAutoShip] Numeric(15,2) NULL,
[<=150OrdersAutoship]  Numeric(15,0) NULL,
[<=150AovAutoship]  Numeric(15,2) NULL,
[<=150FreeShippingAutoShip]  Numeric(15,2) DEFAULT '0',
[<=150FreeGoodAutoShip]  Numeric(15,2) DEFAULT '0',
[<=150DiscountsAutoShip]  Numeric(15,2) DEFAULT '0',
[<=150ListPriceDeviationAutoShip] Numeric(15,2) DEFAULT '0',
[<=150AutoShipTotal]  Numeric(15,2) NULL,
[<=150MarketingPercentAutoship] Decimal(18,2) NULL,
[150-250SalesAutoShip] Numeric(15,2) NULL,
[150-250OrdersAutoship]  Numeric(15,0) NULL,
[150-250AovAutoship]  Numeric(15,2) NULL,
[150-250FreeShippingAutoShip]  Numeric(15,2) DEFAULT '0',
[150-250FreeGoodAutoShip]  Numeric(15,2) DEFAULT '0',
[150-250DiscountsAutoShip]  Numeric(15,2) DEFAULT '0',
[150-250ListPriceDeviationAutoShip] Numeric(15,2) DEFAULT '0',
[150-250AutoShipTotal]  Numeric(15,2) NULL,
[150-250MarketingPercentAutoship] Decimal(6,5) NULL,
[>250SalesAutoShip] Numeric(15,2) NULL,
[>250OrdersAutoship]  Numeric(15,0) NULL,
[>250AovAutoship]  Numeric(15,2) NULL,
[>250FreeShippingAutoShip]  Numeric(15,2) DEFAULT '0',
[>250FreeGoodAutoShip]  Numeric(15,2) DEFAULT '0',
[>250DiscountsAutoShip]  Numeric(15,2) DEFAULT '0',
[>250ListPriceDeviationAutoShip] Numeric(15,2) DEFAULT '0',
[>250AutoShipTotal]  Numeric(15,2) NULL,
[>250MarketingPercentAutoship] Decimal(6,4) NULL,
)

--------CREATE SALES TEMP TABLE------------
IF object_id('tempdb..#Sales') is not null DROP TABLE #Sales
SELECT [visitID] AS [SESSION_ID]
      ,[COOKIE_ID]
      ,Convert(Varchar(10),[TIMESTAMP],121) CalDate
      ,[ORDER_ID]
      ,[REGISTRATION_ID]
      ,convert(money,[ORDER_TOTAL]) as [ORDER_TOTAL]
      ,convert(money,[SHIPPING]) as [SHIPPING]
      --,[SITE_ID]
      ,customer_type AS [Customer Type]
      ,order_type AS [Order Type]
      --,[Pending TSFL Client]
      ,Accomodations_Total AS [Accomodations Total]
      ,convert(money,Discount_Total) as [Discount Total]
      ,convert(money,Tax) as [Tax Total]
      ,convert(money,order_total) as [Order Total]
      ,Payment_Type AS [Payment Type]
      --,[ORDER_ATTRIBUTE_9]
      ,Promo_Code AS [Promo Code]
      --,[SSIStimeStamp]
      --,[SessionDate]
      ,Case when (convert(money,order_total)-convert(money,Tax))=convert(money,[ORDER_TOTAL]) Then convert(money,[SHIPPING]) else 0 end ShipCost
   into #Sales
  FROM WebAnalytics_MEDD.[dbo].[Order]
    where CONVERT(varchar(10),TIMESTAMP,121)>=@Date
    --and SESSION_ID='4427247620903944525'
    --and [Customer Type] ='ONDEMAND'
 
 --select * from  #Sales
-------DAILY SALES----
;with cte as
(SELECT 
[CalDate]
,Case When [Order Type] <>'VIP' Then SUM(ORDER_TOTAL) ELSE 0 END as [SalesOnDemand]
,Case When [Order Type] <> 'VIP' Then Count(Order_id) ELSE 0 END as [OrdersOnDemand]
FROM #Sales
group by [CalDate], [Order Type] )

insert into #master (CalDate, [SalesOnDemand], [OrdersOnDemand], [AovOnDemand])
select CalDate, Sum([SalesOnDemand]), Sum([OrdersOnDemand]),  Case When Sum([OrdersOnDemand]) = 0 Then 0 else Sum([SalesOnDemand])/Sum([OrdersOnDemand])end  from CTE group by CalDate

---------AUTOSHIP--------
IF object_id('tempdb..#Auto') is not null DROP TABLE #Auto
;with cte as
(SELECT 
[CalDate]
,Case When [Order Type] ='VIP' Then SUM(ORDER_TOTAL) ELSE 0 END as [SalesAutoship]
,Case When [Order Type] = 'VIP' Then Count(Order_id) ELSE 0 END as [OrdersAutoship]
FROM #Sales
group by [CalDate], [Order Type] )
select CalDate, Sum([SalesAutoship])[SalesAutoship], Sum([OrdersAutoship]) [OrdersAutoship],  Case When Sum([OrdersAutoship]) = 0 Then 0 else Sum([SalesAutoship])/Sum([OrdersAutoship])end AovAutoship 
into #auto from CTE group by CalDate
 
---------<=150 AUTOSHIP--------
IF object_id('tempdb..#Auto1') is not null DROP TABLE #Auto1
;with cte as
(SELECT 
[CalDate]
,Order_id
,Case When [Order Type] ='VIP' Then SUM(ORDER_TOTAL) ELSE 0 END as [SalesAutoship]
,SUM([DISCOUNT Total]) as Disc
FROM #Sales
where [Order Type] = 'VIP'
group by [CalDate], [Order Type],Order_id 
having SUM(ORDER_TOTAL)<=150)
select CalDate, Sum([SalesAutoship])[SalesAutoship], Count(Order_id) [OrdersAutoship],  Case When Count(Order_id) = 0 Then 0 else Sum([SalesAutoship])/Count(Order_id)end AovAutoship ,SUM(Disc) as Disc
into #auto1 from CTE group by CalDate




---------150 to 250 AUTOSHIP--------
IF object_id('tempdb..#Auto2') is not null DROP TABLE #Auto2
;with cte as
(SELECT 
[CalDate]
,Order_id
,Case When [Order Type] ='VIP' Then SUM(ORDER_TOTAL) ELSE 0 END as [SalesAutoship]
,SUM([DISCOUNT Total]) as Disc
FROM #Sales
where [Order Type] = 'VIP'
group by [CalDate], [Order Type],Order_id 
having SUM(ORDER_TOTAL)>150 and SUM(ORDER_TOTAL)<=250)
select CalDate, Sum([SalesAutoship])[SalesAutoship], Count(Order_id) [OrdersAutoship],  Case When Count(Order_id) = 0 Then 0 else Sum([SalesAutoship])/Count(Order_id)end AovAutoship ,SUM(Disc) as Disc
into #auto2 from CTE group by CalDate

--------->250 AUTOSHIP--------
IF object_id('tempdb..#Auto3') is not null DROP TABLE #Auto3
;with cte as
(SELECT 
[CalDate]
,Order_id
,Case When [Order Type] ='VIP' Then SUM(ORDER_TOTAL) ELSE 0 END as [SalesAutoship]
,SUM([DISCOUNT Total]) as Disc
FROM #Sales
where [Order Type] = 'VIP'
group by [CalDate], [Order Type],Order_id 
having SUM(ORDER_TOTAL)>250)
select CalDate, Sum([SalesAutoship])[SalesAutoship], Count(Order_id) [OrdersAutoship],  Case When Count(Order_id) = 0 Then 0 else Sum([SalesAutoship])/Count(Order_id)end AovAutoship ,SUM(Disc) as Disc
into #auto3 from CTE group by CalDate



-----------AutoShip Update --------------------
UPDATE #MASTER
SET [SalesAutoship] = AU.[SalesAutoship], [OrdersAutoship] = AU.[OrdersAutoship], AovAutoship = AU.AovAutoship 
FROM #Auto AU
WHERE #MASTER.CalDate = AU.CalDate

-----------<=150 AutoShip Update --------------------
UPDATE #MASTER
SET [<=150SalesAutoship] = AU.[SalesAutoship], [<=150OrdersAutoship] = AU.[OrdersAutoship], [<=150AovAutoship] = AU.AovAutoship ,[<=150DiscountsAutoShip] = AU.[Disc]
FROM #Auto1 AU
WHERE #MASTER.CalDate = AU.CalDate

-----------150-250 AutoShip Update --------------------
UPDATE #MASTER
SET [150-250SalesAutoship] = AU.[SalesAutoship], [150-250OrdersAutoship] = AU.[OrdersAutoship], [150-250AovAutoship] = AU.AovAutoship ,[150-250DiscountsAutoShip] = AU.[Disc]
FROM #Auto2 AU
WHERE #MASTER.CalDate = AU.CalDate


----------->250 AutoShip Update --------------------
UPDATE #MASTER
SET [>250SalesAutoship] = AU.[SalesAutoship], [>250OrdersAutoship] = AU.[OrdersAutoship], [>250AovAutoship] = AU.AovAutoship ,[>250DiscountsAutoShip] = AU.[Disc]
FROM #Auto3 AU
WHERE #MASTER.CalDate = AU.CalDate


UPDATE #MASTER
SET [TotalSales] = (SalesOnDemand+SalesAutoShip)

--select * from #MASTER

------DAILY DISCOUNT-----
IF object_id('tempdb..#Disc') is not null DROP TABLE #Disc
;With CTE as (
select
[CalDate]
,Case When [Order Type] ='' Then 'ONDEMAND' ELSE [Order Type] END AS [Order Type] 
,SUM([DISCOUNT Total]) as Disc
From #Sales
GROUP bY [CalDate], [Order Type])
Select [CalDate],[Order Type], SUM(Disc) as Disc into #disc from CTE Group By [CalDate],[Order Type]

UPDATE #MASTER
SET [DiscountsAutoShip] = D.[Disc]
FROM #DISC D
WHERE #MASTER.CalDate = D.CalDate
and d.[Order Type]='VIP'

UPDATE #MASTER
SET [DiscountsOnDemand] = D.[Disc]
FROM #DISC D
WHERE #MASTER.CalDate = D.CalDate
and d.[Order Type]='ONDEMAND'



-------CREATE PRICE TABLE FOR FREE GOOD-------
IF object_id('tempdb..#PRICE_USD') is not null DROP TABLE #PRICE_USD
SELECT
 MAX([Starting Date]) DATE_
,[Item No_]
,[Unit of Measure Code]
,[Sales Code]
,'USD' As Currency
INTO #PRICE_USD
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Price]
WHERE [Sales Code] In('RETAIL')
GROUP BY [Item No_], [Unit of Measure Code], [Sales Code]

ALTER TABLE #PRICE_USD
ADD [Unit Price] Numeric(15,2)

UPDATE #PRICE_USD
SET [Unit Price] = P.[Unit Price]
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Price] P
WHERE #PRICE_USD.[Item No_] = P.[Item No_]
AND #PRICE_USD.DATE_ = P.[Starting Date]
AND #PRICE_USD.[Unit of Measure Code] = P.[Unit of Measure Code]
AND P.[Sales Code]In('RETAIL')

---------FREE GOOD-----
IF object_id('tempdb..#FreeGood') is not null DROP TABLE #FreeGood
SELECT 
 a.[visitID]
,a.product_id
,a.product_name
,b.[order_type] AS [order type]
,Case When (RIGHT(Product_id,2))='EA' THEN 'EA' ELSE 'BX' END AS UOM
,p.[Unit Price]
,CONVERT(varchar(10),a.TIMESTAMP,121) as CalDate
,b.ORDER_TOTAL
,a.QUANTITY
,a.QUANTITY *p.[Unit Price] AS COST
into #FreeGood
FROM [WebAnalytics_MEDD].[dbo].[CartItemPurchase] a
Join   [WebAnalytics_MEDD].[dbo].[Order] b
on a.fullVisitorId = b.fullVisitorId AND a.visitId=b.visitId
Left Join #PRICE_USD  P
ON LEFT(A.Product_id,5) COLLATE DATABASE_DEFAULT = P.[Item No_]
 and Case When (RIGHT(Product_id,2))='EA' THEN 'EA' ELSE 'BX' END = [Unit of Measure Code]
 and [Sales Code] ='RETAIL'
where  CONVERT(varchar(10),a.TIMESTAMP,121)>=@Date
  and BASE_PRICE=0
  and RIGHT(Product_id,3) <>'AUT'
  AND RIGHT(Product_id,2) not in ('CF','OD')
  
 
UPDATE #FreeGood
SET [Unit Price] = JF.[Standard Cost]
from [NAV_ETL].[dbo].[Jason Pharm$Item] JF
WHERE LEFT(#FreeGood.Product_id,5) COLLATE DATABASE_DEFAULT = JF.No_
AND #FreeGood.[Unit Price] is null

UPDATE #FreeGood
SET COST = QUANTITY *[Unit Price]
 
UPDATE #FreeGood
SET [Order Type]  ='ONDEMAND' 
where [Order Type]=''
  

----Free good Total Daily---- 
IF object_id('tempdb..#FGD') is not null DROP TABLE #FGD
 select 
 CalDate
,[Order Type]
,SUM(QUANTITY*[Unit Price]) as Cost
Into #FGD
from #FreeGood
group by
  CalDate
,[Order Type]

UPDATE #MASTER
SET [FreeGoodAutoShip] = F.[Cost]
FROM #FGD F
WHERE #MASTER.CalDate = F.CalDate
and F.[Order Type]='VIP'



----<=150 Free good Total Daily---- 
IF object_id('tempdb..#FGD1') is not null DROP TABLE #FGD1
 select 
 CalDate
,[Order Type]
,SUM(QUANTITY*[Unit Price]) as Cost
Into #FGD1
from #FreeGood
where ORDER_TOTAL<=150
group by
  CalDate
,[Order Type]

UPDATE #MASTER
SET [<=150FreeGoodAutoShip] = F.[Cost]
FROM #FGD1 F
WHERE #MASTER.CalDate = F.CalDate
and F.[Order Type]='VIP'

----150 - 250 Free good Total Daily---- 
IF object_id('tempdb..#FGD2') is not null DROP TABLE #FGD2
 select 
 CalDate
,[Order Type]
,SUM(QUANTITY*[Unit Price]) as Cost
Into #FGD2
from #FreeGood
where ORDER_TOTAL>150 and ORDER_TOTAL<=250
group by
  CalDate
,[Order Type]

UPDATE #MASTER
SET [150-250FreeGoodAutoShip] = F.[Cost]
FROM #FGD2 F
WHERE #MASTER.CalDate = F.CalDate
and F.[Order Type]='VIP'

----150 - 250 Free good Total Daily---- 
IF object_id('tempdb..#FGD3') is not null DROP TABLE #FGD3
 select 
 CalDate
,[Order Type]
,SUM(QUANTITY*[Unit Price]) as Cost
Into #FGD3
from #FreeGood
where ORDER_TOTAL>250
group by
  CalDate
,[Order Type]

UPDATE #MASTER
SET [>250FreeGoodAutoShip] = F.[Cost]
FROM #FGD3 F
WHERE #MASTER.CalDate = F.CalDate
and F.[Order Type]='VIP'

UPDATE #MASTER
SET [FreeGoodOnDemand] = F.[Cost]
FROM #FGD F
WHERE #MASTER.CalDate = F.CalDate
and F.[Order Type]='ONDEMAND'

-----SHIPPING---
IF object_id('tempdb..#SHIP') is not null DROP TABLE #SHIP
Select 
CalDate
,Case When [Order Type] ='' Then 'ONDEMAND' ELSE [Order Type] END AS [Order Type] 
,SUM([Shipping]) as ship
INTO #SHIP
from #Sales
where ([Order Total]-[Tax Total]+[Discount Total])=[ORDER_TOTAL]
GROUP BY 
CalDate
,[Order Type]

UPDATE #MASTER
SET [FreeShippingAutoShip] = Isnull(s.[ship],0)
FROM #SHIP s
WHERE #MASTER.CalDate = s.CalDate
and s.[Order Type]='VIP'

-----<=150 SHIPPING---
IF object_id('tempdb..#SHIP1') is not null DROP TABLE #SHIP1
Select 
CalDate
,Case When [Order Type] ='' Then 'ONDEMAND' ELSE [Order Type] END AS [Order Type] 
,SUM([Shipping]) as ship
INTO #SHIP1
from #Sales
where ([Order Total]-[Tax Total]+[Discount Total])=[ORDER_TOTAL]
and ORDER_TOTAL<=150
GROUP BY 
CalDate
,[Order Type]

UPDATE #MASTER
SET [<=150FreeShippingAutoShip] = Isnull(s.[ship],0)
FROM #SHIP1 s
WHERE #MASTER.CalDate = s.CalDate
and s.[Order Type]='VIP'

-----150 - 250 SHIPPING---
IF object_id('tempdb..#SHIP2') is not null DROP TABLE #SHIP2
Select 
CalDate
,Case When [Order Type] ='' Then 'ONDEMAND' ELSE [Order Type] END AS [Order Type] 
,SUM([Shipping]) as ship
INTO #SHIP2
from #Sales
where ([Order Total]-[Tax Total]+[Discount Total])=[ORDER_TOTAL]
and ORDER_TOTAL>150 and ORDER_TOTAL<=250
GROUP BY 
CalDate
,[Order Type]

UPDATE #MASTER
SET [150-250FreeShippingAutoShip] = Isnull(s.[ship],0)
FROM #SHIP2 s
WHERE #MASTER.CalDate = s.CalDate
and s.[Order Type]='VIP'

----->250 SHIPPING---
IF object_id('tempdb..#SHIP3') is not null DROP TABLE #SHIP3
Select 
CalDate
,Case When [Order Type] ='' Then 'ONDEMAND' ELSE [Order Type] END AS [Order Type] 
,SUM([Shipping]) as ship
INTO #SHIP3
from #Sales
where ([Order Total]-[Tax Total]+[Discount Total])=[ORDER_TOTAL]
and ORDER_TOTAL>250
GROUP BY 
CalDate
,[Order Type]

UPDATE #MASTER
SET [>250FreeShippingAutoShip] = Isnull(s.[ship],0)
FROM #SHIP3 s
WHERE #MASTER.CalDate = s.CalDate
and s.[Order Type]='VIP'

UPDATE #MASTER
SET [FreeShippingOnDemand] =Isnull(s.[ship],0)
FROM #SHIP s
WHERE #MASTER.CalDate = s.CalDate
and s.[Order Type]='ONDEMAND'


-------PRICE DEVIATION-------
IF object_id('tempdb..#CoreOrder') is not null DROP TABLE #CoreOrder
Select CONVERT(Varchar(10),P.TIMESTAMP,121) as CalDate, P.ORDER_ID, P.PRODUCT_ID, P.QUANTITY, o.[Order_Type] AS [Order Type] ,o.ORDER_TOTAL
into #CoreOrder
FROM WebAnalytics_MEDD.[dbo].[CartItemPurchase] p
join WebAnalytics_MEDD.[dbo].[Order] o
on p.ORDER_ID = o.ORDER_ID
WHERE P.TIMESTAMP>=@Date
  



-----PRICE LIST FOR DEVIATION----
IF object_id('tempdb..#PriceList') is not null DROP TABLE #PriceList
SELECT 
CalDate
,oh.ORH_SOURCE_NBR
,Case When [Order Type] = '' Then 'ONDEMAND' Else [Order Type] end as [Order Type]
,b.SKU_CODE
,PRD_Code
,QUANTITY
,ORDER_TOTAL
,[ORL_LIST_PRICE_AMT] UcartListPrice
,e.[Unit Price] NavPrice
,e.[Unit Price]-[ORL_LIST_PRICE_AMT] as Variance
,QUANTITY * (e.[Unit Price]-[ORL_LIST_PRICE_AMT]) as TotalPriceVariance
Into #PriceList
FROM [ECOMM_ETL ].[dbo].[ORDER_HEADER] oh
  Join [ECOMM_ETL ].[dbo].[ORDER_LINE] a
  on oh.ORH_ID = a.ORL_ORH_ID
  Join [prdmartini_MAIN_repl].[dbo].[SKU] b
  on a.ORL_SKU_ID = b.SKU_ID
  Join [prdmartini_MAIN_repl].[dbo].[PRODUCT] c
  on b.SKU_PARENT_ID = c.PRD_ID
  Join #CoreOrder d
  on oh.ORH_SOURCE_NBR = CAST(d.Order_id AS NVARCHAR(250)) 
  and c.PRD_CODE = d.product_id
  Join #PRICE_USD e
  on d.PRODUCT_ID COLLATE DATABASE_DEFAULT = e.[Item No_]
  and RIGHT(b.sku_code,2)COLLATE DATABASE_DEFAULT = e.[Unit of Measure Code]
Group By
CalDate
,oh.ORH_SOURCE_NBR
,PRD_Code
,[ORL_LIST_PRICE_AMT]
,e.[Unit Price]
,b.SKU_CODE
,QUANTITY
,ORDER_TOTAL
,Case When [Order Type] = '' Then 'ONDEMAND' Else [Order Type] end
order by
e.[Unit Price]-[ORL_LIST_PRICE_AMT] desc

Update #PriceList 
set Navprice = 0, Variance =0
where PRD_CODE ='40490'

---------Total Price Deviation-------------
IF object_id('tempdb..#TotPriceDeviation') is not null DROP TABLE #TotPriceDeviation
select 
 CalDate
,[Order Type]
,SUM(TotalPriceVariance) as TotalPriceVariance
Into #TotPriceDeviation
from #PriceList
where Variance <>0
group by CalDate, [Order Type]
order by CalDate


Update #MASTER
SET [ListPriceDeviationOndemand] = T.TotalPriceVariance
FROM #TotPriceDeviation T
WHERE #MASTER.CalDate = T.CalDate
and t.[Order Type] ='ONDEMAND'

Update #MASTER
SET [ListPriceDeviationAutoShip] = T.TotalPriceVariance
FROM #TotPriceDeviation T
WHERE #MASTER.CalDate = T.CalDate
and t.[Order Type] ='VIP'

---------<=150 Total Price Deviation-------------
IF object_id('tempdb..#TotPriceDeviation1') is not null DROP TABLE #TotPriceDeviation1
select 
 CalDate
,[Order Type]
,SUM(TotalPriceVariance) as TotalPriceVariance
Into #TotPriceDeviation1
from #PriceList
where Variance <>0
and ORDER_TOTAL<=150
group by CalDate, [Order Type]
order by CalDate


Update #MASTER
SET [<=150ListPriceDeviationAutoShip] = T.TotalPriceVariance
FROM #TotPriceDeviation1 T
WHERE #MASTER.CalDate = T.CalDate
and t.[Order Type] ='VIP'

---------150 -250  Total Price Deviation-------------
IF object_id('tempdb..#TotPriceDeviation2') is not null DROP TABLE #TotPriceDeviation2
select 
 CalDate
,[Order Type]
,SUM(TotalPriceVariance) as TotalPriceVariance
Into #TotPriceDeviation2
from #PriceList
where Variance <>0
and ORDER_TOTAL>150 and ORDER_TOTAL<=250
group by CalDate, [Order Type]
order by CalDate


Update #MASTER
SET [150-250ListPriceDeviationAutoShip] = T.TotalPriceVariance
FROM #TotPriceDeviation2 T
WHERE #MASTER.CalDate = T.CalDate
and t.[Order Type] ='VIP'

--------->250  Total Price Deviation-------------
IF object_id('tempdb..#TotPriceDeviation3') is not null DROP TABLE #TotPriceDeviation3
select 
 CalDate
,[Order Type]
,SUM(TotalPriceVariance) as TotalPriceVariance
Into #TotPriceDeviation3
from #PriceList
where Variance <>0
and ORDER_TOTAL>250
group by CalDate, [Order Type]
order by CalDate


Update #MASTER
SET [>250ListPriceDeviationAutoShip] = T.TotalPriceVariance
FROM #TotPriceDeviation3 T
WHERE #MASTER.CalDate = T.CalDate
and t.[Order Type] ='VIP'

--select [AutoShipTotal] , [<=150AutoShipTotal] ,[150-250AutoShipTotal],[>250AutoShipTotal]  from #Master

------TOTAL-------
Update #MASTER Set [Total]=[FreeGoodOnDemand]+[FreeShippingOnDemand]+[DiscountsOnDemand]+[ListPriceDeviationOndemand]+[FreeGoodAutoShip]+[FreeShippingAutoShip]+[DiscountsAutoShip]+[ListPriceDeviationAutoShip]
Update #MASTER set [TotalOrders]=[OrdersOnDemand]+[OrdersAutoship]
Update #MASTER set [TotalAov]=Case When [OrdersOnDemand] = 0 and [OrdersAutoship]=0 Then 0 else ([TotalSales]/[TotalOrders])end 
Update #MASTER set [TotalFreeShipping] =[FreeShippingOnDemand]+[FreeShippingAutoShip]
Update #MASTER set [TotalFreeGood]=[FreeGoodOnDemand]+[FreeGoodAutoShip]
Update #MASTER set [TotalDiscounts]=[DiscountsOnDemand]+[DiscountsAutoShip]
Update #MASTER set [TotalListPriceDeviation]=[ListPriceDeviationOndemand]+[ListPriceDeviationAutoShip]

UPDATE #Master Set [OnDemandTotal]  = [FreeGoodOnDemand]+[FreeShippingOnDemand]+[DiscountsOnDemand]+[ListPriceDeviationOndemand]
UPDATE #Master Set [MarketingPercentOnDemand] = Case When [SalesOnDemand] = 0 Then 0 Else ([OnDemandTotal]/[SalesOnDemand]) end

UPDATE #Master Set [AutoShipTotal]  = [FreeGoodAutoShip]+[FreeShippingAutoShip]+[DiscountsAutoShip]+[ListPriceDeviationAutoShip]
UPDATE #Master Set [MarketingPercentAutoShip] = Case When [SalesAutoShip] = 0 Then 0 Else ([AutoShipTotal]/[SalesAutoShip]) end 

UPDATE #Master Set [<=150AutoShipTotal]  = [<=150FreeGoodAutoShip]+[<=150FreeShippingAutoShip]+[<=150DiscountsAutoShip]+[<=150ListPriceDeviationAutoShip]
UPDATE #Master Set [<=150MarketingPercentAutoShip] = Case When [<=150SalesAutoShip] = 0 Then 0 Else  ([<=150AutoShipTotal]/[<=150SalesAutoShip]) end 

UPDATE #Master Set [150-250AutoShipTotal]  = [150-250FreeGoodAutoShip]+[150-250FreeShippingAutoShip]+[150-250DiscountsAutoShip]+[150-250ListPriceDeviationAutoShip]
UPDATE #Master Set [150-250MarketingPercentAutoShip] = Case When [150-250SalesAutoShip] = 0 Then 0 Else ([150-250AutoShipTotal]/[150-250SalesAutoShip]) end 

UPDATE #Master Set [>250AutoShipTotal]  = [>250FreeGoodAutoShip]+[>250FreeShippingAutoShip]+[>250DiscountsAutoShip]+[>250ListPriceDeviationAutoShip]
UPDATE #Master Set [>250MarketingPercentAutoShip] = Case When [>250SalesAutoShip] = 0 Then 0 Else ([>250AutoShipTotal]/[>250SalesAutoShip]) end 

UPDATE #Master set GrandTotal = [AutoShipTotal]+ [OnDemandTotal]
UPDATE #MASTER Set [MarketingPercent] = Case When (SalesOnDemand+SalesAutoShip) = 0 Then 0 Else ([GrandTotal]/(SalesOnDemand+SalesAutoShip)) End
Update #MASTER Set [TotalMarketingPercent]=  Case When [SalesOnDemand] = 0 and [SalesAutoShip] = 0 then 0 else (([OnDemandTotal]+[AutoShipTotal])/([SalesOnDemand]+[SalesAutoShip])) end 

--------TRACK VISITOR-------
IF object_id('tempdb..#Visitor') is not null DROP TABLE #Visitor
Select 
 CONVERT(Varchar(10),[TIMESTAMP],121) as CalDate
,COUNT(Distinct([visitID])) as tot
Into #Visitor
From WebAnalytics_MEDD.[dbo].FirstPageViewSession
Where Convert(Varchar(10),[TIMESTAMP],121)>=@Date
Group By CONVERT(Varchar(10),[TIMESTAMP],121)

Update #Master
Set Visitors = v.Tot
from #visitor v
where #master.caldate = v.caldate

----CONVERSION RATE----
IF object_id('tempdb..#Session') is not null DROP TABLE #Session
Select 
CONVERT(Varchar(10),[TIMESTAMP],121) as CalDate
,Convert(Decimal(18,0),COUNT(Distinct [visitID]))  as [Session]
into #Session
From WebAnalytics_MEDD.[dbo].FirstPageViewSession
where Convert(Varchar(10),[TIMESTAMP],121)>=@Date
group by CONVERT(Varchar(10),[TIMESTAMP],121)

UPDATE #MASTER
set [Session] = s.[Session]
from #Session s
where #master.caldate = s.caldate

UPDATE #MASTER
SET ConversionRate = (([OrdersOnDemand]+[OrdersAutoship])/[Session])

-------FINAL------
Truncate table BI_Reporting.dbo.XL_061_CoremetricWithCost_GA
insert into BI_Reporting.dbo.XL_061_CoremetricWithCost_GA
Select 
 DayOfWeekName
,CalDate
,Visitors
,TotalSales
,ConversionRate As [ConversionRate(%)]
,[TotalOrders] 
,[TotalAov] 
,[TotalFreeShipping] 
,[TotalFreeGood] 
,[TotalDiscounts] 
,[TotalListPriceDeviation] 
,[Total] 
,[TotalMarketingPercent]
,SalesOnDemand
,OrdersOnDemand
,AovOnDemand
,FreeShippingOnDemand
,FreeGoodOnDemand
,DiscountsOnDemand
,[ListPriceDeviationOndemand]
,OnDemandTotal
,MarketingPercentOnDemand As [MarketingPercentOnDemand(%)]

,SalesAutoship
,OrdersAutoship
,AovAutoship
,FreeShippingAutoship
,FreeGoodAutoship
,DiscountsAutoship
,[ListPriceDeviationAutoShip]
,AutoshipTotal
,MarketingPercentAutoship As [MarketingPercentAutoship(%)]
,GrandTotal
,MarketingPercent AS [MarketingPercent(%)]

,CalendarMonthName
,CalendarQuarter 
,[<=150SalesAutoShip] 
,[<=150OrdersAutoship]  
,[<=150AovAutoship] 
,[<=150FreeShippingAutoShip]  
,[<=150FreeGoodAutoShip]  
,[<=150DiscountsAutoShip]  
,[<=150ListPriceDeviationAutoShip] 
,[<=150AutoShipTotal]  
,[<=150MarketingPercentAutoship] as [<=150MarketingPercentAutoship(%)]
,[150-250SalesAutoShip]
,[150-250OrdersAutoship]  
,[150-250AovAutoship] 
,[150-250FreeShippingAutoShip]  
,[150-250FreeGoodAutoShip]  
,[150-250DiscountsAutoShip]  
,[150-250ListPriceDeviationAutoShip]
,[150-250AutoShipTotal]  
,[150-250MarketingPercentAutoship] as [150-250MarketingPercentAutoship(%)]
,[>250SalesAutoShip] 
,[>250OrdersAutoship] 
,[>250AovAutoship]  
,[>250FreeShippingAutoShip]  
,[>250FreeGoodAutoShip] 
,[>250DiscountsAutoShip]  
,[>250ListPriceDeviationAutoShip] 
,[>250AutoShipTotal] 
,[>250MarketingPercentAutoship] as [>250MarketingPercentAutoship(%)]
 From #master ma
join dbo.calendar ca
on ma.CalDate = ca.CalendarDate
Order By caldate

select CONVERT(varchar(10), GETDATE(), 101) as [Last Refresh]




