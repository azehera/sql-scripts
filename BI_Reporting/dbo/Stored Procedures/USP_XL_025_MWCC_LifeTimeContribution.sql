﻿


/*
==========================================================================================================================================================
Author:         Luc Emond
Create date: 04/15/2013
-------------------------[USP_XL_025_MWCC_LifeTimeContribution]-------------------------------------------------------------------------------------------
Book 4 Time Daily Activities for Life Time Contribution
===========================================================================================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------------------------------------------------------------------------------
[BI_Reporting]        dbo.Calendar                              Select    
[BI_Reporting]        dbo.XL_025_MWCC_ItemCode                  Select 
[Book4Time_ETL]       [B4T_transaction_log_header]              Select
[Book4Time_ETL]       [B4T_transaction_log_Detail]              Select
[Book4Time_ETL]       [B4T_location]                            Select
[Book4Time_ETL]       [B4T_product_master]                      Select
[Book4Time_ETL]       [B4T_product_class]                       Select
[Book4Time_ETL]       [B4T_region_locations]                    Select
[Book4Time_ETL]       [B4T_region]                              Select
[Book4Time_ETL]       [B4T_Customer]                            Select
[Book4Time_ETL]       [B4T_Person]                              Select
=============================================================================================================================================================
REVISION LOG
Date           Name                  Change
------------ ----------------------------------------------------------------------------------------------------------------------------------------------
2013-09-09    Luc Emond          Created Table  dbo.XL_025_MWCC_ItemCode to capture programs needed for the report
2013-09-30    Luc Emond          Added Parent Of Parent Id     
2014-01-30    Luc Emond          Added Transaction Date 
2014-04-21    Luc Emond          Changed the @2Ago to be always 2011/01/01
2014-06-18    Luc Emond          Added fields CalendarMonthId and TotalSpend to the report as per Christos
==============================================================================================================================================================
NOTES:
--------------------------------------------------------------------------------

==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_025_MWCC_LifeTimeContribution] as
SET NOCOUNT ON;


----DECLARE TIME FRAM FOR REPORT-----
DECLARE @2AGO DATETIME, @TY DATETIME
SET @2AGO = (Select FirstDateOfYear from dbo.Calendar where CalendarYear = '2011' GROUP BY FirstDateOfYear)-----Luc Emond Updated to be always 2011------
SET @TY = (Select LastDateOfYear from dbo.Calendar where CalendarYear = DATEPART(YYYY,GETDATE()-1) GROUP BY LastDateOfYear)

--SELECT @2AGO, @TY
---------------------ALL SALES DATA FOR PROGRAM ONLY-------------------------------------------
IF object_id('Tempdb..#Program') Is Not Null DROP TABLE #Program
SELECT
Legal_Name
,Customer_id
,item_code
,item_desc
,WeightCategory
,MIN(Transaction_Date)As FirstProgramDate      
,MAX(Transaction_Date)As LastProgramDate
,SUM(sold_price * qty) aS NetSales
,SUM(Qty * Discount_amt)*-1 aS Discount
,0 as Tenure
INTO #Program
From [Book4Time_ETL].[dbo].[B4T_transaction_log_header] h
Join [Book4Time_ETL].[dbo].[B4T_transaction_log_Detail] d
    on h.transaction_id = d.transaction_id
JOIN [Book4Time_ETL].[dbo].[B4T_location]  L
    ON h.location_id = L.location_id  
Join dbo.XL_025_MWCC_ItemCode  ic----Manual table created from an email from Chritos Christou  09/08/2013-------Added 'JS4W' as Per Christos Request which is 4 week Plan 08/18/2014
on d.item_code = ic.ItemCode  
Where (Select Convert(Varchar (10),Transaction_Date, 121))BETWEEN @2AGO AND @TY AND [Transaction_Type] In ('S', 'R')
GROUP BY 
Legal_Name
,Customer_id
,item_code
,item_desc
,WeightCategory
,Transaction_Date----Luc Emond 2014-01-30---
ORDER BY Customer_id

----DELETE UNWANTED DATA-------------
Delete #Program where customer_id is null

------ALL SALES-----
IF object_id('Tempdb..#Sales') Is Not Null DROP TABLE #Sales
SELECT
Legal_Name
,Customer_id
--,item_code
--,item_desc
,MIN(Transaction_Date)As FirstInvoiceDate 
,MAX(Transaction_Date)As LastInvoiceDate 
INTO #Sales
From [Book4Time_ETL].[dbo].[B4T_transaction_log_header] h
Join [Book4Time_ETL].[dbo].[B4T_transaction_log_Detail] d
on h.transaction_id = d.transaction_id
JOIN [Book4Time_ETL].[dbo].[B4T_location]  L
    ON h.location_id = L.location_id  
WHERE (Select Convert(Varchar (10),Transaction_Date, 121))BETWEEN @2AGO AND @TY AND [Transaction_Type] In ('S', 'R') and customer_id is not null
GROUP BY 
Legal_Name
,Customer_id
--,item_code
--,item_desc
ORDER BY Customer_id

---ALTER TABLE TO ADD FIRST AND LAST ORDER DATE--------
ALTER TABLE #PROGRAM ADD FirstInvoiceDate datetime, LastInvoiceDate datetime

UPDATE #PROGRAM 
SET FirstInvoiceDate = S.FirstInvoiceDate, LastInvoiceDate = S.LastInvoiceDate
FROM #Sales S
WHERE #Program.customer_id = S.customer_id
AND #PROGRAM.legal_name = S.legal_name

UPDATE #PROGRAM 
SET Tenure = DATEDIFF(dd,FirstProgramdate,LastInvoiceDate)

-----BRING IN ALL SALES INFO FOOD ETC-------------------------------------------------------------------
IF object_id('Tempdb..#AC') Is Not Null DROP TABLE #AC
SELECT 
 Region_Name
,Location_Name
,Legal_Name
,item_code
,Item_Desc
,b.customer_id
,Convert(Varchar (10),B.Transaction_Date, 121) As TransactionDate
,SUM(a.sold_price * a.qty)as Dollars
,Case When PC.class_desc ='Food' Then SUM(a.sold_price * a.qty) Else 0 end as 'Food'
,Case When PC.class_desc ='Non-Product (Tools)' Then SUM(a.sold_price * a.qty) Else 0 end as 'Non-Product (Tools)'
,Case When PC.class_desc ='Programs' Then SUM(a.sold_price * a.qty) Else 0 end as 'Programs'
,Case When PC.class_desc IN('Care Call','Complimentary Weight Loss Profile','Counseling Visits', 'Medifast Gift Certificate','ZAdmin') Then SUM(a.sold_price * a.qty) Else 0 end as 'Other'
INTO #AC
FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] A
JOIN 
     [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B
      ON B.[transaction_id] = A.[transaction_id]
JOIN [Book4Time_ETL].[dbo].[B4T_product_master] PM
     ON A.product_id = PM.product_id
JOIN [Book4Time_ETL].[dbo].[B4T_product_class] PC
     ON PM.class_id = PC.class_id          
JOIN [Book4Time_ETL].[dbo].[B4T_location]  L
    ON B.location_id = L.location_id  
JOIN [Book4Time_ETL].[dbo].[B4T_region_locations] RL
     ON L.location_id = RL.location_id
JOIN [Book4Time_ETL].[dbo].[B4T_region] R
     ON RL.region_id = R.region_id  
WHERE (Select Convert(Varchar (10),B.Transaction_Date, 121))BETWEEN @2AGO AND @TY AND B.[Transaction_Type] In ('S', 'R') 
Group By 
 Region_Name
,Location_Name
,b.customer_id
,PC.class_desc
,Legal_Name
,item_code
,Item_Desc
,B.Transaction_Date

----DELETE UNWANTED DATA-------------
DELETE #AC WHERE CUSTOMER_ID IS NULL
DELETE #AC WHERE DOLLARS =0

---PUT IT TOGETHER---------
IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master
select 
l.Region_Name,
l.Location_Name,
p.legal_name,
p.customer_id,
c.Parent_id,
p.item_code,
p.item_desc,
ltrim(p.WeightCategory) as WeightCategory,
Convert(Varchar (10),p.FirstProgramDate,121)as FirstProgramDate,
Convert(Varchar (10),p.LastProgramDate,121) as LastProgramDate,
Convert(Varchar (10),p.FirstInvoiceDate,121)as FirstInvoiceDate,
Convert(Varchar (10),p.LastInvoiceDate,121)as LastInvoiceDate,
p.Tenure,
p.NetSales - P.Discount As GrossProgram,
p.Discount,
p.NetSales As NetProgram,
c.custom_field1 as [ASF Account#],
SUM(Food) as Food,
SUM([Non-Product (Tools)]) as [Non-Product (Tools)],
SUM(Programs) as TotalPrograms,
SUM(Other) as Other,
Case When WeightCategory in ('Flex', 'Other', 'RAM') then 0 
     when WeightCategory in ('4 Week') then 28  else (p.WeightCategory/2) * 7 end as ExpectedTenure----Added 'JS4W' as Per Christos Request which is 4 week Plan 08/18/2014
Into #master
from #Program p
Join #AC l
on p.legal_name = l.legal_name
and p.customer_id = l.customer_id
Join [Book4Time_ETL].[dbo].[B4T_customer] c
on p.customer_id = c.customer_id
--and l.TransactionDate between LastProgramDate-1 and LastInvoicedate+1
Group By
l.Region_Name,
l.Location_Name,
p.legal_name,
p.customer_id,
c.Parent_id,
p.item_code,
p.item_desc,
p.WeightCategory,
p.LastProgramDate,
p.LastInvoiceDate,
p.Tenure,
p.NetSales,
p.Discount,
p.FirstProgramDate,
p.FirstInvoiceDate,
c.custom_field1
order by LastProgramDate asc

--ALTER AND UPDATE FOR PARENT OF PARENT ID AND CUSTOMER NAME----
ALTER TABLE #MASTER
Add ParentOfParentId BigInt, CustomerName Char(50)

Update #MASTER
set ParentOfParentId = parent_id 
where #MASTER.customer_id = #MASTER.parent_id

UPDATE #MASTER
SET ParentOfParentId = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
WHERE #MASTER.parent_id = p.customer_id
and ParentOfParentId is null

UPDATE #MASTER
SET CustomerName = P.first_name+' ' +P.last_name
FROM [Book4Time_ETL].[dbo].[B4T_Person]p
WHERE #MASTER.Parent_id = p.person_id
     
----FIANL TABLE      
TRUNCATE TABLE dbo.XL_025_MWCC_LifeTimeContribution
INSERT INTO dbo.XL_025_MWCC_LifeTimeContribution
(
 [Region_Name]
,[Location_Name]
,[legal_name]
,[Parent Of ParentId]
,[customer_id]
,[CustomerName]
,[item_code]
,[item_desc]
,[WeightCategory]
,[FirstProgramDate]
,[LastProgramDate]
,[FirstInvoiceDate]
,[LastInvoiceDate]
,[Tenure]
,[GrossProgram]
,[Discount]
,[NetProgram]
,[ASF Account#]
,[Food]
,[Non-Product (Tools)]
,[TotalPrograms]
,TotalSpend ----2014-06-18 Luc Emond 
,[Other]
,[ExpectedTenure]
,FirstProgram_CalendarMonthID
)
SELECT [Region_Name]
      ,[Location_Name]
      ,[legal_name]
      ,ParentOfParentId as [Parent Of ParentId]
      ,[customer_id]
      ,[CustomerName]
      ,[item_code]
      ,[item_desc]
      ,len([WeightCategory])
      ,[FirstProgramDate]
      ,[LastProgramDate]
      ,[FirstInvoiceDate]
      ,[LastInvoiceDate]
      ,[Tenure]
      ,[GrossProgram]
      ,[Discount]
      ,[NetProgram]
      ,[ASF Account#]
      ,[Food]
      ,[Non-Product (Tools)]
      ,[TotalPrograms]
      ,[Food]+[Non-Product (Tools)]+[TotalPrograms] As TotalSpend ----2014-06-18 Luc Emond 
      ,[Other]
      ,[ExpectedTenure]
      ,CalendarMonthID as FirstProgram_CalendarMonthID
FROM #MASTER m
Join dbo.calendar cal
on m.FirstProgramDate = cal.CalendarDate
Where LastInvoiceDate>='2012-01-01'
Order By LastInvoiceDate
 





