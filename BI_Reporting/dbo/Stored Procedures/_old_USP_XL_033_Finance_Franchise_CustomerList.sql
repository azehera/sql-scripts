﻿
CREATE procedure  [dbo].[_old_USP_XL_033_Finance_Franchise_CustomerList]
as

-- Select distinct x.State from [XL_033_Finance_FranchiseZipGeoInfo] x where x.MilePayOut > 0 and x.[Closing Date] IS NULL order by x.State -- Gets state codes that should be considered

--EXECUTE AS LOGIN = 'ReportViewer'
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT  distinct 
      [Ship-to Address],[Ship-to Address 2]
      ,[Ship-to City]
      ,[Ship-to County] as State,left ([Ship-to Post Code],5) as [Zip5],[Sell-to Country_Region Code] as Country
    into #A FROM NAV_ETL.dbo.[Jason Pharm$Sales Invoice Line] SalesInvLine2 with (nolock)
	                         INNER JOIN NAV_ETL.dbo.[Jason Pharm$Sales Invoice Header] SalesInvHeader2 with (nolock)
	                                    ON SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
   where SalesInvHeader2.[Posting Date]>=(SELECT DATEADD(YY, DATEDIFF (YY,0,GETDATE()-1),0))---First of Year---
   and [Customer Posting Group] in ('MEDIFAST') 
   and [Ship-to County] in (Select distinct x.State from [XL_033_Finance_FranchiseZipGeoInfo] x where x.MilePayOut > 0 and x.[Closing Date] IS NULL)
   
select a.[Ship-to Address],a.[Ship-to Address 2]
      ,a.[Ship-to City]
      , a.State,a.[Zip5], a.Country from #A a
   left join BI_Reporting.dbo.XL_033_Finance_Franchise_Customers b
   on a.[Ship-to Address]=b.[Ship-to Address]
   where b.[Ship-to Address] is  null
