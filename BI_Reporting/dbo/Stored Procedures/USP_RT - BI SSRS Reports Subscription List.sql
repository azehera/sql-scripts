﻿





/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------

[MED-REPORT2]            ReportServer.dbo.Catalog            Select 
[MED-REPORT2]            ReportServer.dbo.Subscriptions      Select
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - BI SSRS Reports Subscription List]
AS
IF OBJECT_ID('Tempdb..#AA') IS NOT NULL
    DROP TABLE #AA;
WITH subscriptionXmL
AS (SELECT SubscriptionID,
           Report_OID,
           ExtensionSettings,
           CONVERT(XML, ExtensionSettings) AS ExtensionSettingsXML,
           Parameters
    FROM [MED-REPORT2].ReportServer.dbo.Subscriptions),
     -- Get the settings as pairs
     SettingsCTE
AS (SELECT SubscriptionID,
           Report_OID,
           ExtensionSettings,
           --ISNULL(Settings.value('(./*:Name/text())[1]', 'nvarchar(1024)'), 'Value') AS SettingName,
           Settings.value('(./*:Value/text())[1]', 'nvarchar(max)') AS SettingValue
    FROM subscriptionXmL
        CROSS APPLY subscriptionXmL.ExtensionSettingsXML.nodes('//*:ParameterValue') Queries(SETTINGS) )
SELECT cat.Path,
       cat.Name,
       se.SubscriptionID,
       --se.SettingName,
       se.SettingValue
INTO #AA
FROM SettingsCTE se
    INNER JOIN [MED-REPORT2].ReportServer.dbo.Catalog cat
        ON cat.ItemID = se.Report_OID
--WHERE settingName IN ( 'TO', 'CC', 'BCC' );

IF OBJECT_ID('Tempdb..#BB') IS NOT NULL
    DROP TABLE #BB;
SELECT A.[SubscriptionID],
       Split.a.value('.', 'VARCHAR(100)') AS Email
INTO #BB
FROM
(
    SELECT [SubscriptionID],
           CAST('<M>' + REPLACE([SettingValue], ';', '</M><M>') + '</M>' AS XML) AS String
    FROM #AA
) AS A
    CROSS APPLY String.nodes('/M') AS Split(a);

SELECT DISTINCT
       a.Path,
       a.Name,
       a.SubscriptionID,
       --a.SettingName,
       b.Email
FROM #AA a
    INNER JOIN #BB b
        ON a.SubscriptionID = b.SubscriptionID
WHERE b.Email LIKE '%@%'
AND b.Email NOT LIKE '%@ReportName%'
ORDER BY b.Email;