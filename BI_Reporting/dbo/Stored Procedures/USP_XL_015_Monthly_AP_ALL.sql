﻿



CREATE PROCEDURE [dbo].[USP_XL_015_Monthly_AP_ALL] As
SET NOCOUNT ON;

/*
=============================================================================================
Author:         Luc Emond
Create date: 08/22/2013
-------------------------[USP_XL_015_Monthly_AP_ALL]-----------------------------
Account Payable information ytd by Month---part of the closing process----
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[BI_Reporting]         [XL_015_Monthly_AP_JasonPharm]            Select  
[BI_Reporting]         USP_XL_015_Monthly_AP_JasonEnterprises    Exec
[BI_Reporting]         USP_XL_015_Monthly_AP_7Crondall           Exec
[BI_Reporting]         USP_XL_015_Monthly_AP_JasonProperties     Exec
[BI_Reporting]         USP_XL_015_Monthly_AP_MedifastInc         Exec  
[BI_Reporting]         USP_XL_015_Monthly_AP_TSFL                Exec 
[NAV_ETL]              [dbo].[Jason Pharm$G_L Entry]             Select
[NAV_ETL]              [dbo].[7 Crondall$G_L Entry]              Select
[NAV_ETL]              [dbo].[Jason Enterprises$G_L Entry]       Select
[NAV_ETL]              [dbo].[Jason Properties$G_L Entry]        Select
[NAV_ETL]              [dbo].[Medifast Inc$Vendor Ledger Entry]  Select
[NAV_ETL]              [dbo].[Take Shape For Life$Vendor Ledger Entry]  Select
                 
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------
2015-08-13	  Micah W.		  Added [Address], [Address 2], [City], [County],[Post Code],[Country_Region Code]
							  per user's request
2017-05-10		D.SMith		Changed [External Document No_] from varchar(20) to varchar (35) by Nav 2016 Upgrade
2017-05-10		D.Smith		Changed to varchar (35) by Nav 2016 Upgrade
==========================================================================================
*/


----DECLARE TABLE FOR DATA-------
DECLARE @TblWorkTable TABLE (
	[Entity] [varchar](25) NULL,
	[Source Code] [varchar](10) NULL,
	[Posting Date] [datetime] NULL,
	[Document Date] [datetime] NULL,
	[G_L Account No_] [varchar](20) NULL,
	[G_L Name] [varchar](50) NULL,
	[Vendor No_] [varchar](20) NULL,
	[1099 Code] [varchar](10) NULL,
	[1099 Amount] [decimal](38, 4) NULL,
	[VendorName] [varchar](50) NULL,
	[Address] [varchar](250) NULL, 
	[Address 2] [varchar](250) NULL, 
	[City][varchar](250) NULL, 
	[State][varchar](250) NULL,
	[Zip Code][varchar](20) NULL,
	[Country_Region Code][varchar](250) NULL,
	[Vendor Posting Group] [varchar] (10) NULL,
	[Document No_] [varchar](20) NULL,
	[Description] [varchar](50) NULL,
	[Global Dimension 2 Code] [varchar](20) NULL,
	[Transaction No_] [int] NULL,
	[Source No_] [varchar](20) NULL,
	[Entry No_] [int] NULL,
	[External Document No_] [varchar] (35) NULL, --Changed to varchar (35) by Nav 2016 Upgrade
	[User ID][varchar] (50) NULL, --Changed to varchar (50) by Nav 2016 Upgrade
	[Dimension Value Code] [varchar](20) NULL,
	[ProjectName] [varchar](50) NULL,
	[Debit Amount] [decimal](38, 4) NULL,
	[Credit Amount] [decimal](38, 4) NULL,
	[NetTotal] [decimal](38, 4) NULL,
	[Dimension Set ID] INT
)

----INSERTING THE DATA------
INSERT INTO @TblWorkTable SELECT * FROM [BI_Reporting].[dbo].[XL_015_Monthly_AP_JasonPharm]
Insert into @TblWorkTable exec [BI_Reporting].dbo.USP_XL_015_Monthly_AP_JasonEnterprises
insert into @TblWorkTable exec [BI_Reporting].dbo.USP_XL_015_Monthly_AP_7Crondall
insert into @TblWorkTable exec [BI_Reporting].dbo.USP_XL_015_Monthly_AP_JasonProperties
insert into @TblWorkTable exec [BI_Reporting].dbo.USP_XL_015_Monthly_AP_MedifastInc
insert into @TblWorkTable exec [BI_Reporting].dbo.USP_XL_015_Monthly_AP_TSFL
insert into @TblWorkTable exec [BI_Reporting].dbo.USP_XL_015_Monthly_AP_MedifastNutrition

----SELECTING THE DATA IN EXCEL REPORT------
TRUNCATE TABLE [dbo].[XL_015_Monthly_AP_ALL]
INSERT INTO [dbo].[XL_015_Monthly_AP_ALL] (
 [Entity]
,[Source Code] 
,[Posting Date] 
,[Document Date] 
,[G_L Account No_] 
,[G_L Name] 
,[Vendor No_] 
,[1099 Code]
,[1099 Amount]
,[VendorName]
,[Address] 
,[Address 2] 
,[City]
,[State]
,[Zip Code]
,[Country_Region Code] 
,[Vendor Posting Group] 
,[Document No_] 
,[Description] 
,[Global Dimension 2 Code] 
,[Transaction No_] 
,[Source No_] 
,[Entry No_] 
,[External Document No_]
,[User ID]
,[Dimension Value Code] 
,[ProjectName]
,[Debit Amount] 
,[Credit Amount] 
,[NetTotal]
,[Check#]
,CheckDate
,DaysElasped
,[Dimension Set ID]
)

SELECT
 [Entity]
,[Source Code] 
,[Posting Date] 
,[Document Date] 
,[G_L Account No_] 
,[G_L Name] 
,[Vendor No_] 
,[1099 Code]
,[1099 Amount]
,[VendorName] 
,[Address] 
,[Address 2] 
,[City]
,[State]
,[Zip Code]
,[Country_Region Code]
,[Vendor Posting Group] 
,[Document No_] 
,[Description] 
,[Global Dimension 2 Code] 
,[Transaction No_] 
,[Source No_] 
,[Entry No_] 
,[External Document No_]
,[User ID]
,[Dimension Value Code] 
,[ProjectName]
,[Debit Amount] 
,[Credit Amount] 
,[NetTotal] 
,null
,null
,NULL
,[Dimension Set ID]
FROM @TblWorkTable
Order By
[Posting Date], Entity


------------------------------------------Check# Update-----------------------------------------------------------------
Declare @ReportDate1 DateTime
Set @ReportDate1 = (Select FirstDateOfYear from dbo.Calendar where CalendarDate = Convert (Varchar(10),GETDATE()-365,121))

--------------------------------------------JASON PHARM-----------------------------------------------------------------
DECLARE @PaymentTable TABLE (
	[Entity] [varchar](25) NULL,
	[Source Code] [varchar](10) NULL,
	[Posting Date] [datetime] NULL,
	[PaymentFor] [varchar](20) NULL,
	[Document No_] [varchar](20) NULL,
	[DaysElapsed] [numeric] (15,0) NULL
)

----JASON PHARM----
INSERT INTO @PaymentTable (
 [Entity]
,[Source Code]
,[Posting Date]
,[PaymentFor]
,[Document No_]
)      
Select
 'JasonPharm' As Entity
,[Source Code]
,[Posting Date]
,RIGHT([Description],10) as PaymentFor
,[Document No_]
From [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] (NOLOCK)
 Where [Posting Date]>= @ReportDate1
  And [Source Code] ='PAYMENTJNL'  
  And [Bal_ Account Type]='3'
  And [Description] like '%Payment of Invoice%'

---- 7 CRONDALL-----  
INSERT INTO @PaymentTable (
 [Entity]
,[Source Code]
,[Posting Date]
,[PaymentFor]
,[Document No_]
)      
Select
 '7 Crondall' As Entity
,[Source Code]
,[Posting Date]
,RIGHT([Description],8) as PaymentFor
,[Document No_]
From [NAV_ETL].[dbo].[7 Crondall$G_L Entry](NOLOCK)
Where [Posting Date]>= @ReportDate1
 And [Source Code] ='PAYMENTJNL'  
 And [Bal_ Account Type]='3' 
 And [Description] like '%Payment of Invoice%' 
  
---- Jason Enterprises-----  
INSERT INTO @PaymentTable (
 [Entity]
,[Source Code]
,[Posting Date]
,[PaymentFor]
,[Document No_]
)      
Select
 'Jason Enterprises' As Entity
,[Source Code]
,[Posting Date]
,RIGHT([Description],9) as PaymentFor
,[Document No_]
From [NAV_ETL].[dbo].[Jason Enterprises$G_L Entry](NOLOCK)
Where [Posting Date]>= @ReportDate1
 And [Source Code] ='PAYMENTJNL'  
 And [Bal_ Account Type]='3'  
 And [Description] like '%Payment of Invoice%'
 
---- Jason Properties-----  
INSERT INTO @PaymentTable (
 [Entity]
,[Source Code]
,[Posting Date]
,[PaymentFor]
,[Document No_]
)      
Select
 'JasonProperties' As Entity
,[Source Code]
,[Posting Date]
,RIGHT([Description],10) as PaymentFor
,[Document No_]
From [NAV_ETL].[dbo].[Jason Properties$G_L Entry](NOLOCK)
Where [Posting Date]>= @ReportDate1
 And [Source Code] ='PAYMENTJNL'  
 And [Bal_ Account Type]='3'  
 And [Description] like '%Payment of Invoice%'   
 
 
---- Jason Properties-----  
INSERT INTO @PaymentTable (
 [Entity]
,[Source Code]
,[Posting Date]
,[PaymentFor]
,[Document No_]
)      
Select
 'Medifast Inc' As Entity
,[Source Code]
,[Posting Date]
,RIGHT([Description],8) as PaymentFor
,[Document No_]
From [NAV_ETL].[dbo].[Medifast Inc$Vendor Ledger Entry](NOLOCK)
Where [Posting Date]>= @ReportDate1
 And [Source Code] ='PAYMENTJNL'  
 And [Bal_ Account Type]='3'  
 And [Description] like '%Payment of Invoice%'
 
 
---- Jason Properties-----  
INSERT INTO @PaymentTable (
 [Entity]
,[Source Code]
,[Posting Date]
,[PaymentFor]
,[Document No_]
)      
Select
 'TSFL' As Entity
,[Source Code]
,[Posting Date]
,RIGHT([Description],9) as PaymentFor
,[Document No_]
From [NAV_ETL].[dbo].[Take Shape For Life$Vendor Ledger Entry](NOLOCK)
Where [Posting Date]>= @ReportDate1
 And [Source Code] ='PAYMENTJNL'  
 And [Bal_ Account Type]='3'  
 and [Description] like '%Payment of Invoice%'
 
 ---- Medifast Nutrition-----  Added New Medifast Nutrition as per Jack request 06/02/2014
INSERT INTO @PaymentTable (
 [Entity]
,[Source Code]
,[Posting Date]
,[PaymentFor]
,[Document No_]
)      
Select
 'Medifast Nutrition' As Entity
,[Source Code]
,[Posting Date]
,Ltrim(RIGHT([Description],9))as PaymentFor  -----Ltrim used for remove extra space
,[Document No_]
From [NAV_ETL].[dbo].[Medifast Nutrition$Vendor Ledger Entry](NOLOCK)
Where [Posting Date]>= @ReportDate1
 And [Source Code] ='PAYMENTJNL'  
 And [Bal_ Account Type]='3'  
 and [Description] like '%Payment of Invoice%'
  
UPDATE [dbo].[XL_015_Monthly_AP_ALL]
SET Check# = A.[Document No_], CheckDate = A.[Posting Date]
FROM @PaymentTable A
WHERE [dbo].[XL_015_Monthly_AP_ALL].Entity COLLATE DATABASE_DEFAULT= A.Entity
    AND [dbo].[XL_015_Monthly_AP_ALL].[Document No_] COLLATE DATABASE_DEFAULT= A.PaymentFor
    
UPDATE [dbo].[XL_015_Monthly_AP_ALL] 
Set [DaysElasped]  =   DATEDIFF(dd,[document date],[checkdate])

 
   





