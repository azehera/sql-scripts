﻿
CREATE PROCEDURE [dbo].[USP_XL_079_Health_Coaches_existing_testing] as

DECLARE @Sql1 varchar(2500)
DECLARE @Sql2 varchar(2500)
DECLARE @String varchar(200)
DECLARE @StartDate date
DECLARE @EndDate date
DECLARE @StartCustId numeric(9,0)
DECLARE @CustId numeric(9,0)
DECLARE @FirstName varchar(50)
DECLARE @LastName varchar(50)
DECLARE @HighRank varchar(50)
DECLARE @DoneFlag integer
SET @DoneFlag = 0

select @EndDate = DATEADD(SECOND, 1, COMMISSION_PERIOD) 
from openquery(Odyssey, 'select MAX(COMMISSION_PERIOD) COMMISSION_PERIOD FROM tsfl_odsy1.COMMISSION_PERIOD where PERIOD_TYPE = ''DEFAULT'' order by COMMISSION_PERIOD desc')
set @StartDate = DATEADD(MONTH, -1, @EndDate)

IF object_id('Tempdb..#CoAppNames') Is Not Null DROP TABLE #CoAppNames
CREATE TABLE #CoAppNames (CUSTOMER_ID numeric(9,0), FIRST_NAME varchar(50), MIDDLE_NAME varchar(50), LAST_NAME varchar(50))

Set @Sql1 = 'select CUSTOMER_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME '
set @Sql1 = @Sql1 + 'from tsfl_odsy1.CO_APPLICANT where IS_OBSOLETE IS NULL order by CUSTOMER_ID '
set @Sql2 = 'insert into #CoAppNames select * from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') '
EXEC(@Sql2)

IF object_id('Tempdb..#CoApps') Is Not Null DROP TABLE #CoApps
CREATE TABLE #CoApps (CustId numeric(9,0), CoAppNames varchar(150))

insert into #CoApps
select CUSTOMER_ID, STUFF((SELECT ',', + CASE WHEN ISNULL([MIDDLE_NAME],'') = '' THEN ISNULL([FIRST_NAME],'') + ' ' + ISNULL([LAST_NAME],'')
ELSE ISNULL([FIRST_NAME],'') + ' ' + ISNULL([MIDDLE_NAME],'') + ' ' + ISNULL([LAST_NAME],'') END 
FROM #CoAppNames WHERE (CUSTOMER_ID = List.CUSTOMER_ID) FOR XML PATH ('')),1,1,'') AS CoApps
FROM #CoAppNames AS List GROUP BY CUSTOMER_ID ORDER BY CUSTOMER_ID

IF object_id('Tempdb..#Rankups') Is Not Null DROP TABLE #Rankups
CREATE TABLE #Rankups (CUSTOMER_ID numeric(9,0), LastRankUpDate varchar(10))

Set @Sql1 = 'select a.CUSTOMER_ID, MAX(TO_CHAR(c.COMMISSION_PERIOD, ''yyyy-mm-dd'')) '
Set @Sql1 = @Sql1 + 'from tsfl_odsy1.CUSTOMER a inner join tsfl_odsy1.BUSINESSCENTER c on a.CUSTOMER_ID = c.CUSTOMER_ID where '
set @Sql1 = @Sql1 + 'RANK_ADVANCE_DATE = COMMISSION_PERIOD and HIGHEST_ACHIEVED_RANK not in (''None'', ''HEALTH_COACH'', ''FT_HEALTH_COACH'') '
Set @Sql1 = @Sql1 + 'group by a.CUSTOMER_ID '
set @Sql2 = 'insert into #Rankups select * from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') '
EXEC(@Sql2)

IF object_id('Tempdb..#Upline') Is Not Null DROP TABLE #Upline
CREATE TABLE #Upline (CustomerId numeric(9,0), LastName varchar(50), FirstName varchar(50), HighRank varchar(50))

set @Sql1 = 'SELECT distinct c.CUSTOMER_ID, c.LAST_NAME, c.FIRST_NAME FROM tsfl_odsy1.CUSTOMER c '
set @Sql1 = @Sql1 + 'left join tsfl_odsy1.CUSTOMER_STATUS cs on c.CUSTOMER_ID = cs.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'left join tsfl_odsy1.BUSINESSCENTER b1 on c.CUSTOMER_ID = b1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'left join tsfl_odsy1.BUSINESSCENTER b2 on b1.SPONSOR_ID = b2.BUSINESSCENTER_ID '
set @Sql1 = @Sql1 + 'where cs.STATUS_ID = ''ACTIV'' and c.CUSTOMER_TYPE in (''C'', ''D'', ''M'') '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD > ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and b2.COMMISSION_PERIOD > ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and b2.COMMISSION_PERIOD < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and NOT CAST(c.CUSTOMER_NUMBER as integer) <= 2002 '
set @Sql1 = @Sql1 + 'and NOT CAST(c.CUSTOMER_NUMBER as integer) in (6957001, 16918401, 16918501, 21283101, '
set @Sql1 = @Sql1 + '30006445, 30015603, 30046536, 30050745, 30191394, 701405694, 744720823, 753300661, 754336921, '
set @Sql1 = @Sql1 + '754337087, 754352568, 754363836, 754364127, 760636249, 760637075) order by c.CUSTOMER_ID'
set @Sql2 = 'DECLARE CustomerCursor CURSOR FOR select ody.* from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody '
EXEC(@Sql2)
	
OPEN CustomerCursor;
FETCH NEXT FROM CustomerCursor INTO @CustId, @LastName, @FirstName
Set @StartCustId = @CustId
WHILE @@FETCH_STATUS = 0
BEGIN
WHILE @DoneFlag = 0
-- Spin thru the coaches genealogy looking for the qualifying ranks
BEGIN
IF object_id('Tempdb..#OneRankup') Is Not Null DROP TABLE #OneRankup
CREATE TABLE #OneRankup (CustId numeric(9,0), FirstName varchar(50), LastName varchar(50), HighRank varchar(50))

set @Sql1 = 'select distinct c2.CUSTOMER_ID, c2.LAST_NAME, c2.FIRST_NAME, b2.HIGHEST_ACHIEVED_RANK '
set @Sql1 = @Sql1 + 'from tsfl_odsy1.BUSINESSCENTER b1 inner join tsfl_odsy1.CUSTOMER c1 on b1.CUSTOMER_ID = c1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.BUSINESSCENTER b2 on b1.PARENT_BC = b2.BUSINESSCENTER_ID and b1.COMMISSION_PERIOD = b2.COMMISSION_PERIOD '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER c2 on b2.CUSTOMER_ID = c2.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'inner join tsfl_odsy1.CUSTOMER_STATUS cs on cs.CUSTOMER_ID = c1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD > ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'and b1.COMMISSION_PERIOD < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql1 = @Sql1 + 'where cs.STATUS_ID = ''ACTIV'' and c1.CUSTOMER_ID = ' + CONVERT(varchar(10), @CustId)
set @Sql2 = 'insert into #OneRankup select ody.CUSTOMER_ID, ody.LAST_NAME, ody.FIRST_NAME, ody.HIGHEST_ACHIEVED_RANK '
set @Sql2 = @Sql2 + 'from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody '
EXEC(@Sql2)
Select @CustId = CustId, @LastName = LastName, @FirstName = FirstName, @HighRank = HighRank from #OneRankup

IF @HighRank in ('Global_Director', 'Presidential_Director','FI_Global_Director','FI_Presidential_Director') Set @DoneFlag = 1
If @FirstName = 'Removed' and @LastName = 'Clients' Set @DoneFlag = 1
IF @DoneFlag = 1
BEGIN
	INSERT INTO #Upline SELECT @StartCustId, @LastName, @FirstName, @HighRank
--	Set @String = 'Inserted Into #Upline: @StartCust = ' + Convert(varchar(20), @StartCustId) + ', First = ' + @FirstName  + ', Last = ' + @LastName + ', Rank = ' + @HighRank
--	RAISERROR(@String, 0, 1) WITH NOWAIT
END
END
FETCH NEXT FROM CustomerCursor INTO @CustId, @LastName, @FirstName
--Set @String = '@@FETCH_STATUS: ' + CONVERT(varchar(10), @@FETCH_STATUS)
--RAISERROR(@String, 0, 1) WITH NOWAIT
Set @StartCustId = @CustId
Set @DoneFlag = @@FETCH_STATUS
END
CLOSE CustomerCursor
DEALLOCATE CustomerCursor

IF object_id('Tempdb..#FirstResult') Is Not Null DROP TABLE #FirstResult
CREATE TABLE #FirstResult (CUSTOMER_ID numeric(9,0), [UserName] varchar(20), [Password] varchar(10), [Health Coach ID] varchar(20), [First Name] varchar(50), [Last Name] varchar(50), 
[Legal Name] varchar(100), [Email Address] varchar(200), [Phone] varchar(50), [Mailing Address Line 1] varchar(150), [Mailing Address Line 2] varchar(150), [City] varchar(50), 
[State] varchar(5), [Zip Code] varchar(20), [Current Rank] varchar(50), [Highest Achieved Rank] varchar(50), [Sponsor Name] varchar(100), [Certification Status] varchar(10), 
[Member Since] varchar(20), [Co-Applicant Name] varchar(200), [First Upline Global Name] varchar(100), [Last Rank-Up Date] varchar(20))

set @Sql1 = 'select distinct c.CUSTOMER_ID, '
set @Sql1 = @Sql1 + 'c.CUSTOMER_NUMBER As UserName, '
set @Sql1 = @Sql1 + '''TSFL123'' As Password, '
set @Sql1 = @Sql1 + 'c.CUSTOMER_NUMBER As HealthCoachId, '
set @Sql1 = @Sql1 + 'NVL(c.FIRST_NAME,'' '') As FirstName, '
set @Sql1 = @Sql1 + 'NVL(c.LAST_NAME,'' '') As LastName, '
set @Sql1 = @Sql1 + 'NVL(c.DISTRIBUTORSHIP_NAME,'' '') As LegalName, '
set @Sql1 = @Sql1 + 'c.EMAIL1_ADDRESS As EmailAddress, '
set @Sql1 = @Sql1 + 'to_char(REPLACE(REPLACE(REPLACE(REPLACE(c.MAIN_PHONE, ''('', ''''), '')'', ''''), ''-'', ''''),'' '','''')) As Phone, '
set @Sql1 = @Sql1 + 'NVL(a.ADDRESS1, '' '') As MailingAddressLine1, '
set @Sql1 = @Sql1 + 'NVL(a.ADDRESS2, '' '') As MailingAddressLine2, '
set @Sql1 = @Sql1 + 'NVL(a.CITY, '' '') As City, '
set @Sql1 = @Sql1 + 'NVL(a.STATE, '' '') As State, '
set @Sql1 = @Sql1 + 'NVL(a.POSTAL_CODE, '' '') As ZipCode, '
set @Sql1 = @Sql1 + 'b1.RANK As CurrentRank, '
set @Sql1 = @Sql1 + 'b1.HIGHEST_ACHIEVED_RANK As HighestAchievedRank, '
set @Sql1 = @Sql1 + 'CASE WHEN s.MIDDLE_NAME IS NULL THEN s.FIRST_NAME || '' '' || s.LAST_NAME '
set @Sql1 = @Sql1 + 'ELSE s.FIRST_NAME || '' '' || s.MIDDLE_NAME || '' '' || s.LAST_NAME End As SponsorName, '
set @Sql1 = @Sql1 + 'CASE cs.CERTIFIED WHEN 1 THEN ''Yes'' ELSE ''No'' END As CertificationStatus, '
set @Sql1 = @Sql1 + 'to_char(NVL(c.EFFECTIVE_DATE, '''')) As MemberSince '
set @Sql1 = @Sql1 + 'from tsfl_odsy1.CUSTOMER c '
set @Sql1 = @Sql1 + 'left join tsfl_odsy1.CUSTOMER_STATUS cs '
set @Sql1 = @Sql1 + 'on c.CUSTOMER_ID = cs.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'left join tsfl_odsy1.CUSTOMER_ADDRESS a '
set @Sql1 = @Sql1 + 'on c.CUSTOMER_ID = a.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'left join tsfl_odsy1.BUSINESSCENTER b1 '
set @Sql1 = @Sql1 + 'on c.CUSTOMER_ID = b1.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'left join tsfl_odsy1.BUSINESSCENTER b2 '
set @Sql1 = @Sql1 + 'on b1.SPONSOR_ID = b2.BUSINESSCENTER_ID '
set @Sql1 = @Sql1 + 'left join tsfl_odsy1.CUSTOMER s '
set @Sql1 = @Sql1 + 'on b2.CUSTOMER_ID = s.CUSTOMER_ID '
set @Sql1 = @Sql1 + 'WHERE NOT to_number(c.CUSTOMER_NUMBER) <= 2002 AND NOT to_number(c.CUSTOMER_NUMBER) in (6957001, 16918401, 16918501, '
set @Sql1 = @Sql1 + '21283101, 30006445, 30015603, 30046536, 30050745, 30191394, 701405694, 744720823, 753300661, 754336921, 754337087, '
set @Sql1 = @Sql1 + '754352568, 754363836, 754364127, 760636249, 760637075) AND cs.STATUS_ID = ''ACTIV'' and c.CUSTOMER_TYPE in (''C'', ''D'', ''M'') '
set @Sql1 = @Sql1 + 'AND a.ADDRESS_TYPE = ''MAIN'' and a.LANGUAGE_CODE = ''ENGLISH'' '
set @Sql1 = @Sql1 + 'AND b1.COMMISSION_PERIOD >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'AND b1.COMMISSION_PERIOD < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql1 = @Sql1 + 'AND b2.COMMISSION_PERIOD >= ''' + CONVERT(varchar(10), @StartDate, 120) + ''' '
set @Sql1 = @Sql1 + 'AND b2.COMMISSION_PERIOD < ''' + CONVERT(varchar(10), @EndDate, 120) + ''' '
set @Sql2 = 'insert into #FirstResult '
set @Sql2 = @Sql2 + 'select ody.*, ISNULL(co.CoAppNames, '''') As CoApplicantName, '
set @Sql2 = @Sql2 + 'u.FirstName + '' '' + u.LastName As FirstUplineGlobalName, '
set @Sql2 = @Sql2 + 'ISNULL(r.LastRankUpDate, '''') As LastRankUpDate '
set @Sql2 = @Sql2 + 'from openquery(Odyssey, ''' + REPLACE(@Sql1, '''', '''''') + ''') ody '
set @Sql2 = @Sql2 + 'left join #CoApps co '
set @Sql2 = @Sql2 + 'on ody.CUSTOMER_ID = co.CustId '
set @Sql2 = @Sql2 + 'left join #Rankups r '
set @Sql2 = @Sql2 + 'on ody.CUSTOMER_ID = r.CUSTOMER_ID '
set @Sql2 = @Sql2 + 'left join #Upline u '
set @Sql2 = @Sql2 + 'on ody.CUSTOMER_ID = u.CustomerId '
set @Sql2 = @Sql2 + 'order by ISNULL(ody.LastName,''''), ISNULL(ody.FirstName,'''') '

EXEC(@Sql2)

select f.[UserName], f.[Password], f.[Health Coach ID], f.[First Name], f.[Last Name], f.[Legal Name], f.[Email Address], 
f.[Co-Applicant Name], f.Phone, f.[Mailing Address Line 1], f.[Mailing Address Line 2], f.[City], f.[State], 
CASE WHEN LEN(f.[Zip Code]) < 6 THEN RIGHT('0' + f.[Zip Code], 5) ELSE f.[Zip Code] END [Zip Code], f.[Current Rank], 
f.[Highest Achieved Rank], Convert(varchar(10), f.[Last Rank-Up Date], 101) [Last Rank-Up Date], f.[Sponsor Name],
f.[First Upline Global Name], f.[Certification Status], Convert(varchar(10), f.[Member Since], 101) [Member Since]
into ##testcoach
from #FirstResult f order by f.[Last Name], f.[First Name]

