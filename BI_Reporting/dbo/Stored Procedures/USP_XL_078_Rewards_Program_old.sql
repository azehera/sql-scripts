﻿
/*
=======================================================================================
Author:         
Created date: 
-------------------------[usp_XL_078_Rewards_Program] -------------------------
--
=======================================================================================   
REFERENCES
Database                     Table/View/UDF							Action            
---------------------------------------------------------------------------------------
prdmartini_STORE_repl     dbo.ACCOUNT								Select
prdmartini_STORE_repl     dbo.ACCOUNT_ACTIVITY					Select
prdmartini_STORE_repl     dbo.PAYMENT								Select
prdmartini_STORE_repl     dbo.V_ORDER_HEADER						Select
prdmartini_STORE_repl     dbo.ORGANIZATION						Select
prdmartini_STORE_repl		dbo.USER_ORG							Select 
[BI_Reporting]              dbo.XL_078_Rewards_Program				Insert


========================================================================================                 
REVISION LOG
Date           Name              Change
----------------------------------------------------------------------------------------
1/22/2013      Menkir Haile		The select statement is removed
--

========================================================================================
*/
CREATE PROCEDURE [dbo].[USP_XL_078_Rewards_Program_old] AS

DECLARE @StartDate Date
DECLARE @EndDate Date
DECLARE @CloseBal money
DECLARE @CursorTranDate Date
DECLARE @CursorChannel varchar(20)
SET @StartDate = '08/01/2014'	-- Startup Date For The Rewards Program
SET @EndDate = '01/01/2026'

-- Notes: ACT_TYPE_CD contains 'ASR' while ACA_TYPE_CD contains 'DBT' or 'CRT'

-- Earned rewards order numbers
IF OBJECT_ID('tempdb..#OrderChannel') IS NOT NULL DROP TABLE #OrderChannel
CREATE TABLE #OrderChannel (OrderNo varchar(20), Channel varchar(20))

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACT_NOTES, 'Rewards earned on order:', '')) [OrderNo], '' [Channel] from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) where
ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
AND ISNULL(ACT_NOTES, '') LIKE 'Rewards earned on order:%' AND ACT_ORIG_BALANCE > 0

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACT_NOTES, 'Rtn Order:', '')) [OrderNo], '' [Channel] from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) where
ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACT_NOTES, '') LIKE 'Rtn Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'Rtn Order:', '')) [OrderNo], '' [Channel] 
from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) on ACA_ACT_ID = ACT_ID 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Rtn Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'Canc Order:', '')) [OrderNo], '' [Channel] 
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate and ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') LIKE 'Canc Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACT_NOTES, 'Canc Order:', '')) [OrderNo], '' [Channel] from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) 
where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACT_NOTES, '') LIKE 'Canc Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'Rwd on Order:', '')) [OrderNo], '' [Channel] 
from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) on ACA_ACT_ID = ACT_ID 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Rwd on Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'Reward on Order:', '')) [OrderNo], '' [Channel] 
from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) on ACA_ACT_ID = ACT_ID 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Reward on Order:%'

INSERT INTO #OrderChannel
SELECT LTRIM(REPLACE(ACA_NOTES, 'CC_TXT_REWARD_ON_ORDER', '')) [OrderNo], '' [Channel] 
from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) on ACA_ACT_ID = ACT_ID 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'CC_TXT_REWARD_ON_ORDER%'

INSERT INTO #OrderChannel
SELECT ORH_SOURCE_NBR [OrderNo], '' [Channel]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
inner join prdmartini_STORE_repl.dbo.PAYMENT (nolock) on ACA_PAY_ID = PAY_ID inner join prdmartini_STORE_repl.dbo.V_ORDER_HEADER (nolock) on PAY_ORH_ID = ORH_ID
left join #OrderChannel c on ORH_SOURCE_NBR = c.OrderNo
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate and ACT_TYPE_CD = 'ASR' and NOT ACA_PAY_ID IS NULL and c.OrderNo IS NULL

INSERT INTO #OrderChannel
SELECT 'NONE', 'TSFL'

-- Update #OrderChannel with Channel
update #OrderChannel set Channel = o.ORG_ACCOUNT_NBR  
from #OrderChannel c inner join prdmartini_STORE_repl.dbo.V_ORDER_HEADER h (nolock) on c.OrderNo = h.ORH_SOURCE_NBR
inner join prdmartini_STORE_repl.dbo.ORGANIZATION o on h.ORH_ORG_ID = o.ORG_ID

IF OBJECT_ID('tempdb..#AccountChannel') IS NOT NULL DROP TABLE #AccountChannel
CREATE TABLE #AccountChannel (AccountId bigint, Channel varchar(20))
INSERT INTO #AccountChannel 
select a.ACT_ID, org.ORG_ACCOUNT_NBR from prdmartini_STORE_repl.dbo.ACCOUNT a 
left join prdmartini_STORE_repl.dbo.V_USER_ACCOUNT ua on a.ACT_OWNED_BY = ua.USA_ID
left join prdmartini_STORE_repl.dbo.USER_ORG uso with (nolock) on uso.USO_ID = ua.USA_DEFAULT_USO_ID 
left join prdmartini_STORE_repl.dbo.ORGANIZATION org with (nolock) on org.org_id = uso.uso_org_id
where org.ORG_ACCOUNT_NBR in ('MEDIFAST', 'TSFL')

IF OBJECT_ID('tempdb..#Trans') IS NOT NULL DROP TABLE #Trans
CREATE TABLE #Trans (Channel varchar(20), TranDate varchar(10), OrderNo varchar(20), OrderCount int, Category varchar(20), TranAmt money)

-- Earned rewards - Column Name = RewardEarned
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACT_NOTES, 'Rewards earned on order:', '')) [OrderNo], 
1 [OrderCount], 'RewardEarned' [Category], ACT_ORIG_BALANCE [TranAmt] from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) where
ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
AND ISNULL(ACT_NOTES, '') LIKE 'Rewards earned on order:%' AND ACT_ORIG_BALANCE > 0

-- Earned rewards - Column Name = RewardEarned
INSERT INTO #Trans
SELECT c.Channel [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], 'Missing' [OrderNo], 
1 [OrderCount], 'RewardEarned' [Category], ACT_ORIG_BALANCE [TranAmt] 
from prdmartini_STORE_repl.dbo.ACCOUNT a (nolock) inner join #AccountChannel c on a.ACT_ID = c.AccountId 
where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
AND ISNULL(ACT_NOTES, '') = '' AND dbo.udf_isRewardAccount (ACT_ID) = 'Y' AND ACT_ORIG_BALANCE > 0

-- Earned rewards - Column Name = RewardEarned
--for Double Rewards
INSERT INTO #Trans
SELECT c.Channel [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], 'Missing' [OrderNo], 
1 [OrderCount], 'RewardEarned' [Category], ACT_ORIG_BALANCE [TranAmt] 
from prdmartini_STORE_repl.dbo.ACCOUNT a (nolock) inner join #AccountChannel c on a.ACT_ID = c.AccountId 
where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
AND ISNULL(ACT_NOTES, '') LIKE 'Double Rewards%' 
--AND dbo.udf_isRewardAccount (ACT_ID) = 'Y' 
AND ACT_ORIG_BALANCE > 0

--Added by D.Smith 12/1/2016 - to pickup double rewarsd from Black Friday 2016
--for Double Rewards from Black Friday 2016
INSERT INTO #Trans
SELECT c.Channel [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], 'Missing' [OrderNo], 
1 [OrderCount], 'RewardEarned' [Category], ACT_ORIG_BALANCE [TranAmt] 
from prdmartini_STORE_repl.dbo.ACCOUNT a (nolock) inner join #AccountChannel c on a.ACT_ID = c.AccountId 
where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
AND ISNULL(ACT_NOTES, '') LIKE 'Black Friday Rewards%' 
--AND dbo.udf_isRewardAccount (ACT_ID) = 'Y' 
AND ACT_ORIG_BALANCE > 0

---- Earned rewards - Column Name = RewardEarned
----For the Reversal of Double Rewards
--INSERT INTO #Trans
--SELECT c.Channel [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], 'Missing' [OrderNo], 
--1 [OrderCount], 'RewardEarned' [Category], ACT_ORIG_BALANCE [TranAmt] 
--from prdmartini_STORE_repl.dbo.ACCOUNT a (nolock) inner join #AccountChannel c on a.ACT_ID = c.AccountId 
--where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' 
--AND ISNULL(ACT_NOTES, '') LIKE 'Reversal of Double Rewards%' AND dbo.udf_isRewardAccount (ACT_ID) = 'Y' AND ACT_ORIG_BALANCE > 0

-- Applied rewards taken back for returned orders - Column Name = ReclaimedReturn
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'Rtn Order:', '')) [OrderNo],
0 [OrderCount], 'ReclaimedReturn' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID where
ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Rtn Order:%'

-- Balances created today for reclaimed rewards that could not be subtracted from existing rewards accounts - Column Name = UnReclaimedReturn
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACT_NOTES, 'Rtn Order:', '')) [OrderNo],
0 [OrderCount], 'UnReclaimedReturn' [Category], ACT_ORIG_BALANCE [TranAmt] from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) where
ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACT_NOTES, '') LIKE 'Rtn Order:%' AND ACT_ORIG_BALANCE < 0

-- Applied rewards that were taken back for cancelled orders - Column Name = Cancelled
INSERT INTO #Trans
SELECT '' [Channel], CONVERT (varchar(10), ACT_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACT_NOTES, 'Rewards applied to cancelled order:', '')) [OrderNo],
0 [OrderCount], 'Cancelled' [Category], 0 - ACT_ORIG_BALANCE [TranAmt] from prdmartini_STORE_repl.dbo.ACCOUNT (nolock)
where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACT_NOTES, '') LIKE 'Rewards applied to cancelled order%'

-- Amounts removed from rewards accounts due to cancellations - Column Name = ReclaimedCancel
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'Canc Order:', '')) [OrderNo],
0 [OrderCount], 'ReclaimedCancel' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' AND ISNULL(ACA_NOTES, '') LIKE 'Canc Order:%'

-- Balances created because rewards for a cancelled order could not be fully applied to the rewards account - Column Name = UnReclaimedCancel
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACT_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACT_NOTES, 'Canc Order:', '')) [OrderNo],
0 [OrderCount], 'UnReclaimedCancel' [Category], ACT_ORIG_BALANCE [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT (nolock) where ACT_CREATE_DT >= @StartDate AND ACT_CREATE_DT < @EndDate and ACT_TYPE_CD = 'ASR' and ACT_ORIG_BALANCE < 0 
and ISNULL(ACT_NOTES, '') LIKE 'Canc Order:%'

-- Wipe out any remaining referral credit account balances - Column Name = Eliminate
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], 'NONE' [OrderNo], 
0 [OrderCount], 'Eliminate' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') = 'Elim Ref Cred Bal'

-- Forfeited rewards - Column Name = Forfeited
INSERT INTO #Trans
SELECT ac.Channel, CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], '' [OrderNo], 
0 [OrderCount], 'Forfeited' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
left join #AccountChannel ac on ACT_ID = ac.AccountId 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and (ISNULL(ACA_NOTES, '') = 'Unused rewards' OR
ISNULL(ACA_NOTES, '') = 'CC_TXT_UNUSED_REWARDS' or ISNULL(ACA_NOTES, '') = 'Expired rewards' OR ISNULL(ACA_NOTES, '') = 'Expired Black Friday Rewards' )

-- Forfeited rewards REVERSED - Column Name = Forfeited
INSERT INTO #Trans
SELECT ac.Channel, CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], '' [OrderNo], 
0 [OrderCount], 'Forfeited' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
left join #AccountChannel ac on ACT_ID = ac.AccountId 
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') = 'Reverse Unused Rewards'

-- Apply rewards to any negative rewards - Column Name = OffsetNegReward - Method 1
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'Rwd on Order:', '')) [OrderNo],
0 [OrderCount], 'OffsetNegReward' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') LIKE 'Rwd on Order:%'

-- Apply rewards to any negative rewards - Column Name = OffsetNegReward - Method 2
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'Reward on Order:', '')) [OrderNo],
0 [OrderCount], 'OffsetNegReward' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') LIKE 'Reward on Order:%'

-- Apply rewards to any negative rewards - Column Name = OffsetNegReward - Method 3
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], LTRIM(REPLACE(ACA_NOTES, 'CC_TXT_REWARD_ON_ORDER', '')) [OrderNo],
0 [OrderCount], 'OffsetNegReward' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate AND ACT_TYPE_CD = 'ASR' and ISNULL(ACA_NOTES, '') LIKE 'CC_TXT_REWARD_ON_ORDER%'

-- Rewards used - Column Name = RewardUsed
INSERT INTO #Trans
SELECT '' [Channel], CONVERT(varchar(10), ACA_CREATE_DT, 101) [TranDate], ORH_SOURCE_NBR [OrderNo],
0 [OrderCount], 'RewardUsed' [Category], CASE ACA_TYPE_CD WHEN 'DBT' THEN 0 - ACA_ADJUST_AMT WHEN 'CRT' THEN ACA_ADJUST_AMT ELSE 0 END [TranAmt]
from prdmartini_STORE_repl.dbo.ACCOUNT_ACTIVITY (nolock) inner join prdmartini_STORE_repl.dbo.ACCOUNT (nolock) on ACA_ACT_ID = ACT_ID
inner join prdmartini_STORE_repl.dbo.PAYMENT (nolock) on ACA_PAY_ID = PAY_ID inner join prdmartini_STORE_repl.dbo.V_ORDER_HEADER (nolock) on PAY_ORH_ID = ORH_ID
where ACA_CREATE_DT >= @StartDate AND ACA_CREATE_DT < @EndDate and ACT_TYPE_CD = 'ASR' and NOT ACA_PAY_ID IS NULL

-- Update Channel for all #Trans rows
update #Trans set Channel = o.Channel from #Trans t inner join #OrderChannel o on t.OrderNo = o.OrderNo

IF OBJECT_ID('tempdb..#TransCounts') IS NOT NULL DROP TABLE #TransCounts
create table #TransCounts (Channel varchar(20), TranDate varchar(10), OrderCount int)
insert into #TransCounts
select Channel, TranDate, sum(OrderCount) from #Trans group by Channel, TranDate

IF OBJECT_ID('tempdb..#TransPivot') IS NOT NULL DROP TABLE #TransPivot
CREATE TABLE #TransPivot (Channel varchar(20), TranDate varchar(10), Category varchar(20), TranAmt money)
insert into #TransPivot
select Channel, TranDate, Category, TranAmt from #Trans

IF OBJECT_ID('tempdb..#Amounts') IS NOT NULL DROP TABLE #Amounts
create table #Amounts (Channel varchar(20), TranDate Date, RewardEarned money, RewardUsed money, Forfeited money, ReclaimedReturn money, 
UnReclaimedReturn money, ReclaimedCancel money, UnReclaimedCancel money, Cancelled money, Eliminate money, OffsetNegReward money)

INSERT INTO #Amounts
SELECT * FROM #TransPivot
PIVOT (SUM([TranAmt]) FOR Category IN ([RewardEarned], [RewardUsed], [Forfeited], [ReclaimedReturn], 
[UnReclaimedReturn], [ReclaimedCancel], [UnReclaimedCancel], [Cancelled], [Eliminate], [OffsetNegReward])) AS DistAmounts
ORDER BY Channel, TranDate

ALTER TABLE #Amounts ADD BeginBalance MONEY, NetChange MONEY, ClosingBalance MONEY

UPDATE #Amounts SET RewardEarned = ISNULL(RewardEarned,0), RewardUsed = ISNULL(RewardUsed,0), 
Forfeited = ISNULL(Forfeited,0), ReclaimedReturn = ISNULL(ReclaimedReturn,0), 
UnReclaimedReturn = ISNULL(UnReclaimedReturn,0), ReclaimedCancel = ISNULL(ReclaimedCancel,0), 
UnReclaimedCancel = ISNULL(UnReclaimedCancel,0), Cancelled = ISNULL(Cancelled,0), 
Eliminate = ISNULL(Eliminate, 0), OffsetNegReward = ISNULL(OffsetNegReward, 0)

UPDATE #Amounts SET BeginBalance = 0, ClosingBalance = 0

UPDATE #Amounts SET [NetChange] = [RewardEarned]+[RewardUsed]+[Forfeited]+[ReclaimedReturn]+
[UnReclaimedReturn]+[ReclaimedCancel]+[UnReclaimedCancel]+[Cancelled]+[Eliminate]+[OffsetNegReward]

DECLARE AmountsCursor CURSOR FOR SELECT Channel, TranDate FROM #Amounts ORDER BY Channel, TranDate
OPEN AmountsCursor
FETCH NEXT FROM AmountsCursor INTO @CursorChannel, @CursorTranDate
WHILE @@FETCH_STATUS = 0
BEGIN
  SELECT @CloseBal = [BeginBalance]+[RewardEarned]+[RewardUsed]+[Forfeited]+[ReclaimedReturn]+
  [UnReclaimedReturn]+[ReclaimedCancel]+[UnReclaimedCancel]+[Cancelled]+[Eliminate]+[OffsetNegReward]
  FROM #Amounts a WHERE [Channel] = @CursorChannel AND [TranDate] = @CursorTranDate
  UPDATE a SET [ClosingBalance] = @CloseBal FROM #Amounts a WHERE [Channel] = @CursorChannel AND [TranDate] = @CursorTranDate
  PRINT @CursorChannel + ', ' + CAST(@CursorTranDate AS VARCHAR(10)) + ', ' + CAST(@CloseBal AS VARCHAR(20))
  UPDATE a SET [BeginBalance] = @CloseBal FROM #Amounts a WHERE [Channel] = @CursorChannel AND [TranDate] = DATEADD(d, 1, @CursorTranDate)
  FETCH NEXT FROM AmountsCursor INTO @CursorChannel, @CursorTranDate
END

IF OBJECT_ID('tempdb..#OrderAmounts') IS NOT NULL DROP TABLE #OrderAmounts
CREATE TABLE #OrderAmounts (Channel VARCHAR(20), TranDate DATE, OrderSubtotal MONEY, OrderDisc MONEY, OrderShip MONEY, OrderShipDisc MONEY, OrderTax MONEY, OrderTotal MONEY)
INSERT INTO #OrderAmounts
SELECT t.Channel, t.TranDate, SUM(h.ORH_SUBTOTAL_AMT), SUM(h.ORH_DISCOUNT_AMT), SUM(h.ORH_TOTAL_SHIP_AMT), SUM(h.ORH_SHIP_DISC_AMT), SUM(h.ORH_TOTAL_TAX_AMT), SUM(h.ORH_TOTAL_AMT)
FROM #Trans t INNER JOIN prdmartini_STORE_repl.dbo.V_ORDER_HEADER h (NOLOCK) ON t.OrderNo = h.ORH_SOURCE_NBR WHERE t.Category = 'RewardEarned'
GROUP BY Channel, TranDate ORDER BY Channel, TranDate

TRUNCATE TABLE [BI_Reporting].dbo.[XL_078_Rewards_Program]
INSERT INTO [BI_Reporting].dbo.[XL_078_Rewards_Program]
SELECT a.Channel, a.TranDate, a.BeginBalance, a.RewardEarned, a.RewardUsed, a.Forfeited, a.ReclaimedReturn, a.UnReclaimedReturn,
a.ReclaimedCancel, a.UnReclaimedCancel, a.Cancelled, a.Eliminate, a.OffsetNegReward, a.NetChange, a.ClosingBalance,
c.OrderCount, o.OrderSubtotal, o.OrderDisc, o.OrderShip, o.OrderShipDisc, o.OrderTax, o.OrderTotal
FROM #Amounts a LEFT JOIN #TransCounts c ON a.Channel = c.Channel AND a.TranDate = c.TranDate
LEFT JOIN #OrderAmounts o ON a.Channel = o.Channel AND a.TranDate = o.TranDate ORDER BY a.Channel, a.TranDate

--select CONVERT(varchar(10), GETDATE(), 101) as [Last Refresh]

