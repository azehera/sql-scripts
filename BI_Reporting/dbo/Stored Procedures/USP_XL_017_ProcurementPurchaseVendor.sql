﻿







/*

--==================================================================================

Author:         Luc Emond

Create date: 10/25/2012

----------------------[dbo].[USP_XL_017_ProcurementPurchaseVendor]-------------------

Provide Daily Purchase order information to the Procurement Team-----

--=================================================================================    

REFERENCES

Database              Table/View/UDF                             Action            

------------------------------------------------------------------------------------

NAV-ETL         [Medifast Consolidated$Purch_ Inv_ Line]         SELECT

                [Medifast Consolidated$Purch_ Inv_ Header]       SELECT

                [7 Crondall$Purch_ Inv_ Line]                    SELECT

                [7 Crondall$Purch_ Inv_ Header]                  SELECT 

                [Jason Enterprises$Purch_ Inv_ Line]             SELECT

                [Jason Enterprises$Purch_ Inv_ Header]           SELECT

                [Jason Pharm$Purch_ Inv_ Line]                   SELECT

                [Jason Pharm$Purch_ Inv_ Header]                 SELECT

                [Jason Properties$Purch_ Inv_ Line]              SELECT

                [Jason Properties$Purch_ Inv_ Header]            SELECT

                [Medifast Inc$Purch_ Inv_ Line]                  SELECT

                [Medifast Inc$Purch_ Inv_ Header]                SELECT

                [Take Shape For Life$Purch_ Inv_ Line]           SELECT 

                [Take Shape For Life$Purch_ Inv_ Header]         SELECT 

                [Jason Pharm$G_L Account]                        SELECT          

==================================================================================

REVISION LOG

Date           Name                          Change

------------------------------------------------------------------------------------

2/4/2016       Menkir Haile				Starting Postng daate is converted to first day

										of previous year.

==================================================================================

NOTES:

------------------------------------------------------------------------------------

==================================================================================

--*/  



CREATE PROCEDURE [dbo].[USP_XL_017_ProcurementPurchaseVendor] As

SET NOCOUNT ON;



--EXECUTE AS LOGIN = 'ReportViewer'

DECLARE @PostingDate datetime

--SET	@PostingDate = '2012-01-01'----HARD CODED AS PER HERMAN. HE WOULD LIKE TO HAVE ALL DATA FROM 2012 GOING FORWARD--------

--SET @PostingDate = DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0)


set  @PostingDate = (DATEADD(MONTH, DATEDIFF(MONTH, 0,DATEADD(MONTH, -12, GETDATE())), 0))

set   @PostingDate= (DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0,  @PostingDate) +1, 0)))

IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master

-------Insert Medifast Consolidated ---------------- 

SELECT 

'Medifast Consolidated' As Entity

,b.[Posting Date]

,Datepart(YYYY,b.[Posting Date]) As CalendarYear

,Datepart (MM,b.[Posting Date]) As CalendarMonth

,Datepart (QQ,b.[Posting Date]) As CalendarQuarter

,a.[Document No_]

,a.[Line No_]

,a.[Shortcut Dimension 2 Code]

,b.[Vendor Posting Group]

,a.[Buy-from Vendor No_]

,b.[Pay-to Vendor No_]

,a.[Type]

,a.[No_]

,a.[Location Code]

,a.[Description]

,a.[Unit of Measure]

,a.[Quantity]

,a.[Line Discount %]

,a.[Line Discount Amount]

,a.[Amount]

,a.[Unit Price (LCY)]

,a.[Gen_ Bus_ Posting Group]

,b.[User ID]

,a.[Posting Group]

Into #Master

FROM [NAV_ETL].[dbo].[Medifast Consolidated$Purch_ Inv_ Line] a

Inner Join 

     [NAV_ETL].[dbo].[Medifast Consolidated$Purch_ Inv_ Header] b

On a.[Document No_] = b.[No_]

Where a.[Amount] <> 0

and b.[Posting Date]> @PostingDate



UNION ALL

---------Insert 7 Crondall Archive---------------- 

SELECT 

'7 Crondall' As Entity

,b.[Posting Date]

,Datepart(YYYY,b.[Posting Date]) As CalendarYear

,Datepart (MM,b.[Posting Date]) As CalendarMonth

,Datepart (QQ,b.[Posting Date]) As CalendarQuarter

,a.[Document No_]

,a.[Line No_]

,a.[Shortcut Dimension 2 Code]

,b.[Vendor Posting Group]

,a.[Buy-from Vendor No_]

,b.[Pay-to Vendor No_]

,a.[Type]

,a.[No_]

,a.[Location Code]

,a.[Description]

,a.[Unit of Measure]

,a.[Quantity]

,a.[Line Discount %]

,a.[Line Discount Amount]

,a.[Amount]

,a.[Unit Price (LCY)]

,a.[Gen_ Bus_ Posting Group]

,b.[User ID]

,a.[Posting Group]

FROM [NAV_ETL].[dbo].[7 Crondall$Purch_ Inv_ Line] a

Inner Join 

     [NAV_ETL].[dbo].[7 Crondall$Purch_ Inv_ Header] b

On a.[Document No_] = b.[No_]

Where a.[Amount] <> 0

and b.[Posting Date]> @PostingDate



UNION ALL

-------Insert Jason Enterprises ---------------- 

SELECT 

'Jason Enterprises' As Entity

,b.[Posting Date]

,Datepart(YYYY,b.[Posting Date]) As CalendarYear

,Datepart (MM,b.[Posting Date]) As CalendarMonth

,Datepart (QQ,b.[Posting Date]) As CalendarQuarter

,a.[Document No_]

,a.[Line No_]

,a.[Shortcut Dimension 2 Code]

,b.[Vendor Posting Group]

,a.[Buy-from Vendor No_]

,b.[Pay-to Vendor No_]

,a.[Type]

,a.[No_]

,a.[Location Code]

,a.[Description]

,a.[Unit of Measure]

,a.[Quantity]

,a.[Line Discount %]

,a.[Line Discount Amount]

,a.[Amount]

,a.[Unit Price (LCY)]

,a.[Gen_ Bus_ Posting Group]

,b.[User ID]

,a.[Posting Group]

FROM [NAV_ETL].[dbo].[Jason Enterprises$Purch_ Inv_ Line] a

Inner Join 

     [NAV_ETL].[dbo].[Jason Enterprises$Purch_ Inv_ Header] b

On a.[Document No_] = b.[No_]

Where a.[Amount] <> 0

and b.[Posting Date]> @PostingDate



UNION ALL

-------Insert Jason Pharm ---------------- 

SELECT 

'Jason Pharm' As Entity

,b.[Posting Date]

,Datepart(YYYY,b.[Posting Date]) As CalendarYear

,Datepart (MM,b.[Posting Date]) As CalendarMonth

,Datepart (QQ,b.[Posting Date]) As CalendarQuarter

,a.[Document No_]

,a.[Line No_]

,a.[Shortcut Dimension 2 Code]

,b.[Vendor Posting Group]

,a.[Buy-from Vendor No_]

,b.[Pay-to Vendor No_]

,a.[Type]

,a.[No_]

,a.[Location Code]

,a.[Description]

,a.[Unit of Measure]

,a.[Quantity]

,a.[Line Discount %]

,a.[Line Discount Amount]

,a.[Amount]

,a.[Unit Price (LCY)]

,a.[Gen_ Bus_ Posting Group]

,b.[User ID]

,a.[Posting Group]

FROM [NAV_ETL].[dbo].[Jason Pharm$Purch_ Inv_ Line] a

Inner Join 

     [NAV_ETL].[dbo].[Jason Pharm$Purch_ Inv_ Header] b

On a.[Document No_] = b.[No_]

Where a.[Amount] <> 0

and b.[Posting Date]> @PostingDate



UNION ALL

-------Insert Jason Properties ---------------- 

SELECT 

'Jason Properties' As Entity

,b.[Posting Date]

,Datepart(YYYY,b.[Posting Date]) As CalendarYear

,Datepart (MM,b.[Posting Date]) As CalendarMonth

,Datepart (QQ,b.[Posting Date]) As CalendarQuarter

,a.[Document No_]

,a.[Line No_]

,a.[Shortcut Dimension 2 Code]

,b.[Vendor Posting Group]

,a.[Buy-from Vendor No_]

,b.[Pay-to Vendor No_]

,a.[Type]

,a.[No_]

,a.[Location Code]

,a.[Description]

,a.[Unit of Measure]

,a.[Quantity]

,a.[Line Discount %]

,a.[Line Discount Amount]

,a.[Amount]

,a.[Unit Price (LCY)]

,a.[Gen_ Bus_ Posting Group]

,b.[User ID]

,a.[Posting Group]

FROM [NAV_ETL].[dbo].[Jason Properties$Purch_ Inv_ Line] a

Inner Join 

     [NAV_ETL].[dbo].[Jason Properties$Purch_ Inv_ Header] b

On a.[Document No_] = b.[No_]

Where a.[Amount] <> 0

and b.[Posting Date]> @PostingDate



UNION ALL

-------Insert Medifast Inc ---------------- 

SELECT 

'Medifast Inc' As Entity

,b.[Posting Date]

,Datepart(YYYY,b.[Posting Date]) As CalendarYear

,Datepart (MM,b.[Posting Date]) As CalendarMonth

,Datepart (QQ,b.[Posting Date]) As CalendarQuarter

,a.[Document No_]

,a.[Line No_]

,a.[Shortcut Dimension 2 Code]

,b.[Vendor Posting Group]

,a.[Buy-from Vendor No_]

,b.[Pay-to Vendor No_]

,a.[Type]

,a.[No_]

,a.[Location Code]

,a.[Description]

,a.[Unit of Measure]

,a.[Quantity]

,a.[Line Discount %]

,a.[Line Discount Amount]

,a.[Amount]

,a.[Unit Price (LCY)]

,a.[Gen_ Bus_ Posting Group]

,b.[User ID]

,a.[Posting Group]

FROM [NAV_ETL].[dbo].[Medifast Inc$Purch_ Inv_ Line] a

Inner Join 

     [NAV_ETL].[dbo].[Medifast Inc$Purch_ Inv_ Header] b

On a.[Document No_] = b.[No_]

Where a.[Amount] <> 0

and b.[Posting Date]> @PostingDate



UNION ALL

-------Insert Take Shape For Life ---------------- 

SELECT 

'Take Shape For Life' As Entity

,b.[Posting Date]

,Datepart(YYYY,b.[Posting Date]) As CalendarYear

,Datepart (MM,b.[Posting Date]) As CalendarMonth

,Datepart (QQ,b.[Posting Date]) As CalendarQuarter

,a.[Document No_]

,a.[Line No_]

,a.[Shortcut Dimension 2 Code]

,b.[Vendor Posting Group]

,a.[Buy-from Vendor No_]

,b.[Pay-to Vendor No_]

,a.[Type]

,a.[No_]

,a.[Location Code]

,a.[Description]

,a.[Unit of Measure]

,a.[Quantity]

,a.[Line Discount %]

,a.[Line Discount Amount]

,a.[Amount]

,a.[Unit Price (LCY)]

,a.[Gen_ Bus_ Posting Group]

,b.[User ID]

,a.[Posting Group]

FROM [NAV_ETL].[dbo].[Take Shape For Life$Purch_ Inv_ Line] a

Inner Join 

     [NAV_ETL].[dbo].[Take Shape For Life$Purch_ Inv_ Header] b

On a.[Document No_] = b.[No_]

Where a.[Amount] <> 0

and b.[Posting Date]> @PostingDate





----Final View-----

Select 

 a.Entity

,Convert(Varchar(10),a.[Posting Date],121) as [Posting Date]

,a.CalendarYear

,a.CalendarMonth

,a.CalendarQuarter

,a.[Document No_]

,a.[Line No_]

,a.[Shortcut Dimension 2 Code]

,a.[Vendor Posting Group]

,a.[Buy-from Vendor No_]

,a.[Pay-to Vendor No_]

,a.[Type]

,Case When a.[Type] =1 Then '[G\L]'

      When a.[Type] =2 Then '[Item]'

      When a.[Type] =4 Then '[Fixed Asset]'

      When a.[Type] =5 Then '[Item Charge]'

      Else '[Other]' End As TypeDescripton

,a.[No_]

,a.[Location Code]

,a.[Description]

,a.[Unit of Measure]

,a.Quantity

,a.[Line Discount %]

,a.[Line Discount Amount]

,a.Amount

,a.[Unit Price (LCY)]

,a.[Gen_ Bus_ Posting Group]

,a.[User ID]

,a.[Posting Group]

,ac.Name as GL_Name

,Item.[Standard Cost] 

From #Master a

Left Outer Join 

[NAV_ETL].[dbo].[Jason Pharm$G_L Account] ac

on a.[No_] COLLATE SQL_Latin1_General_CP1_CI_AS = ac.[No_] 

left join [NAV_ETL].dbo.[Jason Pharm$Item] Item

on a.[No_] COLLATE DATABASE_DEFAULT =Item.No_ 

and a.[Type]=2

Order by a.[Posting Date]












