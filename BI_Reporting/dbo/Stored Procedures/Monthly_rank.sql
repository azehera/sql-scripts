﻿
CREATE PROCEDURE [dbo].[Monthly_rank]
AS
BEGIN
WITH CTE AS(SELECT
 C.DayOfWeekName
,SUM(a.[ActualRevenue])/COUNT(c.DayOfWeekName) AS AverageLY
,0 AS AverageTy
FROM [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_LY] a
JOIN [BI_Reporting].[dbo].[Calendar] c
ON A.[Posting Date] = c.CalendarDate
GROUP BY C.DayOfWeekName

UNION ALL

SELECT
 C.DayOfWeekName
,0 AS AverageLY 
,SUM(a.[ActualRevenue])/COUNT(c.DayOfWeekName) AS AverageTY
FROM [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_TY] a
JOIN [BI_Reporting].[dbo].[Calendar] c
ON A.[Posting Date] = c.CalendarDate
GROUP BY C.DayOfWeekName)

SELECT
 CASE WHEN DayOfWeekName ='Monday' THEN 1 
      WHEN DayOfWeekName ='Tuesday' THEN 2
      WHEN DayOfWeekName ='Wednesday' THEN 3
      WHEN DayOfWeekName ='Thursday' THEN 4 
      WHEN DayOfWeekName ='Friday' THEN 5 END AS DayRank
,DayOfWeekName
,SUM(AverageLY) AS AverageLY
,SUM(AverageTY) AS AverageTY
FROM CTE
GROUP BY DayOfWeekName
ORDER BY DayRank
 END --
