﻿
/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/24/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================*/
CREATE  procedure [dbo].[USP_XL_076_Units_By_Month] as 
set nocount on ;


IF object_id('tempdb..#A') is not null DROP TABLE #A

---------------------Pulling TSFL Sales Data ------------------------------------
SELECT 
 [Posting Date]
,sum([Units]) as Units
INTO #a 
FROM [BI_Reporting].dbo.XL_016_TSFL_LY_TY
where [Posting Date] >= '01-01-2013'
and [Customer Posting Group] in ('TSFL')
Group by
[Posting Date]
order by [Posting Date] asc

---------------------------------------Aggregate to MOnthly figure------------------------
Select
	 [CalendarMonth]
	,[CalendarYear]
	,sum([Units]) as Units
From #a A
	Inner Join [BI_Reporting].[dbo].[Calendar] C
		on A.[Posting Date] = C.[CalendarDate]
Group By 
	 [CalendarYear]
	, [CalendarMonth]
Order by 
	[CalendarYear]
	, [CalendarMonth]


