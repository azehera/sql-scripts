﻿CREATE PROCEDURE [dbo].[usp_NAV_RECEIVING_Report]
    @PHDateStart DATETIME ,
    @PHDateEnd DATETIME ,
    @RHDateStart DATETIME ,
    @RHDateEnd DATETIME ,
    @RLDateStart DATETIME ,
    @RLDateEnd DATETIME ,
    @ERPLDateStart DATETIME ,
    @ERPLDateEnd DATETIME ,
    @INPG VARCHAR(10)
AS /**=================================================================================================
--Title: NAV Receiving Report
--Created: 05/13/2015
--Developer: Micah Williams
=================================================================================================**/
          
    BEGIN
	


        IF ( @INPG IS NULL )
            BEGIN
                SELECT  DISTINCT
                        PL.[No_] AS 'Item Number' ,
                        PL.Description ,
                        PL.[Posting Group] AS 'Inventory Posting Group' ,
                        PL.[Buy-from Vendor No_] AS 'Buy from Vendor No' ,
                        RH.[Order No_] AS 'Order Number' ,
                        PL.[Location Code] AS 'Location Code from Purchase Line' ,
                        PL.Quantity AS 'Quantity from Purchase Line' ,
                        RL.[Unit of Measure] AS 'Unit of Measure from Receipt Line' ,
                        PH.[Order Date] AS 'Order Date from Purchase Header' ,
                        PL.[Expected Receipt Date] AS 'Expected Receipt Date from Purchase Line' ,
                        RL.Quantity AS 'Quantity from Receipt Line' ,
                        RL.[Document No_] AS 'Purchase Receipt Number' ,
                        RL.[Posting Date] AS 'Posting Date from Receipt Header' ,
                        PL.[Outstanding Quantity] AS 'Outstanding Quantity from Purchase Line' ,
                        PL.[Quantity Received] AS 'Quantity Received from Purchase Line' ,
                        PL.[Outstanding Amount] AS 'Outstanding Amount from Purchase Line' ,
						PL.[Direct Unit Cost] * PL.[Qty_ Rcd_ Not Invoiced] AS 'Dollar Amount Received from Purchase Line',
                        PL.[Outstanding Qty_ (Base)] AS 'Outstanding Qty_ (Base) from Purchase Line' ,
                        PL.[Qty_ to Receive (Base)] AS 'Qty_ to Receive (Base) from Purchase Line' ,
                        PL.[Qty_ to Receive] AS 'Qty_ to Receive from Purchase Line' ,
                        RL.[Qty_ Rcd_ Not Invoiced] AS 'Qty_ Rcd_ Not Invoiced from Receipt Line'
                FROM    [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Purchase Line] PL ( NOLOCK )
                        JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Purchase Header] PH ( NOLOCK ) ON PL.[Document No_] = PH.No_
                        JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Purch_ Rcpt_ Header] RH ( NOLOCK ) ON RH.[Order No_] = PH.No_
                        JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Purch_ Rcpt_ Line] RL ( NOLOCK ) ON RL.[Document No_] = RH.No_
                WHERE   PL.No_ = RL.No_
                        AND PL.[No_] >= '0'
                        AND RL.Quantity > '0' -- Aydan doesnt want to pull line where RL.Quantity is 0
                        AND ( ( PH.[Order Date] BETWEEN @PHDateStart
                                                AND     @PHDateEnd )
                              OR ( RH.[Order Date] BETWEEN @RHDateStart
                                                   AND     @RHDateEnd )
                              OR ( RL.[Order Date] BETWEEN @RLDateStart
                                                   AND     @RLDateEnd )
                              OR ( PL.[Expected Receipt Date] BETWEEN @ERPLDateStart
                                                              AND
                                                              @ERPLDateEnd )
                            )
                --AND PL.[Posting Group] = @INPG
                ORDER BY PH.[Order Date] ,
                        RH.[Order No_] ,
                        RL.[Document No_] ,
                        PL.[No_] ASC
            END

        IF ( @INPG IS NOT NULL )
            SELECT  DISTINCT
                    PL.[No_] AS 'Item Number' ,
                    PL.Description ,
                    PL.[Posting Group] AS 'Inventory Posting Group' ,
                    PL.[Buy-from Vendor No_] AS 'Buy from Vendor No' ,
                    RH.[Order No_] AS 'Order Number' ,
                    PL.[Location Code] AS 'Location Code from Purchase Line' ,
                    PL.Quantity AS 'Quantity from Purchase Line' ,
                    RL.[Unit of Measure] AS 'Unit of Measure from Receipt Line' ,
                    PH.[Order Date] AS 'Order Date from Purchase Header' ,
                    PL.[Expected Receipt Date] AS 'Expected Receipt Date from Purchase Line' ,
                    RL.Quantity AS 'Quantity from Receipt Line' ,
                    RL.[Document No_] AS 'Purchase Receipt Number' ,
                    RL.[Posting Date] AS 'Posting Date from Receipt Header' ,
                    PL.[Outstanding Quantity] AS 'Outstanding Quantity from Purchase Line' ,
                    PL.[Quantity Received] AS 'Quantity Received from Purchase Line' ,
                    PL.[Outstanding Amount] AS 'Outstanding Amount from Purchase Line' ,
                    PL.[Outstanding Qty_ (Base)] AS 'Outstanding Qty_ (Base) from Purchase Line' ,
                    PL.[Qty_ to Receive (Base)] AS 'Qty_ to Receive (Base) from Purchase Line' ,
                    PL.[Qty_ to Receive] AS 'Qty_ to Receive from Purchase Line' ,
                    RL.[Qty_ Rcd_ Not Invoiced] AS 'Qty_ Rcd_ Not Invoiced from Receipt Line'
            FROM    [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Purchase Line] PL ( NOLOCK )
                    JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Purchase Header] PH ( NOLOCK ) ON PL.[Document No_] = PH.No_
                    JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Purch_ Rcpt_ Header] RH ( NOLOCK ) ON RH.[Order No_] = PH.No_
                    JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Purch_ Rcpt_ Line] RL ( NOLOCK ) ON RL.[Document No_] = RH.No_
            WHERE   PL.No_ = RL.No_
                    AND PL.[No_] >= '0'
                    AND RL.Quantity > '0' -- Aydan doesnt want to pull line where RL.Quantity is 0
                    AND ( ( PH.[Order Date] BETWEEN @PHDateStart
                                            AND     @PHDateEnd )
                          OR ( RH.[Order Date] BETWEEN @RHDateStart
                                               AND     @RHDateEnd )
                          OR ( RL.[Order Date] BETWEEN @RLDateStart
                                               AND     @RLDateEnd )
                          OR ( PL.[Expected Receipt Date] BETWEEN @ERPLDateStart
                                                          AND @ERPLDateEnd )
                        )
                    AND PL.[Posting Group] = UPPER (@INPG)
            ORDER BY PH.[Order Date] ,
                    RH.[Order No_] ,
                    RL.[Document No_] ,
                    PL.[No_] ASC
    END


   