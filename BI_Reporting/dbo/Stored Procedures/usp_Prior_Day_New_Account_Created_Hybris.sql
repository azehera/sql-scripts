﻿



/*
=======================================================================================
Author:         Derald Smith &  Ronak Shah 
Create date: Date,01/16/2019
-------------------------[usp_Prior_Day_New_Account_Created_Hybris] -------------------------
---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx----------------------
=======================================================================================   
REFERENCES
Database                       Table/View/UDF		             Action            
---------------------------------------------------------------------------------------
[EXIGO].[ExigoSyncSQL_PRD]     [dbo].[customers]				Select


========================================================================================                 
REVISION LOG
Date           Name              Change
----------------------------------------------------------------------------------------

========================================================================================
*/

CREATE Procedure [dbo].[usp_Prior_Day_New_Account_Created_Hybris]
AS
SET NOCOUNT ON;

BEGIN

SELECT CASE C.Field2 --EnrollerID
		WHEN '5' THEN 'MedDirect'
		ELSE 'Optavia'
		END AS CHANNEL,
		date1 AS Created_Date,  
		C.[Field1] AS [Optavia Customer ID],
		C.[CustomerID] AS [Exigo Customer ID], 
		C.[LastName] AS [Customer Last Name], 
		C.[FirstName] AS [Customer First Name], 
		--USA_CUST_NBR
		C.Field1 AS 'USA_CUST_NBR'
--INTO #newList
from [EXIGO].[ExigoSyncSQL_PRD].dbo.customers C
--JOIN [prdmartini_STORE_repl].[dbo].[USER_ACCOUNT] ua WITH (NOLOCK)
--	ON USA_CUST_NBR = Field1
 -- Join [prdmartini_STORE_repl].[dbo].[USER_ORG] O WITH (NOLOCK)
	--ON USO_USA_ID = USA_ID
WHERE CAST(c.[Date1] AS Date)  = CAST(getdate()-1 AS Date)
Order By CHANNEL
		,Date1

END