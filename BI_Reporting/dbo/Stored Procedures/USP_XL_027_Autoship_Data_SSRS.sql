﻿
/*
=============================================================================================
Author:         Luc Emond
Create date: 04/01/2012
-------------------------[dbo].[USP_XL_027_Autoship_Data_SSRS] -----------------------------
Sales Tracker by Month----
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
 
[BI_Reporting]          DBO.XL_027_Autoship_Data                  Insert          
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------
Job: Run after Sales Cube job finished at 6:00 am
==========================================================================================
*/


Create PROCEDURE [dbo].[USP_XL_027_Autoship_Data_SSRS] AS 

SET NOCOUNT ON;

Select COUNT(*) as CountRows,GETDATE() as Date from DBO.XL_027_Autoship_Data 

