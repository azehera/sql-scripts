﻿


/*
=============================================================================================
Author:         Luc Emond
Create date: 04/11/2013
-------------------------[USP_XL_015_Monthly_AP_Jason Enterprises]-----------------------------
Account Payable information ytd by Month---part of the closing process----
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[BI_Reporting]         dbo.Calendar                             Select      
[NAV_ETL]             [Jason Enterprises$G_L Entry]              Select 
[NAV_ETL]             [Jason Enterprises$Vendor Ledger Entry]    Select 
[NAV_ETL]             [Jason Enterprises$Vendor]                 Select 
[NAV_ETL]             [Jason Enterprises$G_L Account]            Select 
[NAV_ETL]             [Jason Enterprises$Ledger Entry Dimension] Select
[NAV_ETL]             [Jason Enterprises$Dimension Value]        Select                        
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------
2013-04-17    Luc Emond       Added Vendor Posting Group and User ID to the report.
2014-02-03    Luc Emond       Updated Departemnet code Global Dimension 1 Code.
2015-04-07	  D. SMith	      Changed References to NAVPROD to NAV_ETL
2015-08-13	  Micah W.		  Added [Address], [Address 2], [City], [County],[Post Code],[Country_Region Code]
							  per user's request
==========================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_015_Monthly_AP_JasonEnterprises] As
SET NOCOUNT ON;

------DECLARE FIRST DAY OF THE YEAR-----------------------------
Declare @ReportDate DateTime
Set @ReportDate = (Select FirstDateOfYear from dbo.Calendar where CalendarDate = Convert (Varchar(10),GETDATE()-1,121))

----------------------------------------------Jason Properties-----------------------------------------------------------------
DECLARE @TblWorkTable TABLE(
	[Entity] [varchar](20) NULL,
	[Source Code] [varchar](10) NULL,
	[Posting Date] [datetime] NULL,
	[Document Date] [datetime] NULL,
	[G_L Account No_] [varchar](20) NULL,
	[G_L Name] [varchar](30) NULL,
	[Vendor No_] [varchar](20) NULL,
	[1099 Code] [varchar](10) NULL,
	[1099 Amount] [decimal](38, 4) NULL,
	[VendorName] [varchar](50) NULL,
	[Address] [varchar](250) NULL, 
	[Address 2] [varchar](250) NULL, 
	[City][varchar](250) NULL, 
	[State][varchar](250) NULL,
	[Zip Code][varchar](20) NULL,
	[Country_Region Code][varchar](250) NULL,
	[Vendor Posting Group] [varchar] (10) NULL,
	[Document No_] [varchar](20) NULL,
	[Description] [varchar](50) NULL,
	[Global Dimension 2 Code] [varchar](20) NULL,
	[Transaction No_] [int] NULL,
	[Source No_] [varchar](20) NULL,
	[Entry No_] [int] NULL,
	[External Document No_] [varchar] (35) NULL,
	[User ID][varchar] (50) NULL,
	[Dimension Value Code] [varchar](20) NULL,
	[ProjectName] [varchar](50) NULL,
	[Debit Amount] [decimal](38, 4) NULL,
	[Credit Amount] [decimal](38, 4) NULL,
	[NetTotal] [decimal](38, 4) NULL,
	[Dimension Set ID] INT
) 

Insert Into @TblWorkTable
([Entity]
,[Source Code]
,[Posting Date]
,[Document Date]
,[G_L Account No_]
,[G_L Name]
,[Vendor No_]
,[1099 Code]
,[1099 Amount]
,[VendorName]
,[Address] 
,[Address 2] 
,[City]
,[State]
,[Zip Code]
,[Country_Region Code]
,[Vendor Posting Group]
,[Document No_]
,[Description]
,[Global Dimension 2 Code]
,[Transaction No_]
,[Source No_]
,[Entry No_]
,[External Document No_]
,[User ID]
,[Dimension Value Code]
,[ProjectName]
,[Debit Amount]
,[Credit Amount]
,[NetTotal]
,[Dimension Set ID]
)      
Select
 'Jason Enterprises' As Entity
,a.[Source Code]
,a.[Posting Date]
,a.[Document Date]
,a.[G_L Account No_]
,d.[Name] As [G_L Name]
,b.[Vendor No_]
--,b.[1099 Code]
,b.[IRS 1099 Code]
--,b.[1099 Amount]
,b.[IRS 1099 Amount]
,c.[Name] As VendorName
,C.[Address] 
,C.[Address 2] 
,C.[City]
,C.[County] AS [State]
,C.[Post Code] AS [Zip Code]
,[Country_Region Code]
,c.[Vendor Posting Group]
,a.[Document No_]
,a.[Description]
,a.[Global Dimension 1 Code]as [Global Dimension 2 Code] ---2014-02-03    Luc Emond 
,a.[Transaction No_]
,a.[Source No_]
,a.[Entry No_]
,a.[External Document No_]
,b.[User ID]
,'' As [Dimension Value Code]---2013-03-19 Luc Emond
,'' As [ProjectName]---2013-03-19 Luc Emond
,Sum(a.[Debit Amount]) As [Debit Amount]
,Sum(a.[Credit Amount])*-1 as [Credit Amount]
,Sum(a.[Debit Amount]) + Sum(a.[Credit Amount])*-1 As NetTotal
,a.[Dimension Set ID]
From [NAV_ETL].[dbo].[Jason Enterprises$G_L Entry] a (NOLOCK)
 Left Outer Join 
     [NAV_ETL].[dbo].[Jason Enterprises$Vendor Ledger Entry] b (NOLOCK)
     on a.[Document No_] = b.[Document No_]
 Left Outer Join 
     [NAV_ETL].[dbo].[Jason Enterprises$Vendor] c (NOLOCK)
     on b.[Vendor No_] COLLATE DATABASE_DEFAULT =  c.[No_] 
 Left Outer Join 
     [NAV_ETL].[dbo].[Jason Enterprises$G_L Account] d (NOLOCK)
     on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = d.[No_]
Where a.[Posting Date]>= @ReportDate
  And a.[Source Code] ='PURCHASES'  
  And a.[G_L Account No_] <> '20100'     
Group By
 a.[Source Code]
,a.[Posting Date]
,a.[Document Date]
,a.[G_L Account No_]
,d.[Name]
,b.[Vendor No_]
--,b.[1099 Code]
,b.[IRS 1099 Code]
--,b.[1099 Amount]
,b.[IRS 1099 Amount]
,c.[Name]
,C.[Address] 
,C.[Address 2] 
,C.[City]
,C.[County] 
,C.[Post Code]
,C.[Country_Region Code]
,c.[Vendor Posting Group]
,a.[Document No_]
,a.[Description]
,a.[Global Dimension 1 Code]---2014-02-03    Luc Emond 
,a.[Transaction No_]
,a.[Source No_]
,a.[Entry No_]
,a.[External Document No_]
,b.[User ID]
,a.[Dimension Set ID]
Order By 
a.[Posting Date]



--EXECUTE AS LOGIN = 'ReportViewer'

--------UPDATE PROJECT CODE AND PROJECT DESCRIPTION--------2013-03-19 Luc Emond
--UPDATE @TblWorkTable
--SET ProjectName = F.Name, [Dimension Value Code] = E.[Dimension Value Code]
--from @TblWorkTable W 
--Join [NAV_ETL].[dbo].[Jason Enterprises$Ledger Entry Dimension] e (NOLOCK)---2013-03-19 Luc Emond
--	ON W.[Entry No_] = e.[Entry No_]
--Join [NAV_ETL].[dbo].[Jason Enterprises$Dimension Value] f (NOLOCK) ---2013-03-19 Luc Emond
--   on e.[Dimension Value Code]  = f.Code  
--  and e.[Table ID] ='17'
--  and e.[Dimension Code] ='PROJECT' 

--Update for changes in Navision 2016 Upgrade - D.Smith 5/12/2017
UPDATE @TblWorkTable
SET ProjectName = F.Name, [Dimension Value Code] = E.[Dimension Value Code]
from @TblWorkTable W 
JOIN [NAV_ETL].[dbo].[Jason Enterprises$Dimension Set Entry] e
	ON w.[Dimension Set ID] = e.[Dimension Set ID] 
Join [NAV_ETL].[dbo].[Jason Enterprises$Dimension Value] f (NOLOCK) ---2013-03-19 Luc Emond
   on e.[Dimension Value Code]  = f.Code  
  AND e.[Dimension Code] = 'PROJECT' 
   
---SELECT DATA FOR THE REPORT------    
SELECT * FROM @TblWorkTable




