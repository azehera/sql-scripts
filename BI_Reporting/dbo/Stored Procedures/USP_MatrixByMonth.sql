﻿
 
CREATE Procedure [dbo].[USP_MatrixByMonth]
 AS
 
/*****************************************************************************
 
===================================================================================
Objects List:

[BI_Reporting].[dbo].[calendar] 
[NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line]
[NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header]

prdmartini_STORE_repl.dbo.ORDER_HEADER
prdmartini_STORE_repl.dbo.ORDER_LINE
prdmartini_STORE_repl.dbo.OBJECT
prdmartini_STORE_repl.dbo.ORGANIZATION
prdmartini_STORE_repl.dbo.OBJECT_ATTRIBUTE

************************************************************************************/
 
 Set noCount on;
 
 
 
 DECLARE 
  @StartDate Datetime
 ,@EndDate DateTime 
 ,@MonthId_ThisMonth Int
 ,@MonthId_LastMonth Int
 ,@LastDateOfMonth_TM Datetime
 ,@LastDateOfMonth_LM Datetime
 ,@FirstDateOfMonth_TM Datetime
 ,@FirstDateOfMonth_LM Datetime
  
Set @StartDate= (Select LastDateOfYear  FROM [BI_Reporting].[dbo].[calendar] where CalendarYear = (Select DATEPART(YYYY,(Getdate()-1))-5) Group By LastDateOfYear)

Set @MonthId_ThisMonth= (SELECT [CalendarMonthID]  FROM [BI_Reporting].[dbo].[calendar]
                           Where CalendarDate = (Select Convert(Varchar(10),Getdate()-1,121)))
                           
Set @EndDate =  (Select LastDateOfMonth FROM [BI_Reporting].[dbo].[calendar]  
                  Where CalendarMonthID = (Select Case When Right(@MonthId_ThisMonth,2) =01 then (SELECT [CalendarMonthID]-90  FROM [BI_Reporting].[dbo].[calendar]
                           Where CalendarMonthID  = @MonthId_ThisMonth GROUP by [CalendarMonthID]) 
                            Else (SELECT [CalendarMonthID]-1  FROM [BI_Reporting].[dbo].[calendar]
                             Where CalendarMonthID  = @MonthId_ThisMonth group by [CalendarMonthID]) end)Group By LastDateOfMonth)                          
       
Set @MonthId_LastMonth= (Select Case When Right(@MonthId_ThisMonth,2) =01 then (SELECT [CalendarMonthID]-89  FROM [BI_Reporting].[dbo].[calendar]
                           Where CalendarMonthID  = @MonthId_ThisMonth GROUP by [CalendarMonthID]) 
                            Else (SELECT [CalendarMonthID]-1  FROM [BI_Reporting].[dbo].[calendar]
                             Where CalendarMonthID  = @MonthId_ThisMonth group by [CalendarMonthID]) end)
 
Set @LastDateOfMonth_TM = (Select [LastDateOfMonth]  FROM [BI_Reporting].[dbo].[calendar]
                            Where CalendarMonthID = @MonthId_ThisMonth Group By [LastDateOfMonth] )
       
Set @LastDateOfMonth_LM = (Select [LastDateOfMonth]  FROM [BI_Reporting].[dbo].[calendar]
                            Where CalendarMonthID = @MonthId_LastMonth Group By [LastDateOfMonth] )     
       
Set @FirstDateOfMonth_TM = (Select [FirstDateOfMonth]  FROM [BI_Reporting].[dbo].[calendar]
                             Where CalendarMonthID = @MonthId_ThisMonth Group By [FirstDateOfMonth] )
       
Set @FirstDateOfMonth_LM = (Select [FirstDateOfMonth]  FROM [BI_Reporting].[dbo].[calendar]
                              Where CalendarMonthID = @MonthId_LastMonth Group By [FirstDateOfMonth] ) 

-------------AUTOSHIP-----------
IF object_id('Tempdb..#Auto') Is Not Null DROP TABLE #Auto	
select
CalendarYear
,CalendarMonthName
,[Document No_]
,[External Document No_]
,B.[Shortcut Dimension 1 Code]
,B.[Shortcut Dimension 1 Code] AS Code2
,CONVERT(varchar(15),' ') as Status
,B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5) as CustomerNumber
,SUM(Amount) as Amount
Into #Auto
from [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line]A
Join [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header]B
on a.[Document No_]=b.No_
Join [BI_Reporting].[dbo].[calendar] Ca	
       on B.[Posting Date] = Ca.CalendarDate
WHERE B.[Posting Date] between  @FirstDateOfMonth_TM  and @LastDateOfMonth_TM  and b.[Customer Posting Group]='MEDIFAST' 
group by
[Document No_]
,[External Document No_]
,B.[Shortcut Dimension 1 Code]
,B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5)
,CalendarYear
,CalendarMonthName

IF object_id('Tempdb..#UcartAuto ') Is Not Null DROP TABLE #UcartAuto 	
SELECT 
     ORH_SOURCE_NBR as OrderNumber,
     convert(varchar(10),OBJ_CREATE_DT,121) as OrderDate,
     case when OBA_ATR_ID IS null then 'On Demand' else 'Autoship' end as OrderType
      INTO #UcartAuto FROM prdmartini_STORE_repl.dbo.ORDER_HEADER
join [prdmartini_STORE_repl].[dbo].[ORDER_LINE] b
on ORDER_HEADER.[ORH_ID]=b.ORL_ORH_ID
	INNER JOIN prdmartini_STORE_repl.dbo.OBJECT ON (OBJECT.OBJ_ID=ORDER_HEADER.orh_id)
	left join prdmartini_STORE_repl.dbo.OBJECT_ATTRIBUTE
	on  OBJECT.OBJ_ID=OBJECT_ATTRIBUTE.OBA_OBJ_ID
	and OBA_ATR_ID='6193548999262269'
	join prdmartini_STORE_repl.dbo.ORGANIZATION Org
       on ORDER_HEADER.[ORH_ORG_ID]=org.[ORG_ID]
 WHERE OBJ_TYPE = 24
 and convert(varchar(10),OBJ_CREATE_DT,121) BETWEEN @FirstDateOfMonth_TM  and @LastDateOfMonth_TM
and  [ORL_STATUS_CD]IN ('H', 'B', 'I', 'T', 'C', 'S', '1', 'R', 'P', 'Y', 'M') 
and org.[ORG_ACCOUNT_NBR]='MEDIFAST'
Group by  ORH_SOURCE_NBR,OBJ_CREATE_DT,OBA_ATR_ID, OBJ_TYPE



--SELECT OrderNumber
--      ,OrderType
--      ,ATR_ORDER_REQUEST_COACH
--       INTO #UcartAuto  
--       FROM [UCARTDW].[prdmartini_DSS].[dbo].[vw_Orders]
--       Where CONVERT(VARCHAR(10),OrderDate,121) BETWEEN @FirstDateOfMonth_TM  and @LastDateOfMonth_TM
--       AND OrderStatus='approved' and OrderChannel= 'MEDIFAST'
  
UPDATE #Auto
SET [Code2] ='AUTO'
WHERE #AUTO.[External Document No_] in (Select OrderNumber from #UcartAuto where OrderType ='Autoship')
and [Code2] <>'AUTO'


UPDATE #Auto
SET Code2 = 'AUTO'
where [Shortcut Dimension 1 Code] <>'AUTO'
and Code2 ='Auto'


-------NEW CUSTOMERS-------
IF object_id('Tempdb..#Master ') Is Not Null DROP TABLE #Master 	
SELECT 
 B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5) As UniqueID
,min(B.[Posting Date]) as FirstorderDate
,MIN([External Document No_]) as FirstOrd
,CONVERT(char(15),' ') as CustomerPostingGroup
,CONVERT(char(15),' ') as [ShortcutDimension1Code]
into #Master
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 
WHERE b.[Posting Date] Between @StartDate  and @LastDateOfMonth_TM and B.[Customer Posting Group] NOT in ('CORPCLINIC') 
group by 
B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5) 
order by 
B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5)


Update #Master
set CustomerPostingGroup = l.[Customer Posting Group], [ShortcutDimension1Code] = l.[Shortcut Dimension 1 Code]
from [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] l
where UniqueID = l.[Sell-to Customer Name]+l.[Sell-to Address]+Left(l.[Sell-to Post Code],5)
and FirstorderDate = l.[Posting Date]

-------------------------------Leads-------------------------------------
IF object_id('Tempdb..#UcartAutoLeads ') Is Not Null DROP TABLE #UcartAutoLeads
SELECT 
     ORH_SOURCE_NBR as OrderNumber,
     OBJ_CREATE_DT as OrderDate,
     case when OBA_ATR_ID IS null then 'On Demand' else 'Autoship' end as OrderType
     Into #UcartAutoLeads
     FROM prdmartini_STORE_repl.dbo.ORDER_HEADER
join [prdmartini_STORE_repl].[dbo].[ORDER_LINE] b
on ORDER_HEADER.[ORH_ID]=b.ORL_ORH_ID
	INNER JOIN prdmartini_STORE_repl.dbo.OBJECT ON (OBJECT.OBJ_ID=ORDER_HEADER.orh_id)
	 join prdmartini_STORE_repl.dbo.OBJECT_ATTRIBUTE
	on  OBJECT.OBJ_ID=OBJECT_ATTRIBUTE.OBA_OBJ_ID
	and OBA_ATR_ID='6193548999262379'
WHERE 
OBJ_TYPE = 24
and  [ORL_STATUS_CD]IN ('H', 'B', 'I', 'T', 'C', 'S', '1', 'R', 'P', 'Y', 'M') 
	and convert(varchar(10),OBJ_CREATE_DT,121) BETWEEN @FirstDateOfMonth_TM  and @LastDateOfMonth_TM
Group by  ORH_SOURCE_NBR,OBJ_CREATE_DT,OBA_ATR_ID, OBJ_TYPE


----NEW CUSTOMER------

IF object_id('Tempdb..#tot ') Is Not Null DROP TABLE #tot
select *
into #tot
From #Master
where  FirstorderDate between @FirstDateOfMonth_TM and @LastDateOfMonth_TM
and CustomerPostingGroup ='MEDIFAST'

Update #Auto set [Status] ='Existing'
Update #Auto set [Status] = 'New' Where #Auto.[External Document No_] IN(Select FirstOrd from #tot)
Update #Auto Set [Status] ='Leads' where [External Document No_] in (Select OrderNumber from #UcartAutoLeads )
                   and status='New'

---------------------------------------------------Last to Last Month---------------------------------------------
------------AUTOSHIP-----------
IF object_id('Tempdb..#Auto1') Is Not Null DROP TABLE #Auto1	
select
CalendarYear
,CalendarMonthName
,[Document No_]
,[External Document No_]
,B.[Shortcut Dimension 1 Code]
,B.[Shortcut Dimension 1 Code] AS Code2
,CONVERT(varchar(15),' ') as Status
,B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5) as CustomerNumber
,SUM(Amount) as Amount
Into #Auto1
from [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line]A
Join [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header]B
on a.[Document No_]=b.No_
Join [BI_Reporting].[dbo].[calendar] Ca	
       on B.[Posting Date] = Ca.CalendarDate
WHERE B.[Posting Date] between  @FirstDateOfMonth_LM  and @LastDateOfMonth_LM  and b.[Customer Posting Group]='MEDIFAST' 
group by
[Document No_]
,[External Document No_]
,B.[Shortcut Dimension 1 Code]
,B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5)
,CalendarYear
,CalendarMonthName

IF object_id('Tempdb..#UcartAuto1 ') Is Not Null DROP TABLE #UcartAuto1 	

SELECT 
     ORH_SOURCE_NBR as OrderNumber,
     convert(varchar(10),OBJ_CREATE_DT,121) as OrderDate,
     case when OBA_ATR_ID IS null then 'On Demand' else 'Autoship' end as OrderType
      INTO #UcartAuto1 FROM prdmartini_STORE_repl.dbo.ORDER_HEADER
join [prdmartini_STORE_repl].[dbo].[ORDER_LINE] b
on ORDER_HEADER.[ORH_ID]=b.ORL_ORH_ID
	INNER JOIN prdmartini_STORE_repl.dbo.OBJECT ON (OBJECT.OBJ_ID=ORDER_HEADER.orh_id)
	left join prdmartini_STORE_repl.dbo.OBJECT_ATTRIBUTE
	on  OBJECT.OBJ_ID=OBJECT_ATTRIBUTE.OBA_OBJ_ID
	and OBA_ATR_ID='6193548999262269'
	join prdmartini_STORE_repl.dbo.ORGANIZATION Org
       on ORDER_HEADER.[ORH_ORG_ID]=org.[ORG_ID]
 WHERE OBJ_TYPE = 24
 and convert(varchar(10),OBJ_CREATE_DT,121) BETWEEN @FirstDateOfMonth_LM  and @LastDateOfMonth_LM
and  [ORL_STATUS_CD]IN ('H', 'B', 'I', 'T', 'C', 'S', '1', 'R', 'P', 'Y', 'M') 
and org.[ORG_ACCOUNT_NBR]='MEDIFAST'
Group by  ORH_SOURCE_NBR,OBJ_CREATE_DT,OBA_ATR_ID, OBJ_TYPE


--SELECT OrderNumber
--      ,OrderType
--      ,ATR_ORDER_REQUEST_COACH
--       INTO #UcartAuto1  
--       FROM [UCARTDW].[prdmartini_DSS].[dbo].[vw_Orders]
--       Where CONVERT(VARCHAR(10),OrderDate,121) BETWEEN @FirstDateOfMonth_LM  and @LastDateOfMonth_LM
--       AND OrderStatus='approved' and OrderChannel= 'MEDIFAST'
  
UPDATE #Auto1
SET [Code2] ='AUTO'
WHERE #AUTO1.[External Document No_] in (Select OrderNumber from #UcartAuto1 where OrderType ='Autoship')
and [Code2] <>'AUTO'

UPDATE #Auto1
SET Code2 = 'AUTO'
where [Shortcut Dimension 1 Code] <>'AUTO'
and Code2 ='Auto'

-------------------------------Leads-------------------------------------
IF object_id('Tempdb..#UcartAutoLeads1 ') Is Not Null DROP TABLE #UcartAutoLeads1
SELECT 
     ORH_SOURCE_NBR as OrderNumber,
     OBJ_CREATE_DT as OrderDate,
     case when OBA_ATR_ID IS null then 'On Demand' else 'Autoship' end as OrderType
     Into #UcartAutoLeads1
     FROM prdmartini_STORE_repl.dbo.ORDER_HEADER
join [prdmartini_STORE_repl].[dbo].[ORDER_LINE] b
on ORDER_HEADER.[ORH_ID]=b.ORL_ORH_ID
	INNER JOIN prdmartini_STORE_repl.dbo.OBJECT ON (OBJECT.OBJ_ID=ORDER_HEADER.orh_id)
	 join prdmartini_STORE_repl.dbo.OBJECT_ATTRIBUTE
	on  OBJECT.OBJ_ID=OBJECT_ATTRIBUTE.OBA_OBJ_ID
	and OBA_ATR_ID='6193548999262379'
WHERE 
OBJ_TYPE = 24
and  [ORL_STATUS_CD]IN ('H', 'B', 'I', 'T', 'C', 'S', '1', 'R', 'P', 'Y', 'M') 
	and convert(varchar(10),OBJ_CREATE_DT,121) BETWEEN @FirstDateOfMonth_LM  and @LastDateOfMonth_LM
Group by  ORH_SOURCE_NBR,OBJ_CREATE_DT,OBA_ATR_ID, OBJ_TYPE
-------NEW CUSTOMERS-------
IF object_id('Tempdb..#Master1 ') Is Not Null DROP TABLE #Master1 	
SELECT 
 B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5) As UniqueID
,min(B.[Posting Date]) as FirstorderDate
,MIN([External Document No_]) as FirstOrd
,CONVERT(char(15),' ') as CustomerPostingGroup
,CONVERT(char(15),' ') as [ShortcutDimension1Code]
into #Master1
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 
WHERE b.[Posting Date] Between @StartDate  and @LastDateOfMonth_LM and B.[Customer Posting Group] NOT in ('CORPCLINIC') 
group by 
B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5) 
order by 
B.[Sell-to Customer Name]+B.[Sell-to Address]+Left(B.[Sell-to Post Code],5)


Update #Master1
set CustomerPostingGroup = l.[Customer Posting Group], [ShortcutDimension1Code] = l.[Shortcut Dimension 1 Code]
from [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] l
where UniqueID = l.[Sell-to Customer Name]+l.[Sell-to Address]+Left(l.[Sell-to Post Code],5)
and FirstorderDate = l.[Posting Date]

----NEW CUSTOMER------

IF object_id('Tempdb..#tot1 ') Is Not Null DROP TABLE #tot1
select *
into #tot1
From #Master1
where FirstorderDate between @FirstDateOfMonth_LM and @LastDateOfMonth_LM

and CustomerPostingGroup ='MEDIFAST'



Update #Auto1 set [Status] ='Existing'
Update #Auto1 set [Status] = 'New' Where #Auto1.[External Document No_] IN(Select FirstOrd from #tot1)
Update #Auto1 Set [Status] ='Leads' where [External Document No_] in (Select OrderNumber from #UcartAutoLeads1)
                   and status='New'

Truncate table [BI_Reporting].dbo.matrix
Insert Into [BI_Reporting].dbo.Matrix	
([CalendarYear]
      ,[CalendarMonthName]
      ,[SalesChannel]
      ,[Accounts]
      ,[OrderCount]
      ,[CustomerCount]
      ,[Dollars])

select CalendarYear
      ,CalendarMonthName
      ,Code2 as SalesChannel,
       Status as Accounts,
      COUNT([Document No_]) as OrderCount,
      COUNT(distinct CustomerNumber) as CustomerCount,
      SUM(Amount) as Dollars
      from #Auto
      Group by CalendarYear
      ,CalendarMonthName,Code2,
       Status
Union all

select CalendarYear
      ,CalendarMonthName
      ,Code2 as SalesChannel,
       Status as Accounts,
      COUNT([Document No_]) as OrderCount,
      COUNT(distinct CustomerNumber) as CustomerCount,
      SUM(Amount) as Dollars
      from #Auto1
      Group by CalendarYear
      ,CalendarMonthName,Code2,
       Status
 Union all
 
 SELECT 	
 CalendarYear	
,CalendarMonthName 	
,B.[Shortcut Dimension 1 Code]as SalesChannel
,'Credit' as Accounts		
,COUNT(Distinct(A.[Document No_])) as OrderCount
,COUNT(Distinct B.[Sell-to Customer No_]) as CustomerCount	
,Sum(A.[Amount])*-1 as Dollars	
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line] A 	
Inner Join 	
     [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] B 	
      ON A.[Document No_]= B.[No_]	
Left Outer Join	
 [BI_Reporting].[dbo].[calendar] Ca	
       on B.[Posting Date] = Ca.CalendarDate	
WHERE B.[Customer Posting Group] ='MEDIFAST' and b.[Posting Date] BETWEEN @FirstDateOfMonth_LM AND @LastDateOfMonth_TM	and a.[Sell-to Customer No_] <> ''	
GROUP BY 	
 B.[Customer Posting Group]	
,CalendarMonthName 	
,CalendarYear	
,B.[Shortcut Dimension 1 Code]      


Truncate table [BI_Reporting].dbo.Matrix_AutoShip
INSERT INTO [BI_Reporting].dbo.Matrix_AutoShip
select 
 CalendarMonthName 
,(select SUM(Dollars) from [BI_Reporting].dbo.Matrix where Accounts <>'Credit' and CalendarMonthName = (Select CalendarMonthName from [BI_Reporting].[dbo].[calendar]  where CalendarMonthID = @MonthId_ThisMonth group by CalendarMonthName)) as GroosSales
,SUM(OrderCount) - (select SUM(OrderCount) from [BI_Reporting].dbo.Matrix where Accounts ='Credit' and CalendarMonthName = (Select CalendarMonthName from [BI_Reporting].[dbo].[calendar]  where CalendarMonthID = @MonthId_ThisMonth group by CalendarMonthName)) as OrderShipped
,(select COUNT(distinct [Document No_]) from #Auto where [Shortcut Dimension 1 Code]='AUTO') as AutoshipOrder
,null from  [BI_Reporting].dbo.matrix	
where CalendarMonthName = (Select CalendarMonthName from [BI_Reporting].[dbo].[calendar]  where CalendarMonthID = @MonthId_ThisMonth group by CalendarMonthName)
group by CalendarMonthName 

----last month----
Truncate table [BI_Reporting].[dbo].[Matrix_AutoShipLastMonth]
INSERT INTO [BI_Reporting].[dbo].[Matrix_AutoShipLastMonth]
select 
 CalendarMonthName 
,(select SUM(Dollars) from [BI_Reporting].dbo.Matrix where Accounts <>'Credit' and CalendarMonthName = (Select CalendarMonthName from [BI_Reporting].[dbo].[calendar]  where CalendarMonthID = @MonthId_LastMonth group by CalendarMonthName)) as GroosSales
,SUM(OrderCount) - (select SUM(OrderCount) from [BI_Reporting].dbo.Matrix where Accounts ='Credit' and CalendarMonthName = (Select CalendarMonthName from [BI_Reporting].[dbo].[calendar]  where CalendarMonthID = @MonthId_LastMonth group by CalendarMonthName)) as OrderShipped
,(select COUNT(distinct [Document No_]) from #Auto1 where [Shortcut Dimension 1 Code]='AUTO') as AutoshipOrder
,null from  [BI_Reporting].dbo.matrix	
where CalendarMonthName = (Select CalendarMonthName from [BI_Reporting].[dbo].[calendar]  where CalendarMonthID = @MonthId_LastMonth group by CalendarMonthName)
group by CalendarMonthName 
-------------------------------------COREMETRIC-----------------------------------------------------------------

EXEC [dbo].[CoreMetricReport]
EXEC [dbo].[USP_Update_Matrix_AutoShipLastMonth]
EXEC [dbo].[USP_Update_Matrix_AutoShip_Monthly]       







