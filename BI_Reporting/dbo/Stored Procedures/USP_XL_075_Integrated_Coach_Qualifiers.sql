﻿/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/24/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_BUSINESSCENTER] 		     Select   
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER_Status]           Select
[ODYSSEY_ETL]          [dbo].[ODYSSEY_VOLUME]                    Select


=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================*/
CREATE  procedure [dbo].[USP_XL_075_Integrated_Coach_Qualifiers] as 
set nocount on ;

----------------------Pull FIHCs Count----------------------------------------
------------------------------------Pull Coaches ED and higher---------------------------
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select 
	 [CUSTOMER_NUMBER] as [HC ID #]
	,B.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,(B.[First_Name]+' '+ B.[Last_Name]) as [CoachName]
	,convert(varchar(10),[COMMISSION_PERIOD],121) as [CommPeriod]
	,[HIGHEST_ACHIEVED_RANK]
	,[Rank] as [Performing Rank]
	,[CERTIFIED]
	,convert(varchar(10),[CERTIFIED_DATE],121) as [CERTIFIED_DATE]
into #a
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
		on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_Status] D
		on B.[CUSTOMER_ID] = D.[CUSTOMER_ID]
	Where convert(varchar(10),[COMMISSION_PERIOD],121) >= GETDATE() - 365
		--and convert(varchar(10),[COMMISSION_PERIOD],121) < '2013-07-01'
		and [CUSTOMER_NUMBER]<> '101'
		
-------------------------------------Pull FLV------------------------------------------------

IF object_id('Tempdb..#FLV') Is Not Null DROP TABLE #FLV		
Select
	 [HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[CommPeriod]
	,[HIGHEST_ACHIEVED_RANK]
	,[Performing Rank]
	,[CERTIFIED]
	,[CERTIFIED_DATE]
	,D.[VOLUME_VALUE] as [FLV]
into #FLV
From #a C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
		and C.[CommPeriod]= convert(varchar(10),D.[PERIOD_END_DATE],121)
	Where convert(varchar(10),[PERIOD_END_DATE],121)> '2014-04-01'
		and D.[VOLUME_TYPE] ='FV'
		and D.[VOLUME_VALUE] >= '6000'
		or convert(varchar(10),[PERIOD_END_DATE],121) >= '2013-09-01'
	and  convert(varchar(10),[PERIOD_END_DATE],121) < '2014-04-01'
	and D.[VOLUME_TYPE] ='RFV'
	and D.[VOLUME_VALUE] >= '6000'
	or convert(varchar(10),[PERIOD_END_DATE],121) < '2013-09-01'
	and D.[VOLUME_TYPE] ='FV'
	and D.[VOLUME_VALUE] >= '6000'
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[CommPeriod]
	,[Performing Rank]
	--,[POSTAL_CODE]
	,[HIGHEST_ACHIEVED_RANK]
	,D.[VOLUME_VALUE]
	,[CERTIFIED]
	,[CERTIFIED_DATE]

-------------------------Pull EDs by Volume with Client counts----------------------------		
Select
	 C.[CommPeriod]
	,COUNT(distinct([HC ID #])) as [FIHCs]
Into #FIHCs
From #FLV C
	Where [CERTIFIED] = '1'
		and convert(varchar(10),[CERTIFIED_DATE],121) < = C.[CommPeriod]
		Group by  C.[CommPeriod]
		
----------------------Pull FIBC Count per Month------------------------------------------		
IF object_id('Tempdb..#AA') Is Not Null DROP TABLE #AA
Select 
	 [CUSTOMER_NUMBER] as [HC ID #]
	,B.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,(B.[First_Name]+' '+ B.[Last_Name]) as [CoachName]
	,convert(varchar(10),[COMMISSION_PERIOD],121) as [CommPeriod]
	,[HIGHEST_ACHIEVED_RANK]
	,[Rank] as [Performing Rank]
	--,[POSTAL_CODE]
into #aa
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
		on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	--Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS]D
	--	on B.[CUSTOMER_ID] = D.[CUSTOMER_ID]
	Where convert(varchar(10),[COMMISSION_PERIOD],121) >= GETDATE() - 365
		--and convert(varchar(10),[COMMISSION_PERIOD],121) < '2014-02-01'
		and [CUSTOMER_NUMBER]<> '101'
		and [RANK] in ('FI_Presidential_Director','Presidential_Director','Global_Director','FI_Global_Director','FI_National_Director','National_Director'
		,'FI_Regional_Director','Regional_Director','FI_Executive_Director','Executive_Director')
		--and [ADDRESS_TYPE] = 'Main'
		--and [LANGUAGE_CODE] = 'English'


-----------------------------------Pull Teams by sponsor---------------------
IF object_id('Tempdb..#zz') Is Not Null DROP TABLE #zz
SELECT 
	   [SPONSOR_ID]
	  ,convert(varchar(10),[COMMISSION_PERIOD],121) as [Comm Period]
      ,sum([IS_SENIOR_COACH_LEG]) as [SC Teams]
      ,sum([IS_Exec_DIR_LEG]) as [ED Teams]
Into #zz
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on B.[Customer_ID] = C.[Customer_ID]
		Group by [SPONSOR_ID],[COMMISSION_PERIOD]
		
----------------------Pull Sponsor ID # and Name-----------------
IF object_id('Tempdb..#structure') Is Not Null DROP TABLE #structure
Select 
      C.[CUSTOMER_NUMBER] as [Dist ID]
      ,(C.[FIRST_NAME]+' '+C.[LAST_NAME]) as [Name]
      ,[Comm Period]
      ,isnull([SC Teams],'0') as [SC Teams]
      ,isnull([ED Teams],'0') as [ED Teams]
    Into #structure
From #zz A
Inner join  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
      on A.[SPONSOR_ID] = B.[BUSINESSCENTER_ID]
      and [Comm Period] = convert(varchar(10),[COMMISSION_PERIOD],121)
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
	  on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]

IF object_id('Tempdb..#FLV2') Is Not Null DROP TABLE #FLV2		
Select
	[HC ID #]
	,[CoachName]
	,A.[Customer_ID]
	,A.[BUSINESSCENTER_ID]
	,[CommPeriod]
	,[HIGHEST_ACHIEVED_RANK]
	,[Performing Rank]
	,[SC Teams]
	,[ED Teams]
	--,[POSTAL_CODE]
	,D.[VOLUME_VALUE] as [FLV]
into #FLV2
From #aa A
	Left join #Structure C
		on A.[HC ID #] = C.[Dist ID]
		and A.[CommPeriod] = C.[Comm Period]
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on A.[BUSINESSCENTER_ID] = D.[NODE_ID]
		and A.[CommPeriod]= convert(varchar(10),D.[PERIOD_END_DATE],121)
	Where convert(varchar(10),[PERIOD_END_DATE],121)> '2014-04-01'
	and D.[VOLUME_TYPE] ='FV'
	or convert(varchar(10),[PERIOD_END_DATE],121) >= '2013-09-01'
	and  convert(varchar(10),[PERIOD_END_DATE],121) < '2014-04-01'
	and D.[VOLUME_TYPE] ='RFV'
	or convert(varchar(10),[PERIOD_END_DATE],121) < '2013-09-01'
	and D.[VOLUME_TYPE] ='FV'
group by 
	[HC ID #]
	,[CoachName]
	,A.[Customer_ID]
	,A.[BUSINESSCENTER_ID]
	,[CommPeriod]
	,[Performing Rank]
	,[SC Teams]
	,[ED Teams]
	--,[POSTAL_CODE]
	,[HIGHEST_ACHIEVED_RANK]
	,D.[VOLUME_VALUE]


IF object_id('Tempdb..#all') Is Not Null DROP TABLE #all
Select
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[CommPeriod]
	,[HIGHEST_ACHIEVED_RANK]
	,[Performing Rank]
	,[SC Teams]
	,[ED Teams]
	--,[POSTAL_CODE]
	,[FLV]
	,D.[VOLUME_VALUE] as [GV]
into #all
From #FLV2 C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
		and C.[CommPeriod]= convert(varchar(10),D.[PERIOD_END_DATE],121)
	Where convert(varchar(10),[PERIOD_END_DATE],121)> '2014-04-01'
	and D.[VOLUME_TYPE] ='GV'
	or convert(varchar(10),[PERIOD_END_DATE],121) >= '2013-09-01'
	and  convert(varchar(10),[PERIOD_END_DATE],121) < '2014-04-01'
	and D.[VOLUME_TYPE] ='RGV'
	or convert(varchar(10),[PERIOD_END_DATE],121)< '2013-09-01'
	and D.[VOLUME_TYPE] ='GV'
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[CommPeriod]
	,[Performing Rank]
	,[SC Teams]
	,[ED Teams]
	,[FLV]	
	--,[POSTAL_CODE]
	,[HIGHEST_ACHIEVED_RANK]
	,D.[VOLUME_VALUE]

	
Select
	[CommPeriod]
	,count(distinct([HC ID #])) as [FIBCs]
Into #FIBC
	From #all A
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_STATUS]S
		on A.[CUSTOMER_ID] = S.[CUSTOMER_ID]
		Where [FLV] >= '6000'
			and [GV] >= '15000'
			and [SC Teams] >='5'
			and convert(varchar(10),S.[CERTIFIED_DATE],121) <= [CommPeriod]
		Group by [CommPeriod]

----------------------Pull FIBL Count per Month------------------------------------------
-----------------------------------Pull Teams by sponsor---------------------
SELECT 
	   [SPONSOR_ID]
      ,convert(varchar(10),[COMMISSION_PERIOD],121) as [Month]
      ,sum([IS_FIBC_LEG]) as [FIBC Teams]
Into #Legs
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on B.[Customer_ID] = C.[Customer_ID]
	Where convert(varchar(10),[COMMISSION_PERIOD],121) >= GETDATE()-365
		Group by [SPONSOR_ID],[COMMISSION_PERIOD]

----------------------Pull Sponsor ID # and Name-----------------
Select 
      C.[CUSTOMER_NUMBER] as [Sponsor ID]
      ,(C.[FIRST_NAME]+' '+C.[LAST_NAME]) as [Sponsor Name]
      ,[Month]
      ,isnull([FIBC Teams],'0') as [FIBC Teams]
Into #FIBCLEGS
From #legs A
Inner join  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
      on A.[SPONSOR_ID] = B.[BUSINESSCENTER_ID]
      and A.[Month] = convert(varchar(10),B.[COMMISSION_PERIOD],121)
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
	  on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]


---------------------Pull Qualified FIBLs---------------------------
Select
	[Month]
	,COUNT(distinct([Sponsor ID])) as [FIBLs]
Into #FIBLs
From #FIBCLEGS
	Where [FIBC Teams] >= '5'
		Group by [Month]
		
--------------------Pull Into Single Report----------------------
Select
	A.[CommPeriod] as [Month]
	,[FIHCs]
	,[FIBCs]
	,[FIBLs]	
From #FIHCs A
	left join #FIBC B
		on A.CommPeriod = B.CommPeriod
	left join #FIBLs C
		on A.CommPeriod = C.[Month]