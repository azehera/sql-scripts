﻿
/*
===============================================================================
Author:         Kalpesh Patel
Create date: 01/20/2014

==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER_ADDRESS]		       Select   
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                     Select  
[ODYSSEY_ETL]       [dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]       Select

=====================================================================================================
REVISION LOG
Date                   Name                  Change
---------------------------------------------------------------------------------------------------
2014-01-22           Luc Emond        Added a new column in Numeric for Coach Id for V_Lookup                 

====================================================================================================
NOTES:
--------------------------------------------------------------------------------------------------
====================================================================================================
*/


CREATE procedure [dbo].[USP_XL_054_New_Certifications] as 
set nocount on ; 

Declare @StartDate Datetime
Declare @EndDate Datetime

Set @StartDate=(Select dateadd(wk, datediff(wk, 0, getdate()) - 1, 0))
Set @EndDate=(Select dateadd(wk, datediff(wk, 0, getdate()), -1))

---------------------------------Pull Certifications for Week-----------------------------
SELECT 
	  A.[CUSTOMER_NUMBER] as [Coach ID]
	 ,Convert(numeric(36,0),(A.[CUSTOMER_NUMBER])) as V_LookupCoachID
	 ,A.[FIRST_NAME]as [First Name]
	 ,A.[LAST_NAME]as [Last Name]
	 ,CA.[ADDRESS1]
	 ,CA.[ADDRESS2]
	 ,CA.[City]
     ,CA.[STATE]
     ,CA.[POSTAL_CODE]
     ,A.[EMAIL1_ADDRESS] as [Coach_Email]
     ,convert(varchar(10),CS.[CERTIFIED_DATE],110) as [Certification Date]
     ,[DayOfWeekName]
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_STATUS]CS
			on A.[CUSTOMER_ID] = CS.[CUSTOMER_ID]
	Inner join [BI_Reporting].[dbo].[calendar_TSFL] C 
		on convert(varchar (10),CS.[CERTIFIED_DATE],121)= C.[CalendarDate]
	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CA
			on A.[CUSTOMER_ID] = CA.[CUSTOMER_ID]
Where convert(varchar (10),CS.[CERTIFIED_DATE],121)between @StartDate and @EndDate
		and CA.[ADDRESS_TYPE] = 'Main'
		and CA.[LANGUAGE_CODE] = 'English'
Order by CS.[CERTIFIED_DATE] asc

-----------------------------------Pull Co-App Certifications for Week-----------------------------
--SELECT 
--	  A.[CUSTOMER_NUMBER] as [Coach ID]
--	 ,Co.[FIRST_NAME]as [Co-App First Name]
--	 ,Co.[LAST_NAME]as [Co-App Last Name]
--	 ,CA.[ADDRESS1]
--	 ,CA.[ADDRESS2]
--	 ,CA.[City]
--     ,CA.[STATE]
--     ,CA.[POSTAL_CODE]
--     ,Co.[EMAIL1_ADDRESS] as [Co-App_Email]
--     ,convert(varchar(10),CS.[CO_APP_CERTIFIED_DATE],110) as [Co-App Certification Date]
--     ,[DayOfWeekName]
--FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
--	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_STATUS]CS
--			on A.[CUSTOMER_ID] = CS.[CUSTOMER_ID]
--	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CO_APPLICANT] Co
--		on A.[CUSTOMER_ID] = Co.[CUSTOMER_ID]
--	Inner join [BI_Reporting].[dbo].[calendar_TSFL] C 
--		on convert(varchar (10),CS.[CO_APP_CERTIFIED_DATE],121)= C.[CalendarDate]
--	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CA
--			on A.[CUSTOMER_ID] = CA.[CUSTOMER_ID]
--Where convert(varchar(10),[FirstDateOfWeek],121) >= @DATE
--		and convert(varchar(10),[LastDateOfWeek],121) < GETDATE()
--		and [DayOfWeekName] <>'Sunday'
--		and CA.[ADDRESS_TYPE] = 'Main'
--		and CA.[LANGUAGE_CODE] = 'English'
--		or convert(varchar(10),[FirstDateOfWeek],121) < GETDATE()
--		and convert(varchar(10),[LastDateOfWeek],121) > GETDATE()
--		and [DayOfWeekName] = 'Sunday'
--		and CA.[ADDRESS_TYPE] = 'Main'
--		and CA.[LANGUAGE_CODE] = 'English'
--Order by CS.[CERTIFIED_DATE] asc


