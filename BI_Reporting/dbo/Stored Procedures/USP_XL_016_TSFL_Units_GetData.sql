﻿
--/*
--===============================================================================
--Author:         Emily Trainor
--Create date: 11/1/2012
---------------------------TSFL Tracker TSFL Units-----------------------------
-------Pull data for TSFL Units Business Week Graph----------------------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--BI_Reporting		XL_016_TSFL_LY_TY				               Select                        
--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------
--06/22/2012	  Emily Trainor					Updated to pull Business Week
--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--===============================================================================
--*/

Create procedure [dbo].[USP_XL_016_TSFL_Units_GetData] as 
set nocount on ;

-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = '01/01/' + Convert(varchar(4), DatePart(yyyy, DateAdd(yyyy, -1, GETDATE())))

---------------------Pulling TSFL Units Data ------------------------------------
SELECT 
 [Calendar Week]
,[Calendar Year]
,sum([Units]) as Units
FROM [BI_Reporting].dbo.XL_016_TSFL_LY_TY
where [Posting Date] >= @DATE
and [Customer Posting Group] ='TSFL'
and [Type] = '2'
Group By 
 [Calendar Year]
,[Calendar Week]
Order by 
 [Calendar Year]
,[Calendar Week]