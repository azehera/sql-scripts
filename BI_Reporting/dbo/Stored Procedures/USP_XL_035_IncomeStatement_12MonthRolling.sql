﻿/*
=============================================================================================
Author:      Luc Emond
Create date: 08/12/2013
-------------------------[dbo].[USP_XL_035__IncomeStatement_12MonthRolling] -----------------------------
select last 12 months of data
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[BI_Reporting]        [dbo].[XL_035_IncomeStatementCenters]     Select 
[BI_Reporting]        DBO.calendar                              select 

===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------

==========================================================================================
*/


CREATE PROCEDURE [dbo].[USP_XL_035_IncomeStatement_12MonthRolling] as 

set nocount on;


DECLARE @MonthId Int
Set @MonthId =(SELECT CalendarMonthID FROM [BI_Reporting].DBO.calendar where CalendarDate = convert(varchar(10),GETDATE()-1,121))

SELECT
[LocationCode]
,[Region] 
,[CalendarYear] 
,[CalendarMonth] 
,[CalendarQuarter] 
,[CalendarMonthName] 
,[CalendarMonthID] 
,[Total Income] 
,[Total Cost of Goods Sold] 
,[Total Direct Costs] 
,[GrossProfit] 
,[Total Operating Costs] 
,[Total Personnel Expenses] 
,[Total Sales & Marketing Exp] 
,[Total Communication] 
,[Total Office Expenses] 
,[Total Other Expenses] 
,[Total Operating Income]
FROM [BI_Reporting].[dbo].[XL_035_IncomeStatementCenters]
where CalendarMonthID between (select @MonthId-100) and (Select Case When RIGHT(@MonthId,2) =01 then @MonthId-89 Else @MonthId-1 end )
Order By CalendarMonthId
