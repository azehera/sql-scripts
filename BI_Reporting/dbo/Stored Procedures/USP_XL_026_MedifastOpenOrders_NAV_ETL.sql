﻿

/*
==================================================================================
Author:         Luc Emond
Create date: 08/16/2013
---------------------------[USP_XL_026_MedifastOpenOrders_NAV_ETL]-------------------
---Provide open orders----This is needed since we having NAVPROD issue. 
==================================================================================   
REFERENCES
Database              Table/View/UDF                       Action            
----------------------------------------------------------------------------------
[NAVPROD]            [Jason Pharm$Sales Header]            SELECT
                        
==================================================================================
REVISION LOG
Date           Name                      Change
----------------------------------------------------------------------------------
--2015-04-17     Menkir Haile            EXECUTE AS LOGIN = 'ReportViewer' commented Out 
==================================================================================
NOTES:

----------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_026_MedifastOpenOrders_NAV_ETL] as 
Set NoCount on;


--Syntax that needs to be inserted in front of the statement that accesses the NAVPOD database
--EXECUTE AS LOGIN = 'ReportViewer'
-------------------------------------------------------

SELECT GETDATE()as ReportDateTime, *
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Header]
WHERE [Document Type]=1
and [Amount Authorized] >0
and  [Coupon_Promotion Code] <>'EXCHANGE'

