﻿
/***************************************************************************
Object: LOAD 
Date: 10/1/2019
Developer(s): Micah W.
Description: [usp_Populate_AutoShip_Snapshot_Table]
=============================================================================
MODIFICATIONS: Take a snapshot of all autoships and places that data into the BI_Reporting.dbo.Autoship_Snapshots table

=============================================================================

******************************************************************************/

CREATE PROCEDURE [dbo].[usp_Populate_AutoShip_Snapshot_Table] 

as



DECLARE @Date1 DATETIME = CAST(GETDATE() AS DATETIME);


DELETE FROM BI_Reporting.[dbo].[Autoship_Snapshots]
WHERE CAST(snapshot_date_time AS  DATE) = CAST(@Date1 AS DATE)


INSERT INTO BI_Reporting.dbo.Autoship_Snapshots
(
    mediast_customer_number,
    user_id,
    template_pk,
    template_status,
    channel,
    next_autoshiporder_date,
    order_total_price,
    snapshot_date_time
)
SELECT 	U.p_cartcustnumber AS 'mediast_customer_number',
		template.p_user AS 'user_id',
       template.PK AS 'template_pk',
       item_t0.p_active AS 'template_status',
       CAST(chanl.p_uid AS CHAR(255)) AS 'channel',
       template.p_nextautoshiporderdate AS 'next_autoshiporder_date',
       CAST(template.p_totalprice AS DECIMAL(30, 8)) AS 'order_total_price',
       GETDATE() AS 'snapshot_date_time'

FROM [Hybris_ETL].[mdf_prd_database].[cronjobs] (nolock) AS item_t0
    JOIN [Hybris_ETL].[mdf_prd_database].[orders] (nolock) AS template
        ON item_t0.p_ordertemplate = template.PK
    LEFT JOIN [Hybris_ETL].[mdf_prd_database].[cmssite] (nolock) AS chanl
        ON template.p_site = chanl.PK
	LEFT JOIN [Hybris_ETL].[mdf_prd_database].[users] U (nolock) ON U.PK = template.p_user
WHERE template.p_nextautoshiporderdate IS NOT NULL
      AND template.p_versionid IS NULL
      AND template.p_autoshipordernumber IS NULL
      AND CAST(template.p_nextautoshiporderdate AS DATE) >= @Date1



