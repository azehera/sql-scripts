﻿

CREATE PROCEDURE [dbo].[USP_XL_042_IntraDayInvoicing] As

SET NOCOUNT ON;



--EXECUTE AS LOGIN = 'ReportViewer'



SELECT 

'NAVPROD' AS [SERVER]

,[Customer Posting Group]

,b.[Posting Date]

,MONTH(b.[Posting Date]) CalMonth

,Count(Distinct([Document No_])) as OrderCounts

,SUM(Amount) Total$

,Count(*) as RowCounts



FROM NAVISION_PROD.[NAVPROD].[dbo].[Jason Pharm$Sales Invoice Line] a

Join NAVISION_PROD.[NAVPROD].[dbo].[Jason Pharm$Sales Invoice Header] b

on a.[Document No_] = b.[No_]

where b.[Posting Date] between Convert(Varchar(10), Getdate(),121) and Convert(Varchar(10), Getdate()+1,121) AND [Customer Posting Group] <>'CORPCLINIC'

group by [Customer Posting Group], b.[Posting Date], MONTH(b.[Posting Date])


