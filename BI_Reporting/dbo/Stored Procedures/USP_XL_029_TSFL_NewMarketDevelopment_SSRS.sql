﻿

/*
==========================================================================================================================
Author:         Luc Emond
Create date: 04/15/2013
-----------------------------[USP_XL_029_TSFL_NewMarketDevelopment]]------------------------------------------------------

--========================================================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
--------------------------------------------------------------------------------------------------------------------------
BI_SSAS_Cubes         dbo.DimSelltoCustomerKey                  Select      
BI_Reporting          dbo.calendar                              Select
[NAV_ETL]             [dbo].[Jason Pharm$Sales Invoice Line]    Select
[NAV_ETL]             [dbo].[Jason Pharm$Sales Invoice Header]  Select
[ODYSSEY_ETL]         [dbo].[ODYSSEY_CORE_COMMISSION_ENTRY]     Select
[ODYSSEY_ETL]         [dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY]     Select
[ODYSSEY_ETL]         [dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]    Select
[ODYSSEY_ETL]         [dbo].[ODYSSEY_CUSTOMER_ADDRESS]	        Select
[ODYSSEY_ETL]         [dbo].[ODYSSEY_CUSTOMER]                  Select
Zip_codes_deluxe       dbo.DMA_Data                             Select
BI_Reporting          dbo.XL_029_TSFL_NewMarketDevelopment      Insert
============================================================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------------------------------------------------
2013-11-14    Luc Emond                Added the ICP Data Fields 
                              [CERTIFIED_BONUS],[CUSTOMER_SUPPORT_BONUS],[FIBC],
                             and [ANSITION_ADJ] to the calculation of Monthly Earnings 

2013-11-14    Luc Emond       Changed the SelltoKey to ShipToKey for the correct DMA and to Match the SalesCube  
2015-08-10    Menkir Haile    The final Select statement is changed to fit SSRS report                     
============================================================================================================================
NOTES:
-----------------------------------------------------------------------------------------------------------------------------
==============================================================================================================================		
*/		
CREATE procedure [dbo].[USP_XL_029_TSFL_NewMarketDevelopment_SSRS]
as
Declare @StartDate Datetime
Declare @EndDate Datetime	

set @StartDate=case when Month(Getdate())=1 then (Select DATEADD(Year, DATEDIFF(Year, -1, getdate()) - 2, 0)) else (Select DATEADD(yy, DATEDIFF(yy,0,getdate()), 0)) end
set @EndDate=case when Month(Getdate())=1 then (Select DATEADD(Year, DATEDIFF(Year, -1, GETDATE())-1, -1)) else (select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))end


IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master		
CREATE TABLE #Master(		
	[Category] [varchar](20) NULL,	
	[CalMonth] [int] NULL,	
	[CalendarMonthName] [char](15) NULL,	
	[DMA Code] [varchar](50) NULL,	
	[DMA Name] [varchar](50) NULL,	
	[Number Of Order] [int] NULL,	
	[Amount] [numeric](38, 20) NULL	
)		
		
Insert into #Master		
SELECT  case when [CustomerPostingGroup]='TSFL' then 'ZTSFL' else 'ZMedifast' end  as Category 		
     ,CalendarMonth		
     ,[CalendarMonthName]		
       ,[DMA Code]		
      ,[DMA Name]		
       ,COUNT(Distinct DocumentNo) as [Number Of Order]		
      ,SUM(Amount) as Amount		
       FROM [BI_SSAS_Cubes].[dbo].[FactSales] a		
  join BI_SSAS_Cubes.dbo.DimShiptoCustomerKey b		
  on a.ShiptoCustomerKey=b.ShiptoCustomerKey		
  join BI_Reporting.dbo.calendar c		
  on a.[Posting Date]=c.CalendarDate		
  where [CustomerPostingGroup] in ('Medifast','TSFL')		
  and [Posting Date] between @StartDate and @EndDate	
  Group by [CustomerPostingGroup]		
      ,[DMA Code]		
      ,[DMA Name],Month([Posting Date]),[CalendarMonthName]	,CalendarMonth	
      Order by Month([Posting Date])		
 		
 -------------------------------'New HC'-----------------------------------		
		
INSERT  into #Master      		
SELECT 'New HC' as Category		
,CalendarMonth		
 ,CalendarMonthName		
 ,[DMA Code]		
,[DMA Name]		
,Count(Distinct B.[Sell-to Customer No_]) as Count		
,NULL 		
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A 		
Inner Join 		
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 		
     on A.[Document No_]= B.[No_]		
     join BI_SSAS_Cubes.dbo.DimSelltoCustomerKey d		
     on B.[Sell-to Customer No_]=d.SelltoCustomerID		
	Inner join [BI_Reporting].[dbo].[calendar] C 	
		on A.[Posting Date] = C.[CalendarDate]
WHERE A.[Posting Date] between @StartDate and @EndDate			
and A.[No_] in('31011','31012','32051','31100','31105','31110')		
Group by CalendarMonthName	,CalendarMonth	
,[DMA Code]		
,[DMA Name]		
		
--------------------------------'New Client'----------------------------------		
INSERT INTO #Master		
SELECT 		
'New Client' as Category		
,CalendarMonth		
 ,CalendarMonthName		
 ,[DMA Code]		
,[DMA Name]		
,Count(Distinct B.[Sell-to Customer No_]) as Count 		
,NULL		
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A 		
Inner Join 		
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 		
     on A.[Document No_]= B.[No_]		
      join BI_SSAS_Cubes.dbo.DimSelltoCustomerKey d		
     on B.[Sell-to Customer No_]=d.SelltoCustomerID		
	Inner join [BI_Reporting].[dbo].[calendar] C 	
		on A.[Posting Date] = C.[CalendarDate]
WHERE A.[Posting Date]  between @StartDate and @EndDate		
		
and A.[No_] in ('40491', '40650') 		
and A.[Amount] = '0'		
Group by CalendarMonthName,CalendarMonth		
,[DMA Code]		
,[DMA Name]		
		
		
		
IF object_id('Tempdb..#Active') Is Not Null DROP TABLE #Active		
-------------------------------------Pulling Monthly Earners with Earnings--------------------------------------------		
SELECT 		
	   A.[CUSTOMER_ID]	
	   ,YEAR(A.[COMMISSION_DATE]) AS CalYear	
	   ,MONTH(A.[COMMISSION_DATE]) AS CalMonth 	
       ,isnull([TOTAL_ADJUSTMENT],0) + isnull([GROWTH_BONUS],0) + isnull([GENERATION_BONUS],0)+ isnull([OVERRIDE_BONUS],0) + isnull([CSC_AMOUNT],0) 
       + isnull([CAB_BONUS],0) + isnull([ASSIST_BONUS],0) + ISNULL([ROLLING_BONUS],0)+ isnull([OVERRIDE_BONUS_GED],0)+ isnull([OVERRIDE_BONUS_PED],0)
       + isnull([CERTIFIED_BONUS],0)+ Isnull([CUSTOMER_SUPPORT_BONUS],0)+ Isnull([FIBC],0)+ Isnull([ANSITION_ADJ],0) as [Total Earnings]		
  Into #Active		
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] A		
	inner join[ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] B	
		on A.[COMMISSION_ENTRY_ID] = B.[COMMISSION_ENTRY_ID]
	Inner join [BI_Reporting].[dbo].[calendar] C 	
		on Convert(varchar(10),A.[COMMISSION_DATE],121) = C.[CalendarDate]
Where Convert(Varchar(10),[COMMISSION_DATE],121)  between @StartDate and @EndDate		
		
		
--------------------------Pulling Weekly Earnings for Active Earning Coaches--------------------------		
Insert into #Active		
SELECT 		
 A.[CUSTOMER_ID]		
,YEAR(A.[COMMISSION_DATE]) AS CalYear		
,MONTH(A.[COMMISSION_DATE]) AS CalMonth		
,([CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]+isnull([TOTAL_ADJUSTMENT],0)) as [Total Earnings] 		
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY] A		
  Inner join [BI_Reporting].[dbo].[calendar] C 		
		on Convert(varchar(10),A.[COMMISSION_DATE],121) = C.[CalendarDate]
Where Convert(Varchar(10),[COMMISSION_DATE],121)  between @StartDate and @EndDate			
		
		
		
----temp table for address---		
IF object_id('Tempdb..#Address') Is Not Null DROP TABLE #Address		
		
Select *  into #Address from [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS]		
where [Address_Type] = 'Main'		
		
IF object_id('Tempdb..#k') Is Not Null DROP TABLE #k		
Select		
'Active HC' as Category		
,CalYear		
,CalMonth		
,Case When CalMonth = 1 Then 'January'		
      When CalMonth = 2 Then 'February'		
      When CalMonth = 3 Then 'March'		
      When CalMonth = 4 Then 'April'		
      When CalMonth = 5 Then 'May'		
      When CalMonth = 6 Then 'June'		
      When CalMonth = 7 Then 'July'		
      When CalMonth = 8 Then 'August'		
      When CalMonth = 9 Then 'September'		
      When CalMonth = 10 Then 'October'		
      When CalMonth = 11 Then 'November'		
      When CalMonth = 12 Then 'December' End As CalendarMonthName		
,A.[CUSTOMER_ID]		
,[CUSTOMER_NUMBER]		
,C.[FIRST_NAME]		
,C.[LAST_NAME]		
,B.[POSTAL_CODE]		
,Case When [DMA Code] IS null then ' ' else [DMA Code] end as [DMA Code]		
,Case When [DMA Name] IS null then 'Unknown' else [DMA Name] end as [DMA Name]		
into #k		
From #Active A		
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C		
		on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner join #Address B	
		on C.[CUSTOMER_ID] = B. [CUSTOMER_ID]
	LEFT JOIN Zip_codes_deluxe.dbo.DMA_Data d	
		on d.ZipCode=left(B.[POSTAL_CODE],5)
	Where [CUSTOMER_NUMBER] not in('2','6','6957001','16654601','9027001','16918401','30006445','101','3','16918501')	
	and [Total Earnings] >0	
		
	
Insert into #Master		
Select 'Active HC' as Category		
,CalMonth		
, CalendarMonthName		
,Case When [DMA Code] IS null then ' ' else [DMA Code] end as [DMA Code]		
,Case When [DMA Name] IS null then 'Unknown' else [DMA Name] end as [DMA Name]		
,Count(Distinct [CUSTOMER_ID]) as [Number Of Order]		
,null from #K		
Group by CalendarMonthName,CalMonth		
,Case When [DMA Code] IS null then ' ' else [DMA Code] end 		
,Case When [DMA Name] IS null then 'Unknown' else [DMA Name] end 		
		
		
		
IF object_id('Tempdb..#l') Is Not Null DROP TABLE #l		
Select		
Category		
,Count(Distinct [CUSTOMER_NUMBER]) as DistinctCoachYtd		
,[DMA Code]		
,[DMA Name]		
into #l		
From #K 		
GROUP BY 		
Category		
,[DMA Code]		
,[DMA Name]		
		

		
Insert into #Master		
select 		
'Active HC YTD' Category		
,CalendarMonth		
,CalendarMonthName		
,[DMA Code]		
,[DMA Name]		
,DistinctCoachYtd 		
,null		
from #l a		
join BI_Reporting.dbo.calendar b		
on 
case when Month(Getdate()) IN (2,3,4,5,6,7,8,9,10,11,12) then Month(Getdate())-1 else 12 end=B.CalendarMonth		
		
Group by 		
 DistinctCoachYtd		
,[DMA Code]		
,[DMA Name]		
,CalendarMonthName		
,CalendarMonth		
		
		
Select   M.[DMA Code]
		,[DMA Name]
		,M.CalMonth
		,M.CalendarMonthName
		,SUM(Case when Category = 'New Client' then  M.[Number Of Order] end) AS [NewClient]
		,SUM(Case when Category = 'New HC' then  M.[Number Of Order] end) AS [NewHC]
		,SUM(Case when Category = 'ZMedifast' then  M.[Number Of Order] end) AS [Medifast]
		,SUM(Case when Category = 'ZTSFL' then  M.[Number Of Order] end) AS [TSFL]
		,SUM(Case when Category = 'Active HC' then  M.[Number Of Order] end) AS [ActiveHC]
		,SUM(Case when Category = 'Active HC YTD' then  M.[Number Of Order] end) AS [ActiveHC YTD]

from #Master M
Group By M.[DMA Code]
		,[DMA Name]
		,M.CalMonth
		,M.CalendarMonthName
Order By M.[DMA Code]
		,M.CalMonth
		,M.CalendarMonthName
