﻿








-- =============================================
-- Author:		Kalpesh Patel
-- Create date: 07/09/2013
-- Description:	<Description,,>
-- Modified: Per CAB #91 - 11/05/2010 (D. Smith)
-- Modified: Per CAB #119 - 12/10/2010 (D. Smith)
-- =============================================
CREATE PROCEDURE [dbo].[usp_MonthlyReportByState] 
	-- Add the parameters for the stored procedure here
	@DistCenter varchar(30),
	@StartDate datetime, 
	@EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--declare @Dept varchar(100)

	--set @Dept = 'TSFL,MEDIFAST'

	declare @OutTable table (
		[shipID] bigint
		, dept nvarchar(255)
		, [state] nvarchar(35)
		, [ZipCode] nvarchar(15)
		, [MinZip] nvarchar(10)
		, [country] nvarchar(10)
		, [CalcState] nvarchar(5)
		, [Loc] nvarchar(10)
		, pCount float)


	declare @StartValue varchar(10)
	declare @EndValue varchar(10)
	
	set @EndDate=dateadd(d,1,@EndDate)

	set @StartValue=cast(year(@StartDate) as varchar(4)) + '-' + right('0' + cast(month(@StartDate) as varchar(2)),2) + '-' + right('0' + cast(day(@StartDate) as varchar(2)),2)
	set @EndValue=cast(year(@EndDate) as varchar(4)) + '-' + right('0' + cast(month(@EndDate) as varchar(2)),2) + '-' + right('0' + cast(day(@EndDate) as varchar(2)),2)

	insert into @Outtable
	select	sh.sid
			, Reference3 as keyvalue
			, a.[state]
			, a.[postcode]
			, substring(a.[postcode],0,6) as minZip
			, a.[countrycode]
			, z.[State]
			, z.[location_cat]
	--		, p.pid
			, count([ui_sales_order_no])

	from FLAGSHIP_WMS_ETL.dbo.shipment_header sh inner join FLAGSHIP_WMS_ETL.dbo.addresses a on sh.[sid]=a.[sid]
			--left join [zip_code] z on substring(a.postcode,0,6)=z.zip 
			--Since the table zip_code contains duplicate records, need to join to the below view
			left join (select distinct zip, [state], location_cat from FLAGSHIP_WMS_ETL.dbo.zip_code) z on substring(a.postcode,0,6)=z.zip 
			inner join FLAGSHIP_WMS_ETL.dbo.[packages] p on p.[sid]=sh.sid
			--inner join [flexdata] f on sh.[sid] = f.[sid]

	where	a.[type]='SHIPTO' 
			and sh.ship_date>= @StartValue and sh.ship_date<=@EndValue
		
			--Added status condition
			and sh.[status]='UPLD'
			and sh.dc in (select * from dbo.ufn_SPLIT(@DistCenter, ','))
			
	group by sh.[sid], reference3, a.[state], a.postcode, a.CountryCode, z.[state], z.[location_cat]
	order by sh.[sid]

	update @OutTable
	set [state]='VI' where Country='VI'

	update @OutTable
	set [state]='GU' where country='GU'
	--
	update @OutTable
	set loc='FS' where loc is null 

	update @OutTable
	set Calcstate='FS' where loc= 'FS'

	--
--	select * from @OutTable
--	order by [state], loc, dept

	select dept, [Calcstate], loc, Sum(pCount) as pCount
	from @OutTable
	group by dept, [Calcstate], loc
	order by [Calcstate], [dept]

END









