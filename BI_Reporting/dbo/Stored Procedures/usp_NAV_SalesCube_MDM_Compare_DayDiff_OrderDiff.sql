﻿-- =============================================
-- Author:		Micah Williams
-- Create date: 06-05-2017
-- Description:	[usp_NAV_SalesCube_MDM_Compare] Comparing the three databases amount and order count data
-- =============================================
CREATE PROCEDURE [dbo].[usp_NAV_SalesCube_MDM_Compare_DayDiff_OrderDiff]

AS


IF OBJECT_ID('Tempdb..#NAV_prep1') IS NOT NULL DROP TABLE #NAV_prep1
SELECT H.No_ AS 'DocNo', SUM(L.Amount) AS 'Amount', H.[Posting Date] AS 'Date'
INTO #NAV_prep1
FROM NAV_ETL.dbo.[Jason Pharm$Sales Invoice Header] H
JOIN NAV_ETL.dbo.[Jason Pharm$Sales Invoice Line] L ON L.[Document No_] = H.No_
WHERE H.[Posting Date] >= DATEADD(month, -6, GETDATE())
GROUP BY H.No_,  H.[Posting Date]


IF OBJECT_ID('Tempdb..#NAV_prep2') IS NOT NULL DROP TABLE #NAV_prep2
SELECT  SUM(CRL.Amount) AS 'Amount',  CR.No_ AS 'DocNo', CR.[Posting Date] AS 'Date'
INTO #NAV_prep2
FROM NAV_ETL.dbo.[Jason Pharm$Sales Cr_Memo Header] CR 
LEFT JOIN NAV_ETL.dbo.[Jason Pharm$Sales Cr_Memo Line] CRL ON CR.No_ = CRL.[Document No_]
WHERE CR.[Posting Date] >= DATEADD(month, -6, GETDATE())
GROUP BY CR.No_ ,  CR.[Posting Date]

IF OBJECT_ID('Tempdb..#NAVETL') IS NOT NULL DROP TABLE #NAVETL
SELECT N1.Date, n1.DocNo, n1.Amount AS 'Amount'
INTO #NAVETL
FROM #NAV_prep1 N1
UNION ALL
SELECT N2.Date, n2.DocNo, (n2.Amount * -1) AS 'Amount'
FROM #NAV_prep2 N2



IF OBJECT_ID('Tempdb..#SC') IS NOT NULL DROP TABLE #SC
SELECT ([Posting Date]) AS 'Date', SUM(Amount) 'Sales_Cube_Amount',  DocumentNo AS 'DocNo'
INTO #SC
FROM BI_SSAS_Cubes.dbo.FactSales
WHERE [Posting Date] >= DATEADD(month, -6, GETDATE())
GROUP BY ([Posting Date]), DocumentNo 
ORDER BY ([Posting Date])

IF OBJECT_ID('Tempdb..#MDM') IS NOT NULL DROP TABLE #MDM
SELECT ([Posting Date]) AS 'Date', SUM(Amount) 'MDM_Cube_Amount', DocumentNo AS 'DocNo'
INTO #MDM
--FROM [MDM].[BICubes].[dbo].FactSales_MDM
FROM [BICUBES_MDM].[dbo].FactSales_MDM
WHERE [Posting Date] >= DATEADD(month, -6, GETDATE())
GROUP BY ([Posting Date]), DocumentNo 
ORDER BY ([Posting Date])



SELECT N.Date AS 'Date' ,
		N.DocNo,
        N.Amount AS 'NAVETL_Amount' ,
        S.Sales_Cube_Amount ,
        M.MDM_Cube_Amount 
FROM    #NAVETL N
        JOIN #SC S ON S.DocNo COLLATE DATABASE_DEFAULT = N.DocNo
        JOIN #MDM M ON M.DocNo COLLATE DATABASE_DEFAULT = N.DocNo
WHERE N.Amount != S.Sales_Cube_Amount OR N.Amount != M.MDM_Cube_Amount

ORDER BY N.Date DESC;


--SELECT * FROM #NAVETL
