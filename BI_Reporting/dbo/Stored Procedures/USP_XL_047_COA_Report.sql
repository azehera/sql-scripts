﻿




/*
===============================================================================
Author:         Kalpesh Patel
Create date: 01/20/2014

==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[ODYSSEY_ETL]       [dbo].[ODYSSEY_BUSINESSCENTER] 			   Select   
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                     Select  
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CORE_COMMISSION_ENTRY]        Select
[ODYSSEY_ETL]       [dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY]        Select
[ODYSSEY_ETL]       [dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]       Select
[NAV_ETL]           [dbo].[Jason Pharm$Sales Invoice Line]       Select
[NAV_ETL]           [dbo].[Jason Pharm$Sales Invoice Header]     Select
=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
2014-01-22           Luc Emond        Added a new column in Numeric for Coach Id for V_Lookup  
2014-01-28           Luc Emond        Modified and added Phone Number abd COA criteria from Emily.  
2014-02-01           Luc Emond        Added Case When for Divided by Zero to be Zero.
2014-02-12           Kalpesh Patel    Added Effective Date
====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/

CREATE procedure [dbo].[USP_XL_047_COA_Report] as 
set nocount on ;

declare @StartdatePrivtoPriv Datetime
Declare @EndatePrivtoPriv Datetime
Declare @StartDatePriv Datetime
Declare @EndDatePriv Datetime

Set @StartdatePrivtoPriv=(select DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0))
Set @EndatePrivtoPriv=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-2, -1))
Set @StartDatePriv=(select DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0))
Set @EndDatePriv=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))
------------------------------------------Pull All Coaches --------------------------------------------------------
IF object_id('Tempdb..#all') Is Not Null DROP TABLE #all

SELECT 
       [CUSTOMER_NUMBER]
      ,[FIRST_NAME]
      ,[LAST_NAME]
      ,[MAIN_PHONE] as [HC Phone]
      ,[RANK]
      ,convert(varchar(10),[COMMISSION_PERIOD], 121) as [Bonus Period]
      ,convert(varchar(10),[EFFECTIVE_DATE] , 121) as [Effective Date] ----2014-02-12 Kalpesh Patel
Into #all
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
	 inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	 Where CONVERT(varchar(10),[COMMISSION_PERIOD],121) > GETDATE() - 30
		and  CONVERT(varchar(10),[COMMISSION_PERIOD],121) < GETDATE()
		and [RANK] <> 'NONE'
		

-------------------------------------Pulling Monthly Earners with Earnings--------------------------------------------
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A

SELECT 
	   A.[CUSTOMER_ID]
      ,A.[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,(A.[TOTAL_ADJUSTMENT]+[GROWTH_BONUS]+[GENERATION_BONUS]+[OVERRIDE_BONUS]+[CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]
      +[ROLLING_BONUS]+[OVERRIDE_BONUS_GED]+[OVERRIDE_BONUS_PED]+[CERTIFIED_BONUS]+[CUSTOMER_SUPPORT_BONUS]+[FIBC]+[ANSITION_ADJ]) as [Total Earnings]
Into #a
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] A
	inner join[ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] B
		on A.[COMMISSION_ENTRY_ID] = B.[COMMISSION_ENTRY_ID]
	Inner join [BI_Reporting].[dbo].[calendar_TSFL] C 
		on Convert(varchar(10),A.[COMMISSION_DATE],121) = C.[CalendarDate]
	Where Convert(varchar(10),A.[COMMISSION_DATE],121) between @StartdatePrivtoPriv and @EndatePrivtoPriv
	Order by [COMMISSION_DATE] asc

--------------------------Pulling Weekly Earnings for Active Earning Coaches--------------------------
Insert into #a
SELECT 
	  [CUSTOMER_ID]
	 ,[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,([CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]+[TOTAL_ADJUSTMENT]) as [Total Earnings]
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY] A
  Inner join [BI_Reporting].[dbo].[calendar_TSFL] B 
		on Convert(varchar(10),A.[COMMISSION_DATE],121) = B.[CalendarDate]
  Where Convert(varchar(10),A.[COMMISSION_DATE],121) between @StartdatePrivtoPriv and @EndatePrivtoPriv
	Order by [COMMISSION_DATE] asc

----------------------------------Pull Total Monthly Earnings by Coach---------------------------------
IF object_id('Tempdb..#b') Is Not Null DROP TABLE #b

Select
	 [CUSTOMER_NUMBER]
	,[Last Day of Month]
	,sum([Total Earnings]) as [Total Prior Month Earnings]
Into #b
From #a A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		on C.[CUSTOMER_ID] = BC.[CUSTOMER_ID]
	Where [Total Earnings] > '0'
		and [CUSTOMER_NUMBER] not in('2','6','6957001','16654601','9027001','16918401','30006445','101','3','16918501')
		and Convert(varchar(10),[COMMISSION_Period],121)  between @StartdatePrivtoPriv and @EndatePrivtoPriv
	Group by 
	 [CUSTOMER_NUMBER]
	,[Last Day of Month]

------------------------------Pulling COA purchases for the month dec for jan---------------------------------
IF object_id('Tempdb..#CA') Is Not Null DROP TABLE #CA

SELECT 
 B.[Document Date]
,B.[Sell-to Customer No_],LEN(B.[Sell-to Customer No_])as Lenght, LEFT(B.[Sell-to Customer No_],4) as Short, LEFT(B.[Sell-to Customer No_],5) as Short2
,A.[Document No_] as Orders
,A.[Amount] 
Into #CA
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A 
Inner Join 
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 
     on A.[Document No_]= B.[No_]
WHERE convert(varchar(10),[Document Date],121) between @StartDatePriv and @EndDatePriv
	and [Customer Posting Group] in ('TSFL')
	and A.[No_] in('COA')
	and A.[Amount] > '0'
	
  ------------Aggregate COA purchase to one monthly total and drop TSFL prefix from ID-------------	
IF object_id('Tempdb..#COA') Is Not Null DROP TABLE #COA
select 
[Sell-to Customer No_],
Case When Short <> 'TSFL' Then [Sell-to Customer No_] 
     When Short ='TSFL' and Lenght = 14 Then Right(right ([Sell-to Customer No_],10),20)
     When Short2 ='TSFL-' and Lenght = 13 Then Right(right ([Sell-to Customer No_],8),20)
     When Short ='TSFL' and Lenght = 13 Then Right(right ([Sell-to Customer No_],9),20)
     When Short ='TSFL' and Lenght = 12 Then Right(right ([Sell-to Customer No_],8),20) 
     When Short ='TSFL' and Lenght = 11 Then Right(right ([Sell-to Customer No_],7),20) 
     When Short ='TSFL' and Lenght = 10 Then Right(right ([Sell-to Customer No_],6),20) 
     When Short ='TSFL' and Lenght =  9 Then Right(right ([Sell-to Customer No_],5),20)
     When Short ='TSFL' and Lenght =  8 Then Right(right ([Sell-to Customer No_],4),20) end as CustomerNo
  ,count(distinct([Orders])) as [COA Orders]
,sum([Amount]) as [Total COA purchased]
Into #COA  
 from #CA 
	Group by 
		[Sell-to Customer No_]
		,Lenght
		,Short
		,Short2
-----------------------Compile All data points to one Report----------------------------------------
IF object_id('Tempdb..#full') Is Not Null DROP TABLE #full -----2014-01-28  Luc Emond----
SELECT 
       A.[CUSTOMER_NUMBER]
      ,[FIRST_NAME]
      ,[LAST_NAME]
      ,[HC Phone]
      ,[RANK]
      ,[Bonus Period]
      ,[Effective Date] 
      ,isnull([Total Prior Month Earnings],'0') as [Total Prior Month Earnings]
      ,isnull([Total COA purchased],'0') as [Total COA purchased]
      ,isnull([COA Orders],'0') as [COA Orders]
Into #Full
From #all A
	left join #b B
		on A.[CUSTOMER_NUMBER] = B.[CUSTOMER_NUMBER]
	inner join #COA C
		on A.CUSTOMER_NUMBER = C.CustomerNo
Order by [Total COA purchased] desc

-------SELECT FOR THE REPORT----  -----2014-01-28  Luc Emond----
SELECT 
       [CUSTOMER_NUMBER]
      ,Convert(numeric(36,0),(A.[CUSTOMER_NUMBER])) as V_LookupCoachID
      ,[FIRST_NAME]
      ,[LAST_NAME]
      ,[HC Phone]
      ,[RANK]
      ,[Bonus Period]
      ,[Effective Date]-----2014-02-12 Kalpesh Patel 
      ,[Total Prior Month Earnings]
      ,[Total COA purchased]
      ,[COA Orders]
      ,Case When [Total Prior Month Earnings] = 0 Then 0 Else ([Total COA purchased]/[Total Prior Month Earnings]) end as [COA %]----Luc Emond 2014-02-01--
    
From #Full A
	Where [Total COA purchased] >= '400'
Order by [Total COA purchased] desc


