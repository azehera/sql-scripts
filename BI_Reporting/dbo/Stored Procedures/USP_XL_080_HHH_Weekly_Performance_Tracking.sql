﻿

CREATE procedure [dbo].[USP_XL_080_HHH_Weekly_Performance_Tracking] as

--DECLARE @DayOfWeek int
DECLARE @EndDate datetime
--SET @DayOfWeek = DATEPART(dw,GETDATE())
--SET @EndDate = CONVERT(varchar(10), DATEADD(d, 2 - @DayOfWeek, GETDATE()), 101) -- Gets Monday Date
SET @EndDate = CONVERT(varchar(10), '1/1/2015', 101) -- Temporary Fix for last run

-- #OldOrder gets UCart order prior to 10/01/2014
IF object_id('Tempdb..#OldOrder') Is Not Null DROP TABLE #OldOrder
CREATE TABLE #OldOrder (CustNo varchar(255), OrderNo nvarchar(30), OrderDate datetime, Channel varchar(20)
CONSTRAINT [PK_OldOrder] PRIMARY KEY CLUSTERED (CustNo ASC, OrderNo ASC)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]) ON [PRIMARY]

INSERT INTO #OldOrder
SELECT ISNULL(ua.USA_CUST_NBR, 0), oh.ORH_SOURCE_NBR, oh.ORH_CREATE_DT, 
CASE oh.ORH_ORG_ID 
	WHEN '12105623021813764' THEN 'WHOLESALE'
    WHEN '12105623021813765' THEN 'DIRECT'
    WHEN '12105623021813766' THEN 'TSFL'
    WHEN '12105623021813778' THEN 'DIRECT CA'
    WHEN '12105623021813780' THEN 'WHOLESALE CA'
    ELSE '' END AS CHANNEL
from [ECOMM_ETL ].dbo.V_USER_ACCOUNT ua WITH (NOLOCK)
INNER JOIN [ECOMM_ETL ].dbo.V_ORDER_HEADER oh WITH (NOLOCK) on ua.USA_ID = oh.ORH_CREATED_FOR
where oh.ORH_CREATE_DT < '10/01/2014' AND oh.ORH_TOTAL_AMT > 0

-- #NewOrder gets UCart order >= 10/01/2014 excluding Channel TSFL
IF object_id('Tempdb..#NewOrder') Is Not Null DROP TABLE #NewOrder
CREATE TABLE #NewOrder (CustNo varchar(255), OrderNo nvarchar(30), OrderDate datetime, Channel varchar(20)
CONSTRAINT [PK_NewOrder] PRIMARY KEY CLUSTERED (CustNo ASC, OrderNo ASC)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]) ON [PRIMARY]

INSERT INTO #NewOrder
SELECT ISNULL(ua.USA_CUST_NBR, 0), oh.ORH_SOURCE_NBR, oh.ORH_CREATE_DT, 
CASE oh.ORH_ORG_ID 
	WHEN '12105623021813764' THEN 'WHOLESALE'
    WHEN '12105623021813765' THEN 'DIRECT'
    WHEN '12105623021813766' THEN 'TSFL'
    WHEN '12105623021813778' THEN 'DIRECT CA'
    WHEN '12105623021813780' THEN 'WHOLESALE CA'
    ELSE '' END AS CHANNEL
from [ECOMM_ETL ].dbo.V_USER_ACCOUNT ua WITH (NOLOCK)
INNER JOIN [ECOMM_ETL ].dbo.V_ORDER_HEADER oh WITH (NOLOCK) on ua.USA_ID = oh.ORH_CREATED_FOR
where oh.ORH_CREATE_DT >= '10/01/2014' AND oh.ORH_TOTAL_AMT > 0 and oh.ORH_ORG_ID <> '12105623021813766'

-- #COAOrder gets UCart order >= 10/01/2014 for Channel TSFL with COA amounts
IF object_id('Tempdb..#COAOrder') Is Not Null DROP TABLE #COAOrder
CREATE TABLE #COAOrder (CustNo varchar(255), OrderNo nvarchar(30), COAAmount numeric(18,4)
CONSTRAINT [PK_COAOrder] PRIMARY KEY CLUSTERED (CustNo ASC, OrderNo ASC)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]) ON [PRIMARY]

INSERT INTO #COAOrder
SELECT ISNULL(ua.USA_CUST_NBR, 0), oh.ORH_SOURCE_NBR, SUM(a.ACT_ORIG_BALANCE)
from [ECOMM_ETL ].dbo.V_USER_ACCOUNT ua WITH (NOLOCK)
INNER JOIN [ECOMM_ETL ].dbo.V_ORDER_HEADER oh WITH (NOLOCK) on ua.USA_ID = oh.ORH_CREATED_FOR
LEFT JOIN [ECOMM_ETL ].dbo.PAYMENT p WITH (NOLOCK) on oh.ORH_ID = p.PAY_ORH_ID
LEFT JOIN [ECOMM_ETL ].dbo.ACCOUNT a WITH (NOLOCK) on p.PAY_TYPE_ID = a.ACT_ID 
where oh.ORH_CREATE_DT >= '10/01/2014' AND oh.ORH_TOTAL_AMT > 0 and oh.ORH_ORG_ID = '12105623021813766' AND p.PAY_TYPE_CD = 'ACT'
GROUP BY ISNULL(ua.USA_CUST_NBR, 0), oh.ORH_SOURCE_NBR

IF object_id('Tempdb..#FirstOrder') Is Not Null DROP TABLE #FirstOrder
CREATE TABLE #FirstOrder (CustId numeric(9,0), CustNo varchar(255), OrderNo varchar(30), FirstOrderDate datetime
CONSTRAINT [PK_FirstOrder] PRIMARY KEY CLUSTERED (CustNo ASC, OrderNo ASC)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]) ON [PRIMARY]

-- Gets the first order number and date for TSFL orders entered >= 10/01/2014 and total > 0
INSERT INTO #FirstOrder
SELECT o.CUSTOMER_ID, c.CUSTOMER_NUMBER, MIN(o.MASTER_ID) [OrderId], MIN(o.ENTRY_DATE)
FROM ODYSSEY_ETL.dbo.ODYSSEY_ORDERS O WITH (NOLOCK) 
INNER JOIN ODYSSEY_ETL.dbo.ODYSSEY_CUSTOMER c on O.CUSTOMER_ID = c.CUSTOMER_ID
WHERE o.ENTRY_DATE >= '10/01/2014' AND o.ENTRY_DATE < @EndDate AND o.ORDER_TOTAL > 0
GROUP BY o.CUSTOMER_ID, c.CUSTOMER_NUMBER 
ORDER BY o.CUSTOMER_ID, c.CUSTOMER_NUMBER

-- Gets orders with total dollars >= 250, eliminating existing customers in #OldOrder
IF object_id('Tempdb..#Orders') Is Not Null DROP TABLE #Orders
CREATE TABLE #Orders (CustId numeric(9,0), CustNo varchar(20), OrderNo nvarchar(30), 
	OrderDate datetime, OrderTotal numeric(18,4), COA numeric(18,4), SponsorId varchar(20), 
		OctOrderCount integer, NovOrderCount integer, DecOrderCount integer)
		
INSERT INTO #Orders
SELECT o.CUSTOMER_ID, cl.CUSTOMER_NUMBER, o.MASTER_ID, o.ENTRY_DATE, o.ORDER_TOTAL, ISNULL(coa.COAAmount, 0),
REPLACE(CONVERT(varchar(20),cl.INITIAL_PLACEMENT), '.001', ''),
CASE WHEN o.ENTRY_DATE >= '10/01/2014' AND o.ENTRY_DATE < '11/01/2014' THEN 1 ELSE 0 END,
CASE WHEN o.ENTRY_DATE >= '11/01/2014' AND o.ENTRY_DATE < '12/01/2014' THEN 1 ELSE 0 END,
CASE WHEN o.ENTRY_DATE >= '12/01/2014' AND o.ENTRY_DATE < '01/01/2015' THEN 1 ELSE 0 END
FROM ODYSSEY_ETL.dbo.ODYSSEY_ORDERS O WITH (NOLOCK) 
INNER JOIN ODYSSEY_ETL.dbo.ODYSSEY_CUSTOMER cl WITH (NOLOCK) on o.CUSTOMER_ID = cl.CUSTOMER_ID
INNER JOIN #FirstOrder f on cl.CUSTOMER_NUMBER = f.CustNo and o.MASTER_ID = f.OrderNo
LEFT JOIN #COAOrder coa on o.MASTER_ID = coa.OrderNo
LEFT JOIN #OldOrder oo on cl.CUSTOMER_NUMBER = oo.CustNo
WHERE o.ORDER_TOTAL + ISNULL(coa.COAAmount, 0) >= 250 AND o.ENTRY_DATE >= '10/01/2014' AND oo.CustNo IS NULL
OPTION (MAXDOP 1)

-- The follow statement removes rows from #Orders based on join to #NewOrder, comparing order numbers
DELETE o FROM #Orders o INNER JOIN #NewOrder n on o.CustNo = n.CustNo WHERE o.OrderNo > n.OrderNo

-- The following SELECT gets the health coach data from Odyssey.  Used ODYSSEY_BUSINESSCENTER tables to get sponsor id.
SELECT distinct s.CUSTOMER_NUMBER [Health Coach Id],
ISNULL(s.[FIRST_NAME],'') As [HC First Name], 
ISNULL(s.[LAST_NAME],'') as [HC Last Name],
ISNULL(s.[LAST_NAME],'') + ', ' + ISNULL(s.[FIRST_NAME],'') as [HC Full Name],
ISNULL(s.RECOGNITION_NAME,'') as [HC Recognition Name],
s.[EMAIL1_ADDRESS] as [HC Email Address],
CONVERT(nvarchar(10), ISNULL(s.EFFECTIVE_DATE, ''), 101) [HC Activation Date],
cl.[CUSTOMER_NUMBER] [Client Id],
o.CustId, o.SponsorId,
ISNULL(cl.[FIRST_NAME],'') As [Client First Name], 
ISNULL(cl.[LAST_NAME],'') as [Client Last Name],
o.OrderNo, o.OrderDate, o.OrderTotal [OrderAmount], o.COA, o.OrderTotal + o.COA [OrderTotal],
(o.OrderTotal + o.COA) / (o.OctOrderCount + o.NovOrderCount + o.DecOrderCount) [Average Order],
o.OctOrderCount, o.NovOrderCount, o.DecOrderCount, o.OctOrderCount + o.NovOrderCount + o.DecOrderCount [Total Orders]
FROM #Orders o LEFT JOIN ODYSSEY_ETL.dbo.ODYSSEY_CUSTOMER cl WITH (NOLOCK) on cl.CUSTOMER_ID = o.CustId
LEFT JOIN ODYSSEY_ETL.dbo.ODYSSEY_CUSTOMER s on o.SponsorId = s.CUSTOMER_ID
left join [ODYSSEY_ETL].[dbo].ODYSSEY_BUSINESSCENTER b1 WITH (NOLOCK)
on cl.CUSTOMER_ID = b1.CUSTOMER_ID
left join [ODYSSEY_ETL].[dbo].ODYSSEY_BUSINESSCENTER b2 WITH (NOLOCK)
on b1.SPONSOR_ID = b2.BUSINESSCENTER_ID 
WHERE o.OctOrderCount + o.NovOrderCount + o.DecOrderCount > 0 AND NOT s.EFFECTIVE_DATE IS NULL 
ORDER BY ISNULL(s.[LAST_NAME],'') + ', ' + ISNULL(s.[FIRST_NAME],'')


