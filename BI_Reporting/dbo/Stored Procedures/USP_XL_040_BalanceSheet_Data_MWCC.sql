﻿

CREATE Procedure [dbo].[USP_XL_040_BalanceSheet_Data_MWCC]
as 

----------fixed the error for was missing july in the overall calculation----------LUC EMOND OCTOBER 07 2013-----


---------delete and re-insert 2 months---------remove because it doesn't work correctly---addded script for first of year instead----LUC EMOND OCTOBER 07 2013
----delete from BI_Reporting.dbo.XL_040_BalanceSheet
----where [CalendarMonth]>MONTH(Getdate())-2

--EXECUTE AS LOGIN = 'ReportViewer'

---------------------------------------2013 Jason Pharm-----------
Truncate table BI_Reporting.dbo.XL_040_BalanceSheet_MWCC



-----------------------------------------------Jason Prop----------------------------------------------------------------
Insert into BI_Reporting.dbo.XL_040_BalanceSheet_MWCC
SELECT 
'Jason Prop' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
, [Global Dimension 1 Code]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Properties$G_L Entry]  a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_] 
   join BI_Reporting.dbo.calendar c
  on [Posting Date] =c.CalendarDate  
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
and a.[G_L Account No_] <>'34400'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,CalendarMonthName
,[CalendarMonth]
, [Global Dimension 1 Code]

Insert into BI_Reporting.dbo.XL_040_BalanceSheet_MWCC 
SELECT 
'Jason Prop' as Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,' ' as [Global Dimension 1 Code]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
  FROM [NAV_ETL].[dbo].[Jason Properties$G_L Entry]  a
   join BI_Reporting.dbo.calendar c
  on [Posting Date] =c.CalendarDate   
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '40000' AND '99999'
Group by
CalendarMonthName
,[CalendarMonth]
 
Insert into BI_Reporting.dbo.XL_040_BalanceSheet_MWCC
Select [Entity]
      ,[G_L Account No_]
      ,[G_L Name]
      ,[RollUpName]
      ,[Global Dimension 1 Code]
      ,13 as [CalendarMonth]
      ,'End Of 2013' as CalendarMonthName
      ,SUM(Amount) as Amount
      from dbo.XL_040_LY_Q4_MWCC
      group by [Entity]
      ,[G_L Account No_]
      ,[G_L Name]
      ,[RollUpName]
      ,[Global Dimension 1 Code]

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet_MWCC
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].dbo.[Jason Properties$G_L Account] d
WHERE XL_040_BalanceSheet_MWCC.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.XL_040_BalanceSheet_MWCC where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet_MWCC SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet_MWCC SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet_MWCC SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet_MWCC SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')


SELECT 
Case When [G_L Account No_] Between '10000' and '19999' Then 'Assets'
     When [G_L Account No_] Between '20000' and '29999' Then 'Liabilities'
     When [G_L Account No_] Between '30000' and '39999' Then 'Equity'
     Else 'Unknow' End As Category
,a.*,b.LocationCode
FROM BI_Reporting.dbo.XL_040_BalanceSheet_MWCC a
left join BI_SSAS_Cubes.dbo.DimLocation b
on a.[Global Dimension 1 Code]=Convert(Varchar(50),b.LocationID)















