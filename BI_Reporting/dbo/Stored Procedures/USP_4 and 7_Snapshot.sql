﻿
CREATE PROCEDURE [dbo].[USP_4 and 7_Snapshot]
AS
DECLARE @Snapshottime DATETIME;
DECLARE @StartDate DATE;
DECLARE @EndDate DATE;
DECLARE @forecastfourday DATE;
DECLARE @forecastSevenday DATE;
DECLARE @twoyr DATE;

SET @StartDate = GETDATE();
SET @EndDate = DATEADD(DAY, 29, @StartDate);
SET @forecastfourday = DATEADD(DAY, 4, @StartDate); 
SET @forecastSevenday = DATEADD(DAY, 7, @StartDate);
SET @twoyr = DATEADD(yy, -2, @StartDate);
SET @Snapshottime = GETDATE()



IF OBJECT_ID('tempdb..#Aship') IS NOT NULL
    DROP TABLE #Aship;
SELECT 'Client' Customer_Type,
       CAST([nextautoshiporderdate] AS DATE) AS OrderDate,
       th.channel,
       COUNT(*) AS TotalCount
INTO #Aship
FROM [Hybris_ETL].[dbo].[vw_Template_header_prd] th WITH (NOLOCK)
WHERE [nextautoshiporderdate]
      BETWEEN @StartDate AND @EndDate
      AND templatestatus = '1'
      AND th.channel <> 'Medifast'
GROUP BY CAST([nextautoshiporderdate] AS DATE),
         th.channel
UNION
SELECT 'Coach' Customer_Type,
       CAST([nextautoshiporderdate] AS DATE) AS OrderDate,
       th.channel,
       COUNT(*) AS TotalCount
FROM [Hybris_ETL].[dbo].[vw_Template_header_prd] th WITH (NOLOCK)
WHERE [nextautoshiporderdate]
      BETWEEN @StartDate AND @EndDate
      AND templatestatus = '1'
      AND th.channel <> 'Medifast'
GROUP BY CAST([nextautoshiporderdate] AS DATE),
         th.channel
UNION
SELECT 'Client' Customer_Type, ----since we don't have coaches in medifast channel
       CAST([nextautoshiporderdate] AS DATE) AS OrderDate,
       th.channel,
       COUNT(*) AS TotalCount
FROM [Hybris_ETL].[dbo].[vw_Template_header_prd] th WITH (NOLOCK)
WHERE [nextautoshiporderdate]
      BETWEEN @StartDate AND @EndDate
      AND templatestatus = '1'
      AND th.channel = 'Medifast'
GROUP BY CAST([nextautoshiporderdate] AS DATE),
         th.channel;

--IF OBJECT_ID('tempdb..#Aship') IS NOT NULL
--    DROP TABLE #Aship;
--SELECT CASE
--           WHEN c.CustomerTypeID = 1 THEN
--               'Coach'
--           WHEN c.CustomerTypeID = 2 THEN
--               'Client'
--       END AS Customer_Type,
--       CAST([nextautoshiporderdate] AS DATE) AS OrderDate,
--       th.channel,
--       COUNT(*) AS TotalCount
--INTO #Aship
--FROM [dp-bidbprod].[Hybris_ETL].[dbo].[vw_Template_header_prd] th
--    LEFT JOIN HYBRIS_ETL.mdf_prd_database.users u
--        ON th.user_id = u.PK
--    LEFT JOIN [dp-bidbprod].EXIGO_ETL.dbo.Customers c
--        ON u.p_cartcustnumber = c.Field1
--WHERE [nextautoshiporderdate]
--      BETWEEN @StartDate AND @EndDate
--      AND templatestatus = '1'
--GROUP BY CAST([nextautoshiporderdate] AS DATE),
--         DATENAME(DW, [nextautoshiporderdate]),
--         th.channel,
--         CASE
--             WHEN c.CustomerTypeID = 1 THEN
--                 'Coach'
--             WHEN c.CustomerTypeID = 2 THEN
--                 'Client'
--         END;

----Delete today's data if the snapshot run multiple times
DELETE FROM [BI_Reporting].[dbo].[Autoship_Channel_Count_Snapshot]
WHERE Expectation_Day = @StartDate;

INSERT INTO [BI_Reporting].[dbo].[Autoship_Channel_Count_Snapshot]
(
    [Snapshot_Date],
    [Expectation_Day],
    [Channel],
    [4 or 7],
    [Expected],
    [Customer_Type]
)
SELECT @Snapshottime AS Snapshot_Date,
       a.Expectation_Day,
       a.Channel,
       a.[4 or 7],
       a.Expected,
       a.Customer_Type
FROM
(
    SELECT @StartDate Expectation_Day,
           channel,
           '7' AS [4 or 7],
           TotalCount AS Expected,
           Customer_Type
    FROM #Aship
    WHERE OrderDate = @forecastSevenday
          AND channel IN ( 'Optavia - US', 'Medifast','Optavia - HK','Optavia - SG' )
    UNION
    SELECT @StartDate Expectation_Day,
           channel,
           '4' AS [4 or 7],
           TotalCount AS Expected,
           Customer_Type
    FROM #Aship
    WHERE OrderDate = @forecastfourday
          AND channel IN ( 'Optavia - US', 'Medifast','Optavia - HK','Optavia - SG' )
) a;

---Delete Historical data more than 2 years old
DELETE FROM [BI_Reporting].[dbo].[Autoship_Channel_Count_Snapshot]
WHERE Expectation_Day <= @twoyr;



