﻿


/*
--==================================================================================
--Author:         Daniel D.
--Create date: 05/29/2019

--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--[NAVPROD]		        [Jason Pharm$Sales Header]				  Select                     
--==================================================================================
--REVISION LOG
--Date           Name               Change
------------------------------------------------------------------------------------
06-18-2019		Micah Williams		
6-26-2019 - Per Judy's request, removed DNS shipment status

--======================================================================================
--NOTES:
----------------------------------------------------------------------------------------
This sproc is used to pull unshipped ordes' count from different distribution center.
--======================================================================================
*/

CREATE PROCEDURE [dbo].[USP_Get_Unshipped_Orders_Details_bckp_0429]
AS
SET NOCOUNT ON;

SELECT distinct (No_) as 'Sales Order Number',
       [Order Date],
       [Location Code] DC,
	   [External Document No_] as 'Order Number',
	   CASE WHEN [Sell-to Customer No_] LIKE 'TSFL-%' THEN SUBSTRING([Sell-to Customer No_],6,20)
                     WHEN [Sell-to Customer No_] LIKE 'TSFL%' THEN SUBSTRING([Sell-to Customer No_],5,20)
                                  ELSE [Sell-to Customer No_] END AS [Sell-to Customer No_],
		C.Email
		,sh.[Customer Posting Group]
	  ,sh.[Ship-to County] as 'Ship State'
	  ,sh.[Ship-to Post Code] as 'Ship Zip'
	  ,z1.Latitude [Ship Latitude]
	  ,z1.Longitude [Ship Longitude]
	  ,CASE WHEN sh.[Location Code] ='MDCWMS' THEN '21660' WHEN sh.[Location Code] = 'RDCWMS' THEN '89434' END AS 'WarehouseZip' 
	  ,z2.Latitude  [Warehouse Latitude]
	  ,z2.Longitude [Warehouse Longitude]
	  ,z1.State AS ZipDeluxeState
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Header] SH
left join EXIGO.ExigoSyncSQL_PRD.CUSTOM.Order_Header OH WITH (NOLOCK) on OH.Order_No = SH.[External Document No_] collate database_default
left join EXIGO.ExigoSyncSQL_PRD.dbo.customers C WITH (NOLOCK) on C.Field1 = OH.Medifast_Customer_Number
 LEFT JOIN [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] z1 WITH (NOLOCK)   ON LEFT(sh.[Ship-to Post Code],5) COLLATE DATABASE_DEFAULT  = z1.[ZipCode] AND sh.[Ship-to County] COLLATE DATABASE_DEFAULT  = z1.[State] AND z1.PrimaryRecord = 'P'
 LEFT JOIN Zip_codes_deluxe.dbo.ZipCodeDatabase_DELUXE z2 WITH (NOLOCK) ON z2.ZipCode = CASE WHEN sh.[Location Code] ='MDCWMS' THEN '21660' WHEN sh.[Location Code] = 'RDCWMS' THEN '89434' END  AND z2.PrimaryRecord = 'P'
WHERE [Document Type] = 1 AND SH.[Shipment Method Code] <> 'DNS'
ORDER BY [Location Code],
         [Order Date];

