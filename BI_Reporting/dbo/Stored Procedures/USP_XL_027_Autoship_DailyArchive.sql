﻿





/*
=============================================================================================
Author:         Luc Emond
Create date: 04/01/2012
-------------------------[dbo].[USP_XL_027_Autoship_DailyArchive] -----------------------------
Sales Tracker by Month----
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------

[BI_Reporting]        [USP_XL_027_Autoship_DailyArchive]          Insert                         
===========================================================================================
REVISION LOG
Date           Name              Change
-----------------------------------------------------------------------------------------
1/18/2016      Menkir Haile		The Select ststement is removed

Job: Run after Sales Cube job finished at 6:30 am

3/8/2019      Micah Williams     Added OrderCount the insert statement 
==========================================================================================
*/
CREATE PROCEDURE [dbo].[USP_XL_027_Autoship_DailyArchive] 
@Today datetime
AS 
SET NOCOUNT ON;


Set @Today = (select Convert(Varchar(10), Getdate()+1,121) ) 

---in case this run more than 1 time in one day------
DELETE [BI_Reporting].[dbo].[XL_027_Autoship_DailyArchive]
Where ReportDate = @Today

---insert new data-------
INSERT Into [bi_reporting].DBO.XL_027_Autoship_DailyArchive
Select 
 Convert(Varchar(10), Getdate()+1,121) as ReportDate
,CartStatus
,Convert(Varchar(10), CalendarDate,121) CalendarDate
,CalendarYear
,CalendarMonth
,CalendarMonthName
,[TypeName]
,SUM(Amount) as Amount
,COUNT(DISTINCT CTH_ID) AS OrderCount
From [bi_reporting].DBO.XL_027_Autoship_Data
Group By
CartStatus
,CalendarDate 
,CalendarYear
,CalendarMonth
,CalendarMonthName
,[TypeName]

--Select COUNT(*) as CountRows, GETDATE() as Date from [bi_reporting].DBO.XL_027_Autoship_DailyArchive






