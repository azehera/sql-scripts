﻿






--/*
--==================================================================================
--Author:         Emily Trainor
--Create date: 12/28/2012
-------------------------------TSFL Sales Figures Report---------------------------
-------------Calculate sales figures needed for TSFL Commission Analysis-----------
--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--ODYSSEY_ETL			ODYSSEY_ORDERS						      Select
--ODYSSEY_ETL			ODYSSEY_ORDER_PRODUCT				      Select   
--NAV_ETL				Jason Pharm$G_L Entry				      Select                      
--==================================================================================
--REVISION LOG
--Date           Name                          Change
------------------------------------------------------------------------------------

--==================================================================================
--NOTES:
------------------------------------------------------------------------------------
--==================================================================================
--*/
CREATE procedure [dbo].[USP_XL_021_TSFLDailySales] as
set nocount on ;

--EXECUTE AS LOGIN = 'ReportViewer'
----------------Declare date (report pulls for previous commission week)------------------

Declare @date datetime
Set @date = convert(varchar(10),GETDATE()-12,121)

-----------------------------Pull all products order in last week-------------------------

IF object_id('Tempdb..#sales') Is Not Null DROP TABLE #sales
SELECT 
	   convert(varchar(10),[ENTRY_DATE],121) as [Entry_Date]
	  ,A.[Master_ID]
	  ,A.[Order_ID]
	  ,B.[Product_ID]
      ,case when [COM_VOLUME] = '0' then 'Non-Commissionable'
		else 'Commissionable' end as [Type]
	  ,case when [Leads_Order] = '1' then 'Lead Order'
		else 'Non-lead Order' end as [Lead]
      ,[TOTAL_PRICE]
Into #Sales
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_ORDERS] A
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_ORDER_PRODUCT]B
		on A.[Order_ID] = B.[Order_ID]
	Where convert(varchar(10),[ENTRY_DATE],121) >= @date


-----------------------Pull total sales including COA usage (payments)----------------
IF object_id('Tempdb..#a') Is Not Null DROP TABLE #a
Select
	 [ENTRY_DATE]
	,[Lead]
	,SUM([TOTAL_PRICE]) as [Total Sales]
Into #a
From #Sales
	Group by 
		[ENTRY_DATE]
		,[Lead]
	Order by [ENTRY_DATE]
			,[Lead]
			
-----------------------Pull non-commissionable sales less COA usage (payments)----------------
IF object_id('Tempdb..#b') Is Not Null DROP TABLE #b
Select
	 [ENTRY_DATE]
	,[Lead]
	,SUM([TOTAL_PRICE]) as [Non-Commissionable Sales]
Into #b
From #Sales
	Where [Type] = 'Non-Commissionable'
		and [TOTAL_PRICE] > '0'
	Group by 
		[ENTRY_DATE]
		,[Lead]
	Order by [ENTRY_DATE]
			,[Lead]

---------------------------Pull Return and Exchange Data---------------------
IF object_id('Tempdb..#returns') Is Not Null DROP TABLE #returns
IF object_id('Tempdb..#z') Is Not Null DROP TABLE #z
SELECT
       [Posting Date]
      ,[Document No_]
      ,[Description]
      ,[Amount]
      ,case when right([External Document No_],2) = 'ex' then 'exchange'
	  else 'return' end as [Return Type]
into #z
  FROM NAV_ETL.[dbo].[Jason Pharm$G_L Entry]
		Where [G_L Account No_] = '49995'
			and [Posting Date] > = @Date

Select
	 [Posting Date]
	,(SUM([Amount])*-1) as [Total Returns]
Into #returns
From #z
Where [Return Type] = 'return'
	Group by 
		 [Posting Date]
		,[Return Type]
	Order by 
		[Posting Date]
---------------------------------Pull data together into one view--------------------------------
IF object_id('Tempdb..#leadsales') Is Not Null DROP TABLE #leadsales
Select
	 A.[ENTRY_DATE]
	,A.[Lead]
	,[Total Sales]
	,[Non-Commissionable Sales]
	,([Total Sales] - [Non-Commissionable Sales]) as [Lead Sales]
Into #LeadSales
From #a	A
	Left Join #b B
		on A.[Entry_Date] = B.[Entry_Date]
		and A.Lead = B.Lead
	Where A.[Lead] = 'Lead Order'
	

	
--------------------------------Pull only Data used------------------------------------
Select
	 A.[ENTRY_DATE]
	,A.[Lead]
	,convert(numeric(15,2),(A.[Total Sales]+ C.[Lead Sales])) as [Total Sales]
	,convert(numeric(15,2),B.[Non-Commissionable Sales]) as [Non-Commissionable Sales]
	,convert(numeric(15,2),isnull([Total Returns],0)) as [Total Returns]
From #a	A
	Left Join #b B
		on A.[Entry_Date] = B.[Entry_Date]
		and A.Lead = B.Lead
	left join #LeadSales C
		on A.[Entry_Date] = C.Entry_Date
	left join #returns R
		on CONVERT(varchar(10),A.[Entry_Date],121) = R.[Posting Date]
	Where A.[Lead] = 'Non-lead Order'
		and B.[Lead] = 'Non-lead Order'	





