﻿



--/*
--==================================================================================
--Author:         Emily Trainor
--Create date: 01/09/2013
---------------------------TSFL Leader and FIBC Analysis (Monthly)------------------
--------Pull growth information related to qualifying FIBC and leader accounts------
--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--ODYSSEY_ETL			ODYSSEY_CUSTOMER					      Select
--ODYSSEY_ETL			ODYSSEY_BUSINESSCENTER				      Select   
--ODYSSEY_ETL			ODYSSEY_VOLUME						      Select    
--etrainor				TSFL - Active Earners				      Select       
--ODYSSEY_ETL			ODYSSEY_CORE_COMMISSION_ENTRY             Select 
--ODYSSEY_ETL			ODYSSEY_TSFL_COMMISSION_ENTRY             Select  
--ODYSSEY_ETL			ODYSSEY_TSFL_COMM_ENTRY_WEEKLY            Select  
--ODYSSEY_ETL			ODYSSEY_CUSTOMER_ADDRESS				  Select
--ODYSSEY_ETL			ODYSSEY_RANK_SCHEME_DETAILS				  Select
--==================================================================================
--REVISION LOG
--Date           Name                          Change
------------------------------------------------------------------------------------
--2015-04-17     Menkir Haile                DB-BIDB server reference is removed
--2015-09-14	 Menkir Haile				 The Second Performing Rank is changed to
--											 Performing Rank Desc
----==================================================================================
----NOTES:
--------------------------------------------------------------------------------------
----==================================================================================
--*/

CREATE procedure [dbo].[USP_XL_046_TSFL_Leader_FIBC_SSRS] as 
set nocount on ;

-----------------------------------------Pulling Coaches Ranking ED and Higher------------------------------

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select 
	 [CUSTOMER_NUMBER] as [HC ID #]
	,B.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,(B.[First_Name]+' '+ B.[Last_Name]) as [CoachName]
	,B.[MAIN_PHONE] as [Phone]
	,convert(varchar(10),[COMMISSION_PERIOD],121) as [CommPeriod]
	,[HIGHEST_ACHIEVED_RANK]
	,[Rank] as [Performing Rank]
	,B.[EMAIL1_ADDRESS] as [Email]
	,CONVERT(varchar(10),Getdate(),121) as [Today]
	,[EFFECTIVE_DATE] as [Activation Date]
into #a
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
		on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Where convert(varchar(10),[COMMISSION_PERIOD],121) >= (GETDATE()-31)
		and convert(varchar(10),[COMMISSION_PERIOD],121) < GETDATE()-1
		and [CUSTOMER_NUMBER]<> '101'
		and [RANK]  in ('Presidential_Director','Global_Director','National_Director','Regional_Director','Executive_Director','FI_Executive_Director',
		'FI_Regional_Director','FI_Global_Director','FI_National_Director','FI_Presidential_Director')
		
-----------------------------Pulling Rank Up Date---------------------------------------------

IF object_id('Tempdb..#Rankups') Is Not Null DROP TABLE #Rankups
SELECT 
	  A.[CUSTOMER_NUMBER] as [Coach ID]
     ,max(convert(varchar(10),C.[COMMISSION_PERIOD],121)) as [Rank Up Month]
Into #Rankups
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
			on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
Where  [RANK_ADVANCE_DATE] = [COMMISSION_PERIOD]
		and [HIGHEST_ACHIEVED_RANK] not in ('None','HEALTH_COACH','FT_HEALTH_COACH')
Group by 
		  A.[CUSTOMER_NUMBER]

-----------------------------------------Pulling Senior Coach Team Info-------------------------------------

-----------------------------------Pull Teams by sponsor---------------------
IF object_id('Tempdb..#z') Is Not Null DROP TABLE #z
SELECT 
	   [SPONSOR_ID]
	  ,convert(varchar(10),[COMMISSION_PERIOD],121) as [Comm Period]
      ,sum([IS_SENIOR_COACH_LEG]) as [SC Teams]
      ,SUM([IS_EXEC_DIR_LEG]) as [ED Teams]
Into #z
  FROM  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on B.[Customer_ID] = C.[Customer_ID]
		Group by [SPONSOR_ID],[COMMISSION_PERIOD]

----------------------Pull Sponsor ID # and Name-----------------
IF object_id('Tempdb..#legs') Is Not Null DROP TABLE #legs
Select 
      C.[CUSTOMER_NUMBER] as [Dist ID]
      ,(C.[FIRST_NAME]+' '+C.[LAST_NAME]) as [Name]
      ,[Comm Period]
      ,isnull([SC Teams],'0') as [SC Teams]
      ,isnull([ED Teams],'0') as [ED Teams]
    Into #legs
From #z A
Inner join  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
      on A.[SPONSOR_ID] = B.[BUSINESSCENTER_ID]
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
	  on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]
---------------------------Pulling All Team Data Points-----------------------	  
	  
IF object_id('Tempdb..#structure') Is Not Null DROP TABLE #structure
Select
	 [HC ID #]
	,[CoachName]
	,[Customer_ID]
	,[BUSINESSCENTER_ID]
	,[Phone]
	,[Email]
	,[HIGHEST_ACHIEVED_RANK]
	,[Performing Rank]
	,[SC Teams] as [SCT]
	,[ED Teams] as [EDT]
Into #Structure
From #a A
	left join #legs B
		on A.[HC ID #] = B.[Dist ID]
	Where B.[Comm Period] >= (GETDATE()-31)
		and B.[Comm Period] < GETDATE()-1
	Group by 
		[HC ID #]
	,[CoachName]
	,[Customer_ID]
	,[BUSINESSCENTER_ID]
	,[HIGHEST_ACHIEVED_RANK]
	,[Performing Rank]
	,[Phone]
	,[Email]
	,[SC Teams]
	,[ED Teams]
-----------------------------------------Pulling Frontline Volume information------------------------------

IF object_id('Tempdb..#FLV') Is Not Null DROP TABLE #FLV		
Select
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[HIGHEST_ACHIEVED_RANK]
	,[Phone]
	,[Email]
	,CONVERT(varchar(10),[PERIOD_END_DATE],121) as [Comm Period]
	,[Performing Rank]
	,[SCT]
	,[EDT]
	,sum([VOLUME_VALUE]) as [FLV]
into #FLV
From #Structure C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
	Where D.[VOLUME_TYPE] ='FV'
		and convert(varchar(10),D.[PERIOD_END_DATE],121) >= (GETDATE()-31)
		and convert(varchar(10),D.[PERIOD_END_DATE],121) < GETDATE()-1
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[Performing Rank]
	,[SCT]
	,[EDT]
	,[HIGHEST_ACHIEVED_RANK]
	,[PERIOD_END_DATE]
	,[Phone]
	,[Email]

-----------------------------------------Pulling Group Volume information------------------------------

IF object_id('Tempdb..#all') Is Not Null DROP TABLE #all
Select
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[Comm Period]
	,[Phone]
	,[Email]
	,[HIGHEST_ACHIEVED_RANK]
	,[Performing Rank]
	,[SCT]
	,[EDT]
	,[FLV]
	,sum (D.[VOLUME_VALUE]) as [GV]
into #all
From #FLV C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
		and C.[Comm Period] =  convert(varchar(10),D.[PERIOD_END_DATE],121)
	Where D.[VOLUME_TYPE] ='GV'
		and convert(varchar(10),D.[PERIOD_END_DATE],121) >= (GETDATE()-31)
		and convert(varchar(10),D.[PERIOD_END_DATE],121) < GETDATE()-1
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[Performing Rank]
	,[SCT]
	,[EDT]
	,[FLV]	
	,[HIGHEST_ACHIEVED_RANK]
	,[Comm Period]
	,[Phone]
	,[Email]

-----------------------------------------Pulling FIBC and Leaders------------------------------------------------

IF object_id('Tempdb..#Leaders') Is Not Null DROP TABLE #Leaders	
Select 
	 [HC ID #]
	,[CoachName]
	,[Customer_ID]
	,[BUSINESSCENTER_ID]
	,[Comm Period]
	,[Phone]
	,[Email]
	,[HIGHEST_ACHIEVED_RANK]
	,[Performing Rank]
	,[SCT]
	,[EDT]
	,[FLV]
	,[GV]
  Into #Leaders
	From #all
		Where [GV] > = '15000'
		 and [FLV] >= '6000'
		 and [SCT] >= '5'
		 Or [Performing Rank] in ('Presidential_Director','Global_Director')

-----------------------------------------Pulling Prior Year information------------------------------

IF object_id('Tempdb..#priorFigs') Is Not Null DROP TABLE #priorFigs	 
Select
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[HIGHEST_ACHIEVED_RANK]
	--,CONVERT(varchar(10),[PERIOD_END_DATE],121) as [Comm Period]
	,[SCT]
	,[EDT]
	,sum([VOLUME_VALUE]) as [Prior GV YTD]
into #priorfigs
From #Leaders C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
  Where convert(varchar(10),D.[PERIOD_END_DATE],121) >= '01/01/' + Convert(varchar(4), DatePart(yyyy, DateAdd(yyyy,-2, GETDATE())))
		and convert(varchar(10),D.[PERIOD_END_DATE],121) < GETDATE()-365
		and D.[VOLUME_TYPE] ='GV'
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[SCT]
	,[EDT]
	,[HIGHEST_ACHIEVED_RANK] 
	

-----------------------------------------Pulling Current Year information------------------------------
	
IF object_id('Tempdb..#currentfigs') Is Not Null DROP TABLE #currentfigs 
Select
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[HIGHEST_ACHIEVED_RANK]
	--,CONVERT(varchar(10),[PERIOD_END_DATE],121) as [Comm Period]
	,[SCT]
	,[EDT]
	,sum([VOLUME_VALUE]) as [Current GV YTD]
into #currentfigs
From #Leaders C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
  Where convert(varchar(10),D.[PERIOD_END_DATE],121) >= '01/01/' + Convert(varchar(4), DatePart(yyyy, DateAdd(yyyy,-1, GETDATE())))
		and convert(varchar(10),D.[PERIOD_END_DATE],121) < GETDATE()-1
		and D.[VOLUME_TYPE] ='GV'
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[SCT]
	,[EDT]
	,[HIGHEST_ACHIEVED_RANK] 	

-----------------------------------------Pulling Prior Month information------------------------------

IF object_id('Tempdb..#priormonth') Is Not Null DROP TABLE #priormonth 
Select
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[HIGHEST_ACHIEVED_RANK]
	--,CONVERT(varchar(10),[PERIOD_END_DATE],121) as [Comm Period]
	,[SCT]
	,[EDT]
	,sum([VOLUME_VALUE]) as [Prior Month GV]
into #priormonth
From #Leaders C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
  Where convert(varchar(10),D.[PERIOD_END_DATE],121) >= GETDATE() - 60 
		and convert(varchar(10),D.[PERIOD_END_DATE],121) < GETDATE()-31
		and D.[VOLUME_TYPE] ='GV'
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[SCT]
	,[EDT]
	,[HIGHEST_ACHIEVED_RANK] 

-----------------------------------------Pulling Prior Year (monthly) information------------------------------
	
IF object_id('Tempdb..#prioryear') Is Not Null DROP TABLE #prioryear
Select
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[HIGHEST_ACHIEVED_RANK]
	--,CONVERT(varchar(10),[PERIOD_END_DATE],121) as [Comm Period]
	,[SCT]
	,[EDT]
	,sum([VOLUME_VALUE]) as [Prior Year Monthly GV]
into #prioryear
From #Leaders C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
  Where convert(varchar(10),D.[PERIOD_END_DATE],121) >= GETDATE() - 395
		and convert(varchar(10),D.[PERIOD_END_DATE],121) < GETDATE()- 365
		and D.[VOLUME_TYPE] ='GV'
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[SCT]
	,[EDT]
	,[HIGHEST_ACHIEVED_RANK] 

-----------------------------------------Pulling Current Month information------------------------------
	
IF object_id('Tempdb..#currentmonth') Is Not Null DROP TABLE #currentmonth
Select
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[HIGHEST_ACHIEVED_RANK]
	--,CONVERT(varchar(10),[PERIOD_END_DATE],121) as [Comm Period]
	,[SCT]
	,[EDT]
	,sum([VOLUME_VALUE]) as [CurrentMonth GV]
into #currentmonth
From #Leaders C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]D
		on C.[BUSINESSCENTER_ID] = D.[NODE_ID]
  Where convert(varchar(10),D.[PERIOD_END_DATE],121) >= GETDATE()-31 
		and convert(varchar(10),D.[PERIOD_END_DATE],121) < GETDATE()-1
		and D.[VOLUME_TYPE] ='GV'
group by 
	[HC ID #]
	,[CoachName]
	,C.[Customer_ID]
	,C.[BUSINESSCENTER_ID]
	,[SCT]
	,[EDT]
	,[HIGHEST_ACHIEVED_RANK] 

-----------------------------Pull into single Report-------------------------------------------------------	
Select
	([RANK_ID]+ 1) as [Performing Rank]
	,A.[HC ID #]
	,Convert(numeric(36,0),(A.[HC ID #])) as V_LookupHCID
	, A.[CoachName]
	,[Comm Period]
	, A.[HIGHEST_ACHIEVED_RANK]
	,isnull([Rank Up Month], 'Historic') as [Rank Up Month]
	,A.[Performing Rank] AS [Performing Rank Desc] -----This filed name is changed as SSRS does not allow duplicate field name
	,case when [FLV] >= '6000' and [GV] >='15000' and A.[SCT] >= '5' then 'FIBC' 
		else 'non-FIBC' end as [FIBC]
	, A.[EDT] as [ED Teams]
	,Datediff(MM,[Activation Date],[Today]) as [LOA - Months]
	,(([Current GV YTD]-[Prior GV YTD])/NullIF([Prior GV YTD],0)) as [% Change YoY YTD GV]
	,(([CurrentMonth GV]- [Prior Year Monthly GV])/NULLIF([Prior Year Monthly GV],0)) as [% Change YoY GV]
	,(([CurrentMonth GV]-[Prior Month GV])/NULLIF([Prior Month GV],0)) as [% Change MoM GV]
	,[Current GV YTD]
	,[CurrentMonth GV] as [Current Month GV]
	, A.[FLV] as [Current Month FLV]
	,[Prior Month GV]
	,[Prior GV YTD]
	,[CITY]
	,[STATE]
	,A.[Phone]
	,A.[Email]
	
From #Leaders A	
	left join #currentfigs B
		on A.[HC ID #] = B.[HC ID #]
	left join #currentmonth C
		on A.[HC ID #] = C.[HC ID #]
	left join #priorfigs D
		on A.[HC ID #] = D.[HC ID #]
	left join #priormonth E
		on A.[HC ID #] = E.[HC ID #]
	left join #prioryear F
		on A.[HC ID #] = F.[HC ID #]
	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] Z
		on A.[CUSTOMER_ID] = Z. [CUSTOMER_ID]
	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] Y
		on [Performing Rank] = [RANK_NAME]
	left join #rankups R
		on  A.[HC ID #] = R.[Coach ID]
	left join #a O
		on A.[HC ID #] = O.[HC ID #]
		and A.[Comm Period] = O.[CommPeriod]
Where [ADDRESS_TYPE] = 'Main'

