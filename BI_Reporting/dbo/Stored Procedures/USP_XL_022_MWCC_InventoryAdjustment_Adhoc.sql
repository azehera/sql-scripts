﻿CREATE PROCEDURE [dbo].[USP_XL_022_MWCC_InventoryAdjustment_Adhoc] As

SET NOCOUNT ON;

---temp table for convertion factor-----
IF object_id('Tempdb..#Convert') Is Not Null DROP TABLE #Convert -----Added Luc Emond 2013-10-16----
SELECT
 [Item No_]
,[Qty_ per Unit of Measure]
Into #Convert    
From NAV_ETL.[dbo].[Jason Pharm$Item Unit of Measure]
Where Code ='BX' AND [Qty_ per Unit of Measure] in('4', '7', '21')

----declare time frame for the report-----
Declare @LastMonthId INT, @Date2 Datetime,  @Date1 Datetime  

Set @Date1 = '2012-01-01'
Set @Date2 = '2013-12-31'

--SELECT @MonthId, @DATE1,  @LastMonthId, @DATE2

----select the data------
SELECT  
 Year(CreateDate) as CalendarYear
,MONTH(CreateDate) as CalendarMonth
,Convert(varchar(10),[createdate],121) as CreateDate
,region_name as Region
,location_code as LocationCode
,location_name as LocationName
,a.[adjustment_num] as AdjustmentNumber
,first_name+' '+last_name as [Created By]
,sku as Sku
,product_name as [Description] 
,Reason_Type
,Case when adjustment_type_cd='I' Then adjustment_qty Else adjustment_qty *-1 end as [Quantity Adjustment]
,IsNull([Standard Cost],0) as [Standard Cost]
,(Case when adjustment_type_cd='I' Then adjustment_qty Else adjustment_qty *-1 end * 
 IsNull([Standard Cost],0)) * IsNull([Qty_ per Unit of Measure],1) as [Amount Adjustment]-----Updated Luc Emond 2013-10-16----
,Case when adjustment_type_cd='I' then 'IN' else 'OUT' end as [Type]
,IsNull(adjustment_notes,' ') as Comments
,IsNull([Qty_ per Unit of Measure],1) as Uom
FROM       [Book4Time_ETL].[dbo].[B4T_inventory_adjustment] a
      join [Book4Time_ETL].[dbo].[B4T_inventory_adjustment_details] b
           on a.adjustment_num=b.[adjustment_num]
      join [Book4Time_ETL].[dbo].[B4T_product_master] c
           on b.product_id=c.product_id
      join [Book4Time_ETL].[dbo].[B4T_region_locations] d
           on a.location_id=d.location_id
      join [Book4Time_ETL].[dbo].[B4T_region] e
           on d.region_id=e.region_id
      join [Book4Time_ETL].dbo.B4T_location f
           on a.location_id=f.location_id
      join [Book4Time_ETL].[dbo].[B4T_Person] g
           on a.createby=g.person_id
      join [Book4Time_ETL].[dbo].[inventory_adjustment_reasons] h
           on b.reason_id = h.reason_id
 Left join NAV_ETL.[dbo].[Jason Pharm$Item] J 
           on Sku COLLATE Latin1_General_CI_AS = J.[No_]  COLLATE Latin1_General_CI_AS  
 left join #Convert k-----Added Luc Emond 2013-10-16----
           on Sku = k.[Item No_]COLLATE Latin1_General_CI_AS          
 Where Convert(varchar(10),[createdate],121) Between @Date1 and @Date2
 Order By Convert(varchar(10),[createdate],121)


