﻿








-- ========================================================================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>

--=====================Modification History ================================================
-- Modified: Per CAB #91 - 11/05/2010 (D. Smith)
-- Modified: Per CAB #274 05/23/2011 (D. Smith) - Increased length of @Dept from 150 to 300
-- ========================================================================================
CREATE PROCEDURE [dbo].[usp_ExcelReportDump] 
	-- Add the parameters for the stored procedure here
    @DistCenter NVARCHAR(30) ,
    @Dept NVARCHAR(300) ,
    @StartDate DATETIME ,
    @EndDate DATETIME
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;

    -- Insert statements for procedure here
        DECLARE @StartValue VARCHAR(10)
        DECLARE @EndValue VARCHAR(10)
	
        SET @StartValue = CAST(YEAR(@StartDate) AS VARCHAR(4)) + '-'
            + RIGHT('0' + CAST(MONTH(@StartDate) AS VARCHAR(2)), 2) + '-'
            + RIGHT('0' + CAST(DAY(@StartDate) AS VARCHAR(2)), 2)
        SET @EndValue = CAST(YEAR(@EndDate) AS VARCHAR(4)) + '-' + RIGHT('0'
                                                              + CAST(MONTH(@EndDate) AS VARCHAR(2)),
                                                              2) + '-'
            + RIGHT('0' + CAST(DAY(@EndDate) AS VARCHAR(2)), 2)


         SELECT  CASE sh.keydata1
                  WHEN 1 THEN 'MDCWMS'
                  WHEN 2 THEN 'TDCWMS'
                END AS dc ,
                reference3 AS [Cost Center] ,
                reference5 AS [Sales Source] ,
                reference1 AS [Sales Order Number] ,
                sh.sid AS [Shipping ID] ,
                p.pid AS [Package ID] ,
                sh.ship_date AS [Ship Date] ,
                sh.carrier ,
                sh.service
                ,
                dbo.udf_getShipCostByShipment(sh.[sid]) AS [Total Order Shipping Charge] ,
                sh.ship_chg AS [Order Shipping Charge] ,
                sh.fuel_chg AS [Order Fuel Surcharge]
                ,
                CASE sh.carrier
                  WHEN 'FDX' THEN sh.total_chg / ( SELECT   COUNT(pid)
                                                   FROM    FLAGSHIP_WMS_ETL.dbo.packages p
                                                   WHERE    p.[sid] = sh.[sid]
                                                 )
                  ELSE ( SELECT cost
                         FROM   dbo.udf_getShipCostByPackage(sh.[sid], p.pid)
                       )--fn.cost
                END AS [Package Total Charge] ,
                p.wgt AS [Package Weight] ,
                sh.ship_to_mailzone AS [Zone Code] ,
                a.company_name AS [Customer Name] ,
                a.address1 ,
                a.address2 ,
                a.city ,
                a.state ,
                a.postCode ,
                a.Countrycode
        FROM    FLAGSHIP_WMS_ETL.dbo.shipment_header sh
                INNER JOIN FLAGSHIP_WMS_ETL.dbo.packages p ON sh.[sid] = p.sid
                INNER JOIN FLAGSHIP_WMS_ETL.dbo.addresses a ON sh.[sid] = a.[sid]
					--cross apply dbo.udf_getShipCostByPackage(sh.[sid], p.pid) fn
        WHERE   a.type = 'SHIPTO'
                and sh.status='UPLD'
                AND sh.ship_date >= @StartValue
                AND sh.ship_date <= @EndValue
        ORDER BY sh.dc 
          

    END









