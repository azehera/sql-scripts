﻿



/*
--===============================================================================
--Author:         Kalpesh Patel
--Create date: 01/20/2014

--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER_ADDRESS]		       Select   
--[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                     Select  
--[ODYSSEY_ETL]       [dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]       Select

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
2014-01-22           Luc Emond        Added a new column in Numeric for Coach Id for V_Lookup                 

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/


CREATE procedure [dbo].[USP_XL_053_Basic_Comp_Exam] as 
set nocount on ; 


Declare @StartDate Datetime
Declare @EndDate Datetime

Set @StartDate=(Select dateadd(wk, datediff(wk, 0, getdate()) - 1, 0))
Set @EndDate=(Select dateadd(wk, datediff(wk, 0, getdate()), -1))

---------------------------------Pull Certifications for Month-----------------------------
SELECT 
	  A.[CUSTOMER_NUMBER] as [Coach ID]
	  ,Convert(numeric(36,0),(A.[CUSTOMER_NUMBER])) as V_LookupCoachID
	 ,A.[FIRST_NAME]as [First Name]
	 ,A.[LAST_NAME]as [Last Name]
	 ,A.[RECOGNITION_NAME]
	 ,CA.[ADDRESS1]
	 ,CA.[ADDRESS2]
	 ,CA.[City]
     ,CA.[STATE]
     ,CA.[POSTAL_CODE]
     ,A.[EMAIL1_ADDRESS] as [Coach_Email]
     ,convert(varchar(10),CS.[BASIC_COMPENTENCY],110) as [Basic Comp. Date]
     ,[DayOfWeekName]
     
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_STATUS]CS
			on A.[CUSTOMER_ID] = CS.[CUSTOMER_ID]
	Inner join [BI_Reporting].[dbo].[calendar_TSFL] C 
		on convert(varchar (10),CS.[BASIC_COMPENTENCY],121)= C.[CalendarDate]
	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CA
			on A.[CUSTOMER_ID] = CA.[CUSTOMER_ID]
Where convert(varchar (10),CS.[BASIC_COMPENTENCY],121)between @StartDate and @EndDate
		and CA.[ADDRESS_TYPE] = 'Main'
		and CA.[LANGUAGE_CODE] = 'English'
Order by CS.[BASIC_COMPENTENCY] asc

