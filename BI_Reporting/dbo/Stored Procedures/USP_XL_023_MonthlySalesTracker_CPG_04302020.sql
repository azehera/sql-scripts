﻿








/*
=================================================================================================
Author:  Kalepsh Patel
Create date: 08/11/2013
-------------------------USP_XL_023_MonthlySalesTracker_CPG--------------------------------------
---------------------------------
=================================================================================================   
REFERENCES
Database              Table/View/UDF                                    Action            
-------------------------------------------------------------------------------------------------
[BI_SSAS_Cubes]        dbo.DimJason Pharm$Customer Posting Group        Select 
[BI_SSAS_Cubes]        dbo.BusinessCalendar_Five                             Select
[BI_SSAS_Cubes]        dbo.FactSales                                    Select
[BI_Reporting]        dbo.Budget                                       Select 
[BI_Reporting]         dbo.XL_023_MonthlySalesTracker_LY_PostingGroup   Insert
[BI_Reporting]         dbo.XL_023_MonthlySalesTracker_LY                Insert
[BI_Reporting]         dbo.XL_023_OpenOrder                             Insert
[BI_SSAS_Cubes]        dbo.BusinessCalendar_Five                       Select------Added by Daniel Dagnachew bc. data in [BI_SSAS_Cubes].dbo.BusinessCalendar_Five is expired.
                                                                        Query for the table definition is saved in BI repository.(BI Repository\Daniel\Business Calendar Query)           
=================================================================================================
REVISION LOG
Date            Name                          Change
-------------------------------------------------------------------------------------------------
2014-02-14     Luc Emond			Corrected Open order$ to be >0 and exclude Exchange Orders				
=================================================================================================
NOTES: changed as per thomas for Budget=CummulativeRevenue month total for 2013
-------------------------------------------------------------------------------------------------
=================================================================================================
*/
CREATE PROCEDURE [dbo].[USP_XL_023_MonthlySalesTracker_CPG_04302020]
 AS 
 
  Declare @LastYearCalendarMonth Int
	 Declare @ThisYearCalendarMonth Int
	 Declare @LastYear int
	 Declare @ThisYear int
	 Declare @Month int
             
             
     Set @LastYearCalendarMonth =(Select CalendarMonthID from BI_Reporting.dbo.calendar where CalendarDate=DateAdd(yy, -1, Convert (Varchar(10),GETDATE()-1,121))) 
     Set @ThisYearCalendarMonth = (Select CalendarMonthID from BI_Reporting.dbo.calendar where CalendarDate=Convert (Varchar(10),GETDATE()-1,121))
     Set @LastYear=(Select [CalendarYear] from BI_Reporting.dbo.calendar where CalendarDate=DateAdd(yy, -1, Convert (Varchar(10),GETDATE()-1,121))) 
     Set @ThisYear = (Select [CalendarYear] from BI_Reporting.dbo.calendar where CalendarDate=Convert (Varchar(10),GETDATE()-1,121))
     Set @Month=(Select [CalendarMonth] from BI_Reporting.dbo.calendar where CalendarDate=DateAdd(yy, -1, Convert (Varchar(10),GETDATE()-1,121))) 
             
    --Select @LastYearCalendarMonth, @ThisYearCalendarMonth,@LastYear, @ThisYear,@Month
    

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
IF object_id('Tempdb..#Z') Is Not Null DROP TABLE #Z
IF object_id('Tempdb..#CustPost') Is Not Null DROP TABLE #CustPost
	
Select [Customer Posting Group],[Posting Date],CalendarYearMonth
into #CustPost from  BI_SSAS_Cubes.dbo.[DimJason Pharm$Customer Posting Group] a
Cross join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] b
	where YEAR(b.[Posting Date]) in(@LastYear,@ThisYear)
						and Month(b.[Posting Date]) in(@Month)
	
	

	SELECT [CustomerPostingGroup]
			,Sum(Amount) as Amount
			,[Posting Date]
				into #Z FROM [BI_SSAS_Cubes].[dbo].[FactSales]
						where YEAR([Posting Date]) in(@LastYear,@ThisYear)
						and Month([Posting Date]) in(@Month)
				group by [Posting Date],[CustomerPostingGroup]
				Order by [Posting Date]
				
			
					
Select  [Customer Posting Group] as [CustomerPostingGroup]
			,isnull((Amount),0) as ActualRevenue
			,CalendarYearMonth
			,a.[Posting Date]
				 into #A FROM  #CustPost a
				left join #Z b
				on a.[Customer Posting Group]=b.[CustomerPostingGroup]
				and a.[Posting Date]=b.[Posting Date]
				where a.[Posting Date]<=DateAdd(dd, DateDiff(dd,0,GetDate()-1), 0)
				Order by a.[Posting Date]
  
IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
 
	SELECT [CalendarMonthID]
			,Channel as CustomerPostingGroup
			,Sum(MonthlyBudget) as MonthlyBudget
            into #B FROM [BI_Reporting].dbo.Budget_XL23   ----- changed as per thomas for Budget=CummulativeRevenue month total for 2013
            where [CalendarMonthID]>=@LastYearCalendarMonth
            Group by [CalendarMonthID],Channel 
            Order by CalendarMonthID
          
            --Select * from #B
                   
               
  

IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C
  
   SELECT convert (int,[BusinessDayOfFiscalYear]) as [BusinessDayOfFiscalYear]
          ,Convert(int,[BusinessDayOfFiscalPeriod]) as[BusinessDayOfFiscalPeriod]
          ,a.[Posting Date]
          ,ActualRevenue
          ,MonthlyBudget
          ,a.CustomerPostingGroup
          ,CalendarMonthID
          into #C from #A a
                  join #B b
						on a.CalendarYearMonth=b.CalendarMonthID
                  and a.CustomerPostingGroup COLLATE DATABASE_DEFAULT=b.CustomerPostingGroup
                  join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] c
						on a.[Posting Date]=c.[Posting Date]
						
						
						

  
   
 IF object_id('Tempdb..#D') Is Not Null DROP TABLE #D 
      
      Select [BusinessDayOfFiscalPeriod]
             ,[Posting Date]
             ,ActualRevenue
             ,MonthlyBudget
             ,CalendarMonthID 
             ,CustomerPostingGroup
             ,row_number() over (Partition by [BusinessDayOfFiscalPeriod] order by [Posting Date]  ) as No 
             into #D 
             from #C
             where CalendarMonthID=@LastYearCalendarMonth
           
             
           
--Select * from #D

  
IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E
   
   ;with CTE as (Select [BusinessDayOfFiscalPeriod],CustomerPostingGroup
                        ,case when no=11 OR No=12 OR No=13 or No=14 or No=15 or No=16 OR No=17 or No=18 or No=19 or No=20  then dateadd(day,-1,[Posting Date])
						      when (no=11 OR No=12 OR No=13 or No=14 or No=15 or No=16 OR No=17 or No=18 or No=19 or No=20) and ([BusinessDayOfFiscalPeriod] = 1)  then dateadd(day,-1,[Posting Date])
                              when no=21 OR No=22 OR No=23 or No=24 or No=25 or No=26 OR No=27 or No=28 or No=29 or No=30 then dateadd(day,-2,[Posting Date])
                              when no=31 OR No=32 OR No=33 or No=34 or No=35 or No=36 OR No=37 or No=38 or No=39 or No=40  then dateadd(day,-3,[Posting Date]) 
                              when no=41 OR No=42 OR No=43 or No=44 or No=45 or No=46 OR No=47 or No=48 or No=49 or No=50 then dateadd(day,-4,[Posting Date])
                              when no=51 OR No=52 OR No=53 or No=54 or No=55 or No=56 OR No=57 or No=58 or No=59 or No=60 then dateadd(day,-5,[Posting Date]) else [Posting Date] end as [Posting Date] 
                        ,ActualRevenue as ActualRevenue
                        ,MonthlyBudget
                        ,CalendarMonthID
                         from #D)
                Select [BusinessDayOfFiscalPeriod],CustomerPostingGroup
                       ,[Posting Date]
                       ,SUM(ActualRevenue) as ActualRevenue
                       ,MonthlyBudget,CalendarMonthID  
                       into #E 
					   from CTE 
                       group by [BusinessDayOfFiscalPeriod],[Posting Date],MonthlyBudget,CalendarMonthID,CustomerPostingGroup
                       order by [Posting Date]
      
IF object_id('Tempdb..#G') Is Not Null DROP TABLE #G
  
     Select 
           c.[BusinessDayOfFiscalPeriod],c.CustomerPostingGroup
           ,c.[Posting Date]
           ,Year(c.[Posting Date]) as Year
           ,c.ActualRevenue
           ,Sum(c1.ActualRevenue) as CummulativeRevenue
           ,isnull(c.MonthlyBudget,0) as MonthlyBudget
           ,c.CalendarMonthID
           into #G 
           from #E c
                   inner join #E c1 on c.[Posting Date]>=c1.[Posting Date]
                   and c.CustomerPostingGroup=c1.CustomerPostingGroup
                   group by c.[BusinessDayOfFiscalPeriod],c.[Posting Date],c.ActualRevenue,c.MonthlyBudget,c.CalendarMonthID,c.CustomerPostingGroup
                   order by c.[Posting Date]
      
--Select * from #G
IF object_id('Tempdb..#H') Is Not Null DROP TABLE #H  
      
      Select Convert(int,[BusinessDayOfFiscalPeriod]) as [BusinessDayOfFiscalPeriod],CustomerPostingGroup
             ,[Posting Date]
             ,ActualRevenue
             ,MonthlyBudget
             ,CalendarMonthID 
             ,row_number() over (Partition by convert (int,[BusinessDayOfFiscalPeriod]) order by [Posting Date]  ) as No 
             into #H 
             from #C
     where CalendarMonthID=@ThisYearCalendarMonth
     
--Select * from #H


IF object_id('Tempdb..#I') Is Not Null DROP TABLE #I
   
      ;with CTE as (Select [BusinessDayOfFiscalPeriod],CustomerPostingGroup
	                  ,case  when no=11 OR No=12 OR No=13 or No=14 or No=15 or No=16 OR No=17 or No=18 or No=19 or No=20  then dateadd(day,-1,[Posting Date])------- not working if the first day of the month falls on non-business days DDagnachew 05/03/2016
	        --           ,case  when (no=1 OR No=2 OR No=3 or No=4 or No=5 or No=6 OR No=7 or No=8 or No=9 or No=10) and (BusinessDayOfFiscalPeriod = 1) then dateadd(day,1,[Posting Date])
         --                     when (no=1 OR No=2 OR No=3 or No=4 or No=5 or No=6 OR No=7 or No=8 or No=9 or No=10) and BusinessDayOfFiscalPeriod = 2  then dateadd(day,0,[Posting Date])
							  --when (no=11 OR No=12 OR No=13 or No=14 or No=15 or No=16 OR No=17 or No=18 or No=19 or No=20) and (BusinessDayOfFiscalPeriod > 2 )  then dateadd(day,-1,[Posting Date])
                              when no=21 OR No=22 OR No=23 or No=24 or No=25 or No=26 OR No=27 or No=28 or No=29 or No=30 then dateadd(day,-2,[Posting Date])
                              when no=31 OR No=32 OR No=33 or No=34 or No=35 or No=36 OR No=37 or No=38 or No=39 or No=40  then dateadd(day,-3,[Posting Date]) 
                              when no=41 OR No=42 OR No=43 or No=44 or No=45 or No=46 OR No=47 or No=48 or No=49 or No=50 then dateadd(day,-4,[Posting Date])
                              when no=51 OR No=52 OR No=53 or No=54 or No=55 or No=56 OR No=57 or No=58 or No=59 or No=60 then dateadd(day,-5,[Posting Date]) else [Posting Date] end as [Posting Date] 
                    ,ActualRevenue as ActualRevenue
                    , MonthlyBudget
                    ,CalendarMonthID
                    from #H)
                    Select [BusinessDayOfFiscalPeriod],CustomerPostingGroup
                    ,[Posting Date]
                    ,SUM(ActualRevenue) as ActualRevenue
                    ,MonthlyBudget,CalendarMonthID  
                    into #I 
					from CTE 
                    group by [BusinessDayOfFiscalPeriod],[Posting Date],MonthlyBudget,CalendarMonthID,CustomerPostingGroup
                    order by [Posting Date]
                    
               
      
 
Insert into #G 
        Select 
             c.[BusinessDayOfFiscalPeriod],c.CustomerPostingGroup
             ,c.[Posting Date]
             ,Year(c.[Posting Date]) as Year
             ,c.ActualRevenue
             ,Sum(c1.ActualRevenue) as CummulativeRevenue
             ,isnull(c.MonthlyBudget,0) as MonthlyBudget
             ,c.CalendarMonthID
             from #I c
                  inner join #I c1 on c.[Posting Date]>=c1.[Posting Date]
                  and c.CustomerPostingGroup=c1.CustomerPostingGroup
                  group by  c.[BusinessDayOfFiscalPeriod],c.[Posting Date],c.ActualRevenue,c.MonthlyBudget,c.CalendarMonthID,c.CustomerPostingGroup
                  order by c.[Posting Date],c.CustomerPostingGroup
                  
              
      
IF object_id('Tempdb..#J') Is Not Null DROP TABLE #J

        SELECT 
             Count(distinct [BusinessDayOfFiscalPeriod]) as [CountBusinessDay] 
             ,CalendarYearMonth
             into #J FROM [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] a
                     where CalendarYearMonth in (@LastYearCalendarMonth,@ThisYearCalendarMonth)
                     Group by CalendarYearMonth
  
IF object_id('Tempdb..#K') Is Not Null DROP TABLE #K
  
  Select 
       distinct Convert(Numeric(15,0),[BusinessDayOfFiscalPeriod])as [BusinessDayOfFiscalPeriod]
       ,[CountBusinessDay]
       ,RealDate 
       into #K 
       FROM [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] a
           Join #J b
           on a.CalendarYearMonth=b.CalendarYearMonth
           where a.CalendarYearMonth in (@LastYearCalendarMonth,@ThisYearCalendarMonth)
  
IF object_id('Tempdb..#L') Is Not Null DROP TABLE #L
  
  Select ([CountBusinessDay]-[BusinessDayOfFiscalPeriod]) as RemainingDays
          ,RealDate
          ,[CountBusinessDay]
          into #L from #K
          
         
  

 --IF object_id('Tempdb..BI_Reporting.dbo.XL_023_MonthlySalesTracker_LY_PostingGroup') Is Not Null DROP TABLE BI_Reporting.dbo.XL_023_MonthlySalesTracker_LY_PostingGroup     
      
truncate  table BI_Reporting.dbo.XL_023_MonthlySalesTracker_LY_PostingGroup
Insert into BI_Reporting.dbo.XL_023_MonthlySalesTracker_LY_PostingGroup
      Select * 
          ,Month([Posting Date])as Month
          ,Case when RemainingDays=0 then (MonthlyBudget-CummulativeRevenue) else  (MonthlyBudget-CummulativeRevenue)/(RemainingDays) end  as AvgDayRemaining
          from #G g
              join #L l
                  on g.[Posting Date]=l.RealDate
                 where CalendarMonthID=@LastYearCalendarMonth
                 
                       
 --IF object_id('Tempdb.BI_Reporting.dbo.XL_023_MonthlySalesTracker_TY_PostingGroup') Is Not Null DROP TABLE BI_Reporting.dbo.XL_023_MonthlySalesTracker_TY_PostingGroup 
Truncate  table BI_Reporting.dbo.XL_023_MonthlySalesTracker_TY_PostingGroup
Insert into BI_Reporting.dbo.XL_023_MonthlySalesTracker_TY_PostingGroup
      Select * 
            ,Month([Posting Date])as Month
            ,Case when RemainingDays=0 then (MonthlyBudget-CummulativeRevenue) else  (MonthlyBudget-CummulativeRevenue)/(RemainingDays) end  as AvgDayRemaining
            ,0.0 as TotalOpen 
      from #G g
      join #L l
      on g.[Posting Date]=l.RealDate
      where CalendarMonthID=@ThisYearCalendarMonth
      
----------------------------------------------------dbo.XL_023_MonthlySalesTracker_LY ---------------------------- 
Truncate table   dbo.XL_023_MonthlySalesTracker_LY  
insert into dbo.XL_023_MonthlySalesTracker_LY
      
SELECT [BusinessDayOfFiscalPeriod]
      ,[Posting Date]
      ,[Year]
      ,Sum([ActualRevenue]) as [ActualRevenue]
      ,Sum([CummulativeRevenue]) as [CummulativeRevenue]
      ,Sum([MonthlyBudget]) as [MonthlyBudget]
      ,[CalendarMonthID]
      ,[RemainingDays]
      ,[RealDate]
      ,[CountBusinessDay]
      ,[Month]
      ,Sum([AvgDayRemaining]) as [AvgDayRemaining]
FROM [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_LY_PostingGroup]
Group by [BusinessDayOfFiscalPeriod]
       ,[Posting Date]
       ,[Year]
       ,[CalendarMonthID]
      ,[RemainingDays]
      ,[RealDate]
      ,[CountBusinessDay]
      ,[Month]
      
      
 ---------------------------------------------------dbo.XL_023_MonthlySalesTracker_TY-------------------------------
Truncate table   dbo.XL_023_MonthlySalesTracker_TY  
Insert into dbo.XL_023_MonthlySalesTracker_TY 

 SELECT [BusinessDayOfFiscalPeriod]
      ,[Posting Date]
      ,[Year]
      ,Sum([ActualRevenue]) as [ActualRevenue]
      ,Sum([CummulativeRevenue]) as [CummulativeRevenue]
      ,Sum([MonthlyBudget]) as [MonthlyBdget]
      ,[CalendarMonthID]
      ,[RemainingDays]
      ,[RealDate]
      ,[CountBusinessDay]
      ,[Month]
      ,Sum([AvgDayRemaining]) as [AvgDayRemaining]
      ,0.00 as Total$Open
FROM [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_TY_PostingGroup]
Group by [BusinessDayOfFiscalPeriod]
       ,[Posting Date]
       ,[Year]
       ,[CalendarMonthID]
      ,[RemainingDays]
      ,[RealDate]
      ,[CountBusinessDay]
      ,[Month]
  
----------------------Open Orders Total--------------------------------------------
DELETE DBO.XL_023_OpenOrder
WHERE Lastdate = CONVERT(Varchar(10),Getdate()-1,121)---IN CASE OF MULTIPLE RUN IN A DAY----

;with cte1 as (SELECT 
 CONVERT(Varchar(10),Getdate()-1,121) as LastDate--- MINUS 1 BECAUSE I NEED THIS DATE BE YESTERDAY.
--,SUM([Amount Authorized]) as Total$Open
,SUM(L.[Amount Including VAT]) as Total$Open  -- Menkir Haile switched from [Amount Authorized] to [Amount Including VAT] 12/22/2016
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Header] H
Join [NAV_ETL].[dbo].[Jason Pharm$Sales Line] L
ON L.[Document No_] = H.[No_]
WHERE H.[Document Type]=1
--and [Amount Authorized] >0---Luc Emond 2014-02-14--- Menkir Haile Commented out on 12/22/2016   
and  [Coupon_Promotion Code] <>'EXCHANGE'---Luc Emond 2014-02-14---
) 
INSERT INTO DBO.XL_023_OpenOrder---ADDED THE BusinessDayOfFiscalPeriod----
select LastDate, Total$Open, BusinessDayOfFiscalPeriod
   from cte1
    Join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] b
   on cte1.LastDate = b.[Posting Date]  

---------UPDATE FIELD Total$Open FROM OPEN ORDER TABLE---------------------  
---step 1---
IF object_id('Tempdb..#X1') Is Not Null DROP TABLE #X1  
 
 ;with cte as (select  [Posting Date] as [LastDate]
      ,isnull([Total$Open],0) as [Total$Open]
      ,b.[BusinessDayOfFiscalPeriod],ROW_NUMBER () over(partition by b.BusinessDayOfFiscalPeriod,Month([Posting Date]) order by [Posting Date]) as no  from DBO.XL_023_OpenOrder a
right join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] b
on a.LastDate=b.[Posting Date]
where [CalendarYearMonth]=@ThisYearCalendarMonth
and [Posting Date]<CONVERT(Varchar(10),GETDATE(),121) )
Select 
case when No=2 then LastDate-1 
when no=3 then LastDate-2 
when no=4 then LastDate-3 
when no=5 then LastDate-4 
when no=6 then LastDate-5 
when no=7 then LastDate-6 else LastDate end As LastDate,
Total$Open,
BusinessDayOfFiscalPeriod,
no
into #X1 from cte 
 order by LastDate

---step 2---- 
IF object_id('Tempdb..#X') Is Not Null DROP TABLE #X  
Select 
 LastDate
,Total$Open
,BusinessDayOfFiscalPeriod
,row_number() over (Partition by BusinessDayOfFiscalPeriod,Month(LastDate) order by No desc ) as No_1 
into #x
from #x1 
order by lastdate      
   ----------------------Open Orders by Channels--------------------------------------------
DELETE DBO.XL_023_OpenOrder_CPG
WHERE Lastdate = CONVERT(Varchar(10),Getdate()-1,121)---IN CASE OF MULTIPLE RUN IN A DAY----

;with cte1 as (SELECT [Customer Posting Group],
 CONVERT(Varchar(10),Getdate()-1,121) as LastDate--- MINUS 1 BECAUSE I NEED THIS DATE BE YESTERDAY.
,SUM([Amount Authorized]) as Total$Open
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Header]
WHERE [Document Type]=1
and [Amount Authorized] >0---Luc Emond 2014-02-14---
and  [Coupon_Promotion Code] <>'EXCHANGE'---Luc Emond 2014-02-14---
Group by [Customer Posting Group]
) 
INSERT INTO DBO.XL_023_OpenOrder_CPG---ADDED THE BusinessDayOfFiscalPeriod----
select [Customer Posting Group],LastDate, Total$Open, BusinessDayOfFiscalPeriod
   from cte1
    Join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] b
   on cte1.LastDate = b.[Posting Date]  



---------UPDATE FIELD Total$Open FROM OPEN ORDER TABLE---------------------  
---step 1---
IF object_id('Tempdb..#X1_CPG') Is Not Null DROP TABLE #X1_CPG  
 
 ;WITH cte AS (SELECT  cust.[Customer Posting Group],[Posting Date] AS [LastDate]
      ,ISNULL([Total$Open],0) AS [Total$Open]
      ,b.[BusinessDayOfFiscalPeriod],ROW_NUMBER () OVER(PARTITION BY b.BusinessDayOfFiscalPeriod,MONTH([Posting Date]) ORDER BY [Posting Date]) AS no  
      FROM BI_SSAS_Cubes.dbo.[DimJason Pharm$Customer Posting Group] cust
      LEFT JOIN DBO.XL_023_OpenOrder_CPG a
      ON cust.[Customer Posting Group]=a.[Customer Posting Group]
RIGHT JOIN [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] b
ON a.LastDate=b.[Posting Date]
WHERE [CalendarYearMonth]=@ThisYearCalendarMonth
AND [Posting Date]<CONVERT(VARCHAR(10),GETDATE(),121) )
SELECT [Customer Posting Group],
CASE WHEN no=11 OR No=12 OR No=13 OR No=14 OR No=15 OR No=16 OR No=17 OR No=18 OR No=19 OR No=20  THEN DATEADD(DAY,-1,[LastDate])
                              WHEN no=21 OR No=22 OR No=23 OR No=24 OR No=25 OR No=26 OR No=27 OR No=28 OR No=29 OR No=30 THEN DATEADD(DAY,-2,[LastDate])
                              WHEN no=31 OR No=32 OR No=33 OR No=34 OR No=35 OR No=36 OR No=37 OR No=38 OR No=39 OR No=40  THEN DATEADD(DAY,-3,[LastDate]) 
                              WHEN no=41 OR No=42 OR No=43 OR No=44 OR No=45 OR No=46 OR No=47 OR No=48 OR No=49 OR No=50 THEN DATEADD(DAY,-4,[LastDate])
                              WHEN no=51 OR No=52 OR No=53 OR No=54 OR No=55 OR No=56 OR No=57 OR No=58 OR No=59 OR No=60 THEN DATEADD(DAY,-5,[LastDate])  ELSE LastDate END AS LastDate,
Total$Open,
BusinessDayOfFiscalPeriod,
no
INTO #X1_CPG FROM cte 
 ORDER BY LastDate
 
---step 2---- 
IF OBJECT_ID('Tempdb..#X_CPG') IS NOT NULL DROP TABLE #X_CPG  
SELECT [Customer Posting Group]
 ,LastDate
,Total$Open
,BusinessDayOfFiscalPeriod
,ROW_NUMBER() OVER (PARTITION BY BusinessDayOfFiscalPeriod,MONTH(LastDate) ORDER BY No DESC ) AS No_1 
INTO #x_CPG
FROM #x1_CPG 
ORDER BY lastdate




----UPDATE OPEN 4---     
UPDATE dbo.XL_023_MonthlySalesTracker_TY 
SET  Total$Open = M.Total$Open
FROM #x M
 WHERE MONTH(dbo.XL_023_MonthlySalesTracker_TY.[Posting Date]) = MONTH(m.LastDate)
AND dbo.XL_023_MonthlySalesTracker_TY .BusinessDayOfFiscalPeriod = m.BusinessDayOfFiscalPeriod
AND m.No_1 =1

----UPDATE OPEN CPG---     
UPDATE  BI_Reporting.dbo.XL_023_MonthlySalesTracker_TY_PostingGroup  
SET  Total$Open = M.Total$Open
FROM #x_CPG M
 WHERE MONTH( BI_Reporting.dbo.XL_023_MonthlySalesTracker_TY_PostingGroup .[Posting Date]) = MONTH(m.LastDate)
AND  BI_Reporting.dbo.XL_023_MonthlySalesTracker_TY_PostingGroup .BusinessDayOfFiscalPeriod = m.BusinessDayOfFiscalPeriod
--and m.No_1 =1
AND m.[Customer Posting Group]=BI_Reporting.dbo.XL_023_MonthlySalesTracker_TY_PostingGroup.[CustomerPostingGroup]






