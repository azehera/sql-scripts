﻿




/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------
[EXIGO].[ExigoSyncSQL_Prd].[dbo].[Orders]                       Select 
[EXIGO].[ExigoSyncSQL_Prd].[dbo].[Customers]                    Select 
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[ RT - BI Orders Modified by Exigo SQL Job for Other16] 
AS 

DECLARE @begin date, @end date
SET @begin = CAST(DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0) AS DATE)
SET @end = CAST(EOMONTH (GETDATE()) AS DATE)

SELECT
	o.[OrderID]
	,o.[Other13]
	,o.[Other16]
	,c.[Field2]
	,o.[CommissionableVolumeTotal]
	,o.[CreatedDate]
	,o.[ModifiedDate]
	,o.[ModifiedBy]
FROM [EXIGO].[ExigoSyncSQL_Prd].[dbo].[Orders] o with(nolock)
JOIN [EXIGO].[ExigoSyncSQL_Prd].[dbo].[Customers] c with(nolock) ON o.CustomerID = c.CustomerID
WHERE o.CreatedDate BETWEEN @begin AND @end
AND LEFT(o.Other13,2) = 'OM'
AND o.[ModifiedBy] LIKE 'tk121496%'




