﻿





/*
===============================================================================
Author:  Kalepsh Patel
Create date:  03/31/2013
-------------------------[USP_XL_065_ Encore_CAB_Tracking_Report]-----------------------------
---------------------------------
==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]				              Select 
[ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]                      Select
[BI_Reporting].[dbo].[Calendar]                                   Select
[ODYSSEY_ETL].[dbo].[ODYSSEY_ORDERS]                              Select
[ODYSSEY_ETL].[dbo].[ODYSSEY_ORDER_PRODUCT]                       Select

===============================================================================
REVISION LOG
Date            Name                          Change
-------------------------------------------------------------------------------
2014-06-30    Luc Emond					Update > GetDate to be >= GetDate                                           
===============================================================================
NOTES:
-------------------------------------------------------------------------------
===============================================================================
*/
CREATE procedure [dbo].[USP_XL_065_Encore_CAB_Tracking_Report]
as 
----------------------Pull Coaches Activating in September------------------
Select
	 [Customer_Number]
	,B.[BUSINESSCENTER_ID]
	,[First_Name]
	,[Last_Name]
	,convert(date,[Effective_Date],121) as [Activation Date]
	,convert(date,([CalendarDate]+30),121) as [CAB deadline]
	,convert(date,([CalendarDate]+60),121) as [60 days out]
Into #Coaches
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] A
inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
	on A.[CUSTOMER_ID]= B.[CUSTOMER_ID]
Inner join [BI_Reporting].[dbo].[Calendar]C
	on convert(date,A.[Effective_Date],121) = C.[CalendarDate]
	Where convert(date,[Effective_Date],121) >= '2014-02-01'
		--and convert(date,[Effective_Date],121) < '2013-08-01'
		and convert(date,B.[COMMISSION_PERIOD],121) >= Convert(Varchar(10), GETDATE(),121)---Updated 06/30/2014 Luc Emond
		
		
------------------------Pull New Clients (within CAB Deadline)------------------------
Select 
     A.[CUSTOMER_NUMBER]
    ,A.[Customer_ID]
	,A.[FIRST_NAME]
	,A.[LAST_NAME]
    ,convert(date,[APPLICATION_DATE],121) as [Entry Date]
    ,C.CUSTOMER_NUMBER as [Sponsor ID]
    ,C.[Activation Date]
    ,[CAB deadline]
    ,[60 days out]
  Into #CABClients
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] A
inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
	on A.[CUSTOMER_ID]= B.[CUSTOMER_ID]
Inner join [BI_Reporting].[dbo].[Calendar] D
	on convert(date,A.[ENTRY_DATE],121) = D.[CalendarDate]
Inner join #Coaches C
	on B.[SPONSOR_ID] = C.[BUSINESSCENTER_ID]
Where A.[CUSTOMER_TYPE] in ('R','I','D','K','U')
	and convert(date,B.[COMMISSION_PERIOD],121) = [LastDateOfMonth]
	and convert(date, A.[APPLICATION_DATE],121) >= C.[Activation Date]
	and convert(date, A.[APPLICATION_DATE],121)<= C.[CAB deadline]
Group By 
	 B.[SPONSOR_ID]
	,A.[CUSTOMER_NUMBER]
	,A.[CUSTOMER_ID]
	,A.[APPLICATION_DATE]
	,A.[FIRST_NAME]
	,A.[LAST_NAME]
	,C.CUSTOMER_NUMBER
	,C.[Activation Date]
	,[CAB deadline]
    ,[60 days out]
Order by 
	[SPONSOR_ID]

-------------------Pull Ordering information for new Clients (CAB)----------------
Select 
	 [Sponsor ID]
	,count(distinct([Customer_Number])) as [New Clients]
	,[Activation Date]
	,[60 days out]
	,convert(date,[CAB deadline],121) as [CAB deadline]
	,convert(numeric(10,2),SUM(O.[Total_Volume])) as [Comm. Total]
Into #CABSales
From #CABClients C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_ORDERS] O
		on C.[CUSTOMER_ID] = O.[Customer_ID]
Where convert(date,O.[ENTRY_DATE],121) > = [Activation Date]
	and convert(date,O.[ENTRY_DATE],121) <= [CAB deadline]
	Group by 
	 [Sponsor ID]
	,[Activation Date]
	,[CAB deadline]
	,[60 days out]
--------------------------Pull CAB Achievers--------------------		
Select
	 [Sponsor ID]
	,[New Clients]
	,[Activation Date]
	,[CAB deadline]
	,[60 days out]
	,[Comm. Total]
Into #CABAcheivers
From #CABSales
	Where [New Clients] >= '5'
		and [Comm. Total] >='1000'
	
------------------------Pull New Clients (within 60 Days)------------------------
Select 
     A.[CUSTOMER_NUMBER]
    ,A.[CUSTOMER_ID]
	,A.[FIRST_NAME]
	,A.[LAST_NAME]
    ,convert(date,[APPLICATION_DATE],121) as [Entry Date]
    ,C.CUSTOMER_NUMBER as [Sponsor ID]
    ,C.[Activation Date]
    ,[CAB deadline]
    ,[60 days out]
  Into #60DayClients
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] A
inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
	on A.[CUSTOMER_ID]= B.[CUSTOMER_ID]
Inner join [BI_Reporting].[dbo].[Calendar] D
	on convert(date,A.[ENTRY_DATE],121) = D.[CalendarDate]
Inner join #Coaches C
	on B.[SPONSOR_ID] = C.[BUSINESSCENTER_ID]
Where A.[CUSTOMER_TYPE] in ('R','I','D','K','U')
	and convert(date,B.[COMMISSION_PERIOD],121) = [LastDateOfMonth]
	and convert(date, A.[APPLICATION_DATE],121) > = [Activation Date]
	and convert(date, A.[APPLICATION_DATE],121)<= [60 days out]
Group By 
	 B.[SPONSOR_ID]
	,A.[CUSTOMER_NUMBER]
	,A.[CUSTOMER_ID]
	,A.[APPLICATION_DATE]
	,A.[FIRST_NAME]
	,A.[LAST_NAME]
	,C.CUSTOMER_NUMBER
	,C.[Activation Date]
	,[CAB deadline]
    ,[60 days out]
	,A.[CUSTOMER_ID]
Order by 
	[SPONSOR_ID]

-------------------Pull Ordering information for new Clients (60 Days)----------------
Select 
	 [Sponsor ID]
	,count(distinct([Customer_Number])) as [New Clients]
	,[Activation Date]
	,convert(date,[60 days out],121) as [60 days out]
	,convert(numeric(10,2),SUM(O.[Total_Volume])) as [Comm. Total]
Into #60DaySales
From #60DayClients C
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_ORDERS] O
		on C.[CUSTOMER_ID] = O.[CUSTOMER_ID]	
Where convert(date, O.[ENTRY_DATE],121) > = [Activation Date]
	and convert(date, O.[ENTRY_DATE],121)<= [60 days out]
	Group by 
	 [Sponsor ID]
	,[Activation Date]
	,[60 days out]	
	
--------------------------Pull 60 Day Out Achievers--------------------		
Select
	 CAB.[Sponsor ID]
	,A.[New Clients]
	,A.[Activation Date]
	,A.[60 days out]
	,A.[Comm. Total]
Into #60DayAchievers 
From #CABAcheivers CAB
	Inner join #60DaySales A
		on CAB.[Sponsor ID] = A.[Sponsor ID]
		
-----------------------Pull all info together to one chart---------------------------
Select 
	 CAB.[Sponsor ID] as [Coach ID]
	,(D.[FIRST_NAME]+' '+ D.[LAST_NAME]) as [Coach Name]
	,isnull(isnull(D.EMAIL1_ADDRESS,D.EMAIL2_ADDRESS),' ') as Email
	,[Recognition_Name]
	,CAB.[Activation Date]
	,CAB.[CAB deadline]
	,CAB.[New Clients]
	,CAB.[Comm. Total]
	,convert(date,CAB.[60 days out],121) as [60 Days Out]
	,isnull(A.[New Clients], 0) as [New Clients 60]
	,isnull(A.[Comm. Total],0) as [Total Volume 60]
	--,[SPONSOR_ID] as [Sponsor ID]
From #CABAcheivers CAB
Left join #60DayAchievers A
	on CAB.[Sponsor ID] = A.[Sponsor ID]
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] D
		on CAB.[Sponsor ID]= D.[Customer_Number]
	Group by 
	 CAB.[Sponsor ID]
	,D.[FIRST_NAME]
	,D.[LAST_NAME]
	,[Recognition_Name]
	,CAB.[Activation Date]
	,CAB.[CAB deadline]
	,CAB.[New Clients]
	,CAB.[Comm. Total]
	,A.[New Clients]
	,A.[Comm. Total]
	,CAB.[60 days out]
	,isnull(isnull(D.EMAIL1_ADDRESS,D.EMAIL2_ADDRESS),' ')
		
-------------------------Clean up Temp Tables------------------------	
Drop table #Coaches
Drop table #CABClients
Drop table #CABAcheivers
Drop table #60DayAchievers
Drop table #60DayClients
Drop table #60DaySales
Drop table #CABSales




