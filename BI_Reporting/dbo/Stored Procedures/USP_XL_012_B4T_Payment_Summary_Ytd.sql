﻿


/*
--============================================================================================================
Author:         Luc Emond
Create date: 05/01/2012
----------------------[dbo].[USP_XL_012_B4T_Payment_Summary_Ytd]----------------------------------------------
Provide Daily Transaction for all MWCC Location
==============================================================================================================   
REFERENCES
Database              Table/View/UDF                       Action            
-------------------------------------------------------------------------------------------------------------
BI_REPORTING       Calendar 				              Select
Book4Time_ETL      B4T_TransactionSummaryList             Select
Book4Time_ETL      B4T_transaction_log_header             Select
Book4Time_ETL      B4T_Customer                           Select
Book4Time_ETL      B4T_transaction_log_detail             Select
Book4Time_ETL      B4T_Discount                           Select
Book4Time_ETL      B4T_Person                             Select
Book4Time_ETL      B4T_employee                           Select
Book4Time_ETL      B4T_ADDRESS                            Select                        
============================================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------------------------------
05/10/2012    Luc Emond           Added customer id and postal code
06/22/2012    Luc Emond           Added discount_desc
08/08/2012    Luc Emond           Issue found related to B4T table need Exclude Total where is Null  
09/10/2012    Luc Emond           Added custom_field1 as [ASF Contract #]
12/19/2012    Luc Emond           Re-Write Store Procedure for Annual Pull and to clean it up a bit
05/01/2014    Kalpesh Patel       l.disc<>0 changed to ld.discount_amt <> 0  as per Christos request 
============================================================================================================
NOTES:
------------------------------------------------------------------------------------------------------------
============================================================================================================
--*/  

CREATE Procedure [dbo].[USP_XL_012_B4T_Payment_Summary_Ytd] As 
Set Nocount on;

----Declare and Set Date for Annual Pull--------12/19/2012 Luc Emond
Declare @Date Datetime
Set @Date = (Select [FirstDateOfYear] from [dbo].[calendar] where CalendarDate = (select Convert (varchar(10), GetDate()-1,121)))

----BRING IN THE INFO FROM THE TABLE PRODUCED BY OUR IT DEPT.--------
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select 
 l.transaction_summary_id
,l.location_name
,l.location_code
,l.technician
,l.transaction_id
,l.lineitem
,l.transaction_type
,l.transaction_date
,l.customer
,l.UpdatedBy
,l.program
,l.food
,l.nonfood
,l.zadmin
,l.disc
,l.tax
,l.total
,l.CASH
,l.VISA
,l.DISCOVER
,l.AMEX
,l.PERSCHECK
,l.MASTERCARD
,l.MEDIFASTGC
,l.CORPCHECK
,l.ASFCHARGE
,l.ASFPAYMENTS
,l.HOUSEACCOUNT
,l.COUPON
,l.PACKAGE
,l.HOUSENSF
,l.EMPLOYEE_ID
,p.Customer_Id-----05/10/2012 Luc Emond
,c.Parent_id
Into #a 
From [Book4Time_ETL].[dbo].[B4T_TransactionSummaryList] l
Left Outer Join
     [Book4Time_ETL].[dbo].[B4T_transaction_log_header] p
     on l.transaction_id = p.transaction_id
Left Outer Join
     [Book4Time_ETL].[dbo].[B4T_Customer]C
     on p.customer_id = C.customer_id
Where l.transaction_date >=@Date     
  And l.TOTAL IS NOT NULL----08/22/2012 Luc Emond
-----------------Need to remove ASF and House----------------------Chistos 08/07/2014
IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C

Select a.Transaction_ID,Sum([sold_price]*[qty]) as [ASF/House] 
into #C from [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] a
join [Book4Time_ETL].dbo.B4T_transaction_log_header b
on a.transaction_id=b.transaction_ID
where item_code in ('ASF','HOUSE')
and CONVERT(varchar(10),transaction_date,121) >=@Date
Group by  a.Transaction_ID


----ALTER TABLE TO ADD ADDITIONAL INFO REQUESTED------
Alter Table #a
Add ParentOfParentId BigInt, discount_desc Varchar(50)

Update #a-----09/10/2012-----
Set ParentOfParentId = parent_id 
Where #a.customer_id = #a.parent_id

Update #a-----09/10/2012-----
Set ParentOfParentId = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
Where #a.parent_id = p.customer_id
  And ParentOfParentId is null

Update #a------06/22/2012 Luc Emond
Set discount_desc = Case when d1.discount_desc is null then ''  ELSE d1.discount_desc end
From #a l
Left Outer Join
     [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] ld
     On l.transaction_id = ld.transaction_id    
Left Outer Join
     [Book4Time_ETL].[dbo].[B4T_Discount] d1
     On ld.discount_id = d1.discount_id    
Where  l.transaction_id = ld.transaction_id 
AND ld.discount_amt <> 0                ----l.disc<>0 changed to ld.discount_amt <> 0  as per Christos request 05/01/2014 Kalpesh

------------------------------------------------------------------------------------------
Select l.transaction_summary_id
      ,l.[Location_Name]
      ,l.[Location_Code]
      ,l.[Transaction_id]
      ,l.[lineitem]
      ,l.[Transaction_Type]
      ,l.[Transaction_Date]
      ,DatePart(MM,l.transaction_date) As CalendarMonth
      ,DatePart(QQ,l.transaction_date) As CalendarQuarter
      ,DATEPART(YYYY,l.transaction_date) As CalendarYear  
      ,l.Employee_Id
      ,l.[Technician]
      ,e.employee_code As Technician_Code
      ,l.customer_id
      ,l.parent_id as parentid
      ,l.ParentofParentId
      ,l.[Customer]
      ,d.Postal_Code-----05/10/2012 Luc Emond
      ,cu.custom_field1 as [ASF Contract #]---09/10/2012 Luc Emond 
      ,l.[UpdatedBy]
      ,c.first_name+' '+c.last_name As UpdatedBy_Name
      ,l.[NonFood]
      ,l.[Program]
      ,l.[Food]
      ,l.[ZAdmin]
      ,l.[Disc]
      ,Case when l.discount_desc is null then ''  ELSE l.discount_desc end As discount_desc
      ,l.[tax]
      ,l.[Total]
      ,l.[CASH]
      ,l.[VISA]
      ,l.[DISCOVER]
      ,l.[AMEX]
      ,l.[PERSCHECK]
      ,l.[MASTERCARD]
      ,l.[MEDIFASTGC]
      ,l.[CORPCHECK]
      ,l.[ASFCHARGE]
      ,l.[ASFPAYMENTS]
      ,l.[HOUSEACCOUNT]
      ,l.[COUPON]
      ,l.[PACKAGE]
      ,l.[HOUSENSF]
      ,ISNULL([ASF/House],0) as [ASF]
      ,(l.[Food]+l.[NonFood])-l.[COUPON] As CashProduct
      ,l.[Program]+l.[ZAdmin]-ISNULL([ASF/House],0) As Program2 ----To match sales cube need to remove ASF,House and Coupon 
      ,l.[tax] As Tax2
      ,((l.[Food]+l.[NonFood])-l.[COUPON])+(l.[Program]+l.[ZAdmin])+l.[tax]-ISNULL([ASF/House],0) As TotalWithTax
      ,l.[Program]-ISNULL([ASF/House],0) As ActualProgram
      ,(l.[CASH]+l.[VISA]+l.[DISCOVER]+l.[AMEX]+l.[PERSCHECK]+l.[MASTERCARD]+l.[MEDIFASTGC]+l.[CORPCHECK]+[ASFCHARGE]+l.[ASFPAYMENTS]+l.[HOUSEACCOUNT]+l.[HOUSENSF])-l.[ASFCHARGE]-ISNULL([ASF/House],0) As CustomerPaidCashMinusCoupon
      ,l.[ASFCHARGE] As CustomerChargedToASF
From #a L
Left Outer Join
     [Book4Time_ETL].[dbo].[B4T_Person]c
     On l.UpdatedBy = c.person_id 
Left Outer Join
     [Book4Time_ETL].[dbo].[B4T_employee]e 
     On l.employee_id = e.employee_id  
Left Outer Join
     [Book4Time_ETL].[dbo].[B4T_ADDRESS]d
     On l.Customer_Id = d.Address_Id    
     and d.address_type =4
Left Outer Join
     [Book4Time_ETL].[dbo].[B4T_customer] cu
     On l.ParentofparentId = cu.customer_id   
Left join #C f
on L.transaction_id=f.Transaction_ID  
    
---CLEAN UP----- 
 Drop Table #a


