﻿
-- =============================================
-- Author:		Menkir H.
-- Create date: 10/19/17
-- Description:	return first upline FIBL

-- =============================================



CREATE procedure [dbo].[usp_Get_First_Upline_FIBL]
(   @BC_ID NUMERIC(9,0),
    @commission_period date )
 
 AS

DECLARE @BusinessCenter_ID NUMERIC;

IF OBJECT_ID('Tempdb..#Temp ') IS NOT NULL DROP TABLE #Temp ;
WITH q AS ( SELECT COMMISSION_PERIOD,
							BUSINESSCENTER_ID,
							1 AS RoWNumber,
							CUSTOMER_ID,
							[RANK],
							PARENT_BC
					FROM ODYSSEY_ETL.dbo.ODYSSEY_BUSINESSCENTER 
					WHERE CAST(COMMISSION_PERIOD AS DATE) = CAST(@commission_period AS DATE) AND CUSTOMER_ID = 9605545
               UNION ALL
					SELECT	BC.COMMISSION_PERIOD,
							BC.BUSINESSCENTER_ID,
							RoWNumber + 1 AS RoWNumber,
							BC.CUSTOMER_ID,
							BC.[RANK],
							BC.PARENT_BC
					FROM ODYSSEY_ETL.dbo.ODYSSEY_BUSINESSCENTER BC
                        JOIN q ON q.PARENT_BC = BC.BUSINESSCENTER_ID AND q.COMMISSION_PERIOD = BC.COMMISSION_PERIOD
               
             )
			 
			 SELECT * INTO #Temp  FROM  q
		
		SET @BusinessCenter_ID = 
			(SELECT TOP 1 BUSINESSCENTER_ID
			FROM  (
			 SELECT BUSINESSCENTER_ID,
					COMMISSION_PERIOD,
					C.CUSTOMER_ID,
					[RANK],
					ROW_NUMBER() OVER (ORDER BY T.RowNumber desc) AS RowNumber	
			 FROM #Temp T
			 JOIN ODYSSEY_ETL.dbo.ODYSSEY_CUSTOMER C
					ON T.CUSTOMER_ID = C.CUSTOMER_ID AND T.[RANK] IN ('FI_GLOBAL_DIRECTOR','FI_PRESIDENTIAL_DIRECTOR')
			)k
		)
Select @BusinessCenter_ID
