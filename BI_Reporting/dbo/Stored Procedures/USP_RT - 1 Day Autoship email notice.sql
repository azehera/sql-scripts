﻿





/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 10/11/2019
------------------------------------------------
Future Autoship orders
--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------
Hybris_ETL            mdf_prd_database.orders                Select
Hybris_ETL            mdf_prd_database.users                 Select  
Hybris_ETL            mdf_prd_database.cronjobs              Select
Exigo_ETL             dbo.Customers                          Select                     
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - 1 Day Autoship email notice]
AS

/****** 1 day autoship email notice for Optavia orders******/
/*https://medifast.atlassian.net/browse/ITSD-10465*/
SELECT u.p_uid AS 'user id',
       u.p_cartcustnumber AS 'customer number',
       CAST(o.p_nextautoshiporderdate AS DATE) AS 'next autoship date',
       o.p_code AS 'autoship id',
       CASE
           WHEN Cu.CustomerTypeID = 1 THEN
               'Coach'
           WHEN Cu.CustomerTypeID = 2 THEN
               'Client'
       END AS Customer_Type
FROM HYBRIS_ETL.mdf_prd_database.orders AS o
    JOIN HYBRIS_ETL.mdf_prd_database.users AS u
        ON o.p_user = u.PK
    JOIN HYBRIS_ETL.mdf_prd_database.cronjobs AS c
        ON c.p_ordertemplate = o.PK
    --LEFT JOIN Hybris_ETL.mdf_prd_database.enumerationvalues v ON o.p_site = v.PK
    LEFT JOIN EXIGO_ETL.dbo.Customers Cu
        ON Cu.Field1 = u.p_cartcustnumber
WHERE o.p_nextautoshiporderdate IS NOT NULL
      AND o.p_versionid IS NULL
      AND o.p_autoshipordernumber IS NULL
      AND CAST(o.p_nextautoshiporderdate AS DATE) = DATEADD(dd, 1, CAST(GETDATE() AS DATE))
      AND c.p_active = 1
      AND o.p_site IN ( 8796093088808, 8796125922344 );


