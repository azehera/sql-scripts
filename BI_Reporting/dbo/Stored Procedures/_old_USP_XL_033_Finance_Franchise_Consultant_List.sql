﻿


/*
===============================================================================
Author:  Luc Emond
Create date: 09/20/2013
----------------------[USP_XL_033_Finance_Franchise_Consultant_List]-----------------------------
==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
[BI_Reporting]     dbo.XL_033_Finance_Franchise_Consultant_List   Select
NAV_ETL          dbo.[Jason Pharm$Sales Invoice Header]         Select
NAV_ETL          dbo.[Jason Pharm$Sales Invoice Line]           Select
===============================================================================
REVISION LOG
Date            Name                          Change
---------------------------------------------------------------------------------
===============================================================================
NOTES:
---------------------------------------------------------------------------------
===============================================================================
*/

CREATE PROCEDURE [dbo].[_old_USP_XL_033_Finance_Franchise_Consultant_List] as

Set NoCount On;	

--EXECUTE AS LOGIN = 'ReportViewer'
-----CREATE TABLE-------
CREATE TABLE #TblWorkTable (
	[ConsultanZipList] [varchar](10)  Default '',
	[Sales] [decimal](18,2) Default 0)

----INSERT INTO TABLE-------		
INSERT INTO #TblWorkTable([ConsultanZipList], [Sales])	
select 
 ZipCode as ConsultanZipList
,0 as [Sales]
From dbo.XL_033_Finance_Franchise_Consultant_List
order by ZipCode

----TEMP TABLE FOR SALES-----
IF object_id('Tempdb..#AA') Is Not Null DROP TABLE #AA
Select 
 LEFT([Ship-to Post Code],5)as MedifastShipToZip5
,SUM(Amount) as Amount
into #AA
From NAV_ETL.dbo.[Jason Pharm$Sales Invoice Header] a
join NAV_ETL.dbo.[Jason Pharm$Sales Invoice Line] b
on a.[No_] = b.[Document No_]
Where [Customer Posting Group]='MEDIFAST'
And a.[Posting Date] >=(SELECT DATEADD(YY, DATEDIFF (YY,0,GETDATE()-1),0))---First Of Year----
And Type ='2'
And LEFT([Ship-to Post Code],5) COLLATE DATABASE_DEFAULT in (Select ZipCode From dbo.XL_033_Finance_Franchise_Consultant_List)
Group by LEFT([Ship-to Post Code],5)
Having SUM(Amount)>0

----UPDATE----
Update #TblWorkTable
set Sales = aa.Amount
from #AA aa
where #TblWorkTable.ConsultanZipList COLLATE DATABASE_DEFAULT = aa.MedifastShipToZip5

----SELECT FOR REPORT-----
SELECT 
 ConsultanZipList
,SUM(Sales) as Sales
,Case When SUM(Sales)>0 Then 'Yes' Else 'No' end [Sales Reported]
FROM #TblWorkTable
Group By
 ConsultanZipList
 order by Case When SUM(Sales)>0 Then 'Yes' Else 'No' end desc, ConsultanZipList





