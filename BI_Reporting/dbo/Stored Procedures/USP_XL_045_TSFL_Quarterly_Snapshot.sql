﻿--/*
--==================================================================================
--Author:         Emily Trainor
--Create date: 08/02/2012
------------------------------TSFL Snapshot View - Quarterly -----------------------
-----------Pull Quarterly TSFL Snapshot Figures for TSFL Executives-----------------
--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--etrainor				TSFL_monthly_snapshot				      Select
--etrainor				Active Earning Coaches Quarterly	      Select                           
--==================================================================================
--REVISION LOG
--Date           Name                          Change
------------------------------------------------------------------------------------

--==================================================================================
--NOTES:
------------------------------------------------------------------------------------
--==================================================================================
--*/

Create procedure [dbo].[USP_XL_045_TSFL_Quarterly_Snapshot] as 
set nocount on ;


Select 
	   B.[Last Day of Quarter]
      ,B.[CalendarYear]
      ,[CalendarQuarter]
      ,B.[Active Earners]
      ,AVG([% of Earning Coaches]) as [Average % of Earning Coaches]
      ,sum([Sponsoring]) as [Quarterly Sponsoring]
      ,sum([Attrition]) as [Quarterly Attrition]
      ,avg([Attrition %]) as [Average Attrition %]
      ,sum([New Clients]) as [Quarterly New Clients]
      ,avg([Orders per Coach]) as [Average Orders per Coach]
      ,[Average Revenue Per Coach]
    FROM [dbo].[TSFL_monthly_snapshot] A
		Inner join [dbo].[Active Earning Coaches Quarterly] B 
			on A.[LastDateOfQuarter] = B.[Last Day of Quarter]
		Group by 
			 B.[Last Day of Quarter]
			,B.[CalendarYear]
			,[CalendarQuarter]
			,B.[Active Earners]
			,[Average Revenue Per Coach]	