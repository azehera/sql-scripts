﻿






/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------
[HYBRIS_ETL]          [dbo].[vw_Template_header_prd]         Select 
[[HYBRIS_ETL]         [dbo].[vw_template_line]               Select
[Hybris_ETL]          [dbo].[users]                          Select 
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - BI SKU in Autoship (Hybris)_11182020]
AS

IF OBJECT_ID('Tempdb..#temp') IS NOT NULL DROP TABLE #temp;
SELECT DISTINCT
       U.p_uid AS [Email Address],
       U.p_cartcustnumber AS [Customer Number],
       U.p_name AS [Customer Name],
       TL.sku,
       TH.channel,
       CAST(TH.nextautoshiporderdate AS DATE) AS [Next Order Date],
	   C.Field2 --AS 'Coach Number'
INTO #temp
FROM [HYBRIS_ETL].[dbo].[vw_Template_header_prd] TH
    INNER JOIN [HYBRIS_ETL].[dbo].[vw_template_line] TL
        ON TH.ordernumber = TL.ordernumber
    INNER JOIN [Hybris_ETL].[mdf_prd_database].[users] U
        ON U.PK = TH.user_id
	LEFT JOIN EXIGO.ExigoSyncSQL_PRD.dbo.Customers C ON C.Field1 = u.p_cartcustnumber AND C.Field2 !=''
WHERE CAST(TH.nextautoshiporderdate AS DATE)
      BETWEEN CAST(GETDATE() AS DATE) AND CAST(GETDATE() + 45 AS DATE)
      AND TH.templatestatus = 1;




SELECT T.*, C.FirstName +' ' +C.LastName AS 'Coach Name', C.Email AS 'Coach Email'
FROM #temp T
LEFT JOIN EXIGO.ExigoSyncSQL_PRD.dbo.Customers C ON T.Field2 = C.Field1


--SELECT DISTINCT
--       U.p_uid AS [Email Address],
--       U.p_cartcustnumber AS [Customer Number],
--       U.p_name AS [Customer Name],
--       TL.sku,
--       TH.channel,
--       CAST(TH.nextautoshiporderdate AS DATE) AS [Next Order Date]
--FROM [HYBRIS_ETL].[dbo].[vw_Template_header_prd] TH
--    INNER JOIN [HYBRIS_ETL].[dbo].[vw_template_line] TL
--        ON TH.ordernumber = TL.ordernumber
--    INNER JOIN [Hybris_ETL].[mdf_prd_database].[users] U
--        ON U.PK = TH.user_id
--WHERE CAST(TH.nextautoshiporderdate AS DATE)
--      BETWEEN CAST(GETDATE() AS DATE) AND CAST(GETDATE() + 45 AS DATE)
--      AND TH.templatestatus = 1;



