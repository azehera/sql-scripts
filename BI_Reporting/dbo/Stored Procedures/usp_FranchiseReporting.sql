﻿/*
===============================================================================
Author:         Menkir Haile
Create date: 03/17/2015

Comments: 
==============================================================================    
REFERENCES
Database					Table/View/UDF                             Action            
------------------------------------------------------------------------------- 
[BookerPOS_ETL]			[Sales]										Select
[BookerPOS_ETL]			[Payment]									Select

===============================================================================
REVISION LOG
Date			         Name                          Change

-------------------------------- Modifications ------------------------------------

===============================================================================
NOTES:
-------------------------------------------------------------------------------
===============================================================================
*/
CREATE PROCEDURE [dbo].[usp_FranchiseReporting]
@StartDate DATE ,@EndDate DATE
AS

--Declare @StartDate date = '2015-04-13', @EndDate date = '2015-04-13'
----------------------------Order From Payment--------------------------------------
   IF OBJECT_ID('Tempdb..#Order') IS NOT NULL DROP TABLE #Order
SELECT 
     [LocationID]
	 ,Count(*) AS Orders
     ,Sum([Amount]) AS [Total Revenue]
INTO #Order
  FROM [BookerPOS_ETL].[dbo].[Payments] P
  Where  [date] between @StartDate and @EndDate
  Group By [LocationID]
  ----------------------------------Member Order----------------------------
  IF OBJECT_ID('Tempdb..#MembersOrder') IS NOT NULL DROP TABLE #MembersOrder
  SELECT 
     [Location_Name] AS LocationID
	 ,Count(distinct [Order_No]) AS Orders
     ,Sum([Original_Price]) AS [Gross Prog $]
	 ,Sum(case when Discount > 0 then Discount * -1.0 else Discount end) AS Discounts
	 ,SUM([Original_Price]) - SUM(abs(Discount)) AS [Net Prog $]
INTO #MembersOrder
  FROM [BookerPOS_ETL].[dbo].[Sales] P
  Where  [Closed_Order_Date] between @StartDate and @EndDate and [Item_Type] like 'M%'
  Group By [Location_Name]
----------------------------Treatment--------------------------------------
   IF OBJECT_ID('Tempdb..#Treatment') IS NOT NULL DROP TABLE #Treatment
  SELECT 
     [Location_Name] AS LocationID
	 ,Count(distinct [Order_No]) AS Visits

INTO #Treatment
  FROM [BookerPOS_ETL].[dbo].[Sales] P
  Where  [Closed_Order_Date] between @StartDate and @EndDate and [Item_Type] = 'Treatment'
  Group By [Location_Name]
----------------------------Performed--------------------------------------
   IF OBJECT_ID('Tempdb..#Performed') IS NOT NULL DROP TABLE #Performed
  SELECT 
     [Location_Name] AS LocationID
	 ,Count(distinct [Order_No]) AS Performed

INTO #Performed
  FROM [BookerPOS_ETL].[dbo].[Sales] P
  Where  [Closed_Order_Date] between @StartDate and @EndDate and [Item_Type] = 'Treatment' and 
		 [Item_Name] = 'Visit - Complimentary Weight Loss Profile' 

  Group By [Location_Name]
--------------------------------------------------------------------------------------
Select   L.LocationName
		,O.[Total Revenue]
		,O.Orders
		,O.LocationID AS [Row Label]
		,M.Orders AS Sold
		,M.[Gross Prog $]
		,M.Discounts
		--,[Net Prog $]
		,IsNULL(M.[Gross Prog $],0.00)  - ISNULL(abs(M.Discounts) ,0.00) AS [Net Prog $]
		,IsNULL(O.[Total Revenue],0.00)  - (IsNULL(M.[Gross Prog $],0.00))  - ISNULL(abs(M.Discounts),0.00) AS [Prod $]
		,(IsNULL(M.[Gross Prog $],0.00) - ISNULL(abs(M.Discounts),0.00) )/
		((IsNULL(M.[Gross Prog $],0.00)  - ISNULL(abs(M.Discounts) ,0.00) + (IsNULL(O.[Total Revenue],0.00)  - (IsNULL(M.[Gross Prog $],0.00))  - ISNULL(abs(M.Discounts),0.00)))) AS [Prog $ %]
		,(IsNULL(O.[Total Revenue],0.00)  - IsNULL(M.[Gross Prog $],0.00)  - ISNULL(abs(M.Discounts),0.00)) / 
		((IsNULL(M.[Gross Prog $],0.00)  - ISNULL(abs(M.Discounts) ,0.00) + (IsNULL(O.[Total Revenue],0.00)  - (IsNULL(M.[Gross Prog $],0.00))  - ISNULL(abs(M.Discounts),0.00)))) AS [Prod $ %] 
		,T.Visits
		,P.Performed
		,Case when O.Orders > T.Visits then (O.Orders - T.Visits) End AS [Walk In Transaction]
		,(IsNULL(O.[Total Revenue],0.00)  - (IsNULL(M.[Gross Prog $],0.00))  - ISNULL(abs(M.Discounts),0.00)) / O.Orders AS [$/Order]
From #Order O
Left Outer Join #MembersOrder M
		ON M.LocationID = O.LocationID
Left Outer Join #Treatment T
		ON T.LocationID = O.LocationID
Left Outer Join #Performed P
		ON P.LocationID = O.LocationID
Left Outer Join (SELECT Distinct 
		[LocationID]
		,L.[LocationName]
  FROM [BookerPOS_ETL].[dbo].[Locations] L ) L
  ON L.[LocationID] = O.LocationID
Drop table #Order 
Drop table #MembersOrder 
Drop table #Treatment 
Drop table #Performed 
			