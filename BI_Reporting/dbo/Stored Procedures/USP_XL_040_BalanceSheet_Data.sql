﻿
CREATE Procedure [dbo].[USP_XL_040_BalanceSheet_Data]
as 


/**************************************************************
04/07/2015 - D. Smith - Change references to NAVPROD to NAV_ETL

****************************************************************/
----------fixed the error for was missing july in the overall calculation----------LUC EMOND OCTOBER 07 2013-----


---------delete and re-insert 2 months---------remove because it doesn't work correctly---addded script for first of year instead----LUC EMOND OCTOBER 07 2013
----delete from BI_Reporting.dbo.XL_040_BalanceSheet
----where [CalendarMonth]>MONTH(Getdate())-2

--EXECUTE AS LOGIN = 'ReportViewer'

---------------------------------------2013 Jason Pharm-----------
Truncate table BI_Reporting.dbo.XL_040_BalanceSheet
Insert into BI_Reporting.dbo.XL_040_BalanceSheet
SELECT 
'Jason Pharm' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] a WITH (NOLOCK)
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e WITH (NOLOCK)
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_] 
   join BI_Reporting.dbo.calendar c
  on [Posting Date] =c.CalendarDate  
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
and a.[G_L Account No_] <>'34400'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,CalendarMonthName
,[CalendarMonth]

Insert into BI_Reporting.dbo.XL_040_BalanceSheet 
SELECT 
'Jason Pharm' as Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] a WITH (NOLOCK)
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate   
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '40000' AND '99999'
Group by
CalendarMonthName
,[CalendarMonth]
 

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet
SET [G_L Name] = D.[Name]
FROM [NAV_ETL].[dbo].[Jason Pharm$G_L Account] d WITH (NOLOCK)
WHERE XL_040_BalanceSheet.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.XL_040_BalanceSheet where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

----------------------------------------------7 Crondall-----------------------------------------------------------------
Insert into BI_Reporting.dbo.XL_040_BalanceSheet
SELECT 
'7 Crondall' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[7 Crondall$G_L Entry] a WITH (NOLOCK)
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e WITH (NOLOCK)
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_] 
   join BI_Reporting.dbo.calendar c
  on [Posting Date] =c.CalendarDate  
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
and a.[G_L Account No_] <>'34400'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,CalendarMonthName
,[CalendarMonth]

Insert into BI_Reporting.dbo.XL_040_BalanceSheet 
SELECT 
'7 Crondall' as Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[7 Crondall$G_L Entry] a WITH (NOLOCK)
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate   
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '40000' AND '99999'
Group by
CalendarMonthName
,[CalendarMonth]


UPDATE BI_Reporting.dbo.XL_040_BalanceSheet
SET [G_L Name] = D.[Name]
FROM [NAV_ETL].dbo.[7 Crondall$G_L Account] d WITH (NOLOCK)
WHERE XL_040_BalanceSheet.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.XL_040_BalanceSheet where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')


---------------------------------------------Jason Ent--------------------------------------------------------------------
Insert into BI_Reporting.dbo.XL_040_BalanceSheet
SELECT 
'Jason Ent' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Enterprises$G_L Entry] a WITH (NOLOCK)
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e WITH (NOLOCK)
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_] 
   join BI_Reporting.dbo.calendar c
  on [Posting Date] =c.CalendarDate  
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
and a.[G_L Account No_] <>'34400'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,CalendarMonthName
,[CalendarMonth]

Insert into BI_Reporting.dbo.XL_040_BalanceSheet 
SELECT 
'Jason Ent' as Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Enterprises$G_L Entry] a WITH (NOLOCK)
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate   
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '40000' AND '99999'
Group by
CalendarMonthName
,[CalendarMonth]
 

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet
SET [G_L Name] = D.[Name]
FROM [NAV_ETL].dbo.[Jason Enterprises$G_L Account] d WITH (NOLOCK)
WHERE XL_040_BalanceSheet.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.XL_040_BalanceSheet where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')


-----------------------------------------------Jason Prop----------------------------------------------------------------
Insert into BI_Reporting.dbo.XL_040_BalanceSheet
SELECT 
'Jason Prop' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Properties$G_L Entry] a WITH (NOLOCK)
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e WITH (NOLOCK)
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_] 
   join BI_Reporting.dbo.calendar c
  on [Posting Date] =c.CalendarDate  
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
and a.[G_L Account No_] <>'34400'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,CalendarMonthName
,[CalendarMonth]

Insert into BI_Reporting.dbo.XL_040_BalanceSheet 
SELECT 
'Jason Prop' as Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Properties$G_L Entry] a WITH (NOLOCK)
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate   
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '40000' AND '99999'
Group by
CalendarMonthName
,[CalendarMonth]
 

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet
SET [G_L Name] = D.[Name]
FROM [NAV_ETL].dbo.[Jason Properties$G_L Account] d WITH (NOLOCK)
WHERE XL_040_BalanceSheet.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.XL_040_BalanceSheet where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')


------------------------------------------------------------Medifast----------------------------------------------------------
Insert into BI_Reporting.dbo.XL_040_BalanceSheet
SELECT 
'Medifast' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Medifast Inc$G_L Entry] a WITH (NOLOCK)
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e WITH (NOLOCK)
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_] 
   join BI_Reporting.dbo.calendar c
  on [Posting Date] =c.CalendarDate  
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
and a.[G_L Account No_] <>'34400'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,CalendarMonthName
,[CalendarMonth]

Insert into BI_Reporting.dbo.XL_040_BalanceSheet 
SELECT 
'Medifast' as Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Medifast Inc$G_L Entry] a WITH (NOLOCK)
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate   
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '40000' AND '99999'
Group by
CalendarMonthName
,[CalendarMonth]

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet
SET [G_L Name] = D.[Name]
FROM [NAV_ETL].dbo.[Medifast Inc$G_L Account] d WITH (NOLOCK)
WHERE XL_040_BalanceSheet.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.XL_040_BalanceSheet where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

-------------------------------------------TSFL--------------------------------------------------------------------
Insert into BI_Reporting.dbo.XL_040_BalanceSheet
SELECT 
'TSFL' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Take Shape For Life$G_L Entry] a WITH (NOLOCK)
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e WITH (NOLOCK)
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_] 
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate  
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
and a.[G_L Account No_] <>'34400'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,CalendarMonthName
,[CalendarMonth]

Insert into BI_Reporting.dbo.XL_040_BalanceSheet 
SELECT 
'TSFL' as Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Take Shape For Life$G_L Entry] a WITH (NOLOCK)
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate   
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '40000' AND '99999'
Group by
CalendarMonthName
,[CalendarMonth]
 

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet
SET [G_L Name] = D.[Name]
FROM [NAV_ETL].dbo.[Take Shape For Life$G_L Account] d WITH (NOLOCK)
WHERE XL_040_BalanceSheet.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.XL_040_BalanceSheet where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

-------------------------------------------Medifast Nutrition--------------------------------------------------------------------

Insert into BI_Reporting.dbo.XL_040_BalanceSheet
SELECT 
'Medifast Nutrition' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Medifast Nutrition$G_L Entry] a WITH (NOLOCK)
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e WITH (NOLOCK)
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_] 
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate  
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
and a.[G_L Account No_] <>'34400'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,CalendarMonthName
,[CalendarMonth]

Insert into BI_Reporting.dbo.XL_040_BalanceSheet 
SELECT 
'Medifast Nutrition' as Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[CalendarMonth]
,CalendarMonthName
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Medifast Nutrition$G_L Entry] a WITH (NOLOCK)
   join BI_Reporting.dbo.calendar c WITH (NOLOCK)
  on [Posting Date] =c.CalendarDate   
Where [Posting Date]>= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-1), 0))
and a.[G_L Account No_] BETWEEN '40000' AND '99999'
Group by
CalendarMonthName
,[CalendarMonth]
 

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet
SET [G_L Name] = D.[Name]
FROM [NAV_ETL].[dbo].[Medifast Nutrition$G_L Account] d WITH (NOLOCK)
WHERE XL_040_BalanceSheet.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.XL_040_BalanceSheet where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.XL_040_BalanceSheet SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')
----------------------------------------FINAL----------------------------------------------------------------

Truncate table BI_Reporting.dbo.XL_040_BalanceSheet_Data
insert  into BI_Reporting.dbo.XL_040_BalanceSheet_Data
SELECT Entity
,[G_L Account No_]
,[G_L Name]
,[RollUpName]
,January         
,February       
,March          
,April          
,May            
,June           
,July           
,August         
,September      
,October        
,November       
,December
  from (Select Entity      
,[G_L Account No_]
,[G_L Name]
,[RollUpName]
,Amount 
,CalendarMonthName
FROM BI_Reporting.dbo.XL_040_BalanceSheet
)p
pivot (Sum(Amount)
for CalendarMonthName in (
[January]         
,[February]       
,[March]          
,[April]          
,[May]            
,[June]           
,[July]           
,[August]         
,[September]      
,[October]        
,[November]       
,[December]) )as pvt
Order by [G_L Account No_];

insert into  BI_Reporting.dbo.XL_040_LY_Q4
Select distinct a.[G_L Account No_],a.[G_L Name],a.RollUpName,0.00 as Amount,a.Entity 
from BI_Reporting.dbo.XL_040_BalanceSheet_Data a WITH (NOLOCK)
left join BI_Reporting.dbo.XL_040_LY_Q4 b WITH (NOLOCK)
on a.[G_L Account No_]=b.[G_L Account No_]
and a.Entity=b.Entity
where b.[G_L Account No_] is null
----where ([G_L Account No_] not in (Select distinct [G_L Account No_] from BI_Reporting.dbo.XL_040_LY_Q4)
----and Entity not in (Select distinct Entity from BI_Reporting.dbo.XL_040_LY_Q4))


------MASTER TABLE---------

TRUNCATE TABLE BI_Reporting.dbo.XL_040_BalanceSheet_Report
INSERT INTO BI_Reporting.dbo.XL_040_BalanceSheet_Report
Select a.Entity,a.[G_L Account No_],a.[G_L Name], b.RollUpName
,case when 1 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0) end as January        
,case when 2 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0)end as  February      
,case when 3 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)end as  March         
,case when 4 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0) end as April          
,case when 5 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0)end as  May          
,case when 6 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)end as   June         
,case when 7 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0) end as July           
,case when 8 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0) end as August         
,case when 9 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else  isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0)+Isnull(September,0) end as September      
,case when 10 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0)+Isnull(September,0)+Isnull(October,0)end as  October       
,case when 11 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0)+Isnull(September,0)+Isnull(October,0)+Isnull(November,0) end as  November      
,case when 12 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0)+Isnull(September,0)+Isnull(October,0)+Isnull(November,0)+Isnull(December,0) end as December
,Amount as [2012Q4]
from BI_Reporting.dbo.XL_040_LY_Q4 a WITH (NOLOCK)
Left join BI_Reporting.dbo.XL_040_BalanceSheet_Data b WITH (NOLOCK)
on a.[G_L Account No_]COLLATE DATABASE_DEFAULT=b.[G_L Account No_]
and a.Entity=b.Entity

--union all

--Select b.Entity,b.[G_L Account No_],b.[G_L Name], b.RollUpName
--,case when 1 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as January --isnull(Amount,0)+isnull(January,0)        
--,case when 2 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  February--isnull(Amount,0)+isnull(January,0)+isnull(February,0)      
--,case when 3 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  March  --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)end as  March         
--,case when 4 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  April--isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0) end as April          
--,case when 5 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  May --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0)end as  May          
--,case when 6 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  June --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)end as   June         
--,case when 7 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  July --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0) end as July           
--,case when 8 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  August --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0) end as August         
--,case when 9 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  September --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0)+Isnull(September,0) end as September      
--,case when 10 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  October --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0)+Isnull(September,0)+Isnull(October,0)end as  October       
--,case when 11 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  November --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0)+Isnull(September,0)+Isnull(October,0)+Isnull(November,0) end as  November      
--,case when 12 >(Select distinct CalendarMonth from BI_Reporting.dbo.calendar where CalendarMonth=MONTH(Getdate())) then 0 else 0 end as  December --isnull(Amount,0)+isnull(January,0)+isnull(February,0) +isnull(March,0)+Isnull(April,0)+Isnull(May,0) +Isnull(June,0)+Isnull(July,0)+Isnull(August,0)+Isnull(September,0)+Isnull(October,0)+Isnull(November,0)+Isnull(December,0) end as December
--,Amount as [2012Q4]
--from BI_Reporting.dbo.XL_040_LY_Q4 a
--right join BI_Reporting.dbo.XL_040_BalanceSheet_Data b
--on a.[G_L Account No_]COLLATE DATABASE_DEFAULT=b.[G_L Account No_]
--and a.Entity=b.Entity
--and a.[G_L Account No_] is null

--Select * from BI_Reporting.dbo.XL_040_BalanceSheet_Report
--where [G_L Account No_]='20200'
-----------------------------------Update Old RollUpName -----------------------------------------------------------------------------------

update  BI_Reporting.dbo.XL_040_BalanceSheet_Report
 set  RollUpName =Case When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '10000' and '11999' Then 'Cash and Cash Equivalents'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '12001' and '12383' Then 'Short Term Investments'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '12501' and '12999' Then 'Affiliate Receivables'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '13001' and '13499' Then 'Account Receivables'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '13501' and '13748' Then 'Notes Receivables'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '13750' and '13998' Then 'Non-Trade Accounts Receivables'
     
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '14000' and '14998' Then 'Inventory'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '15000' and '15997' Then 'Pre-Paid Expenses'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '16000' and '16998' Then 'Long-Term Investments'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17001' and '17007' Then 'Fixed Asset In Process'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17009' and '17098' Then 'Building and Land'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17100' and '17198' Then 'Computers'
     
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17200' and '17298' Then 'Design And Development'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17300' and '17498' Then 'Furniture and Fixtures'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17500' and '17598' Then 'Leasehold Improvments'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17600' and '17698' Then 'Machinery and Equipment'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17700' and '17798' Then 'Software and Website'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17800' and '17898' Then 'Vehicles'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '17900' and '17997' Then 'Video'
          
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '18000' and '18998' Then 'Intangile Assets'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '19000' and '19997' Then 'Other Assets'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '20003' and '20998' Then 'Accounts Payable'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '21000' and '21998' Then 'Payroll Payable'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '22000' and '22998' Then 'Current Portion of LT Debt'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '23000' and '24997' Then 'Affilate Payables'
          
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '25000' and '29997' Then 'Non-Current Liabilities'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '30001' and '32998' Then 'Capital Stock'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '33000' and '33998' Then 'Additional Paid-In-Capital'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '34000' and '35998' Then 'Retained Earnings'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '36000' and '39994' Then 'Accum Other Comprehensive Income'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '39996' and '39997' Then 'Treasury Stock'

      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '40001' and '49998' Then 'Total Income'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT  Between '50001' and '59987' Then 'Total Cost of Goods Sold'
      When [G_L Account No_]  COLLATE DATABASE_DEFAULT Between '60001' and '64998' Then 'Total Direct Costs'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT  Between '65001' and '69996' Then 'Total Operating Costs'----CHANGED 65996 TO 69996 2012-06-20 LUC EMOND---
      When [G_L Account No_] COLLATE DATABASE_DEFAULT  Between '70002' and '74998' Then 'Total Personnel Expenses'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT  Between '75001' and '75998' Then 'Total Sales & Marketing Exp'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT  Between '76501' and '79998' Then 'Total Communication'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '80001' and '82998' Then 'Total Vehicle Expenses'
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '83001' and '85998' Then 'Total Office Expenses' 
      When [G_L Account No_] COLLATE DATABASE_DEFAULT  Between '86001' and '89996' Then 'Total Other Expenses' 
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '91001' and '94996' Then 'Total Other Income' 
      When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '95001' and '99997' Then 'Total Other Expenses' 
       End 
      where RollUpName is null



-----FOR THE REPORT-----ADDED LUC EMOND OCTOBER 07 2013----
SELECT 
Case When [G_L Account No_] Between '10000' and '19999' Then 'Assets'
     When [G_L Account No_] Between '20000' and '29999' Then 'Liabilities'
     When [G_L Account No_] Between '30000' and '39999' Then 'Equity'
     Else 'Unknow' End As Category
,*
FROM BI_Reporting.dbo.XL_040_BalanceSheet_Report WITH (NOLOCK)
--where [G_L Account No_]='20200'









