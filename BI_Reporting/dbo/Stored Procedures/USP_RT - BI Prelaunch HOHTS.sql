﻿

/*
=====================================================================================================
Author:					Daniel Dagnachew
Create date:			07/29/2019

=====================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------

=====================================================================================================
NOTES:
-----------------------------------------------------------------------------------------------------
=====================================================================================================
*/

CREATE procedure [dbo].[USP_RT - BI Prelaunch HOHTS]   


as 
set nocount on ;

--IF OBJECT_ID('tempdb..#Bucket') IS NOT NULL
--    DROP TABLE #Bucket;
--SELECT oe.[createdTS],
--       u.p_cartcustnumber [Coach ID],
--       u.p_name,
--       oe.[p_quantity],
--       o.[p_totalprice],
--       p.[p_code] [Item Description],
--       p.[p_sku] SKU,
--       o.p_code,
--       CASE
--           WHEN Order_Category = 'AutoshipTemplate' THEN
--               'Auto'
--           WHEN Order_Category = 'ReturnOrder' THEN
--               'Return'
--           ELSE
--               'ondemand'
--       END AS OrderType,
--       (oe.p_quantity * p.p_baseunitconversion) totPq
--INTO #bucket
--FROM ecomm_etl_hybris.[dbo].[ETL_orderentries] oe
--    INNER JOIN [ecomm_etl_hybris].[dbo].[ETL_orders] o
--        ON o.PK = oe.p_order
--    INNER JOIN [ecomm_etl_hybris].[dbo].[ETL_users] u
--        ON o.p_user = u.PK
--    INNER JOIN ecomm_etl_hybris.dbo.ETL_ProductsUnits p
--        ON p.PK = oe.p_product
--           AND p.[p_sku] = '76997';

--IF OBJECT_ID('tempdb..#AA') IS NOT NULL
--    DROP TABLE #AA;
--SELECT SKU,
--       p_code,
--       [Item Description],
--       [createdTS],
--       [Coach ID],
--       p_name,
--       OrderType,
--       EO.CommissionableVolumeTotal,
--       [p_totalprice],
--       SUM(p_quantity) [Qty Purchased]
--INTO #AA
--FROM #bucket B
--    JOIN [Exigo_ETL].[dbo].[Orders] EO
--        ON EO.Other13 = B.p_code
--GROUP BY SKU,
--         p_code,
--         [Item Description],
--         [createdTS],
--         [Coach ID],
--         p_name,
--         OrderType,
--         EO.CommissionableVolumeTotal,
--         [p_totalprice];
----HAVING SUM(p_quantity) > 1;

--DECLARE @comper INT = (SELECT PeriodID FROM [Exigo_ETL].[dbo].[Periods] WHERE PeriodTypeID = 1 AND CAST(EndDate AS DATE)  = ( SELECT CONVERT(varchar,dateadd(d,-(day(getdate())),getdate()),106))); 

--SELECT DISTINCT
--       a.SKU,
--       a.[Item Description],
--       a.[Coach ID] [ID],
--       a.p_name [Name],
--       c.RankID,
--       R.RankDescription,
--       CASE
--           WHEN c.CustomerTypeID = 1 THEN
--               1
--           WHEN c.CustomerTypeID = 2 THEN
--               0
--       END AS Coach_Flag,
--       SUM(a.[Qty Purchased]) OVER (PARTITION BY a.[Coach ID], a.OrderType) [Qty Purchased],
--       a.OrderType,
--       a.p_code [Order Number],
--       CAST(a.[createdTS] AS DATE) [Order Date],
--       c.[Date1] [Entry Date],
--       c.[Date2] [Activation Date],
--       c.[Date3] [Reversion Date],
--       c.[Date4] [Reinstatement Date],
--       c.[Date5] [Renewal Date],
--       c3.[Field1] [Sponsor ID],
--       c3.[FirstName] + ' ' + c3.[LastName] [Sponsor Name], ----c3 last name
--       c4.Field1 [First upline Integrated Global ID],
--       c4.[FirstName] + ' ' + c4.[LastName] [First upline Integrated Global Name],
--       c5.Field1 [First upline Integrated Presidential ID],
--       c5.[FirstName] + ' ' + c5.[LastName] [First upline Integrated Presidential Name],
--       a.CommissionableVolumeTotal,
--       a.[p_totalprice] [purchase amount]

----INTO #Filter
--FROM #AA a
--    JOIN [Exigo_ETL].[dbo].[Customers] c
--        ON c.Field1 = a.[Coach ID]
--    JOIN [Exigo_ETL].[dbo].[PeriodVolumes] pv WITH (NOLOCK)
--        ON c.[CustomerID] = pv.[CustomerID]
--           AND pv.PeriodTypeID = 1
--           AND pv.PeriodID = @comper
--    LEFT JOIN [Exigo_ETL].[dbo].[Customers] c4 WITH (NOLOCK)
--        ON SUBSTRING(CAST(pv.Volume127 AS NVARCHAR(32)), 1, (LEN(pv.Volume127) - 3)) = c4.[Field1]
--    JOIN [Exigo_ETL].[dbo].[Customers] c5 WITH (NOLOCK)
--        ON SUBSTRING(CAST(pv.Volume129 AS NVARCHAR(32)), 1, (LEN(pv.Volume129) - 3)) = c5.[Field1]
--    JOIN [Exigo_ETL].[dbo].[Customers] c3 WITH (NOLOCK)
--        ON c.[Field2] = c3.[Field1]
--    LEFT JOIN [Exigo_ETL].[ODSY].[RanksMapping] R
--        ON c.RankID = R.ExigoRankID
--WHERE a.p_totalprice <> 0
----AND p_name LIKE '%Pam Gish%'
--ORDER BY a.[Coach ID];

IF OBJECT_ID('tempdb..#abucket') IS NOT NULL
    DROP TABLE #abucket;
SELECT DISTINCT
       SI.[Order Date],
       fm.ItemCode,                       --oe.[createdTS],
       CASE
           WHEN SI.[Sell-to Customer No_] LIKE '%tsfl%' THEN
               RIGHT(SI.[Sell-to Customer No_], LEN(SI.[Sell-to Customer No_]) - 4)
           ELSE
               SI.[Sell-to Customer No_]
       END AS [Coach ID],                 --u.p_cartcustnumber [Coach ID],
       SI.[Sell-to Customer Name],        --u.p_name,
       CASE
           WHEN rt.OrderType IN ( 'RETURN', 'EXCHANGE' ) THEN
               - (fm.Quantity)
           WHEN so.TotalOrderAmount = 0 THEN
               0
           ELSE
               fm.Quantity
       END AS Quantity,
       CASE
           WHEN rt.OrderType IN ( 'RETURN', 'EXCHANGE' ) THEN
               rt.ReturnOrderAmount
           ELSE
               so.TotalOrderAmount
       END AS TotalOrderAmount,           --o.[p_totalprice],
       CASE
           WHEN rt.OrderType IN ( 'RETURN', 'EXCHANGE' ) THEN
               rt.ReturnOrderAmount
           ELSE
               fm.[Amount Including VAT]
       END AS [Amount Including VAT],     --o.[p_totalprice],

                                          -- 'HabitsHealthSystemKit_Product' [Item Description],
                                          --'76997' AS SKU,                       --p.[p_sku] SKU,
       fm.ItemCodeSubcategory SKU,        -----Daniel D. added to capture 74881
       It.Description [Item Description], -----Daniel D. added to capture 74881
       SI.[External Document No_],
       CASE
           WHEN fm.[OrderType] IN ( 'VIP', 'BSL' )
                AND rt.OrderType IS NULL
                AND so.TotalOrderAmount <> 0 THEN
               'AUTOSHIP'
           WHEN fm.[OrderType] = 'ONDEMAND'
                AND rt.OrderType IS NULL
                AND so.TotalOrderAmount <> 0 THEN
               'ONDEMAND'
           WHEN so.TotalOrderAmount = 0
                AND rt.OrderType IS NULL THEN
               'REPLACEMENT'
           ELSE
               rt.OrderType
       END AS [AUTOSHIP],
       EO.CommissionableVolumeTotal
--rt.OrderType [Return Type]
INTO #abucket
FROM [BICUBES_MDM].[dbo].[FactSales_MDM] fm WITH (NOLOCK)
    LEFT JOIN NAV_ETL.dbo.[Jason Pharm$Sales Invoice Header] SI WITH (NOLOCK)
        ON SI.No_ = fm.DocumentNo COLLATE DATABASE_DEFAULT
    LEFT JOIN [BICUBES_MDM].[dbo].[DimSalesOrder_MDM] so WITH (NOLOCK)
        ON so.DocumentNo = fm.DocumentNo
    LEFT JOIN [BICUBES_MDM].[dbo].[ReturnExchangeOrder] rt WITH (NOLOCK)
        ON rt.[Document No for Original Order] = SI.No_ COLLATE DATABASE_DEFAULT
    JOIN [Exigo_ETL].[dbo].[Orders] EO WITH (NOLOCK)
        ON EO.Other13 COLLATE DATABASE_DEFAULT = SI.[External Document No_]
    JOIN [BICUBES_MDM].[dbo].[DimItemCode] It WITH (NOLOCK)
        ON fm.ItemCodeSubcategory = It.Itemcode
WHERE fm.ItemCodeSubcategory IN ( '74881', '76997' );
---SELECT * FROM #abucket WHERE [Coach ID] = '2591401'
DECLARE @comper INT =
        (
            SELECT PeriodID
            FROM [Exigo_ETL].[dbo].[Periods] WITH (NOLOCK)
            WHERE PeriodTypeID = 1
                  AND CAST(EndDate AS DATE) =
                  (
                      SELECT CONVERT(VARCHAR, DATEADD(d, - (DAY(GETDATE())), GETDATE()), 106)
                  )
        );

SELECT DISTINCT
       a.SKU,
       a.[Item Description],
       a.[Coach ID] [ID],
       a.[Sell-to Customer Name] [Name],
       c.Email,
       c.RankID,
       R.RankDescription,
       CASE
           WHEN c.CustomerTypeID = 1 THEN
               1
           WHEN c.CustomerTypeID = 2 THEN
               0
       END AS Coach_Flag,
       SUM(a.[Quantity]) OVER (PARTITION BY a.[Coach ID], a.[External Document No_]) [Quantity Order],
       SUM(a.[Quantity]) OVER (PARTITION BY a.[Coach ID]) [Quantity Customer],
       a.[AUTOSHIP] OrderType,
       a.[External Document No_] [Order Number],
       CAST(a.[Order Date] AS DATE) [Order Date],
       c.[Date1] [Entry Date],
       c.[Date2] [Activation Date],
       c.[Date3] [Reversion Date],
       c.[Date4] [Reinstatement Date],
       c.[Date5] [Renewal Date],
       c3.[Field1] [Sponsor ID],
       c3.[FirstName] + ' ' + c3.[LastName] [Sponsor Name], ----c3 last name
       c3.Email [Sponsor Email],
       c4.Field1 [First upline Integrated Global ID],
       c4.[FirstName] + ' ' + c4.[LastName] [First upline Integrated Global Name],
       c4.[Email] [First upline Integrated Global Email],
       c5.Field1 [First upline Integrated Presidential ID],
       c5.[FirstName] + ' ' + c5.[LastName] [First upline Integrated Presidential Name],
       c5.[Email] [First upline Integrated Presidential Email],
       a.CommissionableVolumeTotal,
       SUM(a.[Amount Including VAT]) OVER (PARTITION BY a.[Coach ID], a.[External Document No_]) [HOTS Total],
       a.TotalOrderAmount [purchase amount]
--a.[RETURN Type]

--INTO #Filter
FROM #abucket a
    JOIN [Exigo_ETL].[dbo].[Customers] c
        ON c.Field1 = a.[Coach ID] COLLATE DATABASE_DEFAULT
    JOIN [Exigo_ETL].[dbo].[PeriodVolumes] pv WITH (NOLOCK)
        ON c.[CustomerID] = pv.[CustomerID]
           AND pv.PeriodTypeID = 1
           AND pv.PeriodID = @comper
    LEFT JOIN [Exigo_ETL].[dbo].[Customers] c4 WITH (NOLOCK)
        ON SUBSTRING(CAST(pv.Volume127 AS NVARCHAR(32)), 1, (LEN(pv.Volume127) - 3)) = c4.[Field1]
    JOIN [Exigo_ETL].[dbo].[Customers] c5 WITH (NOLOCK)
        ON SUBSTRING(CAST(pv.Volume129 AS NVARCHAR(32)), 1, (LEN(pv.Volume129) - 3)) = c5.[Field1]
    JOIN [Exigo_ETL].[dbo].[Customers] c3 WITH (NOLOCK)
        ON c.[Field2] = c3.[Field1]
    LEFT JOIN [Exigo_ETL].[ODSY].[RanksMapping] R
        ON c.RankID = R.ExigoRankID
--WHERE a.[Coach ID] = '2591401'
--WHERE a.p_totalprice <> 0 
--AND p_name LIKE '%Pam Gish%'
ORDER BY a.[Coach ID];







