﻿/*
--===============================================================================
--Author:         Menkir Haile
--Create date: 03/14/2015

--Comments: Used to track the PV and Sponsoring trend for customers who purchased
			during Cyber Monday 2015.
--==============================================================================    
--REFERENCES
--Database					Table/View/UDF                             Action            
--------------------------------------------------------------------------------- 
--[TSFL_Reporting]			[COACH_CLIENT_RELATIONSHIP]					Select
--[ODYSSEY_ETL]				[ODYSSEY_ORDERS]							Select
--[ODYSSEY_ETL]             [ODYSSEY_ORDER_PRODUCT]						Select

--===============================================================================
--REVISION LOG
--Date			         Name                          Change

---------------------------------- Modifications ------------------------------------

--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--*/
Create Procedure [dbo].[usp_Nov2015CyberMondayIncentive] 
As
Declare @Commission_Period Date = '11/30/2015'
Declare @StartDate Date = '11/27/2015'
Declare @EndDate Date = '11/30/2015'

IF OBJECT_ID('Tempdb..#KitPurchasedCoachNov') IS NOT NULL DROP TABLE #KitPurchasedCoachNov
Select  CC.Customer_Number, 
		CC.Customer_ID,
		O.ENTRY_DATE
INTO #KitPurchasedCoachNov
FROM   [TSFL_Reporting].[dbo].[COACH_CLIENT_RELATIONSHIP] (NOLOCK) CC
JOIN ODYSSEY_ETL.dbo.ODYSSEY_ORDERS O ( NOLOCK ) 
		ON O.CUSTOMER_ID = CC.CUSTOMER_ID 
		AND CC.COMMISSION_PERIOD = CAST(O.COMMISSION_DATE AS DATE)
JOIN ODYSSEY_ETL.dbo.ODYSSEY_ORDER_PRODUCT P ( NOLOCK ) 
		ON P.ORDER_ID = O.ORDER_ID
WHERE   LEFT(P.PRODUCT_ID, 5) IN ( '31100','31110' )
		  AND CC.COMMISSION_PERIOD =  @Commission_Period and cast(O.entry_Date as date) between @StartDate and @EndDate
-----------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID('Tempdb..#KitPurchasedCoachData') IS NOT NULL DROP TABLE #KitPurchasedCoachData
Select CC.BusinessCenter_ID,
		CC.Customer_Number, 
		CC.Customer_ID,
		CC.FIRST_NAME,
		CC.Last_Name,
		CC.[Rank],
		CC.PV,
		CC.FLV,
		CC.COMMISSION_PERIOD,
		K.Entry_Date

INTO #KitPurchasedCoachData 
FROM #KitPurchasedCoachNov K  
Join  [TSFL_Reporting].[dbo].[COACH_CLIENT_RELATIONSHIP] (NOLOCK) CC
		ON K.Customer_ID = CC.Customer_ID
Where CC.COMMISSION_PERIOD >=  @Commission_Period
-----------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID('Tempdb..#SponsordCustomers') IS NOT NULL DROP TABLE #SponsordCustomers
Select 	CC.Customer_ID,
		CC.FIRST_NAME,
		CC.Last_Name,
		CC.[Rank],
		CC.Sponsor_ID,
		CC.ENTRY_DATE,
		CC.COMMISSION_PERIOD
INTO #SponsordCustomers
From #KitPurchasedCoachData K
Left Join [TSFL_Reporting].[dbo].[COACH_CLIENT_RELATIONSHIP] CC
	ON CC.SPONSOR_ID = K.BusinessCenter_ID AND CC.COMMISSION_PERIOD =  K.Commission_Period
Where --Sponsor_ID in (Select BusinessCenter_ID From #KitPurchasedCoach) AND CC.COMMISSION_PERIOD =  @Commission_Period2 AND
		year(CC.ENTRY_DATE) = Year(CC.COMMISSION_PERIOD) AND Month(CC.Entry_Date) = Month(CC.COMMISSION_PERIOD)
--------------------------------------------------------------------------------------
IF OBJECT_ID('Tempdb..#SponsordCoach') IS NOT NULL DROP TABLE #SponsordCoach
Select COMMISSION_PERIOD,
		Sponsor_ID
		, Count(Customer_ID) As SponsordCoach
INTO #SponsordCoach
FROM #SponsordCustomers
Where Rank <> 'NONE'
Group By COMMISSION_PERIOD,
		Sponsor_ID
---------------------------------------------------------------------------------------
IF OBJECT_ID('Tempdb..#SponsordNewClient') IS NOT NULL DROP TABLE #SponsordNewClient
Select COMMISSION_PERIOD,
		Sponsor_ID
		, Count(Customer_ID) As SponsordNewClient
INTO #SponsordNewClient
FROM #SponsordCustomers
Where year(Entry_Date) = Year(Commission_Period) AND Month(Entry_Date) = Month(Commission_Period)
Group By COMMISSION_PERIOD,
		Sponsor_ID

----------------------------------------------------------------------------------------------------
IF OBJECT_ID('Tempdb..#FinalData') IS NOT NULL DROP TABLE #FinalData
Select 	C.CUSTOMER_ID,
		C.CUSTOMER_NUMBER,
		C.ENTRY_DATE,
		C.FIRST_NAME,
		C.Last_Name,
		C.[Rank],
		C.COMMISSION_PERIOD,
		C.PV,
		C.FLV,
		ISNULL(SponsordCoach,0) AS [Number Of Coaches Sponsored],
		ISNULL(SponsordNewClient,0) AS [Number of New Customers Sponsored]
INTO #FinalData	
from #KitPurchasedCoachData C
Left Join #SponsordCoach HC ON C.BusinessCenter_ID = HC.Sponsor_ID AND C.COMMISSION_PERIOD = HC.COMMISSION_PERIOD
Left Join #SponsordNewClient NewClient ON C.BusinessCenter_ID = NewClient.Sponsor_ID AND C.COMMISSION_PERIOD = NewClient.COMMISSION_PERIOD
ORDER BY commission_Period,
		Customer_ID
---------------------------------------------------------------------------------------------
Select * From #FinalData 
Order By COMMISSION_PERIOD,
		CUSTOMER_ID

