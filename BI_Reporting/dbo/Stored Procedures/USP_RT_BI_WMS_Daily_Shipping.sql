﻿




/*
=================================================================================================
Author:         Daniel Dagnachew
Create date:    11/11/2019

==================================================================================================    
REFERENCES
Database                       Table/View/UDF                                       Action            
-------------------------------------------------------------------------------------------------
[NAVISION_PROD].[NAVPROD]        [dbo].[Jason Pharm$Sales Invoice Header]			Select
[NAVISION_PROD].[NAVPROD]        [dbo].[Jason Pharm$NAVRPWMI Shipment Entry]		Select
==================================================================================================
REVISION LOG
Date                   Name                  Change
--------------------------------------------------------------------------------------------------

==================================================================================================
NOTES:
--------------------------------------------------------------------------------------------------
==================================================================================================
*/


CREATE PROCEDURE [dbo].[USP_RT_BI_WMS_Daily_Shipping]
AS 
BEGIN

SELECT DISTINCT
       si.[External Document No_],
       si.[Posting Date] [Shipping Date],
       si.[Location Code],
       e.[Sent to WMS at Date]
FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Invoice Header] si (NOLOCK)
    LEFT JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$NAVRPWMI Shipment Entry] e (NOLOCK)
        ON e.[External Document No_] COLLATE DATABASE_DEFAULT = si.[External Document No_]
WHERE e.[Sent to WMS] = 1
      AND CAST(si.[Posting Date] AS DATE) = CAST(GETDATE() AS DATE)
ORDER BY [Shipping Date];

END 

