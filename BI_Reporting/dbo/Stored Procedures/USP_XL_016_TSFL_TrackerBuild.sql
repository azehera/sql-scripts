﻿



--/*
--===============================================================================
--Author:         Emily Trainor
--Create date: 01/05/2012
---------------------------TSFL Tracker Build--------------------------------
------Create Table for TSFL Tracker Data ------------------------------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--NAV_ETL       Jason Pharm$Sales Invoice Line                     Select
--				Jason Pharm$Sales Invoice Header				   Select                         
--===============================================================================
--REVISION LOG
--Date		          Name                          Change
---------------------------------------------------------------------------------
--06/22/2012		  Emily Trainor				Only updating last 60 days of data
--1/08/2013			  Emily Trainor				Updating calendar table referenced
--===============================================================================
--NOTES: Updated query to only delete and update the last 60 days of data as well
--		 as added business week and business month to table 
---------------------------------------------------------------------------------
--===============================================================================
--*/

CREATE procedure [dbo].[USP_XL_016_TSFL_TrackerBuild] as 
set nocount on ;

--EXECUTE AS LOGIN = 'ReportViewer'
-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = GETDATE()-30

-----------------------Delete Current Year Data--------------------------
Delete [BI_Reporting].dbo.XL_016_TSFL_LY_TY
Where [Posting Date] >= @Date

-------------------------Insert Current Year Data-----------------------------
------------------------Pull Sales Information--------------------------------
Insert into [BI_Reporting].dbo.XL_016_TSFL_LY_TY
SELECT 
 A. [Posting Date]
,[CalendarWeek]
,[CalendarYear]
,B.[Customer Posting Group]
,A.[Document No_] as Orders
,A. [No_]
,A. [Type]
,Sum(A.[Amount]) as Dollars
,Sum(A.[Quantity (Base)])as Units
,Case When ROW_NUMBER() OVER(PARTITION BY A.[Document No_] ORDER BY A.[Document No_] asc) ='1' then '1' else '0' end As OrderCount
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A 
Inner Join 
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 
     on A.[Document No_]= B.[No_]
inner join [BI_Reporting].[dbo].[XL_016_TSFLCALENDAR] C 
	on B.[Posting Date]= C.[CalendarDate]
WHERE A.[Posting Date] >= @Date
and B.[Customer Posting Group] in ('TSFL', 'Medifast')
GROUP BY 
 B.[Customer Posting Group]
,A. [No_]
,A. [Posting Date]
,A.[Document No_]
,A. [Type]
,[CalendarWeek]
,[CalendarYear]

Union all

--------------------------Pull Credit Information--------------------------------
SELECT 
 A. [Posting Date]
,[CalendarWeek]
,[CalendarYear]
,B.[Customer Posting Group]
,A.[Document No_] as Orders
,A.[No_]
,A.[Type]
,Sum(A.[Amount])*-1 as Dollars
,Sum(A.[Quantity (Base)])*-1 as Units
,(Case When ROW_NUMBER() OVER(PARTITION BY A.[Document No_] ORDER BY A.[Document No_] asc) ='1' then '1' else '0' end)*-1 As OrderCount      
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line] A 
Inner Join [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] B 
     on A.[Document No_]= B.[No_]
inner join [BI_Reporting].[dbo].[XL_016_TSFLCALENDAR] C 
	on B.[Posting Date]= C.[CalendarDate]
WHERE A.[Posting Date] >= @Date
and B.[Customer Posting Group] in ('TSFL', 'Medifast')
GROUP BY 
 B.[Customer Posting Group]
,A. [Posting Date]
,A. [No_]
,A.[Document No_]
,A. [Type]
,[CalendarWeek]
,[CalendarYear]




