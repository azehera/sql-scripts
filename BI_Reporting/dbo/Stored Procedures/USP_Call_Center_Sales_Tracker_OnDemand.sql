﻿

--/*
--==================================================================================
--Author:         Menkir Haile
--Create date: 12/28/2012

--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--[BI_Reporting]		[CallOndemand]						      Select
--BI_SSAS_Cubes			FactSales								  Select                       
--==================================================================================
--REVISION LOG
--Date           Name               Change
------------------------------------------------------------------------------------

					
--==================================================================================
--NOTES:
------------------------------------------------------------------------------------
--==================================================================================
--*/
CREATE procedure [dbo].[USP_Call_Center_Sales_Tracker_OnDemand] as
set nocount on ;

SELECT [USA_CUST_NBR]
      ,[Usa_ID]
      ,[USA_First_Nm]
      ,[USA_Last_Nm]
      ,[USA_HOME_PH]
      ,[Last Invoice Date]
      ,[Days Elasped]
      ,SUM(Amount) as Sales
  FROM [BI_Reporting].[dbo].[CallOndemand] a
  left join BI_SSAS_Cubes.dbo.FactSales b
  on a.[USA_CUST_NBR]=b.SelltoCustomerID
  and  [Posting Date]>'2014-06-18'
  group by [USA_CUST_NBR]
      ,[Usa_ID]
      ,[USA_First_Nm]
      ,[USA_Last_Nm]
      ,[USA_HOME_PH]
      ,[Last Invoice Date]
      ,[Days Elasped]