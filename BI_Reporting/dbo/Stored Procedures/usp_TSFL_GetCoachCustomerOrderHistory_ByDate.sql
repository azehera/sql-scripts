﻿

CREATE PROCEDURE [dbo].[usp_TSFL_GetCoachCustomerOrderHistory_ByDate]
    @Year AS INT ,
    @StartMonth AS NUMERIC ,
    @EndMonth AS NUMERIC
AS /***************************************************************************
Developer: Derald Smith
Date: 03/06/2015
Description: Used to get the order invoice data for helath tsfl customers for
	as specified period.
---------------------------------------------------------------------------
REFERENCES
Database              Table/View/UDF                             Action            
----------------------------------------------------------------------------------
NAV_ETL				[dbo].[Jason Pharm$Sales Invoice Header]	Select
NAV_ETL				[dbo].[Jason Pharm$Sales Invoice Line]	Select
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------

==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================

******************************************************************************/


--EXEC dbo.usp_TSFL_GetCoachCustomerOrderHistory_ByDate 2015, 3, 3

    SET NOCOUNT ON;

--DECLARE @Year AS INT
--DECLARE @StartMonth AS NUMERIC
--DECLARE @EndMonth AS NUMERIC

--SET @Year = 2015
--SET @StartMonth = 3
--SET @EndMonth = 3

    IF OBJECT_ID('tempdb..#Orders') IS NOT NULL
        DROP TABLE #Orders
    SELECT  YEAR([Order Date]) AS [Year] ,
            MONTH([Order Date]) AS [Month] ,
            CASE LEFT(H.[Sell-to Customer No_], 4)
              WHEN 'TSFL'
              THEN RIGHT(H.[Sell-to Customer No_],
                         LEN(H.[Sell-to Customer No_]) - 4)
              ELSE H.[Sell-to Customer No_]
            END AS [CID] ,
            H.[Salesperson Code] AS Coach_ID ,
            H.[No_] ,
            RT.[No_] AS [RetrunNo] ,
            H.[Order No_] AS [Order Number] ,
            CAST([Order Date] AS DATE) AS [Order Date]
    INTO    #Orders
    FROM    [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] (NOLOCK) H
            LEFT JOIN NAV_ETL.[dbo].[Jason Pharm$Sales Cr_Memo Header] (NOLOCK) RT ON RT.[Escalate Original Order No_] = H.[External Document No_]
    WHERE   YEAR([Order Date]) = @Year
            AND MONTH([Order Date]) BETWEEN @StartMonth
                                    AND     @EndMonth
            AND H.[Customer Posting Group] = 'TSFL'
        --AND H.[Order No_] = 'SO7477492'

--get SKU counts per order
    IF OBJECT_ID('tempdb..#SkuCounts') IS NOT NULL
        DROP TABLE #SkuCounts
    SELECT  [Document No_] ,
            COUNT(ISNULL(IL.[No_], 0)) AS [Sku Count]
    INTO    #SkuCounts
    FROM    NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL
            JOIN #Orders ON #Orders.No_ = IL.[Document No_]
    GROUP BY [Document No_]

--Get [Number of Units]
    IF OBJECT_ID('tempdb..#Units') IS NOT NULL
        DROP TABLE #Units
    SELECT  [Document No_] ,
            COUNT(ISNULL(IL.Quantity, 0)) AS [Units]
    INTO    #Units
    FROM    NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL
            JOIN #Orders ON #Orders.No_ = IL.[Document No_]
    WHERE   IL.[No_] NOT IN ( '63000' )
    GROUP BY [Document No_]

---GET [6:Total Invoice Value]
    IF OBJECT_ID('tempdb..#InvoiceValue') IS NOT NULL
        DROP TABLE #InvoiceValue
    SELECT  IL.[Document No_] ,
            CAST(SUM(( IL.Quantity * IL.[Unit Price] )
                     - IL.[Line Discount Amount]) AS MONEY)
            - CAST(SUM(ISNULL(cr.Quantity * Cr.[Unit Price], 0)) AS MONEY)  AS [Total Invoice Value] ,
            -1 * CAST(SUM(ISNULL(cr.Quantity * Cr.[Unit Price], 0)) AS MONEY) AS [Return Amount]
    INTO    #InvoiceValue
    FROM    #Orders
            JOIN NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL ON #Orders.No_ = IL.[Document No_]
            LEFT JOIN NAV_ETL.[dbo].[Jason Pharm$Sales Cr_Memo Line] Cr ON #Orders.[RetrunNo] = Cr.[Document No_]
    WHERE   IL.No_ <> '63000'
            AND il.Type <> 1
    GROUP BY IL.[Document No_]

--get [7:Invoice Discount]
    IF OBJECT_ID('tempdb..#Discount') IS NOT NULL
        DROP TABLE #Discount
    SELECT  [Document No_] ,
             CAST(SUM([Line Discount Amount]) AS MONEY) AS [Invoice Discount]
    INTO    #Discount
    FROM    NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL
            JOIN #Orders ON #Orders.No_ = IL.[Document No_]
    GROUP BY [Document No_]

--Get [8:Tot Net Invoice Prod Value]
    IF OBJECT_ID('tempdb..#NetProdValue') IS NOT NULL
        DROP TABLE #NetProdValue
    SELECT  IL.[Document No_] ,
            CAST(SUM(IL.Quantity * IL.[Unit Price]) AS MONEY)
            - CAST(SUM(ISNULL(cr.Quantity * Cr.[Unit Price], 0)) AS MONEY) AS [Tot Net Invoice Prod Value]
    INTO    #NetProdValue
    FROM    NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL
            JOIN #Orders ON #Orders.No_ = IL.[Document No_]
            LEFT JOIN NAV_ETL.[dbo].[Jason Pharm$Sales Cr_Memo Line] Cr ON #Orders.[RetrunNo] = Cr.[Document No_]
    WHERE   IL.[Item Category Code] IN ( 'BARS', 'BREAKFAST', 'CRUNCHERS',
                                         'DESSERT', 'DRINKS', 'EXTRAS',
                                         'HEARTY', 'INFUSERS', 'KITS',
                                         'LEAN GREEN', 'MEAL', 'RTD', 'RTE',
                                         'SHAKES', 'SNACKS', 'SOFTBAKE',
                                         'SOUPS', 'VITAMINS' )
            AND IL.No_ <> '63000'
            AND il.Type <> 1
    GROUP BY IL.[Document No_]

--Get [Taxes]
    IF OBJECT_ID('tempdb..#Tax') IS NOT NULL
        DROP TABLE #Tax
    SELECT  [Document No_] ,
            CAST(SUM([Amount Including VAT] - IL.Amount) AS MONEY) AS [Tax Total]
    INTO    #Tax
    FROM    NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL
            JOIN #Orders ON #Orders.No_ = IL.[Document No_]
    GROUP BY [Document No_]

--Get Shipping Amt
    IF OBJECT_ID('tempdb..#Shipping') IS NOT NULL
        DROP TABLE #Shipping
    SELECT  [Document No_] ,
            CAST(SUM(IL.[Unit Price]) AS MONEY) AS [Shipping Total]
    INTO    #Shipping
    FROM    NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL
            JOIN #Orders ON #Orders.No_ = IL.[Document No_]
    WHERE   IL.No_ = '63000' --shipping sku
            AND IL.Type = 1
    GROUP BY [Document No_]

--Get [9a: Shipping Discount]
    IF OBJECT_ID('tempdb..#ShippingDisc') IS NOT NULL
        DROP TABLE #ShippingDisc
    SELECT  [Document No_] ,
            CAST(SUM(IL.[Amount Including VAT]) AS MONEY)
            - CAST(SUM(IL.[Unit Price]) AS MONEY) AS [Shipping Discount]
    INTO    #ShippingDisc
    FROM    NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL
            JOIN #Orders ON #Orders.No_ = IL.[Document No_]
    WHERE   IL.No_ = '63000' --shipping sku
            AND IL.Type = 1
    GROUP BY [Document No_]

--get [10: Net Invoice]
    IF OBJECT_ID('tempdb..#NetInv') IS NOT NULL
        DROP TABLE #NetInv
    SELECT  [Document No_] ,
            CAST(SUM([Amount Including VAT]) AS MONEY) AS [Amount Including VAT]
    INTO    #NetInv
    FROM    NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] IL
            JOIN #Orders ON #Orders.No_ = IL.[Document No_]
    GROUP BY [Document No_]

--Put it all together:
    SELECT  O.CID ,
            O.Coach_ID ,
            O.[Order Number] ,
            O.[Order Date] ,
            SC.[Sku Count] AS [Number of Skus] ,
            U.Units AS [Number of Units]
	
	--,IV.[Total Invoice Value]AS [Total Invoice Value]  --- ISNULL(S.[Shipping Total],0) - ISNULL(T.[Tax Total],0) + D.[Invoice Discount] 
            ,
            PV.[Tot Net Invoice Prod Value] + D.[Invoice Discount] AS [Total Invoice Value] ,
            IV.[Return Amount] ,
            D.[Invoice Discount] ,
            PV.[Tot Net Invoice Prod Value]
	--,S.[Shipping Total]
	--,T.[Tax Total]
            ,
            ISNULL(S.[Shipping Total], 0) + ISNULL(T.[Tax Total], 0) AS [Tot Invoice Ship+Tax]--9:[Tot Invoice Ship+Tax]
	--,ISNULL(S.[Shipping Total],0)  AS [Shipping Total]
            ,
            ISNULL(SD.[Shipping Discount], 0) AS [Shipping Discounts] --9a: [Shipping Discounts]
	--,ISNULL(NI.[Amount Including VAT],0) + ISNULL(S.[Shipping Total],0) AS [Net Invoice]
            ,
            PV.[Tot Net Invoice Prod Value] + D.[Invoice Discount]
            + ( ISNULL(S.[Shipping Total], 0) + ISNULL(T.[Tax Total], 0) )
            + ISNULL(SD.[Shipping Discount], 0) AS [Net Invoice]
    FROM    #Orders O
            LEFT JOIN #SkuCounts SC ON SC.[Document No_] = O.No_
            LEFT JOIN #Units U ON U.[Document No_] = O.No_
            LEFT JOIN #InvoiceValue IV ON IV.[Document No_] = O.No_
            LEFT JOIN #Discount D ON D.[Document No_] = O.No_
            LEFT JOIN #NetProdValue PV ON PV.[Document No_] = O.No_
            LEFT JOIN #Shipping S ON S.[Document No_] = O.No_
            LEFT JOIN #ShippingDisc SD ON SD.[Document No_] = O.[No_]
            LEFT JOIN #Tax T ON T.[Document No_] = O.No_
            LEFT JOIN #NetInv NI ON NI.[Document No_] = O.No_
    WHERE   o.Coach_ID IN (
            SELECT DISTINCT
                    C.CustNo
            FROM    BI_Reporting.dbo.TSFL_CoachesWithCommissions_2012_to_2014 C ( NOLOCK ) )


