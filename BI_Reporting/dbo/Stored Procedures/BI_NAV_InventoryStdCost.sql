﻿


CREATE PROCEDURE [dbo].[BI_NAV_InventoryStdCost]
AS
/****** MED0001 MRH 01/23/12: Business Intelligence / Executive Dashboard ******/

DECLARE @Today DATETIME

SET @Today = CAST(FLOOR(CAST(GETDATE()-10 AS float)) AS DATETIME)
select @Today

BEGIN TRANSACTION

DELETE FROM [BI_Reporting].[dbo].[Snapshot$NAV_InventoryStdCost]
WHERE (RunDate = @Today)

COMMIT TRANSACTION

BEGIN TRANSACTION

INSERT INTO [BI_Reporting].[dbo].[Snapshot$NAV_InventoryStdCost]
           ([RunDate]
           ,[ItemNo]
           ,[InventoryPostingGroup]
           ,[ItemCategoryCode]
           ,[ProductGroupCode]
           ,[StandardCost])
SELECT @Today
      ,No_ AS _ItemNo
      ,[Inventory Posting Group] AS _InvPostGrp
      ,[Item Category Code] AS _ItemCat
      ,[Product Group Code] AS _ProdGrpCode
      ,[Standard Cost] AS _StdCost
FROM NAV_ETL.dbo.[Jason Pharm$Item]

COMMIT TRANSACTION

