﻿


--/*
--==================================================================================
--Author:         Daniel D.
--Create date: 05/29/2019

--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--[NAV_ETL]		        [Jason Pharm$Sales Header]				  Select                     
--==================================================================================
--REVISION LOG
--Date           Name               Change
------------------------------------------------------------------------------------

--======================================================================================
--NOTES:
----------------------------------------------------------------------------------------
/*This sproc is used to pull unshipped ordes' count from different distribution center.
--======================================================================================
--*/
CREATE PROCEDURE [dbo].[USP_Get_Unshipped_Orders]
AS
SET NOCOUNT ON;

SELECT COUNT(No_) [Order Count],
       [Order Date],
       [Location Code] DC
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Header]
WHERE [Document Type] = 1
GROUP BY [Location Code],
         [Order Date]
ORDER BY [Location Code],
         [Order Date];
