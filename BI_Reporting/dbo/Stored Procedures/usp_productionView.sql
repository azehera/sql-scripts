﻿Create PROCEDURE [dbo].[usp_productionView]
AS

SELECT *
FROM [NAV_ETL].[dbo].[Snapshot$NAV_Production] PV
WHERE RunDate = CAST(FLOOR(CAST(GETDATE() AS float)) AS DATETIME)
