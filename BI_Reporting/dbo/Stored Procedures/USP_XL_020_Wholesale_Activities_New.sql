﻿



/*
==================================================================================
Author:       Luc Emond
Create date:  02/21/2013
---------------------------[USP_XL_020_Wholesale_Activities]-------------------
Provide Monthly Doctors transaction in buckets time frame-----
==================================================================================   
REFERENCES
Database              Table/View/UDF                       Action            
----------------------------------------------------------------------------------
[BI_Reporting]      [Calendar]                             Select
[NAV_ETL]           [Jason Pharm$Sales Invoice Line]       Select
[NAV_ETL]           [Jason Pharm$Sales Invoice Header]     Select
[NAV_ETL]           [Jason Pharm$Customer]                 Select
                  
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------
6/15/2015     Menkir Haile				EXEC dbo.USP_XL_039_TSFL_Compliance_Exception_Report
										is commented out				
==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_020_Wholesale_Activities_New] As
SET NOCOUNT ON;

--EXECUTE AS LOGIN = 'ReportViewer'
--------TSFL MONTHLY REPORT------NEEDED FOR TESTING-----
--EXEC dbo.USP_XL_039_TSFL_Compliance_Exception_Report


----DECLARE AND SET TIMEFRAME-----
DECLARE @Today DATETIME, @LastYear DATETIME, @ThisYear DATETIME

SET @Today = (Select CONVERT(varchar(10), GetDate()-10,121))
SET @ThisYear = (Select [LastDateOfYear] from [dbo].[calendar] where CalendarDate = @Today) 
SET @LastYear = (Select [FirstDateOfYear] from [dbo].[calendar] where CalendarDate = @Today-365) 

--select @Today, @ThisYear, @LastYear

----select the data------
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
select 
 Convert(varchar(20),A.[Sell-to Customer No_] ) AS [Sell-to Customer No_]
,B.[Sell-to Customer Name]
,C.[Address]
,C.[Address 2]
,C.[City]
,C.[County] As State
,C.[Phone No_]
,Convert(Varchar(20),c.[Post Code] ) AS [Post Code]
,C.[E-Mail]
,SUM(A.[Amount]) AS TotalNetsales
,MAX(B.[Posting Date]) LastOrderdate
,Datediff(dd, MAX(A.[Posting Date]), GetDate()) as DaysSinceLastInvoice
,Case When Datediff(dd, MAX(A.[Posting Date]), GetDate()) <= 90 Then '[<= 90 Days]' 
      When Datediff(dd, MAX(A.[Posting Date]), GetDate()) BETWEEN  91 AND 120 Then '[91 - 120 Days]'
      When Datediff(dd, MAX(A.[Posting Date]), GetDate()) BETWEEN 121 AND 180 Then '[121 - 180 Days]'
      When Datediff(dd, MAX(A.[Posting Date]), GetDate()) BETWEEN 181 AND 270 Then '[181 - 270 Days]'   
      When Datediff(dd, MAX(A.[Posting Date]), GetDate()) BETWEEN 271 AND 365 Then '[271 - 365 Days]' 
      Else '[>365 Days]' end as TimeFrame
INTO #A
From [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A With (nolock)
LEFT OUTER JOIN 
     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B With (nolock)
      On A.[Document No_] = B.[No_]
LEFT OUTER JOIN       
     [NAV_ETL].[dbo].[Jason Pharm$Customer] C With (nolock)
      On  A.[Sell-to Customer No_]  COLLATE DATABASE_DEFAULT = c.[No_]
Where B.[Posting Date]Between @LastYear and @ThisYear
  AND B.[Customer Posting Group] IN('DOCTORS')
  AND A.[Sell-to Customer No_] <>''
Group By
 Convert(varchar(20),A.[Sell-to Customer No_] )
,B.[Sell-to Customer Name]
,C.[Address]
,C.[Address 2]
,C.[City]
,C.[County]
,C.[Phone No_]
,C.[E-Mail]
,Convert(Varchar(20),c.[Post Code] ) 

Select * from #A




