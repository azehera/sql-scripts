﻿
/*
--==================================================================================

Author:         Daniel Dagnachew

Create date: 01/14/2014
Modified date 9/3/2015
Modified by Aslam Tailor

-----------------------------dbo.BeSlim_Cancellation-------------------

Create Daily Flash reports from Cube data

--==================================================================================   

REFERENCES

Database              Table/View/UDF                            Action            

------------------------------------------------------------------------------------
UCART.prdmartini_STORE     [dbo].[v_cart_header]                    Select
UCART.prdmartini_STORE     [dbo].[Snapshot$NAV_Product_SalesCube]   Select

Replace input parameter  and add sorting                        

=====================================================================================================
REVISION LOG

Date           Name                          Change

-----------------------------------------------------------------------------------------------------
4/8/2016		D. SMith					CHanged so that it can no longer 

===================================================================================================

NOTES:

----------------------------------------------------------------------------------------------------------

====================================================================================================



*/

--exec [dbo].[USP_XL_084_Autoship_Cancellation] 0


CREATE PROCEDURE [dbo].[USP_XL_084_Autoship_Cancellation] 
@CurrentYearYes BIT
AS
    DECLARE @yesterday DATETIME;



    DECLARE @FirstdayOfTheYear DATETIME;



    DECLARE @Today DATETIME;



    DECLARE @EndOfTheYear DATETIME;

    DECLARE @CurrentYear VARCHAR(4);

    IF @CurrentYearYes = 1
        BEGIN 


            SET @CurrentYear = YEAR(GETDATE());
			SET @FirstdayOfTheYear = DATEADD(YEAR,
                                     DATEDIFF(YEAR, 0,
                                              DATEADD(YEAR, 0, GETDATE())), 0);

        END; 

    IF @CurrentYearYes = 0
        BEGIN 

            SET @CurrentYear = ( YEAR(GETDATE()) - 1 );
			SET @FirstdayOfTheYear = DATEADD(YEAR,
                                     DATEDIFF(YEAR, 0,
                                              DATEADD(YEAR, -1, GETDATE())), 0);
        END; 

    

    SET @yesterday = DATEADD(DAY, DATEDIFF(DAY, -1, GETDATE()), 0);

    SET @Today = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0);

    SET @EndOfTheYear = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, -1); 

    IF OBJECT_ID('Tempdb..#A') IS NOT NULL
        DROP TABLE #A;

    SELECT  COUNT(cst.USA_CUST_NBR) Web ,
            CAST(FLOOR(CAST(A.CTH_MODIFY_DT AS FLOAT)) AS DATETIME) [Date]
    INTO    #A
    FROM    UCART.prdmartini_STORE.dbo.V_CART_HEADER A ( NOLOCK )
            JOIN UCART.prdmartini_STORE.dbo.USER_ACCOUNT cst ( NOLOCK ) ON cst.USA_ID = CTH_CREATED_FOR
            JOIN UCART.prdmartini_STORE.dbo.USER_ACCOUNT chgBy ( NOLOCK ) ON chgBy.USA_ID = CTH_MODIFIED_BY
    WHERE   A.CTH_MODIFY_DT < = @yesterday
            AND A.CTH_MODIFY_DT > = @FirstdayOfTheYear
            AND A.CTH_TYPE_CD = 'BSL'
            AND chgBy.USA_TYPE_CD = 'B2B'
            AND A.CTH_STATUS_CD = 'I'
    GROUP BY CAST(FLOOR(CAST(A.CTH_MODIFY_DT AS FLOAT)) AS DATETIME); 

    IF OBJECT_ID('Tempdb..#B') IS NOT NULL
        DROP TABLE #B;

    SELECT  COUNT(cst.USA_CUST_NBR) CCA ,
            CAST(FLOOR(CAST(A.CTH_MODIFY_DT AS FLOAT)) AS DATETIME) [Date]
    INTO    #B
    FROM    UCART.prdmartini_STORE.dbo.V_CART_HEADER A ( NOLOCK )
            JOIN UCART.prdmartini_STORE.dbo.USER_ACCOUNT cst ( NOLOCK ) ON cst.USA_ID = CTH_CREATED_FOR
            JOIN UCART.prdmartini_STORE.dbo.USER_ACCOUNT chgBy ( NOLOCK ) ON chgBy.USA_ID = CTH_MODIFIED_BY
    WHERE   A.CTH_MODIFY_DT < = @yesterday
            AND A.CTH_MODIFY_DT > = @FirstdayOfTheYear
            AND A.CTH_TYPE_CD = 'BSL'
            AND chgBy.USA_TYPE_CD = 'CSR'
            AND A.CTH_STATUS_CD = 'I'
    GROUP BY CAST(FLOOR(CAST(A.CTH_MODIFY_DT AS FLOAT)) AS DATETIME);

    IF OBJECT_ID('Tempdb..#C') IS NOT NULL
        DROP TABLE #C;

    SELECT  COUNT(cst.USA_CUST_NBR) Web ,
            CAST(FLOOR(CAST(A.CTH_MODIFY_DT AS FLOAT)) AS DATETIME) [Date]
    INTO    #C
    FROM    UCART.prdmartini_STORE.dbo.V_CART_HEADER A ( NOLOCK )
            JOIN UCART.prdmartini_STORE.dbo.USER_ACCOUNT cst ( NOLOCK ) ON cst.USA_ID = CTH_CREATED_FOR
            JOIN UCART.prdmartini_STORE.dbo.USER_ACCOUNT chgBy ( NOLOCK ) ON chgBy.USA_ID = CTH_MODIFIED_BY
    WHERE   A.CTH_MODIFY_DT < = @yesterday
            AND A.CTH_MODIFY_DT > = @FirstdayOfTheYear
            AND A.CTH_TYPE_CD = 'VIP'
            AND chgBy.USA_TYPE_CD = 'B2B'
            AND A.CTH_STATUS_CD = 'I'
    GROUP BY CAST(FLOOR(CAST(A.CTH_MODIFY_DT AS FLOAT)) AS DATETIME); 

    IF OBJECT_ID('Tempdb..#D') IS NOT NULL
        DROP TABLE #D;



    SELECT  COUNT(cst.USA_CUST_NBR) CCA ,
            CAST(FLOOR(CAST(A.CTH_MODIFY_DT AS FLOAT)) AS DATETIME) [DATE]
    INTO    #D
    FROM    UCART.prdmartini_STORE.dbo.V_CART_HEADER A ( NOLOCK )
            JOIN UCART.prdmartini_STORE.dbo.USER_ACCOUNT cst ( NOLOCK ) ON cst.USA_ID = CTH_CREATED_FOR
            JOIN UCART.prdmartini_STORE.dbo.USER_ACCOUNT chgBy ( NOLOCK ) ON chgBy.USA_ID = CTH_MODIFIED_BY
    WHERE   A.CTH_MODIFY_DT < = @yesterday
            AND A.CTH_MODIFY_DT > = @FirstdayOfTheYear

            AND A.CTH_TYPE_CD = 'VIP'
            AND chgBy.USA_TYPE_CD = 'CSR'
            AND A.CTH_STATUS_CD = 'I'
    GROUP BY CAST(FLOOR(CAST(A.CTH_MODIFY_DT AS FLOAT)) AS DATETIME);
 

    SELECT  [Date] ,
            ISNULL([BeSlim Cancellations - WEB],0) AS [BeSlim Cancellations - WEB] ,
            ISNULL([BeSlim Cancellations - CCA],0) AS [BeSlim Cancellations - CCA] ,
            ISNULL([Medifast Advantage Cancellations - WEB],0) AS [Medifast Advantage Cancellations - WEB] ,
            ISNULL([Medifast Advantage Cancellations - CCA],0) AS [Medifast Advantage Cancellations - CCA] ,
            [Year] ,
            [Month]
    FROM    ( SELECT    #A.[Date] ,
                        #A.Web AS [BeSlim Cancellations - WEB] ,
                        #B.CCA AS [BeSlim Cancellations - CCA] ,
                        #C.Web AS [Medifast Advantage Cancellations - WEB] ,
                        #D.CCA AS [Medifast Advantage Cancellations - CCA] ,
                        @CurrentYear AS [Year] ,
                        DATENAME(MONTH, #A.[Date]) AS [Month]
              FROM      #A
                        FULL OUTER JOIN #B ON #A.[Date] = #B.[Date]
                        FULL OUTER JOIN #C ON #B.[Date] = #C.[Date]
                        FULL OUTER JOIN #D ON #C.[Date] = #D.[DATE]
              WHERE     YEAR(#A.[Date]) = @CurrentYear
              UNION
              SELECT    CAST(FLOOR(CAST(CAST([CalendarDate] AS DATETIME) AS FLOAT)) AS DATETIME) [DATE] ,
                        0 AS [BeSlim Cancellations - WEB] ,
                        0 AS [BeSlim Cancellations - CCA] ,
                        0 AS [Medifast Advantage Cancellations - WEB] ,
                        0 AS [Medifast Advantage Cancellations - CCA] ,
                        @CurrentYear AS [Year] ,
                        DATENAME(MONTH, CalendarDate) AS [Month]
              FROM      BI_SSAS_Cubes.dbo.DimCalendar
              WHERE     [CalendarDate] < = @EndOfTheYear
                        AND [CalendarDate] > = @Today
                        AND CAST(CalendarYear AS VARCHAR(4)) = @CurrentYear
            ) a
    ORDER BY Date ASC;

