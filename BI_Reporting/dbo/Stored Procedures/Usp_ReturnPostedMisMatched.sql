﻿


CREATE procedure [dbo].[Usp_ReturnPostedMisMatched] as 

set NoCount On ;

EXECUTE AS LOGIN = 'ReportViewer'

TRUNCATE TABLE [BI_REPORTING].dbo.ReturnPostedMisMatched
INSERT INTO [BI_REPORTING].dbo.ReturnPostedMisMatched
SELECT Ua.USA_CUST_NBR [Customer Number]
,UA.USA_FIRST_NM + ' ' + ua.USA_LAST_NM as [Customer Name]
,OH.ORH_ORG_NBR, oh.ORH_EXT_RMA_NBR AS [Cart SRA Num]
,ORH_TYPE_CD, oh.ORH_STATUS_CD
,oh.ORH_CREATE_DT,oh.ORH_TOTAL_AMT
,'' as [Status]
,'' as [NavCustomerNumber]
,'' as [Match]
FROM ECOMM_ETL .dbo.V_ORDER_HEADER OH (NOLOCK) 
JOIN ECOMM_ETL .dbo.USER_ACCOUNT UA (NOLOCK) ON UA.USA_ID = OH.ORH_CREATED_FOR 
WHERE ORH_TYPE_CD IN ('RET', 'EXC') 
AND OH.ORH_CREATE_DT >= (SELECT  DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-366), 0))

UPDATE [BI_REPORTING].dbo.ReturnPostedMisMatched
SET [Status] = 'Open', NavCustomerNumber = Case When LEFT([Sell-to Customer No_],4)  <> 'TSFL' Then [Sell-to Customer No_]             
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 14 Then Right(right ([Sell-to Customer No_],10),20)        
     When LEFT([Sell-to Customer No_],5)  ='TSFL-' and LEN([Sell-to Customer No_]) = 13 Then Right(right ([Sell-to Customer No_],8),20)       
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 13 Then Right(right ([Sell-to Customer No_],9),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 12 Then Right(right ([Sell-to Customer No_],8),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 11 Then Right(right ([Sell-to Customer No_],7),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 10 Then Right(right ([Sell-to Customer No_],6),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) =  9 Then Right(right ([Sell-to Customer No_],5),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) =  8 Then Right(right ([Sell-to Customer No_],4),20) end
from NAV_ETL.[dbo].[Jason Pharm$Sales Header] B (NOLOCK)
WHERE  [BI_REPORTING].dbo.ReturnPostedMisMatched.[Cart SRA Num] COLLATE DATABASE_DEFAULT= b.[No_]
	
	
UPDATE [BI_REPORTING].dbo.ReturnPostedMisMatched
SET [Status] = 'Posted', NavCustomerNumber = Case When LEFT([Sell-to Customer No_],4)  <> 'TSFL' Then [Sell-to Customer No_]             
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 14 Then Right(right ([Sell-to Customer No_],10),20)        
     When LEFT([Sell-to Customer No_],5)  ='TSFL-' and LEN([Sell-to Customer No_]) = 13 Then Right(right ([Sell-to Customer No_],8),20)       
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 13 Then Right(right ([Sell-to Customer No_],9),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 12 Then Right(right ([Sell-to Customer No_],8),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 11 Then Right(right ([Sell-to Customer No_],7),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) = 10 Then Right(right ([Sell-to Customer No_],6),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) =  9 Then Right(right ([Sell-to Customer No_],5),20)         
     When LEFT([Sell-to Customer No_],4)  ='TSFL' and LEN([Sell-to Customer No_]) =  8 Then Right(right ([Sell-to Customer No_],4),20) end
FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Return Receipt Header] b (NOLOCK)
WHERE [BI_REPORTING].dbo.ReturnPostedMisMatched.[Cart SRA Num] COLLATE DATABASE_DEFAULT= b.[Return Order No_]	

UPDATE [BI_REPORTING].dbo.ReturnPostedMisMatched SET MATCH ='N'

Update [BI_REPORTING].dbo.ReturnPostedMisMatched set Match ='Y' Where [Customer Number] = NavCustomerNumber and Match ='N'
Update [BI_REPORTING].dbo.ReturnPostedMisMatched set Match ='Y' Where [Customer Number] = Right(NavCustomerNumber,8)and Match ='N'
Update [BI_REPORTING].dbo.ReturnPostedMisMatched set Match ='Y' Where [Customer Number] = Right(NavCustomerNumber,7)and Match ='N'


select *  from [BI_REPORTING].dbo.ReturnPostedMisMatched where Match ='N' and [Status] <>'' Order By ORH_CREATE_DT










