﻿



/*
=============================================================================================
Author:         Luc Emond
Create date: 03/07/2014
-------------------------[USP_XL_062_TSFL_ActiveCoachByMonth]-----------------------------
Daily Coremetric info with cost
=============================================================================================   
REFERENCES
Database                          Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[ODYSSEY_ETL]              [dbo].[ODYSSEY_CORE_COMMISSION_ENTRY]
[ODYSSEY_ETL]              [dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] 
[BI_Reporting]             [dbo].[calendar_TSFL]
[ODYSSEY_ETL]              [dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]
[ODYSSEY_ETL]              [dbo].[ODYSSEY_CUSTOMER_ADDRESS] 
[ODYSSEY_ETL]              [dbo].[ODYSSEY_CUSTOMER]
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------

==========================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_062_TSFL_ActiveCoachByMonth] as

set nocount on;


IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT 
         A.[CUSTOMER_ID]
      ,A.[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),A.[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,CalendarYear
      ,CalendarMonth
      ,isnull([TOTAL_ADJUSTMENT],0) + isnull([GROWTH_BONUS],0) + isnull([GENERATION_BONUS],0)+ isnull([OVERRIDE_BONUS],0) + isnull([CSC_AMOUNT],0) 
       + isnull([CAB_BONUS],0) + isnull([ASSIST_BONUS],0) + ISNULL([ROLLING_BONUS],0)+ isnull([OVERRIDE_BONUS_GED],0)+ isnull([OVERRIDE_BONUS_PED],0)
       + isnull([CERTIFIED_BONUS],0)+ Isnull([CUSTOMER_SUPPORT_BONUS],0)+ Isnull([FIBC],0)+ Isnull([ANSITION_ADJ],0) as [Total Earnings]
    Into #a
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] A
      inner join[ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] B
            on A.[COMMISSION_ENTRY_ID] = B.[COMMISSION_ENTRY_ID]
      Inner join [BI_Reporting].[dbo].[calendar_TSFL] C 
            on Convert(varchar(10),A.[COMMISSION_DATE],121) = C.[CalendarDate]
        Where Convert(Varchar(10),A.[COMMISSION_DATE],121) between  (SELECT distinct [FirstDateOfYear]FROM [BI_Reporting].[dbo].[calendar] where CalendarYear=YEAR(Getdate()))
                                                           and (SELECT distinct ([FirstDateOfMonth]-1)FROM [BI_Reporting].[dbo].[calendar] where CalendarDate = Convert(Varchar(10),GETDATE(),121)) 
            and A.[CUSTOMER_ID] not in('9602206','9602208','9663528','9747036','9681355','9749475','9856645','9602209','9749476','9602205')
      Order by A.[COMMISSION_DATE] asc
      

--------------------------Pulling Weekly Earnings for Active Earning Coaches--------------------------
Insert into #a
SELECT 
        [CUSTOMER_ID]
        ,[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,CalendarYear
      ,CalendarMonth
      ,([CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]+[TOTAL_ADJUSTMENT]) as [Total Earnings]
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY] A
  Inner join [BI_Reporting].[dbo].[calendar_TSFL] B 
            on Convert(varchar(10),A.[COMMISSION_DATE],121) = B.[CalendarDate]
  Where Convert(Varchar(10),[COMMISSION_DATE],121) between  (SELECT distinct [FirstDateOfYear]FROM [BI_Reporting].[dbo].[calendar] where CalendarYear=YEAR(Getdate()))
                                                   and (SELECT distinct ([FirstDateOfMonth]-1)FROM [BI_Reporting].[dbo].[calendar] where CalendarDate = Convert(Varchar(10),GETDATE(),121))
      and A.[CUSTOMER_ID] not in('9602206','9602208','9663528','9747036','9681355','9749475','9856645','9602209','9749476','9602205')
      Order by [COMMISSION_DATE] asc
      
      



----temp table for address---
IF object_id('Tempdb..#Address') Is Not Null DROP TABLE #Address

Select *  into #Address from [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS]
where [Address_Type] = 'Main'



IF object_id('Tempdb..#k') Is Not Null DROP TABLE #k
Select
'Active HC' as Category
,CalendarYear
,CalendarMonth
,Case When CalendarMonth = 1 Then 'January'
      When CalendarMonth = 2 Then 'February'
      When CalendarMonth = 3 Then 'March'
      When CalendarMonth = 4 Then 'April'
      When CalendarMonth = 5 Then 'May'
      When CalendarMonth = 6 Then 'June'
      When CalendarMonth = 7 Then 'July'
      When CalendarMonth = 8 Then 'August'
      When CalendarMonth = 9 Then 'September'
      When CalendarMonth = 10 Then 'October'
      When CalendarMonth = 11 Then 'November'
      When CalendarMonth = 12 Then 'December' End As CalendarMonthName
,A.[CUSTOMER_ID]
,[CUSTOMER_NUMBER]
,C.[FIRST_NAME]
,C.[LAST_NAME]
,B.[POSTAL_CODE]
,Case When [DMA Code] IS null then ' ' else [DMA Code] end as [DMA Code]
,Case When [DMA Name] IS null then 'Unknown' else [DMA Name] end as [DMA Name]
into #k
From #A A
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
            on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
      Inner join #Address B
            on C.[CUSTOMER_ID] = B. [CUSTOMER_ID]
      LEFT JOIN Zip_codes_deluxe.dbo.DMA_Data d
            on d.ZipCode=left(B.[POSTAL_CODE],5)      
      Where [CUSTOMER_NUMBER] not in('2','6','6957001','16654601','9027001','16918401','30006445','101','3','16918501')
      and [Total Earnings] >0
      
      
select 
CalendarMonthName
,Count(Distinct [CUSTOMER_NUMBER]) ActiveCoachCount
from #k
Group By
CalendarMonthName,CalendarMonth
Order by CalendarMonth




