﻿




/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------
[Exigo_ETL]          [dbo].[Customers]                       Select 
==================================================================================
REVISION LOG
Date           Name                          Change
-----------------------------------------------------------------------------------
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - BI Hybris ETL Max OrderDateTime ] 
AS 


--when hybris database is in cst
--IF OBJECT_ID('Tempdb..#temp') IS NOT NULL DROP TABLE #temp;
--SELECT MAX(createdTS) AS 'maxtimeDate', DATEADD(mi,-60,GETDATE()) AS  'TimeNow'
--INTO #temp
--FROM Hybris_ETL.mdf_prd_database.orderentries
--WHERE createdTS >= (GETDATE()-2)

--when hybris database is in est
IF OBJECT_ID('Tempdb..#temp') IS NOT NULL DROP TABLE #temp;
SELECT MAX(createdTS) AS 'maxtimeDate', GETDATE() AS  'TimeNow'
INTO #temp
FROM Hybris_ETL.mdf_prd_database.orderentries
WHERE createdTS >= (GETDATE()-2)

SELECT maxtimeDate, TimeNow, (DATEDIFF(ss,maxtimeDate,TimeNow)/60.0) AS 'MinutesDifference'
FROM #temp




