﻿


/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 10/31/2019
------------------------------------------------
Future Autoship orders
--==================================================================================   
REFERENCES   
Database              Table/View/UDF                                Action            
------------------------------------------------------------------------------------
[EXIGO].[ExigoSyncSQL_PRD].[dbo].[Periods]                          Select
[EXIGO].[ExigoSyncSQL_PRD].dbo.Orders                               Select
EXIGO_ETL             dbo.Customers                                 Select  
[EXIGO].[ExigoSyncSQL_PRD].CUSTOM.[Order_Header]                    Select
[MDM_RELTIO].[MDM_Reltio].[dbo].[RELTIO_GROUP_MDM_CUSTOMERS_ALL]    Select
BI_Reporting          dbo.November_December_Incentive               Insert                     
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_RT_BI_November_December_Incentive_Report]
AS
BEGIN

IF OBJECT_ID('Tempdb..##Incentive') IS NOT NULL
    DROP TABLE ##Incentive;
CREATE TABLE ##Incentive
(
    [Week Number] [INT] NOT NULL,
    [OPTAVIA Coach ID] [NVARCHAR](100) NOT NULL,
    [Exigo Coach ID] [INT] NULL,
    [Coach Recognition Last] [NVARCHAR](100) NULL,
    [Coach Recognition First] [NVARCHAR](100) NULL,
    [Client Exigo ID] [INT] NOT NULL,
    [Client Optavia ID] [NVARCHAR](100) NOT NULL,
    [FullName] [NVARCHAR](101) NOT NULL,
    [Coach Email] [NVARCHAR](50) NULL,
    [Exigo Coach of Coach ID] [INT] NULL,
    [Optavia Coach of Coach ID] [NVARCHAR](100) NULL,
    [Coach of Coach Last Name] [NVARCHAR](50) NULL,
    [Coach of Coach First Name] [NVARCHAR](50) NULL,
    [Coach of Coach Recognition Last] [NVARCHAR](100) NULL,
    [Coach of Coach Recognition First] [NVARCHAR](100) NULL,
    [Coach of Coach Email] [NVARCHAR](50) NULL,
    [Order_No] [NVARCHAR](200) NULL,
    [Order_Date] [DATETIME] NOT NULL,
    [Order_Type] [NVARCHAR](255) NULL,
    [BusinessVolumeTotal] [MONEY] NOT NULL,
    [CumPQV] [MONEY] NULL,
    [Total of New Clients meeting criteria] [INT] NULL,
    [Reltio_Id] [NVARCHAR](256) NULL
);

IF OBJECT_ID('Tempdb..#ReltioGIN') IS NOT NULL
    DROP TABLE #ReltioGIN;
SELECT CustomerId,
       B.Reltio_Id,
       Medifast_customer_id
INTO #ReltioGIN
FROM [MDM_Reltio].[MDM_Reltio].[dbo].[RELTIO_GROUP_MDM_CUSTOMERS_ALL] B
WHERE B.ReltioStatus = 'Active'
      AND B.Sources = 'Exigo';


CREATE NONCLUSTERED INDEX ix_tempclient
ON #ReltioGIN (Medifast_customer_id);

DECLARE @FistDayWk TABLE
(
    DayRun DATE PRIMARY KEY
);

INSERT @FistDayWk
SELECT DISTINCT
       FirstDateOfWeek DayRun
FROM [BI_Reporting].[dbo].[calendar]
WHERE CAST(CalendarDate AS DATE)
BETWEEN '10/28/2019' AND CAST(GETDATE() AS DATE);

-- Step 2: Loop through the rows:
SET NOCOUNT ON;

DECLARE @CurrentMember DATE;
SELECT @CurrentMember = MIN(DayRun)
FROM @FistDayWk;

WHILE @CurrentMember IS NOT NULL
BEGIN
    --PRINT @CurrentMember;
    DECLARE @EndDate DATETIME = DATEADD(WEEK, DATEDIFF(WEEK, 0, @CurrentMember), 0);
    SET @EndDate = DATEADD(ss, -1, DATEADD(hh, 3, @EndDate)); ---Current week Monday 2:59AM Eastern 


    DECLARE @StartDate DATETIME = DATEADD(WEEK, DATEDIFF(WEEK, 0, @CurrentMember) - 1, 0);
    SET @StartDate = DATEADD(hh, 3, @StartDate); ---Previous week Monday 2:59AM Eastern 

    DECLARE @weeknum INT =
            (
                SELECT PeriodID
                FROM [EXIGO].[ExigoSyncSQL_PRD].[dbo].[Periods]
                WHERE CAST([StartDate] AS DATE) = CAST(@StartDate AS DATE)
            );


    ---First we pull qualifying customers 
    IF OBJECT_ID('Tempdb..#QualCust') IS NOT NULL
        DROP TABLE #QualCust;
    SELECT DISTINCT
           o.CustomerID,
           CONCAT(C.FirstName, ' ', C.LastName) FullName,
           C.Field1,
           C.Field2
    INTO #QualCust
    FROM [EXIGO].[ExigoSyncSQL_PRD].dbo.Orders o
        JOIN Exigo_ETL.dbo.Customers C
            ON C.CustomerID = o.CustomerID
    WHERE C.CustomerTypeID = 2 ----Client
          AND
          (
              CAST(OrderDate AS DATE) NOT
          BETWEEN '8/11/2019' AND '11/11/2019' ---no order in this time window
              OR C.[Date1]
          BETWEEN @StartDate AND @EndDate -----New client last week
          );

    ---Then pull the orders by this clients 
    IF OBJECT_ID('Tempdb..#Orders') IS NOT NULL
        DROP TABLE #Orders;
    SELECT C.[Field1],
           C.[CustomerID],
           FullName,
           O.Other13 [Order_No],
           O.Total [Order_Total],
           C.Field2,
           COH.Order_Type,
           O.BusinessVolumeTotal,
           O.OrderDate [Order_Date]
    INTO #Orders
    FROM [EXIGO].[ExigoSyncSQL_PRD].dbo.Orders O
        JOIN [EXIGO].[ExigoSyncSQL_PRD].CUSTOM.[Order_Header] COH
            ON COH.[Order_No] = O.Other13
        JOIN #QualCust C
            ON C.[CustomerID] = O.[CustomerID]
    WHERE O.[OrderDate]
          BETWEEN @StartDate AND @EndDate
          AND [Channel] = 'Optavia';

    ---Cumulative PQV 
    IF OBJECT_ID('Tempdb..#FinalQual') IS NOT NULL
        DROP TABLE #FinalQual;

    SELECT CustomerID,
           SUM(BusinessVolumeTotal) CumPQV
    INTO #FinalQual
    FROM #Orders
    GROUP BY CustomerID
    HAVING SUM(BusinessVolumeTotal) >= 345;

    IF OBJECT_ID('Tempdb..#NewClients') IS NOT NULL
        DROP TABLE #NewClients;
    SELECT O.Field2 [OPTAVIA Coach ID],
           COUNT(DISTINCT fq.CustomerID) [Total of New Clients meeting criteria]
    INTO #NewClients
    FROM #Orders O
        JOIN #FinalQual fq
            ON fq.CustomerID = O.CustomerID
    GROUP BY O.Field2;



    INSERT INTO ##Incentive
    (
        [Week Number],
        [OPTAVIA Coach ID],
        [Exigo Coach ID],
        [Coach Recognition Last],
        [Coach Recognition First],
        [Client Exigo ID],
        [Client Optavia ID],
        [FullName],
        [Coach Email],
        [Exigo Coach of Coach ID],
        [Optavia Coach of Coach ID],
        [Coach of Coach Last Name],
        [Coach of Coach First Name],
        [Coach of Coach Recognition Last],
        [Coach of Coach Recognition First],
        [Coach of Coach Email],
        [Order_No],
        [Order_Date],
        [Order_Type],
        [BusinessVolumeTotal],
        [CumPQV],
        [Total of New Clients meeting criteria],
        [Reltio_Id]
    )
    SELECT DISTINCT
           @weeknum [Week Number],
           o.Field2 [OPTAVIA Coach ID],
           C2.CustomerID [Exigo Coach ID],
           C2.Field13 AS [Coach Recognition Last],
           C2.Field12 AS [Coach Recognition First],
           o.CustomerID [Client Exigo ID],
           o.Field1 [Client Optavia ID],
           o.FullName [Client Full Name],
           C2.Email [Coach Email],
           C3.CustomerID AS [Exigo Coach of Coach ID],
           C3.Field1 AS [Optavia Coach of Coach ID],
           C3.LastName AS [Coach of Coach Last Name],
           C3.FirstName AS [Coach of Coach First Name],
           C3.Field13 AS [Coach of Coach Recognition Last],
           C3.Field12 AS [Coach of Coach Recognition First],
           C3.Email AS [Coach of Coach Email],
           o.Order_No,
           o.Order_Date,
           o.Order_Type,
           o.BusinessVolumeTotal,
           fv.CumPQV,
           NC.[Total of New Clients meeting criteria],
           B.Reltio_Id
    FROM #FinalQual fv
        JOIN #Orders o
            ON o.CustomerID = fv.CustomerID
        LEFT JOIN [EXIGO].[ExigoSyncSQL_PRD].dbo.Customers C2
            ON o.Field2 = C2.Field1
        LEFT JOIN [EXIGO].[ExigoSyncSQL_PRD].dbo.Customers C3
            ON C2.Field2 = C3.Field1
        LEFT JOIN #ReltioGIN B
            ON o.Field1 = B.Medifast_customer_id
        LEFT JOIN #NewClients NC
            ON NC.[OPTAVIA Coach ID] = o.Field2
    WHERE LEN(o.Field2) > 0 ---not pulling coaches who doesn't have coaches  
          AND o.Field2 NOT IN ( '9027001', '5', '777', '111', '20000224217', '20000231249', '763987990' ) ---Exigo Test Accounts
          AND C2.Email NOT IN ( 'MATTTEST3@MAILINATOR.COM', 'yqiyucqm@getairmail.com', 'jessestamm+1@gmail.com',
                                'dalvino@CHOOSEMEDIFAST.COM', 'HW1107-Coach3@email.com',
                                'allanjcannington+hwtest01@gmail.com', 'Test3@medifastwcc.com',
                                'matthew.allen.francis@gmail.com', 'acannington@clientcareteam.com',
                                'allan.cannington@medifastcenters.com', 'tracy@email.com', 'angilee.myers@tsfl.com',
                                'Angilee.Myers@tsfl.com', 'alitest50@mailinator.com', 'jhock@choosemedifast.com',
                                'alitest13@mailinator.com', 'Test4@medifastwcc.com',
                                'allanjcannington+052101@gmail.com', 'email3@071414.com',
                                'acannington@medifastdiet.com', 'alitest10@mailinator.com', 'alitest6@mailinator.com',
                                'alitest12@mailinator.com', 'secondtry1234@outlook.com', 'allanjcannington@gmail.com',
                                'tested525231@aol.com', 'HW1107-Coach2@email.com', 'Test5@medifastwcc.com',
                                'Matthew.Francis@tsfl.com', 'alitest8@mailinator.com', 'drewnew618@yahoo.com',
                                'allanjcannington+052102@gmail.com', 'DMGinMD+test1@gmail.com',
                                'matttest5@mailinator.com', 'acannington@medifastprogram.com',
                                'matttest4@mailinator.com', 'intilij+01@gmail.com', 'kgover@tsfl.com',
                                'kurt.gover@tsfl.com', 'mschnell@choosemedifast.com', 'noemail@tsfl.com',
                                'judith.llaudenklos@choosemedifast.com', 'UCart_SB1@choosemedifast.com',
                                'radhika.kanaparthi@choosemedifast.com', 'test062602@allan.com',
                                'optaviahk2018@gmail.com', 'test070301@email.com', 'test062601@allan.com',
                                'test062603@allan.com', 'jpowers@clientcareteam.com'
                              ); ---Exigo Test Accounts

    DELETE @FistDayWk
    WHERE DayRun = @CurrentMember;

    SELECT @CurrentMember = MIN(DayRun)
    FROM @FistDayWk;
END;
SELECT I.*,
       [Total Cumulative PQV],
       [Grand Total of New Clients meeting criteria]
FROM ##Incentive I
    JOIN
    (
        SELECT DISTINCT
               [OPTAVIA Coach ID],
               COUNT(DISTINCT [Client Optavia ID]) [Grand Total of New Clients meeting criteria]
        FROM ##Incentive
        GROUP BY [OPTAVIA Coach ID]
    ) T
        ON T.[OPTAVIA Coach ID] = I.[OPTAVIA Coach ID]
    JOIN
    (
        SELECT a.[Client Optavia ID],
               SUM(CumPQV) [Total Cumulative PQV]
        FROM
        (
            SELECT DISTINCT
                   [Week Number],
                   [Client Optavia ID],
                   CumPQV
            FROM ##Incentive
        ) a
        GROUP BY [Client Optavia ID]
    ) W
        ON W.[Client Optavia ID] = I.[Client Optavia ID];





END;
