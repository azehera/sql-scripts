﻿

-- =============================================
-- Author:	Kalpesh Patel
-- Create date: 07/09/2013
-- Description:	Monthly report #2
--+++++++++++++++++++++++++++++++++++++++++++++++++

-- =============================================
CREATE PROCEDURE [dbo].[usp_MonthlyReportByPostingGroupCarrierService]
	-- Add the parameters for the stored procedure here
	@DistCenter varchar(50),
	@Dept varchar(150),
	@StartDate datetime,
	@EndDate datetime
	--@CarrierService varchar(300)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Working table(
		DistCenter varchar(20)
		, [SID] int
		, CostCenter varchar(50)
		, SalesSource varchar(50)
		, Carrier varchar(20)
		, [Service] varchar(20)
		, PackageCount int
		, TotalChg decimal(10,2)
		, TotalWgt decimal(10,2)
		)

	declare @StartValue varchar(10)
	declare @EndValue varchar(10)
	

	set @StartValue=cast(year(@StartDate) as varchar(4)) + '-' + right('0' + cast(month(@StartDate) as varchar(2)),2) + '-' + right('0' + cast(day(@StartDate) as varchar(2)),2)
	set @EndValue=cast(year(@EndDate) as varchar(4)) + '-' + right('0' + cast(month(@EndDate) as varchar(2)),2) + '-' + right('0' + cast(day(@EndDate) as varchar(2)),2)


	insert into @Working
	select	case sh.keydata1
					when 1 then 'MDCWMS'
					when 2 then 'TDCWMS'
				end as dc
			, sh.[sid]
			, reference3
			, reference5
			, sh.carrier
			, sh.service
			, Count(distinct shipment_no) --(select count(Pid) from packages where [sid]=sh.sid)
			, dbo.udf_getShipCostByShipment(sh.[sid])
			, Sum(p.wgt) as ship_wgt --sh.ship_wgt
	from  FLAGSHIP_WMS_ETL.dbo.shipment_header sh inner join  FLAGSHIP_WMS_ETL.dbo.packages p on sh.[sid] = p.[sid] 
	where	sh.status='UPLD'
				and (sh.ship_date>=@StartValue and sh.ship_date<=@EndValue)
					and reference3 in (select * from dbo.ufn_SPLIT(@Dept,','))
	Group By sh.keydata1, sh.[sid],  reference3,  reference5, sh.carrier, sh.service, dbo.udf_getShipCostByShipment(sh.[sid]) 		

	select	DistCenter
			, CostCenter
			, SalesSource
			, Carrier
			, [Service]
			, sum(PackageCount) as TotalPackageCount
			, sum(TotalChg) as TotalCost
			, sum(TotalWgt) as TotalPackageWeight
			, sum(TotalWgt) / sum(PackageCount) as AvgPackageWeight
			, sum(TotalChg) / sum(PackageCount) as AvgPackageCost
			from @Working 
    group by DistCenter, CostCenter,SalesSource,Carrier, [Service]
	order by DistCenter, CostCenter,SalesSource,Carrier, [Service]

END










