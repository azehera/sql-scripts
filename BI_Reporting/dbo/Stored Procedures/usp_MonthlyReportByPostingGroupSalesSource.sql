﻿




-- =============================================
-- Author:		Kalpesh Patel
-- Create date: 07/09/2013
-- Description:	<Description,,>
-- Modified: Per CAB #91 - 11/05/2010 (D. Smith)
-- =============================================
CREATE PROCEDURE [dbo].[usp_MonthlyReportByPostingGroupSalesSource] 
	-- Add the parameters for the stored procedure here
	@DistCenter varchar(30),
	@Dept varchar(150),
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN

	SET NOCOUNT ON;

 

	declare @StartValue varchar(10)
	declare @EndValue varchar(10)
	declare @Working table(
		DistCenter varchar(30)
		, [SID] int
		,OrderCount Varchar(50)
		, CostCenter varchar(50)
		, SalesSource varchar(50)
		, PackageCount int
		, TotalChg decimal(10,2)
		, TotalWgt decimal(10,2)
		)

	set @StartValue=cast(year(@StartDate) as varchar(4)) + '-' + right('0' + cast(month(@StartDate) as varchar(2)),2) + '-' + right('0' + cast(day(@StartDate) as varchar(2)),2)
	set @EndValue=cast(year(@EndDate) as varchar(4)) + '-' + right('0' + cast(month(@EndDate) as varchar(2)),2) + '-' + right('0' + cast(day(@EndDate) as varchar(2)),2)

	insert into @Working
	select		case sh.dc
					when 1 then 'MDCWMS'
					when 2 then 'TDCWMS'
				end as dc
				, sh.sid
				,[ui_sales_order_no] as OrderCount
				, reference3 as dept
				, reference5 as source
			    , Count( distinct shipment_no) 
				, dbo.udf_getShipCostByShipment(sh.sid)
				, Sum(p.wgt) as ship_wgt 
FROM FLAGSHIP_WMS_ETL.dbo.shipment_header sh 
	left join FLAGSHIP_WMS_ETL.dbo.packages p ON p.sid=sh.sid --joined packages tbl to get package weight
WHERE     (sh.ship_date >= @StartValue and sh.ship_date<=@EndValue) 
			and sh.[status]='UPLD'
	
					and reference3 in (select * from dbo.ufn_SPLIT(@Dept,','))
					
		Group By sh.dc,sh.sid, reference3, reference5, dbo.udf_getShipCostByShipment(sh.sid),[ui_sales_order_no]
	

	select	DistCenter
			, CostCenter
			, SalesSource
			, count(distinct OrderCount) as TotalOrderCount
			, sum(PackageCount) as TotalPackageCount
			, sum(TotalChg) as TotalCost
			, avg(TotalChg) as AvgOrderCost
			, sum(TotalWgt) as TotalPackageWeight
			, sum(totalWgt)/count(distinct OrderCount) as AverageOrderWeight
			, sum(TotalWgt) / sum(PackageCount) as AvgPackageWeight
			, sum(TotalChg) / sum(PackageCount) as AvgPackageCost

	from @Working 

	group by DistCenter, CostCenter,SalesSource
	order by DistCenter, CostCenter,SalesSource




--	print @StartDate
--	print @EndDate
--	print @StartValue
--	print @EndValue


--		SELECT     f.keyValue as dept
--					, f2.KeyValue as source
--					, count(distinct Shipment_header.sid) as OrderCount
--					, (select count(pid) from packages p inner join flexdata fa on p.[sid]=fa.[sid] inner join flexdata fb on p.[sid]=fb.[sid] inner join shipment_header sh on sh.[sid]=p.[sid] where sh.[status] <> 'VOID' and fb.keyvalue=f2.keyvalue and fa.keyvalue=f.keyvalue and (p.ship_date>=@StartValue and p.ship_date<@EndValue) ) as PackageCount
--					, sum(shipment_header.total_chg) as totalCharge
--					, avg(shipment_header.total_chg) as avgCharge
--					, sum(shipment_header.ship_wgt) as TotalWeight
--					, avg(shipment_header.ship_wgt) as avgOrderWeight
--					, (select avg(p.wgt) from packages p inner join flexdata fa on p.[sid]=fa.[sid] inner join flexdata fb on p.[sid]=fb.[sid] inner join shipment_header sh on sh.[sid]=p.[sid] where sh.[status] <> 'VOID' and fb.keyvalue=f2.keyvalue and fa.keyvalue=f.keyvalue and (p.ship_date>=@StartValue and p.ship_date<@EndValue) ) as avgPackageWeight
--
--		 FROM       flexdata f right JOIN
--						shipment_header ON f.sid = shipment_header.sid
--						inner join flexdata f2 on f2.sid=shipment_header.sid 
--	--					inner join packages p on p.sid=shipment_header.sid
--							
--		WHERE     (f.keyName = 'FLEX3') 
--					and (shipment_header.ship_date >= @StartValue and shipment_header.ship_date<@EndValue) 
--					and f2.keyname='FLEX7' 
--					and shipment_header.[status] <> 'VOID'
--					and f.keyvalue in (select * from dbo.ufn_SPLIT(@Dept,','))
--
--		group by f.keyvalue, f2.keyvalue
--		order by f.keyvalue, f2.keyvalue

END



















