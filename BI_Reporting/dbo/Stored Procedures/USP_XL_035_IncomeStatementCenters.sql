﻿
/*
--==================================================================================
Author:         Luc Emond
Create date: 04/15/2013
-----------------------------[USP_XL_035_IncomeStatementCenters]-------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------
BI_Reporting           dbo.calendar                             Select      
BI_SSAS_Cubes          dbo.DimGlobalDimension1Code              Select
BI_Reporting           dbo.XL_035_IncomeStatementCenters        Insert
[BI_SSAS_Cubes]        [dbo].[DimGLCategory]                    Select
[BI_SSAS_Cubes]        [dbo].[FactGL_cube]                      Select
[BI_SSAS_Cubes]        [dbo].[DimLocation]                      Select
[BI_SSAS_Cubes]        [dbo].[DimRegion]                        Select

=======================================================================================
REVISION LOG
Date           Name                          Change
---------------------------------------------------------------------------------------

=======================================================================================
NOTES:
---------------------------------------------------------------------------------------
2013-10-09   Luc Emond               Added AccountsCategory = 2 to provide P&L Only
=======================================================================================		
*/	

CREATE procedure [dbo].[USP_XL_035_IncomeStatementCenters]
as

Declare @MonthID int

Set @MonthID=(Select CalendarMonthID
From BI_Reporting.dbo.calendar
Where CalendarDate=convert(varchar(10),GETDATE(),121))


IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select CalendarYear,CalendarMonth,CalendarQuarter,CalendarMonthID,CalendarMonthName
Into #A 
From BI_Reporting.dbo.calendar
Group by CalendarYear,CalendarMonth,CalendarQuarter,CalendarMonthID,CalendarMonthName 
Order by CalendarMonthID

IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
Select [Global Dimension 1 Code] 
Into #B 
From BI_SSAS_Cubes.dbo.DimGlobalDimension1Code
where [Global Dimension 1 Code] between '000' and '999'
or [Global Dimension 1 Code] like 'CC%' or [Global Dimension 1 Code] in ('FRANCHIS','AUTO',' ','KERR DRUGS','MISC',
'TSFL','WEB','HRV')

Truncate table BI_Reporting.dbo.XL_035_IncomeStatementCenters
Insert into BI_Reporting.dbo.XL_035_IncomeStatementCenters
Select 
LocationCode
,Region
,CalendarYear
,CalendarMonth
,CalendarQuarter
,CalendarMonthName
,CalendarMonthID
,[Total Income]
,[Total Cost of Goods Sold]
,[Total Direct Costs]
,isnull([Total Income],0)+isnull([Total Cost of Goods Sold],0)+isnull([Total Direct Costs],0) as GrossProfit
,[Total Operating Costs]
,[Total Personnel Expenses]
,[Total Sales & Marketing Exp]
,[Total Communication]
,[Total Office Expenses]
,[Total Other Expenses]
,isnull([Total Income],0)+isnull([Total Cost of Goods Sold],0)+isnull([Total Direct Costs],0)+isnull([Total Operating Costs],0)+isnull([Total Other Income],0)
+isnull([Total Personnel Expenses],0)+isnull([Total Sales & Marketing Exp],0)+isnull([Total Communication],0)+isnull([Total Office Expenses],0)
+isnull([Total Other Expenses],0) as [Total Operating Income]
from
(SELECT 
a.[RollUpName] 
,LocationCode
,Region
,CalendarYear
,CalendarMonth
,CalendarQuarter
,CalendarMonthName
,b.CalendarMonthID
,[NetTotal] as [NetTotal]
FROM [BI_SSAS_Cubes].[dbo].[DimGLCategory] A
JOIN [BI_SSAS_Cubes].[dbo].[FactGL_cube] B
ON A.[G_L Account No_] = B.[G_L Account No_]
join [BI_SSAS_Cubes].[dbo].[DimLocation] c
on  B.[Global Dimension 1 Code] = c.LocationID
join [BI_SSAS_Cubes].[dbo].[DimRegion] d
on  B.[Global Dimension 1 Code] = d.RegionId
join #A e
on b.CalendarMonthID=e.CalendarMonthID
WHERE [Global Dimension 2 Code] ='MWCC'
and [Global Dimension 1 Code]between '001' and '999'
and [Source Code]<>'CLSINCOME'
and AccountsCategory=2-----Added Luc Emond October 09 2013----
AND b.CalendarMonthID<@MonthID
) as SourceTable
Pivot(Sum([NetTotal]) for RollUpName in(
[Total Communication],
[Total Cost of Goods Sold],
[Total Direct Costs],
[Total Income],
[Total Office Expenses],
[Total Operating Costs],
[Total Other Expenses],
[Total Other Income],
[Total Personnel Expenses],
[Total Sales & Marketing Exp],
[Total Vehicle Expenses],
[TSFL Commission])) as Sales

Insert into BI_Reporting.dbo.XL_035_IncomeStatementCenters
Select 
LocationCode
,Region
,CalendarYear
,CalendarMonth
,CalendarQuarter
,CalendarMonthName
,CalendarMonthID
,[Total Income]
,[Total Cost of Goods Sold]
,[Total Direct Costs]
,isnull([Total Income],0)+isnull([Total Cost of Goods Sold],0)+isnull([Total Direct Costs],0) as GrossProfit
,[Total Operating Costs]
,[Total Personnel Expenses]
,[Total Sales & Marketing Exp]
,[Total Communication]
,[Total Office Expenses]
,[Total Other Expenses]
,isnull([Total Income],0)+isnull([Total Cost of Goods Sold],0)+isnull([Total Direct Costs],0)+isnull([Total Operating Costs],0)+isnull([Total Other Income],0)
+isnull([Total Personnel Expenses],0)+isnull([Total Sales & Marketing Exp],0)+isnull([Total Communication],0)+isnull([Total Office Expenses],0)
+isnull([Total Other Expenses],0) as [Total Operating Income]
from
(SELECT 
a.[RollUpName] 
, case when [Global Dimension 2 Code] ='Franchise' then 'Franchise' 
      else 
      'Corporate' 
      end 
      as LocationCode
, case when [Global Dimension 2 Code] ='Franchise' then 'Franchise' else 'Corporate' end as Region
,CalendarYear
,CalendarMonth
,CalendarQuarter
,CalendarMonthName
,b.CalendarMonthID
,[NetTotal] as [NetTotal]
FROM [BI_SSAS_Cubes].[dbo].[DimGLCategory] A
JOIN [BI_SSAS_Cubes].[dbo].[FactGL_cube] B
ON A.[G_L Account No_] = B.[G_L Account No_]
join #A e
on b.CalendarMonthID=e.CalendarMonthID
WHERE [Global Dimension 1 Code] like 'CC%'
and EntryNO=4
and [Source Code]<>'CLSINCOME'
and AccountsCategory=2-----Added Luc Emond October 09 2013----
AND b.CalendarMonthID<@MonthID
) as SourceTable
Pivot(Sum([NetTotal]) for RollUpName in(
[Total Communication],
[Total Cost of Goods Sold],
[Total Direct Costs],
[Total Income],
[Total Office Expenses],
[Total Operating Costs],
[Total Other Expenses],
[Total Other Income],
[Total Personnel Expenses],
[Total Sales & Marketing Exp],
[Total Vehicle Expenses],
[TSFL Commission])) as Sales



Insert into BI_Reporting.dbo.XL_035_IncomeStatementCenters
Select 
LocationCode
,Region
,CalendarYear
,CalendarMonth
,CalendarQuarter
,CalendarMonthName
,CalendarMonthID
,[Total Income]
,[Total Cost of Goods Sold]
,[Total Direct Costs]
,isnull([Total Income],0)+isnull([Total Cost of Goods Sold],0)+isnull([Total Direct Costs],0) as GrossProfit
,[Total Operating Costs]
,[Total Personnel Expenses]
,[Total Sales & Marketing Exp]
,[Total Communication]
,[Total Office Expenses]
,[Total Other Expenses]
,isnull([Total Income],0)+isnull([Total Cost of Goods Sold],0)+isnull([Total Direct Costs],0)+isnull([Total Operating Costs],0)+isnull([Total Other Income],0)
+isnull([Total Personnel Expenses],0)+isnull([Total Sales & Marketing Exp],0)+isnull([Total Communication],0)+isnull([Total Office Expenses],0)
+isnull([Total Other Expenses],0)as [Total Operating Income]
from
(SELECT 
a.[RollUpName] 
, 'Franchise' as LocationCode
, 'Franchise'  as Region
,CalendarYear
,CalendarMonth
,CalendarQuarter
,CalendarMonthName
,b.CalendarMonthID
,[NetTotal] as [NetTotal]
FROM [BI_SSAS_Cubes].[dbo].[DimGLCategory] A
JOIN [BI_SSAS_Cubes].[dbo].[FactGL_cube] B
ON A.[G_L Account No_] = B.[G_L Account No_]
join #A e
on b.CalendarMonthID=e.CalendarMonthID
WHERE [Global Dimension 2 Code] ='Franchise'
and [Source Code]<>'CLSINCOME'
and AccountsCategory=2-----Added Luc Emond October 09 2013----
AND b.CalendarMonthID<@MonthID
) as SourceTable
Pivot(Sum([NetTotal]) for RollUpName in(
[Total Communication],
[Total Cost of Goods Sold],
[Total Direct Costs],
[Total Income],
[Total Office Expenses],
[Total Operating Costs],
[Total Other Expenses],
[Total Other Income],
[Total Personnel Expenses],
[Total Sales & Marketing Exp],
[Total Vehicle Expenses],
[TSFL Commission])) as Sales

Insert into BI_Reporting.dbo.XL_035_IncomeStatementCenters
Select 
LocationCode
,Region
,CalendarYear
,CalendarMonth
,CalendarQuarter
,CalendarMonthName
,CalendarMonthID
,[Total Income]
,[Total Cost of Goods Sold]
,[Total Direct Costs]
,isnull([Total Income],0)+isnull([Total Cost of Goods Sold],0)+isnull([Total Direct Costs],0) as GrossProfit
,[Total Operating Costs]
,[Total Personnel Expenses]
,[Total Sales & Marketing Exp]
,[Total Communication]
,[Total Office Expenses]
,[Total Other Expenses]
,isnull([Total Income],0)+isnull([Total Cost of Goods Sold],0)+isnull([Total Direct Costs],0)+isnull([Total Operating Costs],0)+isnull([Total Other Income],0)
+isnull([Total Personnel Expenses],0)+isnull([Total Sales & Marketing Exp],0)+isnull([Total Communication],0)+isnull([Total Office Expenses],0)
+isnull([Total Other Expenses],0) as [Total Operating Income]
from
(SELECT 
a.[RollUpName] 
,[Global Dimension 1 Code]  as LocationCode
,Case when [Global Dimension 1 Code]='AUS' then 'TX - Austin'
      when  [Global Dimension 1 Code]='BAL' then 'MD - Northern'
      when  [Global Dimension 1 Code]='DAL' then 'TX - Dallas/Ft. Worth'
      when  [Global Dimension 1 Code]='DC' then 'MD - Southern'
      when  [Global Dimension 1 Code]='HOU' then 'TX - Houston'
      when  [Global Dimension 1 Code]='JACK' then 'FL - Jacksonville'
      when  [Global Dimension 1 Code]='ORL' then 'FL - Orlando'
      when  [Global Dimension 1 Code]='PHIL' then 'PA - Philadelphia'
      when  [Global Dimension 1 Code]='RALEIGH' then 'NC - Raleigh'
      when  [Global Dimension 1 Code]='RICH' then 'VA - South'
      when  [Global Dimension 1 Code]='SAN' then 'TX - San Antonio'
      when  [Global Dimension 1 Code]='SFL' then 'FL - Miami'
      when  [Global Dimension 1 Code]='TAMP' then 'FL - Tampa'
      else 'Other'end  AS Region
,CalendarYear
,CalendarMonth
,CalendarQuarter
,CalendarMonthName
,b.CalendarMonthID
,[NetTotal] as [NetTotal]
FROM [BI_SSAS_Cubes].[dbo].[DimGLCategory] A
JOIN [BI_SSAS_Cubes].[dbo].[FactGL_cube] B
ON A.[G_L Account No_] = B.[G_L Account No_]
join #A e
on b.CalendarMonthID=e.CalendarMonthID
WHERE 
[Global Dimension 1 Code] not in (Select [Global Dimension 1 Code] from #B)
and [Source Code]<>'CLSINCOME'
and AccountsCategory=2-----Added Luc Emond October 09 2013----
AND b.CalendarMonthID<@MonthID
) as SourceTable
Pivot(Sum([NetTotal]) for RollUpName in(
[Total Communication],
[Total Cost of Goods Sold],
[Total Direct Costs],
[Total Income],
[Total Office Expenses],
[Total Operating Costs],
[Total Other Expenses],
[Total Other Income],
[Total Personnel Expenses],
[Total Sales & Marketing Exp],
[Total Vehicle Expenses],
[TSFL Commission])) as Sales


