﻿Create PROCEDURE [dbo].[usp_productView]
AS

SELECT
[EntryNo],[RunDate],[Yesterday],[StartDateYear],[StartDateMonth],[YesterdayPriorYr],
[StartDateYearPriorYr],[StartDateMonthPriorYr],[CPG],[ItemCategoryCode],[InvSalesDay],[InvCreditsDay],
[InvUnitsDay],[InvOrdersDay],[InvExtCostDay],[InvSalesMTD],[InvSalesPriorYrMTD],
[InvSalesMoProj],[InvUnitsMTD],[InvOrdersMTD],[InvExtCostMTD],[InvSalesYTD],
[InvSalesPriorYrYTD],[InvSalesProjYE],[InvUnitsYTD],[InvOrdersYTD],[InvExtCostYTD]
  FROM [NAV_ETL].[dbo].[Snapshot$NAV_Product] PV
  WHERE (PV.[RunDate] = CAST(FLOOR(CAST(GETDATE() AS float)) AS DATETIME)) AND
    ((PV.InvSalesDay <> 0) OR (PV.InvSalesMTD <> 0) OR (PV.InvSalesYTD <> 0) OR
     (PV.InvUnitsDay <> 0) OR (PV.InvUnitsMTD <> 0) OR (PV.InvUnitsYTD <> 0) OR
     (PV.InvSalesPriorYrMTD <> 0) OR (PV.InvSalesPriorYrYTD <> 0))  and [CPG] <> 'MWCC'
ORDER BY ItemCategoryCode, SortOrder DESC, CPG
