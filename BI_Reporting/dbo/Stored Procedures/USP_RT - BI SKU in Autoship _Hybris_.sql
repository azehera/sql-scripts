﻿
CREATE PROCEDURE [dbo].[USP_RT - BI SKU in Autoship (Hybris)]
AS

IF OBJECT_ID('Tempdb..#temp') IS NOT NULL DROP TABLE #temp;
SELECT DISTINCT
       U.p_uid AS [Email Address],
       U.p_cartcustnumber AS [Customer Number],
       U.p_name AS [Customer Name],
       TL.sku,
       TH.channel,
       CAST(TH.nextautoshiporderdate AS DATE) AS [Next Order Date],
	   C.Field2 --AS 'Coach Number'
INTO #temp
FROM [HYBRIS_ETL].[dbo].[vw_Template_header_prd] TH (nolock)
    INNER JOIN [HYBRIS_ETL].[dbo].[vw_template_line] TL (nolock)
        ON TH.ordernumber = TL.ordernumber
    INNER JOIN [Hybris_ETL].[mdf_prd_database].[users] U (nolock)
        ON U.PK = TH.user_id
	LEFT JOIN EXIGO.ExigoSyncSQL_PRD.dbo.Customers C ON C.Field1 = u.p_cartcustnumber AND C.Field2 !=''
WHERE CAST(TH.nextautoshiporderdate AS DATE)
      BETWEEN CAST(GETDATE()  AS DATE) AND CAST(GETDATE() + 45 AS DATE)
      AND TH.templatestatus = 1;



SELECT T.*, C.FirstName +' ' +C.LastName AS 'Coach Name', CS.Email AS 'Coach Email'

FROM #temp T
LEFT JOIN EXIGO.ExigoSyncSQL_PRD.dbo.Customers C ON T.Field2 = C.Field1
LEFT JOIN [EXIGO].[ExigoSyncSQL_PRD].[dbo].[CustomerSites] CS ON CS.CustomerID = C.CustomerID