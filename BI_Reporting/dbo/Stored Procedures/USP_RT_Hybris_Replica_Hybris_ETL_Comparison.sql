﻿
/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 11/1/2019
------------------------------------------------
Future Autoship orders
--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------
Hybris_ETL            mdf_prd_database.cronjobs            Select
Hybris_ETL            mdf_prd_database.orders              Select
Hybris_ETL            mdf_prd_database.users               Select
Hybris_ETL            mdf_prd_database.pgrels              Select
Hybris_ETL            mdf_prd_database.usergroups          Select                 
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_RT_Hybris_Replica_Hybris_ETL_Comparison]  
AS

BEGIN 

SELECT 'cronjobs' AS 'Hybris_ETL_Target_Table_Name',
       'PK' AS 'Hybris_ETL_Target_Pk_Name',
       CAST(PK AS NVARCHAR(255)) AS Hybris_ETL_Target_PK,
       createdTS Hybris_ETL_Target_createdTS,
       modifiedTS Hybris_ETL_Target_modifiedTS
FROM [Hybris_ETL].[mdf_prd_database].[cronjobs]
UNION ALL
SELECT 'Order' AS 'Hybris_ETL_Target_Table_Name',
       'p_code' AS 'Hybris_ETL_Target_Pk_Name',
       p_code AS Hybris_ETL_Target_PK,
       createdTS Hybris_ETL_Target_createdTS,
       modifiedTS Hybris_ETL_Target_modifiedTS
FROM [Hybris_ETL].[mdf_prd_database].[orders]
UNION ALL
SELECT 'User' AS 'Hybris_ETL_Target_Table_Name',
       'p_cartcustnumber' AS 'Hybris_ETL_Target_Pk_Name',
        p_cartcustnumber  AS Hybris_ETL_Target_PK,
       createdTS Hybris_ETL_Target_createdTS,
       modifiedTS Hybris_ETL_Target_modifiedTS
FROM [Hybris_ETL].[mdf_prd_database].[users]
UNION ALL
SELECT 'pgrels' AS 'Hybris_ETL_Target_Table_Name',
       'PK' AS 'Hybris_ETL_Target_Pk_Name',
       CAST(PK AS NVARCHAR(255)) AS Hybris_ETL_Target_PK,
       createdTS Hybris_ETL_Target_createdTS,
       modifiedTS Hybris_ETL_Target_modifiedTS
FROM [Hybris_ETL].[mdf_prd_database].[pgrels]
UNION ALL
SELECT 'usergroups' AS 'Hybris_ETL_Target_Name',
       'PK' AS 'Hybris_ETL_Target_Pk_Name',
       CAST(PK AS NVARCHAR(255)) AS Hybris_ETL_Target_PK,
       createdTS Hybris_ETL_Target_createdTS,
       modifiedTS Hybris_ETL_Target_modifiedTS
FROM [Hybris_ETL].[mdf_prd_database].[usergroups];



end