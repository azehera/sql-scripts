﻿


--/*
--===============================================================================
--Author:  Divya Reddy
--Create date: 08/03/2021
---------------------------USP_XL_024_QuarterlySalesTracker_CPG_Sunday-----------------------------
-----------------------------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--[BI_SSAS_Cubes]         dbo.DimJason Pharm$Customer Posting Group    Select 
--[BI_SSAS_Cubes]         dbo.BusinessCalendar                         Select
--[BI_SSAS_Cubes]         dbo.FactSales                                Select
--[BI_Reporting]         dbo.Budget                                   Select 
--BI_Reporting            dbo.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup  Insert
--BI_Reporting            dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup      Insert
--BI_Reporting            dbo.dbo.XL_024_QuarterlySalesTracker_LY               Insert
--BI_Reporting            dbo.dbo.XL_024_QuarterlySalesTracker_TY               Insert
--[BI_SSAS_Cubes]          dbo.BusinessCalendar_Five                            Select------Added by Daniel Dagnachew bc. data in [BI_SSAS_Cubes].dbo.BusinessCalendar is expired.
                                                                       -- Query for the table definition is saved in BI repository.(BI Repository\Daniel\Business Calendar Query)   
--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------
--
--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--===============================================================================
--*/
Create Procedure [dbo].[USP_XL_024_QuarterlySalesTracker_CPG_Sunday]
 as 
	
Declare @LastYear int
Declare @ThisYear int

Set @LastYear =(Select CalendarYearQuarter from [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] WITH (NOLOCK) where RealDate=DateAdd(yy, -1, Convert (Varchar(10),GETDATE()-1,121))) 
Set @ThisYear = (Select CalendarYearQuarter  from [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five]  WITH (NOLOCK) where RealDate= DateAdd(yy, 0, Convert (Varchar(10),GETDATE()-1,121)))
             
    

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
IF object_id('Tempdb..#Z') Is Not Null DROP TABLE #Z
IF object_id('Tempdb..#CustPost') Is Not Null DROP TABLE #CustPost
	
Select
      [Customer Posting Group]
	  ,[Posting Date]
	  ,CalendarYearQuarter
	  ,CalendarYearMonth
into #CustPost 
from  BI_SSAS_Cubes.dbo.[DimJason Pharm$Customer Posting Group] a WITH (NOLOCK)
Cross join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] b WITH (NOLOCK)
where CalendarYearQuarter in(@LastYear,@ThisYear)

Select 
      [CustomerPostingGroup]
	  ,Sum(Amount) as Amount
	  ,a.[Posting Date]
			
into #Z 
FROM [BI_SSAS_Cubes].[dbo].[FactSales] a WITH (NOLOCK)
join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five]  b WITH (NOLOCK)
on a.[Posting Date]=b.[Posting Date]
where b.CalendarYearQuarter in (@LastYear,@ThisYear)
group by a.[Posting Date]
        ,[CustomerPostingGroup]
Order by a.[Posting Date]			
				
Select  
      [Customer Posting Group] as [CustomerPostingGroup]
	  ,isnull((Amount),0) as ActualRevenue
	  ,CalendarYearQuarter
	  ,CalendarYearMonth
	  ,a.[Posting Date]
into #A			
FROM  #CustPost a			
left join #Z b
on a.[Customer Posting Group]=b.[CustomerPostingGroup]
and a.[Posting Date]=b.[Posting Date]
where a.[Posting Date]<=DateAdd(dd, DateDiff(dd,0,GetDate()-1), 0)
Order by a.[Posting Date]
                  
             
  
IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
 
SELECT 
     [CalendarMonthID]
	 ,CalendarQuarter
	 ,Channel as CustomerPostingGroup
	 ,Sum(MonthlyBudget) as MonthlyBudget
into #B
FROM [BI_Reporting].dbo.Budget_XL23 WITH (NOLOCK)
where [CalendarMonthID]>=201201
Group by [CalendarMonthID]
         ,Channel
		 ,CalendarQuarter
Order by CalendarMonthID	
            
        
IF object_id('Tempdb..#Budget1') Is Not Null DROP TABLE #Budget1
               
Select 
      CalendarQuarter
	  ,CustomerPostingGroup
	  ,Sum(MonthlyBudget) as MonthlyBudget  
into #Budget1 
from #B 
where CalendarQuarter= @LastYear
Group by CalendarQuarter
        ,CustomerPostingGroup  
      
            
IF object_id('Tempdb..#Budget2') Is Not Null DROP TABLE #Budget2                 
       
Select 
       CalendarQuarter
	  ,CustomerPostingGroup
	  ,Sum(MonthlyBudget) as MonthlyBudget 
into #Budget2 
from #B 
where CalendarQuarter = @ThisYear
Group by CalendarQuarter
        ,CustomerPostingGroup                   
                   

IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C
  
   SELECT convert (int,[BusinessDayOfFiscalQuarter]) as [BusinessDayOfFiscalYear]
          ,Convert(int,[BusinessDayOfFiscalPeriod]) as[BusinessDayOfFiscalPeriod]
          ,a.[Posting Date]
          ,ActualRevenue
          ,MonthlyBudget
          ,a.CustomerPostingGroup
          ,a.CalendarYearMonth
          ,b.CalendarQuarter as CalendarYearQuarter
          into #C from #A a
                  join #Budget1  b
						on a.CalendarYearQuarter=b.CalendarQuarter
                  and a.CustomerPostingGroup COLLATE DATABASE_DEFAULT=b.CustomerPostingGroup
                  join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] c WITH (NOLOCK)
						on a.[Posting Date]=c.[Posting Date]
						
						--Select * from #C
  
   
 IF object_id('Tempdb..#D') Is Not Null DROP TABLE #D 
      
      Select [BusinessDayOfFiscalPeriod]
      ,[BusinessDayOfFiscalYear]
             ,[Posting Date]
             ,ActualRevenue
             ,MonthlyBudget
             ,CalendarYearMonth 
             ,CalendarYearQuarter
             ,CustomerPostingGroup
             ,row_number() over (Partition by [BusinessDayOfFiscalYear] order by [Posting Date]  ) as No 
             into #D 
             from #C
             where CalendarYearQuarter=@LastYear
           
        
           

  
IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E
   
   ;with CTE as (Select [BusinessDayOfFiscalPeriod],[BusinessDayOfFiscalYear],CustomerPostingGroup
                        ,[Posting Date]  as [Posting Date] 
                        ,ActualRevenue as ActualRevenue
                        ,MonthlyBudget
                        ,CalendarYearMonth 
                        ,CalendarYearQuarter
                         from #D)
                Select [BusinessDayOfFiscalPeriod],[BusinessDayOfFiscalYear],CustomerPostingGroup
                       ,[Posting Date]
                       ,SUM(ActualRevenue) as ActualRevenue
                       ,MonthlyBudget,CalendarYearMonth 
                        ,CalendarYearQuarter  
                       into #E from CTE 
                       group by [BusinessDayOfFiscalPeriod],[Posting Date],MonthlyBudget,CalendarYearMonth, CalendarYearQuarter,CustomerPostingGroup,[BusinessDayOfFiscalYear]
                       order by [Posting Date]
      
IF object_id('Tempdb..#G') Is Not Null DROP TABLE #G
  
     Select 
           c.[BusinessDayOfFiscalPeriod],c.[BusinessDayOfFiscalYear],c.CustomerPostingGroup
           ,c.[Posting Date]
           ,Year(c.[Posting Date]) as Year
           ,c.ActualRevenue
           ,Sum(c1.ActualRevenue) as CummulativeRevenue
           ,isnull(c.MonthlyBudget,0) as MonthlyBudget
           ,c.CalendarYearQuarter
           into #G 
           from #E c
                   inner join #E c1 on c.[Posting Date]>=c1.[Posting Date]
                   and c.CustomerPostingGroup=c1.CustomerPostingGroup
                   group by c.[BusinessDayOfFiscalPeriod],c.[Posting Date],c.ActualRevenue,c.MonthlyBudget,c.CalendarYearQuarter,c.CustomerPostingGroup,c.[BusinessDayOfFiscalYear]
                   order by c.[Posting Date]
                   
                   --Select * from #G
      

IF object_id('Tempdb..#H') Is Not Null DROP TABLE #H  
      
       SELECT convert (int,[BusinessDayOfFiscalQuarter]) as [BusinessDayOfFiscalYear]
          ,Convert(int,[BusinessDayOfFiscalPeriod]) as[BusinessDayOfFiscalPeriod]
          ,a.[Posting Date]
          ,ActualRevenue
          ,MonthlyBudget
          ,a.CustomerPostingGroup
          ,a.CalendarYearMonth
          ,b.CalendarQuarter as CalendarYearQuarter
          into #H from #A a
                  join #Budget2  b
						on a.CalendarYearQuarter=b.CalendarQuarter
                  and a.CustomerPostingGroup COLLATE DATABASE_DEFAULT=b.CustomerPostingGroup
                  join [BI_SSAS_CUBES].[dbo].[BusinessCalendar_Five] c
						on a.[Posting Date]=c.[Posting Date]
    
     
IF object_id('Tempdb..#I') Is Not Null DROP TABLE #I
      
      Select [BusinessDayOfFiscalPeriod]
      ,[BusinessDayOfFiscalYear]
             ,[Posting Date]
             ,ActualRevenue
             ,MonthlyBudget
             ,CalendarYearMonth 
             ,CalendarYearQuarter
             ,CustomerPostingGroup
             ,row_number() over (Partition by [BusinessDayOfFiscalYear] order by [Posting Date]  ) as No 
             into #I 
             from #H
             where CalendarYearQuarter=@ThisYear
     
--Select * from #I
IF object_id('Tempdb..#J') Is Not Null DROP TABLE #J
   

	        ;with CTE as (Select [BusinessDayOfFiscalPeriod],[BusinessDayOfFiscalYear],CustomerPostingGroup
			            ,[Posting Date]  as [Posting Date] 
                        ,ActualRevenue as ActualRevenue
                        ,MonthlyBudget
                        ,CalendarYearMonth 
                        ,CalendarYearQuarter
                         from #I)
                Select [BusinessDayOfFiscalPeriod],[BusinessDayOfFiscalYear],CustomerPostingGroup
                       ,[Posting Date]
                       ,SUM(ActualRevenue) as ActualRevenue
                       ,MonthlyBudget,CalendarYearMonth 
                        ,CalendarYearQuarter  
                       into #J 
					   from CTE 
                       group by [BusinessDayOfFiscalPeriod],[Posting Date],MonthlyBudget,CalendarYearMonth, CalendarYearQuarter,CustomerPostingGroup,[BusinessDayOfFiscalYear]
                       order by [Posting Date]
                    
               
      
 
Insert into #G 
       Select 
           c.[BusinessDayOfFiscalPeriod],c.[BusinessDayOfFiscalYear],c.CustomerPostingGroup
           ,c.[Posting Date]
           ,Year(c.[Posting Date]) as Year
           ,c.ActualRevenue
           ,Sum(c1.ActualRevenue) as CummulativeRevenue
           ,isnull(c.MonthlyBudget,0) as MonthlyBudget
           ,c.CalendarYearQuarter
          
           from #J c
                   inner join #J c1 on c.[Posting Date]>=c1.[Posting Date]
                   and c.CustomerPostingGroup=c1.CustomerPostingGroup
                   group by c.[BusinessDayOfFiscalPeriod],c.[Posting Date],c.ActualRevenue,c.MonthlyBudget,c.CalendarYearQuarter,c.CustomerPostingGroup,c.[BusinessDayOfFiscalYear]
                   order by c.[Posting Date]
      
IF object_id('Tempdb..#K') Is Not Null DROP TABLE #K

             SELECT 
             Count(distinct [BusinessDayOfFiscalQuarter]) as [CountBusinessDay] 
             ,CalendarYearQuarter 
             into #K 
             FROM [BI_SSAS_Cubes].[dbo].[BusinessCalendar_Five] a WITH (NOLOCK)
                     where CalendarYearQuarter  in ( @LastYear, @ThisYear  )--(@LastYear,@ThisYear)
                     Group by CalendarYearQuarter 
                     
                     --select * from #K
  
IF object_id('Tempdb..#L') Is Not Null DROP TABLE #L
  
  Select 
       distinct Convert(Numeric(15,0),[BusinessDayOfFiscalQuarter])as [BusinessDayOfFiscalYear]
       ,[CountBusinessDay]
       ,RealDate 
       into #L 
       FROM [BI_SSAS_Cubes].[dbo].[BusinessCalendar_Five] a WITH (NOLOCK)
           Join #K b
           on a.CalendarYearQuarter =b.CalendarYearQuarter 
           where a.CalendarYearQuarter  in ( @LastYear, @ThisYear  )---(@LastYear,@ThisYear)
  
IF object_id('Tempdb..#M') Is Not Null DROP TABLE #M
  
  Select ([CountBusinessDay]-[BusinessDayOfFiscalYear]) as RemainingDays
          ,RealDate
          ,[CountBusinessDay]
          into #M from #L
          
         
  

--IF object_id('Tempdb..BI_Reporting.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup_Sunday ') Is Not Null DROP TABLE BI_Reporting.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup_Sunday     
truncate table  BI_Reporting.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup_Sunday 
Insert into  BI_Reporting.dbo.XL_024_QuarterlySalesTracker_LY_PostingGroup_Sunday    

      Select * 
          ,Month([Posting Date])as Month
          ,Case when RemainingDays=0 then (MonthlyBudget-CummulativeRevenue) else  (MonthlyBudget-CummulativeRevenue)/(RemainingDays) end  as AvgDayRemaining
              from #G g
              join #M l
                  on g.[Posting Date]=l.RealDate
                  
                 where CalendarYearQuarter = @LastYear  
      
 --IF object_id('Tempdb.BI_Reporting.dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup_Sunday ') Is Not Null DROP TABLE BI_Reporting.dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup_Sunday 
Truncate  table BI_Reporting.dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup_Sunday
Insert into BI_Reporting.dbo.XL_024_QuarterlySalesTracker_TY_PostingGroup_Sunday
     Select * 
          ,Month([Posting Date])as Month
          ,Case when RemainingDays=0 then (MonthlyBudget-CummulativeRevenue) else  (MonthlyBudget-CummulativeRevenue)/(RemainingDays) end  as AvgDayRemaining
              from #G g
              join #M l
                  on g.[Posting Date]=l.RealDate
                  
                 where CalendarYearQuarter = @ThisYear  
      
      
 