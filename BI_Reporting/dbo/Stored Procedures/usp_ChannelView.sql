﻿CREATE PROCEDURE [dbo].[usp_ChannelView]
AS

SELECT Ch.Yesterday
      ,Country
      ,Ch.CPG
      ,Ch.SalesSrce
      ,Ch.InvSalesDay
     ,Ch.InvUnitsDay
      ,Ch.InvOrdersDay
      ,Ch.InvCreditsDay
      ,Ch.NetShipRevDay
      ,Ch.InvSalesMTD
      ,Ch.InvSalesPriorYrMTD
      ,Ch.NetShipRevMTD
      ,Ch.InvSalesMoProj
      ,Ch.NetShipRevMoProj
      ,Ch.InvUnitsMTD
      ,Ch.InvOrdersMTD
      ,Ch.InvSalesYTD
      ,Ch.InvSalesPriorYrYTD
      ,Ch.InvSalesProjYE
      ,Ch.InvUnitsYTD
      ,Ch.InvOrdersYTD
      ,Ch.InvExtCostDay
      ,Ch.InvExtCostMTD
      ,Ch.InvExtCostYTD
      ,Fun.TSFLSponsoringDay
      ,Fun.TSFLSponsoringMTD
      ,Fun.TSFLSponsoringPriorMTD
      ,Fun.TSFLSponsoringYTD
      ,Fun.TSFLSponsoringPriorYTD
      ,Fun.TSFLClientAcquisitionDay
      ,Fun.TSFLClientAcquisitionMTD
      ,Fun.TSFLClientAcquisitionPriorMTD
      ,Fun.TSFLClientAcquisitionYTD
      ,Fun.TSFLClientAcquisitionPriorYTD
      ,Fun.ActiveHealthCoachesDay
      ,Fun.ActiveHealthCoachesMTD
      ,Fun.ActiveHealthCoachesPriorMTD
      ,Fun.ActiveHealthCoachesYTD
      ,Fun.ActiveHealthCoachesPriorYTD
  
      ,Fun.TSFLWebCoachRqstDay
      ,Fun.TSFLWebCoachRqstMTD
      ,Fun.TSFLWebCoachRqstPriorMTD
      ,Fun.TSFLWebCoachRqstYTD
      ,Fun.TSFLWebCoachRqstPriorYTD
      ,Fun.TSFLWebVisitorsDay
      ,Fun.TSFLWebVisitorsMTD
      ,Fun.TSFLWebVisitorsPriorMTD
      ,Fun.TSFLWebVisitorsYTD
      ,Fun.TSFLWebVisitorsPriorYTD
      ,Fun.TTSVisitorsDay
      ,Fun.TTSVisitorsMTD
      ,Fun.TTSVisitorsPriorMTD
      ,Fun.TTSVisitorsYTD
      ,Fun.TTSVisitorsPriorYTD
      ,Fun.TTSnewVisitsDay
      ,Fun.TTSnewVisitsMTD
      ,Fun.TTSnewVisitsPriorMTD
      ,Fun.TTSnewVisitsYTD
      ,Fun.TTSnewVisitsPriorYTD
      ,Fun.MedClientAcquisitionDay
      ,Fun.MedClientAcquisitionMTD
      ,Fun.MedClientAcquisitionPriorMTD
      ,Fun.MedClientAcquisitionYTD
      ,Fun.MedClientAcquisitionPriorYTD

      ,Fun.MedDirWebVisitorsDay
      ,Fun.MedDirWebVisitorsMTD
      ,Fun.MedDirWebVisitorsPriorMTD
      ,Fun.MedDirWebVisitorsYTD
      ,Fun.MedDirWebVisitorsPriorYTD
      ,Fun.MedDirProcNavInvcsDay
      ,Fun.MedDirProcNavInvcsMTD
      ,Fun.MedDirProcNavInvcsPriorMTD
      ,Fun.MedDirProcNavInvcsYTD
      ,Fun.MedDirProcNavInvcsPriorYTD
      ,Fun.WholesaleWebVisitorsDay
      ,Fun.WholesaleWebVisitorsMTD
      ,Fun.WholesaleWebVisitorsPriorMTD
      ,Fun.WholesaleWebVisitorsYTD
      ,Fun.WholesaleWebVisitorsPriorYTD
      --,Fun.
  FROM [NAV_ETL].[dbo].[Snapshot$NAV_Channel] Ch
  LEFT OUTER JOIN [NAV_ETL].[dbo].[Snapshot$NAV_Funnel] Fun
  ON (Fun.RunDate = CAST(FLOOR(CAST(GETDATE() AS float)) AS DATETIME))
  WHERE (Ch.RunDate = CAST(FLOOR(CAST(GETDATE() AS float)) AS DATETIME)) AND
    ((Ch.InvSalesDay <> 0) OR (Ch.InvSalesMTD <> 0) OR (Ch.InvSalesYTD <> 0) OR
     (Ch.InvUnitsDay <> 0) OR (Ch.InvUnitsMTD <> 0) OR (Ch.InvUnitsYTD <> 0) OR
     (Ch.InvSalesPriorYrMTD <> 0) OR (Ch.InvSalesPriorYrYTD <> 0))  and Ch.CPG <> 'MWCC'
ORDER BY Ch.SortOrder DESC, Ch.CPG, Ch.SortOrder2 DESC, Ch.SalesSrce
