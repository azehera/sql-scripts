﻿
/*
==================================================================================
Author:         Luc Emond
Create date: 08/14/2012
-------------------------[USP_XL_014_BI_User_Audit]------------------
Provide BI user information about which folder they have access to-----
=================================================================================    
REFERENCES
Database              Table/View/UDF                       Action            
----------------------------------------------------------------------------------
[ReportingInterface]	[dbo].[Users]                        Select   
[ReportingInterface]  [dbo].[User_report_grouping]         Select
[ReportingInterface]  [dbo].[ReportGroup_lkup]             Select
                        
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------
2014-05-29    Luc Emond        Added Deteled Status ='N' and LastName Is Not Null
2014-05-29    Kalpesh Patel    Added count of reports by folder 
2014-05-29    Luc Emond        Comment out count of reports by folder no longer needed
==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_014_BI_User_Audit] AS 
SET NOCOUNT ON;

IF object_id('tempdb..#A') is not null DROP TABLE #A 
CREATE TABLE #A(
	[user_id] [int] NULL,
	[LastName] [nvarchar](256) NULL,
	[FirstName] [nvarchar](256) NULL,
	[UserName] [nvarchar](256) NULL,
	[Email_id] [nvarchar](256) NULL,
	[Title] [nvarchar](256) NULL,
	[Department] [nvarchar](256) NULL,
	[ADGroup] [nvarchar](50) NULL,
	[Executive] [int] DEFAULT 0,
	[Business Intelligence] [int] DEFAULT 0,
	[Supply Chain] [int] DEFAULT 0,
	[Marketing Sales Partner] [int] DEFAULT 0,
	[TSFL] [int] DEFAULT 0,
	[Finance] [int] DEFAULT 0,
	[Contact Center] [int] DEFAULT 0,
	[MWCC] [int] DEFAULT 0,
	[Legal Compliance] [int] DEFAULT 0,
	[Human Resources] [int] DEFAULT 0,
	[Information Technology] [int] DEFAULT 0,
	[Management] [int] DEFAULT 0
) ON [PRIMARY]

----INSERT ALL USER ID-----
INSERT INTO #A (
 [user_id]
,[LastName]
,[FirstName]
,[UserName]
,[Email_id]
,[Title]
,[Department]
,[ADGroup] )
SELECT us.[user_id]
      ,us.[LastName]
      ,us.[FirstName]
      ,us.[UserName]
      ,us.[Email_id]
      ,us.[Title]
      ,us.[Department]
      ,us.[ADGroup]
   FROM [ReportingInterface].[dbo].[Users] us
 Where Deleted ='N'  -----2014-05-29 Luc Emond 
  Group By
  us.[user_id]
      ,us.[LastName]
      ,us.[FirstName]
      ,us.[UserName]
      ,us.[Email_id]
      ,us.[title]
      ,us.[department]
      ,us.[ADGroup] 


INSERT INTO #A
SELECT us.[user_id]
      ,us.[LastName]
      ,us.[FirstName]
      ,us.[UserName]
      ,us.[Email_id]
      ,us.[Title]
      ,us.[Department]
      ,us.[ADGroup]
      ,Case When rpt_grp_name = 'Executive' Then 1 Else 0 End as 'Executive'
      ,Case When rpt_grp_name = 'Business Intelligence' Then 1 Else 0 End as 'Business Intelligence'
      ,Case When rpt_grp_name = 'Supply Chain' Then 1 Else 0 End as 'Supply Chain'
      ,Case When rpt_grp_name = 'Marketing Sales Partner' Then 1 Else 0 End as 'Marketing Sales Partner'
      ,Case When rpt_grp_name = 'TSFL' Then 1 Else 0 End as 'TSFL'
      ,Case When rpt_grp_name = 'Finance' Then 1 Else 0 End as 'Finance'
      ,Case When rpt_grp_name = 'Contact Center' Then 1 Else 0 End as 'Contact Center'
      ,Case When rpt_grp_name = 'MWCC' Then 1 Else 0 End as 'MWCC'
      ,Case When rpt_grp_name = 'Legal Compliance' Then 1 Else 0 End as 'Legal Compliance'
      ,Case When rpt_grp_name = 'Human Resources' Then 1 Else 0 End as 'Human Resources'
      ,Case When rpt_grp_name = 'Information Technology' Then 1 Else 0 End as 'Information Technology'
      ,Case When rpt_grp_name = 'Management' Then 1 Else 0 End as 'Management'
  FROM [ReportingInterface].[dbo].[Users] us
  Left Join [ReportingInterface].[dbo].[User_report_grouping] urg
  on us.user_id = urg.user_id
  Join [ReportingInterface].[dbo].[ReportGroup_lkup] rgl
  on urg.rpt_grp_id = rgl.report_group_id 
 Where Deleted ='N' -----2014-05-29 Luc Emond    
  Group By
  us.[user_id]
      ,us.[LastName]
      ,us.[FirstName]
      ,us.[UserName]
      ,us.[Email_id]
      ,us.[title]
      ,us.[department]
      ,us.[ADGroup]
      ,rpt_grp_name 
      
--------COUNT OF REPORT BY FOLDER-------2014-05-29 Kalpesh Patel     
----INSERT INTO #A
----Select  
----[user_id]
----,[LastName]
----,[FirstName]
----, [UserName]
----, [Email_id]
----,[Title]
----, [Department]
----,[ADGroup]
----,[Executive]
----,isnull([Business Intelligence],0) as [Business Intelligence]
----,isnull([Supply Chain],0) as [Supply Chain]
----,isnull([Marketing Sales Partner],0) as [Marketing Sales Partner]
----,isnull([TSFL],0) as [TSFL]
----,isnull([Finance],0) as [Finance]
----,isnull([Contact Center],0) as [Contact Center]
----,isnull([MWCC],0) as [MWCC]
----,isnull([Legal Compliance],0) as [Legal Compliance]
----,isnull([Human Ressources],0) as [Human Ressources]
----,isnull([Information Technology],0) as [Information Technology]
----,isnull([Management],0) as [Management]
----From (      
----Select 
----'-' as [user_id]
----,'' as [LastName]
----,'A1' as [FirstName]
----,'-' as [UserName]
----,'-' as [Email_id]
----,'-' as [Title]
----,' ' as [Department]
----,'Total Reports In Folder' as [ADGroup]
----,rpt_grp_name
----,COUNT(Distinct(cf.[catalog_id])) as Count
     
----  FROM [ReportingInterface].[dbo].[User_report_grouping] urg
----  Left join [ReportingInterface].[dbo].[CatalogFolder] cf
----  on urg.rpt_grp_id = cf.report_group_id
----  Left Join [ReportingInterface].[dbo].[ReportGroup_lkup] rgl
----  on urg.rpt_grp_id = rgl.report_group_id 
----group by rpt_grp_name) as s
----Pivot
----(
----Sum(Count)
----for rpt_grp_name In (
----[Executive],
----[Business Intelligence],
----[Supply Chain],
----[Marketing Sales Partner],
----[TSFL],
----[Finance],
----[Contact Center],
----[MWCC],
----[Legal Compliance],
----[Human Ressources],
----[Information Technology],
----[Management]
----)) as kalepsh
    
     
---FINAL----    
Select 
 [user_id]
,[LastName]
,[FirstName]
,[UserName]
,[Email_id]
,[title]
,[department]
,[ADGroup]
,SUM(Executive) as Executive
,SUM(Management) as Management
,SUM([Business Intelligence]) as [Business Intelligence]
,SUM([Supply Chain]) as [Supply Chain]
,SUM([Marketing Sales Partner]) as [Marketing Sales Partner]
,SUM(TSFL) as TSFL
,SUM(Finance) as Finance
,SUM([Contact Center]) as [Contact Center]
,SUM(MWCC) as MWCC
,SUM([Legal Compliance]) as [Legal Compliance]
,SUM([Human Resources]) as [Human Resources]
,SUM([Information Technology]) as [Information Technology]
From #a     
Where LastName Is Not Null -----2014-05-29 Luc Emond 
Group By
 [user_id]
,[LastName]
,[FirstName]
,[UserName]
,[Email_id]
,[title]
,[department]
,[ADGroup]


