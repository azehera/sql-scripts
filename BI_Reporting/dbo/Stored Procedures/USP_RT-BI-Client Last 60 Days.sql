﻿





/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 09/30/2019
------------------------------------------------
https://medifast.atlassian.net/browse/ITSD-9724
--==================================================================================   
REFERENCES
Database              Table/View/UDF                                  Action            
------------------------------------------------------------------------------------
[BICUBES_MDM]         [dbo].[FactSales_MDM]                           Select 
NAV_ETL               dbo.[Jason Pharm$Sales Invoice Header]          Select
Exigo_ETL             dbo.Customers                                   Select 
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT-BI-Client Last 60 Days]
AS
SELECT DISTINCT	
       C.FirstName + ' ' + C.LastName [Client Full Name],	
       C.Email [Client Email Address],	
	   sh.[External Document No_] [Order Number],
	   m.[Posting Date] [Invoiced Date],
       c3.FirstName + ' ' + c3.LastName [Coach Full Name],	
	   c3.Email [Coach Email Address]
FROM  [BICUBES_MDM].[dbo].[FactSales_MDM] m
LEFT JOIN NAV_ETL.dbo.[Jason Pharm$Sales Invoice Header] sh
ON sh.No_ COLLATE DATABASE_DEFAULT = m.DocumentNo
    LEFT JOIN Exigo_ETL.dbo.Customers C	
        ON C.Field1 = CASE	
                          WHEN M.SelltoCustomerID LIKE '%tsfl%' THEN	
                              RIGHT(M.SelltoCustomerID, LEN(M.SelltoCustomerID) - 4)	
                          ELSE	
                              M.SelltoCustomerID COLLATE DATABASE_DEFAULT	
                      END	
    LEFT JOIN [Exigo_ETL].[dbo].[Customers] c3 WITH (NOLOCK)	
        ON C.[Field2] = c3.[Field1]	
WHERE DATEDIFF(dd, M.[Posting Date], GETDATE()) < 61	
      AND M.CustomerPostingGroup IN ( 'TSFL' )	
	  AND c.CustomerTypeID = 2
	  AND m.OrderType <> 'RETURN'
ORDER BY [Client Full Name] 





