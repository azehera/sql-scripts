﻿

/*
=============================================================================================
Author:         Luc Emond
Create date: 08/23/2013
-------------------------[AutoShip Errosion]-----------------------------
---Autoship orders errosion versus the first of the month----
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[BI_Reporting]         dbo.Calendar                             Select   
[BI_Reporting]        [dbo].[XL_027_Autoship_DailyArchive]      Select
[BI_Reporting]        [dbo].[AutoShipErrosion]                  Insert
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------

==========================================================================================
*/

CREATE Procedure [dbo].[USP_AutoShipErrosion_2]
as


TRUNCATE TABLE dbo.AutoShipErrosion_2

Declare @Today datetime , @EndOfMonth datetime,@ReportDate datetime,@ReportEndDate datetime
Set @Today = (Select FirstDateOfMonth From Bi_Reporting.Dbo.Calendar where CalendarDate = Convert(Varchar(10),GETDATE()-1,121) GROUP BY FirstDateOfMonth )
set @ReportDate=(Select FirstDateOfMonth From Bi_Reporting.Dbo.Calendar where CalendarDate = Convert(Varchar(10),GETDATE()-38,121) GROUP BY FirstDateOfMonth )
Set @ReportEndDate=(Select FirstDateOfMonth From Bi_Reporting.Dbo.Calendar where CalendarDate = Convert(Varchar(10),GETDATE()-1,121) GROUP BY FirstDateOfMonth )
Set @EndOfMonth =Convert(Varchar(10),GETDATE(),121)

Delete
From dbo.AutoShipErrosion_2
where ReportDate>=@Today
  and TypeName='Medirect'
Set @Today=@Today
while @EndOfMonth = @EndOfMonth
Begin
Begin 



IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A

SELECT ReportDate,CalendarDate, TypeName, Sum([Amount]) Amount
  into #A FROM [BI_Reporting].[dbo].[XL_027_Autoship_DailyArchive]
  where 
  CalendarDate =@Today
  and 
  ReportDate >=@ReportDate
  and CartStatus ='A'
  and TypeName='Medirect'
  group by ReportDate,CalendarDate, TypeName
  order by ReportDate,CalendarDate
  
  

IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B

  Select a.ReportDate,a.Amount, a.TypeName, b.Amount as [7DaysAgo]
  into #B from #A a
  left join #A b
  on Convert(Datetime,a.ReportDate)-7=Convert(Datetime,b.ReportDate)
  
 IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C 

   Select a.ReportDate,a.Amount, a.TypeName, b.Amount as [4DaysAgo]
  into #C from #A a
  left join #A b
  on Convert(Datetime,a.ReportDate)-4=Convert(Datetime,b.ReportDate)
  
 IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E 

  SELECT ReportDate,CalendarDate, TypeName, Sum([Amount]) as [1stOfMonth]
into #E 
FROM [BI_Reporting].[dbo].[XL_027_Autoship_DailyArchive]
  where 
  CalendarDate =@Today
  and 
  ReportDate =@ReportEndDate
  and CartStatus ='A'
  and TypeName='Medirect'
  group by ReportDate,CalendarDate,TypeName
  order by ReportDate,CalendarDate

   insert into dbo.AutoShipErrosion_2
Select b.ReportDate, b.TypeName, b.Amount,[7DaysAgo],[4DaysAgo],e.[1stOfMonth]
from #B b
join #C c
on b.ReportDate=c.ReportDate
and b.TypeName=c.TypeName
join #E e
on b.ReportDate=e.CalendarDate
and b.TypeName=e.TypeName
where b.ReportDate=@Today


set @Today=@Today+1
end 

if (@Today=@EndOfMonth)
break 
else 
Continue
end

--------------------------------------------------------------------------------------------------
Declare @Today1 datetime , @EndOfMonth1 datetime,@ReportDate1 datetime,@ReportEndDate1 datetime
Set @Today1 = (Select FirstDateOfMonth From Bi_Reporting.Dbo.Calendar where CalendarDate = Convert(Varchar(10),GETDATE()-1,121) GROUP BY FirstDateOfMonth )
set @ReportDate1=(Select FirstDateOfMonth From Bi_Reporting.Dbo.Calendar where CalendarDate = Convert(Varchar(10),GETDATE()-38,121) GROUP BY FirstDateOfMonth )
Set @ReportEndDate1=(Select FirstDateOfMonth From Bi_Reporting.Dbo.Calendar where CalendarDate = Convert(Varchar(10),GETDATE()-1,121) GROUP BY FirstDateOfMonth )
Set @EndOfMonth1 =Convert(Varchar(10),GETDATE(),121)


Delete
From dbo.AutoShipErrosion_2
where ReportDate>=@Today1
  and TypeName='TSFL'
Set @Today1=@Today1
while @EndOfMonth1 = @EndOfMonth1
Begin
Begin 



IF object_id('Tempdb..#A1') Is Not Null DROP TABLE #A1

SELECT ReportDate,CalendarDate, TypeName, Sum([Amount]) Amount
  into #A1 FROM [BI_Reporting].[dbo].[XL_027_Autoship_DailyArchive]
  where 
  CalendarDate =@Today1
  and 
  ReportDate >=@ReportDate1
  and CartStatus ='A'
  and TypeName='TSFL'
  group by ReportDate,CalendarDate, TypeName
  order by ReportDate,CalendarDate
  
  

IF object_id('Tempdb..#B1') Is Not Null DROP TABLE #B1

  Select a.ReportDate,a.Amount, a.TypeName, b.Amount as [7DaysAgo]
  into #B1 from #A1 a
  left join #A1 b
  on Convert(Datetime,a.ReportDate)-7=Convert(Datetime,b.ReportDate)
  
 IF object_id('Tempdb..#C1') Is Not Null DROP TABLE #C1 

   Select a.ReportDate,a.Amount, a.TypeName, b.Amount as [4DaysAgo]
  into #C1 from #A1 a
  left join #A1 b
  on Convert(Datetime,a.ReportDate)-4=Convert(Datetime,b.ReportDate)
  
 IF object_id('Tempdb..#E1') Is Not Null DROP TABLE #E1 

  SELECT ReportDate,CalendarDate, TypeName, Sum([Amount]) as [1stOfMonth]
into #E1 
FROM [BI_Reporting].[dbo].[XL_027_Autoship_DailyArchive]
  where 
  CalendarDate =@Today1
  and 
  ReportDate =@ReportEndDate1
  and CartStatus ='A'
  and TypeName='TSFL'
  group by ReportDate,CalendarDate,TypeName
  order by ReportDate,CalendarDate

   insert into dbo.AutoShipErrosion_2
Select b.ReportDate, b.TypeName, b.Amount,[7DaysAgo],[4DaysAgo],e.[1stOfMonth]
from #B1 b
join #C1 c
on b.ReportDate=c.ReportDate
and b.TypeName=c.TypeName
join #E1 e
on b.ReportDate=e.CalendarDate
and b.TypeName=e.TypeName
where b.ReportDate=@Today1


set @Today1=@Today1+1
end 

if (@Today1=@EndOfMonth1)
break 
else 
Continue
end


Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='1221547.35',
[4DaysAgo]='1096404.94'
where ReportDate='2013-08-01'
and TypeName='TSFL'

Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='215567.18',
[4DaysAgo]='193483.23'
where ReportDate='2013-08-01'
and TypeName='Medirect'
-----------------------------------------------------------
Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='670852.67',
[4DaysAgo]='552569.73'
where ReportDate='2013-08-02'
and TypeName='TSFL'

Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='118385.77',
[4DaysAgo]='97512.31'
where ReportDate='2013-08-02'
and TypeName='Medirect'
----------------------------------------------------------------
Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='600010.95',
[4DaysAgo]='477945.98'
where ReportDate='2013-08-03'
and TypeName='TSFL'

Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='105884.29',
[4DaysAgo]='84343.41'
where ReportDate='2013-08-03'
and TypeName='Medirect'
--------------------------------------------------------------
Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='317057.28',
[4DaysAgo]='237750.96'
where ReportDate='2013-08-04'
and TypeName='TSFL'

Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='55951.29',
[4DaysAgo]='41956.05'
where ReportDate='2013-08-04'
and TypeName='Medirect'
----------------------------------------------------------------
Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='781788.16'
where ReportDate='2013-08-05'
and TypeName='TSFL'

Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='137962.62'
where ReportDate='2013-08-05'
and TypeName='Medirect'
--------------------------------------------------------------------
Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='409441.91'
where ReportDate='2013-08-06'
and TypeName='TSFL'

Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='72254.46'
where ReportDate='2013-08-06'
and TypeName='Medirect'

----------------------------------------------------------------------------
Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='451451.65'
where ReportDate='2013-08-07'
and TypeName='TSFL'

Update dbo.AutoShipErrosion_2
Set [7DaysAgo]='79667.94'
where ReportDate='2013-08-07'
and TypeName='Medirect'



select 
 ReportDate
,TypeName
,[1stOfMonth]
,[7DaysAgo]
,[4DaysAgo]
,Amount 
from dbo.AutoShipErrosion_2
order by ReportDate

