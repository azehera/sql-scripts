﻿



/*
===============================================================================
Author:         Kalpesh Patel
Create date: 01/20/2014

==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[ODYSSEY_ETL]       [dbo].[ODYSSEY_BUSINESSCENTER] 			   Select   
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                     Select  
[ODYSSEY_ETL]       [dbo].[ODYSSEY_RANK_SCHEME_DETAILS]          Select
=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
2014-01-22           Luc Emond        Added a new column in Numeric for Coach Id for V_Lookup 
2014-01-24           Luc Emond        Added Coach Address and Phone Number               

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/


CREATE procedure [dbo].[USP_XL_048_New_Rank_Ups]
@M1EndDate Date
 as 
set nocount on ;
--Declare @M1EndDate DATE = '12/31/2017'
Declare @StartDate Datetime

Declare @PrevMonthStartDate Datetime
Declare @PrevMonthEndDate Datetime

Set @StartDate=(select DATEADD(MONTH, DATEDIFF(MONTH, 0, @M1EndDate), 0)) 
--Set @M1EndDate=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)) 

Set @PrevMonthStartDate=(select DATEADD(MONTH, DATEDIFF(MONTH, 0, @M1EndDate)-1, 0)) 
Set @PrevMonthEndDate=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, @M1EndDate)-1, -1)) 
------------------------------Pull only active Co-Apps into a Co-App Temp Table-----------------------------------
Select *
Into #Coapp
	From [ODYSSEY_ETL].[dbo].[ODYSSEY_CO_APPLICANT]
		Where [IS_OBSOLETE] is null		
-----------------------Pull Rank ups with contact info---------------------------------------------
SELECT
 A.[CUSTOMER_NUMBER] as [Coach ID]
 ,Convert(numeric(36,0),(A.[CUSTOMER_NUMBER])) as V_LookupCoachID---2014-01-24 Luc Emond
,A.[CUSTOMER_ID]
,A.[FIRST_NAME]as [First Name]
,A.[LAST_NAME]as [Last Name]
,isnull(Co.[LAST_NAME],'')  as [Co-App Last Name]
,isnull(Co.[FIRST_NAME],'') as [Co-App First Name]
,[Recognition_Name]
,A.[EMAIL1_ADDRESS] as [Coach_Email]
,Convert(Varchar(10),C.[COMMISSION_PERIOD],110) as [Rank Up Month]
,C.[HIGHEST_ACHIEVED_RANK] as [New Rank Up]
--,C.[RANK] as [New Rank Up]
,(RS.[RANK_ID]+1) as [Rank #]
--,Rs.[RANK_ID] as [Rank #]
,C2.[RANK] AS PrviousMonthRank
--,Rs2.RANK_ID AS [Prev Rank #]
--,Convert(Varchar(10),C2.[COMMISSION_PERIOD],110) as [Prev Commission Period]
,A.[MAIN_PHONE]
,CA.[ADDRESS1]
,CA.[ADDRESS2]
,CA.[CITY]
,CA.[STATE]
,CA.[POSTAL_CODE] 
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
	Inner JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] C
			on A.[CUSTOMER_ID] = C.[CUSTOMER_ID] 
				AND CAST(C.[COMMISSION_PERIOD] AS Date) between @StartDate and @M1EndDate
	Left JOIN [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] C2
			on A.[CUSTOMER_ID] = C2.[CUSTOMER_ID] 
				AND CAST(C2.[COMMISSION_PERIOD] AS Date) between @PrevMonthStartDate AND @PrevMonthEndDate
	Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] RS
		on C.[RANK] = RS.[RANK_NAME]
	--Left Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] RS2
	--	on C2.[RANK] = RS2.[RANK_NAME]
	Inner Join 	[ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] ca---2014-01-24 Luc Emond
	   on  A.[MAIN_ADDRESS_ID] = ca.[ADDRESS_ID]
	   	left join #Coapp Co
		on A.[CUSTOMER_ID] = Co.[CUSTOMER_ID]
Where  C.[RANK_ADVANCE_DATE] = C.[COMMISSION_PERIOD]
		and C.[HIGHEST_ACHIEVED_RANK] not in ('None','HEALTH_COACH','FT_HEALTH_COACH')
		 
	
Order by Rs.[RANK_ID] DESC ,C.[HIGHEST_ACHIEVED_RANK] asc




