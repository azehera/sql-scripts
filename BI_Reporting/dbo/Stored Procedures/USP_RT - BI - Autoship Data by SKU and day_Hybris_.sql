﻿



/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------
Future Autoship orders
--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------
ecomm_etl_hybris     [dbo].[ETL_orderentries]                Select
ecomm_etl_hybris     [dbo].[ETL_orders]                      Select   
ecomm_etl_hybris     [dbo].[ETL_ProductsUnits]				 Select 
BI_SSAS_Cubes        [dbo].[DimItemCode]                     Select                     
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - BI - Autoship Data by SKU and day(Hybris)] 
AS 

/****** Script for ITSD 2225 Quantities of Autoship SKU Orders for the next 60 days ******/
/*https://medifast.atlassian.net/browse/ITSD-2225*/
IF OBJECT_ID('Tempdb..#AA') IS NOT NULL
    DROP TABLE #AA;
SELECT DISTINCT
       PK,
       CAST(p_nextautoshiporderdate AS DATE) NextAutoDate,
	   Autoship_Status
INTO #AA
FROM ecomm_etl_hybris.dbo.ETL_orders
WHERE Order_Category = 'AutoshipTemplate'
      --AND Autoship_Status = 'Active'
      AND CAST(p_nextautoshiporderdate AS DATE)
      BETWEEN CAST(GETDATE() AS DATE) AND CAST(GETDATE() + 60 AS DATE);

IF OBJECT_ID('Tempdb..#AggregatedBucket') IS NOT NULL
    DROP TABLE #AggregatedBucket;
SELECT *
INTO #AggregatedBucket
FROM
(
    SELECT A1.PK apk,
           A1.NextAutoDate,
		   A1.Autoship_Status,
           oe.[createdTS],
           oe.[modifiedTS],
           oe.[PK] bpk,
           oe.[p_order],
           oe.[p_product],
           oe.[p_bundleno],
           oe.[p_kitno],
           oe.[p_quantity],
           oe.[p_entrynumber],
           oe.[p_totalprice],
           oe.[upsrt_dttm], --
           p.[createdTS] pcreatedts,
           p.[modifiedTS] pmodifiedts,
           p.[PK] ppk,
           p.[p_code],
           p.[p_unit],
           p.[p_sku],
           p.[UnitCode],
           p.[p_conversion],
           p.[p_unittype],
           p.[p_baseunitconversion],
           (oe.p_quantity * p.p_baseunitconversion) totPq
    FROM ecomm_etl_hybris.[dbo].[ETL_orderentries] oe
        INNER JOIN #AA A1
            ON A1.PK = oe.p_order
        INNER JOIN ecomm_etl_hybris.dbo.ETL_ProductsUnits p
            ON p.PK = oe.p_product
) jj;


-------Aggregate

SELECT CASE
           WHEN A.SKU LIKE '%[^0-9]%' THEN
               STUFF(A.SKU, PATINDEX('%[^0-9]%', A.SKU), 1, '')
           ELSE
               A.SKU
       END AS SKU,
       --SKU,
       [Product Name],
       [next auto ship Date],
       [Discontinued],
	   Autoship_Status,
       SUM([Qty of Eaches]) [Qty of Eaches]
FROM
(
    SELECT LEFT(p_sku, 5) SKU,
           p_code [Product Name],
           NextAutoDate [next auto ship Date],
		   Autoship_Status,
           (p_quantity * p_baseunitconversion) [Qty of Eaches]
    FROM #AggregatedBucket
) A
    JOIN BI_SSAS_Cubes.dbo.DimItemCode B
        ON CASE
               WHEN A.SKU LIKE '%[^0-9]%' THEN
                   STUFF(A.SKU, PATINDEX('%[^0-9]%', A.SKU), 1, '')
               ELSE
                   A.SKU
           END = B.[Itemcode]
--WHERE sku like '77795%' AND [next auto ship Date] = '2019-04-30'
GROUP BY SKU,
         [Product Name],
         [next auto ship Date],
		 Autoship_Status,
         [Discontinued];


