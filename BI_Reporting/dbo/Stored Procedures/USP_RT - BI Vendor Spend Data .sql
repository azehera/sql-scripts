﻿





/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------

[BI_Reporting]     [dbo].[XL_015_Monthly_AP_ALL];            Select 
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - BI Vendor Spend Data ]
AS
 
SELECT [Entity],
       [Posting Date],
       [Document Date],
       [G_L Account No_],
       [G_L Name],
       [Vendor No_],
       [VendorName],
       [Vendor Posting Group],
       [Global Dimension 2 Code] [Department],
       [External Document No_],
       [Debit Amount],
       [Credit Amount],
       [NetTotal]
FROM [BI_Reporting].[dbo].[XL_015_Monthly_AP_ALL];


