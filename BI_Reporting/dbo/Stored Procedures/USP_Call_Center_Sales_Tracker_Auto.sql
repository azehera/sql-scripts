﻿

--/*
--==================================================================================
--Author:         Menkir Haile
--Create date: 12/28/2012

--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--[BI_Reporting]		[CallAutoship]						      Select
--BI_SSAS_Cubes			FactSalesODYSSEY_ETL				      Select                       
--==================================================================================
--REVISION LOG
--Date           Name               Change
------------------------------------------------------------------------------------

--==================================================================================
--NOTES:
------------------------------------------------------------------------------------
--==================================================================================
--*/
CREATE procedure [dbo].[USP_Call_Center_Sales_Tracker_Auto] as
set nocount on ;

SELECT  [Usa_ID]
      ,[USA_CUST_NBR]
      ,[USA_First_Nm]
      ,[USA_Last_Nm]
      ,[USA_HOME_PH]
      ,[TypeName]
      ,[CustomerJoinedAutoship]
      ,[1stAutoshipDate]
      ,[1stAutoshipAmount]
      ,[2stAutoshipDate]
      ,[2stAutoshipAmount]
      ,SUM(Amount) as Sales
  FROM [BI_Reporting].[dbo].[CallAutoship] a
  left join BI_SSAS_Cubes.dbo.FactSales b
  on a.[USA_CUST_NBR]=b.SelltoCustomerID
  and  [Posting Date]>'2014-06-18'
  group by [Usa_ID]
      ,[USA_CUST_NBR]
      ,[USA_First_Nm]
      ,[USA_Last_Nm]
      ,[USA_HOME_PH]
      ,[TypeName]
      ,[CustomerJoinedAutoship]
      ,[1stAutoshipDate]
      ,[1stAutoshipAmount]
      ,[2stAutoshipDate]
      ,[2stAutoshipAmount]
	  order by sales desc