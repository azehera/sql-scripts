﻿
/*
--==================================================================================
Author:         Emily Trainor
Create date: 11/1/2012
--------------------------MWCC Accounts Receivable Aging Report--------------------
Calculate Charges, Payments, Balances and Aging for all ASF accounts with MWCC---
=================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
Book4Time_ETL			B4T_transaction_log_detail			      Select
Book4Time_ETL			B4T_transaction_log_header			      Select   
Book4Time_ETL			B4T_product_master					      Select    
Book4Time_ETL			B4T_tlog_detail						      Select
Book4Time_ETL			B4T_tlog_header						      Select 
Book4Time_ETL			B4T_transaction_log_tenders			      Select 
Book4Time_ETL			B4T_Tender_Types					      Select 
Book4Time_ETL			B4T_tlog_tenders					      Select 
Book4Time_ETL			B4T_Person							      Select 
Book4Time_ETL			B4T_customer						      Select  
Book4Time_ETL			B4T_region_locations				      Select 
Book4Time_ETL			B4T_region							      Select 
Book4Time_ETL			B4T_location						      Select 
Book4Time_ETL			B4T_Phone							      Select                          
==================================================================================
REVISION LOG
Date              Name                       Change
------------------------------------------------------------------------------------
2013-03-25      Luc Emond                   Comment Out the Payment = $0.00  
2014-01-16      Luc Emond                   Update script for the the last Visit
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/

CREATE procedure [dbo].[USP_XL_018_MWCC_AR_Aging] as 
set nocount on ;

---------------------Pull Client Data (Parent of ParentID)------------------
IF object_id('Tempdb..#AA') Is Not Null DROP TABLE #AA
Select
	[customer_id] into #aa
	From [Book4Time_ETL].[dbo].[B4T_Customer]

CREATE CLUSTERED INDEX IDX_C_aa_customer_id ON #aa(customer_id)
	
Alter table #aa

Add
	 [Parent_id] bigint null
	,[parent_id of parent_ID] bigint null
	,[ASF_Contract] varchar (50)
    ,[Home Phone] varchar (32)
    ,[Cell/Mobile] varchar (32)
    ,[Business Phone] varchar (32)
	
Update #aa
SET [Parent_id] = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
where #aa.customer_id = p.customer_id
and #aa.[Parent_id] is null

Update #aa
set [parent_id of parent_ID] = parent_id 
where #aa.customer_id = #aa.parent_id
	
UPDATE #aa
SET [parent_id of parent_ID] = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
where #aa.parent_id = p.customer_id
and [parent_id of parent_ID] is null

Update #aa
	Set [ASF_Contract] = [custom_field1]
From [Book4Time_ETL].[dbo].[B4T_Customer]p
	where #aa.[parent_id of parent_ID] = p.customer_id

update #aa	
	Set [Home Phone] = ph.phone_number
From [Book4Time_ETL].[dbo].[B4T_Phone] Ph
	Where [phone_type] = '1'
		and #aa.[parent_id of parent_ID] = ph.[phone_id]

update #aa	
	Set [Cell/Mobile] = ph.phone_number
From [Book4Time_ETL].[dbo].[B4T_Phone] Ph
	Where [phone_type] = '2'
	and #aa.[parent_id of parent_ID] = ph.[phone_id]	

update #aa	
	Set [Business Phone] = ph.phone_number
From [Book4Time_ETL].[dbo].[B4T_Phone] Ph
	Where [phone_type] = '3'	
	and #aa.[parent_id of parent_ID] = ph.[phone_id]

------------------------------Declare Report Date------------------------------
DECLARE @end_date datetime
SET @end_date = GETDATE()
--SET @end_date = '08/31/2014 23:59'

--------------------Create Temp table for Charge and Payment Data--------------- 

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
   
CREATE TABLE #a(
      transaction_id bigint
      ,transaction_date datetime
      ,charge decimal(10,2)
      ,payments decimal(10,2)
      ,location_id bigint
      ,customer_id bigint
      ,parent_id bigint
      ,[parent_id of parent_ID] bigint
      ,cid bigint
      ,ASF_Contract varchar (50)
      ,[Home Phone] varchar (32)
      ,[Cell/Mobile] varchar (32)
      ,[Business Phone] varchar (32)
      )

SET NOCOUNT ON;

----------------------Pull Payment data by customer by location------------------
INSERT INTO #a

SELECT 
       tl.transaction_id
      ,th.transaction_date
      ,0.00
      ,tl.sold_price*tl.qty
      ,th.location_id 
      ,th.customer_id
      ,null as [parent_id]
      ,null as [parent_id of parent_ID]
      ,th.cid
      ,null as [ASF_Contract]
      ,null as [Home Phone]
      ,null as [Cell/Mobile]
      ,null as [Business Phone]
FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] tl
INNER JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] th 
                  ON tl.transaction_id = th.transaction_id 
                  AND th.transaction_type in( 's','r' ) 
INNER JOIN [Book4Time_ETL].[dbo].[B4T_product_master] pm 
                  on tl.product_id = pm.product_id 
                  and pm.product_type_cd ='h'
WHERE th.transaction_date <= @end_date 
      
            
UNION ALL
SELECT 
       tl.tlog_id
      ,th.transaction_date
      ,0.00
      ,tl.sold_price*tl.qty 
      ,th.location_id 
      ,th.customer_id
      ,null as [parent_id]
      ,null as [parent_id of parent_ID]
      ,th.cid
      ,null as [ASF_Contract]
       ,null as [Home Phone]
      ,null as [Cell/Mobile]
      ,null as [Business Phone]
FROM [Book4Time_ETL].[dbo].[B4T_tlog_detail] tl
INNER JOIN [Book4Time_ETL].[dbo].[B4T_tlog_header] th 
            ON tl.tlog_id = th.tlog_id
            AND th.transaction_type in( 's','r' ) 
            and is_pending = '0'
INNER JOIN [Book4Time_ETL].[dbo].[B4T_product_master] pm 
            on tl.product_id = pm.product_id 
            and pm.product_type_cd ='h'
WHERE th.transaction_date <= @end_date 

----------------------Pull Charge data by customer by location------------------
INSERT INTO #a

SELECT 
       th.transaction_id
      ,th.transaction_date
      ,tt.tender_amt
      ,0.00
      ,th.location_id 
      ,th.customer_id
      ,null as [parent_id]
      ,null as [parent_id of parent_ID]
      ,th.cid
      ,null as [ASF_Contract]
       ,null as [Home Phone]
      ,null as [Cell/Mobile]
      ,null as [Business Phone]
FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_header] th
INNER JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_tenders] tt 
      ON th.transaction_id = tt.transaction_group_id 
      AND th.transaction_type in( 's','r' )
INNER JOIN [Book4Time_ETL].[dbo].[B4T_Tender_Types] tnt 
      ON tnt.tender_type_id = tt.tender_type_id 
      and tnt.tender_cd ='house' 
WHERE th.transaction_date <= @end_date


UNION ALL

SELECT 
       th.tlog_id
      ,th.transaction_date
      ,tt.tender_amt
      ,0.00
      ,th.location_id 
      ,th.customer_id
      ,null as [parent_id]
      ,null as [parent_id of parent_ID]
      ,th.cid
      ,null as [ASF_Contract]
       ,null as [Home Phone]
      ,null as [Cell/Mobile]
      ,null as [Business Phone]
FROM [Book4Time_ETL].[dbo].[B4T_tlog_header] th
INNER JOIN [Book4Time_ETL].[dbo].[B4T_tlog_tenders] tt 
      ON th.tlog_id = tt.tlog_id  
      AND th.transaction_type in( 's','r' )
INNER JOIN [Book4Time_ETL].[dbo].[B4T_Tender_Types] tnt 
      ON tnt.tender_type_id = tt.tender_type_id 
      and tnt.tender_cd ='house'
WHERE th.transaction_date <= @end_date
-----------------------Update with Parent of Parent ID Contact Information-------------------
Update #a
Set [parent_id] = a.[parent_id], 
	[parent_id of parent_ID] = A.[parent_id of parent_ID],
	[ASF_Contract] = A.[ASF_Contract],
	[Home Phone] = A.[Home Phone],
	[Cell/Mobile] = A.[Cell/Mobile],
	[Business Phone] = A.[Business Phone]
From #aa A
	Where #a.[customer_id] = A.[customer_id]

-----------------------------Pull Date Information------------------------------2014-01-16 Luc Emond 
IF object_id('Tempdb..#APP') Is Not Null DROP TABLE #APP------2014-01-16 Luc Emond 
select customer_id, location_id, MAX(convert(varchar(10),start_time,121)) as [Last Visit Date]
INTO #APP
from [Book4Time_ETL].[dbo].[B4T_appointment]	
group by customer_id, location_id

IF object_id('Tempdb..#ADF') Is Not Null DROP TABLE #ADF------2014-01-16 Luc Emond 
Select 
a.customer_id,
a.parent_id,
a.[parent_id of parent_ID],
a.location_id,
MIN(convert(varchar(10),a.transaction_date,121)) as [Charge date],
case when MIN(convert(varchar(10),a.transaction_date,121))=max(convert(varchar(10),b.transaction_date,121)) 
         then null else max(convert(varchar(10),b.transaction_date,121)) end   as [Payment date],
[Last Visit Date]
into #adf 
from #a a
left join #a b
	on a.[parent_id of parent_ID]=b.customer_id
	and a.location_id=b.location_id
left join #APP c
	on a.customer_id=c.customer_id
	and a.location_id=c.location_id
--Where c.start_time < convert(varchar(10),GETDATE(),121)
---a.payments = '0.00' 	and ----comment out 2013-03-25---
group by a.customer_id
	,a.location_id
	,a.parent_id
,a.[parent_id of parent_ID]
,[Last Visit Date]

----------------------------Pull Phone Data into Report--------------------------------
IF object_id('Tempdb..#b') Is Not Null DROP TABLE #b

Select		
			 r.region_name AS [Region Name]
            ,l.location_name AS [Location Name]
            ,b.customer_id as [Customer ID]
            ,b.parent_id as [Parent ID]
            ,b.[parent_id of parent_ID] as [parent_id of parent_ID]
            ,pt.first_name + ' ' + pt.last_name as [Customer Name]
            ,isnull(pt.email,' ') as [Email]
            ,cast((null) as varchar) as [Home Phone]                   
            ,cast((null) as varchar) [Cell/Mobile]
            ,cast((null) as varchar) [Business Phone]
            ,[Charge Date]
            ,isnull([Payment Date],'None') as [Last Payment Date]
            ,isnull(b.[Last Visit Date], 'None') as [Last Visit Date]
            ,SUM(t.charge) AS Charge
            ,SUM(t.payments) AS Payments
            ,convert(VARCHAR,GETDATE()-1,101) as [as of day]
            ,(SUM(t.charge)-(SUM(t.payments))) as [Balance]
            ,DATEDIFF(d,[Charge Date],GETDATE())as [Aging - Charge]
            ,isnull(DATEDIFF(d,[Payment Date],GETDATE()),'0') as [Aging - Last Payment]
            ,cast((null) as varchar) as [ASF Contract #] 
Into #b
FROM #a t
INNER JOIN #adf b
	  on t.[parent_id of parent_ID] = b.[customer_id]
      and t.[location_id] = b.[location_id]
INNER JOIN [Book4Time_ETL].[dbo].[B4T_Person] pt 
      ON t.[parent_id of parent_ID] = pt.person_id
INNER JOIN [Book4Time_ETL].[dbo].[B4T_customer] c 
      ON t.customer_id = c.customer_id 
INNER JOIN [Book4Time_ETL].[dbo].[B4T_region_locations] rl 
      ON rl.location_id = t.location_id
INNER JOIN [Book4Time_ETL].[dbo].[B4T_region]r
      ON rl.region_id = r.region_id
INNER JOIN [Book4Time_ETL].[dbo].[B4T_location] l
      on rl.location_id = l.location_id
    GROUP BY 
             b.customer_id  
            ,r.region_name 
            ,l.location_name
            ,pt.first_name
            ,pt.last_name 
            ,c.[custom_field1]
            ,pt.email
            ,[Charge Date]
            ,[Payment Date]
            ,[Last Visit Date]
            ,b.parent_id
            ,b.[parent_id of parent_ID]
            having (SUM(t.charge)-(SUM(t.payments)))<>0
ORDER BY 
             r.region_name 
            ,l.location_name
            ,Balance desc
------------------------Insert Phone data for Parent of Parent ID----------------------  
Update #b
	Set [Home Phone] = a.[Home Phone],
		[Cell/Mobile] = a.[Cell/Mobile],
		[Business Phone] = a.[Business Phone],
		[ASF Contract #] = a.[ASF_Contract]
	From #a A
		Where #b.[parent_id of parent_ID] = A.[parent_id of parent_ID]

--------------------------Pull Final Report ---------------------------------------------
Select 
   [Region Name],
   [Location Name],
   --[Customer ID], 
   --[Parent ID],
   [parent_id of parent_ID] as [Customer ID], 
   [Customer Name], 
   [Email], 
   isnull([Home Phone],' ') as [Home Phone], 
   isnull([Cell/Mobile],' ') as [Cell/Mobile], 
   isnull([Business Phone],' ') as [Business Phone], 
   [Charge Date], 
   [Last Payment Date], 
   [Last Visit Date], 
   sum([Charge])as [Charge], 
   sum([Payments]) as [Payments], 
   [as of day], 
   sum([Balance]) as [Balance], 
   [Aging - Charge], 
   [Aging - Last Payment],
   isnull([ASF Contract #], ' ') as [ASF Contract #]
From #b	
	Group by 
			[Region Name],
   [Location Name],
   --[Customer ID], 
   --[Parent ID],
   [parent_id of parent_ID], 
   [Customer Name], 
   [Email], 
   [Home Phone], 
   [Cell/Mobile], 
   [Business Phone], 
   [Charge Date], 
   [Last Payment Date], 
   [Last Visit Date], 
   [as of day], 
   [Aging - Charge], 
   [Aging - Last Payment],
   [ASF Contract #]
   having (SUM(charge)-(SUM(payments)))<>0

