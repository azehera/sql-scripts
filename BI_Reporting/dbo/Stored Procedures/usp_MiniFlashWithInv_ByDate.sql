﻿create PROCEDURE [dbo].[usp_MiniFlashWithInv_ByDate]
@RunDate DATETIME
AS

/***********************************************************************
Date: 6/22/2015
Develeoper: Derald Smith
Description: Variation of the stored procedure usp_MiniFlashWithInv so that
the data can be run for a specific date and not just yesterday.

*************************************************************************/

--EXEC [dbo].[usp_MiniFlashWithInv_ByDate] @RunDate = '2015-06-20'

SET NOCOUNT ON

SELECT
  DATEADD(DD, -1, CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AS Yesterday,
  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS SalesMedDirect,

  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS SalesFranchise,
  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS SalesTSFL,
  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS SalesWholesale,
  (
    SELECT isnull(SUM(InvSalesDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS SalesINTL,
  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL)
  ) AS SalesTotal,
  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS UnitsMedDirect,

  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS UnitsFranchise,
  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS UnitsTSFL,
  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS UnitsWholesale,
  (
    SELECT isnull(SUM(InvUnitsDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS UnitsINTL,
  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL)
  ) AS UnitsTotal,
  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
  ) AS VarianceMTD_Franchise,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
  ) AS VarianceYTD_Franchise,
  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
  ) AS VarianceMTD_MedDirect,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
  ) AS VarianceYTD_MedDirect,


  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
  ) AS VarianceMTD_TSFL,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
  ) AS VarianceYTD_TSFL,
  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
  ) AS VarianceMTD_Wholesale,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
  ) AS VarianceYTD_Wholesale,
  (
    SELECT
          isnull(((SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
  ) AS VarianceMTD_INTL,
  (
    SELECT
          isnull(((SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
  ) AS VarianceYTD_INTL,
  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
    AND CPG <> 'MWCC'
  ) AS VarianceMTD_Total,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
    AND CPG <> 'MWCC'
  ) AS VarianceYTD_Total,
  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS SalesMTD_MedDirect,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS SalesYTD_MedDirect,


  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS SalesMTD_Franchise,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS SalesYTD_Franchise,
  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS SalesMTD_TSFL,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS SalesYTD_TSFL,
  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS SalesMTD_Wholesale,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS SalesYTD_Wholesale,
  (
    SELECT isnull(SUM(InvSalesMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS SalesMTD_INTL,
  (
    SELECT isnull(SUM(InvSalesYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS SalesYTD_INTL,
  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL)
  ) AS SalesMTD_Total,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL)
  ) AS SalesYTD_Total,
  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS UnitsMTD_MedDirect,
  (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS UnitsYTD_MedDirect,


  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS UnitsMTD_Franchise,
  (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS UnitsYTD_Franchise,
  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS UnitsMTD_TSFL,
  (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS UnitsYTD_TSFL,
  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS UnitsMTD_Wholesale,
  (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS UnitsYTD_Wholesale,
  (
    SELECT isnull(SUM(InvUnitsMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS UnitsMTD_INTL,
   (
    SELECT isnull(SUM(InvUnitsYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS UnitsYTD_INTL,
  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL)
  ) AS UnitsMTD_Total,
   (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL)
  ) AS UnitsYTD_Total,
  (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS ASPDay_INTL,

  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS ASPMTD_INTL,


 (
	SELECT (case when Sum(CAST([InvUnitsMTD] AS bigint)) = 0 then 0 else SUM(InvSalesMTD)/SUM(CAST([InvUnitsMTD] AS bigint)) end -
	case when Sum(CAST([InvUnitsPriorYrMTD] AS bigint)) = 0 then 0 else SUM(InvSalesPriorYrMTD)/SUM(CAST([InvUnitsPriorYrMTD] AS bigint)) end) /
	(case when Sum(CAST([InvUnitsPriorYrMTD] AS bigint)) = 0 then 1 when SUM(InvSalesPriorYrMTD) = 0 then 100 else SUM(InvSalesPriorYrMTD)/SUM(CAST([InvUnitsPriorYrMTD] AS bigint)) end) * 100
		FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
		WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
			  (InvSalesMTD IS NOT NULL) AND
			(CPG = 'INTL')
  ) AS VarianceASPMTD_INTL,



  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS ASPYTD_INTL,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS VarianceASPYTD_INTL,
      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS ASPDay_TSFL,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS ASPMTD_TSFL,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS VarianceASPMTD_TSFL,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS ASPYTD_TSFL,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS VarianceASPYTD_TSFL,

      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS ASPDay_DOCTORS,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS ASPMTD_DOCTORS,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS VarianceASPMTD_DOCTORS,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS ASPYTD_DOCTORS,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS VarianceASPYTD_DOCTORS,
      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS ASPDay_FRANCHISE,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS ASPMTD_FRANCHISE,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) 
    when SUM(InvSalesPriorYrMTD)=0 then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS VarianceASPMTD_FRANCHISE,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS ASPYTD_FRANCHISE,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS VarianceASPYTD_FRANCHISE,
      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS ASPDay_MEDIFAST,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS ASPMTD_MEDIFAST,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS VarianceASPMTD_MEDIFAST,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS ASPYTD_MEDIFAST,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS VarianceASPYTD_MEDIFAST,
      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
  ) AS ASPDay_Total,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL)
  ) AS ASPMTD_Total,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND CPG <> 'MWCC'
  ) AS VarianceASPMTD_Total,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
  ) AS ASPYTD_Total,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND CPG <> 'MWCC'
  ) AS VarianceASPYTD_Total,
   (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
  ) AS VarianceUnitsMTD_Franchise,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
  ) AS VarianceUnitsYTD_Franchise,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
  ) AS VarianceUnitsMTD_MedDirect,
0 AS VarianceUnitsYTD_MedDirect,

  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
  ) AS VarianceUnitsMTD_TSFL,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
  ) AS VarianceUnitsYTD_TSFL,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
  ) AS VarianceUnitsMTD_Wholesale,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
  ) AS VarianceUnitsYTD_Wholesale,
  (
    SELECT
          isnull(Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD])) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
  ) AS VarianceUnitsMTD_INTL,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
  ) AS VarianceUnitsYTD_INTL,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND CPG <> 'MWCC'
  ) AS VarianceUnitsMTD_Total,
  (
    SELECT
         Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND CPG <> 'MWCC'
  ) AS VarianceUnitsYTD_Total,
  
  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS OrdersMedDirect,

  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS OrdersFranchise,
  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS OrdersTSFL,
  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS OrdersWholesale,
  (
    SELECT isnull(SUM(InvOrdersDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS OrdersINTL,
  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL)
  ) AS OrdersTotal,
  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS OrdersMTD_MedDirect,
   (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
  ) AS OrdersYTD_MedDirect,


  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS OrdersMTD_Franchise,
   (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
  ) AS OrdersYTD_Franchise,
  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS OrdersMTD_TSFL,
   (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'TSFL')
  ) AS OrdersYTD_TSFL,
  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS OrdersMTD_Wholesale,
  (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
  ) AS OrdersYTD_Wholesale,
  (
    SELECT isnull(SUM(InvOrdersMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS OrdersMTD_INTL,
  (
    SELECT isnull(SUM(InvOrdersYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'INTL')
  ) AS OrdersYTD_INTL,
  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL)
  ) AS OrdersMTD_Total,
    (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL)
  ) AS OrdersYTD_Total,
   (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
  ) AS VarianceOrdersMTD_Franchise,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
  ) AS VarianceOrdersYTD_Franchise,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
  ) AS VarianceOrdersMTD_MedDirect,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD])))/ NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
  ) AS VarianceOrdersYTD_MedDirect,


  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
  ) AS VarianceOrdersMTD_TSFL,
  
  
  
  
  
  
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
  ) AS VarianceOrdersYTD_TSFL,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
  ) AS VarianceOrdersMTD_Wholesale,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
  ) AS VarianceOrdersYTD_Wholesale,
  (
    SELECT
          isnull(Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD])) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
  ) AS VarianceOrdersMTD_INTL,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
  ) AS VarianceOrdersYTD_INTL,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND CPG <> 'MWCC'
  ) AS VarianceOrdersMTD_Total,
  (
    SELECT
         Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND CPG <> 'MWCC'
  ) AS VarianceOrdersYTD_Total,
-------------------------------------------USA--------------------------------------------------

  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='USA'
  ) AS SalesMedDirectUSA,

  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='USA'
  ) AS SalesFranchiseUSA,
  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='USA'
  ) AS SalesTSFLUSA,
  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='USA'
  ) AS SalesWholesaleUSA,
  (
    SELECT isnull(SUM(InvSalesDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='USA'
  ) AS SalesINTLUSA,
  (
    SELECT SUM(InvSalesDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL)
          and Country='USA'
  ) AS SalesTotalUSA,
  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='USA'
  ) AS UnitsMedDirectUSA,

  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='USA'
  ) AS UnitsFranchiseUSA,
  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='USA'
  ) AS UnitsTSFLUSA,
  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='USA'
  ) AS UnitsWholesaleUSA,
  (
    SELECT isnull(SUM(InvUnitsDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='USA'
  ) AS UnitsINTLUSA,
  (
    SELECT SUM(InvUnitsDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL)
          and Country='USA'
  ) AS UnitsTotalUSA,
  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
          and Country='USA'
  ) AS VarianceMTD_FranchiseUSA,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
          and Country='USA'
  ) AS VarianceYTD_FranchiseUSA,
  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
          and Country='USA'
  ) AS VarianceMTD_MedDirectUSA,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
          and Country='USA'
  ) AS VarianceYTD_MedDirectUSA,


  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
          and Country='USA'
  ) AS VarianceMTD_TSFLUSA,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
          and Country='USA'
  ) AS VarianceYTD_TSFLUSA,
  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
          and Country='USA'
  ) AS VarianceMTD_WholesaleUSA,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
          and Country='USA'
  ) AS VarianceYTD_WholesaleUSA,
  
  
  ----------
  (
    SELECT
          isnull(((SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
          and Country='USA'
  ) AS VarianceMTD_INTLUSA,
  (
    SELECT
          isnull(((SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
          and Country='USA'
  ) AS VarianceYTD_INTLUSA,
  (
    SELECT
          (SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
    and Country='USA' AND CPG <> 'MWCC'
  ) AS VarianceMTD_TotalUSA,
  (
    SELECT
          (SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
   and Country='USA' AND CPG <> 'MWCC'
  ) AS VarianceYTD_TotalUSA,
  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='USA'
  ) AS SalesMTD_MedDirectUSA,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='USA'
  ) AS SalesYTD_MedDirectUSA,

  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='USA'
  ) AS SalesMTD_FranchiseUSA,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='USA'
  ) AS SalesYTD_FranchiseUSA,
  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='USA'
  ) AS SalesMTD_TSFLUSA,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='USA'
  ) AS SalesYTD_TSFLUSA,
  
 ---------------------------------------------------------------------- 
  
  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='USA'
  ) AS SalesMTD_WholesaleUSA,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='USA'
  ) AS SalesYTD_WholesaleUSA,
  (
    SELECT isnull(SUM(InvSalesMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='USA'
  ) AS SalesMTD_INTLUSA,
  (
    SELECT isnull(SUM(InvSalesYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='USA'
  ) AS SalesYTD_INTLUSA,
  (
    SELECT SUM(InvSalesMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL)
          and Country='USA'
  ) AS SalesMTD_TotalUSA,
  (
    SELECT SUM(InvSalesYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL)
           and Country='USA'
  ) AS SalesYTD_TotalUSA,
  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='USA'
  ) AS UnitsMTD_MedDirectUSA,
  (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='USA'
  ) AS UnitsYTD_MedDirectUSA,

  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='USA'
  ) AS UnitsMTD_FranchiseUSA,
  (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='USA'
  ) AS UnitsYTD_FranchiseUSA,
  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='USA'
  ) AS UnitsMTD_TSFLUSA,
  (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='USA'
  ) AS UnitsYTD_TSFLUSA,
  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='USA'
  ) AS UnitsMTD_WholesaleUSA,
  (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='USA'
  ) AS UnitsYTD_WholesaleUSA,
  (
    SELECT isnull(SUM(InvUnitsMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='USA'
  ) AS UnitsMTD_INTLUSA,
   (
    SELECT isnull(SUM(InvUnitsYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='USA'
  ) AS UnitsYTD_INTLUSA,
  (
    SELECT SUM(InvUnitsMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL)
          and Country='USA'
  ) AS UnitsMTD_TotalUSA,
   (
    SELECT SUM(InvUnitsYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL)
          and Country='USA'
  ) AS UnitsYTD_TotalUSA,
  (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='USA'
  ) AS ASPDay_INTLUSA,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='USA'  ) AS ASPMTD_INTLUSA,
           
           
  ----------------------------------------------------------------------           
           
 (
	SELECT (case when Sum(CAST([InvUnitsMTD] AS bigint)) = 0 then 0 else SUM(InvSalesMTD)/SUM(CAST([InvUnitsMTD] AS bigint)) end -
	case when Sum(CAST([InvUnitsPriorYrMTD] AS bigint)) = 0 then 0 else SUM(InvSalesPriorYrMTD)/SUM(CAST([InvUnitsPriorYrMTD] AS bigint)) end) /
	(case when Sum(CAST([InvUnitsPriorYrMTD] AS bigint)) = 0 then 1 when SUM(InvSalesPriorYrMTD) = 0 then 100 else SUM(InvSalesPriorYrMTD)/SUM(CAST([InvUnitsPriorYrMTD] AS bigint)) end) * 100
    FROM  [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='USA' 
  ) AS VarianceASPMTD_INTLUSA,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='USA'
  ) AS ASPYTD_INTLUSA,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='USA'
  ) AS VarianceASPYTD_INTLUSA,
      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='USA'
  ) AS ASPDay_TSFLUSA,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='USA'
  ) AS ASPMTD_TSFLUSA,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='USA'
  ) AS VarianceASPMTD_TSFLUSA,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='USA'
  ) AS ASPYTD_TSFLUSA,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='USA'
  ) AS VarianceASPYTD_TSFLUSA,



     (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='USA'
  ) AS ASPDay_DOCTORSUSA,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='USA'
  ) AS ASPMTD_DOCTORSUSA,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='USA'
  ) AS VarianceASPMTD_DOCTORSUSA,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='USA'
  ) AS ASPYTD_DOCTORSUSA,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='USA'
  ) AS VarianceASPYTD_DOCTORSUSA,
      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='USA'
  ) AS ASPDay_FRANCHISEUSA,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='USA'
  ) AS ASPMTD_FRANCHISEUSA,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) 
    when SUM(InvSalesPriorYrMTD)=0 then Convert(decimal(18,2),1)  else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='USA'
  ) AS VarianceASPMTD_FRANCHISEUSA,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='USA'
  ) AS ASPYTD_FRANCHISEUSA,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='USA'
  ) AS VarianceASPYTD_FRANCHISEUSA,
      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='USA'
  ) AS ASPDay_MEDIFASTUSA,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='USA'
  ) AS ASPMTD_MEDIFASTUSA,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='USA'
  ) AS VarianceASPMTD_MEDIFASTUSA,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='USA'
  ) AS ASPYTD_MEDIFASTUSA,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='USA'
  ) AS VarianceASPYTD_MEDIFASTUSA,
      (
    SELECT case when SUM([InvUnitsDay])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
           and Country='USA'
  ) AS ASPDay_TotalUSA,
  (
    SELECT case when SUM([InvUnitsMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL)
           and Country='USA'
  ) AS ASPMTD_TotalUSA,
 (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsMTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when Sum([InvUnitsPriorYrMTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
           and Country='USA' AND CPG <> 'MWCC'
  ) AS VarianceASPMTD_TotalUSA,
  (
    SELECT Convert(Decimal(18,2),case when SUM([InvUnitsYTD])=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
           and Country='USA'
  ) AS ASPYTD_TotalUSA,
  (
    SELECT Convert(decimal(18,2),((case when Sum([InvUnitsYTD]) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when Sum([InvUnitsPriorYrYTD])=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
           and Country='USA' AND CPG <> 'MWCC'
  ) AS VarianceASPYTD_TotalUSA,
   (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
           and Country='USA'
  ) AS VarianceUnitsMTD_FranchiseUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
           and Country='USA'
  ) AS VarianceUnitsYTD_FranchiseUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
           and Country='USA'
  ) AS VarianceUnitsMTD_MedDirectUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD])))/ NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
           and Country='USA'
  ) AS VarianceUnitsYTD_MedDirectUSA,

  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
           and Country='USA'
  ) AS VarianceUnitsMTD_TSFLUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
           and Country='USA'
  ) AS VarianceUnitsYTD_TSFLUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
           and Country='USA'
  ) AS VarianceUnitsMTD_WholesaleUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
           and Country='USA'
  ) AS VarianceUnitsYTD_WholesaleUSA,
  (
    SELECT
          isnull(Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD])) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
           and Country='USA'
  ) AS VarianceUnitsMTD_INTLUSA,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
           and Country='USA'
  ) AS VarianceUnitsYTD_INTLUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
     and Country='USA' AND CPG <> 'MWCC'
  ) AS VarianceUnitsMTD_TotalUSA,
  (
    SELECT
         Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
     and Country='USA' AND CPG <> 'MWCC'
  ) AS VarianceUnitsYTD_TotalUSA,
  
  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='USA'
  ) AS OrdersMedDirectUSA,

  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='USA'
  ) AS OrdersFranchiseUSA,
  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='USA'
  ) AS OrdersTSFLUSA,
  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='USA'
  ) AS OrdersWholesaleUSA,
  (
    SELECT isnull(SUM(InvOrdersDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='USA'
  ) AS OrdersINTLUSA,
  (
    SELECT SUM(InvOrdersDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL)
           and Country='USA'
  ) AS OrdersTotalUSA,
  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='USA'
  ) AS OrdersMTD_MedDirectUSA,
   (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='USA'
  ) AS OrdersYTD_MedDirectUSA,

  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='USA'
  ) AS OrdersMTD_FranchiseUSA,
   (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='USA'
  ) AS OrdersYTD_FranchiseUSA,
  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='USA'
  ) AS OrdersMTD_TSFLUSA,
   (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='USA'
  ) AS OrdersYTD_TSFLUSA,
  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='USA'
  ) AS OrdersMTD_WholesaleUSA,
  (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='USA'
  ) AS OrdersYTD_WholesaleUSA,
  (
    SELECT isnull(SUM(InvOrdersMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='USA'
  ) AS OrdersMTD_INTLUSA,
  (
    SELECT isnull(SUM(InvOrdersYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='USA'
  ) AS OrdersYTD_INTLUSA,
  (
    SELECT SUM(InvOrdersMTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL)
           and Country='USA'
  ) AS OrdersMTD_TotalUSA,
    (
    SELECT SUM(InvOrdersYTD)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL)
           and Country='USA'
  ) AS OrdersYTD_TotalUSA,
  
  
  
  ---------------------------------------------------------------------------------------------------------------------------------------------------
  
  
  
  
  
   (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
           and Country='USA'
  ) AS VarianceOrdersMTD_FranchiseUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
           and Country='USA'
  ) AS VarianceOrdersYTD_FranchiseUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
           and Country='USA'
  ) AS VarianceOrdersMTD_MedDirectUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD])))/ NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
           and Country='USA'
  ) AS VarianceOrdersYTD_MedDirectUSA,

  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
           and Country='USA'
  ) AS VarianceOrdersMTD_TSFLUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
           and Country='USA'
  ) AS VarianceOrdersYTD_TSFLUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
           and Country='USA'
  ) AS VarianceOrdersMTD_WholesaleUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
           and Country='USA'
  ) AS VarianceOrdersYTD_WholesaleUSA,
  (
    SELECT
          isnull(Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD])) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
           and Country='USA'
  ) AS VarianceOrdersMTD_INTLUSA,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
           and Country='USA'
  ) AS VarianceOrdersYTD_INTLUSA,
  (
    SELECT
          Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
     and Country='USA' AND CPG <> 'MWCC'
  ) AS VarianceOrdersMTD_TotalUSA,
  (
    SELECT
         Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
     and Country='USA' AND CPG <> 'MWCC'
  ) AS VarianceOrdersYTD_TotalUSA,
 --------------------------------------------------------CANADA-------------------------------------------------------------- 
   (
    SELECT isnull(SUM(InvSalesDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='CAD'
  ) AS SalesMedDirectCAD,
  (
    SELECT isnull(SUM(InvSalesDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='CAD'
  ) AS SalesFranchiseCAD,
  (
    SELECT isnull(SUM(InvSalesDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='CAD'
  ) AS SalesTSFLCAD,
  (
    SELECT isnull(SUM(InvSalesDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='CAD'
  ) AS SalesWholesaleCAD,
  (
    SELECT isnull(SUM(InvSalesDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='CAD'
  ) AS SalesINTLCAD,
  (
    SELECT isnull(SUM(InvSalesDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesDay IS NOT NULL)
          and Country='CAD'
  ) AS SalesTotalCAD,
  (
    SELECT isnull(SUM(InvUnitsDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='CAD'
  ) AS UnitsMedDirectCAD,

  (
    SELECT isnull(SUM(InvUnitsDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='CAD'
  ) AS UnitsFranchiseCAD,
  (
    SELECT isnull(SUM(InvUnitsDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='CAD'
  ) AS UnitsTSFLCAD,
  (
    SELECT isnull(SUM(InvUnitsDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='CAD'
  ) AS UnitsWholesaleCAD,
  (
    SELECT isnull(SUM(InvUnitsDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='CAD'
  ) AS UnitsINTLCAD,
  (
    SELECT isnull(SUM(InvUnitsDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsDay IS NOT NULL)
          and Country='CAD' 
  ) AS UnitsTotalCAD,
  (
    SELECT
          isnull(((SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
          and Country='CAD' 
  ) AS VarianceMTD_FranchiseCAD,
  (
    SELECT
          isnull(((SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
          and Country='CAD'
  ) AS VarianceYTD_FranchiseCAD,
  (
    SELECT
          isnull(((SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
          and Country='CAD'
  ) AS VarianceMTD_MedDirectCAD,
  (
    SELECT
          isnull(((SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
          and Country='CAD'
  ) AS VarianceYTD_MedDirectCAD,

  (
    SELECT
          isnull(((SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
          and Country='CAD'
  ) AS VarianceMTD_TSFLCAD,
  (
    SELECT
          isnull(((SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
          and Country='CAD'
  ) AS VarianceYTD_TSFLCAD,
  (
    SELECT
          isnull(((SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
          and Country='CAD'
  ) AS VarianceMTD_WholesaleCAD,
  (
    SELECT
          isnull(((SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
          and Country='CAD'
  ) AS VarianceYTD_WholesaleCAD,
  (
    SELECT
          isnull(((SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
          and Country='CAD'
  ) AS VarianceMTD_INTLCAD,
  (
    SELECT
          isnull(((SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
          and Country='CAD'
  ) AS VarianceYTD_INTLCAD,
  (
    SELECT
          isnull(((SUM([InvSalesMTD]) - SUM([InvSalesPriorYrMTD])) / NULLIF(SUM([InvSalesPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
    and Country='CAD' AND CPG <> 'MWCC'
  ) AS VarianceMTD_TotalCAD,
  (
    SELECT
          isnull(((SUM([InvSalesYTD]) - SUM([InvSalesPriorYrYTD])) / NULLIF(SUM([InvSalesPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
   and Country='CAD' AND CPG <> 'MWCC'
  ) AS VarianceYTD_TotalCAD,
  (
    SELECT isnull(SUM(InvSalesMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='CAD'
  ) AS SalesMTD_MedDirectCAD,
  (
    SELECT isnull(SUM(InvSalesYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='CAD'
  ) AS SalesYTD_MedDirectCAD,

  (
    SELECT isnull(SUM(InvSalesMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='CAD'
  ) AS SalesMTD_FranchiseCAD,
  (
    SELECT isnull(SUM(InvSalesYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='CAD'
  ) AS SalesYTD_FranchiseCAD,
  (
    SELECT isnull(SUM(InvSalesMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='CAD'
  ) AS SalesMTD_TSFLCAD,
  (
    SELECT isnull(SUM(InvSalesYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='CAD'
  ) AS SalesYTD_TSFLCAD,
  (
    SELECT isnull(SUM(InvSalesMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='CAD'
  ) AS SalesMTD_WholesaleCAD,
  (
    SELECT isnull(SUM(InvSalesYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='CAD'
  ) AS SalesYTD_WholesaleCAD,
  (
    SELECT isnull(SUM(InvSalesMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='CAD'
  ) AS SalesMTD_INTLCAD,
  (
    SELECT isnull(SUM(InvSalesYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='CAD'
  ) AS SalesYTD_INTLCAD,
  (
    SELECT isnull(SUM(InvSalesMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL)
          and Country='CAD'
  ) AS SalesMTD_TotalCAD,
  (
    SELECT isnull(SUM(InvSalesYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesYTD IS NOT NULL)
           and Country='CAD'
  ) AS SalesYTD_TotalCAD,
  (
    SELECT isnull(SUM(InvUnitsMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='CAD'
  ) AS UnitsMTD_MedDirectCAD,
  (
    SELECT isnull(SUM(InvUnitsYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
          and Country='CAD'
  ) AS UnitsYTD_MedDirectCAD,

  (
    SELECT isnull(SUM(InvUnitsMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='CAD'
  ) AS UnitsMTD_FranchiseCAD,
  (
    SELECT isnull(SUM(InvUnitsYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
          and Country='CAD'
  ) AS UnitsYTD_FranchiseCAD,
  (
    SELECT isnull(SUM(InvUnitsMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='CAD'
  ) AS UnitsMTD_TSFLCAD,
  (
    SELECT isnull(SUM(InvUnitsYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'TSFL')
          and Country='CAD'
  ) AS UnitsYTD_TSFLCAD,
  (
    SELECT isnull(SUM(InvUnitsMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='CAD'
  ) AS UnitsMTD_WholesaleCAD,
  (
    SELECT isnull(SUM(InvUnitsYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
          and Country='CAD'
  ) AS UnitsYTD_WholesaleCAD,
  (
    SELECT isnull(SUM(InvUnitsMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='CAD'
  ) AS UnitsMTD_INTLCAD,
   (
    SELECT isnull(SUM(InvUnitsYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='CAD'
  ) AS UnitsYTD_INTLCAD,
  (
    SELECT isnull(SUM(InvUnitsMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsMTD IS NOT NULL)
          and Country='CAD'
  ) AS UnitsMTD_TotalCAD,
   (
    SELECT isnull(SUM(InvUnitsYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvUnitsYTD IS NOT NULL)
          and Country='CAD' 
  ) AS UnitsYTD_TotalCAD,
  
  
  
 ---------------------------------------------------------------------------------------------------- 
  
  
  (
    SELECT case when isnull(SUM([InvUnitsDay]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
          and Country='CAD'
  ) AS ASPDay_INTLCAD,
  (
    SELECT case when isnull(SUM([InvUnitsMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='CAD'
  ) AS ASPMTD_INTLCAD,
 (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsMTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='CAD'
  ) AS VarianceASPMTD_INTLCAD,
  (
    SELECT Convert(Decimal(18,2),case when isnull(SUM([InvUnitsYTD]),0)=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='CAD'
  ) AS ASPYTD_INTLCAD,
  (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsYTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='CAD'
  ) AS VarianceASPYTD_INTLCAD,
      (
    SELECT case when isnull(SUM([InvUnitsDay]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='CAD'
  ) AS ASPDay_TSFLCAD,
  (
    SELECT case when isnull(SUM([InvUnitsMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='CAD'
  ) AS ASPMTD_TSFLCAD,
 (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsMTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='CAD'
  ) AS VarianceASPMTD_TSFLCAD,
  (
    SELECT Convert(Decimal(18,2),case when isnull(SUM([InvUnitsYTD]),0)=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='CAD'
  ) AS ASPYTD_TSFLCAD,
  (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsYTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='CAD'
  ) AS VarianceASPYTD_TSFLCAD,


    (
    SELECT case when isnull(SUM([InvUnitsDay]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='CAD'
  ) AS ASPDay_DOCTORSCAD,
  (
    SELECT case when isnull(SUM([InvUnitsMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='CAD'
  ) AS ASPMTD_DOCTORSCAD,
 (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsMTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='CAD'
  ) AS VarianceASPMTD_DOCTORSCAD,
  (
    SELECT Convert(Decimal(18,2),case when isnull(SUM([InvUnitsYTD]),0)=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='CAD'
  ) AS ASPYTD_DOCTORSCAD,
  (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsYTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='CAD'
  ) AS VarianceASPYTD_DOCTORSCAD,
      (
    SELECT case when isnull(SUM([InvUnitsDay]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='CAD'
  ) AS ASPDay_FRANCHISECAD,
  (
    SELECT case when isnull(SUM([InvUnitsMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='CAD'
  ) AS ASPMTD_FRANCHISECAD,
 (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsMTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='CAD'
  ) AS VarianceASPMTD_FRANCHISECAD,
  (
    SELECT Convert(Decimal(18,2),case when isnull(SUM([InvUnitsYTD]),0)=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='CAD'
  ) AS ASPYTD_FRANCHISECAD,
  (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsYTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='CAD'
  ) AS VarianceASPYTD_FRANCHISECAD,
      (
    SELECT case when isnull(SUM([InvUnitsDay]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='CAD'
  ) AS ASPDay_MEDIFASTCAD,
  (
    SELECT case when isnull(SUM([InvUnitsMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='CAD'
  ) AS ASPMTD_MEDIFASTCAD,
 (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsMTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='CAD'
  ) AS VarianceASPMTD_MEDIFASTCAD,
  (
    SELECT Convert(Decimal(18,2),case when isnull(SUM([InvUnitsYTD]),0)=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='CAD'
  ) AS ASPYTD_MEDIFASTCAD,
  (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsYTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='CAD'
  ) AS VarianceASPYTD_MEDIFASTCAD,
      (
    SELECT case when isnull(SUM([InvUnitsDay]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else  Convert(Decimal(18,2),(isnull(SUM(InvSalesDay),0)/SUM([InvUnitsDay]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
           and Country='CAD'
  ) AS ASPDay_TotalCAD,
  (
    SELECT case when isnull(SUM([InvUnitsMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else convert(decimal(18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL)
           and Country='CAD'
  ) AS ASPMTD_TotalCAD,
 (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsMTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesMTD),0)/SUM([InvUnitsMTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrMTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrMTD),0)/SUM([InvUnitsPriorYrMTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
           and Country='CAD' AND CPG <> 'MWCC'
  ) AS VarianceASPMTD_TotalCAD,
  (
    SELECT Convert(Decimal(18,2),case when isnull(SUM([InvUnitsYTD]),0)=0 then 0 else (isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD])) end) 
  
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
           and Country='CAD'
  ) AS ASPYTD_TotalCAD,
  (
    SELECT Convert(decimal(18,2),((case when isnull(Sum([InvUnitsYTD]),0) =Convert(decimal(18,2),0) then Convert(decimal(18,2),0)else Convert(decimal (18,2),(isnull(SUM(InvSalesYTD),0)/SUM([InvUnitsYTD]))) end -
    case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),0) else Convert(decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD]))) end)
    /case when isnull(Sum([InvUnitsPriorYrYTD]),0)=Convert(decimal(18,2),0) then Convert(decimal(18,2),1) else Convert(Decimal(18,2),(isnull(SUM(InvSalesPriorYrYTD),0)/SUM([InvUnitsPriorYrYTD])))end )*100)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvSalesMTD IS NOT NULL) 
           and Country='CAD' AND CPG <> 'MWCC'
  ) AS VarianceASPYTD_TotalCAD,
   (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
           and Country='CAD'
  ) AS VarianceUnitsMTD_FranchiseCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
           and Country='CAD'
  ) AS VarianceUnitsYTD_FranchiseCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
           and Country='CAD'
  ) AS VarianceUnitsMTD_MedDirectCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD])))/ NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
           and Country='CAD'
  ) AS VarianceUnitsYTD_MedDirectCAD,

  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
           and Country='CAD'
  ) AS VarianceUnitsMTD_TSFLCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
           and Country='CAD'
  ) AS VarianceUnitsYTD_TSFLCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
           and Country='CAD'
  ) AS VarianceUnitsMTD_WholesaleCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
           and Country='CAD'
  ) AS VarianceUnitsYTD_WholesaleCAD,
  (
    SELECT
          isnull(Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD])) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
           and Country='CAD'
  ) AS VarianceUnitsMTD_INTLCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
           and Country='CAD'
  ) AS VarianceUnitsYTD_INTLCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvUnitsMTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrMTD]))) / NULLIF(SUM([InvUnitsPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
     and Country='CAD' AND CPG <> 'MWCC'
  ) AS VarianceUnitsMTD_TotalCAD,
  (
    SELECT
         isnull((Convert(Decimal(18,2),(SUM([InvUnitsYTD])) - Convert(Decimal(18,2),SUM([InvUnitsPriorYrYTD]))) / NULLIF(SUM([InvUnitsPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
     and Country='CAD' AND CPG <> 'MWCC'
  ) AS VarianceUnitsYTD_TotalCAD,
  
  (
    SELECT isnull(SUM(InvOrdersDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='CAD'
  ) AS OrdersMedDirectCAD,

  (
    SELECT isnull(SUM(InvOrdersDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='CAD'
  ) AS OrdersFranchiseCAD,
  (
    SELECT isnull(SUM(InvOrdersDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='CAD'
  ) AS OrdersTSFLCAD,
  (
    SELECT isnull(SUM(InvOrdersDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='CAD'
  ) AS OrdersWholesaleCAD,
  (
    SELECT isnull(SUM(InvOrdersDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='CAD'
  ) AS OrdersINTLCAD,
  (
    SELECT isnull(SUM(InvOrdersDay),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersDay IS NOT NULL)
           and Country='CAD'
  ) AS OrdersTotalCAD,
  (
    SELECT isnull(SUM(InvOrdersMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='CAD'
  ) AS OrdersMTD_MedDirectCAD,
   (
    SELECT isnull(SUM(InvOrdersYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'MEDIFAST')
           and Country='CAD'
  ) AS OrdersYTD_MedDirectCAD,

  (
    SELECT isnull(SUM(InvOrdersMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='CAD'
  ) AS OrdersMTD_FranchiseCAD,
   (
    SELECT isnull(SUM(InvOrdersYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'FRANCHISE')
           and Country='CAD'
  ) AS OrdersYTD_FranchiseCAD,
  (
    SELECT isnull(SUM(InvOrdersMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='CAD'
  ) AS OrdersMTD_TSFLCAD,
   (
    SELECT isnull(SUM(InvOrdersYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'TSFL')
           and Country='CAD'
  ) AS OrdersYTD_TSFLCAD,
  (
    SELECT isnull(SUM(InvOrdersMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='CAD'
  ) AS OrdersMTD_WholesaleCAD,
  (
    SELECT isnull(SUM(InvOrdersYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'DOCTORS')
           and Country='CAD'
  ) AS OrdersYTD_WholesaleCAD,
  (
    SELECT isnull(SUM(InvOrdersMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='CAD'
  ) AS OrdersMTD_INTLCAD,
  (
    SELECT isnull(SUM(InvOrdersYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL) AND
          (CPG = 'INTL')
           and Country='CAD'
  ) AS OrdersYTD_INTLCAD,
  (
    SELECT isnull(SUM(InvOrdersMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersMTD IS NOT NULL)
           and Country='CAD'
  ) AS OrdersMTD_TotalCAD,
    (
    SELECT isnull(SUM(InvOrdersYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvOrdersYTD IS NOT NULL)
           and Country='CAD'
  ) AS OrdersYTD_TotalCAD,
   (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
           and Country='CAD'
  ) AS VarianceOrdersMTD_FranchiseCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'FRANCHISE')
           and Country='CAD'
  ) AS VarianceOrdersYTD_FranchiseCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
           and Country='CAD'
  ) AS VarianceOrdersMTD_MedDirectCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD])))/ NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'MEDIFAST')
           and Country='CAD'
  ) AS VarianceOrdersYTD_MedDirectCAD,

  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
           and Country='CAD'
  ) AS VarianceOrdersMTD_TSFLCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'TSFL')
           and Country='CAD'
  ) AS VarianceOrdersYTD_TSFLCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
           and Country='CAD'
  ) AS VarianceOrdersMTD_WholesaleCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'DOCTORS')
           and Country='CAD'
  ) AS VarianceOrdersYTD_WholesaleCAD,
  (
    SELECT
          isnull(Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD])) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
           and Country='CAD'
  ) AS VarianceOrdersMTD_INTLCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          ([CPG] = 'INTL')
           and Country='CAD'
  ) AS VarianceOrdersYTD_INTLCAD,
  (
    SELECT
          isnull((Convert(Decimal(18,2),(SUM([InvOrdersMTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrMTD]))) / NULLIF(SUM([InvOrdersPriorYrMTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
     and Country='CAD' AND CPG <> 'MWCC'
  ) AS VarianceOrdersMTD_TotalCAD,
  (
    SELECT
         isnull((Convert(Decimal(18,2),(SUM([InvOrdersYTD])) - Convert(Decimal(18,2),SUM([InvOrdersPriorYrYTD]))) / NULLIF(SUM([InvOrdersPriorYrYTD]),0) * 100),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Channel]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
     and Country='CAD' AND CPG <> 'MWCC'
  ) AS VarianceOrdersYTD_TotalCAD,
  (
    SELECT
      SUM(OnHandAmtDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Inventory]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvPostClass = 'FG') AND (OnHandAmtDay IS NOT NULL)
          and [InvLocationType] <> 'MWCC'
  ) AS InvFG,
  (
    SELECT
      SUM(OnHandAmtDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Inventory]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvPostClass = 'RM') AND (OnHandAmtDay IS NOT NULL)
           and [InvLocationType] <> 'MWCC'
  ) AS InvRM,
  (
    SELECT
      SUM(OnHandAmtDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Inventory]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvPostClass = 'PM') AND (OnHandAmtDay IS NOT NULL)
           and [InvLocationType] <> 'MWCC'
  ) AS InvPM,
  (
    SELECT
      SUM(OnHandAmtDay)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Inventory]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (OnHandAmtDay IS NOT NULL) and [InvLocationType] <> 'MWCC'
  ) AS InvTotal,
  (
    SELECT
      ISNULL(SUM(OnHandQtyBaseDay) / (NULLIF(SUM(ForecastQtyCurrentMonth),0) / 28),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Inventory]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)) AND
          (InvPostClass = 'FG')
  ) AS FG_DaysOnHand,
  
  
  
  
  
  (
    SELECT
      ISNULL(SUM(UnitProductionToday),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Production]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
  ) AS UnitProductionToday,
  (
    SELECT
      ISNULL(SUM(UnitProductionMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Production]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
  ) AS UnitProductionMTD,
  (
    SELECT
      ISNULL(SUM(UnitProductionYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Production]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
  ) AS UnitProductionYTD,
  (
    SELECT
      ISNULL(SUM(UnitProductionPriorYrMTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Production]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
  ) AS UnitProductionPriorYrMTD,
  (
    SELECT
      ISNULL(SUM(UnitProductionPriorYrYTD),0)
    FROM [NAV_ETL].dbo.[Snapshot$NAV_Production]
    WHERE (RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))
  ) AS UnitProductionPriorYrYTD
  ,ISNULL(Fun.TSFLSponsoringDay,0) AS TSFLSponsoringDay
  ,ISNULL(Fun.TSFLSponsoringMTD,0) AS TSFLSponsoringMTD
  ,ISNULL(Fun.TSFLSponsoringPriorMTD,0) AS TSFLSponsoringPriorMTD
  ,ISNULL(Fun.TSFLSponsoringYTD,0) AS TSFLSponsoringYTD
  ,ISNULL(Fun.TSFLSponsoringPriorYTD,0) AS TSFLSponsoringPriorYTD
  ,ISNULL(Fun.TSFLClientAcquisitionDay,0) AS TSFLClientAcquisitionDay
  ,ISNULL(Fun.TSFLClientAcquisitionMTD,0) AS TSFLClientAcquisitionMTD
  ,ISNULL(Fun.TSFLClientAcquisitionPriorMTD,0) AS TSFLClientAcquisitionPriorMTD
  ,ISNULL(Fun.TSFLClientAcquisitionYTD,0) AS TSFLClientAcquisitionYTD
  ,ISNULL(Fun.TSFLClientAcquisitionPriorYTD,0) AS TSFLClientAcquisitionPriorYTD
  ,ISNULL(Fun.ActiveHealthCoachesDay,0) AS ActiveHealthCoachesDay
  ,ISNULL(Fun.ActiveHealthCoachesMTD,0) AS ActiveHealthCoachesMTD
  ,ISNULL(Fun.ActiveHealthCoachesPriorMTD,0) AS ActiveHealthCoachesPriorMTD
  ,ISNULL(Fun.TSFLWebCoachRqstDay,0) AS TSFLWebCoachRqstDay
  ,ISNULL(Fun.TSFLWebCoachRqstMTD,0) AS TSFLWebCoachRqstMTD
  ,ISNULL(Fun.TSFLWebCoachRqstPriorMTD,0) AS TSFLWebCoachRqstPriorMTD
  ,ISNULL(Fun.TSFLWebCoachRqstYTD,0) AS TSFLWebCoachRqstYTD
  ,ISNULL(Fun.TSFLWebCoachRqstPriorYTD,0) AS TSFLWebCoachRqstPriorYTD
  ,ISNULL(Fun.TSFLWebVisitorsDay,0) AS TSFLWebVisitorsDay
  ,ISNULL(Fun.TSFLWebVisitorsMTD,0) AS TSFLWebVisitorsMTD
  ,ISNULL(Fun.TSFLWebVisitorsPriorMTD,0) AS TSFLWebVisitorsPriorMTD
  ,ISNULL(Fun.TSFLWebVisitorsYTD,0) AS TSFLWebVisitorsYTD
  ,ISNULL(Fun.TSFLWebVisitorsPriorYTD,0) AS TSFLWebVisitorsPriorYTD
  ,ISNULL(Fun.TTSVisitorsDay,0) AS TTSVisitorsDay
  ,ISNULL(Fun.TTSVisitorsMTD,0) AS TTSVisitorsMTD
  ,ISNULL(Fun.TTSVisitorsPriorMTD,0) AS TTSVisitorsPriorMTD
  ,ISNULL(Fun.TTSVisitorsYTD,0) AS TTSVisitorsYTD
  ,ISNULL(Fun.TTSVisitorsPriorYTD,0) AS TTSVisitorsPriorYTD
  ,ISNULL(Fun.TTSnewVisitsDay,0) AS TTSnewVisitsDay
  ,ISNULL(Fun.TTSnewVisitsMTD,0) AS TTSnewVisitsMTD
  ,ISNULL(Fun.TTSnewVisitsPriorMTD,0) AS TTSnewVisitsPriorMTD
  ,ISNULL(Fun.TTSnewVisitsYTD,0) AS TTSnewVisitsYTD
  ,ISNULL(Fun.TTSnewVisitsPriorYTD,0) AS TTSnewVisitsPriorYTD
  ,ISNULL(Fun.MedClientAcquisitionDay,0) AS MedClientAcquisitionDay
  ,ISNULL(Fun.MedClientAcquisitionMTD,0) AS MedClientAcquisitionMTD
  ,ISNULL(Fun.MedClientAcquisitionPriorMTD,0) AS MedClientAcquisitionPriorMTD
  ,ISNULL(Fun.MedClientAcquisitionYTD,0) AS MedClientAcquisitionYTD
  ,ISNULL(Fun.MedClientAcquisitionPriorYTD,0) AS MedClientAcquisitionPriorYTD
   ,ISNULL(Fun.MedDirWebVisitorsDay,0) AS MedDirWebVisitorsDay
  ,ISNULL(Fun.MedDirWebVisitorsMTD,0) AS MedDirWebVisitorsMTD
  ,ISNULL(Fun.MedDirWebVisitorsPriorMTD,0) AS MedDirWebVisitorsPriorMTD
  ,ISNULL(Fun.MedDirWebVisitorsYTD,0) AS MedDirWebVisitorsYTD
  ,ISNULL(Fun.MedDirWebVisitorsPriorYTD,0) AS MedDirWebVisitorsPriorYTD
  ,ISNULL(Fun.MedDirProcNavInvcsDay,0) AS MedDirProcNavInvcsDay
  ,ISNULL(Fun.MedDirProcNavInvcsMTD,0) AS MedDirProcNavInvcsMTD
  ,ISNULL(Fun.MedDirProcNavInvcsPriorMTD,0) AS MedDirProcNavInvcsPriorMTD
  ,ISNULL(Fun.MedDirProcNavInvcsYTD,0) AS MedDirProcNavInvcsYTD
  ,ISNULL(Fun.MedDirProcNavInvcsPriorYTD,0) AS MedDirProcNavInvcsPriorYTD
  ,ISNULL(Fun.WholesaleWebVisitorsDay,0) AS WholesaleWebVisitorsDay
  ,ISNULL(Fun.WholesaleWebVisitorsMTD,0) AS WholesaleWebVisitorsMTD
  ,ISNULL(Fun.WholesaleWebVisitorsPriorMTD,0) AS WholesaleWebVisitorsPriorMTD
  ,ISNULL(Fun.WholesaleWebVisitorsYTD,0) AS WholesaleWebVisitorsYTD
  ,ISNULL(Fun.WholesaleWebVisitorsPriorYTD,0) AS WholesaleWebVisitorsPriorYTD
FROM [NAV_ETL].dbo.[Snapshot$NAV_Funnel] Fun WHERE (Fun.RunDate = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME))