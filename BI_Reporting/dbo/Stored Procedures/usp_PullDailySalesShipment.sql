﻿











-- =============================================
-- Author:		Kalpesh Patel
-- Create date: 07/09/2013
-- Description:	Daily Shipment Report
-- =============================================
CREATE PROCEDURE [dbo].[usp_PullDailySalesShipment]
	-- Add the parameters for the stored procedure here
	@DistCenter varchar(50),
	@Dept varchar(150),
	@StartDate datetime, 
	@EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @StartValue varchar(10)
	declare @EndValue varchar(10)
	
	--set @EndDate=dateadd(d,1,@EndDate)

	set @StartValue=cast(year(@StartDate) as varchar(4)) + '-' + right('0' + cast(month(@StartDate) as varchar(2)),2) + '-' + right('0' + cast(day(@StartDate) as varchar(2)),2)
	set @EndValue=cast(year(@EndDate) as varchar(4)) + '-' + right('0' + cast(month(@EndDate) as varchar(2)),2) + '-' + right('0' + cast(day(@EndDate) as varchar(2)),2)

	print @StartValue
	Print @EndValue

--	SELECT     flexdata.keyValue, count(packages.pid) as PCount--,shipment_header.ship_date--, shipment_header.[sid]--, shipment_header.ship_date
--	FROM         flexdata INNER JOIN
--						  packages ON flexdata.sid = packages.sid INNER JOIN
--						  shipment_header ON packages.sid = shipment_header.sid
--	WHERE     (flexdata.keyName = 'FLEX3') and (shipment_header.ship_date >= @StartValue and shipment_header.ship_date<@EndValue)
--	group by keyvalue
--	order by keyvalue


	 SELECT     case shipment_header.keydata1
					when 1 then 'MDCWMS'
					when 2 then 'TDCWMS'
				end as dc
				, reference3 as dept
				, count(distinct [ui_sales_order_no]) as sCount
				, reference5 as source
	 FROM      FLAGSHIP_WMS_ETL.dbo.shipment_header
						
	WHERE    
				shipment_header.[status]='UPLD' and
				(shipment_header.ship_date >= @StartValue and shipment_header.ship_date<=@EndValue) and 
			
				reference3 in (select * from dbo.ufn_SPLIT(@Dept,',')) 
				--shipment_header.keydata1 in (select * from ufn_SPLIT(@DistCenter, ','))
			

	group by shipment_header.keydata1, reference3,reference5
	order by shipment_header.keydata1

END













