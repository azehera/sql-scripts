﻿
/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/10/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CO_APPLICANT] 		     Select   
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  


=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/

CREATE  procedure [dbo].[USP_XL_073_Weekly_New CoApps Added Since Last Report_02] as 
set nocount on ;


---- Delete for same day if we need to run again --------
Delete [BI_Reporting].dbo.[XL_073_CO_APP_ID]
where DATE=CONVERT(varchar(10),Getdate()-1,121)


delete  a
from [BI_Reporting].dbo.[XL_073_CO_APP_ID] a
join [BI_Reporting].[dbo].[calendar] b
on a.Date=b.CalendarDate
where [DayOfWeekName]<>'Sunday'

-------------------------Pull All New CoApps Added Since Last Report------------------
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select 
	[CUSTOMER_NUMBER] as [ID]
	,B.[FIRST_NAME] as [Coach First Name]
	,B.[LAST_NAME] as [Coach Last Name]
	,[CO_APP_ID]
	,A.[FIRST_NAME] as [CoApp First Name]
	,A.[LAST_NAME] as [CoApp Last Name]
	into #A From [ODYSSEY_ETL].[dbo].[ODYSSEY_CO_APPLICANT] A
		Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] B
			on A.[CUSTOMER_ID] = B.[CUSTOMER_ID]
	Where [IS_OBSOLETE] is null
		and [CO_APP_ID] > (Select MAX([CO_APP_ID]) from [BI_Reporting].dbo.[XL_073_CO_APP_ID])
		Order by [CO_APP_ID]

IF ((Select MAX([CO_APP_ID]) from #A)  is not null) 
Begin
	Insert into [BI_Reporting].dbo.[XL_073_CO_APP_ID] 		
	Select  
     MAX([CO_APP_ID]) as [CO_APP_ID],
     Convert(varchar(10),GETDATE()-1,121) as Date 
      from #A 
 End     
Select * from #A





