﻿

/*
--==================================================================================
Author:        Daniel Dagnachew
Create date: 02/17/2016
-----------------------------dbo.BI_NAV_Product_SalesCube-------------------
Create Feb/March Reactivation Campaign Sales Report
--==================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------
[mdm].[BICubes]           [dbo].FactSales_MDM                      Select ---Linked Server mdm 
[Ecomm_ETL]   [dbo].[v_ORDER_HEADER]                   Select
[Ecomm_ETL]   [dbo].v_user_account                     Select 
[NAV_ETL]                 [dbo].[Jason Pharm$Sales Invoice Header] Select 
[NAV_ETL]                 [dbo].[MEDIFAST_ORDERLINE_ASSOCIATION]   Select
[NAV_ETL]                 [dbo].[Jason Pharm$Sales Invoice Header] Select
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE Procedure [dbo].[USP_Reactivation_March & February 2016] 
as 

------Pulling Customer's information in the specified time range for the activation Program
--IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A

--SELECT  DISTINCT 
--        UA.USA_CUST_NBR, 
--        UA.USA_EMAIL,
--        --IH.[External Document No_] [ORDER ID],
--		OH.[ORH_SOURCE_NBR] [ORDER ID],
--        OH.[ORH_PROMOTION_CODE],
--		IH.[No_],
--        CAST(OH.[ORH_SYS_TIMESTAMP] AS DATE) [Order Date],
--        OH.[ORH_TOTAL_AMT] [ORDER_TOTAL]
--INTO    #A     
--FROM    [Ecomm_ETL].[dbo].[v_ORDER_HEADER] OH WITH ( NOLOCK )
--        --INNER JOIN [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] IH ON IH.[External Document No_] = OH.ORH_SOURCE_NBR
--		INNER JOIN [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Header] IH ON IH.[External Document No_] collate DATABASE_DEFAULT = OH.ORH_SOURCE_NBR
--        LEFT JOIN [Ecomm_ETL].[dbo].v_user_account UA WITH ( NOLOCK ) ON UA.usa_id = OH.orh_created_for
----WHERE   OH.[ORH_PROMOTION_CODE] IN ( 'BESTYEAR20','BESTYEARCC' )-- Promo codes changed as of March 20, 2016
----WHERE   OH.[ORH_PROMOTION_CODE] IN ( 'BESTYEAR50','BESTYEAR50CC' )-- Changed by Daniel Dagnachew based on Request from Jennifer Cruise(Reference Number#21481533 )
--WHERE   OH.[ORH_PROMOTION_CODE] IN ('BESTYEAR20','BESTYEARCC', 'BESTYEAR50','BESTYR50CC')-- Changed by Daniel Dagnachew based on Request from Jennifer Cruise(email March 22nd,2016 11:14 am)
--        AND IH.[Order Date] BETWEEN '02-28-2016'
--                            AND     '03-31-2016'
--ORDER BY UA.USA_CUST_NBR

----------Pulling GINID and Order Type
--IF object_id('Tempdb..#YY') Is Not Null DROP TABLE #YY
--Select distinct GINID,IH.[No_] ,M.[OrderType]
--		INTO #YY
--       from #A A
--       join [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Header]  IH
--       on  IH.[No_] collate database_default = A.[No_]
--       left join [mdm].[BICubes].[dbo].FactSales_MDM  M
--       ON M.DocumentNo = IH.[No_] 

------ Kit's Sales Amount      
--IF object_id('Tempdb..#XX') Is Not Null DROP TABLE #XX
--Select A.[No_], OLA.MOA_SALE_PRICE_AMT
--into #XX
--from #A A
--left JOIN [NAV_ETL].dbo.MEDIFAST_ORDERLINE_ASSOCIATION OLA ON  OLA.MOA_ORDER_NBR =  A.[ORDER ID]
--AND [MOA_ASSOC_TYPE] = 'kit'
--WHERE OLA.MOA_ASSOC_SKU IN ('74016')


------ Last order date, last order amount and last order type
--IF object_id('Tempdb..#ZZ') Is Not Null DROP TABLE #ZZ

--;With Cte as (
--               SELECT IH.[Sell-to Customer No_]
--                      --,[Amount Including VAT]
--                        ,MAX(IH.[Posting Date]) [Posting Date]
--              FROM [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Header] IH 
--              WHERE IH.[Posting Date] < '02-29-2016'
--              and IH.[Sell-to Customer No_] in (Select distinct USA_CUST_NBR from #A)
--              AND [Customer Posting Group] = 'MEDIFAST'
--              GROUP BY IH.[Sell-to Customer No_])

--       Select DISTINCT M.GINID,C.[Sell-to Customer No_],[No_] ,M.[OrderType],C.[Posting Date][Last Order Date Prior to 2/28/16],IH.[Amount Authorized] [Prior Order Total]
--       INTO #ZZ
--       from Cte C
--       join [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Header]  IH
--       on C.[Posting Date] = IH.[Posting Date] and C.[Sell-to Customer No_] = IH.[Sell-to Customer No_]
--       join [mdm].[BICubes].[dbo].FactSales_MDM  M
--       ON M.DocumentNo = IH.[No_]
       

--SELECT DISTINCT Z.GINID,
--       A.USA_CUST_NBR [CUSTOMER NUMBER]
--      ,A.USA_EMAIL [Email Address]
--      ,A.[ORDER ID]
--      ,A.[Order Date]
--      ,A.[ORDER_TOTAL]
--	  ,X.MOA_SALE_PRICE_AMT [Amount for SKU# 74016]
--	  ,Y.[OrderType] 
--      ,Z.[Last Order Date Prior to 2/28/16]
--      ,Z.[Prior Order Total]
--      ,Z.[OrderType] [LAST ORDER TYPE Prior to 2/28/16]
--FROM #A A 
--left JOIN #YY Y ON Y.[No_] = A.[No_] 
--INNER JOIN #XX X ON X.[No_] = A.[No_] 
--LEFT JOIN #ZZ Z ON A.USA_CUST_NBR = Z.[Sell-to Customer No_]
-- order by A.[Order Date]

  IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A

SELECT  DISTINCT 
        GINID,
        UA.USA_CUST_NBR, 
        UA.USA_EMAIL,
		OH.[ORH_ID],
		OH.[ORH_SOURCE_NBR] [ORDER ID],
        OH.[ORH_PROMOTION_CODE],
		
		SK.Total_Sku_Amount, 
        CAST(OH.[ORH_SYS_TIMESTAMP] AS DATE) [Order Date],
        OH.[ORH_TOTAL_AMT] [ORDER_TOTAL],
		Case when [ORH_SOURCE_CD] in ('CON','WEB') Then 'ONDEMAND'
		     When [ORH_SOURCE_CD] in ('AUT') Then 'AUT0'
			 END AS ORDER_TYPE
INTO    #A     
FROM    [Ecomm_ETL].[dbo].[v_ORDER_HEADER] OH WITH ( NOLOCK )
        LEFT JOIN [Ecomm_ETL].[dbo].v_user_account UA WITH ( NOLOCK ) ON UA.usa_id = OH.orh_created_for
		Left join [mdm].[BICubes].[dbo].FactSales_MDM  M
		on M.[SelltoCustomerID] = UA.USA_CUST_NBR
		LEFT JOIN (SELECT MOA_ORDER_NBR , OLA.MOA_SALE_PRICE_AMT Total_Sku_Amount 
		           FROM [NAV_ETL].dbo.MEDIFAST_ORDERLINE_ASSOCIATION OLA
				   WHERE OLA.MOA_ASSOC_SKU IN ('74016')
				   AND [MOA_ASSOC_TYPE] = 'kit' ) SK
        ON SK.MOA_ORDER_NBR collate DATABASE_DEFAULT = OH.[ORH_SOURCE_NBR]
WHERE   OH.[ORH_PROMOTION_CODE] IN ('BESTYEAR20','BESTYEARCC', 'BESTYEAR50','BESTYR50CC')-- Changed by Daniel Dagnachew based on Request from Jennifer Cruise(email March 22nd,2016 11:14 am)
        AND CAST(OH.[ORH_SYS_TIMESTAMP] AS DATE) BETWEEN '02-28-2016' AND '03-31-2016'
ORDER BY GINID

IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
;WITH CTE AS (SELECT GINID,MAX([Posting Date]) [Last Order Date Prior to 2/28/16]
             FROM [mdm].[BICubes].[dbo].FactSales_MDM 
			 WHERE [Posting Date] < '02/29/2016'
			 and GINID IN (SELECT DISTINCT GINID from #A )
             GROUP BY GINID
             )
		Select distinct c.GINID,DocumentNo,C.[Last Order Date Prior to 2/28/16],sum([Amount Including VAT]) Total,[OrderType]
		into #B
		FROM Cte C 
		join [mdm].[BICubes].[dbo].FactSales_MDM  M
		on C.GINID = M.GINID AND cast(C.[Last Order Date Prior to 2/28/16] as date) = cast(M.[Posting Date] as date)
		group by c.GINID,DocumentNo,C.[Last Order Date Prior to 2/28/16],[OrderType]
		order by GINID



SELECT A.GINID,
       A.USA_CUST_NBR [CUSTOMER_NUMBER],
	   A.USA_EMAIL [Email_Address],
	   A.[ORDER ID] [ORDER_ID],
	   A.[Order Date] [Order_Date],
	   A.[ORDER_TOTAL],
	   A.Total_Sku_Amount [Amount_for_SKU__74016],
	   A.ORDER_TYPE,
	   B.[Last Order Date Prior to 2/28/16],
	   B.Total [Prior_Order_Total],
	   B.[OrderType] [LAST_ORDER_TYPE_Prior_to_2_28_16]

From #A A
full outer join #B B
on A.GINID = B.GINID
order by [Order_Date]