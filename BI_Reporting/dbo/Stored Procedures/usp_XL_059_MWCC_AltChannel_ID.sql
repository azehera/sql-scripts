﻿

/*
--==============================================================================================================================
Author:       Luc Emond
Create date: 02/05/2014
--------------------------MWCC Alt Channel--------------------
Provide Monthly MWCC Alt Channel info
================================================================================================================================  
REFERENCES
Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------------------------------------------------------
BI_SSAS_Cubes           FactSales
Book4Time_ETL			B4T_customer						      Select  
Book4Time_ETL			B4T_location						      Select 
Book4Time_ETL			B4T_Person							      Select 
=================================================================================================================================
REVISION LOG
Date              Name                       Change
--------------------------------------------------------------------------------------------------------------------------------
02-28-2014     Luc Emond             added transaction Id and transaction date
06-12-2014     Luc Emond	         Added Group Id, Group Name, Flavor Of Home, Negative Commission, and a New Total
================================================================================================================================
NOTES:
--------------------------------------------------------------------------------------------------------------------------------
================================================================================================================================
*/

CREATE PROCEDURE [dbo].[usp_XL_059_MWCC_AltChannel_ID] as
set nocount on; 

IF object_id('Tempdb..#Sales') Is Not Null DROP TABLE #Sales
SELECT 
 [CustomerPostingGroup]
,[SelltoCustomerID]
,b.group_id ---06-12-2014 Luc Emond
,[DocumentNo]
,ROW_NUMBER() OVER(PARTITION BY [SelltoCustomerID]  ORDER BY [Posting Date] desc) AS Rowid
,[Posting Date]
,LocationID
,l.location_code
,l.Location_name
,parent_id 
,ISNULL([custom_long_field3],'') [custom_long_field3]
,Year([Posting Date]) CalYear
,Month([Posting Date]) CalMonth
,Case When c.[Product Group Code] ='FLAVOFHOME' Then sum([Amount]) ELse 0 End AS [Flavor Of Home Amount]---06-12-2014 Luc Emond
,Case When c.[Product Group Code] <>'FLAVOFHOME' Then sum([Amount]) ELse 0 End AS [Other Amount]---06-12-2014 Luc Emond
into #Sales   
FROM [BI_SSAS_Cubes].[dbo].[FactSales]A
Join [Book4Time_ETL].[dbo].[B4T_location] l
on a.LocationID = l.location_code
and l.location_code <>'TST'
JOIN [Book4Time_ETL].[dbo].[B4T_customer] b
  ON A.[SelltoCustomerID] = B.customer_id
Join [BI_SSAS_Cubes].dbo.DimItemCode c
on a.ItemCode = c.Itemcode  
Where CustomerPostingGroup='MWCC' and [Posting Date]>=DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0)
Group By
 [CustomerPostingGroup]
,[SelltoCustomerID]
,Year([Posting Date]) 
,[DocumentNo]
,[Posting Date]
,LocationID
,l.Location_Name
,parent_id
,[custom_long_field3]
,l.location_code
,c.[Product Group Code]
,b.group_id---06-12-2014 Luc Emond

----ALTER TABLE AND UPDATES------
ALTER TABLE #Sales
Add ParentOfParentId Varchar(60), CustomerName Char(50), GroupName nvarchar(16)

Update #Sales
set ParentOfParentId = parent_id 
where #Sales.[SelltoCustomerID] = #Sales.parent_id

UPDATE #Sales
SET ParentOfParentId = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
WHERE #Sales.parent_id = p.customer_id
and ParentOfParentId is null

UPDATE #Sales
SET CustomerName = P.first_name+' ' +P.last_name
FROM [Book4Time_ETL].[dbo].[B4T_Person]p
WHERE #Sales.ParentOfParentId = p.person_id

UPDATE #Sales ---06-12-2014 Luc Emond
Set group_id = H.Group_id
From [Book4Time_ETL].[dbo].[B4T_Customer] h
where #sales.ParentOfParentId = h.Customer_id

UPDATE #SALES ---06-12-2014 Luc Emond
SET GroupName = g.Name
From [Book4Time_ETL].[dbo].[B4T_Customer_Group] g
where #sales.group_id = g.Group_id

----------------------------Find all Program ---------------------------------------------Christo Requested on 07/25/2014

IF object_id('Tempdb..#XL_055_MWCC_Program_Sales') Is Not Null DROP TABLE #XL_055_MWCC_Program_Sales
;with cte as (select row_number() over (partition by customer_ID order by customer_ID,Caldate desc) as No 
,* from dbo.XL_055_MWCC_Program_Sales
where Item_code not in  ('DP','ADFEE','AF25','AF50','HASF','HOUSE','Miscellaneous','PROFEE','OTP','OTP1'))
Select * into #XL_055_MWCC_Program_Sales from cte
where No=1

----Final View----
Select 
 a.CalYear
,a.CalMonth 
,[Posting Date]-----Luc Emond 2014-02-28---
,[SelltoCustomerID]
,a.CustomerName
,a.GroupName ---06-12-2014 Luc Emond
,a.location_name as LocationName
,a.location_code
,[custom_long_field3] as [Alternate Channel]
,IsNull([CalDate],' ') [Last Program Date]
,IsNull(Item_code,' ' ) LastProgram
,IsNull(item_desc,' ' ) LastProgramDescription
,DocumentNo-----Luc Emond 2014-02-28---
,SUM([Flavor Of Home Amount]) as [Flavor Of Home Amount] ---06-12-2014 Luc Emond
,SUM([Other Amount]) as [Other Amount] ---06-12-2014 Luc Emond
,SUM([Flavor Of Home Amount])+SUM([Other Amount]) as [SubTotal Amount] ---06-12-2014 Luc Emond
,-20.00 as [Negative Commission] ---06-12-2014 Luc Emond
,Case When SUM([Flavor Of Home Amount])+SUM([Other Amount]) <=20 Then 0 Else (SUM([Flavor Of Home Amount])+SUM([Other Amount])-20)end as [Total] ---06-12-2014 Luc Emond
from #Sales a
 left join #XL_055_MWCC_Program_Sales b
on a.SelltoCustomerID = b.[Customer_id]
where [custom_long_field3] <>''
Group By
 a.CalYear
,a.CalMonth 
,[Posting Date]
,a.CustomerName
,a.location_name 
,a.location_code
,[custom_long_field3]
,IsNull([CalDate],' ') 
,IsNull(Item_code,' ' )
,IsNull(item_desc,' ')
,[SelltoCustomerID]
,DocumentNo
,GroupName  ---06-12-2014 Luc Emond


