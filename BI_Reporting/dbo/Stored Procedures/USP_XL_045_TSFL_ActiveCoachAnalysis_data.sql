﻿
--/*
--==================================================================================
--Author:         Emily Trainor
--Create date: 07/30/2012
---------------------------TSFL Active Earning versus Field Analysis------------------
-------------Calculate data for Coach base, sponsoring, and new Clients--------------
--=================================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------
--ODYSSEY_ETL			ODYSSEY_CUSTOMER					      Select
--ODYSSEY_ETL			ODYSSEY_BUSINESSCENTER				      Select   
--etrainor				TSFL_LY_TY								  Select    
--BI_Reporting				CALENDAR							      Select                          
--==================================================================================
--REVISION LOG
--Date           Name                          Change
------------------------------------------------------------------------------------

--==================================================================================
--NOTES:
------------------------------------------------------------------------------------
--==================================================================================
--*/

CREATE procedure [dbo].[USP_XL_045_TSFL_ActiveCoachAnalysis_data] as 
set nocount on ;

-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = '2013-01-01'

-------------------------Delete Current Year Data--------------------------
Delete dbo.TSFL_monthlyactive_data
Where [Calendar Month] >= @Date

--------------------------Pull HC Base-------------------------------------
SELECT 
	    CONVERT(varchar(10),[Commission_Period],121) as [Month]
	   ,count(distinct([Customer_Number])) as [Total Health Coaches]
Into #HCBase
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
			on BC.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Where CONVERT(varchar(10),[COMMISSION_PERIOD],121) >= @Date
		and BC.[RANK_FLOOR]<> 'NONE'
	Group by 
		[Commission_Period]
---------------------------Pull Sponsoring---------------------------------		
SELECT 
 [Posting Date]
,[Orders]
INTO #a 
FROM [BI_Reporting].[dbo].[XL_016_TSFL_LY_TY]
where [Posting Date] >= @Date
and [Customer Posting Group] in ('TSFL')
and [No_] in('31011','31012','32051','31100','31105','31110')
order by [Posting Date] asc

-----Pull order count figure for orders containing sponsoring SKU aggregate to month-----
Select
	 [LastDateOfMonth]
	,sum(B.[OrderCount]) as [Sponsoring]
into #Sponsoring
From #a A
	Inner join [BI_Reporting].[dbo].[XL_016_TSFL_LY_TY] B
		on A.[Orders] = B.Orders
	Inner Join [BI_Reporting].[dbo].[Calendar] C
		on A.[Posting Date] = C. [CalendarDate]
Group By 
	 [LastDateOfMonth]
Order by 
	 [LastDateOfMonth]
-------------------------Pull new Clients----------------------------------------------	
SELECT 
 [Posting Date]
,[Orders]
INTO #b 
FROM [BI_Reporting].[dbo].[XL_016_TSFL_LY_TY]
where [Posting Date] >= @Date
and [No_] in ('40491', '40650') 
and [Dollars] = '0'
order by [Posting Date] asc

-----Pull order count figure for orders containing Client Acquisition SKU aggregate to month-----
Select
	 [LastDateOfMonth]
	,sum(B.[OrderCount]) as [New Clients]
into #Clients
From #b A
	Inner join [BI_Reporting].[dbo].[XL_016_TSFL_LY_TY] B
		on A.[Orders] = B.Orders
	Inner Join [BI_Reporting].[dbo].[Calendar] C
		on A.[Posting Date] = C. [CalendarDate]
Group By 
	 [LastDateOfMonth]
Order by 
	 [LastDateOfMonth]

----------------------------Pull TSFL Orders per Month------------------------------
SELECT 
 [Posting Date]
,[OrderCount]
INTO #d 
FROM [BI_Reporting].[dbo].[XL_016_TSFL_LY_TY]
where [Posting Date] >= @Date
	and [Customer Posting Group] = 'TSFL'
order by [Posting Date] asc

-----------------------------Pull order count aggregate to month------------------------
Select
	 [LastDateOfMonth]
	,sum(A.[OrderCount]) as [Orders]
into #Orders
From #d A
	Inner Join [BI_Reporting].[dbo].[Calendar] C
		on A.[Posting Date] = C. [CalendarDate]
Group By 
	 [LastDateOfMonth]
Order by 
	 [LastDateOfMonth]
		 
	 
-------------------Pull to one table-----------------------
Select 
	 convert(varchar(10),[Month],121) as [Calendar Month]
	,[Total Health Coaches]
	,[Sponsoring]
	,[New Clients]
	,[Orders]
into #all
From #HCBase HC
	left join #Sponsoring S
		on HC.[Month] = S.[LastDateOfMonth]
	left join #Clients C
		on S.[LastDateOfMonth] = C.[LastDateOfMonth]
	left join #Orders O
		on C.[LastDateOfMonth] = O.[LastDateOfMonth]

-----------Insert information into table---------------
	
Insert into dbo.TSFL_monthlyactive_data
Select * from #all		

---clean up---
drop table #a
drop table #HCBase
drop table #Sponsoring
drop table #b
Drop table #Clients
Drop table #all
