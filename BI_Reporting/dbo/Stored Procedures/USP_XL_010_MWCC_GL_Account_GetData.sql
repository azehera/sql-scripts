﻿/*
==================================================================================
Author:         Luc Emond
Create date: 08/14/2012
---------------------------[USP_XL_010_MWCC_GL_Account_GetData]-------------------
Provide Daily Book 4 Time GL Account Transaction-----
==================================================================================   
REFERENCES
Database              Table/View/UDF                       Action            
----------------------------------------------------------------------------------
[BI_Reporting]       [XL_010_MWCC GL Account Master]        Select
[Book4Time_ETL]      [B4T_MWCC_GL_Account_Detail]		    Select  
[Book4Time_ETL]      [B4T_location]                         Select 
                        
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------
2012-12-04   Luc Emond                Removed Prefix [DP-BIDB]
==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_010_MWCC_GL_Account_GetData] As
SET NOCOUNT ON;

-----CREATE THE MASTER TABLE--------
IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master
CREATE TABLE #Master(
	[CalendarDate] [nchar](10) NULL,
	[CalendarYear] [int] NULL,
	[CalendarMonth] [int] NULL,
	[LocationId] [nchar](10) NULL,
	[Location_Name] [varchar](50) NULL,
	[GL_Account] [nvarchar](10) NULL,
	[TenderName] [char](25) NULL,
	[TenderAmount] [numeric](15, 2) NULL
)
----ALL GL ACCOUNT INFO-----
Insert Into #Master
(GL_Account,
TenderName,
TenderAmount)
SELECT
GL_Account,
Tender_Name As TenderName,
0 As TenderAmount
from [BI_Reporting].[dbo].[XL_010_MWCC GL Account Master]

---ALL GL TRANSACTION----- 
Insert Into #Master
Select
 Date As CalendarDate
,Datepart(YYYY,Date) as CalendarYear
,DATEPART(MM,Date) As CalendarMonth
,LocationId
,Location_Name
,LTRIM(g.GL_Account) as GL_Account
,TenderName
,TenderAmount
From [Book4Time_ETL].[dbo].[B4T_MWCC_GL_Account_Detail] g
Left Outer Join
[Book4Time_ETL].[dbo].[B4T_location] 
On LocationId = location_Code


----SELECT FOR THE REPORT-----
Select * from #Master order by CalendarDate desc
