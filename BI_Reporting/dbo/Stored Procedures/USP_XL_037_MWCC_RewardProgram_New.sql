﻿

CREATE PROCEDURE [dbo].[USP_XL_037_MWCC_RewardProgram_New] as
Set Nocount on;

/*
==================================================================================
Author:         Luc Emond
Create date: 08/21/2012
-------------------------[USP_XL_037_MWCC_RewardProgram]------------------
Provide Daily Sales Transaction by Customer for the reward program-----
=================================================================================    
REFERENCES
Database              Table/View/UDF                       Action            
----------------------------------------------------------------------------------
[Bi_Reporting]        [dbo].[Calendar]                      Select  
[Bi_Reporting]        [XL_025_MWCC_LifeTimeContribution]    Select      
[BI_SSAS_Cubes]       [dbo].[FactSales]                     Select 
[BI_SSAS_Cubes]       [dbo].[DimItemCode]                   Select
[Book4Time_ETL]       [dbo].[B4T_location]                  Select
[Book4Time_ETL]       [dbo].[B4T_customer]                  Select
[Book4Time_ETL]       [dbo].[B4T_Person]                    Select
[Book4Time_ETL]       [dbo].[B4T_transaction_log_header]    Select
[Book4Time_ETL]       [dbo].[B4T_transaction_log_detail]    Select
                       
=====================================================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------------------------------------------
2013-09-20    Luc Emond               adding the [custom_long_field2] for comments
2013-09-30    Luc Emond               added Parent Id of Parent Id
2013-01-06    Kalpesh Patel           changed current year to Last Year+Current Year
2014-01-29    Luc Emond               added an Exec for the XL_055_MWCC_Program_Sales
2014-02-05    Luc Emond               added a final table since we need the data for the XL_059
2014-04-21    Luc Emond               Null the calendar Year for grouping purposes on the final table
2014-10-22    D Smith				Added nolock hint to select statements to avoid locking if updates or SSAS processing
									is occuring
======================================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------------------------

======================================================================================================================
*/

----DAILY INFO NEEDED FOR THE XL_055_MWCC_Program_Sales-----
EXEC [BI_Reporting].[dbo].[USP_XL_055_MWCC_Program_Sales]

----TRANSACTION THIS YEAR----------------------
IF object_id('Tempdb..#Sales') Is Not Null DROP TABLE #Sales
SELECT 
 [CustomerPostingGroup]
,[SelltoCustomerID]
,[DocumentNo]
,ROW_NUMBER() OVER(PARTITION BY [SelltoCustomerID]  ORDER BY [Posting Date] desc) AS Rowid
,[Posting Date]
,LocationID
,l.location_code
,l.Location_name
,parent_id 
,ISNULL([custom_field2], '') [custom_field2]
,ISNULL([custom_field3], '') [custom_field3]
,ISNULL([custom_field4], '') [custom_field4]
,ISNULL([custom_long_field2],'') [custom_long_field2]
,Year([Posting Date]) CalYear
,sum([Amount]) AS [Amount]
,Coupon
into #Sales   
FROM [BI_SSAS_Cubes].[dbo].[FactSales]A (NOLOCK)
Join [Book4Time_ETL].[dbo].[B4T_location] l (NOLOCK)
on a.LocationID = l.location_code
and l.location_code <>'TST'
JOIN [Book4Time_ETL].[dbo].[B4T_customer] B (NOLOCK)
  ON A.[SelltoCustomerID] = B.customer_id
Join [Book4Time_ETL].[dbo].[B4T_TransactionSummaryList] c (NOLOCK)
on a.DocumentNo = c.transaction_id  
Where CustomerPostingGroup='MWCC' and [Posting Date]>=DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()-366), 0)--Change Current year to last year+Current Year
Group By
 [CustomerPostingGroup]
,[SelltoCustomerID]
,Year([Posting Date]) 
,[DocumentNo]
,[Posting Date]
,LocationID
,l.Location_Name
,parent_id
,[custom_field2]
,[custom_field3]
,[custom_field4]
,[custom_long_field2]
,l.location_code
,Coupon

------DELETE INCORRECT INFO AS CHRISTOS--------
--delete #sales where [SelltoCustomerID] <> parent_id 

----ALTER TEMP TABLE TO ADD CUSTOMER NAME-----
ALTER TABLE #Sales
Add ParentOfParentId Varchar(60), CustomerName Char(50)

Update #Sales
set ParentOfParentId = parent_id 
where #Sales.[SelltoCustomerID] = #Sales.parent_id

UPDATE #Sales
SET ParentOfParentId = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
WHERE #Sales.parent_id = p.customer_id
and ParentOfParentId is null

UPDATE #Sales
SET CustomerName = P.first_name+' ' +P.last_name
FROM [Book4Time_ETL].[dbo].[B4T_Person]p
WHERE #Sales.ParentOfParentId = p.person_id

------LIFE TIME VALUE-----
IF object_id('Tempdb..#LTV') Is Not Null DROP TABLE #LTV
Select
[SelltoCustomerID]
,Sum([Amount]) AS [Amount]
into #LTV
FROM [BI_SSAS_Cubes].[dbo].[FactSales] (NOLOCK)
where [SelltoCustomerID] in (Select [SelltoCustomerID] from #Sales Group By [SelltoCustomerID])
group by [SelltoCustomerID]

------LIFE TIME VALUE SUPPLEMENT-----
IF object_id('Tempdb..#SUP') Is Not Null DROP TABLE #SUP
Select
[SelltoCustomerID]
,Sum([Amount]) AS [Amount]
INTO #SUP
FROM [BI_SSAS_Cubes].[dbo].[FactSales] a (NOLOCK)
Join [BI_SSAS_Cubes].[dbo].[DimItemCode] b (NOLOCK)
on a.ItemCode = b.Itemcode
where [SelltoCustomerID] in (Select [SelltoCustomerID] from #Sales Group By [SelltoCustomerID]) 
AND b.[Product Group Code]in ('SUPPLEMENT', 'VITAMINS')
group by [SelltoCustomerID]

-----LAST LOCATION -------
IF object_id('Tempdb..#LastLocation') Is Not Null DROP TABLE #LastLocation
Select
[SelltoCustomerID]
,Location_Name
,LocationID
,[Posting Date] as LastInvoicedDate
INTO #LastLocation
FROM #Sales
where Rowid =1

---First and Last Program and $$$----
IF object_id('Tempdb..#ProgramSum') Is Not Null DROP TABLE #ProgramSum
select 
customer_id
,item_code
,item_desc
,row_number() over (Partition by customer_id  order by  FirstProgramDate asc) as FirstRowid
,row_number() over (Partition by customer_id  order by  LastProgramDate desc) as LastRowid
,CONVERT(varchar(10),FirstProgramDate,121)FirstProgramDate
,NetProgram as NetSales
into #ProgramSum
from [dbo].[XL_025_MWCC_LifeTimeContribution] (NOLOCK)
--where customer_id='8654473'
order by customer_id, FirstRowid

--select * from #ProgramSum where LastRowid =1 order by FirstProgramDate desc
--select * from #ProgramSum where customer_id='28485463'

----ALL TOGETHER IN MASTER ------------------
IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master
Select 
 [CustomerPostingGroup]
,ParentOfParentId ------added Luc Emond 2013-09-30---
,a.[SelltoCustomerID]
,[CustomerName]
,CalYear
,[custom_field2]
,[custom_field3]
,[custom_field4]
,CASE WHEN [custom_field2] ='' then 0 else 1 end as ListField2
,CASE WHEN [custom_field3] ='' then 0 else 1 end as ListField3
,CASE WHEN [custom_field4] ='' then 0 else 1 end as ListField4
,CASE WHEN [custom_long_field2] ='' then 0 else 1 end as ListField5
,CASE WHEN discount_id ='27644984' Then discount_amt Else 0 End as TotalDiscount    
,[custom_long_field2] as [Reward Comment]
,LTV.AMOUNT AS LTV$
,SUP.Amount AS [Supp/Vita]
,LL.location_name AS LastCenterPurchased
,LL.locationID as location_code
,LL.LastInvoicedDate
,Coupon
into #master
From #Sales a      
LEFT JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_detail]C
  ON A.DocumentNo = C.transaction_id 
LEFT JOIN #LTV LTV
  ON A.SelltoCustomerID  = LTV.SelltoCustomerID
LEFT JOIN #SUP SUP
  ON A.SelltoCustomerID  = SUP.SelltoCustomerID  
LEFT JOIN  #LastLocation LL
  ON A.SelltoCustomerID  = LL.SelltoCustomerID 
--WHERE A.[SelltoCustomerID] ='9756888' 
GROUP BY  
 [CustomerPostingGroup]
,ParentOfParentId ------added Luc Emond 2013-09-30---
,a.[SelltoCustomerID]
,CalYear
,[custom_field2]
,[custom_field3]
,[custom_field4]
,CASE WHEN discount_id ='27644984' Then discount_amt Else 0 End
,[custom_long_field2]
,[CustomerName]  
,LTV.AMOUNT
,SUP.Amount
,LL.location_name
,LL.LocationID
,LL.LastInvoicedDate
,Coupon

----FINAL TABLE----
IF object_id('Tempdb..#Final') Is Not Null DROP TABLE #Final
select 
 [CustomerPostingGroup] as [Customer Posting Group]
,ParentOfParentId ------added Luc Emond 2013-09-30---
,[SelltoCustomerID] as [Sell to CustomerId] 
,a.[CustomerName] as [Customer Name]
,ISNULL(b.FirstProgramDate, '') AS [First Program Date]
,ISNULL(b.NetSales, 0) AS [First Program Amount]
,b.item_code as FirstProgram
,b.item_desc as FirstProgramDescription
,ISNULL(c.FirstProgramDate, '') AS [Last Program Date]
,c.item_code as LastProgram
,c.item_desc as LastProgramDescription
,ISNULL(c.NetSales, 0) AS [Last Program Amount]
,CalYear as [Calendar Year]
,[custom_field2] As [Reward Start Date]
,[custom_field3] as [Reward End Date]
,Case when len([custom_field2]) <5 Then '1900-01-01' Else Cast(Replace([custom_field2], '@', ' ') as datetime) end UpdateRewardStartDate
,Case When len([custom_field3]) <5 Then '1900-01-01' Else Cast(Replace([custom_field3], '@', ' ') as datetime) end UpdateRewardEndDate
,[custom_field4]  as [Reward Original Prog$]
,[Reward Comment]
,TotalDiscount as [Total Discount]
,LTV$
,[Supp/Vita]
,LastCenterPurchased as [Last center Purchased]
,location_code as [Location Code]
,Coupon+TotalDiscount as Coupon
,LastInvoicedDate
into #Final
from #Master a
Left join #ProgramSum b
    on a.SelltoCustomerID = b.customer_id
    and b.FirstRowid =1
Left join #ProgramSum c
    on a.SelltoCustomerID = c.customer_id  
    and c.LastRowid=1 
Where ListField2+ListField3+ListField4+ListField5 >0 
and  [custom_field3] not in ('6.14.14', '5/19/204')

INSERT INTO #Final
([Customer Posting Group]
,ParentOfParentId 
,[Sell to CustomerId] 
,[Customer Name]
,[First Program Date]
,[First Program Amount]
,FirstProgram
,FirstProgramDescription
,[Last Program Date]
,LastProgram
,LastProgramDescription
,[Last Program Amount]
,[Calendar Year]
,[Reward Start Date]
,[Reward End Date]
,UpdateRewardStartDate
,UpdateRewardEndDate
,[Reward Original Prog$]
,[Reward Comment]
,[Total Discount]
,LTV$
,[Supp/Vita]
,[Last center Purchased]
,[Location Code]
,Coupon
,LastInvoicedDate)
select 
 [CustomerPostingGroup] as [Customer Posting Group]
,ParentOfParentId ------added Luc Emond 2013-09-30---
,[SelltoCustomerID] as [Sell to CustomerId] 
,a.[CustomerName] as [Customer Name]
,ISNULL(b.FirstProgramDate, '') AS [First Program Date]
,ISNULL(b.NetSales, 0) AS [First Program Amount]
,b.item_code as FirstProgram
,b.item_desc as FirstProgramDescription
,ISNULL(c.FirstProgramDate, '') AS [Last Program Date]
,c.item_code as LastProgram
,c.item_desc as LastProgramDescription
,ISNULL(c.NetSales, 0) AS [Last Program Amount]
,CalYear as [Calendar Year]
,[custom_field2] As [Reward Start Date]
,[custom_field3] as [Reward End Date]
,NULL UpdateRewardStartDate
,NULL UpdateRewardEndDate
,[custom_field4]  as [Reward Original Prog$]
,[Reward Comment]
,TotalDiscount as [Total Discount]
,LTV$
,[Supp/Vita]
,LastCenterPurchased as [Last center Purchased]
,location_code as [Location Code]
,Coupon+TotalDiscount as Coupon
,LastInvoicedDate
from #Master a
Left join #ProgramSum b
    on a.SelltoCustomerID = b.customer_id
    and b.FirstRowid =1
Left join #ProgramSum c
    on a.SelltoCustomerID = c.customer_id  
    and c.LastRowid=1 
Where ListField2+ListField3+ListField4+ListField5 >0 
and  [custom_field3] in ('6.14.14', '5/19/204')

--select * from #Master
--where ListField5 =1 AND ListField2+ListField3+ListField4=0

-----update coupon and suplement to 0 where null----
update #Final set Coupon =0 where coupon is null
update #Final set [Supp/Vita] =0 where [Supp/Vita] is null

------DELETE UNWANTED DATA-----
DELETE #Final WHERE  [ParentOfParentID] <> [Sell to CustomerId] 

-----ALTER AND UPDATE NEW COLUMNS NEEDED----
ALTER TABLE #FINAL
ADD [Accepted] Char(15), [1st Payment] Datetime, [Entering Stay Fit] Datetime 	

Update #Final
Set [Accepted] = Case When UpdateRewardStartDate = UpdateRewardEndDate or UpdateRewardEndDate <= Convert(Varchar(10),GETDATE(),121) Then 'Not Accepted' Else 'Accepted' End 

Update #Final
Set [1st Payment] = Case When [Accepted] ='Accepted' Then UpdateRewardEndDate-365/2 Else 0 end 

Update #Final
Set [Entering Stay Fit] = Case When [Accepted] ='Accepted' Then UpdateRewardEndDate-365 Else 0 end

Update #Final set [1st Payment] = Null Where [1st Payment] ='1900-01-01 00:00:00.000'
Update #Final set [Entering Stay Fit] = Null Where [Entering Stay Fit] ='1900-01-01 00:00:00.000'


--------------FINAL TABLE-----''
TRUNCATE TABLE dbo.XL_037_MWCC_RewardProgram_New-------Luc Emond 2014-02-05
INSERT INTO dbo.XL_037_MWCC_RewardProgram_New----------Luc Emond 2014-02-05 
Select 
 [Customer Posting Group]
,ParentOfParentId as [Parent Of ParentID]------added Luc Emond 2013-09-30---
,[Sell to CustomerId] 
,[Customer Name]
,[First Program Date]
,FirstProgram
,FirstProgramDescription
,[First Program Amount]
,[Last Program Date]
,LastProgram
,LastProgramDescription
,[Last Program Amount]
,Null as [Calendar Year]----Luc Emond Null the calendar year to group better
,[Reward Start Date]
,[Reward End Date]
,[Reward Original Prog$]
,[Reward Comment]
,Sum(coupon) as Coupon
,LTV$
,[Supp/Vita]
,[Last Center Purchased]
,[Location Code]
,LastInvoicedDate as [Last Invoiced Date]
,UpdateRewardStartDate
,UpdateRewardEndDate
,[Accepted]
,[1st Payment]
,[Entering Stay Fit]
from #Final
Group By
 [Customer Posting Group]
,ParentOfParentId  ------added Luc Emond 2013-09-30---
,[Sell to CustomerId] 
,[Customer Name]
,[First Program Date]
,FirstProgram
,FirstProgramDescription
,[First Program Amount]
,[Last Program Date]
,LastProgram
,LastProgramDescription
,[Last Program Amount]
--,[Calendar Year]----Luc Emond Null the calendar year to group better
,[Reward Start Date]
,[Reward End Date]
,[Reward Original Prog$]
,LTV$
,[Supp/Vita]
,[Last Center Purchased]
,[Location Code]
,[Reward Comment]
,LastInvoicedDate
,UpdateRewardStartDate
,UpdateRewardEndDate
,[Accepted]
,[1st Payment]
,[Entering Stay Fit]
order by ParentOfParentId

-----SELECT ALL THE DATA FOR THE REPORT-----Luc Emond 2014-02-05
select * from dbo.XL_037_MWCC_RewardProgram_New (NOLOCK)
 --WHERE [Reward End Date] in ('6.14.14', '5/19/204')
