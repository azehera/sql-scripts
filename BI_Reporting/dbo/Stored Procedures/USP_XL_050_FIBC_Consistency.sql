﻿



/*
===============================================================================
Author:         Kalpesh Patel
Create date: 01/20/2014

==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CORE_COMMISSION_ENTRY]		   Select   
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                     Select  
[ODYSSEY_ETL]       [dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY]        Select
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CO_APPLICANT]                 Select
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER_ADDRESS]             Select
=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
2014-01-22           Luc Emond        Added a new column in Numeric for Coach Id for V_Lookup                 

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/
CREATE procedure [dbo].[USP_XL_050_FIBC_Consistency] 
@M1EndDate Date
AS
set nocount on ;


Declare @StartDate Datetime
--Declare @M1EndDate Datetime

Set @StartDate=(select DATEADD(MONTH, DATEDIFF(MONTH, 0, @M1EndDate), 0))
--Set @M1EndDate=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))
------------------------------Pull only active Co-Apps into a Co-App Temp Table-----------------------------------
Select *
Into #Coapp
	From [ODYSSEY_ETL].[dbo].[ODYSSEY_CO_APPLICANT]
		Where [IS_OBSOLETE] is null
-----------------------------------------Pull FIBC Consistency Bonuses Earned----------------------------------
Select
	 A.[CUSTOMER_NUMBER] as [Coach ID]
	,Convert(numeric(36,0),(A.[CUSTOMER_NUMBER])) as V_LookupCoachID
	,A.[FIRST_NAME]as [First_Name]
	,A.[LAST_NAME]as [LN]
	,isnull(Co.[LAST_NAME],'')  as [Co-App Last Name]
	,isnull(Co.[FIRST_NAME],'') as [Co-App First Name]
	,[Recognition_Name]
	,CA.[City]
    ,CA.[STATE]
    ,A.[EMAIL1_ADDRESS] as [Coach_Email]
	,[FIBC] as [FIBC Bonus]
	,convert(varchar (10),E.COMMISSION_DATE,121) as [Bonus Month]

FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] E
	inner join[ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] D
		on E.[COMMISSION_ENTRY_ID] = D.[COMMISSION_ENTRY_ID]
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
		on E.[CUSTOMER_ID] = A.[CUSTOMER_ID]
	--Inner join [BI_Reporting].[dbo].[calendar_TSFL]C 
	--	on convert(varchar (10),E.COMMISSION_DATE,121)= C.[CalendarDate]
	left join #Coapp Co
		on A.[CUSTOMER_ID] = Co.[CUSTOMER_ID]
	left join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS] CA
		on A.[CUSTOMER_ID] = CA.[CUSTOMER_ID]
	Where [FIBC] > '0'
		and CAST(E.COMMISSION_DATE AS DATE)between @StartDate and @M1EndDate
		and CA.[ADDRESS_TYPE] = 'Main'
		and CA.[LANGUAGE_CODE] = 'English'
		and A.[CUSTOMER_NUMBER] not in ('1','2','3','101','6','30006445','9027001','6957001','21283101','763987990')
 Order by [FIBC]
 
 -------------------------Clean up Temp tables created-------------------
 Drop table #Coapp

