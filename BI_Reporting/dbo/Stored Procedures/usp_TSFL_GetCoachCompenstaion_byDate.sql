﻿CREATE PROCEDURE [dbo].[usp_TSFL_GetCoachCompenstaion_byDate]
    @StartDate DATE ,
    @EndDate DATE
AS /***************************************************************************
Developer: Ron Baldwin
Date: 03/06/2015
Description: Used to get TSFL Health Coach Compensation for a given data range.
---------------------------------------------------------------------------
REFERENCES
Database			Table/View/UDF								Action            
----------------------------------------------------------------------------------
ODYSSEY_ETL			[dbo].[ODYSSEY_BUSINESSCENTER]				Select
ODYSSEY_ETL			[dbo].[ODYSSEY_CUSTOMER]				Select
ODYSSEY_ETL			[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]				Select
ODYSSEY_ETL			[dbo].[ODYSSEY_COMMISSION_PERIOD]				Select
ODYSSEY_ETL			[dbo].[ODYSSEY_COMMISSION_PAYMENT]				Select
ODYSSEY_ETL			[dbo].[ODYSSEY_VOLUME]				Select

==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------

==================================================================================
NOTES:
----------------------------------------------------------------------------------
==================================================================================

******************************************************************************/

    SET NOCOUNT ON

----EXEC [dbo].[usp_TSFL_GetCoachCompenstaion_byDate] @StartDate = '01/01/2012', @EndDate = '01/01/2015'

--Set @StartDate = '01/01/2012'
--Set @EndDate = '01/01/2015'

    IF OBJECT_ID('Tempdb..#Commissions') IS NOT NULL
        DROP TABLE #Commissions
    CREATE TABLE #Commissions
        (
          CustNo VARCHAR(20) ,
          CommPeriod DATETIME ,
          CommType VARCHAR(10) ,
          CSC MONEY ,
          CB MONEY ,
          CSB MONEY ,
          HCC MONEY ,
          CAB MONEY ,
          AB MONEY ,
          TG MONEY ,
          FIBC MONEY ,
          EDGB MONEY ,
          NED MONEY ,
          PED MONEY ,
          GED MONEY ,
          TRANADJ MONEY
        )

--Weekly Commissions
    INSERT  INTO #Commissions
            SELECT  c.CUSTOMER_NUMBER ,
                    per.COMMISSION_PERIOD ,
                    'WEEKLY' ,
                    SUM(ISNULL(ce.CSC_AMOUNT, 0)) CSC ,
                    0 AS CB ,
                    0 AS CSB ,
                    0 AS HCC ,
                    SUM(ISNULL(ce.CAB_BONUS, 0)) CAB ,
                    SUM(ISNULL(ce.ASSIST_BONUS, 0)) AB ,
                    0 AS TG ,
                    0 AS FIBC ,
                    0 AS EDGB ,
                    0 AS NED ,
                    0 AS PED ,
                    0 AS GED ,
                    0 AS TRANADJ
            FROM    [ODYSSEY_ETL].[dbo].ODYSSEY_COMMISSION_PAYMENT cp
                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c ON cp.PARTICIPANT_ID = c.CUSTOMER_ID
                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_TSFL_COMM_ENTRY_WEEKLY ce ON cp.COMMISSION_ENTRY_ID = ce.COMMISSION_ENTRY_ID
                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_COMMISSION_PERIOD per ON cp.COMMISSION_PERIOD_ID = per.COMMISSION_PERIOD_ID
            WHERE   per.COMMISSION_PERIOD >= @StartDate
                    AND per.COMMISSION_PERIOD < @EndDate
                    AND per.PERIOD_TYPE = 'WEEKLY'
            GROUP BY c.CUSTOMER_NUMBER ,
                    per.COMMISSION_PERIOD
            ORDER BY c.CUSTOMER_NUMBER ,
                    per.COMMISSION_PERIOD

--Monthly Commissions
    INSERT  INTO #Commissions
            SELECT  c.CUSTOMER_NUMBER ,
                    per.COMMISSION_PERIOD ,
                    'MONTHLY' ,
                    SUM(ISNULL(ce.CSC_AMOUNT, 0)) CSC ,
                    SUM(ISNULL(ce.CERTIFIED_BONUS, 0)) CB ,
                    SUM(ISNULL(ce.CUSTOMER_SUPPORT_BONUS, 0)) CSB ,
                    SUM(ISNULL(ce.ROLLING_BONUS, 0)) HCC ,
                    SUM(ISNULL(ce.CAB_BONUS, 0)) CAB ,
                    SUM(ISNULL(ce.ASSIST_BONUS, 0)) AB ,
                    SUM(ISNULL(ce.GROWTH_BONUS, 0)) TG ,
                    SUM(ISNULL(ce.FIBC, 0)) FIBC ,
                    SUM(ISNULL(ce.GENERATION_BONUS, 0)) EDGB ,
                    SUM(ISNULL(ce.OVERRIDE_BONUS, 0)) NED ,
                    SUM(ISNULL(ce.OVERRIDE_BONUS_PED, 0)) PED ,
                    SUM(ISNULL(ce.OVERRIDE_BONUS_GED, 0)) GED ,
                    SUM(ISNULL(ce.ANSITION_ADJ, 0)) TRANADJ
            FROM    [ODYSSEY_ETL].[dbo].ODYSSEY_COMMISSION_PAYMENT cp
                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c ON cp.PARTICIPANT_ID = c.CUSTOMER_ID
                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_TSFL_COMMISSION_ENTRY ce ON cp.COMMISSION_ENTRY_ID = ce.COMMISSION_ENTRY_ID
                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_COMMISSION_PERIOD per ON cp.COMMISSION_PERIOD_ID = per.COMMISSION_PERIOD_ID
            WHERE   per.COMMISSION_PERIOD >= @StartDate
                    AND per.COMMISSION_PERIOD < @EndDate
                    AND per.PERIOD_TYPE = 'DEFAULT'
            GROUP BY c.CUSTOMER_NUMBER ,
                    per.COMMISSION_PERIOD
            ORDER BY c.CUSTOMER_NUMBER ,
                    per.COMMISSION_PERIOD

    SELECT DISTINCT
            c.CommPeriod ,
            c.CustNo
    FROM    #Commissions c -- 1,115,585 rows
    SELECT DISTINCT
            c.CommPeriod ,
            c.CustNo
    FROM    #Commissions c
    WHERE   c.CommType = 'MONTHLY' -- 173,387
    SELECT DISTINCT
            c.CommPeriod ,
            c.CustNo ,
            COUNT(*)
    FROM    #Commissions c
    WHERE   c.CommType = 'MONTHLY'
    GROUP BY c.CommPeriod ,
            c.CustNo
    HAVING  COUNT(*) > 1 -- None
    SELECT DISTINCT
            c.CommPeriod ,
            c.CustNo
    FROM    #Commissions c
    WHERE   c.CommType = 'WEEKLY' -- 962,413
    SELECT DISTINCT
            c.CommPeriod ,
            c.CustNo ,
            COUNT(*)
    FROM    #Commissions c
    WHERE   c.CommType = 'WEEKLY'
    GROUP BY c.CommPeriod ,
            c.CustNo
    HAVING  COUNT(*) > 1 -- None

    IF OBJECT_ID('Tempdb..#Volume') IS NOT NULL
        DROP TABLE #Volume
    CREATE TABLE #Volume
        (
          CustNo VARCHAR(20) ,
          CommPeriod DATETIME ,
          CommType VARCHAR(10) ,
          QP INTEGER ,
          FL MONEY ,
          GV MONEY
        )

-- Remaining Columns
    INSERT  INTO #Volume
            SELECT  comm.CustNo ,
                    comm.CommPeriod ,
                    'MONTHLY' ,
                    ISNULL(( SELECT ISNULL(v.VOLUME_VALUE, 0)
                             FROM   [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c
                                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_BUSINESSCENTER b ON c.CUSTOMER_ID = b.CUSTOMER_ID
                                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_VOLUME v ON b.BUSINESSCENTER_ID = v.NODE_ID
                                                              AND b.COMMISSION_PERIOD = v.PERIOD_END_DATE
                             WHERE  c.CUSTOMER_NUMBER = comm.CustNo
                                    AND v.PERIOD_END_DATE = comm.CommPeriod
                                    AND v.VOLUME_TYPE = 'QP'
                           ), 0) QP ,
                    ISNULL(( SELECT ISNULL(v.VOLUME_VALUE, 0)
                             FROM   [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c
                                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_BUSINESSCENTER b ON c.CUSTOMER_ID = b.CUSTOMER_ID
                                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_VOLUME v ON b.BUSINESSCENTER_ID = v.NODE_ID
                                                              AND b.COMMISSION_PERIOD = v.PERIOD_END_DATE
                             WHERE  c.CUSTOMER_NUMBER = comm.CustNo
                                    AND v.PERIOD_END_DATE = comm.CommPeriod
                                    AND v.VOLUME_TYPE = 'FV'
                           ), 0) FV ,
                    ISNULL(( SELECT ISNULL(v.VOLUME_VALUE, 0)
                             FROM   [ODYSSEY_ETL].[dbo].ODYSSEY_CUSTOMER c
                                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_BUSINESSCENTER b ON c.CUSTOMER_ID = b.CUSTOMER_ID
                                    INNER JOIN [ODYSSEY_ETL].[dbo].ODYSSEY_VOLUME v ON b.BUSINESSCENTER_ID = v.NODE_ID
                                                              AND b.COMMISSION_PERIOD = v.PERIOD_END_DATE
                             WHERE  c.CUSTOMER_NUMBER = comm.CustNo
                                    AND v.PERIOD_END_DATE = comm.CommPeriod
                                    AND v.VOLUME_TYPE = 'GV'
                           ), 0) GV
            FROM    #Commissions comm
            WHERE   comm.CommType = 'MONTHLY'

    SELECT DISTINCT
            v.CommPeriod ,
            v.CustNo
    FROM    #Volume v -- 173,387

    INSERT  INTO #Volume
            SELECT  CustNo ,
                    CommPeriod ,
                    'WEEKLY' ,
                    0 ,
                    0 ,
                    0
            FROM    #Commissions
            WHERE   CommType = 'WEEKLY'
            ORDER BY CommPeriod DESC -- 962,413

    SELECT  962413 + 173387 -- 1,135,800

-- Without Volume
    SELECT  c.CommType ,
            c.CommPeriod ,
            SUM(c.CSC + c.CB + c.CSB + c.HCC + c.CAB + c.AB + c.TG + c.FIBC
                + c.EDGB + c.NED + c.PED + c.GED + c.TRANADJ) Amount
    FROM    #Commissions c
    GROUP BY c.CommType ,
            c.CommPeriod
    ORDER BY c.CommType ,
            c.CommPeriod -- 193 rows

-- With Volume, Commissions Summed
    SELECT  v.CommType ,
            v.CommPeriod ,
            SUM(c.CSC + c.CB + c.CSB + c.HCC + c.CAB + c.AB + c.TG + c.FIBC
                + c.EDGB + c.NED + c.PED + c.GED + c.TRANADJ) Amount
    FROM    #Volume v
            LEFT JOIN #Commissions c ON v.CustNo = c.CustNo
                                        AND v.CommPeriod = c.CommPeriod
                                        AND c.CommType = v.CommType
    GROUP BY v.CommType ,
            v.CommPeriod
    ORDER BY v.CommType ,
            v.CommPeriod

-- Volume Joined With Commissions
    SELECT  CONVERT(VARCHAR(10), v.CommPeriod, 121) CommPeriod ,
            CONVERT(BIGINT, v.CustNo) CustNo ,
            v.CommType ,
            v.QP ,
            v.FL ,
            v.GV ,
            c.CSC ,
            c.CB ,
            c.CSB ,
            c.HCC ,
            c.CAB ,
            c.AB ,
            c.TG ,
            c.FIBC ,
            c.EDGB ,
            c.NED ,
            c.PED ,
            c.GED ,
            c.TRANADJ
    FROM    #Volume v
            LEFT JOIN #Commissions c ON v.CustNo = c.CustNo
                                        AND v.CommPeriod = c.CommPeriod
                                        AND c.CommType = v.CommType
    ORDER BY c.CommPeriod ,
            CONVERT(BIGINT, v.CustNo) ,
            v.CommType -- 1,135,800

    IF OBJECT_ID('Tempdb..#Final') IS NOT NULL
        DROP TABLE #Final
    CREATE TABLE #Final
        (
          CommPeriod VARCHAR(10) ,
          CustNo VARCHAR(20) ,
          QP INTEGER ,
          FL MONEY ,
          GV MONEY ,
          CSC MONEY ,
          CB MONEY ,
          CSB MONEY ,
          HCC MONEY ,
          CAB MONEY ,
          AB MONEY ,
          TG MONEY ,
          FIBC MONEY ,
          EDGB MONEY ,
          NED MONEY ,
          PED MONEY ,
          GED MONEY ,
          TRANADJ MONEY
        )

-- Volume Joined With Commissions And No CommType
    INSERT  INTO #Final
            SELECT  CONVERT(VARCHAR(10), v.CommPeriod, 121) CommPeriod ,
                    CONVERT(BIGINT, v.CustNo) CustNo ,
                    SUM(v.QP) QP ,
                    SUM(v.FL) FL ,
                    SUM(v.GV) GV ,
                    SUM(c.CSC) CSC ,
                    SUM(c.CB) CB ,
                    SUM(c.CSB) CSB ,
                    SUM(c.HCC) HCC ,
                    SUM(c.CAB) CAB ,
                    SUM(c.AB) AB ,
                    SUM(c.TG) TG ,
                    SUM(c.FIBC) FIBC ,
                    SUM(c.EDGB) EDGB ,
                    SUM(c.NED) NED ,
                    SUM(c.PED) PED ,
                    SUM(c.GED) GED ,
                    SUM(c.TRANADJ) TRANADJ
            FROM    #Volume v
                    LEFT JOIN #Commissions c ON v.CustNo = c.CustNo
                                                AND v.CommPeriod = c.CommPeriod
                                                AND c.CommType = v.CommType
            GROUP BY v.CommPeriod ,
                    CONVERT(BIGINT, v.CustNo)
            ORDER BY v.CommPeriod ,
                    CONVERT(BIGINT, v.CustNo) -- 1,115,585

-- The next 36 select originally created to break the data into 36 months
/*
select * from #Final f where f.CommPeriod >= '2012-01-01' and f.CommPeriod <= '2012-01-31' order by f.CustNo, CommPeriod
select * from #Final f where f.CommPeriod >= '2012-02-01' and f.CommPeriod <= '2012-02-29' order by f.CustNo, CommPeriod
select * from #Final f where f.CommPeriod >= '2012-03-01' and f.CommPeriod <= '2012-03-31' order by f.CustNo, CommPeriod
select * from #Final f where f.CommPeriod >= '2012-04-01' and f.CommPeriod <= '2012-04-30' order by f.CustNo, CommPeriod
select * from #Final f where f.CommPeriod >= '2012-05-01' and f.CommPeriod <= '2012-05-31' order by f.CustNo, CommPeriod
select * from #Final f where f.CommPeriod >= '2012-06-01' and f.CommPeriod <= '2012-06-30' order by f.CustNo, CommPeriod
select * from #Final f where f.CommPeriod >= '2012-07-01' and f.CommPeriod <= '2012-07-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2012-08-01' and f.CommPeriod <= '2012-08-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2012-09-01' and f.CommPeriod <= '2012-09-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2012-10-01' and f.CommPeriod <= '2012-10-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2012-11-01' and f.CommPeriod <= '2012-11-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2012-12-01' and f.CommPeriod <= '2012-12-31' order by f.CustNo, CommPeriod 

-----

select * from #Final f where f.CommPeriod >= '2013-01-01' and f.CommPeriod <= '2013-01-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-02-01' and f.CommPeriod <= '2013-02-28' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-03-01' and f.CommPeriod <= '2013-03-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-04-01' and f.CommPeriod <= '2013-04-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-05-01' and f.CommPeriod <= '2013-05-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-06-01' and f.CommPeriod <= '2013-06-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-07-01' and f.CommPeriod <= '2013-07-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-08-01' and f.CommPeriod <= '2013-08-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-09-01' and f.CommPeriod <= '2013-09-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-10-01' and f.CommPeriod <= '2013-10-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-11-01' and f.CommPeriod <= '2013-11-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2013-12-01' and f.CommPeriod <= '2013-12-31' order by f.CustNo, CommPeriod 

-----

select * from #Final f where f.CommPeriod >= '2014-01-01' and f.CommPeriod <= '2014-01-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-02-01' and f.CommPeriod <= '2014-02-29' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-03-01' and f.CommPeriod <= '2014-03-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-04-01' and f.CommPeriod <= '2014-04-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-05-01' and f.CommPeriod <= '2014-05-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-06-01' and f.CommPeriod <= '2014-06-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-07-01' and f.CommPeriod <= '2014-07-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-08-01' and f.CommPeriod <= '2014-08-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-09-01' and f.CommPeriod <= '2014-09-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-10-01' and f.CommPeriod <= '2014-10-31' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-11-01' and f.CommPeriod <= '2014-11-30' order by f.CustNo, CommPeriod 
select * from #Final f where f.CommPeriod >= '2014-12-01' and f.CommPeriod <= '2014-12-31' order by f.CustNo, CommPeriod 
*/
IF OBJECT_ID('TSFL_CoachesWithCommissions_2012_to_2014') IS NOT NULL
DROP TABLE TSFL_CoachesWithCommissions_2012_to_2014
-- Single File Per Tom K: Changed Tool - Options - Results To Text - Tab Delimited
    SELECT  *
    INTO    TSFL_CoachesWithCommissions_2012_to_2014
    FROM    #Final f
    ORDER BY f.CommPeriod ,
            f.CustNo -- 1115585

