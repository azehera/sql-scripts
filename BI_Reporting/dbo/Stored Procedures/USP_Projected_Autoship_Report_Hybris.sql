﻿



/*
=======================================================================================
Author:         Derald Smith & Ronak Shah
Create date: Date, 01/16/2015
-------------------------[USP_Projected_Autoship_Report_Hybris] -------------------------
---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx----------------------
=======================================================================================   
REFERENCES
Database             Table/View/UDF		             Action            
---------------------------------------------------------------------------------------
ECOMM_ETL_Hybris	[dbo].[template_header]			Select
ECOMM_ETL_Hybris	[dbo].[order_header]			Select
ECOMM_ETL_Hybris	[dbo].[consignments]			Select

========================================================================================                 
REVISION LOG
Date           Name              Change
----------------------------------------------------------------------------------------
7/26/2019   Daniel D.		I changed the data source to vw_Template_header_pr which is
							pointing to hybris_ETL and the case statement is removed for
							channel since all the case conditions are done in the view.
========================================================================================

*/

CREATE PROCEDURE [dbo].[USP_Projected_Autoship_Report_Hybris]
AS
SET NOCOUNT ON;

BEGIN

    DECLARE @StartDate DATE;
    DECLARE @EndDate DATE;

    SET @StartDate = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0);
    SET @EndDate = DATEADD(DAY, 29, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0));


    SELECT CAST([nextautoshiporderdate] AS DATE) AS OrderDate,
           DATENAME(DW, [nextautoshiporderdate]) AS OrderDay,
           --CASE WHEN th.channel = 'Optavia' THEN 'BeSlim'
           --	 WHEN th.channel = 'Medifast' THEN 'VIP' END AS Channel,
           th.channel,
           NULL AS DistCenter,
           COUNT(*) AS TotalCount
    --FROM ecomm_etl_hybris.dbo.template_header th
    FROM Hybris_ETL.[dbo].[vw_Template_header_prd] th WITH (NOlock)
    WHERE [nextautoshiporderdate]
          BETWEEN @StartDate AND @EndDate
          AND templatestatus = '1'
    GROUP BY CAST([nextautoshiporderdate] AS DATE),
             DATENAME(DW, [nextautoshiporderdate]),
             th.channel;









END;


