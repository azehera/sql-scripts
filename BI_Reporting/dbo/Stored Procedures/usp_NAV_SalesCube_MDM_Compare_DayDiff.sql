﻿-- =============================================
-- Author:		Micah Williams
-- Create date: 06-05-2017
-- Description:	[usp_NAV_SalesCube_MDM_Compare] Comparing the three databases amount and order count data
-- =============================================
CREATE PROCEDURE [dbo].[usp_NAV_SalesCube_MDM_Compare_DayDiff]

AS


IF OBJECT_ID('Tempdb..#NAV_prep1') IS NOT NULL DROP TABLE #NAV_prep1
SELECT (H.[Posting Date])AS 'Date', SUM(L.Amount) AS 'Amount', COUNT(DISTINCT H.No_) AS 'Count'
INTO #NAV_prep1
FROM NAV_ETL.dbo.[Jason Pharm$Sales Invoice Header] H
JOIN NAV_ETL.dbo.[Jason Pharm$Sales Invoice Line] L ON L.[Document No_] = H.No_
WHERE H.[Posting Date] >= DATEADD(month, -6, GETDATE())
GROUP BY (H.[Posting Date])


IF OBJECT_ID('Tempdb..#NAV_prep2') IS NOT NULL DROP TABLE #NAV_prep2
SELECT (CR.[Posting Date]) AS 'Date' , SUM(CRL.Amount) AS 'Amount', COUNT(DISTINCT CR.No_) AS 'Count'
INTO #NAV_prep2
FROM NAV_ETL.dbo.[Jason Pharm$Sales Cr_Memo Header] CR 
LEFT JOIN NAV_ETL.dbo.[Jason Pharm$Sales Cr_Memo Line] CRL ON CR.No_ = CRL.[Document No_]
WHERE CR.[Posting Date] >= DATEADD(month, -6, GETDATE())
GROUP BY (CR.[Posting Date])

IF OBJECT_ID('Tempdb..#NAVETL') IS NOT NULL DROP TABLE #NAVETL
SELECT N1.Date, (n1.Amount-n2.Amount) AS 'Amount', (N1.Count+n2.Count) AS 'Count'
INTO #NAVETL
FROM #NAV_prep1 N1
LEFT JOIN #NAV_prep2 n2 ON n2.Date = N1.Date




IF OBJECT_ID('Tempdb..#SC') IS NOT NULL DROP TABLE #SC
SELECT ([Posting Date]) AS 'Date', SUM(Amount) 'Sales_Cube_Amount', COUNT(DISTINCT DocumentNo) AS 'Sales_Cube_Order_Count'
INTO #SC
FROM BI_SSAS_Cubes.dbo.FactSales
WHERE [Posting Date] >= DATEADD(month, -6, GETDATE())
GROUP BY ([Posting Date])
ORDER BY ([Posting Date])

IF OBJECT_ID('Tempdb..#MDM') IS NOT NULL DROP TABLE #MDM
SELECT ([Posting Date]) AS 'Date', SUM(Amount) 'MDM_Cube_Amount', COUNT(DISTINCT DocumentNo) AS 'MDM_Cube_Order_Count'
INTO #MDM
--FROM [MDM].[BICubes].[dbo].FactSales_MDM
FROM [BICUBES_MDM].[dbo].FactSales_MDM
WHERE [Posting Date] >= DATEADD(month, -6, GETDATE())
GROUP BY ([Posting Date])
ORDER BY ([Posting Date])



SELECT N.Date AS 'Date' ,
        N.Amount AS 'NAVETL_Amount' ,
        S.Sales_Cube_Amount ,
        M.MDM_Cube_Amount ,
        N.Count AS' NAVET_Count' ,
        S.Sales_Cube_Order_Count ,
        M.MDM_Cube_Order_Count
FROM    #NAVETL N
        JOIN #SC S ON S.Date = N.Date
        JOIN #MDM M ON M.Date = N.Date
WHERE N.Amount != S.Sales_Cube_Amount OR N.Amount != M.MDM_Cube_Amount
	 OR N.Count != S.Sales_Cube_Order_Count OR M.MDM_Cube_Order_Count != N.Count
ORDER BY N.Date DESC;
