﻿

/*
===============================================================================
Author:  Kalepsh Patel
Create date: 08/11/2013
-------------------------[USP_XL_023_MonthlyForecastView]-----------------------------
---------------------------------
==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[BI_SSAS_Cubes]            [dbo].[FactSales]                                      Select 
[BI_Reporting]        [dbo].[XL_023_MonthlySalesForecast]            Select
[BI_Reporting]        [dbo].[XL_023_MonthlySalesTracker_TY]          Select
[BI_Reporting]        [dbo].[XL_023_MonthlySalesTracker_TY_PostingGroup] Select 
[Bi_Reporting]        [dbo].[XL_023_MonthlyForecastView]             Insert
===============================================================================
REVISION LOG
Date            Name                          Change
-------------------------------------------------------------------------------
2014-01-03     Luc Emond                  Updated Store Procedure as followed:
                                          recalculate the autoship factor.
                                          6 month of sales data to calc on demand
                                           
2014-01-31     Luc Emond                  Updated Store Procedure as followed:
                                          Thomas wanted to change the calculation
                                          to last 30 days versus MTD.
                                          
2014-12-05       Ron Baldwin                            Enhancement to handle NULL values for 
                                                                     OnDemandForecastXXXX
--2016-10-13     Daniel Dagnachew           Updated Store Procedure as followed:
--                                          Thomas wanted to change the calculation
--                                          to last 28 days versus MTD.
===============================================================================
NOTES:
-------------------------------------------------------------------------------
===============================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_023_MonthlyForecastView_04302020] AS 
SET NOCOUNT ON;

----TEMP TABLES----
IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master
IF object_id('Tempdb..#Step1') Is Not Null DROP TABLE #Step1
IF object_id('Tempdb..#Step2') Is Not Null DROP TABLE #Step2
IF object_id('Tempdb..#Step3') Is Not Null DROP TABLE #Step3
IF object_id('Tempdb..#Step4') Is Not Null DROP TABLE #Step4
IF object_id('Tempdb..#Step5') Is Not Null DROP TABLE #Step5
IF object_id('Tempdb..#Step6') Is Not Null DROP TABLE #Step6
IF object_id('Tempdb..#Step7') Is Not Null DROP TABLE #Step7

----DECLARE TIMEFRAME----
DECLARE 
 @Today DateTime
,@Yesterday Datetime
,@FirstOfYear DateTime
,@TsflPercentCalcManual Numeric(15,6)
,@MedifastPercentCalcManual Numeric(15,6)
,@FirstDateOfMonth DateTime
,@LastDateOfMonth DateTime
,@Passed Numeric(15,6)
,@Remain Numeric(15,6)
,@ThisMonth Numeric(15,6)
        
SET @Today = (SELECT CONVERT(VARCHAR(10), GETDATE(),121))
SET @Yesterday = (@Today-1)
SET @FirstOfYear = (SELECT FirstDateOfYear FROM DBO.calendar WHERE CalendarDate= @Yesterday)
SET @FirstDateOfMonth = (SELECT FirstDateOfMonth FROM DBO.calendar WHERE CalendarDate= @Yesterday)
SET @LastDateOfMonth = (SELECT LastDateOfMonth FROM DBO.calendar WHERE CalendarDate= @Yesterday)
SET @TsflPercentCalcManual = (Select (SUM(TsflActual)/SUM(TsflForecast))from [dbo].[XL_023_MonthlyAutoshipForecastAndActual] Where CalendarDate>=GETDATE()-210)---Luc Emond 2014-01-03---
SET @MedifastPercentCalcManual = (Select (SUM(MedifastActual)/SUM(MedifastForecast))from [dbo].[XL_023_MonthlyAutoshipForecastAndActual]where CalendarDate>=GETDATE()-210)---Luc Emond 2014-01-03---
--SET @Passed =(Select MAX(BusinessDayOfFiscalPeriod) from [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_TY])
--SET @Remain = (select MIN(RemainingDays) from [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_TY])
SET @ThisMonth =(select datediff(day, dateadd(day, 1-day(@Yesterday), @Yesterday),
                 dateadd(month, 1, dateadd(day, 1-day(@Yesterday), @Yesterday))))---Number of day in the month----
              
SET @Passed =(Select DATEDIFF (Day , @FirstDateOfMonth , CONVERT(Varchar(10), @Today,121)))---Number of day passed---
SET @Remain = (@ThisMonth - @Passed)---Number of day remaining----

---select  @Today, @Yesterday, @ThisMonth, @Passed, @Remain
-----ADDED LUC EMOND-----NEED ADDITONAL DATA DAILY-----

----delete today info in table [dbo].[XL_023_MonthlySalesForecast]-----
DELETE [dbo].[XL_023_MonthlySalesForecast]
WHERE CalendarDate >= GETDATE()-1

----insert today info----
;with CTE as(
SELECT 
CalendarDate
,Case When TypeName ='TSFL' Then SUM(amount) Else 0 End as 'TSFL'
,Case When TypeName ='Medirect' Then SUM(amount) Else 0 End as 'MEDIFAST'
, SUM(amount) AS Total
FROM DBO.XL_027_Autoship_Data
where CalendarDate =@Yesterday+1
and CartStatus ='A'
group by TypeName, CalendarDate)

---add daily info into [XL_023_MonthlySalesForecast]-----
INSERT INTO [dbo].[XL_023_MonthlySalesForecast]
Select 
CalendarDate
,SUM(TSFL) as TSFL
,SUM(MEDIFAST) as MEDIFAST
,SUM(Total) AS Total
,0.00 as AutoshipActualTSFL
,0.00 as AutoshipActualMedifast
FROM CTE
GROUP BY CalendarDate

IF object_id('Tempdb..#Auto') Is Not Null DROP TABLE #Auto
----UPDATE AUTOSHIP ACTUAL SALES---
SELECT 
[Posting Date]
,CustomerPostingGroup
,Sum(Amount) as [InvSalesDay]
INTO #AUTO
FROM [BI_SSAS_Cubes].[dbo].[FactSales]
WHERE [Posting Date] >= @Yesterday-5 AND SalesChannel ='AUTO' and CustomerPostingGroup IN('TSFL', 'MEDIFAST')
GROUP BY 
[Posting Date],CustomerPostingGroup
HAVING Sum([Amount]) <> 0


--Select * from [dbo].[XL_023_MonthlySalesForecast]
----UPDATE [dbo].[XL_023_MonthlySalesForecast] FOR THE TOTAL AUTOSHIP-----
Update [dbo].[XL_023_MonthlySalesForecast]
SET AutoShipActualTSFL = isnull([InvSalesDay],0.0)
From #AUTO h
Where [dbo].[XL_023_MonthlySalesForecast].CalendarDate = h.[Posting Date]
and h.CustomerPostingGroup='TSFL'

----UPDATE [dbo].[XL_023_MonthlySalesForecast] FOR THE TOTAL AUTOSHIP-----
Update [dbo].[XL_023_MonthlySalesForecast]
SET AutoShipActualMedifast = isnull([InvSalesDay],0.0)
From #AUTO h
Where [dbo].[XL_023_MonthlySalesForecast].CalendarDate = h.[Posting Date]
and h.CustomerPostingGroup='Medifast'

----step 1-----autoship percent from Ytd sales tsfl medifast---
IF object_id('Tempdb..#Step1') Is Not Null DROP TABLE #Step1
;WITH CTE AS(
SELECT 
 CustomerPostingGroup as CPG
,Case When  SalesChannel='AUTO' THEN Sum(Amount) ELSE 0 END AS SalesAuto
,Case When  SalesChannel<>'AUTO' THEN Sum(Amount) ELSE 0 END AS SalesDemand
,Sum(Amount) AS InvSalesYTD
FROM [BI_SSAS_Cubes].[dbo].[FactSales]
WHERE [Posting Date] >=(Select [FirstDateOfMonth] from BI_Reporting.dbo.calendar where CalendarDate=DateAdd(dd, DateDiff(dd,0,GetDate()-180), 0))---Luc Emond 2014-01-03---
GROUP BY CustomerPostingGroup, SalesChannel
HAVING Sum(Amount) <> 0)
SELECT 
 CPG
,SUM(SalesAuto)/Sum([InvSalesYTD]) AutoPercent
INTO #STEP1
FROM CTE
GROUP BY 
CPG
Having SUM(SalesAuto)/Sum([InvSalesYTD]) <> 0


----step 2-----autoship forecast accuracy TSFL---- 
IF object_id('Tempdb..#Step2') Is Not Null DROP TABLE #Step2
select 
SUM(AutoShipActualTSFL)/SUM(TSFL) AS CalcPercentTSFL
INTO #STEP2
from [BI_Reporting].[dbo].[XL_023_MonthlySalesForecast]

----step 2.1-----autoship forecast accuracy Medifast---- 
IF object_id('Tempdb..#Step21') Is Not Null DROP TABLE #Step21
select 
SUM(AutoShipActualMedifast)/SUM(Medifast) AS CalcPercentTSFL
INTO #STEP21
from [BI_Reporting].[dbo].[XL_023_MonthlySalesForecast]


----step3-----Tsfl  channel mix distribution-----
IF object_id('Tempdb..#Step3') Is Not Null DROP TABLE #Step3
SELECT 
(SELECT 
 Sum([Amount]) AS InvSalesYTD
FROM [BI_SSAS_Cubes].[dbo].[FactSales]
WHERE [Posting Date] >=(Select [FirstDateOfMonth] from BI_Reporting.dbo.calendar 
                        Where CalendarDate=DateAdd(dd, DateDiff(dd,0,GetDate()-180), 0)) and CustomerPostingGroup in ('TSFL')---Luc Emond 2014-01-03---
HAVING Sum([Amount]) <> 0) / Sum([Amount]) AS PercentYTDTSFL
into #step3
FROM [BI_SSAS_Cubes].[dbo].[FactSales]
WHERE [Posting Date] >=(Select [FirstDateOfMonth] from BI_Reporting.dbo.calendar where CalendarDate=DateAdd(dd, DateDiff(dd,0,GetDate()-180), 0))---Luc Emond 2014-01-03---


----step3----- medifast channel  distribution-----
IF object_id('Tempdb..#Step31') Is Not Null DROP TABLE #Step31
SELECT 
(SELECT 
 Sum([Amount]) AS InvSalesYTD
FROM [BI_SSAS_Cubes].[dbo].[FactSales]
WHERE [Posting Date] >=(Select [FirstDateOfMonth] from BI_Reporting.dbo.calendar 
                        Where CalendarDate=DateAdd(dd, DateDiff(dd,0,GetDate()-180), 0)) and CustomerPostingGroup in ('Medifast')---Luc Emond 2014-01-03---
HAVING Sum([Amount]) <> 0) / Sum([Amount]) AS PercentYTDMedifast
into #step31
FROM [BI_SSAS_Cubes].[dbo].[FactSales]
WHERE [Posting Date] >=(Select [FirstDateOfMonth] from BI_Reporting.dbo.calendar where CalendarDate=DateAdd(dd, DateDiff(dd,0,GetDate()-180), 0))---Luc Emond 2014-01-03---

----step 4----month to date sales----
IF object_id('Tempdb..#Step4') Is Not Null DROP TABLE #Step4
SELECT [CustomerPostingGroup]
       ,[CummulativeRevenue]
       ,[Total$Open]
INTO #STEP4       
FROM [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_TY_PostingGroup]
where [Posting Date] = (Select Max([Posting Date]) from [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_TY_PostingGroup])


-----step 5 ---- consumed sales other channel (no TSFL and No Medifast)---
--SELECT Sum([CummulativeRevenue]) *-1 As OtherChannel$
--into #step5
--FROM [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_TY_PostingGroup]
--Where [CustomerPostingGroup] not in ('TSFL', 'MEDIFAST')
--and [Posting Date]= (Select Max([Posting Date]) from [BI_Reporting].[dbo].[XL_023_MonthlySalesTracker_TY])

IF object_id('Tempdb..#StepOpen') Is Not Null DROP TABLE #StepOpen
SELECT [Customer Posting Group],
SUM(L.[Amount Including VAT]) as Total$Open
into  #StepOpen
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Header] H
Join [NAV_ETL].[dbo].[Jason Pharm$Sales Line] L
 ON L.[Document No_] = H.[No_]
WHERE H.[Document Type]=1 AND [Customer Posting Group] NOT IN ('','SPRTNTR')
and  [Coupon_Promotion Code] <>'EXCHANGE'
Group by [Customer Posting Group]------Temp #StepOpen added by Daniel Dagnachew


-------------------Ondemand --------------------------------------
IF object_id('Tempdb..#Step6') Is Not Null DROP TABLE #Step6
SELECT 
 CustomerPostingGroup
,SalesChannel
,Sum([Amount]) AS InvSales
INTO #STEP6
FROM [BI_SSAS_Cubes].[dbo].[FactSales]
WHERE [Posting Date] >= (select CONVERT(Varchar(10),Getdate()-28,121))
group By  CustomerPostingGroup
,SalesChannel
-----master----
IF object_id('Tempdb..#Master') Is Not Null DROP TABLE #Master
Create Table #Master (
[TSFLAuto] [Numeric] (15,2) Default 0.00,
[TsflCalcPercent] [Numeric] (15,6) Default 0.00,
[AutoPercentTSFL] [Numeric] (15,6) Default 0.00,
[AutoForecast$TSFL] [Numeric] (15,2) Default 0.00,
[OnDemandForecast$TSFL] [Numeric] (15,2) Default 0.00,
[OpenOrder$TSFL] [Numeric] (15,2) Default 0.00,
[Actual$TSFL] [Numeric] (15,2) Default 0.00,

[MedifastAuto] [Numeric] (15,2) Default 0.00,
[MedifastCalcPercent] [Numeric] (15,6) Default 0.00,
[AutoPercentMedifast] [Numeric] (15,6) Default 0.00,
[AutoForecast$Medifast] [Numeric] (15,2) Default 0.00,
[OnDemandForecast$Medifast] [Numeric] (15,2) Default 0.00,
[OpenOrder$Medifast] [Numeric] (15,2) Default 0.00,
[Actual$Medifast] [Numeric] (15,2) Default 0.00,

[DoctorsAuto] [Numeric] (15,2) Default 0.00,
[DoctorsCalcPercent] [Numeric] (15,6) Default 0.00,
[AutoPercentDoctors] [Numeric] (15,6) Default 0.00,
[AutoForecast$Doctors] [Numeric] (15,2) Default 0.00,
[OnDemandForecast$Doctors] [Numeric] (15,2) Default 0.00,
[OpenOrder$Doctors] [Numeric] (15,2) Default 0.00,
[Actual$Doctors] [Numeric] (15,2) Default 0.00,

[MWCCAuto] [Numeric] (15,2) Default 0.00,
[MWCCCalcPercent] [Numeric] (15,6) Default 0.00,
[AutoPercentMWCC] [Numeric] (15,6) Default 0.00,
[AutoForecast$MWCC] [Numeric] (15,2) Default 0.00,
[OnDemandForecast$MWCC] [Numeric] (15,2) Default 0.00,
[OpenOrder$MWCC] [Numeric] (15,2) Default 0.00,
[Actual$MWCC] [Numeric] (15,2) Default 0.00,

[FranchiseAuto] [Numeric] (15,2) Default 0.00,
[FranchiseCalcPercent] [Numeric] (15,6) Default 0.00,
[AutoPercentFranchise] [Numeric] (15,6) Default 0.00,
[AutoForecast$Franchise] [Numeric] (15,2) Default 0.00,
[OnDemandForecast$Franchise] [Numeric] (15,2) Default 0.00,
[OpenOrder$Franchise] [Numeric] (15,2) Default 0.00,
[Actual$Franchise] [Numeric] (15,2) Default 0.00,

[INTLAuto] [Numeric] (15,2) Default 0.00,
[INTLCalcPercent] [Numeric] (15,6) Default 0.00,
[AutoPercentINTL] [Numeric] (15,6) Default 0.00,
[AutoForecast$INTL] [Numeric] (15,2) Default 0.00,
[OnDemandForecast$INTL] [Numeric] (15,2) Default 0.00,
[OpenOrder$INTL] [Numeric] (15,2) Default 0.00,
[Actual$INTL] [Numeric] (15,2) Default 0.00,


[ActualTotal$] [Numeric] (15,2) Default 0.00,
[OnDemandTotalForecast] [Numeric] (15,2) Default 0.00

)
insert into  #Master ([TSFLAuto])
values (0.00)

Update #Master set [TSFLAuto] =(select SUM(Amount) From [BI_REPORTING].DBO.XL_027_Autoship_Data where TypeName ='TSFL' and CartStatus ='A' and calendarDate between @FirstDateOfMonth and @LastDateOfMonth)
Update #Master set [TsflCalcPercent] = @TsflPercentCalcManual
Update #Master set AutoPercentTSFL = (select AutoPercent from #step1 where CPG ='TSFL')
Update #Master set [AutoForecast$TSFL] = ([TsflAuto]*@TsflPercentCalcManual)
Update #Master set OpenOrder$TSFL = (Select Total$Open From #StepOpen where [Customer Posting Group]='TSFL')
Update #Master set [OnDemandForecast$TSFL] = ISNULL((SELECT ((SELECT SUM(InvSales) FROM #STEP6 
                                 Where SalesChannel <>'Auto' AND CustomerPostingGroup IN('TSFL'))/ 28) * @Remain), 0)
Update #Master set [Actual$TSFL] =(Select CummulativeRevenue From #STEP4 where CustomerPostingGroup='TSFL')                                    


Update #Master set [MedifastAuto] = (select SUM(Amount) From [BI_REPORTING].DBO.XL_027_Autoship_Data where TypeName ='MEDIRECT' and CartStatus ='A' and calendarDate between @FirstDateOfMonth and @LastDateOfMonth)
Update #Master set [MedifastCalcPercent] = @MedifastPercentCalcManual
Update #Master set [AutoPercentMedifast] = (select AutoPercent from #step1 where CPG ='MEDIFAST')
Update #Master set [AutoForecast$Medifast] = ([MedifastAuto]*@MedifastPercentCalcManual)
Update #Master set OpenOrder$Medifast = (Select Total$Open From #StepOpen where [Customer Posting Group]='MEDIFAST')
Update #Master set [OnDemandForecast$Medifast]= ISNULL((SELECT ((SELECT SUM(InvSales) FROM #STEP6 
                                 Where SalesChannel <>'Auto' AND CustomerPostingGroup IN('Medifast'))/ 28)*@Remain), 0)
Update #Master set [Actual$Medifast] =(Select CummulativeRevenue From #STEP4 where CustomerPostingGroup='Medifast') 

Update #Master set [DoctorsAuto] = 0.0
Update #Master set [DoctorsCalcPercent] = 0.0
Update #Master set [AutoPercentDoctors] = 0.0
Update #Master set [AutoForecast$Doctors] = 0.0
Update #Master set OpenOrder$Doctors = (Select Total$Open From #StepOpen where [Customer Posting Group]='Doctors')
Update #Master set [OnDemandForecast$Doctors]= ISNULL((SELECT ((SELECT SUM(InvSales) FROM #STEP6
                    Where SalesChannel <>'Auto' AND CustomerPostingGroup IN('Doctors'))/ 28)*@Remain), 0)
Update #Master set [Actual$Doctors] =(Select CummulativeRevenue From #STEP4 where CustomerPostingGroup='Doctors') 

Update #Master set [MWCCAuto] = 0.0
Update #Master set [MWCCCalcPercent] = 0.0
Update #Master set [AutoPercentMWCC] = 0.0
Update #Master set [AutoForecast$MWCC] = 0.0
Update #Master set OpenOrder$MWCC = (Select Total$Open From #StepOpen where [Customer Posting Group]='MWCC')
Update #Master set [OnDemandForecast$MWCC]= ISNULL((SELECT ((SELECT SUM(InvSales) FROM #STEP6
                    Where SalesChannel <>'Auto' AND CustomerPostingGroup IN('MWCC'))/ 28)*@Remain),0)
Update #Master set [Actual$MWCC] =(Select CummulativeRevenue From #STEP4 where CustomerPostingGroup='MWCC') 
                                       
Update #Master set [FranchiseAuto] = 0.0
Update #Master set [FranchiseCalcPercent] = 0.0
Update #Master set [AutoPercentFranchise] = 0.0
Update #Master set [AutoForecast$Franchise] = 0.0
UPDATE #Master SET OpenOrder$Franchise = (SELECT Total$Open FROM #StepOpen WHERE [Customer Posting Group]='Franchise')
UPDATE #Master SET [OnDemandForecast$Franchise]= ISNULL((SELECT ((SELECT SUM(InvSales) FROM #STEP6
                    WHERE SalesChannel <>'Auto' AND CustomerPostingGroup IN('Franchise'))/ 28)*@Remain), 0)
UPDATE #Master SET [Actual$Franchise] =(SELECT CummulativeRevenue FROM #STEP4 WHERE CustomerPostingGroup='Franchise') 

UPDATE #Master SET [INTLAuto] = 0.0
UPDATE #Master SET [INTLCalcPercent] = 0.0
UPDATE #Master SET [AutoPercentINTL] = 0.0
UPDATE #Master SET [AutoForecast$INTL] = 0.0
UPDATE #Master SET OpenOrder$INTL = (SELECT Total$Open FROM #StepOpen WHERE [Customer Posting Group]='INTL')
UPDATE #Master SET [OnDemandForecast$INTL]= ISNULL((SELECT ((SELECT SUM(InvSales) FROM #STEP6
                    WHERE SalesChannel <>'Auto' AND CustomerPostingGroup IN('INTL'))/ 28)*@Remain), 0)
UPDATE #Master SET [Actual$INTL] =(SELECT CummulativeRevenue FROM #STEP4 WHERE CustomerPostingGroup='INTL') 
UPDATE #Master SET [ActualTotal$]=  [Actual$TSFL]+[Actual$Medifast] + [Actual$Doctors]+[Actual$MWCC]+[Actual$Franchise] +[Actual$INTL]                                  
UPDATE #Master SET [OnDemandTotalForecast] =[OnDemandForecast$TSFL]+[OnDemandForecast$Medifast]+[OnDemandForecast$Doctors]+[OnDemandForecast$MWCC]+[OnDemandForecast$Franchise]+[OnDemandForecast$INTL]

--------------------------------------------------------OLD FOR JANUARY 2014---------------------
----------NEW FORECAST SALES FROM THOMAS-----------------------------------
----SELECT 
---- CustomerPostingGroup
----,SalesChannel
----,Sum([Amount]) AS InvSales
----INTO #STEP6
----FROM [BI_SSAS_Cubes].[dbo].[FactSales]
----WHERE [Posting Date]  between @FirstDateOfMonth and @LastDateOfMonth
----group By  CustomerPostingGroup
----,SalesChannel

------------UPDATE NEW FORECAST-------------
----UPDATE #Master 
----SET [OnDemandNew] = (SELECT 
----                          ((SELECT SUM(InvSales) FROM #STEP6
----                                       Where SalesChannel <>'Auto' 
----                                       AND CustomerPostingGroup IN('TSFL', 'MEDIFAST'))/ @PASSED)*@Remain) 
----UPDATE #Master 
----SET [OtherChannelNew] = (SELECT
----                           ((SELECT SUM(InvSales) FROM #STEP6
----                              Where CustomerPostingGroup NOT IN('TSFL', 'MEDIFAST'))/ @PASSED)*@Remain)

---Final---
TRUNCATE TABLE  [Bi_Reporting].[dbo].[XL_023_MonthlyForecastView]
INSERT INTO  [Bi_Reporting].[dbo].[XL_023_MonthlyForecastView]
SELECT *  FROM #Master

------SELECT * FROM #Master
----SELECT * FROM [Bi_Reporting].[dbo].[XL_023_MonthlyForecastView]


