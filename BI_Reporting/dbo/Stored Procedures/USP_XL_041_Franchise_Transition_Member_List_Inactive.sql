﻿



CREATE PROCEDURE [dbo].[USP_XL_041_Franchise_Transition_Member_List_Inactive] as 

SET NOCOUNT ON;

/*
=============================================================================================
Author:      Luc Emond
Create date: 12/04/2013
-------------------------[dbo].[USP_XL_041_MWCC_MarketingList_Inactive] -----------------------------
Provide Monthly Inactive Customer Information to Marketing Group                
===========================================================================================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------------------------------------------------------------------------------
[Book4Time_ETL]       [B4T_transaction_log_header]              Select
[Book4Time_ETL]       [B4T_transaction_log_Detail]              Select
[Book4Time_ETL]       [B4T_location]                            Select
[Book4Time_ETL]       [B4T_region_locations]                    Select
[Book4Time_ETL]       [B4T_region]                              Select
[Book4Time_ETL]       [B4T_Customer]                            Select
[Book4Time_ETL]       [B4T_Person]                              Select
[Book4Time_ETL]       [B4T_Address]                             Select
[Zip_codes_deluxe]    [ZipCodeDatabase_DELUXE]                  Select  
=============================================================================================================================================================
REVISION LOG
Date           Name                  Change
----------------------------------------------------------------------------------------------------------------------
------------------------------------
08/21/2014	D. SMith				Added Via Mail to t-sql per ticket #1440178
08/21/2014	D. Smith				Made changes to allow for groupping on Customer ID. Removed the join to														dbo.B4T_Phone at the end and instead added updates to phone1, phone2, 
									and	phone3 after the grouping so that only one line would be displayed
11/30/2015  M.Haile					Duplicate Column(LastTransaction) is removed to as SSRS does not allow

==============================================================================================================================================================
NOTES:
--------------------------------------------------------------------------------

==================================================================================
*/


-----DATE RANGE------------------
Declare @CalendarMonthID int, @Date1 datetime, @Date2 datetime 
                        
Set @Date1 = (Select Convert(Varchar(10), Getdate()-35,121)) ------5 weeks ago define by Marketing Group-----
set @Date2 ='2011-01-01' ----DATE PROVIDED BY JENNIFER CRUISE------                                     

----LIST OF CUSTOMERS------
IF object_id('tempdb..#CustList') is not null DROP TABLE #CustList
Select 
Customer_id
Into #custList 
From [Book4Time_ETL].[dbo].[B4T_transaction_log_header] h
WHERE (Select Convert(Varchar (10),Transaction_Date, 121))>= @Date1 and [Transaction_Type] In ('S', 'R') 
group by customer_id

----MASTER LIST OF SALES--------
IF object_id('tempdb..#A') is not null DROP TABLE #A
SELECT
Region_name
,Location_Name
,Parent_Id
,h.Customer_id
,First_Name
,Last_Name
,Email
,'          ' as Phone1
,'          ' as Phone2
,'          ' AS Phone3
,Via_Email
,via_mail
,MAX(Convert(Varchar (10),Transaction_Date, 121)) as LastTransaction
INTO #A
From [Book4Time_ETL].[dbo].[B4T_transaction_log_header] h
Join [Book4Time_ETL].[dbo].[B4T_transaction_log_Detail] d
on h.transaction_id = d.transaction_id
Left Outer Join [Book4Time_ETL].[dbo].[B4T_location]  L
    ON h.location_id = L.location_id  
Left Outer Join [Book4Time_ETL].[dbo].[B4T_person]  p
    on h.customer_id = p.person_id
Left Outer Join [Book4Time_ETL].[dbo].[B4T_region_locations] rl
    on h.location_id= rl.location_id
Left Outer Join [Book4Time_ETL].[dbo].[B4T_region] r
    on rl.region_id = r.region_id 
Left Outer Join [Book4Time_ETL].[dbo].[B4T_Customer] c
    on h.Customer_id = c.customer_id     
WHERE (Select Convert(Varchar (10),Transaction_Date, 121))>= @Date2 and [Transaction_Type] In ('S', 'R') 
group by
Region_name
,Location_Name
,h.Customer_id
,First_Name
,Last_Name
,Email
,Parent_Id
,via_email
,via_mail

----DELETE UN NEEDED INFO----
Delete #a Where customer_id In (Select customer_id From #custList)
Delete #a Where customer_id is null

----ALTER TABLE AND UPDATES TO ADD CUSTOMER INFO----
ALTER TABLE #A
ADD [Parent1] bigint, Street Char(50), City Char(25), Country Char(3), Postal_Code Char(15), State Char(25)

Update #A
set [Parent1] = parent_id 
where #A.customer_id = #a.parent_id

UPDATE #A
SET [Parent1] = p.Parent_id
From [Book4Time_ETL].[dbo].[B4T_Customer]p
where #a.parent_id = p.customer_id
and [Parent1] is null

UPDATE #A
SET First_Name = pe.first_name, Last_Name = pe.Last_Name, Email = pe.Email
from [Book4Time_ETL].[dbo].[B4T_person]  pe
   where #a.[parent1] = pe.person_id

Update #A
Set Street = n.Street, City = N.City, Country = N.Country, Postal_Code = N.Postal_Code
From [Book4Time_ETL].[dbo].[B4T_Address] N
Where #A.[Parent1] = n.Address_id
and N.address_type='4'

Update #a
set State = z.State
From [Zip_codes_deluxe].dbo.[ZipCodeDatabase_DELUXE] Z
where Left(#a.Postal_Code,5)= z.[ZipCode] and z.primaryRecord ='p'


--08/21/2014 - D.Smith  - Added update sttaemenst for Phone1, Phone2, and Phone3
UPDATE #A
SET Phone1 = LEFT(phone_number,10)
From #a 
Left Outer Join [Book4Time_ETL].dbo.B4T_Phone d
   on d.phone_id=Customer_id
WHERE phone_type=1

UPDATE #A
SET Phone2 = LEFT(phone_number,10)
From #a 
Left Outer Join [Book4Time_ETL].dbo.B4T_Phone d
   on d.phone_id=Customer_id
WHERE phone_type=2

UPDATE #A
SET Phone3 = LEFT(phone_number,10)
From #a 
Left Outer Join [Book4Time_ETL].dbo.B4T_Phone d
   on d.phone_id=Customer_id
WHERE phone_type=3

----FINAL----
Select 
 Region_Name
,Location_Name
,Customer_Id
,Parent_Id
,[Parent1]
,LastTransaction
,First_Name
,Last_Name
,Street
,City
,State
,Postal_Code
,Email
,Phone1
,Phone2
,Phone3
/* D. Smith 08/21/2014 Commeneted out as replaced by update statements */
--,Case when phone_type=1 then phone_number else ' ' end as Phone1
--,Case when phone_type=2 then phone_number else ' ' end as Phone2
--,Case when phone_type=3 then phone_number else ' ' end as Phone3
--,LastTransaction
,Case When Via_Email =0 Then 'No' Else 'Yes' End Via_Email 
,Case When Via_Mail =0 Then 'No' Else 'Yes' End as Via_Mail
From #a 
/* D. Smith 08/21/2014 Commeneted out as replaced by update statements */
--Left Outer Join [Book4Time_ETL].dbo.B4T_Phone d
--   on d.phone_id=Customer_id
order by #a.LastTransaction




