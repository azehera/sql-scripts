﻿
/*
=================================================================================================================================
Author:         Luc Emond
Create date: 01/29/2014

=================================================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
----------------------------------------------------------------------------------------------------------------------------------
Book4Time_ETL			B4T_transaction_log_detail			      Select
Book4Time_ETL			B4T_transaction_log_header			      Select   
Book4Time_ETL			B4T_product_master					      Select   
Book4Time_ETL			B4T_product_class					      Select  
Book4Time_ETL			B4T_customer						      Select
Book4Time_ETL			B4T_location						      Select 
Book4Time_ETL			B4T_Person							      Select 
BI_Reporting            Calendar                                  Select
BI_Reporting            XL_053_MWCC_Program_Sales                 Insert
==================================================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------------------------------------
2014-03-21          Luc Emond              Update Report for first order  <> 0
2014-03-26          Luc Emond              Update New Member to be into the last 30 days 
2014-04-08          Luc Emond              Added join to final data to have the correct Item_Description
2014-06-18          Luc Emond              Changed the 30 days to 15 days per Christos
===================================================================================================================================
NOTES:
-----------------------------------------------------------------------------------------------------------------------------------
===================================================================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_055_MWCC_Program_Sales] as 
set nocount on ; 

IF object_id('tempdb..#A') is not null DROP TABLE #A
SELECT 
 Convert(varchar(10),[transaction_date],121) as CalDate
,Convert(varchar(10),[transaction_date]-15,121) as CalDate15Days----2014-03-26 Luc Emond & ----2014-06-18 Luc Emond 15 days---
,a.location_id
,location_code
,location_name
,a.Transaction_Id
,Item_code
,item_desc
,class_desc
,transaction_type
,ROW_NUMBER() OVER(PARTITION BY a.Customer_id  ORDER BY Convert(varchar(10),[transaction_date],121),a.Transaction_Id asc) AS Rowid_ASC
,SUM(b.sold_price * b.qty)as Dollars
,a.Customer_id
,Parent_id
INTO #A
FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_header] a
  left Join [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] b
  on a.transaction_id = b.transaction_id
  left Join [Book4Time_ETL].[dbo].[B4T_product_master] c
  on b.item_code = c.sku
  left Join [Book4Time_ETL].[dbo].[B4T_product_class] d
  on c.class_id = d.class_id
  left Join [Book4Time_ETL].[dbo].[B4T_Customer] c1
  on a.customer_id = c1.customer_id
  left Join [Book4Time_ETL].[dbo].[B4T_LOCATION] L
  on a.location_id = l.location_id
Where Convert(Varchar(10),a.transaction_date,121)>='2011-01-01' and c.class_id='3620036' and transaction_type in('R', 'S')
Group by 
 Convert(varchar(10),[transaction_date],121) 
,Convert(varchar(10),[transaction_date]-15,121)----2014-06-18 Luc Emond
,a.location_id
,a.Transaction_Id
,Item_code
,item_desc
,class_desc
,transaction_type
,a.Customer_id
,parent_id
,location_code
,location_name
Order By a.customer_id, Convert(varchar(10),[transaction_date],121)


ALTER TABLE #A
ADD ParentIdOfParentId BIGINT, CustomerName Char(50), FirstInvoiceDate Datetime, CalDateMonthId int, FirstDateMonthId int, NewMember Char(1), SameMonthId Char(1)

UPDATE #A SET ParentIdOfParentId = parent_id WHERE customer_id = parent_id AND ParentIdOfParentId IS NULL

UPDATE #A
SET ParentIdOfParentId = c2.Parent_id
FROM [Book4Time_ETL].[dbo].[B4T_Customer] c2
WHERE #A.parent_id = C2.customer_id
AND ParentIdOfParentId IS NULL

UPDATE #A
SET CustomerName = P.first_name+' ' +P.last_name
FROM [Book4Time_ETL].[dbo].[B4T_Person]p
WHERE #A.ParentIdOfParentId = p.person_id

--------------------------------------------remove from script per meeting with Christos---------------------------------------------------014-03-21 Luc Emond
----------IF object_id('tempdb..#b') is not null DROP TABLE #b
----------select Customer_id, MIN(Convert(Varchar(10),Transaction_Date,121)) MinDate
----------into #b
----------FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_header] a
----------where customer_id in(select distinct customer_id from #A)  
----------group by Customer_id

----------UPDATE #A
----------SET FirstInvoiceDate = l.MinDate
----------from #b L
----------Where #a.customer_id = l.customer_id


--------------------------------------new script to be <> 0 per meeting with Chrstos---------------------------------------------014-03-21 Luc Emond
IF object_id('tempdb..#b') is not null DROP TABLE #b
select 
Convert(varchar(10),[transaction_date],121) as CalDate
,Customer_id
,a.Transaction_Id 
,ROW_NUMBER() OVER(PARTITION BY a.Customer_id  ORDER BY Convert(varchar(10),[transaction_date],121),a.Transaction_Id asc) AS Rowid_ASC
,SUM(b.sold_price * b.qty)as Dollars
into #b
FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_header] a
  left Join [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] b
  on a.transaction_id = b.transaction_id
where customer_id in(select distinct customer_id from #A)  
group by Convert(varchar(10),[transaction_date],121) 
,Customer_id
,a.Transaction_Id 
having SUM(b.sold_price * b.qty)<>0

UPDATE #A
SET FirstInvoiceDate = l.CalDate
from #b L
Where #a.customer_id = l.customer_id
and l.Rowid_ASC = 1

UPDATE #A SET NewMember = 'N'

UPDATE #A 
SET NewMember ='Y'
WHERE convert(Varchar(10),FirstInvoiceDate,121) Between CalDate15Days and CalDate----2014-03-26 Luc Emond  and -2014-06-18 Luc Emond 15 days----

UPDATE #A
SET FirstDateMonthId = CA.CalendarMonthID
FROM DBO.calendar CA
Where #a.FirstInvoiceDate = ca.CalendarDate

UPDATE #A
SET CalDateMonthId = CA.CalendarMonthID
FROM DBO.calendar CA
Where #a.CalDate = ca.CalendarDate

UPDATE #A  set SameMonthId = 'N'

UPDATE #A 
SET SameMonthId ='Y'
WHERE FirstDateMonthId = CalDateMonthId

----FINAL TABLE-----
TRUNCATE TABLE dbo.XL_055_MWCC_Program_Sales
INSERT INTO dbo.XL_055_MWCC_Program_Sales
SELECT 
 CalDate
,CalDate15Days -----2014-06-18 Luc Emond
,location_id
,location_code
,location_name
,Transaction_Id
,Item_code
,pm.Product_name as item_desc
,class_desc
,transaction_type
,Rowid_ASC
,Dollars
,Customer_id
,Parent_id
,ParentIdOfParentId
,CustomerName
,FirstInvoiceDate As FirstInvoiceDateNoneZero
,CalDateMonthId
,FirstDateMonthId
,NewMember as NewMemberLast15Days -----2014-06-18 Luc Emond
,SameMonthId
,YEAR(CalDate) AS CalYear
,Datepart(QQ,CalDate) AS CalQuarter
,MONTH(Caldate) CalMonth
FROM #A z
join [Book4Time_ETL].[dbo].[B4T_product_master] pm---Luc Emond 2014-04-08 to fix the Lenght of the item_description------
on z.item_code = pm.sku

WHERE customer_id IS NOT NULL 
ORDER BY ParentIdOfParentId, CalDate

----select * from dbo.XL_055_MWCC_Program_Sales