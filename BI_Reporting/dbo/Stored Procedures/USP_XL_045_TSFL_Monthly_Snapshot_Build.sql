﻿
/*
==========================================================================================
Author:         Emily Trainor
Create date: 08/02/2012
-------------------------TSFL Monthly Snapshot View Build---------------------------------
---------------Pull monthly figures for TSFL Executive Report-----------------------------
==========================================================================================   
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------
etrainor				ACTIVE EARNING COACHES					   Select
etrainor				TSFL_monthlyactive_data					   Select     
BI_Reporting				CALENDAR_TSFL								   Select                          
===========================================================================================
REVISION LOG
Date           Name                          Change
-------------------------------------------------------------------------------------------
2014-02-06  Luc Emond               Modify the Store Procedure to exclude current Month 
==========================================================================================
NOTES:
------------------------------------------------------------------------------------------
==========================================================================================
*/

CREATE procedure [dbo].[USP_XL_045_TSFL_Monthly_Snapshot_Build] as 
set nocount on ;

-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = (SELECT DATEADD(yy, DATEDIFF(yy,0,GETDATE())-1, 0)) ---First Of Last Year----

-------------------------Delete Current Year Data--------------------------
Delete dbo.TSFL_monthly_snapshot
Where [Last Day of Month] >= @Date

Select 
	   [Last Day of Month]
	  ,[LastDateOfQuarter]
      ,A.[CalendarYear]
      ,A.[CalendarMonth]
      ,[Active Earners]
      ,B.[Total Health Coaches]
      ,([Active Earners]/B.[Total Health Coaches]) as [% of Earning Coaches]
      ,B.[Sponsoring]
      ,((C.[Total Health Coaches]+ B.Sponsoring)- B.[Total Health Coaches]) as [Attrition]
      --,C.[Calendar Month] as [Prior Month]
      --,C.[Total Health Coaches] as [Prior Month Coaches]
      ,(((C.[Total Health Coaches]+ B.Sponsoring)- B.[Total Health Coaches])/B.[Total Health Coaches]) as [Attrition %]
      --,[TSFL Sales]
      --,[TSFL Shipping Revenue]
      --,[Total Revenue]
      ,B.[New Clients]
      --,B.[Orders]
      ,(B.[Orders]/[Active Earners]) as [Orders per Coach]
      ,[Revenue Per Coach]
into #all
    FROM [dbo].[Active Earning Coaches] A
		Inner join [dbo].[TSFL_monthlyactive_data] B
			on A.[Last Day of Month] = B.[Calendar Month]
		Left join [dbo].[TSFL_monthlyactive_data] C
			on B.[Calendar Month] = (C.[Calendar Month]+30)
			or B.[Calendar Month] = (C.[Calendar Month]+31)
			or B.[Calendar Month] = (C.[Calendar Month]+28)
		    or B.[Calendar Month] = (C.[Calendar Month]+29)
		Inner Join [BI_Reporting].[dbo].[Calendar] D
			on A.[Last Day of Month] = D.LastDateOfMonth 
  Where [Last Day of Month] >= @DATE
	Group by 
			[LastDateOfQuarter]
			,[Last Day of Month]
			,A.[CalendarYear]
		    ,A.[CalendarMonth]
			,[Active Earners]
			,B.[Total Health Coaches]
			,C.[Total Health Coaches]
			,B.[Sponsoring]
			,B.[New Clients]
			,B.[Orders]
			,[Revenue Per Coach]
	Order by [Last Day of Month]
	
-----------Insert information into table---------------
Insert into dbo.TSFL_monthly_snapshot
Select * from #all	
where [Last Day of Month] <(SELECT CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH,0,GETDATE()),0),121))-----Luc Emond 201-02-06 Exclude Current Month----	


drop table #all
