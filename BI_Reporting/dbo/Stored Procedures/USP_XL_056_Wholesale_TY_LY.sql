﻿
/*
=================================================================================================================================
Author:         Menkir Haile
Create date: 7/28/2015

=================================================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
----------------------------------------------------------------------------------------------------------------------------------
[BI_SSAS_Cubes]         FactSales			                      Select
[BI_SSAS_Cubes]         DimSelltoCustomerKey			          Select
[BI_SSAS_Cubes]         DimShiptoCustomerKey			          Select
[Bi_Reporting]			calendar								  Select
[ECOMM_ETL ] USER_ACCOUNT							  Select
==================================================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------------------------------------

===================================================================================================================================
NOTES:
-----------------------------------------------------------------------------------------------------------------------------------
===================================================================================================================================
*/

CREATE procedure [dbo].[USP_XL_056_Wholesale_TY_LY] as 
set nocount on ;

Select Distinct
 [CustomerPostingGroup]
,a.[SelltoCustomerID]
,[Sell-to Customer Name]
,[Sell-to City]
,[Sell-to County]
,b.[Zip5] as SellToZip5
,[Ship-to Customer Name]
,[Ship-to City]
,[Ship-to County]
,b1.[Zip5] as ShipToZip5
,USA_HOME_PH
,USA_EMAIL
,Year([Posting Date]) as CalYear
,CalendarMonthName
,CalendarMonth
,Case When Year([Posting Date]) = YEAR(getdate())-1 then Sum(Amount) Else 0 end as PreviousYear
,Case When Year([Posting Date]) =YEAR(getdate()) then Sum(Amount) Else 0 end as CurrentYear   
from  [BI_SSAS_Cubes].dbo.FactSales a
join [BI_SSAS_Cubes].[dbo].[DimSelltoCustomerKey] b
on a.SelltoCustomerKey = b.SelltoCustomerKey
Join [BI_SSAS_Cubes].[dbo].[DimShiptoCustomerKey] b1
on a.ShiptoCustomerKey = b1.ShiptoCustomerKey
Join [Bi_Reporting].dbo.calendar c
on a.[Posting Date] = c.CalendarDate    
Left join [ECOMM_ETL ].[dbo].[USER_ACCOUNT] ua
on a.[SelltoCustomerID] = ua.USA_CUST_NBR
where [Posting Date] between (Select DATEADD(YY, DATEDIFF(YY,0,getdate()) - 1, 0)) and (Select  DATEADD(dd, -1, DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()) + 1, 0)))
and [CustomerPostingGroup]='DOCTORS'
Group by 
 [CustomerPostingGroup]
,a.[SelltoCustomerID]
,[Sell-to Customer Name]
,[Sell-to City]
,[Sell-to County]
,b.[Zip5]
,[Ship-to Customer Name]
,[Ship-to City]
,[Ship-to County]
,b1.[Zip5]
,Year([Posting Date]) 
,CalendarMonthId
,CalendarMonthName
,CalendarMonth
,USA_EMAIL
,USA_HOME_PH
