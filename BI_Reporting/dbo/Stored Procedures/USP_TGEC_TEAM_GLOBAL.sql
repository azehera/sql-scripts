﻿
/*
--==================================================================================
Author:        Daniel Dagnachew
Create date: 12/02/2015
-----------------------------Odyssey_ETL--------------------------------------------
Create last six months global director and Presidential directors earning analysis
--==================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------
[ODYSSEY_ETL]      dbo.ODYSSEY_BUSINESSCENTER                  SELECT
[ODYSSEY_ETL]      dbo.ODYSSEY_CUSTOMER                        SELECT
[ODYSSEY_ETL]      dbo.ODYSSEY_CORE_COMMISSION_ENTRY           SELECT
[ODYSSEY_ETL]      dbo.ODYSSEY_TSFL_COMM_ENTRY_WEEKLY          SELECT                        
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/



/****** Script for SelectTopNRows command from SSMS  ******/
CREATE Procedure [dbo].[USP_TGEC_TEAM_GLOBAL]
as 

Declare @today datetime
Declare @SixMonth datetime
set @today = (Select getdate())
set @SixMonth = (Select Dateadd(Month, Datediff(Month, 0, DATEADD(m, -6, @today)), 0) )



IF object_id('Tempdb..#CUSTOMER') Is Not Null DROP TABLE #CUSTOMER
 Select distinct 
       BC.Customer_ID
	  ,[LAST_NAME]
	  ,[FIRST_NAME]
	  ,[COMMISSION_PERIOD]
	  ,[Rank]
 INTO #CUSTOMER
 From [ODYSSEY_ETL].dbo.ODYSSEY_BUSINESSCENTER BC
 join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
     on BC.CUSTOMER_ID = C.CUSTOMER_ID
 where [Rank] in ('GLOBAL_DIRECTOR','FI_GLOBAL_DIRECTOR','PRESIDENTIAL_DIRECTOR','FI_PRESIDENTIAL_DIRECTOR')
 and [COMMISSION_PERIOD] between  @SixMonth and @today
 

IF object_id('Tempdb..#GV') Is Not Null DROP TABLE #GV

 Select C.CUSTOMER_ID,C.[COMMISSION_PERIOD],sum(Volume_Value) GV
 INTO #GV
 From [ODYSSEY_ETL].dbo.ODYSSEY_BUSINESSCENTER BC
 JOIN ODYSSEY_ETL.dbo.ODYSSEY_VOLUME (NOLOCK) V
	        ON BC.BUSINESSCENTER_ID = V.NODE_ID AND V.PERIOD_END_DATE = BC.COMMISSION_PERIOD
 JOIN #CUSTOMER  C
	        ON BC.CUSTOMER_ID = C.CUSTOMER_ID AND C.COMMISSION_PERIOD = BC.COMMISSION_PERIOD
 WHERE V.VOLUME_TYPE = 'GV'
 GROUP BY C.CUSTOMER_ID,C.[COMMISSION_PERIOD]

IF object_id('Tempdb..#TOTALEARNING') Is Not Null DROP TABLE #TOTALEARNING
;With Cte as (
SELECT A.[CUSTOMER_ID]
      ,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,[COMMISSION_DATE])+1,0))as [Commission_Period]
      ,isnull([TOTAL_ADJUSTMENT],0) + isnull([GROWTH_BONUS],0) + isnull([GENERATION_BONUS],0)+ isnull([OVERRIDE_BONUS],0) + isnull([CSC_AMOUNT],0) 
       + isnull([CAB_BONUS],0) + isnull([ASSIST_BONUS],0) + ISNULL([ROLLING_BONUS],0)+ isnull([OVERRIDE_BONUS_GED],0)+ isnull([OVERRIDE_BONUS_PED],0)
       + isnull([CERTIFIED_BONUS],0)+ Isnull([CUSTOMER_SUPPORT_BONUS],0)+ Isnull([FIBC],0)+ Isnull([ANSITION_ADJ],0) as [Total Earnings]
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] A (NOLOCK)
inner join[ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] B (NOLOCK)
 on A.[COMMISSION_ENTRY_ID] = B.[COMMISSION_ENTRY_ID]
Inner join [BI_Reporting].[dbo].[calendar_TSFL] C (NOLOCK)
 on Convert(varchar(10),A.[COMMISSION_DATE],121) = C.[CalendarDate]
where A.[COMMISSION_DATE] between  @SixMonth and @today
AND A.Customer_ID IN (SELECT DISTINCT CUSTOMER_ID FROM #CUSTOMER)

--union all

--SELECT [CUSTOMER_ID]
--	 ,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,[COMMISSION_DATE])+1,0))as [Commission_Period]
--     ,([CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]+[TOTAL_ADJUSTMENT]) as [Total Earnings]
--FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY] A (NOLOCK)
--Inner join [BI_Reporting].[dbo].[calendar_TSFL] B (NOLOCK)
-- on Convert(date,A.[COMMISSION_DATE]) = B.[CalendarDate]
--where A.[COMMISSION_DATE] between  @SixMonth and @today
--AND A.Customer_ID IN (SELECT DISTINCT CUSTOMER_ID FROM #CUSTOMER)
	) 

Select DISTINCT C.CUSTOMER_ID,C.Commission_Period, sum([Total Earnings]) as [Total Earnings]
into #TOTALEARNING
from Cte C
group by C.CUSTOMER_ID,C.Commission_Period
order by C.CUSTOMER_ID,C.Commission_Period


;with CTE as (SELECT customer_id,COUNT(CUSTOMER_ID) [Coach Count] 
from #CUSTOMER GROUP BY CUSTOMER_ID
)

Select DISTINCT
       T.Customer_ID
	  ,CUSTOMER_NUMBER
	  ,C.[LAST_NAME]
	  ,C.[FIRST_NAME]
	  ,T.[Commission_Period]
	  ,[Rank]
	  , GV
	  ,[Total Earnings] Commission
	  ,CTE.[COACH COUNT] 

from #TOTALEARNING T
 join #GV G on T.CUSTOMER_ID = G.CUSTOMER_ID and T.COMMISSION_PERIOD = G.COMMISSION_PERIOD
 join #CUSTOMER C on C.CUSTOMER_ID = T.CUSTOMER_ID and C.COMMISSION_PERIOD = T.COMMISSION_PERIOD
 join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] Cu on C.CUSTOMER_ID = Cu.CUSTOMER_ID
 join  CTE on T.CUSTOMER_ID = CTE.CUSTOMER_ID
where CTE.[COACH COUNT] > 2 
and T.Customer_ID in (Select distinct Customer_ID from(Select CUSTOMER_ID,max([GV]) Maxgv
													from #GV
													group by CUSTOMER_ID
													Having max([GV]) > 100000)gv) 
and T.Customer_ID in (Select distinct Customer_ID from(Select CUSTOMER_ID,max([Total Earnings]) Comm
                                                       from #TOTALEARNING 
                                                       group by CUSTOMER_ID
                                                       Having max([Total Earnings]) > 10000)Cm) 
ORDER BY T.Customer_ID,T.[Commission_Period]
