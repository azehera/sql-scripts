﻿






CREATE Procedure [dbo].[USP_XL_040_LY_Q4]
as

declare @EndYear Datetime

Set @EndYear=(Select distinct [LastDateOfYear] from BI_Reporting.dbo.calendar
where CalendarYear=(YEAR(Getdate()-1)-1))


--EXECUTE AS LOGIN = 'ReportViewer'

Truncate table BI_Reporting.dbo.[XL_040_LY_Q4]
INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT 
'Jason Pharm' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]  
Where [Posting Date] <= @EndYear
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]


UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] 
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].[dbo].[Jason Pharm$G_L Account] d
WHERE [XL_040_LY_Q4].[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.[XL_040_LY_Q4] where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT
Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[Current Year Retained Earnings] as Amount
from BI_Reporting.dbo.XL_040_BalanceSheetLY
where Entity='Jason Pharm'


-----------------------------------------7 Crondall-------------------------------------------------------


INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT 
'7 Crondall' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[7 Crondall$G_L Entry]  a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]  
Where [Posting Date] <= @EndYear
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]


UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] 
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].dbo.[7 Crondall$G_L Account] d
WHERE [XL_040_LY_Q4].[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.[XL_040_LY_Q4] where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT
Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[Current Year Retained Earnings] as Amount
from BI_Reporting.dbo.XL_040_BalanceSheetLY
where Entity='7 Crondall'


----------------------------------------Jason Ent-----------------------------------------------------------------




INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT 
'Jason Ent' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Enterprises$G_L Entry]  a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]  
Where [Posting Date] <= @EndYear
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]


UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] 
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].dbo.[Jason Enterprises$G_L Account] d
WHERE [XL_040_LY_Q4].[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.[XL_040_LY_Q4] where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT
Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[Current Year Retained Earnings] as Amount
from BI_Reporting.dbo.XL_040_BalanceSheetLY
where Entity='Jason Ent'


-----------------------------------------------Jason Prop--------------------------------------------------------------

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT 
'Jason Prop' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Jason Properties$G_L Entry]  a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]  
Where [Posting Date] <= @EndYear
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]


UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] 
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].dbo.[Jason Properties$G_L Account] d
WHERE [XL_040_LY_Q4].[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.[XL_040_LY_Q4] where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT
Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[Current Year Retained Earnings] as Amount
from BI_Reporting.dbo.XL_040_BalanceSheetLY
where Entity='Jason Prop'


---------------------------------------------Medifast--------------------------------------------------------------------
INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT 
'Medifast' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Medifast Inc$G_L Entry] a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]  
Where [Posting Date] <= @EndYear
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]


UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] 
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].dbo.[Medifast Inc$G_L Account] d
WHERE [XL_040_LY_Q4].[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.[XL_040_LY_Q4] where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT
Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[Current Year Retained Earnings] as Amount
from BI_Reporting.dbo.XL_040_BalanceSheetLY
where Entity='Medifast'



-------------------------------------------TSFL--------------------------------------------------------------------

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT 
'TSFL' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,sum([Amount]) as Amount
FROM [NAV_ETL].[dbo].[Take Shape For Life$G_L Entry] a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]  
Where [Posting Date] <= @EndYear
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]


UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] 
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].dbo.[Take Shape For Life$G_L Account] d
WHERE [XL_040_LY_Q4].[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.[XL_040_LY_Q4] where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT
Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[Current Year Retained Earnings] as Amount
from BI_Reporting.dbo.XL_040_BalanceSheetLY
where Entity='TSFL'

--delete from BI_Reporting.dbo.[XL_040_LY_Q4]
--where [G_L Account No_]='34400' and RollUpName is null

-------------------------------------------Medifast Nutrition --------------------------------------------------------------------

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT 
'Medifast Nutrition' as Entity
,a.[G_L Account No_]
,[G_L Name]
,[RollUpName]
,sum([Amount]) as Amount
FROM [NAV_ETL].dbo.[Medifast Nutrition$G_L Entry] a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
  on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = e.[G_L Account No_]  
Where [Posting Date] <= @EndYear
and a.[G_L Account No_] BETWEEN '10000' AND '39999'
Group by
a.[G_L Account No_]
,[G_L Name]
,[RollUpName]


UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] 
SET [G_L Name] = D.[Name]
FROM  [NAV_ETL].dbo.[Medifast Nutrition$G_L Account] d
WHERE [XL_040_LY_Q4].[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
and [G_L Name] IS NULL

delete BI_Reporting.dbo.[XL_040_LY_Q4] where [G_L Account No_]='1000.01'

UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Affiliate Receivables' Where [G_L Account No_] in ('12505', '12700')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Pre-Paid Expenses' Where [G_L Account No_] in ('15950')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Other Assets' Where [G_L Account No_] in ('19199')
UPDATE BI_Reporting.dbo.[XL_040_LY_Q4] SET RollUpName ='Capital Stock' Where [G_L Account No_] in ('32001')

INSERT INTO BI_Reporting.dbo.[XL_040_LY_Q4](Entity,[G_L Account No_], [G_L Name], [RollUpName], [Amount]) 
SELECT
Entity
,'34400' AS [G_L Account No_]
,'Current Year Retained Earnings' as  [G_L Name]
,'Current Year Retained Earnings' as [RollUpName]
,[Current Year Retained Earnings] as Amount
from BI_Reporting.dbo.XL_040_BalanceSheetLY
where Entity='Medifast Nutrition'

delete from BI_Reporting.dbo.[XL_040_LY_Q4]
where [G_L Account No_]='34400' and RollUpName is null



