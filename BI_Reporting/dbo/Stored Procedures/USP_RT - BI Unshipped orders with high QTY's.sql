﻿





/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                                        Action            
------------------------------------------------------------------------------------

[NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Line]                     Select 
[NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Header]                   Select
[Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE]							 Select
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - BI Unshipped orders with high QTY's]
AS
 SELECT DISTINCT
       [Document No_]
      ,sl.[Sell-to Customer No_]
      ,sl.[No_] AS 'SKU'
	  ,sl.Description AS 'SKU Name'
      ,CAST([Quantity] AS DECIMAL (10,0)) AS 'QTY'
	  ,[Customer Posting Group]
	  ,sh.[Ship-to County] AS 'Ship State'
	  ,sh.[Ship-to Post Code] AS 'Ship Zip'
	  ,sh.[Bill-to Address] AS 'Bill Address'
	  ,sh.[Bill-to County] AS 'Billing State'
	  ,sh.[Bill-to Post Code] AS 'Billing Zip'
	  ,sh.[Location Code]
	  ,CONVERT(VARCHAR, [Order Date], 107) AS 'Order Date'
	  ,CONVERT(VARCHAR, [sh].[Posting Date],107) AS 'Posting Date'
	  ,FORMAT([Order Time], 'hh:mm tt') AS 'Order Time'
	  ,z.Latitude [Warehouse Latitude]
	  ,z.Longitude [Warehouse Longitude]
	  ,z1.Latitude [Ship Latitude]
	  ,z1.Longitude [Ship Longitude]

  FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Line] sl 
  JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Header] sh  ON sl.[Document No_]  = sh.[No_]
  LEFT JOIN [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] z 
  ON z.[ZipCode] = CASE WHEN sh.[Location Code] = 'MDCWMS' THEN 21660 WHEN sh.[Location Code] = 'RDCWMS' THEN 89434 END
  LEFT JOIN [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] z1 
  ON LEFT(sh.[Ship-to Post Code],5) COLLATE DATABASE_DEFAULT  = z1.[ZipCode] AND sh.[Ship-to County] COLLATE DATABASE_DEFAULT  = z1.[State] 
   
  WHERE (Quantity > 14 OR sh.[Amount Authorized] > 1400)
  AND sl.No_ <> 'HOLCRD'
  AND [Type] = 2
  AND [Document No_] NOT LIKE 'SRA%'
  AND [Customer Posting Group] <> 'FRANCHISE'
  AND TRY_PARSE(sh.[Bill-to Customer No_] AS BIGINT) > 20000000000
  --order by sh.[Order Date], sh.[Order Time]








