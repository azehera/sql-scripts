﻿

/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database              Table/View/UDF                  Action            
------------------------------------------------------------------------------------

BI_SSAS_Cubes           dbo.FactSales                 Select 

REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_Validation_SalesCube]
AS
SELECT [Posting Date], SUM(Amount)AS[Amount], COUNT(DocumentNo) AS [DocumentNo]
FROM BI_SSAS_Cubes.dbo.FactSales WITH (NOLOCK)GROUP BY [Posting Date]
ORDER BY  [Posting Date]


