﻿

CREATE procedure [dbo].[USP_XL_040_BalanceSheetLY]
as 


--EXECUTE AS LOGIN = 'ReportViewer'
--------TOTAL 40000 TO 99999-----dbo.BalanceSheet2012---
DROP TABLE #A

--2015-04-10, D. Smith - Change references to NAVPRDO to use NAV_ETL instead

SELECT 'Medifast Nutrition' AS Entity,  sum([Amount]) as [Current Year Retained Earnings]
into #a
FROM [NAV_ETL].dbo.[Medifast Nutrition$G_L Entry] a 
Where [Posting Date] <= '2013-12-31' 
and a.[G_L Account No_] BETWEEN '40000' AND '99999'

INSERT INTO #A
SELECT 'Jason Pharm' AS Entity, sum([Amount]) as [Current Year Retained Earnings]
FROM [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] a 
Where [Posting Date] <= '2013-12-31' 
and a.[G_L Account No_] BETWEEN '40000' AND '99999'

INSERT INTO #A
SELECT '7 Crondall' AS Entity,  sum([Amount]) as [Current Year Retained Earnings]
FROM [NAV_ETL].[dbo].[7 Crondall$G_L Entry] a 
Where [Posting Date] <= '2013-12-31' 
and a.[G_L Account No_] BETWEEN '40000' AND '99999'


INSERT INTO #A
SELECT 'Jason Ent' AS Entity,  sum([Amount]) as [Current Year Retained Earnings]
FROM [NAV_ETL].[dbo].[Jason Enterprises$G_L Entry] a 
Where [Posting Date] <= '2013-12-31' 
and a.[G_L Account No_] BETWEEN '40000' AND '99999'


INSERT INTO #A
SELECT 'Jason Prop' AS Entity,  sum([Amount]) as [Current Year Retained Earnings]
FROM [NAV_ETL].[dbo].[Jason Properties$G_L Entry] a 
Where [Posting Date] <= '2013-12-31' 
and a.[G_L Account No_] BETWEEN '40000' AND '99999'


INSERT INTO #A
SELECT 'Medifast' AS Entity,  sum([Amount]) as [Current Year Retained Earnings]
FROM [NAV_ETL].[dbo].[Medifast Inc$G_L Entry] a 
Where [Posting Date] <= '2013-12-31' 
and a.[G_L Account No_] BETWEEN '40000' AND '99999'

INSERT INTO #A
SELECT 'TSFL' AS Entity,  sum([Amount]) as [Current Year Retained Earnings]
FROM [NAV_ETL].[dbo].[Take Shape For Life$G_L Entry] a 
Where [Posting Date] <= '2013-12-31' 
and a.[G_L Account No_] BETWEEN '40000' AND '99999'



Insert into [BI_Reporting].[dbo].[XL_040_BalanceSheetLY]
select Entity ,[Current Year Retained Earnings]   from #a





