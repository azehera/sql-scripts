﻿
/*
=======================================================================================================
Author:         Kalpesh Patel
Create date: 07/24/2014

======================================================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
------------------------------------------------------------------------------------------------------
[ODYSSEY_ETL]          [dbo].[ODYSSEY_BUSINESSCENTER] 		     Select   
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER]                  Select  
[ODYSSEY_ETL]          [dbo].[ODYSSEY_CUSTOMER_ADDRESS]          Select
[ODYSSEY_ETL]          [dbo].[ODYSSEY_VOLUME]                    Select
[ODYSSEY_ETL]          [dbo].[ODYSSEY_RANK_SCHEME_DETAILS]       Select

=====================================================================================================
REVISION LOG
Date                   Name                  Change
-----------------------------------------------------------------------------------------------------
--2015-04-17     Menkir Haile                DB-BIDB server reference is removed           

====================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------
====================================================================================================
*/

CREATE  procedure [dbo].[USP_XL_074_Global_Tree_ Data] as 
set nocount on ;

----------------------------------Identify Globals and Above--------------------------
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT 
	   B.[CUSTOMER_NUMBER]
	  ,B.[FIRST_NAME]
	  ,B.[LAST_NAME]
	  ,CONVERT(varchar(10),B.[Effective_Date], 121) as [Activation Date]
	  ,CA.[CITY]
	  ,CA.[STATE]
	  ,B.[CUSTOMER_ID]
	  ,C.[BUSINESSCENTER_ID]
      ,C.[COMMISSION_PERIOD]
      ,C.[RANK]
      ,C.[HIGHEST_ACHIEVED_RANK]
      ,C.[RANK_ADVANCE_DATE]
    Into #a
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]B
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
      on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]
Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_ADDRESS]CA
	  on C.[CUSTOMER_ID] = CA.[CUSTOMER_ID]
Where C.[HIGHEST_ACHIEVED_RANK] in ('Global_Director', 'Presidential_Director','FI_Global_Director','FI_Presidential_Director' )
	and Convert(varchar(10),C.[COMMISSION_PERIOD],121) >= convert(varchar(10),GETDATE(),121)
	and B.[CUSTOMER_NUMBER] <> '101'
	and CA.[ADDRESS_TYPE] = 'MAIN'
Group by  
	   B.[CUSTOMER_NUMBER]
	  ,B.[CUSTOMER_ID]
	  ,C.[BUSINESSCENTER_ID]
      ,C.[COMMISSION_PERIOD]
      ,C.[RANK]
      ,C.[RANK_ADVANCE_DATE]
      ,C.[HIGHEST_ACHIEVED_RANK]
      ,B.[FIRST_NAME]
	  ,B.[LAST_NAME]
	  ,CA.[CITY]
	  ,CA.[STATE]
	  ,B.[Effective_Date]
Order by 
	 C.[HIGHEST_ACHIEVED_RANK]
-----------------------------Pull FLV-----------------------------------------------
IF object_id('Tempdb..#FLV') Is Not Null DROP TABLE #FLV
Select 
	 A.[CUSTOMER_NUMBER]
	,V.[PERIOD_END_DATE]
	,V.[VOLUME_VALUE] as [Frontline Volume]
Into #FLV
From #a A
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]V
      on A.[BUSINESSCENTER_ID] = V.[NODE_ID]
    Where Convert(nvarchar(10),V.[PERIOD_END_DATE],121) >= convert(varchar(10),GETDATE()-90,121)
		and Convert(nvarchar(10),V.[PERIOD_END_DATE],121) < convert(varchar(10),GETDATE(),121)
		and V.VOLUME_TYPE = 'FV'
		
----------------------------Pull GV-------------------------------------------------
IF object_id('Tempdb..#GV') Is Not Null DROP TABLE #GV
Select 
	 A.[CUSTOMER_NUMBER]
	,V.[PERIOD_END_DATE]
	,V.[VOLUME_VALUE] as [Group Volume]
Into #GV
From #a A
	Left join [ODYSSEY_ETL].[dbo].[ODYSSEY_VOLUME]V
      on A.[BUSINESSCENTER_ID] = V.[NODE_ID]
    Where Convert(nvarchar(10),V.[PERIOD_END_DATE],121) >= convert(varchar(10),GETDATE()-90,121)
		and Convert(nvarchar(10),V.[PERIOD_END_DATE],121) < convert(varchar(10),GETDATE(),121)
		and V.VOLUME_TYPE = 'GV'
		
-----------------------------------Pull Teams by sponsor---------------------
IF object_id('Tempdb..#Leggs') Is Not Null DROP TABLE #Leggs
SELECT 
	   [SPONSOR_ID]
	  ,convert(varchar(10),[COMMISSION_PERIOD],121) as [Comm Period]
      ,sum([IS_SENIOR_COACH_LEG]) as [SC Teams]
      ,sum([IS_EXEC_DIR_LEG]) as [ED Teams]
Into #Leggs
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on B.[Customer_ID] = C.[Customer_ID]
	Where convert(varchar(10),[COMMISSION_PERIOD],121)>= convert(varchar(10),GETDATE()-90,121)
		and convert(varchar(10),[COMMISSION_PERIOD],121)< convert(varchar(10),GETDATE(),121)
		Group by [SPONSOR_ID],[COMMISSION_PERIOD]

----------------------Pull Sponsor ID # and Name-----------------
IF object_id('Tempdb..#Legs') Is Not Null DROP TABLE #Legs
Select 
      C.[CUSTOMER_NUMBER] 
      ,(C.[FIRST_NAME]+' '+C.[LAST_NAME]) as [Sponsor Name]
      ,[Comm Period]
      ,isnull([SC Teams],'0') as [SC Teams]
      ,isnull([ED Teams],'0') as [ED Teams]
 Into #Legs
From #leggs A
Inner join  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
      on A.[SPONSOR_ID] = B.[BUSINESSCENTER_ID]
      and A.[Comm Period] = convert(varchar(10),[COMMISSION_PERIOD],121)
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
	  on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]
--------------------------------Pull Volume, Rank, and Team data---------------------------------
IF object_id('Tempdb..#Data') Is Not Null DROP TABLE #Data
Select 
	 A.[CUSTOMER_NUMBER]
	,(A.[FIRST_NAME]+' '+ A.[LAST_NAME]) as [Name]
	,(A.[CITY]+','+ A.[STATE]) as [Location]
	,A.[HIGHEST_ACHIEVED_RANK]
	,Convert(nvarchar(10),A.[RANK_ADVANCE_DATE],121)as [RANK_ADVANCE_DATE]
	,Convert(nvarchar(10),A.[Activation Date],121) as [Activation Date]
	,Convert(numeric(10,2),avg(FLV.[Frontline Volume])) as [Avg FLV]
	,Convert(numeric(10,2),avg(GV.[Group Volume])) as [Avg GV]
	,Convert(numeric(10,0),avg([SC Teams])) as [Avg SC Teams]
	,Convert(numeric(10,0),avg ([ED Teams])) as [Avg ED Teams]
Into #Data
  From #a A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
		on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner join #FLV FLV
		on A.[CUSTOMER_NUMBER] = FLV.CUSTOMER_NUMBER
		and C.COMMISSION_PERIOD = FLV.PERIOD_END_DATE
	Inner join #GV GV
		on A.[CUSTOMER_NUMBER] = GV.CUSTOMER_NUMBER
		and C.COMMISSION_PERIOD = GV.PERIOD_END_DATE
    Left outer join #legs E
		on A.[CUSTOMER_NUMBER] = E.[CUSTOMER_NUMBER]
		and Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) = E.[Comm Period]
	Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS]RS
		on RS.[RANK_NAME] = C.[RANK]
    Where Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) >= convert(varchar(10),GETDATE()-90,121)
		and Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) < convert(varchar(10),GETDATE(),121)
		Group by 
			 A.[CUSTOMER_NUMBER]
			,[FIRST_NAME]
			,[LAST_NAME]
			,[City]
			,[STATE]
			,A.[HIGHEST_ACHIEVED_RANK]
			,A.[RANK_ADVANCE_DATE]
			,[Activation Date]
			
-----------------------------Ranks by month (month 1)--------------------------------
IF object_id('Tempdb..#rank1') Is Not Null DROP TABLE #rank1
Select 
	 A.[CUSTOMER_NUMBER]
	,(A.[FIRST_NAME]+' '+ A.[LAST_NAME]) as [Name]
	,(A.[CITY]+','+ A.[STATE]) as [Location]
	,A.[HIGHEST_ACHIEVED_RANK]
	,Convert(nvarchar(10),A.[RANK_ADVANCE_DATE],121)as [RANK_ADVANCE_DATE]
	,Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) as [COMMISSION_PERIOD]
	,case(C.[RANK])
		when 'FI_PRESIDENTIAL_DIRECTOR' then 'IPD'
		when 'PRESIDENTIAL_DIRECTOR' then 'PD'
		when 'FI_GLOBAL_DIRECTOR' then 'IGD'
		when 'GLOBAL_DIRECTOR' then 'GD'
		when 'FI_NATIONAL_DIRECTOR' then 'IND'
		when 'NATIONAL_DIRECTOR' then 'ND'
		when 'FI_REGIONAL_DIRECTOR' then 'IRD'
		when 'REGIONAL_DIRECTOR' then 'RD'
		when 'FI_EXECUTIVE_DIRECTOR' then 'IED'
		when 'EXECUTIVE_DIRECTOR' then 'ED'
		when 'DIRECTOR' then 'Dr'
		when 'ASSOCIATE_DIRECTOR' then 'AD'
		when 'MANAGER' then 'MG'
		when 'SENIOR_COACH' then 'SC' end as [Rank]
Into #rank1
From #a A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
		on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
    Where Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) >= convert(varchar(10),GETDATE()-90,121)
		and Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) < convert(varchar(10),GETDATE()-60,121)

-----------------------------Ranks by month (month 2)--------------------------------
IF object_id('Tempdb..#rank2') Is Not Null DROP TABLE #rank2
Select 
	 A.[CUSTOMER_NUMBER]
	,(A.[FIRST_NAME]+' '+ A.[LAST_NAME]) as [Name]
	,(A.[CITY]+','+ A.[STATE]) as [Location]
	,A.[HIGHEST_ACHIEVED_RANK]
	,Convert(nvarchar(10),A.[RANK_ADVANCE_DATE],121)as [RANK_ADVANCE_DATE]
	,Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) as [COMMISSION_PERIOD]
	,case(C.[RANK])
		when 'FI_PRESIDENTIAL_DIRECTOR' then 'IPD'
		when 'PRESIDENTIAL_DIRECTOR' then 'PD'
		when 'FI_GLOBAL_DIRECTOR' then 'IGD'
		when 'GLOBAL_DIRECTOR' then 'GD'
		when 'FI_NATIONAL_DIRECTOR' then 'IND'
		when 'NATIONAL_DIRECTOR' then 'ND'
		when 'FI_REGIONAL_DIRECTOR' then 'IRD'
		when 'REGIONAL_DIRECTOR' then 'RD'
		when 'FI_EXECUTIVE_DIRECTOR' then 'IED'
		when 'EXECUTIVE_DIRECTOR' then 'ED'
		when 'DIRECTOR' then 'Dr'
		when 'ASSOCIATE_DIRECTOR' then 'AD'
		when 'MANAGER' then 'MG'
		when 'SENIOR_COACH' then 'SC' end as [Rank]
Into #rank2
From #a A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
		on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS]RS
		on RS.[RANK_NAME] = C.[RANK]
    Where Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) >= convert(varchar(10),GETDATE()-60,121)
		and Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) < convert(varchar(10),GETDATE()-30,121)
		
-----------------------------Ranks by month (month 2)--------------------------------
IF object_id('Tempdb..#rank3') Is Not Null DROP TABLE #rank3
Select 
	 A.[CUSTOMER_NUMBER]
	,(A.[FIRST_NAME]+' '+ A.[LAST_NAME]) as [Name]
	,(A.[CITY]+','+ A.[STATE]) as [Location]
	,C.[HIGHEST_ACHIEVED_RANK]
	,Convert(nvarchar(10),C.[RANK_ADVANCE_DATE],121)as [RANK_ADVANCE_DATE]
	,Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) as [COMMISSION_PERIOD]
	,case(C.[RANK])
		when 'FI_PRESIDENTIAL_DIRECTOR' then 'IPD'
		when 'PRESIDENTIAL_DIRECTOR' then 'PD'
		when 'FI_GLOBAL_DIRECTOR' then 'IGD'
		when 'GLOBAL_DIRECTOR' then 'GD'
		when 'FI_NATIONAL_DIRECTOR' then 'IND'
		when 'NATIONAL_DIRECTOR' then 'ND'
		when 'FI_REGIONAL_DIRECTOR' then 'IRD'
		when 'REGIONAL_DIRECTOR' then 'RD'
		when 'FI_EXECUTIVE_DIRECTOR' then 'IED'
		when 'EXECUTIVE_DIRECTOR' then 'ED'
		when 'DIRECTOR' then 'Dr'
		when 'ASSOCIATE_DIRECTOR' then 'AD'
		when 'MANAGER' then 'MG'
		when 'SENIOR_COACH' then 'SC' end as [Rank]
Into #rank3
From #a A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]C
		on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS]RS
		on RS.[RANK_NAME] = C.[RANK]
    Where Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) >= convert(varchar(10),GETDATE()-30,121)
		and Convert(nvarchar(10),C.[COMMISSION_PERIOD],121) < convert(varchar(10),GETDATE(),121)

-------------------Pull All Data Aggregated to one view----------------------

Select 
	 distinct(A.[CUSTOMER_NUMBER])
	,A.[Name]
	,A.[Location]
	,R3.[HIGHEST_ACHIEVED_RANK]
	,R3.[RANK_ADVANCE_DATE]
	,DATEDIFF(month,convert(datetime,[Activation Date]),GETDATE()-1) as [Months in Business]
	,(--'FLV'+' '+
	 convert(char(15),[Avg FLV])) as [Avg FLV]  ---Gregory request remove FLV and GV word---'2014-08-07
	,(--'GV'+' '+ 
	convert(char(20),[Avg GV])) as [Avg GV]
	,('SCT'+' '+ convert(char(2),[Avg SC Teams])) as [Avg SC Teams]
	,('EDT'+' '+ convert(char(2),[Avg ED Teams])) as [Avg ED Teams]
	,(R1.[Rank]+','+ R2.[Rank]+','+ R3.[Rank]) as [Rank]
	--,convert(char(2),R2.[Rank ID])
	--,convert(char(2),R3.[Rank ID])
from #data A
	inner join #rank1 R1
		on A.CUSTOMER_NUMBER = R1.[CUSTOMER_NUMBER]
	inner join #rank2 R2
		on A.CUSTOMER_NUMBER = R2.[CUSTOMER_NUMBER]
	inner join #rank3 R3
		on A.CUSTOMER_NUMBER = R3.[CUSTOMER_NUMBER]

