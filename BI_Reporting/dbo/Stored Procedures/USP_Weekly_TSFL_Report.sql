﻿/*

============================================================================================================   

Author:         Daniel Dagnachew

Create date: 10/06/2014

---------------------------[USP_Weekly_TSFL_Report] ---------------------------------------------------------

Returns a list of orders with negative volume ---------------------

=============================================================================================================   

REFERENCES

Database                            Table/View/UDF													 Action            

-------------------------------------------------------------------------------------------------------------

 NAV_ETL						dbo.Jason Pharm$Sales Line Archive								    Select

                                 

=============================================================================================================



*/

CREATE PROCEDURE [dbo].[USP_Weekly_TSFL_Report]



AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



    -- Insert statements for procedure here

select [Document No_],

  [Shipment Date],

  [Sell-to Customer No_],

  [No_],

  CAST([Amount Including VAT] AS DECIMAL(5,2)) as [Amount Including VAT],

  [Unit of Measure],

  [Quantity],

  CAST([Line Discount %] AS DECIMAL(5,2)) as [Line Discount %],

  [Description],

  [Volume],

  ([Unit Price]*[Quantity]) as [Line Amount Excl. Tax],

  [Inv_ Discount Amount] as [Inv_Disc_Amount to Invoice]

  

    FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Line Archive]

    where [Shortcut Dimension 2 Code]='TSFL'

    AND [Shipment Date] >= DATEADD(DAY, DATEDIFF(DAY, 7, GETDATE()), 0)

        AND [Shipment Date] <= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)

        AND Volume < 0





END
