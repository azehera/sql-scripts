﻿

CREATE procedure [dbo].[USP_CouponCode]
as   


IF object_id('Tempdb..#Cart') Is Not Null DROP TABLE #Cart
SELECT 
 CONVERT(Varchar(10), ORH_CREATE_DT ,121) AS ORH_CREATE_DT
,ORH_SOURCE_NBR
,ORH_PROMOTION_CODE
,ORH_STATUS_CD
,[ORH_CREATED_FOR]
,[USA_EMAIL]
,Case When ORH_STATUS_CD ='N' Then 'New'
      When ORH_STATUS_CD ='J' Then 'Rejected'
      When ORH_STATUS_CD ='C' Then 'Completed'
      When ORH_STATUS_CD ='X' Then 'Cancelled'
      When ORH_STATUS_CD ='1' Then 'RoutedToNav'      
      Else 'Other' End As StatusDescription
      ,ORH_PROMOTION_CODE as CouponCode
INTO #Cart    
FROM [ECOMM_ETL ].[dbo].[V_ORDER_HEADER] a
join [ECOMM_ETL ].[dbo].[USER_ACCOUNT] b
on a.[ORH_CREATED_FOR]=[USA_ID]
WHERE CONVERT(Varchar(10), ORH_CREATE_DT,121) >= '2013-01-01'
AND ORH_PROMOTION_CODE in (
 'GET14'
,'GET28'
,'GET56'
,'SHIP275'
,'PAN275'
,'TV28FREE'
,'DL25MEDCC'
,'DL25'
,'MED25OFF'
,'MED25OFFCC'
,'MOREMEALS28',
'PHILIP28',
'FREE275',
'SUCCESS28',
'SUCCESS28',
'FREEFALL56',
'FALL56',
'GINAM28',
'GINA28',
'FOODFREE28',
'FREEFOOD28',
'SCORE14',
'SCORES14',
'CYNTHIA28',
'JAN56',
'FEB14',
'FEB28',
'MAR56',
'APR28',
'NICESAVE14',
'RUN28BC',
'MEDMEALS',
'FREEMEALS',
'GETFREE28',
'MYFREE28',
'MARINER28',
'HEALTH28',
'PAN275',
'WMSFREE',
'FRIDAY25',
'KEEP15',
'CYBER25')


IF object_id('Tempdb..#Nav1') Is Not Null DROP TABLE #Nav1
Select 
[Order Date] As NavOrderDate
,b.[Posting Date] As InvoicedDate
,[Document No_]
,[External Document No_]
,[Coupon_Promotion Code]
,SUM(Amount) as Amount
,Case When a.No_='63000' Then SUM(Amount) else 0 end as ShippingCharged
into #Nav1
from [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Line] a
join 
    [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Header] b
    on a.[Document No_] = b.[No_] 
WHERE b.[Posting Date]>='2013-01-01'
Group By 
[Document No_], a.No_,[External Document No_], [Coupon_Promotion Code], [Order Date], b.[Posting Date]

IF object_id('Tempdb..#Nav2') Is Not Null DROP TABLE #Nav2
select 
 NavOrderDate
,InvoicedDate 
,[Document No_] as NavInvoiceNumber
,[External Document No_] as Nav_CartOrderNumber
,[Coupon_Promotion Code]
,SUM(Amount) as Amount
,Sum(ShippingCharged) as ShippingCharged
Into #Nav2
from #Nav1
group by  
[Document No_]
,[External Document No_]
,[Coupon_Promotion Code]
,NavOrderDate
,InvoicedDate 

Select 
 ORH_SOURCE_NBR as CartOrderNumber
,ORH_CREATE_DT AS CartOrderDate
,[ORH_CREATED_FOR]
,[USA_EMAIL]
,CouponCode
,StatusDescription as CartStatusDescription
,NavInvoiceNumber
,NavOrderDate
,InvoicedDate
,Nav_CartOrderNumber
,[Coupon_Promotion Code]
,Amount  
,ShippingCharged
From #Cart a
left join #Nav2 b
on a.ORH_SOURCE_NBR = b.Nav_CartOrderNumber



