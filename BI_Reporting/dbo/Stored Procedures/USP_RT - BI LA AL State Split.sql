﻿






/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 07/26/2019
------------------------------------------------

--==================================================================================   
REFERENCES
Database                        Table/View/UDF                         Action            
------------------------------------------------------------------------------------

NAV_ETL       [Jason Pharm$Sales Line]               Select 
NAV_ETL       [Jason Pharm$Sales Header]			   Select 
[Zip_codes_deluxe]              [ZipCodeDatabase_DELUXE]               Select 
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE PROCEDURE [dbo].[USP_RT - BI LA AL State Split]
AS
SELECT [External Document No_] [Order Number],
       H.[Posting Date] [Order Date],
       CASE
           WHEN H.[Customer Posting Group] = 'TSFL' THEN
               'OPTAVIA'
           WHEN H.[Customer Posting Group] = 'MEDIFAST' THEN
               'MEDIFAST'
           ELSE
               'OTHER'
       END AS [Client Division],
       C.Field1 [Client ID],
       C.FirstName + ' ' + C.LastName [Client Full Name],
       C.Email [Client Email],
       [Ship-to County] [State],
       C.Field2 [Sponsoring Coach ID],
       C2.Email [Sponsoring Coach Email]
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Header] H
    --LEFT JOIN HYBRIS_ETL.mdf_prd_database.orders o
    --    ON H.[External Document No_] COLLATE DATABASE_DEFAULT = o.p_code
    LEFT JOIN Exigo_ETL.dbo.Customers C
        ON C.Field1 COLLATE DATABASE_DEFAULT = CASE
                                                   WHEN H.[Sell-to Customer No_] LIKE '%tsfl%' THEN
                                                       RIGHT(H.[Sell-to Customer No_], LEN(H.[Sell-to Customer No_])
                                                                                       - 4)
                                                   ELSE
                                                       H.[Sell-to Customer No_]
                                               END
    LEFT JOIN Exigo_ETL.dbo.Customers C2
        ON C.Field2 = C2.Field1
WHERE H.[Posting Date] >= '8/16/2019'
      AND [Ship-to County] IN ( 'AL', 'LA' )
      AND [Location Code] = 'RDCWMS';








