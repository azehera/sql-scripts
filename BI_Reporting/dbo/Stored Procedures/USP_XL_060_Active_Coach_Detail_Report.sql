﻿


/*
===============================================================================
Author:         Kalpesh Patel
Create date: 01/20/2014

==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CORE_COMMISSION_ENTRY]		   Select   
[ODYSSEY_ETL]       [dbo].[ODYSSEY_CUSTOMER]                     Select  
[ODYSSEY_ETL]       [dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY]        Select
[ODYSSEY_ETL]       [dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY]        Select
[ODYSSEY_ETL]       [dbo].[ODYSSEY_BUSINESSCENTER]             Select
=====================================================================================================
REVISION LOG
Date                   Name                  Change
---------------------------------------------------------------------------------------------------
 --2015-04-17     Menkir Haile                DB-BIDB server reference is removed              

====================================================================================================
NOTES:
--------------------------------------------------------------------------------------------------
====================================================================================================
*/
CREATE procedure [dbo].[USP_XL_060_Active_Coach_Detail_Report]
	@M1EndDate Date
	 as 
set nocount on ;

Declare @StartDate Datetime
--Declare @EndDatePrev Datetime

Set @StartDate=(select DATEADD(MONTH, DATEDIFF(MONTH, 0, @M1EndDate ), 0)) 
--Set @EndDatePrev=(select DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))

-------------------------------------Pulling Monthly Earners with Earnings--------------------------------------------
SELECT 
	   A.[CUSTOMER_ID]
      ,A.[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,isnull([TOTAL_ADJUSTMENT],0) + isnull([GROWTH_BONUS],0) + isnull([GENERATION_BONUS],0)+ isnull([OVERRIDE_BONUS],0) + isnull([CSC_AMOUNT],0) 
       + isnull([CAB_BONUS],0) + isnull([ASSIST_BONUS],0) + ISNULL([ROLLING_BONUS],0)+ isnull([OVERRIDE_BONUS_GED],0)+ isnull([OVERRIDE_BONUS_PED],0)
       + isnull([CERTIFIED_BONUS],0)+ Isnull([CUSTOMER_SUPPORT_BONUS],0)+ Isnull([FIBC],0)+ Isnull([ANSITION_ADJ],0) as [Total Earnings]
    Into #a
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CORE_COMMISSION_ENTRY] A
	inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMMISSION_ENTRY] B
		on A.[COMMISSION_ENTRY_ID] = B.[COMMISSION_ENTRY_ID]
	Inner join [BI_Reporting].[dbo].[calendar_TSFL] C 
		on CAST(A.[COMMISSION_DATE] AS DATE) = CAST(C.[CalendarDate] AS DATE)
	Where CAST(A.[COMMISSION_DATE] AS DATE) between @StartDate and @M1EndDate
	Order by [COMMISSION_DATE] asc
	

--------------------------Pulling Weekly Earnings for Active Earning Coaches--------------------------
Insert into #a
SELECT 
	  [CUSTOMER_ID]
	 ,[COMMISSION_ENTRY_ID]
      ,Convert(varchar(10),[COMMISSION_DATE],121) as [Commission Date]
      ,Convert(varchar(10),[LastDateOfMonth],121) as [Last Day of Month]
      ,Convert(varchar(10),[LastDateOfQuarter],121)as [Last Day of Quarter]
      ,([CSC_AMOUNT]+[CAB_BONUS]+[ASSIST_BONUS]+[TOTAL_ADJUSTMENT]) as [Total Earnings]
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_TSFL_COMM_ENTRY_WEEKLY] A
  Inner join [BI_Reporting].[dbo].[calendar_TSFL] B 
		on CAST(A.[COMMISSION_DATE] AS DATE) = CAST(B.[CalendarDate] AS DATE)
  Where CAST(A.[COMMISSION_DATE] AS DATE) between @Startdate and @M1EndDate
	Order by [COMMISSION_DATE] asc
	
-----------------------------------Pull Teams by sponsor---------------------
IF object_id('Tempdb..#z') Is Not Null DROP TABLE #z
SELECT 
	   [SPONSOR_ID]
	  ,convert(varchar(10),[COMMISSION_PERIOD],121) as [Comm Period]
      ,sum([IS_SENIOR_COACH_LEG]) as [SC Teams]
      ,sum([IS_Exec_DIR_LEG]) as [ED Teams]
Into #z
  FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] B
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on B.[Customer_ID] = C.[Customer_ID]
	Where CAST([Commission_Period] AS DATE) = @M1EndDate
		Group by [SPONSOR_ID],[COMMISSION_PERIOD]
		
----------------------Pull Sponsor ID # and Name-----------------
IF object_id('Tempdb..#structure') Is Not Null DROP TABLE #structure
Select 
       C.[CUSTOMER_NUMBER] as [Dist ID]
      ,B.[CUSTOMER_ID]
      ,(C.[FIRST_NAME]+' '+C.[LAST_NAME]) as [Name]
      ,[Comm Period]
      ,isnull([SC Teams],'0') as [SC Teams]
      ,isnull([ED Teams],'0') as [ED Teams]
    Into #structure
From #z A
Inner join  [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]B
      on A.[SPONSOR_ID] = B.[BUSINESSCENTER_ID]
      and [Comm Period] = convert(varchar(10),[COMMISSION_PERIOD],121)
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
	  on B.[CUSTOMER_ID] = C.[CUSTOMER_ID]
Where CAST([Commission_Period] AS DATE) = @M1EndDate

-------------------------------------Highest Rank ever achived------------------------------
IF object_id('Tempdb..#HighestRank ') Is Not Null DROP TABLE #HighestRank 
Select a.CUSTOMER_ID,
       MAX(RANK_ID) as [Highest Achieved Rank#] 
       into #HighestRank 
from #a a
Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
      on a.CUSTOMER_ID=BC.CUSTOMER_ID
Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] RS
		on BC.[RANK] = RS.[RANK_NAME]
Group by a.CUSTOMER_ID
	  
----------------------------------Select All Weekly and Monthly Earnings---------------------------------
Select
	 [CUSTOMER_NUMBER]
	,Convert(numeric(36,0),([CUSTOMER_NUMBER])) as V_LookupCoachID
	,([First_Name]+' '+[LAST_NAME]) as [Name]
	,BC.[RANK]
	,([RANK_ID]+1) as [Rank #]
	,[Last Day of Month]
	,isnull([SC Teams],'0') as [SC Teams]
	,isnull([ED Teams],'0') as [ED Teams]
    ,BC.[HIGHEST_ACHIEVED_RANK] as [Highest Achieved Rank]
    ,([Highest Achieved Rank#]+1) as [Highest Achieved Rank#]
From #a A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] C
		on A.[CUSTOMER_ID] = C.[CUSTOMER_ID]
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER] BC
		on C.[CUSTOMER_ID] = BC.[CUSTOMER_ID]
    Inner Join [ODYSSEY_ETL].[dbo].[ODYSSEY_RANK_SCHEME_DETAILS] RS
		on BC.[RANK] = RS.[RANK_NAME]
	Left join #structure S
		on A.CUSTOMER_ID = S.CUSTOMER_ID 
		and A.[Last Day of Month] = S.[Comm Period]
		left Join #HighestRank H
		on h.CUSTOMER_ID=C.CUSTOMER_ID
	Where [Total Earnings] > '0'
		and [CUSTOMER_NUMBER] not in('2','6','6957001','16654601','9027001','16918401','30006445','101','3','16918501')
		and CAST([Commission_Period] AS DATE) = @M1EndDate
Group by 
	[CUSTOMER_NUMBER]
	,[First_Name]
	,[LAST_NAME]
	,BC.[RANK]
	,[Last Day of Month]
	,[SC Teams]
	,[ED Teams]
	,BC.[HIGHEST_ACHIEVED_RANK]
	,[Highest Achieved Rank#]
	,[RANK_ID]
Order by [ED Teams] desc
-----------------Clean up Temp Tables------------------------------------
Drop table #a


