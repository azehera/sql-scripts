﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[usp_xl_71B]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @Period int
Set @Period = (Select CalendarMonthId from [BI_SSAS_Cubes].[dbo].[DimCalendar] where CalendarDate = DATEADD(yy,-1,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0))) -----First Day of Last Year

-----------RETRIEVE INFORMATION-----
SELECT  
 1 AS EntryNO
,c.CalendarMonthID 
,a.[Source No_]
,b.[ShipToName]
,a.[G_L Account No_] COLLATE DATABASE_DEFAULT AS [G_L Account No_] 
,a.[Document No_] COLLATE DATABASE_DEFAULT + '' + a.[G_L Account No_]COLLATE DATABASE_DEFAULT AS DocumentID 
,a.[Document No_] COLLATE DATABASE_DEFAULT AS [Document No_] 
,a.[Global Dimension 1 Code] COLLATE DATABASE_DEFAULT AS [Global Dimension 1 Code] 
,a.[Source Code] COLLATE DATABASE_DEFAULT AS [Source Code] 
,CASE WHEN a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'FRANCHISE' THEN 'JASON PHARM FRANCHISE'
      WHEN a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'TSFL' THEN 'TSFL'
      WHEN a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'SALES' THEN 'WHOLESALE'
      ELSE a.[Global Dimension 2 Code] END AS [Global Dimension 2 Code] 
,a.[Transaction No_] 
,(a.[Amount]) * -1  AS NetTotal 
,NULL AS Budget$ 
,CASE WHEN a.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
      WHEN a.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
      ELSE 3 END AS AccountsCategory
,e.[G_L Name]
,e.[RollUpName] 
,b.[RegionName]
FROM [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] a
   Left Join [BI_SSAS_Cubes].[dbo].[DimCalendar] c 
                   on DATEADD(D, 0,DATEDIFF(D, 0,a.[Posting Date])) = c.CalendarDate
        Join [BI_Reporting].dbo.XL_033_Finance_FranchiseZipGeoInfo b
                   on case when len(b.[ShipToKey])=5 then LEFT(b.[ShipToKey],3)
                           when len(b.[ShipToKey])=6 then LEFT(b.[ShipToKey],4) else LEFT(b.[ShipToKey],4) end =a.[Source No_]
        Join [BI_SSAS_Cubes].[dbo].[DimGLCategory] e
                   on a.[G_L Account No_] = e.[G_L Account No_]
 Where [Global Dimension 2 Code] = 'Franchise'
          and   CalendarMonthID >= @Period
          and a.[G_L Account No_] >= '10000'
order by CalendarMonthID
END
