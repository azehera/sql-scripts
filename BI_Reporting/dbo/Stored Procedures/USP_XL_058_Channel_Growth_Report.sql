﻿--USE [BI_Reporting]
--GO
--/****** Object:  StoredProcedure [dbo].[XL_058_Channel_Growth_Report]    Script Date: 02/03/2014 16:44:56 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO



--/*
--=============================================================================================
--Author:         Kalpesh Patel
--Create date: 02/03/2014
---------------------------Amount and Units Comparision between years-----------------------------
------
--=============================================================================================   
--REFERENCES
--Database              Table/View/UDF                            Action            
-----------------------------------------------------------------------------------------------
--[BI_SSAS_Cubes]                          [dbo].[FactSales]      Select 
                 
--===========================================================================================
--REVISION LOG
--Date           Name              Change
---------------------------------------------------------------------------------------------

--==========================================================================================
--*/

CREATE Procedure [dbo].[USP_XL_058_Channel_Growth_Report]
as 

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select  [CustomerPostingGroup]
      ,CalendarMonthName
      ,ISNULL([2012],0) as [2012]
      ,ISNULL([2013],0) as [2013]
      ,ISNULL([2014],0) as [2014]
      ,(ISNULL([2013],0)-isnull([2012],0)) as Difference$_1213  
      ,(ISNULL([2014],0)-isnull([2013],0)) as Difference$_1314  
      into #A FROM (Select [CustomerPostingGroup]
      ,CalendarMonthName,[CalendarYear],Units from [BI_SSAS_Cubes].[dbo].[FactSales] a
   join [BI_SSAS_Cubes].dbo.DimCalendar b
   on a.[Posting Date]=b.[CalendarDate]
   where [Posting Date]>=(Select DATEADD(yy, DATEDIFF(yy,0,getdate()) - 2, 0))) up
   --where CalendarMonthName ='January') UP
      pivot (Sum(Units) for CalendarYear in ([2012],[2013],[2014])) as pvt
      Order by [CustomerPostingGroup]

      
 IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B     
 Select  [CustomerPostingGroup]
      ,CalendarMonthName
      ,ISNULL([2012],0) as [2012]
      ,ISNULL([2013],0) as [2013]
      ,ISNULL([2014],0) as [2014]
      ,(ISNULL([2013],0)-isnull([2012],0)) as DifferenceUnit_1213  
      ,(ISNULL([2014],0)-isnull([2013],0)) as DifferenceUnit_1314   
      into #B FROM (Select [CustomerPostingGroup]
      ,CalendarMonthName,[CalendarYear],Amount from [BI_SSAS_Cubes].[dbo].[FactSales] a
   join [BI_SSAS_Cubes].dbo.DimCalendar b
   on a.[Posting Date]=b.[CalendarDate]
   where [Posting Date]>=(Select DATEADD(yy, DATEDIFF(yy,0,getdate()) - 2, 0))) up
   --where CalendarMonthName ='January') UP
      pivot (Sum(Amount) for CalendarYear in ([2012],[2013],[2014])) as pvt
      Order by [CustomerPostingGroup]
      
      
 Select a.[CustomerPostingGroup]
        ,a.CalendarMonthName
        ,a.Difference$_1213  
        ,a.Difference$_1314  
        ,b.DifferenceUnit_1213  
        ,b.DifferenceUnit_1314 
        from #A a
        join #B b
        on a. [CustomerPostingGroup]=b.[CustomerPostingGroup]
        and a.CalendarMonthName=b.CalendarMonthName
