﻿CREATE Procedure [dbo].[USP_XL_027_AutoshipAsOf] AS 

SET NOCOUNT ON;


/*
=============================================================================================
Author:         Luc Emond
Create date: 01/23/2014
-------------------------[dbo].[USP_XL_027_AutoshipAsOf] -----------------------------
Archive Monthly Autoship Detail as of first of month.
=============================================================================================   
REFERENCES
Database              Table/View/UDF                                Action            
---------------------------------------------------------------------------------------------
[BI_Reporting]        [dbo].[Calendar]                               Select
[BI_Reporting]        [dbo].[XL_027_AutoshipAsOf]                    Select  
[BI_SSAS_Cubes]       [dbo].[FactSales]                              Select
[BI_Reporting]     [dbo].[XL_023_MonthlyAutoshipForecastAndActual]   Insert                         
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------
1/18/2016      Menkir Haile		The Select ststement is removed
==========================================================================================
*/

Declare @Today datetime, @FirstDayOfMonth datetime, @FirstDateLM Datetime, @FirstDateTM Datetime, @LastDateLM Datetime, @LastDateTM Datetime,@FirstDateNM Datetime
Set @Today = (select Convert(Varchar(10), Getdate(),121)) 
Set @FirstDayOfMonth = (Select FirstDateOfMonth from [BI_Reporting].dbo.calendar where CalendarDate=@Today)
Set @FirstDateLM =(SELECT Convert(Varchar(10),DATEADD(dd,(-1) * (DATEPART(dd,GETDATE()) - 1),DATEADD(mm,-1,GETDATE())),121))
Set @LastDateLM = (SELECT LastDateOfMonth from  BI_Reporting.dbo.calendar where CalendarDate=@FirstDateLM)
SET @FirstDateTM =(SELECT DATEADD(month, DATEDIFF(month, -1, getdate()-1)-1, 0))
SET @LastDateTM = (SELECT LastDateOfMonth from  BI_Reporting.dbo.calendar where CalendarDate=@FirstDateTM)
SET @FirstDateNM =(SELECT DATEADD(month, DATEDIFF(month, -1, getdate())-1, 0))
-----Select @today, @FirstDayOfMonth, @FirstDateLM, @LastDateLM

-----PRECAUTON IN CASE WE LOSE THE DATA-------
Truncate Table dbo.[XL_027_AutoshipAsOf_Bkup]
Insert into dbo.[XL_027_AutoshipAsOf_Bkup]
select * from [BI_Reporting].[dbo].[XL_027_AutoshipAsOf]

------CAPTURE FIRST OF MONTH DATA AND SAVE IT-----
------in case this run more than 1 time in one day------
Delete [BI_Reporting].[dbo].[XL_027_AutoshipAsOf]
Where AutoshipAsOf =@Today

---INSERT DATA INTO NEW TABLE---
Insert Into [BI_Reporting].dbo.XL_027_AutoshipAsOf
SELECT 
CONVERT(Varchar(10), Getdate(),121) as AutoshipAsOf 
,[USA_ID]
,[CTH_ID]
,[CartStatus]
,[TypeName]
,[CalendarDate]
,[CalendarYear]
,[CalendarMonthName]
,[CalendarMonth]
,[Amount]
FROM [BI_Reporting].[dbo].[XL_027_Autoship_Data]
Where @Today = @FirstDayOfMonth

----Archive the Forecast and the Actual-----------
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
Select 
@FirstDateTM AS CalDate
,Case When TypeName ='TSFL' Then SUM(Amount) Else 0 End TsflForecast
,Case When TypeName ='Medirect' Then SUM(Amount) Else 0 End MedifastForecast
into #A
From [BI_Reporting].[dbo].[XL_027_AutoshipAsOf]
Where CalendarDate BETWEEN @FirstDateTM AND @LastDateTM
and CartStatus ='A'
Group By TypeName

IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
SELECT 
@FirstDateTM AS CalDate
,Case When CustomerPostingGroup ='TSFL' AND SalesChannel='AUTO' THEN Sum(Amount) ELSE 0 END AS TsflActual
,Case When CustomerPostingGroup ='Medifast' AND SalesChannel='AUTO' THEN Sum(Amount) ELSE 0 END AS MedifastActual
Into #B
FROM [BI_SSAS_Cubes].[dbo].[FactSales]
WHERE [Posting Date] BETWEEN @FirstDateTM AND @LastDateTM
GROUP BY CustomerPostingGroup, SalesChannel

IF object_id('Tempdb..#MASTER') Is Not Null DROP TABLE #MASTER
select * 
INTO #MASTER 
from  [dbo].[XL_023_MonthlyAutoshipForecastAndActual]

DELETE #MASTER 

INSERT INTO #MASTER(CalendarDate)
SELECT @FirstDateTM

UPDATE #MASTER
SET TsflForecast= (SELECT SUM(TsflForecast) FROM #A), MedifastForecast = (SELECT SUM(MedifastForecast) from #A)
Where #MASTER.CalendarDate = @FirstDateTM

UPDATE #MASTER
SET TsflActual= (SELECT SUM(TsflActual) FROM #B), MedifastActual = (SELECT SUM(MedifastActual) from #B)
Where #MASTER.CalendarDate = @FirstDateTM

DELETE [dbo].[XL_023_MonthlyAutoshipForecastAndActual]
WHERE CalendarDate = @FirstDateTM

insert into [dbo].[XL_023_MonthlyAutoshipForecastAndActual]
select * from #MASTER
where CONVERT(varchar(10),getdate(),121)=@FirstDateNM

----FINAL VIEW-----
--select * from [dbo].[XL_023_MonthlyAutoshipForecastAndActual]

--select COUNT(*) from [BI_Reporting].[dbo].[XL_027_AutoshipAsOf]
--select COUNT(*) FROM dbo.[XL_027_AutoshipAsOf_Bkup]