﻿

CREATE procedure [dbo].[usp_ADHOC_CartRepAccomondation_For_finance]

as
IF object_id('Tempdb..#Cart') Is Not Null DROP TABLE #Cart
SELECT  CONVERT(VARCHAR(10), ACM_CREATE_DT, 121) AS CalDate ,
        [ACM_TYPE_CD] ,
        [ORH_SOURCE_NBR] AS [External Document No_] ,
        SUM([ACM_AMOUNT]) [ACM_AMOUNT] ,
        SKU_CODE
INTO    #cart
FROM    [ECOMM_ETL ].[dbo].[V_ORDER_HEADER] voh
        JOIN [ECOMM_ETL ].[dbo].[ORDER_LINE] ol ON voh.ORH_ID = ol.ORL_ORH_ID
        LEFT JOIN [ECOMM_ETL ].[dbo].ACCOMMODATION accom ON ol.ORL_ID = accom.ACM_ORL_ID
        JOIN [prdmartini_MAIN_repl].[dbo].SKU sku ON ol.ORL_SKU_ID = sku.SKU_ID
WHERE   [ACM_TYPE_CD] IN ( 'REP' )
        AND CONVERT(VARCHAR(10), ACM_CREATE_DT, 121) >= '2013-01-01'
        AND [ACM_AMOUNT] IS NOT NULL
GROUP BY CONVERT(VARCHAR(10), ACM_CREATE_DT, 121) ,
        [ORH_SOURCE_NBR] ,
        [ACM_AMOUNT] ,
        [ACM_TYPE_CD] ,
        SKU_CODE


----SHIP ACCOMONDATION----
INSERT  INTO #cart
        SELECT  CONVERT(VARCHAR(10), ACM_CREATE_DT, 121) AS CalDate ,
                [ACM_TYPE_CD] ,
                [ORH_SOURCE_NBR] AS [External Document No_] ,
                SUM([ACM_AMOUNT]) [ACM_AMOUNT] ,
                '63000_BX' AS Sku_code
        FROM    [ECOMM_ETL ].[dbo].[ACCOMMODATION] accom
                JOIN [ECOMM_ETL ].[dbo].[V_ORDER_HEADER] head ON acm_orh_id = head.orh_id
        WHERE   CONVERT(VARCHAR(10), ACM_CREATE_DT, 121) >= '2013-01-01'
                AND [ACM_TYPE_CD] IN ( 'SHO' )
                AND ACM_AMOUNT IS NOT NULL
        GROUP BY CONVERT(VARCHAR(10), ACM_CREATE_DT, 121) ,
                [ACM_TYPE_CD] ,
                [ORH_SOURCE_NBR]

--select * from #cart
--order by CalDate

IF object_id('Tempdb..#nav') Is Not Null DROP TABLE #nav
SELECT  [Customer Posting Group] ,
        [External Document No_] ,
        [Document No_] ,
        a.[No_] ,
        a.[Description] ,
        a.[Shortcut Dimension 1 Code] ,
        YEAR(b.[Posting Date]) AS CalYear ,
        MONTH(b.[Posting Date]) AS CalMonth ,
        CalendarMonthName ,
        CalendarQuarter ,
        Quantity ,
        [Unit of Measure] ,
        [Quantity (Base)] ,
        a.[Unit Price] ,
        [Line Discount Amount] AS [Discount Amount]
--,[Inv_ Discount Amount]
--,[Line Discount Amount]+[Inv_ Discount Amount] as [Discount Amount]
        ,
        CASE WHEN [Standard Cost] IS NULL THEN a.[Unit Price]
             ELSE [Standard Cost]
        END AS [Standard Cost] ,
        [Line Discount Amount] AS RetailCost ,
        ( [Quantity (Base)] )
        * ( CASE WHEN [Standard Cost] IS NULL THEN a.[Unit Price]
                 ELSE [Standard Cost]
            END ) AS Cost ,
        [Coupon_Promotion Code] AS NavCategory
INTO    #NAV
FROM    [nav_etl].dbo.[Jason Pharm$Sales Invoice Line] a
        JOIN [nav_etl].dbo.[Jason Pharm$Sales Invoice Header] b ON a.[Document No_] = b.[No_]
        LEFT JOIN [nav_etl].dbo.[Jason Pharm$Item] c ON a.[No_] COLLATE DATABASE_DEFAULT = c.[No_]
        JOIN [dbo].[calendar] d ON a.[Posting Date] = d.CalendarDate
WHERE   B.[External Document No_] IN ( SELECT   [External Document No_]
                                       FROM     #cart )

IF object_id('Tempdb..#Check') Is Not Null DROP TABLE #Check
SELECT  *
INTO    #check
FROM    #cart
WHERE   [External Document No_] NOT IN ( SELECT [External Document No_]
                                         FROM   #NAV )

--select * from [nav_etl].dbo.[Jason Pharm$Sales Invoice Header]
--where [External Document No_] in (SELECT [External Document No_] FROM #check)

ALTER TABLE #Nav
ADD CartCategory CHAR(30)
--, CategoryAmount Numeric(15,2)


UPDATE  #NAV
SET     CartCategory = cart.[ACM_TYPE_CD]
FROM    #cart cart
WHERE   #nav.[External Document No_] = cart.[External Document No_]
        AND #nav.No_ = CASE WHEN LEN(cart.SKU_CODE) = 14
                            THEN LEFT(cart.SKU_CODE, 11)
                            WHEN LEN(cart.SKU_CODE) = 10
                            THEN LEFT(cart.SKU_CODE, 7)
                            WHEN LEN(cart.SKU_CODE) = 9
                            THEN LEFT(cart.SKU_CODE, 6)
                            WHEN LEN(cart.SKU_CODE) = 8
                            THEN LEFT(cart.SKU_CODE, 5)
                            WHEN LEN(cart.SKU_CODE) = 7
                            THEN LEFT(cart.SKU_CODE, 4)
                            WHEN LEN(cart.SKU_CODE) = 6
                            THEN LEFT(cart.SKU_CODE, 3)
                       END
--and cart.[ACM_TYPE_CD] in('SHO', 'REP')                 
                    
DELETE  #NAV
WHERE   CartCategory IS NULL   

--select * from #nav
--where RetailCost <> [Discount Amount]

--select * from #nav
--WHERE CartCategory IS NULL  

--select * from #nav where [External Document No_]='12877680'
--select * from #cart where [External Document No_]='12877680'

TRUNCATE TABLE dbo.CartRepAccomondation
INSERT  INTO dbo.CartRepAccomondation
        SELECT  *
        FROM    #NAV




INSERT  INTO dbo.CartRepAccomondation
        ( [Customer Posting Group] ,
          [External Document No_] ,
          [No_] ,
          CalYear ,
          CalMonth ,
          CalendarMonthName ,
          CalendarQuarter ,
          RetailCost ,
          CartCategory
        )
        SELECT  'NotInNav' AS [Customer Posting Group] ,
                [External Document No_] ,
                [SKU_CODE] ,
                CalendarYear ,
                CalendarMonth ,
                CalendarMonthName ,
                CalendarQuarter ,
                ACM_AMOUNT ,
                ACM_TYPE_CD
        FROM    #check k
                JOIN dbo.calendar ca ON K.CalDate = ca.CalendarDate


SELECT  *
FROM    dbo.CartRepAccomondation


--select Year(CalDate), ACM_TYPE_CD, SUM(ACM_AMOUNT) from #cart group by  ACM_TYPE_CD, Year(CalDate)


