﻿
/*
=============================================================================================
Author:         Luc Emond
Create date: 03/07/2014
-------------------------[USP_XL_061_CoremetricWithCost]-----------------------------
Daily Coremetric info with cost
=============================================================================================   
REFERENCES
Database                          Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[BI_Reporting]                     dbo.Calendar                             Select 
[DDF]                             [dbo].[Order]                             Select
[DDF]                             [dbo].[CartItemPurchase]                  Select
[DDF]                             [dbo].[SessionFirstPageView]              Select
[NAV_ETL]                         [dbo].[Jason Pharm$Sales Price]           Select
[NAV_ETL]                         [dbo].[Jason Pharm$Item]                  Select
[ECOMM_ETL ]           [dbo].[ORDER_HEADER]                      Select   
[ECOMM_ETL ]           [dbo].[ORDER_LINE]                        Select
[prdmartini_MAIN_repl]            [dbo].[SKU]                               Select
[prdmartini_MAIN_repl]            [dbo].[PRODUCT]                           Select
===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------

==========================================================================================
*/

CREATE procedure [dbo].[USP_XL_061_CoremetricWithCost_Adhoc] as

set nocount on;


-----DECLARE AND SET DATE FOR REPORT-----
DECLARE @Date DateTime

Set @Date =(Select FirstDateOfMonth from dbo.calendar where CalendarDate=convert(varchar(10),Getdate()-1,121))
Set @Date ='2015-05-04'----Adhoc Purposes-----

------CREATE MASTER TABLE-----
IF object_id('tempdb..#MASTER') is not null DROP TABLE #MASTER
CREATE TABLE #MASTER (
[CalDate] DateTime NUll,
[ConversionRate]  Numeric(15,2) NULL,
[Visitors]  Numeric(15,0) NULL,
[TotalSales] Numeric(15,2) NULL, 
[SalesOnDemand] Numeric(15,2) NULL,
[OrdersOnDemand]  Numeric(15,0) NULL,
[AovOnDemand] Numeric(15,0) NULL,
[FreeShippingOnDemand]  Numeric(15,2) DEFAULT '0',
[FreeGoodOnDemand]  Numeric(15,2) DEFAULT '0',
[DiscountsOnDemand]  Numeric(15,2) DEFAULT '0',
[ListPriceDeviationOnDemand] Numeric(15,2) DEFAULT '0',
[OnDemandTotal] Numeric(15,2) NULL,
[MarketingPercentOnDemand] Decimal(6,4) NULL,

[SalesAutoShip] Numeric(15,2) NULL,
[OrdersAutoship]  Numeric(15,0) NULL,
[AovAutoship]  Numeric(15,2) NULL,
[FreeShippingAutoShip]  Numeric(15,2) DEFAULT '0',
[FreeGoodAutoShip]  Numeric(15,2) DEFAULT '0',
[DiscountsAutoShip]  Numeric(15,2) DEFAULT '0',
[ListPriceDeviationAutoShip] Numeric(15,2) DEFAULT '0',
[AutoShipTotal]  Numeric(15,2) NULL,
[MarketingPercentAutoship] Decimal(6,4) NULL,
[GrandTotal] Numeric(15,2) NULL,
[MarketingPercent] Decimal(6,4) NULL,
[Session]  Numeric(15,0) NULL
)

--------CREATE SALES TEMP TABLE------------
IF object_id('tempdb..#Sales') is not null DROP TABLE #Sales
SELECT [SESSION_ID]
      ,[COOKIE_ID]
      ,Convert(Varchar(10),[TIMESTAMP],121) CalDate
      ,[ORDER_ID]
      ,[REGISTRATION_ID]
      ,convert(money,[ORDER_TOTAL]) as [ORDER_TOTAL]
      ,convert(money,[SHIPPING]) as [SHIPPING]
      ,[SITE_ID]
      ,[Customer Type]
      ,[Order Type]
      ,[Pending TSFL Client]
      ,[Accomodations Total]
      ,convert(money,[Discount Total]) as [Discount Total]
      ,convert(money,[Tax Total]) as [Tax Total]
      ,convert(money,[Order Total]) as [Order Total]
      ,[Payment Type]
      ,[ORDER_ATTRIBUTE_9]
      ,[Promo Code]
      ,[SSIStimeStamp]
      ,[SessionDate]
      ,Case when (convert(money,[Order Total])-convert(money,[Tax Total]))=convert(money,[ORDER_TOTAL]) Then convert(money,[SHIPPING]) else 0 end ShipCost
   into #Sales
  FROM [DDF].[dbo].[Order]
    where CONVERT(varchar(10),TIMESTAMP,121)>=@Date
    --and [Customer Type] ='ONDEMAND'
  
-------DAILY SALES----
;with cte as
(SELECT 
[CalDate]
,Case When [Order Type] <>'VIP' Then SUM(ORDER_TOTAL) ELSE 0 END as [SalesOnDemand]
,Case When [Order Type] <> 'VIP' Then Count(Order_id) ELSE 0 END as [OrdersOnDemand]
FROM #Sales
group by [CalDate], [Order Type] )

insert into #master (CalDate, [SalesOnDemand], [OrdersOnDemand], [AovOnDemand])
select CalDate, Sum([SalesOnDemand]), Sum([OrdersOnDemand]),  Case When Sum([OrdersOnDemand]) = 0 Then 0 else Sum([SalesOnDemand])/Sum([OrdersOnDemand])end  from CTE group by CalDate

---------AUTOSHIP--------
IF object_id('tempdb..#Auto') is not null DROP TABLE #Auto
;with cte as
(SELECT 
[CalDate]
,Case When [Order Type] ='VIP' Then SUM(ORDER_TOTAL) ELSE 0 END as [SalesAutoship]
,Case When [Order Type] = 'VIP' Then Count(Order_id) ELSE 0 END as [OrdersAutoship]
FROM #Sales
group by [CalDate], [Order Type] )
select CalDate, Sum([SalesAutoship])[SalesAutoship], Sum([OrdersAutoship]) [OrdersAutoship],  Case When Sum([OrdersAutoship]) = 0 Then 0 else Sum([SalesAutoship])/Sum([OrdersAutoship])end AovAutoship 
into #auto from CTE group by CalDate

UPDATE #MASTER
SET [SalesAutoship] = AU.[SalesAutoship], [OrdersAutoship] = AU.[OrdersAutoship], AovAutoship = AU.AovAutoship 
FROM #Auto AU
WHERE #MASTER.CalDate = AU.CalDate

UPDATE #MASTER
SET [TotalSales] = (SalesOnDemand+SalesAutoShip)

------DAILY DISCOUNT-----
IF object_id('tempdb..#Disc') is not null DROP TABLE #Disc
;With CTE as (
select
[CalDate]
,Case When [Order Type] ='' Then 'ONDEMAND' ELSE [Order Type] END AS [Order Type] 
,SUM([DISCOUNT Total]) as Disc
From #Sales
GROUP bY [CalDate], [Order Type])
Select [CalDate],[Order Type], SUM(Disc) as Disc into #disc from CTE Group By [CalDate],[Order Type]

UPDATE #MASTER
SET [DiscountsAutoShip] = D.[Disc]
FROM #DISC D
WHERE #MASTER.CalDate = D.CalDate
and d.[Order Type]='VIP'

UPDATE #MASTER
SET [DiscountsOnDemand] = D.[Disc]
FROM #DISC D
WHERE #MASTER.CalDate = D.CalDate
and d.[Order Type]='ONDEMAND'

-------CREATE PRICE TABLE FOR FREE GOOD-------
IF object_id('tempdb..#PRICE_USD') is not null DROP TABLE #PRICE_USD
SELECT
 MAX([Starting Date]) DATE_
,[Item No_]
,[Unit of Measure Code]
,[Sales Code]
,'USD' As Currency
INTO #PRICE_USD
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Price]
WHERE [Sales Code] In('RETAIL')
GROUP BY [Item No_], [Unit of Measure Code], [Sales Code]

ALTER TABLE #PRICE_USD
ADD [Unit Price] Numeric(15,2)

UPDATE #PRICE_USD
SET [Unit Price] = P.[Unit Price]
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Price] P
WHERE #PRICE_USD.[Item No_] = P.[Item No_]
AND #PRICE_USD.DATE_ = P.[Starting Date]
AND #PRICE_USD.[Unit of Measure Code] = P.[Unit of Measure Code]
AND P.[Sales Code]In('RETAIL')

---------FREE GOOD-----
IF object_id('tempdb..#FreeGood') is not null DROP TABLE #FreeGood
SELECT 
 a.SESSION_ID
,a.PRODUCT_ID
,a.PRODUCT_NAME
,[Order Type]
,Case When (RIGHT(Product_id,2))='EA' THEN 'EA' ELSE 'BX' END AS UOM
,[Unit Price]
,CONVERT(varchar(10),a.TIMESTAMP,121) as CalDate
,QUANTITY
,QUANTITY *[Unit Price] AS COST
into #FreeGood
FROM [DDF].[dbo].[CartItemPurchase] a
Join   [DDF].[dbo].[Order] b
on a.SESSION_ID = b.SESSION_ID
Left Join #PRICE_USD  P
ON LEFT(A.Product_id,5) COLLATE DATABASE_DEFAULT = P.[Item No_]
 and Case When (RIGHT(Product_id,2))='EA' THEN 'EA' ELSE 'BX' END = [Unit of Measure Code]
 and [Sales Code] ='RETAIL'
where  CONVERT(varchar(10),a.TIMESTAMP,121)>=@Date
  and BASE_PRICE=0
  and RIGHT(Product_id,3) <>'AUT'
  AND RIGHT(Product_id,2) not in ('CF','OD')
 
UPDATE #FreeGood
SET [Unit Price] = JF.[Standard Cost]
from [NAV_ETL].[dbo].[Jason Pharm$Item] JF
WHERE LEFT(#FreeGood.Product_id,5) COLLATE DATABASE_DEFAULT = JF.No_
AND #FreeGood.[Unit Price] is null

UPDATE #FreeGood
SET COST = QUANTITY *[Unit Price]
 
UPDATE #FreeGood
SET [Order Type]  ='ONDEMAND' 
where [Order Type]=''
  
----Free good Total Daily---- 
IF object_id('tempdb..#FGD') is not null DROP TABLE #FGD
 select 
 CalDate
,[Order Type]
,SUM(QUANTITY*[Unit Price]) as Cost
Into #FGD
from #FreeGood
group by
  CalDate
,[Order Type]

UPDATE #MASTER
SET [FreeGoodAutoShip] = F.[Cost]
FROM #FGD F
WHERE #MASTER.CalDate = F.CalDate
and F.[Order Type]='VIP'

UPDATE #MASTER
SET [FreeGoodOnDemand] = F.[Cost]
FROM #FGD F
WHERE #MASTER.CalDate = F.CalDate
and F.[Order Type]='ONDEMAND'

-----SHIPPING---
IF object_id('tempdb..#SHIP') is not null DROP TABLE #SHIP
Select 
CalDate
,Case When [Order Type] ='' Then 'ONDEMAND' ELSE [Order Type] END AS [Order Type] 
,SUM([Shipping]) as ship
INTO #SHIP
from #Sales
where ([Order Total]-[Tax Total])=[ORDER_TOTAL]
GROUP BY 
CalDate
,[Order Type]

UPDATE #MASTER
SET [FreeShippingAutoShip] = Isnull(s.[ship],0)
FROM #SHIP s
WHERE #MASTER.CalDate = s.CalDate
and s.[Order Type]='VIP'

UPDATE #MASTER
SET [FreeShippingOnDemand] =Isnull(s.[ship],0)
FROM #SHIP s
WHERE #MASTER.CalDate = s.CalDate
and s.[Order Type]='ONDEMAND'

-------PRICE DEVIATION-------
IF object_id('tempdb..#CoreOrder') is not null DROP TABLE #CoreOrder
Select CONVERT(Varchar(10),P.TIMESTAMP,121) as CalDate, P.ORDER_ID, PRODUCT_ID, QUANTITY, [Order Type]
into #CoreOrder
FROM [DDF].[dbo].[CartItemPurchase] p
join [DDF].[dbo].[Order] o
on p.ORDER_ID = o.ORDER_ID
WHERE P.TIMESTAMP>=@Date
  
-----PRICE LIST FOR DEVIATION----
IF object_id('tempdb..#PriceList') is not null DROP TABLE #PriceList
SELECT 
CalDate
,oh.ORH_SOURCE_NBR
,Case When [Order Type] = '' Then 'ONDEMAND' Else [Order Type] end as [Order Type]
,b.SKU_CODE
,PRD_Code
,QUANTITY
,[ORL_LIST_PRICE_AMT] UcartListPrice
,e.[Unit Price] NavPrice
,e.[Unit Price]-[ORL_LIST_PRICE_AMT] as Variance
,QUANTITY * (e.[Unit Price]-[ORL_LIST_PRICE_AMT]) as TotalPriceVariance
Into #PriceList
FROM [ECOMM_ETL ].[dbo].[ORDER_HEADER] oh
  Join [ECOMM_ETL ].[dbo].[ORDER_LINE] a
  on oh.ORH_ID = a.ORL_ORH_ID
  Join [prdmartini_MAIN_repl].[dbo].[SKU] b
  on a.ORL_SKU_ID = b.SKU_ID
  Join [prdmartini_MAIN_repl].[dbo].[PRODUCT] c
  on b.SKU_PARENT_ID = c.PRD_ID
  Join #CoreOrder d
  on oh.ORH_SOURCE_NBR = d.Order_id
  and c.PRD_CODE = d.product_id
  Join #PRICE_USD e
  on d.PRODUCT_ID COLLATE DATABASE_DEFAULT = e.[Item No_]
  and RIGHT(b.sku_code,2)COLLATE DATABASE_DEFAULT = e.[Unit of Measure Code]
Group By
CalDate
,oh.ORH_SOURCE_NBR
,PRD_Code
,[ORL_LIST_PRICE_AMT]
,e.[Unit Price]
,b.SKU_CODE
,QUANTITY
,Case When [Order Type] = '' Then 'ONDEMAND' Else [Order Type] end
order by
e.[Unit Price]-[ORL_LIST_PRICE_AMT] desc

Update #PriceList 
set Navprice = 0, Variance =0
where PRD_CODE ='40490'

---------Total Price Deviation-------------
IF object_id('tempdb..#TotPriceDeviation') is not null DROP TABLE #TotPriceDeviation
select 
 CalDate
,[Order Type]
,SUM(TotalPriceVariance) as TotalPriceVariance
Into #TotPriceDeviation
from #PriceList
where Variance <>0
group by CalDate, [Order Type]
order by CalDate


Update #MASTER
SET [ListPriceDeviationOndemand] = T.TotalPriceVariance
FROM #TotPriceDeviation T
WHERE #MASTER.CalDate = T.CalDate
and t.[Order Type] ='ONDEMAND'

Update #MASTER
SET [ListPriceDeviationAutoShip] = T.TotalPriceVariance
FROM #TotPriceDeviation T
WHERE #MASTER.CalDate = T.CalDate
and t.[Order Type] ='VIP'


------TOTAL-------
UPDATE #Master Set [OnDemandTotal]  = [FreeGoodOnDemand]+[FreeShippingOnDemand]+[DiscountsOnDemand]+[ListPriceDeviationOndemand]
UPDATE #Master Set [MarketingPercentOnDemand] = Case When [SalesOnDemand] = 0 Then 0 Else ([OnDemandTotal]/[SalesOnDemand])*100 end

UPDATE #Master Set [AutoShipTotal]  = [FreeGoodAutoShip]+[FreeShippingAutoShip]+[DiscountsAutoShip]+[ListPriceDeviationAutoShip]
UPDATE #Master Set [MarketingPercentAutoShip] = Case When [SalesAutoShip] = 0 Then 0 Else ([AutoShipTotal]/[SalesAutoShip])*100 end 

UPDATE #Master set GrandTotal = [AutoShipTotal]+ [OnDemandTotal]
UPDATE #MASTER Set [MarketingPercent] = Case When (SalesOnDemand+SalesAutoShip) = 0 Then 0 Else ([GrandTotal]/(SalesOnDemand+SalesAutoShip))*100 End

--------TRACK VISITOR-------
IF object_id('tempdb..#Visitor') is not null DROP TABLE #Visitor
Select 
 CONVERT(Varchar(10),[TIMESTAMP],121) as CalDate
,COUNT(Distinct(Cookie_id)) as tot
Into #Visitor
From [DDF].[dbo].[SessionFirstPageView]
Where Convert(Varchar(10),SSISTimestamp,121)>=@Date
Group By CONVERT(Varchar(10),[TIMESTAMP],121)

Update #Master
Set Visitors = v.Tot
from #visitor v
where #master.caldate = v.caldate

----CONVERSION RATE----
IF object_id('tempdb..#Session') is not null DROP TABLE #Session
Select 
CONVERT(Varchar(10),[TIMESTAMP],121) as CalDate
,Convert(Decimal(18,0),COUNT(Distinct [SESSION_ID]))  as [Session]
into #Session
From [DDF].[dbo].[SessionFirstPageView]
where Convert(Varchar(10),SSISTimestamp,121)>=@Date
group by CONVERT(Varchar(10),[TIMESTAMP],121)

UPDATE #MASTER
set [Session] = s.[Session]
from #Session s
where #master.caldate = s.caldate

UPDATE #MASTER
SET ConversionRate = (([OrdersOnDemand]+[OrdersAutoship])/[Session])*100

-------FINAL------
--Truncate Table dbo.DailyCoremetricInfo
--Insert Into dbo.DailyCoremetricInfo
Select 
 DayOfWeekName
,CalDate
,Visitors
,TotalSales
,ConversionRate As [ConversionRate(%)]

,SalesOnDemand
,OrdersOnDemand
,AovOnDemand
,FreeShippingOnDemand
,FreeGoodOnDemand
,DiscountsOnDemand
,[ListPriceDeviationOndemand]
,OnDemandTotal
,MarketingPercentOnDemand As [MarketingPercentOnDemand(%)]

,SalesAutoship
,OrdersAutoship
,AovAutoship
,FreeShippingAutoship
,FreeGoodAutoship
,DiscountsAutoship
,[ListPriceDeviationAutoShip]
,AutoshipTotal
,MarketingPercentAutoship As [MarketingPercentAutoship(%)]
,GrandTotal
,MarketingPercent AS [MarketingPercent(%)]

,CalendarMonthName
,CalendarQuarter 
From #master ma
join dbo.calendar ca
on ma.CalDate = ca.CalendarDate
Order By caldate

