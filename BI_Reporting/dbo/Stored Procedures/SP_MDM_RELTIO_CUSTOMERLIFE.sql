﻿
  
/******************************************************************************************
OBJECT NAME:  [dbo].[MDM_RELTIO_CustomerLife] 
DEVELOPER:  Ronak Shah     
DATE:  8/31/2018           
DESCRIPTION: Loading/Updating data for [dbo].[Customer_Life_MDM] from Nav_ETL database     
PARAMETERS/VARRIABLES:
NOTES: This is Delta Process for [dbo].[Customer_Life_MDM] table. It compareds Previous day load vs today's Load.
       If any change in any record process will update that row. 
	   If there is no change process will not perform any action.
	   If there is a new record(s) will be inserted. 

Modification : 
Date: 09/06/2019
NOTES: Modify this stored procedure to include [First Order Type],[First Order Posting Group] 
											   [Last Order Type],[Last Order Posting Group] 
											   [GINID],[Coach Tenure]
											   
Date: 11/13/2019
NOYES: Modify this stored procedure to include [LifeTimeReturn] and [CoachDraftClass]	

Date: 01/24/2019
Notes: Replace BICUBES to EDW_PROD.BICUBES_MDM. Since we moved MDM Sales cube process to DP-BIDBPROD
Developer :Ronak Shah
RFC-741			

Date:05/18/2020
Notes: Optimize this stored procedure. 
       Move this process from DP-MDMDB\MDMPROD to DP-BIDBPROD to run in desired time.
	   Why move: As this process uses linked server & never finished on DP-MDMDB\MDMPROD post oracle Go live
       Remove [EDW_PROD].NAV_ETL.dbo.[Jason Pharm$Customer] C table
	   Since this process move to DP-BIDBPROD removed [EDW_PROD] linked server(No need)
RFC-1150

Date:04/15/2021
Notes: Modified Stored Procedure to get the data from FactSalesSummary -  DEHA-931	
Developer: Atya	
RFC-2624
			
*******************************************************************************************/

CREATE PROCEDURE [dbo].[SP_MDM_RELTIO_CUSTOMERLIFE] 
AS 

IF OBJECT_ID('Tempdb..#temp_AllData') IS NOT NULL
    DROP TABLE #temp_AllData;
SELECT  
		case when FSS.[SelltoCustomerID] like 'TSFL-%' THEN SUBSTRING(FSS.[SelltoCustomerID],6,20)
             when FSS.[SelltoCustomerID] like 'TSFL%' THEN SUBSTRING(FSS.[SelltoCustomerID],5,20)
			 ELSE FSS.[SelltoCustomerID] 
	   END as MedifastCustomerNumber
	  ,NULL AS [Blocked]	  
	  ,MIN(FSS.[Posting Date]) AS  [FirstInvoiceDate]
	  ,COUNT(Distinct FSS.[DocumentNo]) AS [Life Invoice]
	  ,LEFT(CONVERT(varchar, MIN(FSS.[Posting Date]),112),6) AS [CustomerDraftClass]
	  ,DATEDIFF(MM, MIN(FSS.[Posting Date]), MAX(FSS.[Posting Date])) AS [LifeMonth]	  
	  , GETDATE() as [Created_Date]
	  , GETDATE() as [Updated_Date]
	  ,MAX(FSS.[Posting Date]) as [LastInvoiceDate]
    --  ,MAX(FSS.[GINID]) AS [GINID]
	  ,case When IS_FIBC = 0 Then 'No' 
			When IS_FIBC = 1 Then 'Yes' 
	   END as FIBC
	  ,case When IS_FIBL = 0 Then 'No' 
			When IS_FIBL = 1 Then 'Yes' 
		END as FIBL
	  ,DC.COACH_MONTH	as [Coach Tenure]
	  INTO #temp_AllData
  FROM [BICUBES_MDM].[dbo].[FactSalesSummary] FSS 
LEFT OUTER JOIN   [OPTAVIA_CUBE].[dbo].[DIM_CUSTOMER_PERIOD_INFO] DC  ON
 DC.CUSTOMER_NUMBER  COLLATE DATABASE_DEFAULT = case when FSS.[SelltoCustomerID] like 'TSFL-%' THEN SUBSTRING(FSS.[SelltoCustomerID],6,20)
             when FSS.[SelltoCustomerID] like 'TSFL%' THEN SUBSTRING(FSS.[SelltoCustomerID],5,20)
			 ELSE FSS.[SelltoCustomerID] 
	   END 
	   AND Year(COMMISSION_PERIOD)=Year(Getdate()) AND Month(COMMISSION_PERIOD)=MONTH(Getdate())
	   where [Posting Date]>= '2010-01-01' AND  FSS.OrderType NOT IN('EXCHANGE','RETURN')
  Group by case when FSS.[SelltoCustomerID] like 'TSFL-%' THEN SUBSTRING(FSS.[SelltoCustomerID],6,20)
             when FSS.[SelltoCustomerID] like 'TSFL%' THEN SUBSTRING(FSS.[SelltoCustomerID],5,20)
			 ELSE FSS.[SelltoCustomerID] 
	   END,	IS_FIBC ,IS_FIBL,DC.COACH_MONTH  ;
	   
/******************Update existing Records***************************/

Update  CLM 
SET 
CLM.[MedifastCustomerNumber]=t.[MedifastCustomerNumber] , 
--CLM.[Blocked]=t.[Blocked] , 
CLM.[FirstInvoiceDate]= t.[FirstInvoiceDate] , 
CLM.[Life Invoice]= t.[Life Invoice] , 
CLM.[CustomerDraftClass]=t.[CustomerDraftClass] , 
CLM.[LifeMonth]= t.[LifeMonth] ,   
CLM.[Updated_Date]=  GETDATE() , 
CLM.[LastInvoiceDate] =t.[LastInvoiceDate] ,
--CLM.[GINID] = t.[GINID] , 
CLM.FIBC= t.FIBC , 
CLM.FIBL =t.FIBL , 
CLM.[Coach Tenure]=t.[Coach Tenure]
FROM [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM] CLM
JOIN #temp_AllData t ON CLM.MedifastCustomerNumber= t.MedifastCustomerNumber
AND 
(
--t.[Blocked]!=CLM.[Blocked] OR 
t.[FirstInvoiceDate]!= CLM.[FirstInvoiceDate] OR 
t.[Life Invoice]!= CLM.[Life Invoice] OR 
t.[CustomerDraftClass]!=CLM.[CustomerDraftClass] OR 
t.[LifeMonth]!= CLM.[LifeMonth] OR   
t.[LastInvoiceDate] !=CLM.[LastInvoiceDate] OR
--t.[GINID] != CLM.[GINID] OR 
t.FIBC!= CLM.FIBC OR 
t.FIBL !=CLM.FIBL OR 
t.[Coach Tenure]!=CLM.[Coach Tenure]
) 

/******************Insert new Records***************************/
	   
INSERT INTO [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM]
([MedifastCustomerNumber],[Blocked],[FirstInvoiceDate],[Life Invoice],[CustomerDraftClass],[LifeMonth],[Created_Date],
[Updated_Date],[LastInvoiceDate]--,[GINID]
,FIBC,FIBL,[Coach Tenure])
SELECT
t.[MedifastCustomerNumber],t.[Blocked],t.[FirstInvoiceDate],t.[Life Invoice],t.[CustomerDraftClass],t.[LifeMonth],t.[Created_Date],
t.[Updated_Date],t.[LastInvoiceDate]--,t.[GINID]
,t.FIBC,t.FIBL,t.[Coach Tenure]
FROM #temp_AllData t LEFT JOIN [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM] CLM with( nolock) ON
t.MedifastCustomerNumber=CLM.MedifastCustomerNumber
WHERE CLM.MedifastCustomerNumber IS NULL;

/************************************************************************************/	
 /* Update GINID */

Update CLM
   SET GINID = GMR.GINID
     ,Updated_Date= GETDATE()
 from [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM]  CLM
 Left Outer Join [BICUBES_MDM].[dbo].[Group_MDM_Reltio] GMR
 ON CLM.MedifastCustomerNumber = GMR.CustomerId 
 where CLM.GINID IS NULL
 
 Update CLM
   SET GINID = GMR.GINID
     ,Updated_Date= GETDATE()
 from [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM]  CLM
 Left Outer Join [BICUBES_MDM].[dbo].[Group_MDM_Reltio] GMR
 ON CLM.MedifastCustomerNumber = GMR.CustomerId 
 where CLM.GINID!=GMR.GINID


/********************************First/Last Order*****************************************/
-- Update First Order Type & First Order Posting Group
Update CLM
     SET [First Order Type] = FM.OrderType
	 ,[First Order Posting Group] = FM.CustomerPostingGroup
	 ,Updated_Date= GETDATE()
from [BICUBES_MDM].[dbo].[FactSalesSummary] FM  
INNER JOIN  [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM]  CLM
ON  CLM.MedifastCustomerNumber =case when FM.SelltoCustomerID  like 'TSFL-%' THEN SUBSTRING(FM.SelltoCustomerID ,6,20)
									 when FM.SelltoCustomerID  like 'TSFL%' THEN SUBSTRING(FM.SelltoCustomerID ,5,20)
									 ELSE FM.SelltoCustomerID  END  COLLATE DATABASE_DEFAULT
and Cast(CLM.FirstInvoiceDate as Date)= Cast(FM.[Posting Date] as Date)
Where CLM.[First Order Type] IS NULL or CLM.[First Order Posting Group] IS NULL


-- Update Last Order Type & Last Order Posting Group
Update CLM
     SET [Last Order Type] = FM1.OrderType
	 ,[Last Order Posting Group] =FM1.CustomerPostingGroup
	 ,Updated_Date= GETDATE()
	  From [BICUBES_MDM].[dbo].[FactSalesSummary] FM1
INNER JOIN  [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM] CLM
ON  CLM.MedifastCustomerNumber =case when FM1.SelltoCustomerID  like 'TSFL-%' THEN SUBSTRING(FM1.SelltoCustomerID ,6,20)
									 when FM1.SelltoCustomerID  like 'TSFL%' THEN SUBSTRING(FM1.SelltoCustomerID ,5,20)
									 ELSE FM1.SelltoCustomerID  END  COLLATE DATABASE_DEFAULT
and Cast(CLM.LastInvoiceDate as Date)= Cast(FM1.[Posting Date] as Date)
Where CLM.[Last Order Type] IS NULL OR CLM.[Last Order Posting Group] IS NULL
/************************************************************************************/			 


/********************************LifeTimeReturn*****************************************/
IF OBJECT_ID('Tempdb..#temp_Return') IS NOT NULL
    DROP TABLE #temp_Return;
select case when A.SelltoCustomerID  like 'TSFL-%' THEN SUBSTRING(A.SelltoCustomerID ,6,20)
														    when A.SelltoCustomerID  like 'TSFL%' THEN SUBSTRING(A.SelltoCustomerID ,5,20)
															ELSE A.SelltoCustomerID  END AS SelltoCustomerID,ROUND(SUM(LifeTimeReturn),2) LifeTimeReturn 
															INTO  #temp_Return from (
Select distinct SelltoCustomerID, SUM(AMOUNT) LifeTimeReturn from [BICUBES_MDM].dbo.FactSales_MDM with (nolock) where OrderType='RETURN'
group by SelltoCustomerID
UNION ALL
Select distinct SelltoCustomerID, SUM(AMOUNT) LifeTimeReturn from [BICUBES_MDM].dbo.FactSales_MDM_Archive with (nolock) where OrderType='RETURN'
group by SelltoCustomerID
UNION ALL
SELECT SelltoCustomerID,SUM(Amount) as LifeTimeValue 
	FROM [BICUBES_MDM].[dbo].FactSalesArchive with (nolock)   where OrderType='RETURN'
and [Posting Date]>= '2010-01-01'	group by SelltoCustomerID	
)A Group by case when A.SelltoCustomerID  like 'TSFL-%' THEN SUBSTRING(A.SelltoCustomerID ,6,20)
														    when A.SelltoCustomerID  like 'TSFL%' THEN SUBSTRING(A.SelltoCustomerID ,5,20)
															ELSE A.SelltoCustomerID  END

-- Update LifeTimeReturn
Update CLM
     SET [LifeTimeReturn] = FM.LifeTimeReturn
	 ,Updated_Date= GETDATE()
from #temp_Return FM  
INNER JOIN  [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM]  CLM
ON  CLM.MedifastCustomerNumber = FM.SelltoCustomerID COLLATE DATABASE_DEFAULT															
Where CLM.[LifeTimeReturn] IS NULL or CLM.[LifeTimeReturn] != FM.LifeTimeReturn

/************************************************************************************/			 

/********************************LifeTimeValue*****************************************/
IF OBJECT_ID('Tempdb..#temp_amt') IS NOT NULL
    DROP TABLE #temp_amt;
--update LifeTimeValue
SELECT case when A.SelltoCustomerID  like 'TSFL-%' THEN SUBSTRING(A.SelltoCustomerID ,6,20)
     when A.SelltoCustomerID  like 'TSFL%' THEN SUBSTRING(A.SelltoCustomerID ,5,20)
	ELSE A.SelltoCustomerID  
END AS SelltoCustomerID,ROUND(SUM(LifeTimeValue),2) AS LifeTimeValue
INTo  #temp_amt  FROM (
Select  SelltoCustomerID,sum(amount) as LifeTimeValue
	FROM [BICUBES_MDM].[dbo].factsales_MDM  with (nolock) where OrderType IN ('BSL','ONDEMAND','VIP') group by SelltoCustomerID
UNION ALL
SELECT SelltoCustomerID,SUM(Amount) as LifeTimeValue 
	FROM [BICUBES_MDM].[dbo].Factsales_MDM_archive with (nolock) where OrderType IN ('BSL','ONDEMAND','VIP') group by SelltoCustomerID
UNION ALL
SELECT SelltoCustomerID,SUM(Amount) as LifeTimeValue 
	FROM [BICUBES_MDM].[dbo].FactSalesArchive with (nolock) where OrderType IN ('BSL','ONDEMAND','VIP')
and [Posting Date]>= '2010-01-01'	group by SelltoCustomerID		
)A GROUP BY 
case when A.SelltoCustomerID  like 'TSFL-%' THEN SUBSTRING(A.SelltoCustomerID ,6,20)
     when A.SelltoCustomerID  like 'TSFL%' THEN SUBSTRING(A.SelltoCustomerID ,5,20)
	ELSE A.SelltoCustomerID  
END

Update CLM
     SET [LifeTimeValue] = FM.LifeTimeValue
	 ,Updated_Date= GETDATE()
from #temp_amt FM  
INNER JOIN  [dbo].[CUSTOMER_LIFE_MDM]  CLM
ON  CLM.MedifastCustomerNumber =FM.SelltoCustomerID  COLLATE DATABASE_DEFAULT															
Where CLM.[LifeTimeValue] IS NULL or CLM.[LifeTimeValue] != FM.LifeTimeValue
/************************************************************************************/	

/********************************CoachDraftClass*****************************************/

-- Get newly inserted updated records' CoachDraftClass 
IF OBJECT_ID('Tempdb..#CDC') IS NOT NULL
    DROP TABLE #CDC;
Select 
case when len(DCI.DRAFT_MONTH) = '1'
then concat(DCI.DRAFT_YEAR,'0',DCI.DRAFT_MONTH) 
else concat(DCI.DRAFT_YEAR,'',DCI.DRAFT_MONTH) 
end as CoachDraftClass,
case when DCI.CUSTOMER_NUMBER  like 'TSFL-%' THEN SUBSTRING(DCI.CUSTOMER_NUMBER ,6,20)
	 when DCI.CUSTOMER_NUMBER  like 'TSFL%' THEN SUBSTRING(DCI.CUSTOMER_NUMBER ,5,20)
ELSE DCI.CUSTOMER_NUMBER  END AS CUSTOMER_NUMBER
INTO  #CDC
FROM [OPTAVIA_CUBE].[dbo].[DIM_CUSTOMER_INFO] DCI
INNER JOIN [Exigo_ETL].[dbo].[Customers] C
on DCI.CUSTOMER_NUMBER=C.Field1
where C.CustomerTypeID = 1 ;

-- Update CoachDraftClass
Update CLM
     SET [CoachDraftClass] = DCI.CoachDraftClass
	 ,Updated_Date= GETDATE()
from #CDC DCI --Alias DCI because it is derived from Dim Customer Info table
INNER JOIN  [BI_Reporting].[dbo].[CUSTOMER_LIFE_MDM] CLM
ON  CLM.MedifastCustomerNumber = DCI.CUSTOMER_NUMBER COLLATE DATABASE_DEFAULT														
Where CLM.[CoachDraftClass] IS NULL	

/************************************************************************************/			 