﻿



/*
=============================================================================================
Author:      Luc Emond
Create date: 10/28/2013
-------------------------[dbo].[USP_XL_039_TSFL_Compliance_Exception_Report] ----------------
Provide Monthly TSFL Coach Violation on Credit Card Used and accounts above $1000.               
=============================================================================================   
REFERENCES
Database                    Table/View/UDF                                       Action            
---------------------------------------------------------------------------------------------
NAV_ETL                 [Jason Pharm$Sales Invoice Header]                     Select
NAV_ETL                 [Jason Pharm$Sales Invoice Line]                       Select
[ECOMM_ETL ]   [ORDER_HEADER]                                         Select    
[ECOMM_ETL ]   [CREDIT_CARD_TXN]                                      Select
[ECOMM_ETL ]   [Payment]                                              Select
[ECOMM_ETL ]   [Credit_Card]                                          Select
[ODYSSEY_ETL]             [ODYSSEY_CUSTOMER]                                     Select 
[ODYSSEY_ETL]             [ODYSSEY_BUSINESSCENTER]                               Select
[BI_Reporting]            XL_039_TSFL_Compliance_Exception_Violation             Insert
[BI_Reporting]            XL_039_TSFL_Compliance_Exception_Sales_Above_$1000     Insert
============================================================================================
REVISION LOG
Date           Name              Change
--------------------------------------------------------------------------------------------

============================================================================================
*/


CREATE procedure [dbo].[USP_XL_039_TSFL_Compliance_Exception_Report] as 

Set NoCount On;
--EXECUTE AS LOGIN = 'ReportViewer'
--------SALES INFO--------
IF object_id('Tempdb..#Sales') Is Not Null DROP TABLE #Sales
SELECT
 a.[No_]
,a.[Salesperson Code]
,a.[Sell-to Customer No_]
,a.[Bill-to Customer No_]
,a.[Bill-to Name]
,a.[Sell-to Customer Name]
,a.[Ship-to Name]
,ccd_holder_nm [Credit Card Holder Name]
,ccd_brand_cd [Brand]
,CCD_NUMBER_EXT
,ccd_brand_cd+CCD_NUMBER_EXT AS UniqueCardId
,a.[Posting Date]
,a.[Document Date]
,MONTH(a.[Posting Date]) as CalMonth
,a.[Shortcut Dimension 1 Code]
,a.[Shortcut Dimension 2 Code]
,a.[Customer Posting Group]
,a.[External Document No_]
,a.[Payment Method Code]
,a.[Authorization No_]
,a.[Amount Authorized]
,b.ORH_ID
,b.ORH_CREATED_FOR as UsaId
,SUM(aa.Amount) as GrossSales
into #sales   
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Header]a
join NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] aa
      on aa.[Document No_] = a.[No_]
join [ECOMM_ETL ].[dbo].[ORDER_HEADER] b
      on a.[External Document No_] COLLATE DATABASE_DEFAULT= b.ORH_SOURCE_NBR
Join [ECOMM_ETL ].dbo.CREDIT_CARD_TXN c
      on a.[Authorization No_] COLLATE DATABASE_DEFAULT= c.CCT_REFERENCE_NBR
Join [ECOMM_ETL ].dbo.payment d
      on c.CCT_PAY_ID = d.PAY_ID
Join [ECOMM_ETL ].dbo.credit_card e
      on  ccd_id = pay_type_id
Where a.[Document Date]>=(SELECT Convert(Varchar(10),DATEADD(MONTH, DATEDIFF(MONTH, '19000101', GETDATE()) - 1, '19000101'),121))----First Day Previous Month-----
    and [Customer Posting Group]='TSFL'
Group By
 a.[No_]
,a.[Salesperson Code]
,a.[Sell-to Customer No_]
,a.[Bill-to Customer No_]
,a.[Bill-to Name]
,a.[Sell-to Customer Name]
,a.[Ship-to Name]
,ccd_holder_nm 
,ccd_brand_cd 
,CCD_NUMBER_EXT
,ccd_brand_cd+CCD_NUMBER_EXT 
,a.[Posting Date]
,a.[Document Date]
,MONTH(a.[Document Date]) 
,a.[Shortcut Dimension 1 Code]
,a.[Shortcut Dimension 2 Code]
,a.[Customer Posting Group]
,a.[External Document No_]
,a.[Payment Method Code]
,a.[Authorization No_]
,a.[Amount Authorized]
,b.ORH_ID
,b.ORH_CREATED_FOR
      
----COACH INFO----
IF object_id('Tempdb..#Coach1') Is Not Null DROP TABLE #Coach1
select 
 A.CUSTOMER_ID
,CUSTOMER_NUMBER
,FIRST_NAME+' '+LAST_NAME  OdysseyName
,EMAIL1_ADDRESS 
,Case When [Rank] = 'None' then 'Client' else 'Coach' end as [Rank] 
,MONTH(Commission_Period) as CalMOnth
INTO #COACH1
from [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER] a
Join [ODYSSEY_ETL].[dbo].[ODYSSEY_BUSINESSCENTER]b
on a.CUSTOMER_ID = b.CUSTOMER_ID
where Convert(Varchar(10),Commission_Period,121)>= (SELECT Convert(Varchar(10),DATEADD(MONTH, DATEDIFF(MONTH, '19000101', GETDATE()) - 1, '19000101'),121))----First Day Previous Month-----
and CUSTOMER_NUMBER in(Select distinct [Salesperson Code] COLLATE DATABASE_DEFAULT from #sales)

----CREDIT CARD TRANSACTION----
IF object_id('Tempdb..#Final') Is Not Null DROP TABLE #Final
SELECT
 a.[No_] as DocumentNumber
,a.[Salesperson Code]
,b.OdysseyName as CoachName
,b.[Rank]
,a.[Sell-to Customer No_]
,a.[Sell-to Customer Name]
,a.[Bill-to Customer No_]
,a.[Bill-to Name]
,a.[Ship-to Name]
,a.[Credit Card Holder Name]
,a.[Brand]
,a.CCD_NUMBER_EXT
,Case When b.OdysseyName = a.[Credit Card Holder Name] and b.OdysseyName COLLATE DATABASE_DEFAULT<>  a.[Ship-to Name] Then  'Violation' Else 'Ok' End as [CreditCardRules]
,a.[Document Date]
,a.CalMonth
,a.[Shortcut Dimension 1 Code]
,a.[Customer Posting Group]
,a.[External Document No_]
,a.[Authorization No_]
,a.[Amount Authorized]
,a.ORH_ID as CartOrderId
into #Final
  FROM #sales a
  Left Join #Coach1 b
  on a.[Salesperson Code] COLLATE DATABASE_DEFAULT= b.CUSTOMER_NUMBER
  and MONTH([Document Date]) = b.CalMOnth
 group by
 a.[No_]
,a.[Salesperson Code]
,b.OdysseyName
,b.[Rank]
,a.[Sell-to Customer No_]
,a.[Bill-to Customer No_]
,a.[Bill-to Name]
,a.[Sell-to Customer Name]
,a.[Ship-to Name]
,a.[Credit Card Holder Name]
,a.[Brand]
,a.CCD_NUMBER_EXT
,a.[Document Date]
,a.CalMonth
,a.[Shortcut Dimension 1 Code]
,a.[Customer Posting Group]
,a.[External Document No_]
,a.[Authorization No_]
,a.[Amount Authorized]
,a.ORH_ID 
order by [No_]

-------FINAL TABLE-------
Truncate Table dbo.XL_039_TSFL_Compliance_Exception_Violation
Insert Into dbo.XL_039_TSFL_Compliance_Exception_Violation 
(DocumentNumber
,[Salesperson Code]
,CoachName
,[Rank]
,[Sell-to Customer No_]
,[Sell-to Customer Name]
,[Bill-to Customer No_]
,[Bill-to Name]
,[Ship-to Name]
,[Credit Card Holder Name]
,Brand
,CCD_NUMBER_EXT
,CreditCardRules
,[Document Date]
,CalMonth
,[Shortcut Dimension 1 Code]
,[Customer Posting Group]
,[External Document No_]
,[Authorization No_]
,[Amount Authorized]
)
Select
 DocumentNumber
,[Salesperson Code]
,CoachName
,[Rank]
,[Sell-to Customer No_]
,[Sell-to Customer Name]
,[Bill-to Customer No_]
,[Bill-to Name]
,[Ship-to Name]
,[Credit Card Holder Name]
,Brand
,CCD_NUMBER_EXT
,CreditCardRules
,[Document Date]
,CalMonth
,[Shortcut Dimension 1 Code]
,[Customer Posting Group]
,[External Document No_]
,[Authorization No_]
,[Amount Authorized]
From #Final 
Where CreditCardRules='Violation' and [Rank] ='Coach' 
Order By [Document Date], [Salesperson Code]

-----FINAL TABLE----------------
Truncate Table dbo.XL_039_TSFL_Compliance_Exception_Sales_Above_$1000
Insert Into  dbo.XL_039_TSFL_Compliance_Exception_Sales_Above_$1000
(
 [Sell-to Customer No_]
,[Sell-to Customer Name]
,[Salesperson Code]
,CoachName
,CoachEmail
,CalMonth
,GrossSales
,OrderCounts
)
Select 
[Sell-to Customer No_]
,[Sell-to Customer Name]
,[Salesperson Code] 
,OdysseyName as CoachName
,EMAIL1_ADDRESS as CoachEmail
,MONTH([Document Date]) as CalMonth
,SUM(GrossSales) as GrossSales
,COUNT([No_]) as OrderCounts
From #sales a
Left Join #Coach1 b
  on a.[Salesperson Code] COLLATE DATABASE_DEFAULT= b.CUSTOMER_NUMBER 
  and MONTH([Document Date]) = b.CalMOnth
--Where MONTH([Document Date]) = MONTH(Getdate()-1)
Group By
[Sell-to Customer No_]
,[Sell-to Customer Name]
,[Salesperson Code] 
,OdysseyName
,MONTH([Document Date])
,EMAIL1_ADDRESS
Having SUM(GrossSales)>=1000



--select * from #sales where [Sell-to Customer No_]='TSFL18560801'
  
--select * from #COACH1 where CUSTOMER_NUMBER= '2564801'







