﻿

/*
==========================================================================================
Author:         Menkir Haile
Create date: 08/24/2015
-------------------------TSFL Monthly Snapshot---------------------------------
---------------Pull monthly figures for TSFL Executive Report-----------------------------
==========================================================================================   
REFERENCES
Database              Table/View/UDF									 Action            
------------------------------------------------------------------------------------------    
BI_Reporting			TSFL_monthly_snapshot TSFL_monthly_snapshot		Select                          
===========================================================================================
REVISION LOG
Date           Name                          Change
-------------------------------------------------------------------------------------------

==========================================================================================
NOTES:
------------------------------------------------------------------------------------------
==========================================================================================
**/
CREATE procedure [dbo].[USP_XL_045_TSFL_Monthly_Snapshot] as 
set nocount on ;
SELECT TSFL_monthly_snapshot."Last Day of Month",
		Month( TSFL_monthly_snapshot."Last Day of Month") AS CalMonth,

		TSFL_monthly_snapshot.LastDateOfQuarter, 
		TSFL_monthly_snapshot.CalendarYear, 
		TSFL_monthly_snapshot.CalendarMonth, 
		TSFL_monthly_snapshot."Active Earners", 
		TSFL_monthly_snapshot."Total Health Coaches", 
		TSFL_monthly_snapshot."% of Earning Coaches", 
		TSFL_monthly_snapshot.Sponsoring, 
		TSFL_monthly_snapshot.Attrition, 
		TSFL_monthly_snapshot."Attrition %", 
		TSFL_monthly_snapshot."New Clients", 
		TSFL_monthly_snapshot."Orders per Coach", 
		TSFL_monthly_snapshot."Revenue Per Coach"
FROM [Bi_Reporting].dbo.TSFL_monthly_snapshot TSFL_monthly_snapshot



