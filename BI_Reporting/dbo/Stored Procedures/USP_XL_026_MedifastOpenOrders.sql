﻿

/*

==================================================================================

Author:         Luc Emond

Create date: 06/26/2013

---------------------------[USP_XL_026_MedifastOpenOrders]-------------------

---Provide open orders----

==================================================================================   

REFERENCES

Database              Table/View/UDF                       Action            

----------------------------------------------------------------------------------

[NAVISION_PROD]      [Jason Pharm$Sales Header]            SELECT

                        

==================================================================================

REVISION LOG

Date           Name                          Change

----------------------------------------------------------------------------------



==================================================================================

NOTES:



----------------------------------------------------------------------------------

==================================================================================

*/



CREATE PROCEDURE [dbo].[USP_XL_026_MedifastOpenOrders] as 

Set NoCount on;





--Syntax that needs to be inserted in front of the statement that accesses the NAVPOD database

--EXECUTE AS LOGIN = 'ReportViewer'



-------------------------------------------------------

--SELECT GETDATE()as ReportDateTime, [Coupon_Promotion Code],[Order Date], [Document Date], [DT Order Conception],

--Case When [Coupon_Promotion Code] ='EXCHANGE' Then [Posting Date] Else [Order Date] end as [OrderDate], *

--FROM [NAVPROD].[dbo].[Jason Pharm$Sales Header]

--WHERE [Document Type]=1

--and [Amount Authorized] >0



SELECT GETDATE()as ReportDateTime, *

FROM NAVISION.[NAVPROD].[dbo].[Jason Pharm$Sales Header] WITH (NOLOCK)

WHERE [Document Type]=1

and [Amount Authorized] >0

and  [Coupon_Promotion Code] <>'EXCHANGE'


