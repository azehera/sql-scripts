﻿


create PROCEDURE [dbo].[USP_XL_068_MWCC_P&L_Franchisee_Cost_test] as 
SET NOCOUNT ON;

/*
==================================================================================
Author:         Luc Emond
Create date: 05/19/2014
-------------------------[USP_XL_066_MWCC_P&L_Franchisee_Cost]------------------
Provide Daily Sales Transaction by Customer for the reward program-----
=================================================================================    
REFERENCES
Database              Table/View/UDF                       Action            
----------------------------------------------------------------------------------
[BI_SSAS_Cubes]       [dbo].[FactGL_cube]                  SELECT   
[BI_SSAS_Cubes]       [dbo].[DimGLCategory]                SELECT
[BI_SSAS_Cubes]       [dbo].[DimGLCubeCalendar]            SELECT
[BI_SSAS_Cubes]       [dbo].[DimGlobalDimension1Code]      SELECT
=====================================================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------------------------------------------
9/22/2014 D. Smith					Added where condition to last statement to filter out corporate centers
======================================================================================================================
NOTES:
----------------------------------------------------------------------------------------------------------------------

======================================================================================================================
*/

-----VARIABLE TABLE-----
Declare @a Table (
[EntryNO] [int] NULL,
[CalendarYear] [int] NULL,
[CalendarQuarter] [int] NULL,
[CalendarMonthName] [char](15) NULL,
[CalendarMonthID] [int] NULL,
[G_L Account No_] [varchar](20) NULL,
[G_L Name] [varchar](100) NULL,
[RollUpName] [varchar](200) NULL,
[Category] [varchar](11) NULL,	
[Global Dimension 1 Code] [varchar](20) NULL,
[LocationName] [varchar](50) NULL,
[Global Dimension 2 Code] [varchar](50) NULL,
[NetTotal] [decimal](38, 18)  NULL
)
	
Insert Into @a
Select 
 [EntryNO]
,c.CalendarYear
,c.CalendarQuarter
,c.CalendarMonthName 
,a.[CalendarMonthID]
,a.[G_L Account No_]
,b.[G_L Name]
,b.[RollUpName]
,b.[Category]
,a.[Global Dimension 1 Code]
,d.LocationName
,a.[Global Dimension 2 Code]
,Sum(a.[NetTotal]) as NetTotal
From [BI_SSAS_Cubes].[dbo].[FactGL_cube] a
Left join [BI_SSAS_Cubes].[dbo].[DimGLCategory] b
  on a.[G_L Account No_] = b.[G_L Account No_]
Left Join [BI_SSAS_Cubes].[dbo].[DimGLCubeCalendar] c
  on a.CalendarMonthID = c.CalendarMonthID
Left Join [BI_SSAS_Cubes].[dbo].[DimGlobalDimension1Code] d
  on a.[Global Dimension 1 Code] = d.[Global Dimension 1 Code]
Where a.[Global Dimension 1 Code] between '200' and '999'
  and a.CalendarMonthID>='201201'
  and a.[G_L Account No_] >='40000'
  and a.[G_L Account No_] not in
  ('59980', '64998', '69996', '74998', '79998', '85998','89996', '94005', '94010', '94020', '94025', '20400', '20700', '23253',
  '42650', '63000', '57000', '63001', '63002', '84200', '84800', '65500', '86200', '91050', '92000', '85404', '86700', '98000')
  and [Source Code]<>'CLSINCOME'
Group By
[EntryNO]
,c.CalendarYear
,c.CalendarMonthName 
,a.[CalendarMonthID]
,a.[G_L Account No_]
,b.[G_L Name]
,b.[RollUpName]
,b.[Category]
,a.[Global Dimension 1 Code]
,a.[Global Dimension 2 Code]
,d.LocationName
,c.CalendarQuarter

------ROYALTY OF $1000 PER MONTH AS PER JACK WICKHAM III---------
insert into @a
select 
 distinct [EntryNO]
,CalendarYear
,CalendarQuarter
,CalendarMonthName 
,[CalendarMonthID]
,'95600' as [G_L Account No_]
,'Royalty'as [G_L Name] ---'Royalty'
,'Total Cost of Goods Sold' as [RollUpName]
,'SG&A' as [Category]
,[Global Dimension 1 Code]
,LocationName
,[Global Dimension 2 Code]
,-1000.00  as NetTotal 
from @a
where [G_L Account No_]='42400'

-----TEMP TABLE FOR RECALCULATION OF THE CSOT OF GOOD SOLD------
IF object_id('tempdb..#B') is not null DROP TABLE #B
Select  
CalendarYear
,CalendarMonthName 
,[CalendarMonthID]
,'51100' as [G_L Account No_]
,[Global Dimension 1 Code]
,(Sum([NetTotal])/2)*-1 as NetTotal
Into #B from @a
Where 
[G_L Account No_] in('42400','49970')
Group by CalendarYear
,CalendarMonthName 
,[CalendarMonthID]
,[Global Dimension 1 Code]

---update new total cost-----
Update a
Set  a.NetTotal=b.NetTotal
From @a a
join #B b
   on a.CalendarYear=b.CalendarYear
   and a.CalendarMonthID=b.CalendarMonthID
   and a.[Global Dimension 1 Code]=b.[Global Dimension 1 Code]
   and a.[G_L Account No_]=b.[G_L Account No_]

----final----
Select 
 [EntryNO]
,CalendarYear
,CalendarQuarter
,CalendarMonthName 
,[CalendarMonthID]
,[G_L Account No_]
,[G_L Name]
,[RollUpName]
,[Category]
,[Global Dimension 1 Code]
,LocationName
,[Global Dimension 2 Code]
, isnull(NetTotal,0) as NetTotal
 From @a
 where [Global Dimension 1 Code]<>'299'
 AND LocationName NOT IN (SELECT location_name FROM dbo.B4T_location WHERE OpenStores >= 1)





