﻿





/*

===============================================================================

Author:  Kalepsh Patel

Create date: 03/31/2013

-------------------------[USP_XL_063_ NAV_Commissionable Sales]-----------------------------

---------------------------------

==============================================================================    

REFERENCES

Database              Table/View/UDF                             Action            

-------------------------------------------------------------------------------

[NAV_ETL].[dbo].[Jason Pharm$Sales Header]				         Select 

[NAV_ETL].[dbo].[Jason Pharm$Sales Line]                         Select

[NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header]               Select

[NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line]                 Select

[NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header]               Select 

[NAV_ETL].[dbo].[Jason Pharm$Sales Invoice line]                 Select 

[BI_Reporting].[dbo].[calendar]                                  Select 

===============================================================================

REVISION LOG

Date            Name                          Change

-------------------------------------------------------------------------------

                                          

===============================================================================

NOTES:

-------------------------------------------------------------------------------

===============================================================================

*/


CREATE procedure [dbo].[USP_XL_063_ NAV_Commissionable Sales]

as 

--------------------Pulling Open Orders Information-----------------

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A

SELECT A.[Document Type]

	  ,A.[No_]

      ,A.[Sell-to Customer No_]

      ,convert(varchar(10),[Order Date],121) as [Order Date]

      ,[Customer Posting Group]

      --,[Document Date]

      ,[External Document No_]

	,sum(B.[Amount]) as [Commissionable Total]

Into #a

  FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Header] A

	Join [NAV_ETL].[dbo].[Jason Pharm$Sales Line] B

		On A.[No_] = B.[Document No_]

	Where [Coupon_Promotion Code] <> 'EXCHANGE'

		and A.[Document Type]= '1'

		and [Customer Posting Group] = 'TSFL'

		and [Type] = '2'

		and [Posting Group] not in ('KITNPD','NPD','Printing','PKG')

		and convert(varchar(10),[Order Date],121) < Getdate()

Group by A.[Document Type]

	  ,A.[No_]

      ,A.[Sell-to Customer No_]

      ,[Order Date]

      ,[Customer Posting Group]

      ,[Document Date]

      ,[External Document No_]





IF object_id('Tempdb..#open') Is Not Null DROP TABLE #open     

Select 

	[Order Date]

	,SUM([Commissionable Total]) as [Total Open]

	into #open

From #a

	Group by [Order Date]



--------------Pulling Posted Order Information---------------------------	 

IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B

SELECT A.[No_]

      ,A.[Sell-to Customer No_]

      ,convert(varchar(10),[Order Date],121) as [Order Date]

      ,[Customer Posting Group]

      --,[Document Date]

      ,[External Document No_]

	,sum(B.[Amount]) as [Commissionable Total]

Into #b

  FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] A

	Join [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line]  B

		On A.[No_] = B.[Document No_]

	Where [Customer Posting Group] = 'TSFL'

		and [Type] = '2'

		and [Posting Group] not in ('KITNPD','NPD','Printing','PKG')

		and convert(varchar(10),[Order Date],121) > = GETDATE ()- 40

		or [Customer Posting Group] = 'TSFL'

		and [Type] = '3'

		and B.[No_] = 'CCADJUST'

		and convert(varchar(10),[Order Date],121) > = GETDATE ()- 40

Group by A.[No_]

      ,A.[Sell-to Customer No_]

      ,[Order Date]

      ,[Customer Posting Group]

      ,[Document Date]

      ,[External Document No_]

  



IF object_id('Tempdb..#invoiced') Is Not Null DROP TABLE #invoiced  	   

Select 

	[Order Date]

	,SUM([Commissionable Total]) as [Total Invoiced]

	into #invoiced

From #b

	 Group by [Order Date]

	 

-------------------Pull Return Data------------------------------

IF object_id('Tempdb..#Returns') Is Not Null DROP TABLE #Returns

SELECT A.[No_]

      ,A.[Sell-to Customer No_]

      ,convert(varchar(10),A.[Posting Date],121) as [Process Date]

      ,[Customer Posting Group]

      --,[Document Date]

      ,[External Document No_]

	,(sum(B.[Amount])*-1) as [Ret. Commissionable Total]

Into #returns

  FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] A

	Join [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line] B

		On A.[No_] = B.[Document No_]

	Where convert(varchar(10),A.[Posting Date],121) > = GETDATE() - 40

		and convert(varchar(10),A.[Posting Date],121) < Getdate()

		and right([External Document No_],2) <> 'ex' 

		and [Customer Posting Group] = 'TSFL'

		and [Type] = '2'

		and [Posting Group] not in ('KITNPD','NPD','Printing','PKG')

Group by A.[No_]

      ,A.[Sell-to Customer No_]

      ,A.[Posting Date]

      ,[Customer Posting Group]

      ,[Document Date]

      ,[External Document No_]

      

--- Removed  as per request of emily on 05/27/2014    

-- ---------------Full Integration Kit Deductions----------------

-- IF object_id('Tempdb..#K') Is Not Null DROP TABLE #K

-- SELECT 

-- B.[Order Date]

--,B.[Sell-to Customer No_]

--,B.[Sell-to Customer Name] as [Name]

--,B.[External Document No_]as Orders

--,A.[Quantity]

--,(A.[Quantity]*-60.50) as [Kit Deduction]

--Into #k

--FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A 

--Inner Join 

--     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 

--     on A.[Document No_]= B.[No_]

--WHERE B.[Order Date] >= GETDATE() - 40

--and B.[Order Date] < GETDATE()

--and [Customer Posting Group] in ('TSFL')

--and A.[No_] = '74868'

	

---------Open Orders-----------------

--insert #k



--SELECT 

-- B.[Order Date]

--,B.[Sell-to Customer No_]

--,B.[Sell-to Customer Name] as [Name]

--,B.[External Document No_]as Orders

--,A.[Quantity]

--,(A.[Quantity]*-60.50) as [Kit Deduction]

--FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Line] A

--	Join [NAV_ETL].[dbo].[Jason Pharm$Sales Header] B

--		On B.[No_] = A.[Document No_]

--WHERE B.[Order Date] >= GETDATE() - 40

--and B.[Order Date] < GETDATE()

--and [Customer Posting Group] in ('TSFL')

--and A.[No_] = '74868'



--IF object_id('Tempdb..#kits') Is Not Null DROP TABLE #kits

--Select 

--	[Order Date]

--	,SUM([Kit Deduction]) as [Kit Deduction]

--Into #kits

--From #k

--	Group by [Order Date]    

	

	 

 ---------------Flavors of Home Deductions added as per request of emily on 05/27/2014----------------

 IF object_id('Tempdb..#F') Is Not Null DROP TABLE #F

 SELECT 

 B.[Order Date]

,B.[Sell-to Customer No_]

,B.[Sell-to Customer Name] as [Name]

,B.[External Document No_]as Orders

,A.[Quantity]

,(A.[Amount]/2) as [FoH Index]

Into #F

FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] A 

Inner Join 

     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] B 

     on A.[Document No_]= B.[No_]

WHERE B.[Order Date] >= GETDATE() - 40

and B.[Order Date] < GETDATE()

and [Customer Posting Group] in ('TSFL')

and A.[No_] in ('66625','66630','66605')



-------Open Orders-----------------

insert #F



SELECT 

 B.[Order Date]

,B.[Sell-to Customer No_]

,B.[Sell-to Customer Name] as [Name]

,B.[External Document No_]as Orders

,A.[Quantity]

,(A.[Amount]/2) as [FoH Index]

FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Line] A

	Join [NAV_ETL].[dbo].[Jason Pharm$Sales Header] B

		On B.[No_] = A.[Document No_]

WHERE B.[Order Date] >= GETDATE() - 10

and B.[Order Date] < GETDATE()

and [Customer Posting Group] in ('TSFL')

and A.[No_] in ('66625','66630','66605')



IF object_id('Tempdb..#FoH') Is Not Null DROP TABLE #FoH

Select 

	[Order Date]

	,SUM([FoH Index]) as [FoH Index Deduction]

Into #FoH

From #F

	Group by [Order Date] 

		

----------------------Compile as one report---------------------	     

Select 
--convert(varchar(10),[CalendarDate],121) as [Date]

--	   ,isnull(B.[Total Invoiced],'0') as [Total Invoiced]

--	   ,isnull(A.[Total Open],'0') as [Total Open]

--	   ,isnull(sum([Ret. Commissionable Total]),'0') as [Total Returns]

--	   --,ISNULL([Kit Deduction],'0') as [Kit Deduction] -----Removed  as per request of emily on 05/27/2014

--	   ,ISNULL([FoH Index Deduction]*-1,'0') as [FoH Index Deduction]

convert(varchar(10),[CalendarDate],121) as [Date],
convert(numeric(10,2),isnull(B.[Total Invoiced],0)) as [Total Invoiced]
,convert(numeric(10,2),isnull(A.[Total Open],0)) as [Total Open]
,convert(numeric(10,2),isnull(sum([Ret. Commissionable Total]),0)) as [Total Returns]
--,ISNULL([Kit Deduction],'0') as [Kit Deduction] -----Removed  as per request of emily on 05/27/2014
,convert(numeric(10,2),ISNULL([FoH Index Deduction]*-1,0)) as [FoH Index Deduction]


From [BI_Reporting].[dbo].[calendar] C

	Left join #open A

		on convert(varchar(10),C.CalendarDate,121) = A.[Order Date]

	Left join #invoiced B

		on convert(varchar(10),C.CalendarDate,121)= B.[Order Date]

	Left join #returns R

		on convert(varchar(10),C.CalendarDate,121)= R.[Process Date]

	--Left Join #kits K

	--	on convert(varchar(10),C.CalendarDate,121)= K.[Order Date]

	Left Join #FoH F

		on convert(varchar(10),C.CalendarDate,121)= F.[Order Date]

Where convert(varchar(10),[CalendarDate],121) >= GETDATE() - 40

	and convert(varchar(10),[CalendarDate],121) < convert(varchar(10),GETDATE(),121)

Group by [CalendarDate],[Total Invoiced],[Total Open],--[Kit Deduction]

[FoH Index Deduction]

	




