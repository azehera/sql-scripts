﻿

/*
--==================================================================================
Author:         Daniel Dagnachew
Create date: 10/11/2019
------------------------------------------------
Future Autoship orders
--==================================================================================   
REFERENCES
Database              Table/View/UDF                         Action            
------------------------------------------------------------------------------------
Hybris_ETL            mdf_prd_database.orders                Select
Hybris_ETL            mdf_prd_database.processes             Select
Hybris_ETL            mdf_prd_database..enumerationvalues    Select  
Hybris_ETL            mdf_prd_database.cronjobs              Select
BI_Reporting          dbo.Autoship_Channel_Count_Snapshot    Select                     
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------
2/26/2020     Daniel D.                 added logic to capture zero count
==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_RT_4_and_7_Email_Notice]
AS

BEGIN 



--DECLARE @lastthirtythreedays DATE;
--SET @lastthirtythreedays = CAST(DATEADD(dd, -33, GETDATE()) AS DATE)

--IF OBJECT_ID('Tempdb..#last33days') IS NOT NULL DROP TABLE #last33days;
--;WITH CTE AS (SELECT CONVERT(DATE, GETDATE()) OrderDate
--             UNION ALL
--             SELECT DATEADD(DAY, -1, OrderDate)FROM CTE WHERE OrderDate> @lastthirtythreedays)
--SELECT * INTO #last33days FROM CTE

--IF OBJECT_ID('Tempdb..#fourSev') IS NOT NULL DROP TABLE #fourSev;
--CREATE TABLE #fourSev (fourSev NVARCHAR(255));
--INSERT INTO #fourSev----Populate the processes for email analytics
--(fourSev)
--VALUES('4'),('7')

--IF OBJECT_ID('Tempdb..#Channel') IS NOT NULL DROP TABLE #Channel;
--CREATE TABLE #Channel (Channel VARCHAR(20));
--INSERT INTO #Channel----Populate the channel for email analytics
--(Channel)
--VALUES ('Optavia - US '),('Optavia - HK'),('Optavia - SG')


-----Getting email counts for three distinct processes for the last 33 days
--IF OBJECT_ID('TEMPDB..#Email') IS NOT NULL
--    DROP TABLE #Email;
--SELECT CAST(createdTS AS DATE) AS OrderDate,
--       p_processdefinitionname,
--       CASE
--           WHEN p_site IN ( 8796093088808, 8796125922344 ) THEN
--               'Optavia - US '
--           WHEN p_site IN ( 8796093056040) THEN
--               'Medifast'
--           WHEN p_site = 8796125856808 THEN
--               'Optavia - HK'
--           WHEN p_site = 8796125824040 THEN
--               'Optavia - SG'
--           ELSE
--               'other'
--       END AS 'channel',
--       ISNULL(   CAST(CASE
--                          WHEN p_processdefinitionname = 'sendHCAutoshipReminderEmailProcess' THEN
--                              'Coach'
--                          WHEN p_processdefinitionname = 'medifastSubscriptionReminderEmailProcess' THEN
--                              'Client'
--                      END AS NVARCHAR(20)),
--                 'OrderConfirmation'
--             ) AS Customer_Type,
--       CASE
--           WHEN p_numofdays IS NULL
--                AND p_processdefinitionname = 'sendHCAutoshipReminderEmailProcess' THEN
--               'HCReminder'
--           WHEN p_numofdays IS NULL
--                AND p_processdefinitionname = 'hcOrderConfirmationEmailProcess' THEN
--               'OrderConfirmation'
--           ELSE
--               CAST(p_numofdays AS NVARCHAR(20))
--       END AS '4 or 7',
--       COUNT(PK) Count_PK
--INTO #Email
--FROM Hybris_ETL.mdf_prd_database.processes
--WHERE CAST(createdTS AS DATE) > = @lastthirtythreedays
--      AND p_processdefinitionname IN ( 'medifastSubscriptionReminderEmailProcess',
--                                       'sendHCAutoshipReminderEmailProcess', 'hcOrderConfirmationEmailProcess'
--                                     )
--GROUP BY CAST(createdTS AS DATE),
--         p_processdefinitionname,
--         p_site,
--         p_numofdays
--ORDER BY CAST(createdTS AS DATE),
--         p_processdefinitionname,
--         p_site;

--IF OBJECT_ID('TEMPDB..#stg') IS NOT NULL
--    DROP TABLE #stg;
--SELECT *  INTO #stg FROM #Email

--UNION ALL 

--SELECT OrderDate,'hcOrderConfirmationEmailProcess' AS p_processdefinitionname,'other' AS channel, 'OrderConfirmation' AS Customer_Type, 'OrderConfirmation' AS '4 and 7', 0 AS Count_PK FROM #last33days 
--WHERE orderDate IN (
--SELECT OrderDate FROM #last33days 
--EXCEPT 
--SELECT OrderDate FROM #Email WHERE p_processdefinitionname = 'hcOrderConfirmationEmailProcess')

--UNION ALL

--SELECT OrderDate,'medifastSubscriptionReminderEmailProcess' AS p_processdefinitionname,'Medifast' AS channel, 'Client' AS Customer_Type,  [4 and 7], 0 AS Count_PK 
--From (
--SELECT OrderDate,'medifastSubscriptionReminderEmailProcess' AS p_processdefinitionname,'Medifast' AS channel, 'Client' AS Customer_Type,fourSev AS '4 and 7' FROM #last33days CROSS JOIN #fourSev 
--EXCEPT 
--SELECT OrderDate,p_processdefinitionname,channel,Customer_Type,[4 or 7] FROM #Email WHERE p_processdefinitionname = 'medifastSubscriptionReminderEmailProcess' AND channel = 'Medifast' )Med


--UNION ALL 

--SELECT *,0 AS Count_PK FROM(
--SELECT OrderDate,'medifastSubscriptionReminderEmailProcess' AS p_processdefinitionname,Channel,'Client' AS Customer_Type,fourSev '4 or 7' FROM #last33days CROSS JOIN #Channel CROSS JOIN #fourSev
--EXCEPT 
--SELECT OrderDate,p_processdefinitionname,channel,Customer_Type,[4 or 7] FROM #Email WHERE p_processdefinitionname = 'medifastSubscriptionReminderEmailProcess')A

--UNION ALL 

--SELECT *,0 AS Count_PK FROM(
--SELECT OrderDate,'sendHCAutoshipReminderEmailProcess' AS p_processdefinitionname,Channel,'Coach' AS Customer_Type,fourSev '4 or 7' FROM #last33days CROSS JOIN #Channel CROSS JOIN #fourSev
--EXCEPT 
--SELECT OrderDate,p_processdefinitionname,channel,Customer_Type,[4 or 7] FROM #Email WHERE p_processdefinitionname = 'sendHCAutoshipReminderEmailProcess')B


--IF OBJECT_ID('TEMPDB..#expected') IS NOT NULL
--    DROP TABLE #expected;
--SELECT CAST(Snapshot_Date AS DATE) Today,
--       Channel,
--       [4 or 7],
--       Expected,
--       Customer_Type
--INTO #expected
--FROM [BI_Reporting].[dbo].[Autoship_Channel_Count_Snapshot]
--WHERE CAST(Snapshot_Date AS DATE) >= @lastthirtythreedays
--      AND Channel <> 'other'
--UNION
-------Pulling historical confirmation on the fly
--SELECT CAST(o.createdTS AS DATE) AS 'Today',
--       'other' AS Channel,
--       'OrderConfirmation' AS '4 or 7',
--       COUNT(CAST(o.p_code AS CHAR(255))) AS 'Expected',
--       'OrderConfirmation' AS Customer_Type
--FROM Hybris_ETL.mdf_prd_database.orders AS o WITH (NOLOCK)
--    LEFT JOIN Hybris_ETL.mdf_prd_database.enumerationvalues AS e WITH (NOLOCK)
--        ON o.p_status = e.PK
--WHERE CAST(o.createdTS AS DATE) >= @lastthirtythreedays
--      AND e.Code IS NOT NULL
--GROUP BY CAST(o.createdTS AS DATE);

--SELECT e.*,
--       ex.Expected,
--      CASE WHEN  e.Count_PK <  0.99  * ex.Expected   THEN
--               'yes'
--           ELSE
--               'no'
--       END AS 'Less than expected'
--FROM #stg e
--    LEFT JOIN #expected ex
--        ON ex.[4 or 7] = e.[4 or 7]
--           AND ex.Today = e.OrderDate
--           AND ex.Channel = e.channel
--           AND ex.Customer_Type = e.Customer_Type
--ORDER BY e.OrderDate;


DECLARE @lastthirtythreedays DATE;
SET @lastthirtythreedays = CAST(DATEADD(dd, -33, GETDATE()) AS DATE)

IF OBJECT_ID('Tempdb..#last33days') IS NOT NULL DROP TABLE #last33days;
;WITH CTE AS (SELECT CONVERT(DATE, GETDATE()) OrderDate
             UNION ALL
             SELECT DATEADD(DAY, -1, OrderDate)FROM CTE WHERE OrderDate> @lastthirtythreedays)
SELECT * INTO #last33days FROM CTE

IF OBJECT_ID('Tempdb..#fourSev') IS NOT NULL DROP TABLE #fourSev;
CREATE TABLE #fourSev (fourSev NVARCHAR(255));
INSERT INTO #fourSev----Populate the processes for email analytics
(fourSev)
VALUES('4'),('7')

IF OBJECT_ID('Tempdb..#Channel') IS NOT NULL DROP TABLE #Channel;
CREATE TABLE #Channel (Channel VARCHAR(20));
INSERT INTO #Channel----Populate the channel for email analytics
(Channel)
VALUES ('Optavia - US'),('Optavia - HK'),('Optavia - SG')


---Getting email counts for three distinct processes for the last 33 days
IF OBJECT_ID('TEMPDB..#Email') IS NOT NULL
    DROP TABLE #Email;
SELECT CAST(createdTS AS DATE) AS OrderDate,
       p_processdefinitionname,
       CASE
           WHEN p_site IN ( 8796093088808, 8796125922344 ) THEN
               'Optavia - US'
           WHEN p_site IN ( 8796093056040) THEN
               'Medifast'
           WHEN p_site = 8796125856808 THEN
               'Optavia - HK'
           WHEN p_site = 8796125824040 THEN
               'Optavia - SG'
           ELSE
               'other'
       END AS 'channel',
       ISNULL(   CAST(CASE
                          WHEN p_processdefinitionname = 'sendHCAutoshipReminderEmailProcess' THEN
                              'Coach'
                          WHEN p_processdefinitionname = 'medifastSubscriptionReminderEmailProcess' THEN
                              'Client'
						  WHEN p_processdefinitionname = 'OptaviaHealthCoachLeadPoolEmailProcess' THEN
                              'HealthCoachLeadPool'
                      END AS NVARCHAR(20)),
                 'OrderConfirmation'
             ) AS Customer_Type,
       CASE
           WHEN p_numofdays IS NULL
                AND p_processdefinitionname = 'sendHCAutoshipReminderEmailProcess' THEN
               'HCReminder'
		   WHEN p_numofdays IS NULL
                AND p_processdefinitionname = 'OptaviaHealthCoachLeadPoolEmailProcess' THEN
               'HealthCoachLeadPool'
           WHEN p_numofdays IS NULL
                AND p_processdefinitionname = 'hcOrderConfirmationEmailProcess' THEN
               'OrderConfirmation'
           ELSE
               CAST(p_numofdays AS NVARCHAR(20))
       END AS '4 or 7',
       COUNT(PK) Count_PK
INTO #Email
FROM Hybris_ETL.mdf_prd_database.processes
WHERE CAST(createdTS AS DATE) > = @lastthirtythreedays
      AND p_processdefinitionname IN ( 'medifastSubscriptionReminderEmailProcess',
                                       'sendHCAutoshipReminderEmailProcess', 'hcOrderConfirmationEmailProcess','OptaviaHealthCoachLeadPoolEmailProcess'
                                     )
GROUP BY CAST(createdTS AS DATE),
         p_processdefinitionname,
         p_site,
         p_numofdays
ORDER BY CAST(createdTS AS DATE),
         p_processdefinitionname,
         p_site;

IF OBJECT_ID('TEMPDB..#stg') IS NOT NULL
    DROP TABLE #stg;
SELECT *  INTO #stg FROM #Email

UNION ALL 

SELECT OrderDate,'hcOrderConfirmationEmailProcess' AS p_processdefinitionname,'other' AS channel, 'OrderConfirmation' AS Customer_Type, 'OrderConfirmation' AS '4 and 7', 0 AS Count_PK FROM #last33days 
WHERE orderDate IN (
SELECT OrderDate FROM #last33days 
EXCEPT 
SELECT OrderDate FROM #Email WHERE p_processdefinitionname = 'hcOrderConfirmationEmailProcess')

UNION ALL

SELECT OrderDate,'medifastSubscriptionReminderEmailProcess' AS p_processdefinitionname,'Medifast' AS channel, 'Client' AS Customer_Type,  [4 and 7], 0 AS Count_PK 
FROM (
SELECT OrderDate,'medifastSubscriptionReminderEmailProcess' AS p_processdefinitionname,'Medifast' AS channel, 'Client' AS Customer_Type,fourSev AS '4 and 7' FROM #last33days CROSS JOIN #fourSev 
EXCEPT 
SELECT OrderDate,p_processdefinitionname,channel,Customer_Type,[4 or 7] FROM #Email WHERE p_processdefinitionname = 'medifastSubscriptionReminderEmailProcess' AND channel = 'Medifast' )Med


UNION ALL 

SELECT *,0 AS Count_PK FROM(
SELECT OrderDate,'medifastSubscriptionReminderEmailProcess' AS p_processdefinitionname,Channel,'Client' AS Customer_Type,fourSev '4 or 7' FROM #last33days CROSS JOIN #Channel CROSS JOIN #fourSev
EXCEPT 
SELECT OrderDate,p_processdefinitionname,channel,Customer_Type,[4 or 7] FROM #Email WHERE p_processdefinitionname = 'medifastSubscriptionReminderEmailProcess')A

UNION ALL 

SELECT *,0 AS Count_PK FROM(
SELECT OrderDate,'sendHCAutoshipReminderEmailProcess' AS p_processdefinitionname,Channel,'Coach' AS Customer_Type,fourSev '4 or 7' FROM #last33days CROSS JOIN #Channel CROSS JOIN #fourSev
EXCEPT 
SELECT OrderDate,p_processdefinitionname,channel,Customer_Type,[4 or 7] FROM #Email WHERE p_processdefinitionname = 'sendHCAutoshipReminderEmailProcess')B


IF OBJECT_ID('TEMPDB..#expected') IS NOT NULL
    DROP TABLE #expected;
SELECT CAST(Snapshot_Date AS DATE) Today,
       Channel,
       [4 or 7],
       Expected,
       Customer_Type
INTO #expected
FROM [BI_Reporting].[dbo].[Autoship_Channel_Count_Snapshot]
WHERE CAST(Snapshot_Date AS DATE) >= @lastthirtythreedays
      AND Channel <> 'other'
UNION
-----Pulling historical confirmation on the fly
SELECT CAST(o.createdTS AS DATE) AS 'Today',
       'other' AS Channel,
       'OrderConfirmation' AS '4 or 7',
       COUNT(CAST(o.p_code AS CHAR(255))) AS 'Expected',
       'OrderConfirmation' AS Customer_Type
FROM Hybris_ETL.mdf_prd_database.orders AS o WITH (NOLOCK)
    LEFT JOIN Hybris_ETL.mdf_prd_database.enumerationvalues AS e WITH (NOLOCK)
        ON o.p_status = e.PK
WHERE CAST(o.createdTS AS DATE) >= @lastthirtythreedays
      AND e.Code IS NOT NULL
GROUP BY CAST(o.createdTS AS DATE)

UNION
-----Pulling historical optavia pool on the fly

SELECT CAST([TimeStamp] AS DATE) AS 'Today',
       CASE WHEN cu.MainCountry = 'US' THEN 'Optavia - US' 
	        WHEN cu.MainCountry = 'HK' THEN 'Optavia - HK'
			WHEN cu.MainCountry = 'SG' THEN 'Optavia - SG' END AS Channel,
       'HealthCoachLeadPool' AS '4 or 7',
       COUNT([Prospect_Email]) AS 'Expected',
       'HealthCoachLeadPool' AS Customer_Type
FROM [EXIGO].[ExigoSyncSQL_PRD].[CUSTOM].[Prospect_Info] pin
    INNER JOIN [EXIGO].[ExigoSyncSQL_PRD].[dbo].[Customers] cu
        ON pin.[HC_Customer_ID] = cu.[Field1]
WHERE CAST([TimeStamp] AS DATE) >= @lastthirtythreedays
     
GROUP BY CAST([TimeStamp] AS DATE),CASE WHEN cu.MainCountry = 'US' THEN 'Optavia - US' 
	        WHEN cu.MainCountry = 'HK' THEN 'Optavia - HK'
			WHEN cu.MainCountry = 'SG' THEN 'Optavia - SG' END ;


SELECT e.*,
       ex.Expected,
      CASE WHEN  e.Count_PK <  0.99  * ex.Expected   THEN
               'yes'
           ELSE
               'no'
       END AS 'Less than expected'
FROM #stg e
    LEFT JOIN #expected ex
        ON ex.[4 or 7] = e.[4 or 7]
           AND ex.Today = e.OrderDate
           AND ex.Channel = e.channel
           AND ex.Customer_Type = e.Customer_Type
   
ORDER BY OrderDate;







END 
