﻿







-- =============================================
-- Author: Kalpesh Patel
-- Create date: 07/09/2013
-- Description:	Daily Shipment Report
-- =============================================
CREATE PROCEDURE [dbo].[usp_PullDailyShipmentShiftLevel1] 
	-- Add the parameters for the stored procedure here
	@DistCenter varchar(30),
	@Dept varchar(150),
	@StartDate datetime, 
	@EndDate datetime,
	@StartHour varchar(2),
	@StartMinute varchar(2),
	@EndHour varchar(2),
	@EndMin varchar(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @StartValue varchar(25)
	declare @EndValue varchar(25)

	set @StartValue=cast(year(@StartDate) as varchar(4)) +  right('0' + cast(month(@StartDate) as varchar(2)),2) +  right('0' + cast(day(@StartDate) as varchar(2)),2)+ @StartHour + @StartMinute + '00000'
	set @EndValue=cast(year(@EndDate) as varchar(4)) + right('0' + cast(month(@EndDate) as varchar(2)),2) +  right('0' + cast(day(@EndDate) as varchar(2)),2) + @EndHour + @EndMin + '00000'

	print @StartValue
	Print @EndValue

	SELECT  case shipment_header.dc
					when 1 then 'MDC'
					when 2 then 'TDC'
				end as dc
			, reference3
			, count([ui_sales_order_no]) as PCount--,shipment_header.ship_date--, shipment_header.[sid]--, shipment_header.ship_date
	FROM         
						  FLAGSHIP_WMS_ETL.dbo.packages INNER JOIN
						  FLAGSHIP_WMS_ETL.dbo.shipment_header ON packages.sid = shipment_header.sid
	WHERE     (shipment_header.created_timestamp >= @StartValue and shipment_header.created_timestamp<=@EndValue)
				and shipment_header.status='UPLD'
				and reference3 in (select * from ufn_SPLIT(@Dept,','))
			
	group by shipment_header.dc, reference3
	order by shipment_header.dc, reference3

--	 SELECT     flexdata.keyValue, count(Shipment_header.sid) as sCount--,shipment_header.ship_date--, shipment_header.[sid]--, shipment_header.ship_date
--	FROM         flexdata INNER JOIN
--						  shipment_header ON flexdata.sid = shipment_header.sid
--	WHERE     (flexdata.keyName = 'FLEX3') and (shipment_header.ship_date >= @StartValue and shipment_header.ship_date<@EndValue)
--	group by keyvalue
--	order by keyvalue
--
END











