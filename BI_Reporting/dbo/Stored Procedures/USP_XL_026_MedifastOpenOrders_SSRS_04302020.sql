﻿
/*
==================================================================================
Author:         Luc Emond
Create date: 06/26/2013
---------------------------[USP_XL_026_MedifastOpenOrders]-------------------
---Provide open orders----
==================================================================================   
REFERENCES
Database              Table/View/UDF                       Action            
----------------------------------------------------------------------------------
[NAVISION_PROD]      [Jason Pharm$Sales Header]            SELECT
                        
==================================================================================
REVISION LOG
Date           Name                          Change
----------------------------------------------------------------------------------
08/25/2015		Menkir Haile				[NAVISION_PROD] reference changed to NAV_ETL
==================================================================================
NOTES:

----------------------------------------------------------------------------------
==================================================================================
*/

CREATE PROCEDURE [dbo].[USP_XL_026_MedifastOpenOrders_SSRS_04302020]
AS
SET NOCOUNT ON;


--Syntax that needs to be inserted in front of the statement that accesses the NAVPOD database
--EXECUTE AS LOGIN = 'ReportViewer'

-------------------------------------------------------
--SELECT GETDATE()as ReportDateTime, [Coupon_Promotion Code],[Order Date], [Document Date], [DT Order Conception],
--Case When [Coupon_Promotion Code] ='EXCHANGE' Then [Posting Date] Else [Order Date] end as [OrderDate], *
--FROM [NAVPROD].[dbo].[Jason Pharm$Sales Header]
--WHERE [Document Type]=1
--and [Amount Authorized] >0

--SELECT GETDATE()as ReportDateTime, 
--		[Location Code],
--		[Customer Posting Group],
--		--OrderSource,
--		Cast([Order Date] AS Date) AS [Order Date],
--		[Amount Authorized],
--		[Shortcut Dimension 1 Code]
----FROM [NAV_ETL].dbo.[Jason Pharm$Sales Header] WITH (NOLOCK)
--FROM NAVISION.[NAVPROD].[dbo].[Jason Pharm$Sales Header] WITH (NOLOCK)----Modified by Daniel Dagnachew 3/24/2016 because we rolled back from the office writer vesion
--WHERE [Document Type]=1
--and [Amount Authorized] >0
--and  [Coupon_Promotion Code] <>'EXCHANGE'
--and [Customer Posting Group] in ('MEDIFAST','TSFL','DOCTORS', 'SPRTNTR', 'FRANCHISE')


--SELECT  GETDATE()as ReportDateTime,
--               SI.[Location Code],
--              [Customer Posting Group],
--              Units,
--              Cast([Order Date] AS Date) AS [Order Date],
--              [Amount Authorized],
--              SI.[Shortcut Dimension 1 Code]
--FROM NAVISION.[NAVPROD].[dbo].[Jason Pharm$Sales Header] SI  WITH (NOLOCK)
--join (Select [Document No_]
--             , sum(([Quantity] * [Qty_ per Unit of Measure]))UNITS 
--      from  NAVISION.[NAVPROD].[dbo].[Jason Pharm$Sales Line]  WITH (NOLOCK)
--	  group by [Document No_]) SL
--ON SI.[No_] = SL.[Document No_]
--WHERE SI.[Document Type]=1
--and [Amount Authorized] >0
--and  [Coupon_Promotion Code] <>'EXCHANGE'
--and [Customer Posting Group] in ('MEDIFAST','TSFL','DOCTORS', 'SPRTNTR', 'FRANCHISE')

SELECT GETDATE() AS ReportDateTime,
       SI.[Location Code],
       [Customer Posting Group],
       UNITS,
       CAST([Order Date] AS DATE) AS [Order Date],
       [Amount Authorized],
       SI.[Shortcut Dimension 1 Code]
FROM NAV_ETL.[dbo].[Jason Pharm$Sales Header] SI WITH (NOLOCK)
    JOIN
    (
        SELECT [Document No_],
               SUM(([Quantity] * [Qty_ per Unit of Measure])) UNITS
        FROM NAV_ETL.[dbo].[Jason Pharm$Sales Line] WITH (NOLOCK)
        GROUP BY [Document No_]
    ) SL
        ON SI.[No_] = SL.[Document No_]
WHERE SI.[Document Type] = 1
      AND [Amount Authorized] > 0
      AND [Coupon_Promotion Code] <> 'EXCHANGE'
      AND [Customer Posting Group] IN ( 'MEDIFAST', 'TSFL', 'DOCTORS', 'SPRTNTR', 'FRANCHISE' );









