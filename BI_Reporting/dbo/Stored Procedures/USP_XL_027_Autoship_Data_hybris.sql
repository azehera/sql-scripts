﻿


/*
=============================================================================================
Author:         Luc Emond
Create date: 04/01/2012
-------------------------[dbo].[USP_XL_027_Autoship_Data_hybris] -----------------------------
Sales Tracker by Month----
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[bi_reporting]           dbo.calendar                             Select      
[BI_Reporting]          DBO.XL_027_Autoship_Data                  Insert 
[BI_Reporting]          [dbo].[XL_023_MonthlySalesForecast]       Insert    

ecomm_etl_hybris		dbo.template_header						   Select  
===========================================================================================
REVISION LOG
Date           Name              Change
1/18/2016      Menkir Haile		The Select statement is removed
4/18/2018      Menkir Haile     @Date is changed to @Date2 = (Select EOMONTH(@Date1)) 
4/18/2018      Menkir Haile     Added where ststement a.cth_type_cd IN ( 'BSL','VIP')
2/21/2019		D.Smith			Modified to use Hybris Data from ecomm_etl_hybris
2/28/2019      Ronak Shah		Include templatestatus =1 in where clause
-------------------------------------------------------------------------------------------
Job: Run after Sales Cube job finished at 6:00 am
==========================================================================================
*/


CREATE procedure [dbo].[USP_XL_027_Autoship_Data_hybris] 
@ReportDate DATETIME
AS 

SET NOCOUNT ON;
--DECLARE @ReportDate DATETIME = GETDATE()

DECLARE @Date1 DateTime, @Date2 DateTime, @Yesterday DATETIME
SET @Date1 = (Select Convert(Varchar(10),@ReportDate+1,121))
--SET @Date2 = (Select LastDateOfYear from dbo.calendar where CalendarDate = @Date1+365)
SET @Date2 = (Select EOMONTH(@Date1))
SET @Yesterday = (SELECT CONVERT(VARCHAR(10), @ReportDate-1,121))

IF object_id('Tempdb..#TH') Is Not Null DROP TABLE #TH
SELECT 
template.pk as 'pk',--
item_t0.p_active as 'templatestatus',--
cast(chanl.p_uid as char(255)) as 'channel',--
template.p_user as 'user_id',  --
cast(template.p_totalprice  as decimal(30,8)) as 'order_totalprice',--
template.p_nextautoshiporderdate as 'nextautoshiporderdate'--
INTO #TH
FROM [Hybris_ETL].[mdf_prd_database].[cronjobs] as item_t0 
JOIN [Hybris_ETL].[mdf_prd_database].[orders] as template ON  item_t0.p_ordertemplate  =  template.PK  
left join [Hybris_ETL].[mdf_prd_database].[cmssite] as chanl on template.p_site = chanl.pk
--left join [mdf_prd_database].[enumerationvalues] as stat on template.p_status= stat.pk
--left join [mdf_prd_database].[enumerationvalues] as otype on template.p_ordertype = otype.pk
WHERE template.p_nextautoshiporderdate is not null
and template.p_versionid is null 
and template.p_autoshipordernumber is null 
and cast(template.p_nextautoshiporderdate as date) Between @Date1 and @Date2

--exec [dbo].[USP_XL_027_Autoship_Data_hybris] @ReportDate = '2/21/2019'

Truncate Table DBO.XL_027_Autoship_Data 
INSERT INTO DBO.XL_027_Autoship_Data (
  USA_ID
,CTH_ID
,CartStatus
,TypeName
,CalendarDate
,CalendarYear
,CalendarMonthName
,CalendarMonth
,Amount
)
SELECT
TH.user_id AS USA_ID,
TH.PK AS CTH_ID,
TH.[templatestatus] AS CartStatus,
CASE TH.channel when 'optavia' then 'TSFL' when 'medifast' then 'VIP' else 'Unknown' END AS TypeName,
CAST(TH.nextautoshiporderdate AS DATE) CalendarDate,
ca.CalendarYear,
ca.CalendarMonthName,
ca.CalendarMonth,
SUM(TH.order_totalprice) as Amount
FROM #TH TH
join [bi_reporting].dbo.calendar ca (NOLOCK) ON CAST(ca.CalendarDate AS DATE) = CAST(TH.nextautoshiporderdate AS DATE) AND TH.templatestatus = '1'
WHERE  CAST(TH.nextautoshiporderdate AS DATE) BETWEEN  @Date1 and @Date2 --'2019-02-01' AND '2019-02-28'
Group By
TH.user_id
,TH.PK
,TH.[templatestatus]
,CASE TH.channel when 'optavia' then 'TSFL' when 'medifast' then 'VIP' else 'Unknown' END,
--,case a.cth_type_cd when 'BSL' then 'TSFL' when 'VIP' then 'Medirect' else 'Unknown' END 
CAST(TH.nextautoshiporderdate AS date) ,
ca.CalendarYear,
ca.CalendarMonthName,
ca.CalendarMonth

--Select a.[USA_ID]
--      ,a.[CTH_ID]
--      ,c.CTH_STATUS_CD As CartStatus
--      ,case a.cth_type_cd when 'BSL' then 'TSFL' when 'VIP' then 'Medirect' else 'Unknown' END TypeName
--      ,a.[CTH_Next_Order_Date] as CalendarDate
--      ,ca.CalendarYear
--      ,ca.CalendarMonthName
--      ,ca.CalendarMonth
--      ,Sum(a.[CTH_TOTAL_AMT]) as Amount
--From 
--     UCART.prdmartini_STORE.[dbo].[V_CART_HEADER] c (NOLOCK)
--join 
--    UCART.prdmartini_STORE.[dbo].[V_USER_ACCOUNT] ua (NOLOCK)
--    on ua.USA_ID = c.CTH_CREATED_FOR 
--    and ua.USA_STATUS_CD = 'A'
--join
--    UCART.prdmartini_STORE.[dbo].[vw_Autoship] a (NOLOCK)
--    on a.CTH_ID = c.CTH_ID
--join [bi_reporting].dbo.calendar ca (NOLOCK)
--    on a.[CTH_Next_Order_Date] = ca.CalendarDate    
--Where  
--[CTH_Next_Order_Date] between @Date1 and @Date2 
--AND a.cth_type_cd IN ( 'BSL','VIP')----Added 4/18/2018   by   Menkir Haile
--Group By
--a.[USA_ID]
--,a.[CTH_ID]
--,c.CTH_STATUS_CD
--,case a.cth_type_cd when 'BSL' then 'TSFL' when 'VIP' then 'Medirect' else 'Unknown' END 
--,a.[CTH_Next_Order_Date] 
--,ca.CalendarYear
--,ca.CalendarMonthName
--,ca.CalendarMonth

------------------------------------------------------------------------------------------

--Select COUNT(*) as CountRows,GETDATE() as Date from DBO.XL_027_Autoship_Data 


-------ADDED LUC EMOND-----NEED ADDITONAL DATA DAILY-----

------delete today info in table [dbo].[XL_023_MonthlySalesForecast]-----
--DELETE [dbo].[XL_023_MonthlySalesForecast]
--WHERE CalendarDate >= GETDATE()-1

------insert today info----
--;with CTE as(
--SELECT 
--CalendarDate
--,Case When TypeName ='TSFL' Then SUM(amount) Else 0 End as 'TSFL'
--,Case When TypeName ='Medirect' Then SUM(amount) Else 0 End as 'MEDIFAST'
--, SUM(amount) AS Total
--FROM DBO.XL_027_Autoship_Data
--where CalendarDate =@Yesterday+1
--and CartStatus ='A'
--group by TypeName, CalendarDate)

-----add daily info into [XL_023_MonthlySalesForecast]-----
--INSERT INTO [dbo].[XL_023_MonthlySalesForecast]
--Select 
--CalendarDate
--,SUM(TSFL) as TSFL
--,SUM(MEDIFAST) as MEDIFAST
--,SUM(Total) AS Total
--,0.00 as AutoshipActual
--FROM CTE
--GROUP BY CalendarDate

--IF object_id('Tempdb..#Auto') Is Not Null DROP TABLE #Auto
------UPDATE AUTOSHIP ACTUAL SALES---
--SELECT 
--[Posting Date]
--,Sum(Amount) as [InvSalesDay]
--INTO #AUTO
--FROM [BI_SSAS_Cubes].[dbo].[FactSales]
--WHERE [Posting Date] >= @Yesterday-5 AND SalesChannel ='AUTO' and CustomerPostingGroup IN('TSFL', 'MEDIFAST')
--GROUP BY 
--[Posting Date]
--HAVING Sum([Amount]) <> 0



------UPDATE [dbo].[XL_023_MonthlySalesForecast] FOR THE TOTAL AUTOSHIP-----
--Update [dbo].[XL_023_MonthlySalesForecast]
--SET AutoShipActual = [InvSalesDay]
--From #AUTO h
--Where [dbo].[XL_023_MonthlySalesForecast].CalendarDate = h.[Posting Date]






