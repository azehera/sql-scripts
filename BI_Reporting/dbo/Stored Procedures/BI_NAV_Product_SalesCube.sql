﻿

/*
--==================================================================================
Author:         Kalpesh Patel
Create date: 01/14/2014
-----------------------------dbo.BI_NAV_Product_SalesCube-------------------
Create Daily Flash reports from Cube data
--==================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------
[BI_SSAS_Cubes]       [dbo].[FactSales]                        Select
[BI_SSAS_Cubes]       [dbo].[Snapshot$NAV_Product_SalesCube]   Insert
                        
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/
CREATE Procedure [dbo].[BI_NAV_Product_SalesCube](@DateEnd AS DATETIME)
as 


DECLARE @DateMonthBegin DATETIME
DECLARE @DateMonthEnd DATETIME
DECLARE @DateYearBegin DATETIME
DECLARE @DateYearEnd DATETIME
DECLARE @DateYesterday DATETIME
DECLARE @DatePriorYrMonthBegin DATETIME
DECLARE @DatePriorYrYearBegin DATETIME
DECLARE @DatePriorYrYesterday DATETIME
DECLARE @DaysMonth INT
DECLARE @DaysMonthElapsed INT
DECLARE @DaysMonthRemaining INT
DECLARE @DaysYear INT
DECLARE @DaysYearElapsed INT
DECLARE @DaysYearRemaining INT
DECLARE @ProcessTimeBegin DATETIME
DECLARE @ProcessTimeEnd DATETIME

SET @ProcessTimeBegin = GetDate()
SET @DateEnd =  CAST(FLOOR(CAST(@DateEnd AS float)) AS DATETIME)
SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1



DELETE FROM [Snapshot$NAV_Product_SalesCube]
WHERE (RunDate = @DateEnd)

---- Calculate Yesterday Sales,Units,Cost and Orders info.---------------

IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT @DateYesterday as ReportDate
      ,[CustomerPostingGroup]
      ,a.[ItemCategoryCode] as [Item Category Code]
      ,isnull(Sum([Amount]),0) as [Invcd. Sales Yesterday]
      ,isnull(SUM(Units),0) as [Invcd. Units Yesterday]
      ,isnull(SUM([Unit Cost (LCY)] *Units),0) as InvExtCostYesterday
      ,case when [ItemCategoryCode] ='NON-ITEM' then 0 else  isnull(COUNT(DocumentNo),0) end  as [Invcd. Orders Yesterday]
      into #A FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date]=@DateYesterday
Group by [CustomerPostingGroup]
         ,[ItemCategoryCode] 

  
---- Calculate MTD Sales,Units,Cost and Orders info.--------------- 
      
IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B    
 SELECT @DateYesterday as ReportDate
        ,[CustomerPostingGroup]
        ,[ItemCategoryCode] as [Item Category Code]
        ,isnull(Sum([Amount]),0) as [Invcd. Sales MTD]
        ,isnull(SUM(Units),0) as [Invcd. Units MTD]
        ,isnull(SUM([Unit Cost (LCY)]*Units),0) as InvExtCostMTD
        ,case when [ItemCategoryCode] ='NON-ITEM' then 0 else  isnull(COUNT(DocumentNo),0) end as [Invcd. Orders MTD]
        into #B FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date] between @DateMonthBegin and @DateYesterday 
Group by [CustomerPostingGroup]
         ,[ItemCategoryCode] 
         
---- Calculate Prior Year MTD Sales,Units,Cost and Orders info.--------------- 
  
IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C 
SELECT @DateYesterday as ReportDate
       ,[CustomerPostingGroup]
       ,[ItemCategoryCode] as [Item Category Code]
       ,isnull(Sum([Amount]),0) as [Invcd. Sales Prior Year MTD]
       into #C FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date] between @DatePriorYrMonthBegin and @DatePriorYrYesterday
Group by [CustomerPostingGroup]
         ,[ItemCategoryCode] 
         
---- Calculate YTD Sales,Units,Cost and Orders info.---------------
 
IF object_id('Tempdb..#D') Is Not Null DROP TABLE #D      
SELECT @DateYesterday as ReportDate
       ,[CustomerPostingGroup]
       ,[ItemCategoryCode] as [Item Category Code]
       ,isnull(Sum([Amount]),0) as [Invcd. Sales YTD]
       ,isnull(SUM(Units),0) as [Invcd. Units YTD]
       ,isnull(SUM([Unit Cost (LCY)] * Units),0) as InvExtCostYTD
       ,case when [ItemCategoryCode] ='NON-ITEM' then 0 else  isnull(COUNT(DocumentNo),0) end as [Invcd. Orders YTD]
       into #D FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date] between @DateYearBegin and  @DateYesterday 
Group by [CustomerPostingGroup]
         ,[ItemCategoryCode] 
  
---- Calculate Prior Year YTD Sales,Units,Cost and Orders info.--------------- 
 
IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E       
SELECT @DateYesterday as ReportDate
       ,[CustomerPostingGroup]
       ,[ItemCategoryCode] as [Item Category Code]
       ,isnull(Sum([Amount]),0) as [Invcd. Sales Prior Year YTD]
       into #E FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date] between @DatePriorYrYearBegin and  @DatePriorYrYesterday
Group by [CustomerPostingGroup]
         ,[ItemCategoryCode] 
      
---- Calculate Sales Credit for Yesterday---------------  
 
IF object_id('Tempdb..#I') Is Not Null DROP TABLE #I
SELECT @DateYesterday as ReportDate
       ,[CustomerPostingGroup]
       ,[ItemCategoryCode] as [Item Category Code]
       ,isnull(Sum([Amount]),0) as [Invcd. SalesCredit Yesterday]
       into #I FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date]=@DateYesterday and SalesType=2
Group by [CustomerPostingGroup]
         ,[ItemCategoryCode] 
 
---- Calculate Orders Credit for Yesterday---------------  
     
IF object_id('Tempdb..#J') Is Not Null DROP TABLE #J
SELECT @DateYesterday as ReportDate
       ,[CustomerPostingGroup]
       ,[ItemCategoryCode] as [Item Category Code]
       ,case when [ItemCategoryCode] ='NON-ITEM' then 0 else  isnull(COUNT(DocumentNo),0) end  as [Invcd. CrOrders Yesterday]
       into #J FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date]=@DateYesterday and SalesType=2
Group by [CustomerPostingGroup]
         ,[ItemCategoryCode] 

---- Calculate Orders Credit for MTD--------------- 
      
IF object_id('Tempdb..#K') Is Not Null DROP TABLE #K    
SELECT @DateYesterday as ReportDate
       ,[CustomerPostingGroup]
       ,[ItemCategoryCode] as [Item Category Code]
       ,case when [ItemCategoryCode] ='NON-ITEM' then 0 else  isnull(COUNT(DocumentNo),0) end as [Invcd. CrOrders MTD]
       into #K FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date] between @DateMonthBegin and @DateYesterday and SalesType=2
Group by [CustomerPostingGroup]
         ,[ItemCategoryCode]
         
---- Calculate Orders Credit for YTD--------------- 
      
IF object_id('Tempdb..#L') Is Not Null DROP TABLE #L      
SELECT @DateYesterday as ReportDate
       ,[CustomerPostingGroup]
       ,[ItemCategoryCode] as [Item Category Code]
       ,case when [ItemCategoryCode] ='NON-ITEM' then 0 else  isnull(COUNT(DocumentNo),0) end as [Invcd. CrOrders YTD]
       into #L FROM [BI_SSAS_Cubes].[dbo].[FactSales] a
where [Posting Date] between @DateYearBegin and  @DateYesterday and SalesType=2
Group by [CustomerPostingGroup]
      ,[ItemCategoryCode] 
      
---- Find all ItemCategoryCode and Customer Posting Group ---------------      
   IF object_id('Tempdb..#H') Is Not Null DROP TABLE #H     
SELECT  distinct [CustomerPostingGroup] as  [Customer Posting Group]
        ,[ItemCategoryCode] as [Item Category Code]
        into #H from [BI_SSAS_Cubes].[dbo].[FactSales]

----Insert Data into [dbo].[Snapshot$NAV_Product] for Flash----

insert into [dbo].[Snapshot$NAV_Product_SalesCube]
  Select @DateEnd as RunDate
         ,@DateYesterday as [Yesterday]
         ,@DateYearBegin as [StartDateYear] 
         ,@DateMonthBegin as [StartDateMonth] 
         ,@DatePriorYrYesterday as [YesterdayPriorYr]
         ,@DatePriorYrYearBegin as [StartDateYearPriorYr]
         ,@DatePriorYrMonthBegin as [StartDateMonthPriorYr]
         ,[Customer Posting Group] as CPG
         ,h.[Item Category Code] as [ItemCategoryCode] 
         ,Convert(DECIMAL(18,2),isnull([Invcd. Sales Yesterday],0)) as [InvSalesDay]
         ,isnull([Invcd. Units Yesterday],0) as [InvUnitsDay]
         ,(isnull([Invcd. Orders Yesterday],0)-ISnull([Invcd. CrOrders Yesterday],0)-ISnull([Invcd. CrOrders Yesterday],0)) as [InvOrdersDay]
         ,Convert(DECIMAL(18,2),isnull(InvExtCostYesterday,0)) as [InvExtCostDay]
         ,Convert(DECIMAL(18,2),isnull([Invcd. Sales MTD],0)) as [InvSalesMTD]
         ,Convert(DECIMAL(18,2),isnull([Invcd. Sales Prior Year MTD],0)) as [InvSalesPriorYrMTD]
         ,Convert(DECIMAL(18,2),isnull(([Invcd. Sales MTD]/NULLIF(@DaysMonthElapsed,0)) * @DaysMonth,0)) as [InvSalesMoProj]
         ,isnull([Invcd. Units MTD],0) as [InvUnitsMTD]
         ,(isnull([Invcd. Orders MTD],0)-ISnull([Invcd. CrOrders MTD],0)-ISnull([Invcd. CrOrders MTD],0)) as [InvOrdersMTD]
         ,Convert(DECIMAL(18,2),isnull(InvExtCostMTD,0)) as [InvExtCostMTD]
         ,Convert(DECIMAL(18,2), isnull([Invcd. Sales YTD],0)) as [InvSalesYTD]
         ,Convert(DECIMAL(18,2),isnull([Invcd. Sales Prior Year YTD],0)) as [InvSalesPriorYrYTD]
         ,Convert(DECIMAL(18,2),isnull(([Invcd. Sales YTD]/NULLIF(@DaysYearElapsed,0)) * @DaysYear,0)) as [InvSalesProjYE]
         ,isnull([Invcd. Units YTD],0) as [InvUnitsYTD]
         ,(isnull([Invcd. Orders YTD],0)-ISnull([Invcd. CrOrders YTD],0)-ISnull([Invcd. CrOrders YTD],0)) as [InvOrdersYTD]
         ,Convert(DECIMAL(18,2),isnull(InvExtCostYTD,0)) as [InvExtCostYTD]
         ,Convert(DECIMAL(18,2),isnull([Invcd. SalesCredit Yesterday],0)) as [InvCreditsDay]
         ,0 as [SortOrder]
         ,0 as [SortOrder2]
         from #H h
              left join #A a
              on h.[Customer Posting Group]=a.[CustomerPostingGroup]
              and h.[Item Category Code]=a.[Item Category Code] 
              left join #B b
              on h.[Customer Posting Group]=b.[CustomerPostingGroup]
              and h.[Item Category Code]=b.[Item Category Code] 
              left join #C c
              on h.[Customer Posting Group]=c.[CustomerPostingGroup]
              and h.[Item Category Code]=c.[Item Category Code] 
              left join #D d
			  on h.[Customer Posting Group]=d.[CustomerPostingGroup]
              and h.[Item Category Code]=d.[Item Category Code] 
              left join #E e
              on h.[Customer Posting Group]=e.[CustomerPostingGroup]
              and h.[Item Category Code]=e.[Item Category Code] 
              left join #I i
              on h.[Customer Posting Group]=i.[CustomerPostingGroup]
              and h.[Item Category Code]=i.[Item Category Code] 
              left join #J j
              on h.[Customer Posting Group]=j.[CustomerPostingGroup]
              and h.[Item Category Code]=j.[Item Category Code] 
              left join #K k
              on h.[Customer Posting Group]=k.[CustomerPostingGroup]
              and h.[Item Category Code]=k.[Item Category Code] 
              left join #L l
              on h.[Customer Posting Group]=l.[CustomerPostingGroup]
              and h.[Item Category Code]=l.[Item Category Code] 
      where ([Invcd. Sales Yesterday] is not null or
             [Invcd. Units Yesterday] is not null or 
             [Invcd. Orders Yesterday] is not null or
             [Invcd. Sales MTD] is not null or 
             [Invcd. Sales Prior Year MTD] is not null or 
             [Invcd. Units MTD] is not null or
             [Invcd. Orders MTD] is not null or  
             [Invcd. Sales YTD] is not null or
             [Invcd. Sales Prior Year YTD] is not null or
             [Invcd. Units YTD] is not null or 
             [Invcd. Orders YTD] is not null or 
             [Invcd. SalesCredit Yesterday] is not null)
    
      


UPDATE [Snapshot$NAV_Product_SalesCube]
SET SortOrder = 1000
WHERE (CPG = 'TSFL') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product_SalesCube]
SET SortOrder = 900
WHERE (CPG = 'MEDIFAST') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product_SalesCube]
SET SortOrder = 800
WHERE (CPG = 'MWCC') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product_SalesCube]
SET SortOrder = 700
WHERE (CPG = 'FRANCHISE') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product_SalesCube]
SET SortOrder = 600
WHERE (CPG = 'DOCTORS') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product_SalesCube]
SET SortOrder = -100
WHERE (CPG = 'CORPHEALTH') AND (SortOrder = 0)


/****** SECTION 9:  REPORT DURATION OF QUERY ***********************************/

SET @ProcessTimeEnd = GetDate()
PRINT 'Elapsed time: '
PRINT DATEDIFF(SS,@ProcessTimeBegin,@ProcessTimeEnd)
   
      
      

      
      

      












