﻿


/*
===============================================================================
Author:  Kalepsh Patel
Create date:  03/31/2013
-------------------------[USP_XL_064_ Encore_CAB_Kit_Purchase_Report]-----------------------------
---------------------------------
==============================================================================    
REFERENCES
Database              Table/View/UDF                             Action            
-------------------------------------------------------------------------------
[ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]				              Select 
[ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_STATUS]                     Select

===============================================================================
REVISION LOG
Date            Name                          Change
-------------------------------------------------------------------------------
                                          
===============================================================================
NOTES:
-------------------------------------------------------------------------------
===============================================================================
*/
CREATE Procedure [dbo].[USP_XL_064_Encore_CAB_Kit_Purchase_Report]
as 

SELECT 
	  A.[CUSTOMER_NUMBER] as [Coach ID]
	 ,Convert(numeric(36,0),(A.[CUSTOMER_NUMBER])) as V_LookupCoachID
	 ,A.[FIRST_NAME]as [First Name]
	 ,A.[LAST_NAME]as [Last Name]
	 ,A.[EMAIL1_ADDRESS] as [E-mail]
	 ,convert(varchar(10),[PENDING_HEALTH_COACH_DATE],121) as [Kit Purchase Date]
FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]A
	Inner join [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER_STATUS]CS
			on A.[CUSTOMER_ID] = CS.[CUSTOMER_ID]
Where convert(varchar(10),CS.[PENDING_HEALTH_COACH_DATE],121) >= GETDATE ()-8
