﻿




/*
=================================================================================================
Author:         Daniel Dagnachew
Create date:    11/11/2019

==================================================================================================    
REFERENCES
Database                       Table/View/UDF                                       Action            
-------------------------------------------------------------------------------------------------
[NAVISION_PROD].[NAVPROD]        [dbo].[Jason Pharm$Sales Invoice Header]			Select
[NAVISION_PROD].[NAVPROD]        [dbo].[Jason Pharm$Sales Line]		                Select
==================================================================================================
REVISION LOG
Date                   Name                  Change
--------------------------------------------------------------------------------------------------

==================================================================================================
NOTES:
--------------------------------------------------------------------------------------------------
==================================================================================================
*/


CREATE PROCEDURE [dbo].[USP_SSRS_Unshipped_Orders_Highqty]
AS 
BEGIN

IF OBJECT_ID('Tempdb..#ALL') IS NOT NULL
    DROP TABLE #ALL;
SELECT DISTINCT
       sl.[Document No_],
       sh.[External Document No_],
       sh.[Sell-to Customer No_],
       sl.[No_] AS 'SKU',
       sl.Description AS 'SKU Name',
       CAST([Quantity] AS DECIMAL(10, 0)) AS 'QTY',
       [Customer Posting Group],
       sh.[Ship-to County] AS 'Ship State',
       sh.[Ship-to Post Code] AS 'Ship Zip',
       sh.[Bill-to County] AS 'Billing State',
       sh.[Bill-to Post Code] AS 'Billing Zip',
       CONVERT(VARCHAR, [Order Date], 107) AS 'Order Date',
       CONVERT(VARCHAR, [sh].[Posting Date], 107) AS 'Posting Date',
       FORMAT([Order Time], 'hh:mm tt') AS 'Order Time',
       am.TotAmount 'Amount'
INTO #ALL
FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Header] sh WITH (NOLOCK)
    JOIN [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Line] sl WITH (NOLOCK)
        ON sl.[Document No_] = sh.[No_]
    JOIN
    (
        SELECT [Document No_],
               SUM(Amount) TotAmount
        FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Line] WITH (NOLOCK)
        GROUP BY [Document No_]
    ) am
        ON sh.No_ = am.[Document No_]
WHERE sh.[Customer Posting Group] NOT IN ( 'HK', 'SG' )
      AND sl.No_ <> 'HOLCRD'
      AND
      (
          Quantity > CASE
                         WHEN sl.[Unit of Measure Code] = 'CS' THEN
                             1
                         WHEN sl.[Unit of Measure Code] = 'BX' THEN
                             14
                     END
          OR am.TotAmount > 1400
      )
      AND [Type] = 2
      AND sl.[Document No_] NOT LIKE 'SRA%'
      AND [Customer Posting Group] <> 'FRANCHISE';




SELECT *
FROM #ALL A
    JOIN
    (
        SELECT [Sell-to Customer No_],
               MIN(a.[Posting Date]) FirstOrderDate
        FROM
        (
            SELECT [Sell-to Customer No_],
                   [Posting Date]
            FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Invoice Header] WITH (NOLOCK)
            UNION ALL
            SELECT [Sell-to Customer No_],
                   [Posting Date]
            FROM [NAVISION_PROD].[NAVPROD].[dbo].[Jason Pharm$Sales Header] WITH (NOLOCK)
        ) a
        GROUP BY [Sell-to Customer No_]
    ) B
        ON B.[Sell-to Customer No_] = A.[Sell-to Customer No_]
WHERE DATEDIFF(dd, B.FirstOrderDate, GETDATE()) < 120
ORDER BY A.[Order Date],
         A.[Order Time];
END 

