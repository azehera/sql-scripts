﻿


/*
=======================================================================================
Author:         Daniel Dagnachew
Create date: Date,4/17/2015
-------------------------[usp_XL_078_Rewards_Program_TSFL_New_Proc] -------------------------
---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx----------------------
=======================================================================================   
REFERENCES
Database             Table/View/UDF		             Action            
---------------------------------------------------------------------------------------
[BI_Reporting].dbo.XL_078_Rewards_Program            Select


========================================================================================                 
REVISION LOG
Date           Name              Change
----------------------------------------------------------------------------------------

========================================================================================
*/
Create PROCEDURE [dbo].[usp_XL_078_Rewards_Program_TSFL_New_Proc] 

AS
SET NOCOUNT ON;

BEGIN

select Channel
, TranDate
, BeginBalance [Beginning Balance]
, RewardEarned [Reward Earned]
, RewardUsed [Reward Used] 
, Forfeited
, ReclaimedReturn [Reclaimed Return]
, UnReclaimedReturn [UnReclaimed Return]
, ReclaimedCancel [Reclaimed Cancel]
, UnReclaimedCancel [UnReclaimed Cancel]
, Cancelled, Eliminate
, OffsetNegReward [Offset Neg Reward]
, NetChange [Net Change]
, ClosingBalance [Closing Balance]
, OrderCount [Order Count]
, OrderSubtotal [Order Subtotal]
, OrderDisc [Order Disc] 
, OrderShip [Order Ship]
, OrderShipDisc [Order Ship Disc]
, OrderTax [Order Tax]
, OrderTotal [Order Total]
from [BI_Reporting].dbo.XL_078_Rewards_Program 
where Channel = 'TSFL' 
order by TranDate

end