﻿

/*
=============================================================================================
Author:      Luc Emond
Create date: 08/13/2013
-------------------------[dbo].[USP_XL_036_MWCC_FoodOrderAvg_byLocation] -----------------------------
Provide Daily Sales Info with Average for all MWCC Centers                 
=============================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
---------------------------------------------------------------------------------------------
[BI_SSAS_Cubes]       [FactSales]                               Select
[BI_SSAS_Cubes]       [DimRegion]                               Select
[BI_SSAS_Cubes]       [DimLocation]                             Select
[Book4Time_ETL]       [B4T_product_master]                      Select
[Book4Time_ETL]       [B4T_product_class]                       Select
[BI_Reporting]        DBO.calendar                              Select 

===========================================================================================
REVISION LOG
Date           Name              Change
-------------------------------------------------------------------------------------------
3/5/2014     Kalpesh Patel    Included new group 3622634=Counseling Visits(Requested Christos)
==========================================================================================
*/


CREATE PROCEDURE [dbo].[USP_XL_036_MWCC_FoodOrderAvg_byLocation] As

Set Nocount On;
 
----DECLARE TIME PERIOD-----
Declare @StartDate datetime
Declare @EndDate Datetime
Declare @MonthStartDate Datetime
Declare @MonthEndDate Datetime
Declare @CurrentDate Datetime 
declare @3MonthDate datetime

Set @StartDate =(Select [FirstDateOfYear] from [BI_Reporting].[dbo].[calendar] where CalendarDate=CONVERT(Varchar(10),Getdate()-1,121))
Set @EndDate =(Select CalendarDate from [BI_Reporting].[dbo].[calendar] where CalendarDate=CONVERT(Varchar(10),Getdate()-1,121))
Set @MonthStartDate =(Select [FirstDateOfMonth] from [BI_Reporting].[dbo].[calendar] where CalendarDate=CONVERT(Varchar(10),Getdate()-1,121))
Set @MonthEndDate=(Select LastDateOfMonth from [BI_Reporting].[dbo].[calendar] where CalendarDate=CONVERT(Varchar(10),Getdate()-1,121))----Change CalendarDate to LastDateOfMonth  Luc 08/01/2013----
set @CurrentDate =(Select CalendarDate from [BI_Reporting].[dbo].[calendar] where CalendarDate=CONVERT(Varchar(10),Getdate()-1,121))
Set @3MonthDate=(Select CalendarDate from [BI_Reporting].[dbo].[calendar] where Calendardate=DATEADD(month, -3, Convert(Varchar(10),GETDATE(),121)))

--select @StartDate as StartDate, @EndDate as EndDate, @MonthStartDate as MonthStartDate,
--       @MonthEndDate as MonthEndDate, @CurrentDate as CurrentDate, @3MonthDate as [3MonthDate]

----SALES ACTIVITIES-----
IF object_id('Tempdb..#A') Is Not Null DROP TABLE #A
SELECT 
 A.[CustomerPostingGroup]
,A.[DocumentNo]
,Case When ROW_NUMBER() OVER(PARTITION BY A.[DocumentNo] ORDER BY A.[DocumentNo] asc) =1 then 1 else 0 end As OrderCount
,A.[ItemCode]
,B.[class_id]
,C.[class_desc]
,A.[Amount]
,A.[LineDiscount]
,A.[InvoiceDiscount]
,A.[Gross Amount]
,A.[LocationID]
,D.[LocationCode]
,Region
,A.[Posting Date]
Into #A 
From [BI_SSAS_Cubes].[dbo].[FactSales] A
JOIN [Book4Time_ETL].[dbo].[B4T_product_master] B
ON A.ItemCode = B.sku
JOIN [Book4Time_ETL].[dbo].[B4T_product_class] C
ON B.class_id = C.class_id
right JOIN [BI_SSAS_Cubes].[dbo].[DimLocation] D
ON A.LocationID = D.LocationID 
join [BI_SSAS_Cubes].dbo.DimRegion E
on a.RegionID=E.RegionID
WHERE [Posting Date] between @StartDate and @EndDate
AND [CustomerPostingGroup]='MWCC'
AND C.Class_id in('3620027', '3620043','3622634') ---"3620027=Non-Product (Tools)", "3620043 = Food","3622634=Counseling Visits"---
AND A.SalesType =1
AND A.ItemCode <>'Gratuity'
ORDER BY A.DocumentNo

------INCLUDE THE CREDIT BUT 0 FOR THE COUNT OF ORDERS------LUC EMOND 08/30/2013-----
INSERT INTO #A
SELECT 
 A.[CustomerPostingGroup]
,A.[DocumentNo]
,0 AS OrderCount
,A.[ItemCode]
,B.[class_id]
,C.[class_desc]
,A.[Amount]
,A.[LineDiscount]
,A.[InvoiceDiscount]
,A.[Gross Amount]
,A.[LocationID]
,D.[LocationCode]
,Region
,A.[Posting Date]
From [BI_SSAS_Cubes].[dbo].[FactSales] A
JOIN [Book4Time_ETL].[dbo].[B4T_product_master] B
ON A.ItemCode = B.sku
JOIN [Book4Time_ETL].[dbo].[B4T_product_class] C
ON B.class_id = C.class_id
right JOIN [BI_SSAS_Cubes].[dbo].[DimLocation] D
ON A.LocationID = D.LocationID 
join [BI_SSAS_Cubes].dbo.DimRegion E
on a.RegionID=E.RegionID
WHERE [Posting Date] between @StartDate and @EndDate
AND [CustomerPostingGroup]='MWCC'
AND C.Class_id in('3620027', '3620043','3622634') ---"3620027=Non-Product (Tools)", "3620043 = Food","3622634=Counseling Visits"---
AND A.SalesType =2
AND A.ItemCode <>'Gratuity'
ORDER BY A.DocumentNo


----YESTERDAY----
IF object_id('Tempdb..#B') Is Not Null DROP TABLE #B
Select Region,
 LocationCode
,SUM(Amount)as Amount
,SUM(OrderCount) as OrderCount
,Case When SUM(OrderCount)= 0 Then 0 else SUM(Amount)/SUM(OrderCount) end as OrdAvg  
Into #B 
From #A 
Where [Posting Date]=@CurrentDate
Group by LocationCode,Region
  
---MTD-----  
IF object_id('Tempdb..#C') Is Not Null DROP TABLE #C
Select 
Region,
 LocationCode
,SUM(Amount)as MTDAmount
,SUM(OrderCount) as MTDOrderCount
,Case When SUM(OrderCount)= 0 Then 0 else SUM(Amount)/SUM(OrderCount) end as MTDOrdAvg  
Into #C 
From #A 
Where [Posting Date]between @MonthStartDate and @MonthEndDate
Group by LocationCode,Region
  
----LAST 3 MONTHS-----  
IF object_id('Tempdb..#E') Is Not Null DROP TABLE #E
Select Region
 ,LocationCode
,SUM(Amount)as [3MTDAmount]
,SUM(OrderCount) as [3MTDOrderCount]
,Case When SUM(OrderCount)= 0 Then 0 else SUM(Amount)/SUM(OrderCount) end as [3MTDOrdAvg]  
Into #E 
From #A 
Where [Posting Date]between @3MonthDate and @MonthEndDate
Group by LocationCode,Region
 
-----YTD----- 
IF object_id('Tempdb..#D') Is Not Null DROP TABLE #D
Select  Region
 ,LocationCode,SUM(Amount)as YTDAmount
,SUM(OrderCount) as YTDOrderCount
,Case When SUM(OrderCount)= 0 Then 0 else SUM(Amount)/SUM(OrderCount) end as YTDOrdAvg  
Into #D 
From #A 
Where [Posting Date]between @StartDate and  @EndDate
Group by LocationCode,Region
  
----SELECT FOR REPORT-----  
Select
 d.Region
,d.LocationCode
,isnull(Amount,0) as [Yest Food$]
,isnull(OrderCount,0) as [Yest FoodOrders]
--,isnull(OrdAvg,0)  as [Yest AvgFoodOrder$]
,isnull(MTDAmount,0)  as [MTD Food$]
,isnull(MTDOrderCount,0) as [MTD #FoodOrders]
--,isnull(MTDOrdAvg,0) as [MTD AvgFoodOrder$]
,isnull([3MTDAmount],0)  as [Last3M Food$]
,isnull([3MTDOrderCount],0) as [Last3M #FoodOrders]
--,isnull([3MTDOrdAvg],0) as [Last3M AvgFoodOrder$]
,isnull(YTDAmount,0)  as [YTD Food$]
,isnull(YTDOrderCount,0) as [YTD #FoodOrders]
--,isnull(YTDOrdAvg,0) as [YTD AvgFoodOrder$]
,Convert(Varchar(10),GETDATE()-1,121) as ReportDate
from #D d
left join #C c
on d.LocationCode=c.LocationCode
left join #b b
on d.LocationCode=b.LocationCode
left join #E e
on d.LocationCode=e.LocationCode
  
  

