﻿CREATE TABLE [dbo].[Jason Pharm$Customer Price Group] (
    [timestamp]                    ROWVERSION    NULL,
    [Code]                         NVARCHAR (10) NOT NULL,
    [Price Includes VAT]           TINYINT       NOT NULL,
    [Allow Invoice Disc_]          TINYINT       NOT NULL,
    [VAT Bus_ Posting Gr_ (Price)] NVARCHAR (10) NOT NULL,
    [Description]                  NVARCHAR (50) NULL,
    [Allow Line Disc_]             TINYINT       NOT NULL
) ON [Data Filegroup 1];

