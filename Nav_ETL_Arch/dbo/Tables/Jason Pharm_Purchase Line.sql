﻿CREATE TABLE [dbo].[Jason Pharm$Purchase Line] (
    [Document Type]                  INT              NOT NULL,
    [Document No_]                   NVARCHAR (20)    NOT NULL,
    [Line No_]                       INT              NOT NULL,
    [Buy-from Vendor No_]            NVARCHAR (20)    NOT NULL,
    [Type]                           INT              NOT NULL,
    [No_]                            NVARCHAR (20)    NOT NULL,
    [Location Code]                  NVARCHAR (10)    NOT NULL,
    [Posting Group]                  NVARCHAR (10)    NOT NULL,
    [Expected Receipt Date]          DATETIME         NOT NULL,
    [Description]                    NVARCHAR (50)    NOT NULL,
    [Description 2]                  NVARCHAR (50)    NOT NULL,
    [Unit of Measure]                NVARCHAR (10)    NOT NULL,
    [Quantity]                       DECIMAL (38, 20) NOT NULL,
    [Outstanding Quantity]           DECIMAL (38, 20) NOT NULL,
    [Qty_ to Invoice]                DECIMAL (38, 20) NOT NULL,
    [Qty_ to Receive]                DECIMAL (38, 20) NOT NULL,
    [Direct Unit Cost]               DECIMAL (38, 20) NOT NULL,
    [Unit Cost (LCY)]                DECIMAL (38, 20) NOT NULL,
    [VAT _]                          DECIMAL (38, 20) NOT NULL,
    [Line Discount _]                DECIMAL (38, 20) NOT NULL,
    [Line Discount Amount]           DECIMAL (38, 20) NOT NULL,
    [Amount]                         DECIMAL (38, 20) NOT NULL,
    [Amount Including VAT]           DECIMAL (38, 20) NOT NULL,
    [Unit Price (LCY)]               DECIMAL (38, 20) NOT NULL,
    [Allow Invoice Disc_]            TINYINT          NOT NULL,
    [Gross Weight]                   DECIMAL (38, 20) NOT NULL,
    [Net Weight]                     DECIMAL (38, 20) NOT NULL,
    [Units per Parcel]               DECIMAL (38, 20) NOT NULL,
    [Unit Volume]                    DECIMAL (38, 20) NOT NULL,
    [Appl_-to Item Entry]            INT              NOT NULL,
    [Shortcut Dimension 1 Code]      NVARCHAR (20)    NOT NULL,
    [Shortcut Dimension 2 Code]      NVARCHAR (20)    NOT NULL,
    [Job No_]                        NVARCHAR (20)    NOT NULL,
    [Indirect Cost _]                DECIMAL (38, 20) NOT NULL,
    [Outstanding Amount]             DECIMAL (38, 20) NOT NULL,
    [Qty_ Rcd_ Not Invoiced]         DECIMAL (38, 20) NOT NULL,
    [Amt_ Rcd_ Not Invoiced]         DECIMAL (38, 20) NOT NULL,
    [Quantity Received]              DECIMAL (38, 20) NOT NULL,
    [Quantity Invoiced]              DECIMAL (38, 20) NOT NULL,
    [Receipt No_]                    NVARCHAR (20)    NOT NULL,
    [Receipt Line No_]               INT              NOT NULL,
    [Profit _]                       DECIMAL (38, 20) NOT NULL,
    [Pay-to Vendor No_]              NVARCHAR (20)    NOT NULL,
    [Inv_ Discount Amount]           DECIMAL (38, 20) NOT NULL,
    [Vendor Item No_]                NVARCHAR (20)    NOT NULL,
    [Sales Order No_]                NVARCHAR (20)    NOT NULL,
    [Sales Order Line No_]           INT              NOT NULL,
    [Drop Shipment]                  TINYINT          NOT NULL,
    [Gen_ Bus_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Gen_ Prod_ Posting Group]       NVARCHAR (10)    NOT NULL,
    [VAT Calculation Type]           INT              NOT NULL,
    [Transaction Type]               NVARCHAR (10)    NOT NULL,
    [Transport Method]               NVARCHAR (10)    NOT NULL,
    [Attached to Line No_]           INT              NOT NULL,
    [Entry Point]                    NVARCHAR (10)    NOT NULL,
    [Area]                           NVARCHAR (10)    NOT NULL,
    [Transaction Specification]      NVARCHAR (10)    NOT NULL,
    [Tax Area Code]                  NVARCHAR (20)    NOT NULL,
    [Tax Liable]                     TINYINT          NOT NULL,
    [Tax Group Code]                 NVARCHAR (10)    NOT NULL,
    [Use Tax]                        TINYINT          NOT NULL,
    [VAT Bus_ Posting Group]         NVARCHAR (10)    NOT NULL,
    [VAT Prod_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Currency Code]                  NVARCHAR (10)    NOT NULL,
    [Outstanding Amount (LCY)]       DECIMAL (38, 20) NOT NULL,
    [Amt_ Rcd_ Not Invoiced (LCY)]   DECIMAL (38, 20) NOT NULL,
    [Blanket Order No_]              NVARCHAR (20)    NOT NULL,
    [Blanket Order Line No_]         INT              NOT NULL,
    [VAT Base Amount]                DECIMAL (38, 20) NOT NULL,
    [Unit Cost]                      DECIMAL (38, 20) NOT NULL,
    [System-Created Entry]           TINYINT          NOT NULL,
    [Line Amount]                    DECIMAL (38, 20) NOT NULL,
    [VAT Difference]                 DECIMAL (38, 20) NOT NULL,
    [Inv_ Disc_ Amount to Invoice]   DECIMAL (38, 20) NOT NULL,
    [VAT Identifier]                 NVARCHAR (10)    NOT NULL,
    [IC Partner Ref_ Type]           INT              NOT NULL,
    [IC Partner Reference]           NVARCHAR (20)    NOT NULL,
    [Prod_ Order No_]                NVARCHAR (20)    NOT NULL,
    [Variant Code]                   NVARCHAR (10)    NOT NULL,
    [Bin Code]                       NVARCHAR (20)    NOT NULL,
    [Qty_ per Unit of Measure]       DECIMAL (38, 20) NOT NULL,
    [Unit of Measure Code]           NVARCHAR (10)    NOT NULL,
    [Quantity (Base)]                DECIMAL (38, 20) NOT NULL,
    [Outstanding Qty_ (Base)]        DECIMAL (38, 20) NOT NULL,
    [Qty_ to Invoice (Base)]         DECIMAL (38, 20) NOT NULL,
    [Qty_ to Receive (Base)]         DECIMAL (38, 20) NOT NULL,
    [Qty_ Rcd_ Not Invoiced (Base)]  DECIMAL (38, 20) NOT NULL,
    [Qty_ Received (Base)]           DECIMAL (38, 20) NOT NULL,
    [Qty_ Invoiced (Base)]           DECIMAL (38, 20) NOT NULL,
    [FA Posting Date]                DATETIME         NOT NULL,
    [FA Posting Type]                INT              NOT NULL,
    [Depreciation Book Code]         NVARCHAR (10)    NOT NULL,
    [Salvage Value]                  DECIMAL (38, 20) NOT NULL,
    [Depr_ until FA Posting Date]    TINYINT          NOT NULL,
    [Depr_ Acquisition Cost]         TINYINT          NOT NULL,
    [Maintenance Code]               NVARCHAR (10)    NOT NULL,
    [Insurance No_]                  NVARCHAR (20)    NOT NULL,
    [Budgeted FA No_]                NVARCHAR (20)    NOT NULL,
    [Duplicate in Depreciation Book] NVARCHAR (10)    NOT NULL,
    [Use Duplication List]           TINYINT          NOT NULL,
    [Responsibility Center]          NVARCHAR (10)    NOT NULL,
    [Cross-Reference No_]            NVARCHAR (20)    NOT NULL,
    [Unit of Measure (Cross Ref_)]   NVARCHAR (10)    NOT NULL,
    [Cross-Reference Type]           INT              NOT NULL,
    [Cross-Reference Type No_]       NVARCHAR (30)    NOT NULL,
    [Item Category Code]             NVARCHAR (10)    NOT NULL,
    [Nonstock]                       TINYINT          NOT NULL,
    [Purchasing Code]                NVARCHAR (10)    NOT NULL,
    [Product Group Code]             NVARCHAR (10)    NOT NULL,
    [Special Order]                  TINYINT          NOT NULL,
    [Special Order Sales No_]        NVARCHAR (20)    NOT NULL,
    [Special Order Sales Line No_]   INT              NOT NULL,
    [Completely Received]            TINYINT          NOT NULL,
    [Requested Receipt Date]         DATETIME         NOT NULL,
    [Promised Receipt Date]          DATETIME         NOT NULL,
    [Lead Time Calculation]          VARCHAR (32)     NULL,
    [Inbound Whse_ Handling Time]    VARCHAR (32)     NULL,
    [Planned Receipt Date]           DATETIME         NOT NULL,
    [Order Date]                     DATETIME         NOT NULL,
    [Allow Item Charge Assignment]   TINYINT          NOT NULL,
    [Return Qty_ to Ship]            DECIMAL (38, 20) NOT NULL,
    [Return Qty_ to Ship (Base)]     DECIMAL (38, 20) NOT NULL,
    [Return Qty_ Shipped Not Invd_]  DECIMAL (38, 20) NOT NULL,
    [Ret_ Qty_ Shpd Not Invd_(Base)] DECIMAL (38, 20) NOT NULL,
    [Return Shpd_ Not Invd_]         DECIMAL (38, 20) NOT NULL,
    [Return Shpd_ Not Invd_ (LCY)]   DECIMAL (38, 20) NOT NULL,
    [Return Qty_ Shipped]            DECIMAL (38, 20) NOT NULL,
    [Return Qty_ Shipped (Base)]     DECIMAL (38, 20) NOT NULL,
    [Return Shipment No_]            NVARCHAR (20)    NOT NULL,
    [Return Shipment Line No_]       INT              NOT NULL,
    [Return Reason Code]             NVARCHAR (10)    NOT NULL,
    [Tax To Be Expensed]             DECIMAL (38, 20) NOT NULL,
    [IRS 1099 Liable]                TINYINT          NOT NULL,
    [Delivery Tolerance Code]        NVARCHAR (10)    NOT NULL,
    [Routing No_]                    NVARCHAR (20)    NOT NULL,
    [Operation No_]                  NVARCHAR (10)    NOT NULL,
    [Work Center No_]                NVARCHAR (20)    NOT NULL,
    [Finished]                       TINYINT          NOT NULL,
    [Prod_ Order Line No_]           INT              NOT NULL,
    [Overhead Rate]                  DECIMAL (38, 20) NOT NULL,
    [MPS Order]                      TINYINT          NOT NULL,
    [Planning Flexibility]           INT              NOT NULL,
    [Safety Lead Time]               VARCHAR (32)     NULL,
    [Routing Reference No_]          INT              NOT NULL,
    [Prepayment _]                   DECIMAL (38, 20) NOT NULL,
    [Prepmt_ Line Amount]            DECIMAL (38, 20) NOT NULL,
    [Prepmt_ Amt_ Inv_]              DECIMAL (38, 20) NOT NULL,
    [Prepmt_ Amt_ Incl_ VAT]         DECIMAL (38, 20) NOT NULL,
    [Prepayment Amount]              DECIMAL (38, 20) NOT NULL,
    [Prepmt_ VAT Base Amt_]          DECIMAL (38, 20) NOT NULL,
    [Prepayment VAT _]               DECIMAL (38, 20) NOT NULL,
    [Prepmt_ VAT Calc_ Type]         INT              NOT NULL,
    [Prepayment VAT Identifier]      NVARCHAR (10)    NOT NULL,
    [Prepayment Tax Area Code]       NVARCHAR (20)    NOT NULL,
    [Prepayment Tax Liable]          TINYINT          NOT NULL,
    [Prepayment Tax Group Code]      NVARCHAR (10)    NOT NULL,
    [Prepmt Amt to Deduct]           DECIMAL (38, 20) NOT NULL,
    [Prepmt Amt Deducted]            DECIMAL (38, 20) NOT NULL,
    [Prepayment Line]                TINYINT          NOT NULL,
    [Prepmt_ Amount Inv_ Incl_ VAT]  DECIMAL (38, 20) NOT NULL,
    [Prepmt_ Amount Inv_ (LCY)]      DECIMAL (38, 20) NOT NULL,
    [IC Partner Code]                NVARCHAR (20)    NOT NULL,
    [Prepayment VAT Difference]      DECIMAL (38, 20) NOT NULL,
    [Prepmt VAT Diff_ to Deduct]     DECIMAL (38, 20) NOT NULL,
    [Prepmt VAT Diff_ Deducted]      DECIMAL (38, 20) NOT NULL,
    [Job Task No_]                   NVARCHAR (20)    NOT NULL,
    [Job Line Type]                  INT              NOT NULL,
    [Job Unit Price]                 DECIMAL (38, 20) NOT NULL,
    [Job Total Price]                DECIMAL (38, 20) NOT NULL,
    [Job Line Amount]                DECIMAL (38, 20) NOT NULL,
    [Job Line Discount Amount]       DECIMAL (38, 20) NOT NULL,
    [Job Line Discount _]            DECIMAL (38, 20) NOT NULL,
    [Job Unit Price (LCY)]           DECIMAL (38, 20) NOT NULL,
    [Job Total Price (LCY)]          DECIMAL (38, 20) NOT NULL,
    [Job Line Amount (LCY)]          DECIMAL (38, 20) NOT NULL,
    [Job Line Disc_ Amount (LCY)]    DECIMAL (38, 20) NOT NULL,
    [Job Currency Factor]            DECIMAL (38, 20) NOT NULL,
    [Job Currency Code]              NVARCHAR (20)    NOT NULL,
    [Provincial Tax Area Code]       NVARCHAR (20)    NOT NULL,
    [Select for Receipt]             TINYINT          NOT NULL,
    [Shipping Agent Code]            NVARCHAR (10)    NOT NULL,
    [Shipping Agent Service Code]    NVARCHAR (10)    NOT NULL,
    [Shipping Time]                  VARCHAR (32)     NULL,
    [Recalculate Invoice Disc_]      TINYINT          NOT NULL,
    [Prepmt_ VAT Amount Inv_ (LCY)]  DECIMAL (38, 20) NOT NULL,
    [Outstanding Amt_ Ex_ VAT (LCY)] DECIMAL (38, 20) NOT NULL,
    [A_ Rcd_ Not Inv_ Ex_ VAT (LCY)] DECIMAL (38, 20) NOT NULL,
    [Dimension Set ID]               INT              NOT NULL,
    [Job Planning Line No_]          INT              NOT NULL,
    [Job Remaining Qty_]             DECIMAL (38, 20) NOT NULL,
    [Job Remaining Qty_ (Base)]      DECIMAL (38, 20) NOT NULL,
    [Deferral Code]                  NVARCHAR (10)    NOT NULL,
    [Returns Deferral Start Date]    DATETIME         NOT NULL,
    [GST_HST]                        INT              NOT NULL,
    [EDI Unit Cost]                  DECIMAL (38, 20) NOT NULL,
    [EDI Cost Discrepancy]           TINYINT          NOT NULL,
    [EDI Segment Group]              INT              NOT NULL,
    [Shipping Charge]                TINYINT          NOT NULL,
    [Over Receive]                   TINYINT          NOT NULL,
    [Over Receive Verified]          TINYINT          NOT NULL,
    [Ship Date]                      DATETIME         NOT NULL,
    [Ready Date]                     DATETIME         NOT NULL,
    [Product Brand]                  NVARCHAR (10)    NOT NULL,
    CONSTRAINT [Jason Pharm$Purchase Line$0] PRIMARY KEY CLUSTERED ([Document Type] ASC, [Document No_] ASC, [Line No_] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

