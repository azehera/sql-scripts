﻿CREATE TABLE [dbo].[Jason Pharm$Item Category] (
    [timestamp]                     ROWVERSION    NULL,
    [Code]                          NVARCHAR (10) NOT NULL,
    [Description]                   NVARCHAR (50) NULL,
    [Def_ Gen_ Prod_ Posting Group] NVARCHAR (10) NOT NULL,
    [Def_ Inventory Posting Group]  NVARCHAR (10) NOT NULL,
    [Def_ Tax Group Code]           NVARCHAR (10) NOT NULL,
    [Def_ Costing Method]           INT           NOT NULL,
    [Def_ VAT Prod_ Posting Group]  NVARCHAR (10) NOT NULL
) ON [Data Filegroup 1];

