﻿CREATE TABLE [dbo].[Currency Exchange Rate_old] (
    [CurExhRateID]                   INT              IDENTITY (1, 1) NOT NULL,
    [Date]                           DATE             NULL,
    [Currency Code]                  NVARCHAR (10)    NULL,
    [Starting Date]                  DATETIME         NULL,
    [Exchange Rate Amount]           NUMERIC (38, 20) NULL,
    [Adjustment Exch_ Rate Amount]   NUMERIC (38, 20) NULL,
    [Relational Currency Code]       NVARCHAR (10)    NULL,
    [Relational Exch_ Rate Amount]   NUMERIC (38, 20) NULL,
    [Fix Exchange Rate Amount]       INT              NULL,
    [Relational Adjmt Exch Rate Amt] NUMERIC (38, 20) NULL
);

