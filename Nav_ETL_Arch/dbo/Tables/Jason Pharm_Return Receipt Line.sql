﻿CREATE TABLE [dbo].[Jason Pharm$Return Receipt Line] (
    [Document No_]                   NVARCHAR (20)    NOT NULL,
    [Line No_]                       INT              NOT NULL,
    [Sell-to Customer No_]           NVARCHAR (20)    NOT NULL,
    [Type]                           INT              NOT NULL,
    [No_]                            NVARCHAR (20)    NOT NULL,
    [Location Code]                  NVARCHAR (10)    NOT NULL,
    [Posting Group]                  NVARCHAR (10)    NOT NULL,
    [Shipment Date]                  DATETIME         NOT NULL,
    [Description]                    NVARCHAR (50)    NOT NULL,
    [Description 2]                  NVARCHAR (50)    NOT NULL,
    [Unit of Measure]                NVARCHAR (10)    NOT NULL,
    [Quantity]                       DECIMAL (38, 20) NOT NULL,
    [Unit Price]                     DECIMAL (38, 20) NOT NULL,
    [Unit Cost (LCY)]                DECIMAL (38, 20) NOT NULL,
    [VAT %]                          DECIMAL (38, 20) NOT NULL,
    [Line Discount %]                DECIMAL (38, 20) NOT NULL,
    [Allow Invoice Disc_]            TINYINT          NOT NULL,
    [Gross Weight]                   DECIMAL (38, 20) NOT NULL,
    [Net Weight]                     DECIMAL (38, 20) NOT NULL,
    [Units per Parcel]               DECIMAL (38, 20) NOT NULL,
    [Unit Volume]                    DECIMAL (38, 20) NOT NULL,
    [Appl_-to Item Entry]            INT              NOT NULL,
    [Item Rcpt_ Entry No_]           INT              NOT NULL,
    [Shortcut Dimension 1 Code]      NVARCHAR (20)    NOT NULL,
    [Shortcut Dimension 2 Code]      NVARCHAR (20)    NOT NULL,
    [Customer Price Group]           NVARCHAR (10)    NOT NULL,
    [Job No_]                        NVARCHAR (20)    NOT NULL,
    [Work Type Code]                 NVARCHAR (10)    NOT NULL,
    [Quantity Invoiced]              DECIMAL (38, 20) NOT NULL,
    [Bill-to Customer No_]           NVARCHAR (20)    NOT NULL,
    [Gen_ Bus_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Gen_ Prod_ Posting Group]       NVARCHAR (10)    NOT NULL,
    [VAT Calculation Type]           INT              NOT NULL,
    [Transaction Type]               NVARCHAR (10)    NOT NULL,
    [Transport Method]               NVARCHAR (10)    NOT NULL,
    [Attached to Line No_]           INT              NOT NULL,
    [Exit Point]                     NVARCHAR (10)    NOT NULL,
    [Area]                           NVARCHAR (10)    NOT NULL,
    [Transaction Specification]      NVARCHAR (10)    NOT NULL,
    [Tax Area Code]                  NVARCHAR (20)    NOT NULL,
    [Tax Liable]                     TINYINT          NOT NULL,
    [Tax Group Code]                 NVARCHAR (10)    NOT NULL,
    [VAT Bus_ Posting Group]         NVARCHAR (10)    NOT NULL,
    [VAT Prod_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Blanket Order No_]              NVARCHAR (20)    NOT NULL,
    [Blanket Order Line No_]         INT              NOT NULL,
    [VAT Base Amount]                DECIMAL (38, 20) NOT NULL,
    [Unit Cost]                      DECIMAL (38, 20) NOT NULL,
    [Variant Code]                   NVARCHAR (10)    NOT NULL,
    [Bin Code]                       NVARCHAR (20)    NOT NULL,
    [Qty_ per Unit of Measure]       DECIMAL (38, 20) NOT NULL,
    [Unit of Measure Code]           NVARCHAR (10)    NOT NULL,
    [Quantity (Base)]                DECIMAL (38, 20) NOT NULL,
    [Qty_ Invoiced (Base)]           DECIMAL (38, 20) NOT NULL,
    [FA Posting Date]                DATETIME         NOT NULL,
    [Depreciation Book Code]         NVARCHAR (10)    NOT NULL,
    [Depr_ until FA Posting Date]    TINYINT          NOT NULL,
    [Duplicate in Depreciation Book] NVARCHAR (10)    NOT NULL,
    [Use Duplication List]           TINYINT          NOT NULL,
    [Responsibility Center]          NVARCHAR (10)    NOT NULL,
    [Cross-Reference No_]            NVARCHAR (20)    NOT NULL,
    [Unit of Measure (Cross Ref_)]   NVARCHAR (10)    NOT NULL,
    [Cross-Reference Type]           INT              NOT NULL,
    [Cross-Reference Type No_]       NVARCHAR (30)    NOT NULL,
    [Item Category Code]             NVARCHAR (10)    NOT NULL,
    [Nonstock]                       TINYINT          NOT NULL,
    [Purchasing Code]                NVARCHAR (10)    NOT NULL,
    [Product Group Code]             NVARCHAR (10)    NOT NULL,
    [Return Qty_ Rcd_ Not Invd_]     DECIMAL (38, 20) NOT NULL,
    [Appl_-from Item Entry]          INT              NOT NULL,
    [Item Charge Base Amount]        DECIMAL (38, 20) NOT NULL,
    [Correction]                     TINYINT          NOT NULL,
    [Return Order No_]               NVARCHAR (20)    NOT NULL,
    [Return Order Line No_]          INT              NOT NULL,
    [Return Reason Code]             NVARCHAR (10)    NOT NULL,
    [Allow Line Disc_]               TINYINT          NOT NULL,
    [Customer Disc_ Group]           NVARCHAR (20)    NULL,
    [Package Tracking No_]           NVARCHAR (30)    NOT NULL,
    [Manufacturer Part No_]          NVARCHAR (20)    NOT NULL,
    [Volume]                         DECIMAL (38, 20) NOT NULL,
    [Commission Volume]              DECIMAL (38, 20) NOT NULL,
    [Volume Discount]                DECIMAL (38, 20) NOT NULL,
    [Returnable Container]           TINYINT          NOT NULL,
    [Posting Date]                   DATETIME         NOT NULL,
    [Dimension Set ID]               INT              NULL,
    [EDI Item Cross Ref_]            NVARCHAR (40)    NULL,
    [EDI Unit of Measure]            NVARCHAR (10)    NULL,
    [EDI Unit Price]                 DECIMAL (38, 20) NULL,
    [EDI Price Discrepancy]          TINYINT          NULL,
    [EDI Segment Group]              INT              NULL,
    [EDI Original Qty_]              DECIMAL (38, 20) NULL,
    [EDI Code]                       NVARCHAR (20)    NULL,
    [EDI ID]                         NVARCHAR (20)    NULL,
    [EDI Variant Code]               NVARCHAR (40)    NULL,
    [Over Receive]                   TINYINT          NULL,
    [Over Receive Verified]          TINYINT          NULL,
    CONSTRAINT [PK_Jason Pharm$Return Receipt Line] PRIMARY KEY CLUSTERED ([Document No_] ASC, [Line No_] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$Return Receipt Line_idx2]
    ON [dbo].[Jason Pharm$Return Receipt Line]([Posting Date] ASC) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE STATISTICS [Blanket Order Line No_]
    ON [dbo].[Jason Pharm$Return Receipt Line]([Blanket Order Line No_]);


GO
CREATE STATISTICS [Line No_]
    ON [dbo].[Jason Pharm$Return Receipt Line]([Line No_]);


GO
CREATE STATISTICS [Return Order Line No_]
    ON [dbo].[Jason Pharm$Return Receipt Line]([Return Order Line No_]);

