﻿CREATE TABLE [dbo].[Jason Pharm$Inventory Posting Group] (
    [timestamp]       ROWVERSION    NULL,
    [Code]            NVARCHAR (10) NOT NULL,
    [Description]     NVARCHAR (50) NOT NULL,
    [Origin Tracking] TINYINT       NOT NULL,
    [Food Product]    TINYINT       NULL
) ON [Data Filegroup 1];

