﻿CREATE TABLE [dbo].[Snapshot$NAV_OriginTracking_FP] (
    [EntryNo]              INT              IDENTITY (1, 1) NOT NULL,
    [RunDate]              DATETIME         NOT NULL,
    [Yesterday]            DATETIME         NOT NULL,
    [Location Code]        VARCHAR (10)     NOT NULL,
    [Item No_]             VARCHAR (20)     NOT NULL,
    [Description]          VARCHAR (50)     NOT NULL,
    [Unit of Measure Code] VARCHAR (10)     NOT NULL,
    [Document No_]         VARCHAR (20)     NOT NULL,
    [Expiration Date]      DATETIME         NOT NULL,
    [Quantity (Base)]      NUMERIC (38, 20) NOT NULL,
    CONSTRAINT [PK_Snapshot$NAV_OriginTracking_FP] PRIMARY KEY CLUSTERED ([EntryNo] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Snapshot$NAV_OriginTracking_FP_idx1]
    ON [dbo].[Snapshot$NAV_OriginTracking_FP]([RunDate] ASC)
    INCLUDE([EntryNo]) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];

