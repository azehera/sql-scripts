﻿CREATE TABLE [dbo].[Jason Pharm$Product Group] (
    [timestamp]            ROWVERSION    NULL,
    [Item Category Code]   NVARCHAR (10) NOT NULL,
    [Code]                 NVARCHAR (10) NOT NULL,
    [Description]          NVARCHAR (50) NULL,
    [Warehouse Class Code] NVARCHAR (10) NOT NULL
) ON [Data Filegroup 1];

