﻿CREATE TABLE [dbo].[Jason Pharm$Sales Cr_Memo Header] (
    [No_]                            NVARCHAR (20)    NOT NULL,
    [Sell-to Customer No_]           NVARCHAR (20)    NOT NULL,
    [Bill-to Customer No_]           NVARCHAR (20)    NOT NULL,
    [Bill-to Name]                   NVARCHAR (50)    NOT NULL,
    [Bill-to Name 2]                 NVARCHAR (50)    NOT NULL,
    [Bill-to Address]                NVARCHAR (50)    NOT NULL,
    [Bill-to Address 2]              NVARCHAR (50)    NOT NULL,
    [Bill-to City]                   NVARCHAR (30)    NOT NULL,
    [Bill-to Contact]                NVARCHAR (50)    NOT NULL,
    [Your Reference]                 NVARCHAR (35)    NULL,
    [Ship-to Code]                   NVARCHAR (10)    NOT NULL,
    [Ship-to Name]                   NVARCHAR (50)    NOT NULL,
    [Ship-to Name 2]                 NVARCHAR (50)    NOT NULL,
    [Ship-to Address]                NVARCHAR (50)    NOT NULL,
    [Ship-to Address 2]              NVARCHAR (50)    NOT NULL,
    [Ship-to City]                   NVARCHAR (30)    NOT NULL,
    [Ship-to Contact]                NVARCHAR (50)    NOT NULL,
    [Posting Date]                   DATETIME         NOT NULL,
    [Shipment Date]                  DATETIME         NOT NULL,
    [Posting Description]            NVARCHAR (50)    NOT NULL,
    [Payment Terms Code]             NVARCHAR (10)    NOT NULL,
    [Due Date]                       DATETIME         NOT NULL,
    [Payment Discount %]             DECIMAL (38, 20) NOT NULL,
    [Pmt_ Discount Date]             DATETIME         NOT NULL,
    [Shipment Method Code]           NVARCHAR (10)    NOT NULL,
    [Location Code]                  NVARCHAR (10)    NOT NULL,
    [Shortcut Dimension 1 Code]      NVARCHAR (20)    NOT NULL,
    [Shortcut Dimension 2 Code]      NVARCHAR (20)    NOT NULL,
    [Customer Posting Group]         NVARCHAR (10)    NOT NULL,
    [Currency Code]                  NVARCHAR (10)    NOT NULL,
    [Currency Factor]                DECIMAL (38, 20) NOT NULL,
    [Customer Price Group]           NVARCHAR (10)    NOT NULL,
    [Prices Including VAT]           TINYINT          NOT NULL,
    [Invoice Disc_ Code]             NVARCHAR (20)    NOT NULL,
    [Customer Disc_ Group]           NVARCHAR (20)    NULL,
    [Language Code]                  NVARCHAR (10)    NOT NULL,
    [Salesperson Code]               NVARCHAR (10)    NOT NULL,
    [No_ Printed]                    INT              NOT NULL,
    [On Hold]                        NVARCHAR (3)     NOT NULL,
    [Applies-to Doc_ Type]           INT              NOT NULL,
    [Applies-to Doc_ No_]            NVARCHAR (20)    NOT NULL,
    [Bal_ Account No_]               NVARCHAR (20)    NOT NULL,
    [VAT Registration No_]           NVARCHAR (20)    NOT NULL,
    [Reason Code]                    NVARCHAR (10)    NOT NULL,
    [Gen_ Bus_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [EU 3-Party Trade]               TINYINT          NOT NULL,
    [Transaction Type]               NVARCHAR (10)    NOT NULL,
    [Transport Method]               NVARCHAR (10)    NOT NULL,
    [VAT Country_Region Code]        NVARCHAR (10)    NOT NULL,
    [Sell-to Customer Name]          NVARCHAR (50)    NOT NULL,
    [Sell-to Customer Name 2]        NVARCHAR (50)    NOT NULL,
    [Sell-to Address]                NVARCHAR (50)    NOT NULL,
    [Sell-to Address 2]              NVARCHAR (50)    NOT NULL,
    [Sell-to City]                   NVARCHAR (30)    NOT NULL,
    [Sell-to Contact]                NVARCHAR (50)    NOT NULL,
    [Bill-to Post Code]              NVARCHAR (20)    NOT NULL,
    [Bill-to County]                 NVARCHAR (30)    NOT NULL,
    [Bill-to Country_Region Code]    NVARCHAR (10)    NOT NULL,
    [Sell-to Post Code]              NVARCHAR (20)    NOT NULL,
    [Sell-to County]                 NVARCHAR (30)    NOT NULL,
    [Sell-to Country_Region Code]    NVARCHAR (10)    NOT NULL,
    [Ship-to Post Code]              NVARCHAR (20)    NOT NULL,
    [Ship-to County]                 NVARCHAR (30)    NOT NULL,
    [Ship-to Country_Region Code]    NVARCHAR (10)    NOT NULL,
    [Bal_ Account Type]              INT              NOT NULL,
    [Exit Point]                     NVARCHAR (10)    NOT NULL,
    [Correction]                     TINYINT          NOT NULL,
    [Document Date]                  DATETIME         NOT NULL,
    [External Document No_]          NVARCHAR (35)    NULL,
    [Area]                           NVARCHAR (10)    NOT NULL,
    [Transaction Specification]      NVARCHAR (10)    NOT NULL,
    [Payment Method Code]            NVARCHAR (10)    NOT NULL,
    [Pre-Assigned No_ Series]        NVARCHAR (10)    NOT NULL,
    [No_ Series]                     NVARCHAR (10)    NOT NULL,
    [Pre-Assigned No_]               NVARCHAR (20)    NOT NULL,
    [User ID]                        NVARCHAR (50)    NULL,
    [Source Code]                    NVARCHAR (10)    NOT NULL,
    [Tax Area Code]                  NVARCHAR (20)    NOT NULL,
    [Tax Liable]                     TINYINT          NOT NULL,
    [VAT Bus_ Posting Group]         NVARCHAR (10)    NOT NULL,
    [VAT Base Discount %]            DECIMAL (38, 20) NOT NULL,
    [Campaign No_]                   NVARCHAR (20)    NOT NULL,
    [Sell-to Contact No_]            NVARCHAR (20)    NOT NULL,
    [Bill-to Contact No_]            NVARCHAR (20)    NOT NULL,
    [Responsibility Center]          NVARCHAR (10)    NOT NULL,
    [Return Order No_]               NVARCHAR (20)    NOT NULL,
    [Return Order No_ Series]        NVARCHAR (10)    NOT NULL,
    [Allow Line Disc_]               TINYINT          NOT NULL,
    [Get Return Receipt Used]        TINYINT          NOT NULL,
    [Ship-to UPS Zone]               NVARCHAR (2)     NOT NULL,
    [Tax Exemption No_]              NVARCHAR (30)    NOT NULL,
    [Salesperson 2]                  NVARCHAR (10)    NOT NULL,
    [Customer Service Rep_]          NVARCHAR (50)    NULL,
    [Order Time]                     DATETIME         NOT NULL,
    [Prepmt_ Cr_ Memo No_ Series]    NVARCHAR (10)    NOT NULL,
    [Prepayment Credit Memo]         TINYINT          NOT NULL,
    [Prepayment Order No_]           NVARCHAR (20)    NOT NULL,
    [STE Transaction ID]             NVARCHAR (20)    NOT NULL,
    [Avalara Status Code]            NVARCHAR (20)    NOT NULL,
    [Leads Order]                    TINYINT          NOT NULL,
    [Escalate Order]                 TINYINT          NOT NULL,
    [Amount Authorized]              DECIMAL (38, 20) NOT NULL,
    [Order Cancelled]                TINYINT          NOT NULL,
    [Authorization No_]              NVARCHAR (50)    NOT NULL,
    [Escalate Original Order No_]    NVARCHAR (20)    NOT NULL,
    [Dimension Set ID]               INT              NULL,
    [Document Exchange Identifier]   NVARCHAR (50)    NULL,
    [Document Exchange Status]       INT              NULL,
    [Doc_ Exch_ Original Identifier] NVARCHAR (50)    NULL,
    [Credit Card No_]                NVARCHAR (20)    NULL,
    [Canceled]                       TINYINT          NULL,
    [Cust_ Ledger Entry No_]         INT              NULL,
    [Electronic Document Sent]       TINYINT          NULL,
    [Original Document XML]          IMAGE            NULL,
    [No_ of E-Documents Sent]        INT              NULL,
    [Original String]                IMAGE            NULL,
    [Digital Stamp SAT]              IMAGE            NULL,
    [Certificate Serial No_]         NVARCHAR (250)   NULL,
    [Signed Document XML]            IMAGE            NULL,
    [Digital Stamp PAC]              IMAGE            NULL,
    [Electronic Document Status]     INT              NULL,
    [Date_Time Stamped]              NVARCHAR (50)    NULL,
    [Date_Time Sent]                 NVARCHAR (50)    NULL,
    [Date_Time Canceled]             NVARCHAR (50)    NULL,
    [Error Code]                     NVARCHAR (10)    NULL,
    [Error Description]              NVARCHAR (250)   NULL,
    [PAC Web Service Name]           NVARCHAR (50)    NULL,
    [QR Code]                        IMAGE            NULL,
    [Fiscal Invoice Number PAC]      NVARCHAR (50)    NULL,
    [Date_Time First Req_ Sent]      NVARCHAR (50)    NULL,
    [EDI Order]                      TINYINT          NULL,
    [EDI Internal Doc_ No_]          NVARCHAR (10)    NULL,
    [EDI Cr_ Memo Generated]         TINYINT          NULL,
    [EDI Cr_ Memo Gen_ Date]         DATETIME         NULL,
    [EDI Trade Partner]              NVARCHAR (20)    NULL,
    [EDI Sell-to Code]               NVARCHAR (20)    NULL,
    [EDI Ship-to Code]               NVARCHAR (20)    NULL,
    [EDI Ship-for Code]              NVARCHAR (20)    NULL,
    [EDI Transaction Date]           DATETIME         NULL,
    [EDI Transaction Time]           DATETIME         NULL,
    CONSTRAINT [PK_Jason Pharm$Sales Cr_Memo Header] PRIMARY KEY CLUSTERED ([No_] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1] TEXTIMAGE_ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$Sales Cr_Memo Header_PostingDate]
    ON [dbo].[Jason Pharm$Sales Cr_Memo Header]([Posting Date] ASC) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_JasonPharm_SalesCrMemoHeader_idx1]
    ON [dbo].[Jason Pharm$Sales Cr_Memo Header]([Posting Date] ASC)
    INCLUDE([No_], [Customer Posting Group], [External Document No_], [Return Order No_], [Escalate Original Order No_]) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [Jason Pharm$Sales Cr_Memo Header_idx1]
    ON [dbo].[Jason Pharm$Sales Cr_Memo Header]([Posting Date] ASC)
    INCLUDE([Sell-to Customer No_], [Ship-to Name], [Payment Discount %], [Currency Code], [Currency Factor], [Sell-to Post Code], [Ship-to Post Code], [Ship-to County], [Ship-to Country_Region Code]) WITH (FILLFACTOR = 85)
    ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$Sales Cr_Memo Header_Posting Date]
    ON [dbo].[Jason Pharm$Sales Cr_Memo Header]([Posting Date] ASC)
    INCLUDE([No_], [Sell-to Customer No_], [Bill-to Customer No_], [Ship-to Name], [Posting Description], [Shortcut Dimension 1 Code], [Customer Posting Group], [Sell-to Customer Name], [Sell-to Address], [Sell-to Address 2], [Sell-to Post Code], [Ship-to Post Code], [Ship-to Country_Region Code], [Document Date], [External Document No_], [Amount Authorized], [Escalate Original Order No_]) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE STATISTICS [External Document No_]
    ON [dbo].[Jason Pharm$Sales Cr_Memo Header]([External Document No_]);


GO
CREATE STATISTICS [Posting Date]
    ON [dbo].[Jason Pharm$Sales Cr_Memo Header]([Posting Date]);

