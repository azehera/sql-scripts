﻿CREATE TABLE [dbo].[Jason Pharm$Country_Region] (
    [timestamp]                  ROWVERSION    NULL,
    [Code]                       NVARCHAR (10) NOT NULL,
    [Name]                       NVARCHAR (50) NOT NULL,
    [EU Country_Region Code]     NVARCHAR (10) NOT NULL,
    [Intrastat Code]             NVARCHAR (10) NOT NULL,
    [Address Format]             INT           NOT NULL,
    [Contact Address Format]     INT           NOT NULL,
    [Distribution Location Code] NVARCHAR (10) NOT NULL,
    [Return Location Code]       NVARCHAR (10) NOT NULL,
    [VAT Scheme]                 NVARCHAR (10) NULL,
    [ISO 2 char Country Code]    NVARCHAR (2)  NULL,
    [ISO 3 digit Country Code]   NVARCHAR (3)  NULL,
    [UPS]                        TINYINT       NULL,
    [FedEx]                      TINYINT       NULL,
    [LTL]                        TINYINT       NULL,
    [Airborne]                   TINYINT       NULL,
    [DHL]                        TINYINT       NULL,
    [Rating Engine]              TINYINT       NULL,
    [USPostal]                   TINYINT       NULL
) ON [Data Filegroup 1];

