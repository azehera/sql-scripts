﻿CREATE TABLE [dbo].[Jason Pharm$Customer Posting Group] (
    [timestamp]                      ROWVERSION       NULL,
    [Code]                           NVARCHAR (10)    NOT NULL,
    [Receivables Account]            NVARCHAR (20)    NOT NULL,
    [Service Charge Acc_]            NVARCHAR (20)    NOT NULL,
    [Payment Disc_ Debit Acc_]       NVARCHAR (20)    NOT NULL,
    [Invoice Rounding Account]       NVARCHAR (20)    NOT NULL,
    [Additional Fee Account]         NVARCHAR (20)    NOT NULL,
    [Interest Account]               NVARCHAR (20)    NOT NULL,
    [Debit Curr_ Appln_ Rndg_ Acc_]  NVARCHAR (20)    NOT NULL,
    [Credit Curr_ Appln_ Rndg_ Acc_] NVARCHAR (20)    NOT NULL,
    [Debit Rounding Account]         NVARCHAR (20)    NOT NULL,
    [Credit Rounding Account]        NVARCHAR (20)    NOT NULL,
    [Payment Disc_ Credit Acc_]      NVARCHAR (20)    NOT NULL,
    [Payment Tolerance Debit Acc_]   NVARCHAR (20)    NOT NULL,
    [Payment Tolerance Credit Acc_]  NVARCHAR (20)    NOT NULL,
    [Discount Freight Min Amount]    DECIMAL (38, 20) NOT NULL,
    [Discount Freight Percentage]    DECIMAL (38, 20) NOT NULL,
    [Enable Item Shipping Exclusion] TINYINT          NOT NULL,
    [Merchant ID]                    NVARCHAR (20)    NOT NULL,
    [Escalate Customer Template]     NVARCHAR (10)    NOT NULL,
    [Add_ Fee per Line Account]      NVARCHAR (20)    NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_Code]
    ON [dbo].[Jason Pharm$Customer Posting Group]([Code] ASC) WITH (FILLFACTOR = 90);

