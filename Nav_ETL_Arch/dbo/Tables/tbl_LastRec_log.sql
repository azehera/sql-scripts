﻿CREATE TABLE [dbo].[tbl_LastRec_log] (
    [LastRecID]     INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TableName]     NVARCHAR (50) NULL,
    [MaxCountValue] NVARCHAR (50) NULL,
    [FieldName]     NVARCHAR (50) NULL,
    [DateModified]  SMALLDATETIME NULL,
    CONSTRAINT [PK_tbl_LastRec_log] PRIMARY KEY CLUSTERED ([LastRecID] ASC)
);

