﻿CREATE TABLE [dbo].[Medifast Nutrition$Ledger Entry Dimension] (
    [timestamp]            ROWVERSION    NOT NULL,
    [Table ID]             INT           NOT NULL,
    [Entry No_]            INT           NOT NULL,
    [Dimension Code]       NVARCHAR (20) NOT NULL,
    [Dimension Value Code] NVARCHAR (20) NOT NULL
);

