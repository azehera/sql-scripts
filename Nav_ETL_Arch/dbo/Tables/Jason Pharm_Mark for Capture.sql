﻿CREATE TABLE [dbo].[Jason Pharm$Mark for Capture] (
    [Document Type]             INT              NOT NULL,
    [NAV Document No_]          NVARCHAR (20)    NOT NULL,
    [Nav Order No_]             NVARCHAR (20)    NOT NULL,
    [Order ID]                  NVARCHAR (20)    NOT NULL,
    [Merchant ID]               NVARCHAR (20)    NOT NULL,
    [Reference No_]             NVARCHAR (50)    NOT NULL,
    [Authorization Amount]      DECIMAL (38, 20) NOT NULL,
    [Shipped Amount]            DECIMAL (38, 20) NOT NULL,
    [Tax Amount]                DECIMAL (38, 20) NOT NULL,
    [Status Reponse]            NVARCHAR (200)   NOT NULL,
    [MFC Sent Date]             DATETIME         NOT NULL,
    [MFC Response Process Date] DATETIME         NOT NULL,
    [Entry No_]                 INT              NOT NULL,
    [Posting Date]              DATETIME         NOT NULL,
    [Processed]                 TINYINT          NOT NULL,
    [Avalara Status Code]       NVARCHAR (20)    NOT NULL,
    [Return Type]               INT              NOT NULL,
    CONSTRAINT [PK_Jason Pharm$Mark for Capture] PRIMARY KEY CLUSTERED ([NAV Document No_] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

