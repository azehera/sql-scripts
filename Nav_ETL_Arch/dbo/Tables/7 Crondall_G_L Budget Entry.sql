﻿CREATE TABLE [dbo].[7 Crondall$G_L Budget Entry] (
    [Entry No_]               INT              NOT NULL,
    [Budget Name]             NVARCHAR (10)    NULL,
    [G_L Account No_]         NVARCHAR (20)    NULL,
    [Date]                    DATETIME         NULL,
    [Global Dimension 1 Code] NVARCHAR (20)    NULL,
    [Global Dimension 2 Code] NVARCHAR (20)    NULL,
    [Amount]                  NUMERIC (38, 20) NULL,
    [Description]             NVARCHAR (50)    NULL,
    [Business Unit Code]      NVARCHAR (10)    NULL,
    [User ID]                 NVARCHAR (50)    NULL,
    [Budget Dimension 1 Code] NVARCHAR (20)    NULL,
    [Budget Dimension 2 Code] NVARCHAR (20)    NULL,
    [Budget Dimension 3 Code] NVARCHAR (20)    NULL,
    [Budget Dimension 4 Code] NVARCHAR (20)    NULL,
    [Last Date Modified]      DATETIME         NULL,
    [Dimension Set ID]        INT              NULL,
    CONSTRAINT [PK_7 Crondall$G_L Budget Entry] PRIMARY KEY CLUSTERED ([Entry No_] ASC) WITH (FILLFACTOR = 90)
);

