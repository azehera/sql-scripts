﻿CREATE TABLE [dbo].[Jason Pharm$Accounting Period] (
    [timestamp]               ROWVERSION    NULL,
    [Starting Date]           DATETIME      NOT NULL,
    [Name]                    NVARCHAR (10) NOT NULL,
    [New Fiscal Year]         TINYINT       NOT NULL,
    [Closed]                  TINYINT       NOT NULL,
    [Date Locked]             TINYINT       NOT NULL,
    [Average Cost Calc_ Type] INT           NOT NULL,
    [Average Cost Period]     INT           NOT NULL,
    CONSTRAINT [PK_Jason Pharm$Accounting Period_1] PRIMARY KEY CLUSTERED ([Starting Date] ASC, [Name] ASC, [New Fiscal Year] ASC, [Closed] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

