﻿CREATE TABLE [dbo].[Snapshot$NAV_InventoryStdCost_ForUpdate2] (
    [EntryNo]               INT             IDENTITY (1, 1) NOT NULL,
    [RunDate]               DATETIME        NOT NULL,
    [ItemNo]                VARCHAR (20)    NOT NULL,
    [InventoryPostingGroup] VARCHAR (10)    NULL,
    [ItemCategoryCode]      VARCHAR (10)    NULL,
    [ProductGroupCode]      VARCHAR (10)    NULL,
    [StandardCost]          DECIMAL (18, 6) NULL
);

