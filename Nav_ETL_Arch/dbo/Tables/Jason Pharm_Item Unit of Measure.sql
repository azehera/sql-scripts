﻿CREATE TABLE [dbo].[Jason Pharm$Item Unit of Measure] (
    [timestamp]                ROWVERSION       NULL,
    [Item No_]                 NVARCHAR (20)    NOT NULL,
    [Code]                     NVARCHAR (10)    NOT NULL,
    [Qty_ per Unit of Measure] DECIMAL (38, 20) NOT NULL,
    [Length]                   DECIMAL (38, 20) NOT NULL,
    [Width]                    DECIMAL (38, 20) NOT NULL,
    [Height]                   DECIMAL (38, 20) NOT NULL,
    [Cubage]                   DECIMAL (38, 20) NOT NULL,
    [Weight]                   DECIMAL (38, 20) NOT NULL,
    [Volume]                   DECIMAL (38, 20) NOT NULL,
    [Commission Volume]        DECIMAL (38, 20) NOT NULL,
    [Trading Unit Type]        INT              NOT NULL,
    [Trading Unit Code]        NVARCHAR (20)    NOT NULL,
    [Std_ Pack UPC_EAN Number] NVARCHAR (20)    NULL
) ON [Data Filegroup 1];

