﻿CREATE TABLE [dbo].[Jason Pharm$Salesperson_Purchaser] (
    [timestamp]               ROWVERSION       NULL,
    [Code]                    NVARCHAR (10)    NOT NULL,
    [Name]                    NVARCHAR (50)    NOT NULL,
    [Commission %]            DECIMAL (38, 20) NOT NULL,
    [Global Dimension 1 Code] NVARCHAR (20)    NOT NULL,
    [Global Dimension 2 Code] NVARCHAR (20)    NOT NULL,
    [E-Mail]                  NVARCHAR (80)    NOT NULL,
    [Phone No_]               NVARCHAR (30)    NOT NULL,
    [Job Title]               NVARCHAR (30)    NOT NULL,
    [Search E-Mail]           NVARCHAR (80)    NOT NULL,
    [E-Mail 2]                NVARCHAR (80)    NOT NULL,
    [Salesperson 2]           NVARCHAR (10)    NOT NULL,
    [Sales Manager]           NVARCHAR (10)    NOT NULL
) ON [Data Filegroup 1];

