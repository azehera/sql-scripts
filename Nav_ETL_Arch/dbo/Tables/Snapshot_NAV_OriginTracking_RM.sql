﻿CREATE TABLE [dbo].[Snapshot$NAV_OriginTracking_RM] (
    [EntryNo]                  INT              IDENTITY (1, 1) NOT NULL,
    [RunDate]                  DATETIME         NOT NULL,
    [Yesterday]                DATETIME         NOT NULL,
    [Posting Date]             DATETIME         NOT NULL,
    [Entry Type]               VARCHAR (30)     NOT NULL,
    [Document Type]            VARCHAR (30)     NOT NULL,
    [Document No_]             VARCHAR (20)     NOT NULL,
    [Item No_]                 VARCHAR (20)     NOT NULL,
    [Unit of Measure Code]     VARCHAR (10)     NOT NULL,
    [Expiration Date]          DATETIME         NOT NULL,
    [Lot No_]                  VARCHAR (20)     NOT NULL,
    [Location Code]            VARCHAR (10)     NOT NULL,
    [Quantity]                 DECIMAL (38, 20) NOT NULL,
    [Invoiced Quantity]        DECIMAL (38, 20) NOT NULL,
    [Remaining Quantity]       DECIMAL (38, 20) NOT NULL,
    [Qty_ per Unit of Measure] DECIMAL (38, 20) NOT NULL,
    [Completely Invoiced]      TINYINT          NOT NULL,
    [Cost Amount (Expected)]   DECIMAL (38, 20) NOT NULL,
    [Cost Amount (Actual)]     DECIMAL (38, 20) NOT NULL,
    [Open]                     TINYINT          NOT NULL,
    CONSTRAINT [PK_Snapshot$NAV_OriginTracking_RM] PRIMARY KEY CLUSTERED ([EntryNo] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Snapshot$NAV_OriginTracking_RM_idx1]
    ON [dbo].[Snapshot$NAV_OriginTracking_RM]([RunDate] ASC)
    INCLUDE([EntryNo]) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];

