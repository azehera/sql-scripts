﻿CREATE TABLE [dbo].[Jason Pharm$G_L Entry] (
    [Entry No_]                      INT              NOT NULL,
    [G_L Account No_]                NVARCHAR (20)    NOT NULL,
    [Posting Date]                   DATETIME         NOT NULL,
    [Document Type]                  INT              NOT NULL,
    [Document No_]                   NVARCHAR (20)    NOT NULL,
    [Description]                    NVARCHAR (50)    NOT NULL,
    [Bal_ Account No_]               NVARCHAR (20)    NOT NULL,
    [Amount]                         DECIMAL (38, 20) NOT NULL,
    [Global Dimension 1 Code]        NVARCHAR (20)    NOT NULL,
    [Global Dimension 2 Code]        NVARCHAR (20)    NOT NULL,
    [User ID]                        NVARCHAR (50)    NULL,
    [Source Code]                    NVARCHAR (10)    NOT NULL,
    [System-Created Entry]           TINYINT          NOT NULL,
    [Prior-Year Entry]               TINYINT          NOT NULL,
    [Job No_]                        NVARCHAR (20)    NOT NULL,
    [Quantity]                       DECIMAL (38, 20) NOT NULL,
    [VAT Amount]                     DECIMAL (38, 20) NOT NULL,
    [Business Unit Code]             NVARCHAR (10)    NOT NULL,
    [Journal Batch Name]             NVARCHAR (10)    NOT NULL,
    [Reason Code]                    NVARCHAR (10)    NOT NULL,
    [Gen_ Posting Type]              INT              NOT NULL,
    [Gen_ Bus_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Gen_ Prod_ Posting Group]       NVARCHAR (10)    NOT NULL,
    [Bal_ Account Type]              INT              NOT NULL,
    [Transaction No_]                INT              NOT NULL,
    [Debit Amount]                   DECIMAL (38, 20) NOT NULL,
    [Credit Amount]                  DECIMAL (38, 20) NOT NULL,
    [Document Date]                  DATETIME         NOT NULL,
    [External Document No_]          NVARCHAR (35)    NULL,
    [Source Type]                    INT              NOT NULL,
    [Source No_]                     NVARCHAR (20)    NOT NULL,
    [No_ Series]                     NVARCHAR (10)    NOT NULL,
    [Tax Area Code]                  NVARCHAR (20)    NOT NULL,
    [Tax Liable]                     TINYINT          NOT NULL,
    [Tax Group Code]                 NVARCHAR (10)    NOT NULL,
    [Use Tax]                        TINYINT          NOT NULL,
    [VAT Bus_ Posting Group]         NVARCHAR (10)    NOT NULL,
    [VAT Prod_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Additional-Currency Amount]     DECIMAL (38, 20) NOT NULL,
    [Add_-Currency Debit Amount]     DECIMAL (38, 20) NOT NULL,
    [Add_-Currency Credit Amount]    DECIMAL (38, 20) NOT NULL,
    [Close Income Statement Dim_ ID] INT              NOT NULL,
    [IC Partner Code]                NVARCHAR (20)    NOT NULL,
    [Reversed]                       TINYINT          NOT NULL,
    [Reversed by Entry No_]          INT              NOT NULL,
    [Reversed Entry No_]             INT              NOT NULL,
    [Prod_ Order No_]                NVARCHAR (20)    NOT NULL,
    [FA Entry Type]                  INT              NOT NULL,
    [FA Entry No_]                   INT              NOT NULL,
    [STE Transaction ID]             NVARCHAR (20)    NOT NULL,
    [Dimension Set ID]               INT              NULL,
    [GST_HST]                        INT              NULL,
    CONSTRAINT [PK_Jason Pharm$G_L Entry] PRIMARY KEY CLUSTERED ([Entry No_] ASC) ON [Data Filegroup 1]
) ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$G_L Entry_idx2]
    ON [dbo].[Jason Pharm$G_L Entry]([Source Code] ASC, [G_L Account No_] ASC, [Posting Date] ASC)
    INCLUDE([Entry No_], [Document No_], [Description], [Global Dimension 2 Code], [Transaction No_], [Debit Amount], [Credit Amount], [Document Date], [External Document No_], [Source No_]) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$G_L Entry_PostingDate]
    ON [dbo].[Jason Pharm$G_L Entry]([Posting Date] ASC) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE STATISTICS [Posting Date]
    ON [dbo].[Jason Pharm$G_L Entry]([Posting Date]);

