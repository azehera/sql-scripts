﻿CREATE TABLE [dbo].[Jason Pharm$Origin Tracking Entry] (
    [Entry No_]            INT              NOT NULL,
    [Document No_]         NVARCHAR (20)    NOT NULL,
    [Location Code]        NVARCHAR (10)    NOT NULL,
    [Zone Code]            NVARCHAR (10)    NOT NULL,
    [Bin Code]             NVARCHAR (20)    NOT NULL,
    [Item No_]             NVARCHAR (20)    NOT NULL,
    [Quantity (Base)]      NUMERIC (38, 20) NOT NULL,
    [Expiration Date]      DATETIME         NOT NULL,
    [Lot No_]              NVARCHAR (20)    NOT NULL,
    [Quantity]             NUMERIC (38, 20) NOT NULL,
    [Unit of Measure Code] NVARCHAR (10)    NOT NULL,
    [Open Date]            DATETIME         NOT NULL,
    [Close Date]           DATETIME         NOT NULL,
    [Open Time]            DATETIME         NOT NULL,
    [Close Time]           DATETIME         NOT NULL,
    [Creation DateTime]    DATETIME         NOT NULL,
    [Created by]           NVARCHAR (50)    NULL,
    [On Hold]              TINYINT          NOT NULL,
    [On Hold by]           NVARCHAR (50)    NULL,
    [On Hold DateTime]     DATETIME         NOT NULL,
    CONSTRAINT [Jason Pharm$Origin Tracking Entry$0] PRIMARY KEY CLUSTERED ([Entry No_] ASC) WITH (FILLFACTOR = 90)
);

