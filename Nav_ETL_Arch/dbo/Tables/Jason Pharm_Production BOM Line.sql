﻿CREATE TABLE [dbo].[Jason Pharm$Production BOM Line] (
    [Production BOM No_]   NVARCHAR (20)    NULL,
    [Version Code]         NVARCHAR (20)    NULL,
    [Line No_]             INT              NULL,
    [Type]                 INT              NULL,
    [No_]                  NVARCHAR (20)    NULL,
    [Description]          NVARCHAR (50)    NULL,
    [Unit of Measure Code] NVARCHAR (10)    NULL,
    [Quantity]             NUMERIC (38, 20) NULL,
    [Position]             NVARCHAR (10)    NULL,
    [Position 2]           NVARCHAR (10)    NULL,
    [Position 3]           NVARCHAR (10)    NULL,
    [Production Lead Time] VARCHAR (32)     NULL,
    [Routing Link Code]    NVARCHAR (10)    NULL,
    [Scrap %]              NUMERIC (38, 20) NULL,
    [Variant Code]         NVARCHAR (10)    NULL,
    [Starting Date]        DATETIME         NULL,
    [Ending Date]          DATETIME         NULL,
    [Length]               NUMERIC (38, 20) NULL,
    [Width]                NUMERIC (38, 20) NULL,
    [Weight]               NUMERIC (38, 20) NULL,
    [Depth]                NUMERIC (38, 20) NULL,
    [Calculation Formula]  INT              NULL,
    [Quantity per]         NUMERIC (38, 20) NULL,
    [Lot Determining]      TINYINT          NULL,
    [Item Type]            INT              NULL,
    [Share %]              NUMERIC (38, 20) NULL,
    [Shares]               NUMERIC (38, 20) NULL
);

