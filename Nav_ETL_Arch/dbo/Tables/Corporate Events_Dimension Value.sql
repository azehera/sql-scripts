﻿CREATE TABLE [dbo].[Corporate Events$Dimension Value] (
    [Dimension Code]                 NVARCHAR (20)    NOT NULL,
    [Code]                           NVARCHAR (20)    NOT NULL,
    [Name]                           NVARCHAR (50)    NOT NULL,
    [Dimension Value Type]           INT              NOT NULL,
    [Totaling]                       NVARCHAR (250)   NOT NULL,
    [Blocked]                        TINYINT          NOT NULL,
    [Consolidation Code]             NVARCHAR (20)    NOT NULL,
    [Indentation]                    INT              NOT NULL,
    [Global Dimension No_]           INT              NOT NULL,
    [Map-to IC Dimension Code]       NVARCHAR (20)    NOT NULL,
    [Map-to IC Dimension Value Code] NVARCHAR (20)    NOT NULL,
    [Commission Rate %]              DECIMAL (38, 20) NOT NULL,
    [Dim_ Total]                     DECIMAL (38, 20) NOT NULL,
    [Dimension Value ID]             INT              NULL
);

