﻿CREATE TABLE [dbo].[Jason Pharm$Ledger Entry Dimension] (
    [Table ID]             INT           NOT NULL,
    [Entry No_]            INT           NOT NULL,
    [Dimension Code]       NVARCHAR (20) NOT NULL,
    [Dimension Value Code] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_Jason Pharm$Ledger Entry Dimension_1] PRIMARY KEY CLUSTERED ([Table ID] ASC, [Entry No_] ASC, [Dimension Code] ASC) WITH (FILLFACTOR = 80)
);

