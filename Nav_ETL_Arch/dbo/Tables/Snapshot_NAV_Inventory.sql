﻿CREATE TABLE [dbo].[Snapshot$NAV_Inventory] (
    [EntryNo]                         INT             IDENTITY (1, 1) NOT NULL,
    [RunDate]                         DATETIME        NULL,
    [Yesterday]                       DATETIME        NULL,
    [YesterdayPriorYr]                DATETIME        NULL,
    [EndingDatePriorMonth]            DATETIME        NULL,
    [EndingDatePriorYear]             DATETIME        NULL,
    [B4T_ItemNo]                      VARCHAR (20)    COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [ItemNo]                          VARCHAR (20)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [NavItemNo]                       VARCHAR (20)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [InvLocation]                     VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [InvPostClass]                    VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [InvPostGroup]                    VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ItemCategoryCode]                VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ProductGroupCode]                VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [StandardCost]                    DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Inventory_StandardCost] DEFAULT ((0)) NULL,
    [StandardCostPriorYrDay]          DECIMAL (18, 6) NULL,
    [StandardCostPriorMonthEnd]       DECIMAL (18, 6) NULL,
    [StandardCostPriorYearEnd]        DECIMAL (18, 6) NULL,
    [QtyPerUnitOfMeasure]             DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Inventory_QtyPerUnitOfMeasure] DEFAULT ((1)) NOT NULL,
    [OnHandQtyDay]                    INT             CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandQtyDay] DEFAULT ((0)) NOT NULL,
    [OnHandQtyBaseDay]                INT             CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandQtyBaseDay] DEFAULT ((0)) NOT NULL,
    [OnHandAmtDay]                    DECIMAL (18, 2) CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandAmtDay] DEFAULT ((0)) NOT NULL,
    [OnHandQtyPriorYrDay]             INT             CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandQtyPriorYrDay] DEFAULT ((0)) NOT NULL,
    [OnHandQtyBasePriorYrDay]         INT             CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandQtyBasePriorYrDay] DEFAULT ((0)) NOT NULL,
    [OnHandAmtPriorYrDay]             DECIMAL (18, 2) CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandAmtPriorYrDay] DEFAULT ((0)) NOT NULL,
    [OnHandQtyPriorMonthEnd]          INT             CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandQtyPriorMonthEnd] DEFAULT ((0)) NOT NULL,
    [OnHandQtyBasePriorMonthEnd]      INT             CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandQtyBasePriorMonthEnd] DEFAULT ((0)) NOT NULL,
    [OnHandAmtPriorMonthEnd]          DECIMAL (18, 2) CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandAmtPriorMonthEnd] DEFAULT ((0)) NOT NULL,
    [OnHandQtyPriorYearEnd]           INT             CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandQtyPriorYearEnd] DEFAULT ((0)) NOT NULL,
    [OnHandQtyBasePriorYearEnd]       INT             CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandQtyBasePriorYearEnd] DEFAULT ((0)) NOT NULL,
    [OnHandAmtPriorYearEnd]           DECIMAL (18, 2) CONSTRAINT [DF_Snapshot$NAV_Inventory_OnHandAmtPriorYearEnd] DEFAULT ((0)) NOT NULL,
    [InvLocationName]                 VARCHAR (50)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [InvLocationType]                 VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ForecastQtyCurrentMonth]         DECIMAL (18, 2) NULL,
    [OnHandForecastDays]              DECIMAL (18, 2) NULL,
    [OnHandForecastDaysPriorYrDay]    DECIMAL (18, 2) NULL,
    [OnHandForecastDaysPriorMonthEnd] DECIMAL (18, 2) NULL,
    [OnHandForecastDaysPriorYearEnd]  DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_Snapshot$NAV_Inventory] PRIMARY KEY CLUSTERED ([EntryNo] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [Temp1]
    ON [dbo].[Snapshot$NAV_Inventory]([RunDate] ASC, [NavItemNo] ASC, [InvLocation] ASC)
    INCLUDE([OnHandQtyBasePriorYearEnd]) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_Snapshot$NAV_Inventory_idx2]
    ON [dbo].[Snapshot$NAV_Inventory]([B4T_ItemNo] ASC)
    INCLUDE([EntryNo], [InvLocation]) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_Snapshot$NAV_Inventory_idx1]
    ON [dbo].[Snapshot$NAV_Inventory]([RunDate] ASC, [NavItemNo] ASC)
    INCLUDE([EntryNo], [InvPostGroup]) WITH (FILLFACTOR = 85)
    ON [Data Filegroup 1];

