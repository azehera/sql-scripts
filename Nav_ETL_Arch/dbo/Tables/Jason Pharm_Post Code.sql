﻿CREATE TABLE [dbo].[Jason Pharm$Post Code] (
    [timestamp]            ROWVERSION    NULL,
    [Code]                 NVARCHAR (20) NOT NULL,
    [City]                 NVARCHAR (30) NOT NULL,
    [Search City]          NVARCHAR (30) NOT NULL,
    [Search State]         NVARCHAR (30) NOT NULL,
    [County]               NVARCHAR (30) NOT NULL,
    [Country_Region Code]  NVARCHAR (10) NULL,
    [USPS Non Deliverable] INT           NULL
) ON [Data Filegroup 1];

