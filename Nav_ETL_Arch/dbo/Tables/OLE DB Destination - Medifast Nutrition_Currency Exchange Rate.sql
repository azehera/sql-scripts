﻿CREATE TABLE [dbo].[OLE DB Destination - Medifast Nutrition$Currency Exchange Rate] (
    [timestamp]                      BINARY (8)       NULL,
    [Currency Code]                  NVARCHAR (10)    NULL,
    [Starting Date]                  DATETIME         NULL,
    [Exchange Rate Amount]           NUMERIC (38, 20) NULL,
    [Adjustment Exch_ Rate Amount]   NUMERIC (38, 20) NULL,
    [Relational Currency Code]       NVARCHAR (10)    NULL,
    [Relational Exch_ Rate Amount]   NUMERIC (38, 20) NULL,
    [Fix Exchange Rate Amount]       INT              NULL,
    [Relational Adjmt Exch Rate Amt] NUMERIC (38, 20) NULL
);

