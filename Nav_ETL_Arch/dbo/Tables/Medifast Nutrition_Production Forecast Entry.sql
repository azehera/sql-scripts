﻿CREATE TABLE [dbo].[Medifast Nutrition$Production Forecast Entry] (
    [Entry No_]                INT              NOT NULL,
    [Production Forecast Name] NVARCHAR (10)    NOT NULL,
    [Item No_]                 NVARCHAR (20)    NOT NULL,
    [Forecast Date]            DATETIME         NOT NULL,
    [Forecast Quantity]        DECIMAL (38, 20) NOT NULL,
    [Unit of Measure Code]     NVARCHAR (10)    NOT NULL,
    [Qty_ per Unit of Measure] DECIMAL (38, 20) NOT NULL,
    [Forecast Quantity (Base)] DECIMAL (38, 20) NOT NULL,
    [Location Code]            NVARCHAR (10)    NOT NULL,
    [Component Forecast]       TINYINT          NOT NULL,
    [Description]              NVARCHAR (50)    NOT NULL,
    [Exclusion]                TINYINT          NOT NULL,
    CONSTRAINT [Medifast Nutrition$Production Forecast Entry$0] PRIMARY KEY CLUSTERED ([Entry No_] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

