﻿CREATE TABLE [dbo].[Jason Pharm$Sales Shipment Line] (
    [Document No_]                   NVARCHAR (20)    NOT NULL,
    [Line No_]                       INT              NOT NULL,
    [Sell-to Customer No_]           NVARCHAR (20)    NOT NULL,
    [Type]                           INT              NOT NULL,
    [No_]                            NVARCHAR (20)    NOT NULL,
    [Location Code]                  NVARCHAR (10)    NOT NULL,
    [Posting Group]                  NVARCHAR (10)    NOT NULL,
    [Shipment Date]                  DATETIME         NOT NULL,
    [Description]                    NVARCHAR (50)    NOT NULL,
    [Description 2]                  NVARCHAR (50)    NOT NULL,
    [Unit of Measure]                NVARCHAR (10)    NOT NULL,
    [Quantity]                       DECIMAL (38, 20) NOT NULL,
    [Unit Price]                     DECIMAL (38, 20) NOT NULL,
    [Unit Cost (LCY)]                DECIMAL (38, 20) NOT NULL,
    [VAT %]                          DECIMAL (38, 20) NOT NULL,
    [Line Discount %]                DECIMAL (38, 20) NOT NULL,
    [Allow Invoice Disc_]            TINYINT          NOT NULL,
    [Gross Weight]                   DECIMAL (38, 20) NOT NULL,
    [Net Weight]                     DECIMAL (38, 20) NOT NULL,
    [Units per Parcel]               DECIMAL (38, 20) NOT NULL,
    [Unit Volume]                    DECIMAL (38, 20) NOT NULL,
    [Appl_-to Item Entry]            INT              NOT NULL,
    [Item Shpt_ Entry No_]           INT              NOT NULL,
    [Shortcut Dimension 1 Code]      NVARCHAR (20)    NOT NULL,
    [Shortcut Dimension 2 Code]      NVARCHAR (20)    NOT NULL,
    [Customer Price Group]           NVARCHAR (10)    NOT NULL,
    [Job No_]                        NVARCHAR (20)    NOT NULL,
    [Work Type Code]                 NVARCHAR (10)    NOT NULL,
    [Qty_ Shipped Not Invoiced]      DECIMAL (38, 20) NOT NULL,
    [Quantity Invoiced]              DECIMAL (38, 20) NOT NULL,
    [Order No_]                      NVARCHAR (20)    NOT NULL,
    [Order Line No_]                 INT              NOT NULL,
    [Bill-to Customer No_]           NVARCHAR (20)    NOT NULL,
    [Purchase Order No_]             NVARCHAR (20)    NOT NULL,
    [Purch_ Order Line No_]          INT              NOT NULL,
    [Drop Shipment]                  TINYINT          NOT NULL,
    [Gen_ Bus_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Gen_ Prod_ Posting Group]       NVARCHAR (10)    NOT NULL,
    [VAT Calculation Type]           INT              NOT NULL,
    [Transaction Type]               NVARCHAR (10)    NOT NULL,
    [Transport Method]               NVARCHAR (10)    NOT NULL,
    [Attached to Line No_]           INT              NOT NULL,
    [Exit Point]                     NVARCHAR (10)    NOT NULL,
    [Area]                           NVARCHAR (10)    NOT NULL,
    [Transaction Specification]      NVARCHAR (10)    NOT NULL,
    [Tax Area Code]                  NVARCHAR (20)    NOT NULL,
    [Tax Liable]                     TINYINT          NOT NULL,
    [Tax Group Code]                 NVARCHAR (10)    NOT NULL,
    [VAT Bus_ Posting Group]         NVARCHAR (10)    NOT NULL,
    [VAT Prod_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Blanket Order No_]              NVARCHAR (20)    NOT NULL,
    [Blanket Order Line No_]         INT              NOT NULL,
    [VAT Base Amount]                DECIMAL (38, 20) NOT NULL,
    [Unit Cost]                      DECIMAL (38, 20) NOT NULL,
    [Variant Code]                   NVARCHAR (10)    NOT NULL,
    [Bin Code]                       NVARCHAR (20)    NOT NULL,
    [Qty_ per Unit of Measure]       DECIMAL (38, 20) NOT NULL,
    [Unit of Measure Code]           NVARCHAR (10)    NOT NULL,
    [Quantity (Base)]                DECIMAL (38, 20) NOT NULL,
    [Qty_ Invoiced (Base)]           DECIMAL (38, 20) NOT NULL,
    [FA Posting Date]                DATETIME         NOT NULL,
    [Depreciation Book Code]         NVARCHAR (10)    NOT NULL,
    [Depr_ until FA Posting Date]    TINYINT          NOT NULL,
    [Duplicate in Depreciation Book] NVARCHAR (10)    NOT NULL,
    [Use Duplication List]           TINYINT          NOT NULL,
    [Responsibility Center]          NVARCHAR (10)    NOT NULL,
    [Cross-Reference No_]            NVARCHAR (20)    NOT NULL,
    [Unit of Measure (Cross Ref_)]   NVARCHAR (10)    NOT NULL,
    [Cross-Reference Type]           INT              NOT NULL,
    [Cross-Reference Type No_]       NVARCHAR (30)    NOT NULL,
    [Item Category Code]             NVARCHAR (10)    NOT NULL,
    [Nonstock]                       TINYINT          NOT NULL,
    [Purchasing Code]                NVARCHAR (10)    NOT NULL,
    [Product Group Code]             NVARCHAR (10)    NOT NULL,
    [Requested Delivery Date]        DATETIME         NOT NULL,
    [Promised Delivery Date]         DATETIME         NOT NULL,
    [Shipping Time]                  VARCHAR (32)     NULL,
    [Outbound Whse_ Handling Time]   VARCHAR (32)     NULL,
    [Planned Delivery Date]          DATETIME         NOT NULL,
    [Planned Shipment Date]          DATETIME         NOT NULL,
    [Appl_-from Item Entry]          INT              NOT NULL,
    [Item Charge Base Amount]        DECIMAL (38, 20) NOT NULL,
    [Correction]                     TINYINT          NOT NULL,
    [Return Reason Code]             NVARCHAR (10)    NOT NULL,
    [Allow Line Disc_]               TINYINT          NOT NULL,
    [Customer Disc_ Group]           NVARCHAR (20)    NULL,
    [Package Tracking No_]           NVARCHAR (30)    NOT NULL,
    [Manufacturer Part No_]          NVARCHAR (20)    NOT NULL,
    [Volume]                         DECIMAL (38, 20) NOT NULL,
    [Commission Volume]              DECIMAL (38, 20) NOT NULL,
    [Returnable Container]           TINYINT          NOT NULL,
    [Delivery Tolerance Code]        NVARCHAR (10)    NOT NULL,
    [Posting Date]                   DATETIME         NOT NULL,
    [Job Task No_]                   NVARCHAR (20)    NOT NULL,
    [Job Contract Entry No_]         INT              NOT NULL,
    [Order Item No_]                 NVARCHAR (20)    NULL,
    [Dimension Set ID]               INT              NULL,
    [Authorized for Credit Card]     TINYINT          NULL,
    [EDI Item Cross Ref_]            NVARCHAR (40)    NULL,
    [EDI Unit of Measure]            NVARCHAR (10)    NULL,
    [EDI Unit Price]                 DECIMAL (38, 20) NULL,
    [EDI Price Discrepancy]          TINYINT          NULL,
    [EDI Segment Group]              INT              NULL,
    [EDI Original Qty_]              DECIMAL (38, 20) NULL,
    [EDI Status Pending]             TINYINT          NULL,
    [EDI Release No_]                NVARCHAR (20)    NULL,
    [EDI Ship Req_ Date]             DATETIME         NULL,
    [EDI Kanban No_]                 NVARCHAR (20)    NULL,
    [EDI Line Type]                  INT              NULL,
    [EDI Line Status]                INT              NULL,
    [EDI Cumulative Quantity]        DECIMAL (38, 20) NULL,
    [EDI Forecast Begin Date]        DATETIME         NULL,
    [EDI Forecast End Date]          DATETIME         NULL,
    [EDI Code]                       NVARCHAR (35)    NULL,
    [EDI ID]                         NVARCHAR (20)    NULL,
    [EDI Variant Code]               NVARCHAR (40)    NULL,
    [EDI Time]                       DATETIME         NULL,
    [EDI DateTime]                   DATETIME         NULL,
    [Shipping Charge]                TINYINT          NULL,
    [Qty_ Packed (Base)]             DECIMAL (38, 20) NULL,
    [Pack]                           TINYINT          NULL,
    [Rate Quoted]                    TINYINT          NULL,
    [Std_ Package Unit of Meas Code] NVARCHAR (10)    NULL,
    [Std_ Package Quantity]          DECIMAL (38, 20) NULL,
    [Qty_ per Std_ Package]          DECIMAL (38, 20) NULL,
    [Std_ Package Qty_ to Ship]      DECIMAL (38, 20) NULL,
    [Std_ Packs per Package]         INT              NULL,
    [Package Quantity]               DECIMAL (38, 20) NULL,
    [Package Qty_ to Ship]           DECIMAL (38, 20) NULL,
    [E-Ship Whse_ Outst_ Qty (Base)] DECIMAL (38, 20) NULL,
    [Shipping Charge BOL No_]        NVARCHAR (20)    NULL,
    [Required Shipping Agent Code]   NVARCHAR (10)    NULL,
    [Required E-Ship Agent Service]  NVARCHAR (30)    NULL,
    [Allow Other Ship_ Agent_Serv_]  TINYINT          NULL,
    [E-Ship Agent Code]              NVARCHAR (10)    NULL,
    [E-Ship Agent Service]           NVARCHAR (30)    NULL,
    [Shipping Payment Type]          INT              NULL,
    [Third Party Ship_ Account No_]  NVARCHAR (20)    NULL,
    [Shipping Insurance]             INT              NULL,
    CONSTRAINT [Jason Pharm$Sales Shipment Line$0] PRIMARY KEY CLUSTERED ([Document No_] ASC, [Line No_] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_JasonPharm$SalesShipmentLine_PostDte]
    ON [dbo].[Jason Pharm$Sales Shipment Line]([Posting Date] ASC)
    INCLUDE([Document No_]) WITH (FILLFACTOR = 80)
    ON [Data Filegroup 1];


GO
CREATE STATISTICS [Line No_]
    ON [dbo].[Jason Pharm$Sales Shipment Line]([Line No_]);


GO
CREATE STATISTICS [Order Line No_]
    ON [dbo].[Jason Pharm$Sales Shipment Line]([Order Line No_]);


GO
CREATE STATISTICS [Type]
    ON [dbo].[Jason Pharm$Sales Shipment Line]([Type]);

