﻿CREATE TABLE [dbo].[Jason Pharm$Sales Price] (
    [timestamp]                    ROWVERSION       NULL,
    [Item No_]                     NVARCHAR (20)    NOT NULL,
    [Sales Type]                   INT              NOT NULL,
    [Sales Code]                   NVARCHAR (20)    NOT NULL,
    [Starting Date]                DATETIME         NOT NULL,
    [Currency Code]                NVARCHAR (10)    NOT NULL,
    [Variant Code]                 NVARCHAR (10)    NOT NULL,
    [Unit of Measure Code]         NVARCHAR (10)    NOT NULL,
    [Minimum Quantity]             DECIMAL (38, 20) NOT NULL,
    [Unit Price]                   DECIMAL (38, 20) NOT NULL,
    [Price Includes VAT]           TINYINT          NOT NULL,
    [Allow Invoice Disc_]          TINYINT          NOT NULL,
    [VAT Bus_ Posting Gr_ (Price)] NVARCHAR (10)    NOT NULL,
    [Ending Date]                  DATETIME         NOT NULL,
    [Allow Line Disc_]             TINYINT          NOT NULL,
    CONSTRAINT [Jason Pharm$Sales Price$0] PRIMARY KEY CLUSTERED ([Item No_] ASC, [Sales Type] ASC, [Sales Code] ASC, [Starting Date] ASC, [Currency Code] ASC, [Variant Code] ASC, [Unit of Measure Code] ASC, [Minimum Quantity] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

