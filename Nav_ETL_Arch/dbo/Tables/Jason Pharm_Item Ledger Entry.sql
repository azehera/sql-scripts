﻿CREATE TABLE [dbo].[Jason Pharm$Item Ledger Entry] (
    [Entry No_]                    INT              NOT NULL,
    [Item No_]                     NVARCHAR (20)    NOT NULL,
    [Posting Date]                 DATETIME         NOT NULL,
    [Entry Type]                   INT              NOT NULL,
    [Source No_]                   NVARCHAR (20)    NOT NULL,
    [Document No_]                 NVARCHAR (20)    NOT NULL,
    [Description]                  NVARCHAR (50)    NOT NULL,
    [Location Code]                NVARCHAR (10)    NOT NULL,
    [Quantity]                     DECIMAL (38, 20) NOT NULL,
    [Remaining Quantity]           DECIMAL (38, 20) NOT NULL,
    [Invoiced Quantity]            DECIMAL (38, 20) NOT NULL,
    [Applies-to Entry]             INT              NOT NULL,
    [Open]                         TINYINT          NOT NULL,
    [Global Dimension 1 Code]      NVARCHAR (20)    NOT NULL,
    [Global Dimension 2 Code]      NVARCHAR (20)    NOT NULL,
    [Positive]                     TINYINT          NOT NULL,
    [Source Type]                  INT              NOT NULL,
    [Drop Shipment]                TINYINT          NOT NULL,
    [Transaction Type]             NVARCHAR (10)    NOT NULL,
    [Transport Method]             NVARCHAR (10)    NOT NULL,
    [Country_Region Code]          NVARCHAR (10)    NOT NULL,
    [Entry_Exit Point]             NVARCHAR (10)    NOT NULL,
    [Document Date]                DATETIME         NOT NULL,
    [External Document No_]        NVARCHAR (35)    NULL,
    [Area]                         NVARCHAR (10)    NOT NULL,
    [Transaction Specification]    NVARCHAR (10)    NOT NULL,
    [No_ Series]                   NVARCHAR (10)    NOT NULL,
    [Variant Code]                 NVARCHAR (10)    NOT NULL,
    [Qty_ per Unit of Measure]     DECIMAL (38, 20) NOT NULL,
    [Unit of Measure Code]         NVARCHAR (10)    NOT NULL,
    [Derived from Blanket Order]   TINYINT          NOT NULL,
    [Cross-Reference No_]          NVARCHAR (20)    NOT NULL,
    [Originally Ordered No_]       NVARCHAR (20)    NOT NULL,
    [Originally Ordered Var_ Code] NVARCHAR (10)    NOT NULL,
    [Out-of-Stock Substitution]    TINYINT          NOT NULL,
    [Item Category Code]           NVARCHAR (10)    NOT NULL,
    [Nonstock]                     TINYINT          NOT NULL,
    [Purchasing Code]              NVARCHAR (10)    NOT NULL,
    [Product Group Code]           NVARCHAR (10)    NOT NULL,
    [Completely Invoiced]          TINYINT          NOT NULL,
    [Last Invoice Date]            DATETIME         NOT NULL,
    [Applied Entry to Adjust]      TINYINT          NOT NULL,
    [Correction]                   TINYINT          NOT NULL,
    [Prod_ Order Comp_ Line No_]   INT              NOT NULL,
    [Serial No_]                   NVARCHAR (20)    NOT NULL,
    [Lot No_]                      NVARCHAR (20)    NOT NULL,
    [Warranty Date]                DATETIME         NOT NULL,
    [Expiration Date]              DATETIME         NOT NULL,
    [Return Reason Code]           NVARCHAR (10)    NOT NULL,
    [Bin Code]                     NVARCHAR (20)    NOT NULL,
    [Lot No_ Trading Unit]         NVARCHAR (20)    NOT NULL,
    [Trading Unit No_]             NVARCHAR (20)    NOT NULL,
    [QC Status]                    NVARCHAR (10)    NOT NULL,
    [Document Type]                INT              NOT NULL,
    [Document Line No_]            INT              NOT NULL,
    [Job No_]                      NVARCHAR (20)    NOT NULL,
    [Job Task No_]                 NVARCHAR (20)    NOT NULL,
    [Job Purchase]                 TINYINT          NOT NULL,
    [Shipped Qty_ Not Returned]    DECIMAL (38, 20) NOT NULL,
    [Item Tracking]                INT              NOT NULL,
    [Manufacture Date]             DATETIME         NOT NULL,
    [Order Type]                   INT              NULL,
    [Order No_]                    NVARCHAR (20)    NULL,
    [Order Line No_]               INT              NULL,
    [Dimension Set ID]             INT              NULL,
    [Assemble to Order]            TINYINT          NULL,
    CONSTRAINT [Jason Pharm$Item Ledger Entry$0] PRIMARY KEY CLUSTERED ([Entry No_] ASC)
);


GO
CREATE NONCLUSTERED INDEX [SnapshotNavProductionIndex2]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Entry Type] ASC, [Location Code] ASC, [Item Category Code] ASC, [Product Group Code] ASC, [Posting Date] ASC)
    INCLUDE([Item No_], [Quantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$Item Ledger Entry_idx1]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Posting Date] ASC) WITH (FILLFACTOR = 90);


GO
CREATE STATISTICS [Applied Entry to Adjust]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Applied Entry to Adjust]);


GO
CREATE STATISTICS [Document Line No_]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Document Line No_]);


GO
CREATE STATISTICS [Document Type]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Document Type]);


GO
CREATE STATISTICS [Drop Shipment]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Drop Shipment]);


GO
CREATE STATISTICS [Expiration Date]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Expiration Date]);


GO
CREATE STATISTICS [Open]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Open]);


GO
CREATE STATISTICS [Positive]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Positive]);


GO
CREATE STATISTICS [Posting Date]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Posting Date]);


GO
CREATE STATISTICS [Prod_ Order Comp_ Line No_]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Prod_ Order Comp_ Line No_]);


GO
CREATE STATISTICS [Source Type]
    ON [dbo].[Jason Pharm$Item Ledger Entry]([Source Type]);

