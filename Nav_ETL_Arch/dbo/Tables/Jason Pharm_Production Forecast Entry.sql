﻿CREATE TABLE [dbo].[Jason Pharm$Production Forecast Entry] (
    [Entry No_]                INT              NOT NULL,
    [Production Forecast Name] NVARCHAR (10)    NULL,
    [Item No_]                 NVARCHAR (20)    NULL,
    [Forecast Date]            DATETIME         NULL,
    [Forecast Quantity]        NUMERIC (38, 20) NULL,
    [Unit of Measure Code]     NVARCHAR (10)    NULL,
    [Qty_ per Unit of Measure] NUMERIC (38, 20) NULL,
    [Forecast Quantity (Base)] NUMERIC (38, 20) NULL,
    [Location Code]            NVARCHAR (10)    NULL,
    [Component Forecast]       TINYINT          NULL,
    [Description]              NVARCHAR (50)    NULL,
    [Exclusion]                TINYINT          NULL,
    [EntryDate]                DATETIME         NULL
);

