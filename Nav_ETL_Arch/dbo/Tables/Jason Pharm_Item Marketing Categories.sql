﻿CREATE TABLE [dbo].[Jason Pharm$Item Marketing Categories] (
    [timestamp]        ROWVERSION    NOT NULL,
    [Item No_]         NVARCHAR (20) NOT NULL,
    [MKTG Type]        NVARCHAR (20) NOT NULL,
    [MKTG Category]    NVARCHAR (20) NOT NULL,
    [MKTG Subcategory] NVARCHAR (20) NOT NULL,
    [Launch Date]      DATETIME      NOT NULL,
    [MKTG Status]      INT           NOT NULL,
    CONSTRAINT [Jason Pharm$Item Marketing Categories$0] PRIMARY KEY CLUSTERED ([Item No_] ASC) WITH (FILLFACTOR = 85) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

