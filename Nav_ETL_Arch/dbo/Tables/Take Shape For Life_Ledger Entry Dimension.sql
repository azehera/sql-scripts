﻿CREATE TABLE [dbo].[Take Shape For Life$Ledger Entry Dimension] (
    [timestamp]            ROWVERSION    NOT NULL,
    [Table ID]             INT           NOT NULL,
    [Entry No_]            INT           NOT NULL,
    [Dimension Code]       NVARCHAR (20) NOT NULL,
    [Dimension Value Code] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_Take Shape For Life$Ledger Entry Dimension] PRIMARY KEY CLUSTERED ([timestamp] ASC) WITH (FILLFACTOR = 80)
);

