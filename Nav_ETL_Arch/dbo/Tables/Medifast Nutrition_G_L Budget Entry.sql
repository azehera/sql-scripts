﻿CREATE TABLE [dbo].[Medifast Nutrition$G_L Budget Entry] (
    [Entry No_]               INT              NOT NULL,
    [Budget Name]             NVARCHAR (10)    NOT NULL,
    [G_L Account No_]         NVARCHAR (20)    NOT NULL,
    [Date]                    DATETIME         NOT NULL,
    [Global Dimension 1 Code] NVARCHAR (20)    NOT NULL,
    [Global Dimension 2 Code] NVARCHAR (20)    NOT NULL,
    [Amount]                  DECIMAL (38, 20) NOT NULL,
    [Description]             NVARCHAR (50)    NOT NULL,
    [Business Unit Code]      NVARCHAR (10)    NOT NULL,
    [User ID]                 NVARCHAR (50)    NULL,
    [Budget Dimension 1 Code] NVARCHAR (20)    NOT NULL,
    [Budget Dimension 2 Code] NVARCHAR (20)    NOT NULL,
    [Budget Dimension 3 Code] NVARCHAR (20)    NOT NULL,
    [Budget Dimension 4 Code] NVARCHAR (20)    NOT NULL,
    [Last Date Modified]      DATETIME         NULL,
    [Dimension Set ID]        INT              NULL,
    CONSTRAINT [Medifast Nutrition$G_L Budget Entry$0] PRIMARY KEY CLUSTERED ([Entry No_] ASC) WITH (FILLFACTOR = 90) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

