﻿CREATE TABLE [dbo].[Windows Login] (
    [timestamp] ROWVERSION     NOT NULL,
    [SID]       NVARCHAR (119) NOT NULL,
    CONSTRAINT [PK_Windows Login] PRIMARY KEY CLUSTERED ([timestamp] ASC) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

