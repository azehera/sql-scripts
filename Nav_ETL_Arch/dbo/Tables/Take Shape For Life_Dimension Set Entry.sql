﻿CREATE TABLE [dbo].[Take Shape For Life$Dimension Set Entry] (
    [Dimension Set ID]     INT           NOT NULL,
    [Dimension Code]       NVARCHAR (20) NOT NULL,
    [Dimension Value Code] NVARCHAR (20) NOT NULL,
    [Dimension Value ID]   INT           NOT NULL
) ON [Data Filegroup 1];

