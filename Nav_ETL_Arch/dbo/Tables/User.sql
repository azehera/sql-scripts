﻿CREATE TABLE [dbo].[User] (
    [timestamp]       ROWVERSION    NOT NULL,
    [User ID]         NVARCHAR (20) NOT NULL,
    [Password]        NVARCHAR (30) NOT NULL,
    [Name]            NVARCHAR (30) NOT NULL,
    [Expiration Date] DATETIME      NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([timestamp] ASC) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

