﻿CREATE TABLE [dbo].[Jason Pharm$Coupon_Promotion] (
    [timestamp]                    ROWVERSION       NULL,
    [Coupon_Promotion Code]        NVARCHAR (10)    NOT NULL,
    [Customer Posting Group]       NVARCHAR (10)    NOT NULL,
    [Start Date]                   DATETIME         NOT NULL,
    [End Date]                     DATETIME         NOT NULL,
    [Discount Amount]              DECIMAL (38, 20) NOT NULL,
    [Amount over]                  DECIMAL (38, 20) NOT NULL,
    [Discount Percent]             DECIMAL (38, 20) NOT NULL,
    [Allow Mulitple Use]           TINYINT          NOT NULL,
    [Group Code]                   NVARCHAR (10)    NOT NULL,
    [Escalate Coupon Ledger Entry] TINYINT          NOT NULL,
    [Non-commissionable]           TINYINT          NULL
) ON [Data Filegroup 1];

