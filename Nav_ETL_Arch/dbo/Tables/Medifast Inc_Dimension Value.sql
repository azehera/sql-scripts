﻿CREATE TABLE [dbo].[Medifast Inc$Dimension Value] (
    [Dimension Code]                 NVARCHAR (20)    NULL,
    [Code]                           NVARCHAR (20)    NULL,
    [Name]                           NVARCHAR (50)    NULL,
    [Dimension Value Type]           INT              NULL,
    [Totaling]                       NVARCHAR (250)   NULL,
    [Blocked]                        TINYINT          NULL,
    [Consolidation Code]             NVARCHAR (20)    NULL,
    [Indentation]                    INT              NULL,
    [Global Dimension No_]           INT              NULL,
    [Map-to IC Dimension Code]       NVARCHAR (20)    NULL,
    [Map-to IC Dimension Value Code] NVARCHAR (20)    NULL,
    [Commission Rate %]              NUMERIC (38, 20) NULL,
    [Dim_ Total]                     NUMERIC (38, 20) NULL,
    [Dimension Value ID]             INT              NULL
);

