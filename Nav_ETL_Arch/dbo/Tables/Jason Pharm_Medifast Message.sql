﻿CREATE TABLE [dbo].[Jason Pharm$Medifast Message] (
    [Entry No_]    INT            NULL,
    [Entry Code]   NVARCHAR (10)  NULL,
    [Message Text] NVARCHAR (250) NULL,
    [Entry Date]   DATETIME       NULL,
    [Entry Time]   DATETIME       NULL,
    [User ID]      NVARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$Medifast Message_Entry Code_Entry Date]
    ON [dbo].[Jason Pharm$Medifast Message]([Entry Code] ASC, [Entry Date] ASC) WITH (FILLFACTOR = 90);

