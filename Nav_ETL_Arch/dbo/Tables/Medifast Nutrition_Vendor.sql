﻿CREATE TABLE [dbo].[Medifast Nutrition$Vendor] (
    [No_]                            NVARCHAR (20)    NOT NULL,
    [Name]                           NVARCHAR (50)    NOT NULL,
    [Search Name]                    NVARCHAR (50)    NOT NULL,
    [Name 2]                         NVARCHAR (50)    NOT NULL,
    [Address]                        NVARCHAR (50)    NOT NULL,
    [Address 2]                      NVARCHAR (50)    NOT NULL,
    [City]                           NVARCHAR (30)    NOT NULL,
    [Contact]                        NVARCHAR (50)    NOT NULL,
    [Phone No_]                      NVARCHAR (30)    NOT NULL,
    [Telex No_]                      NVARCHAR (20)    NOT NULL,
    [Our Account No_]                NVARCHAR (20)    NOT NULL,
    [Territory Code]                 NVARCHAR (10)    NOT NULL,
    [Global Dimension 1 Code]        NVARCHAR (20)    NOT NULL,
    [Global Dimension 2 Code]        NVARCHAR (20)    NOT NULL,
    [Budgeted Amount]                DECIMAL (38, 20) NOT NULL,
    [Vendor Posting Group]           NVARCHAR (10)    NOT NULL,
    [Currency Code]                  NVARCHAR (10)    NOT NULL,
    [Language Code]                  NVARCHAR (10)    NOT NULL,
    [Statistics Group]               INT              NOT NULL,
    [Payment Terms Code]             NVARCHAR (10)    NOT NULL,
    [Fin_ Charge Terms Code]         NVARCHAR (10)    NOT NULL,
    [Purchaser Code]                 NVARCHAR (10)    NOT NULL,
    [Shipment Method Code]           NVARCHAR (10)    NOT NULL,
    [Shipping Agent Code]            NVARCHAR (10)    NOT NULL,
    [Invoice Disc_ Code]             NVARCHAR (20)    NOT NULL,
    [Country_Region Code]            NVARCHAR (10)    NOT NULL,
    [Blocked]                        INT              NOT NULL,
    [Pay-to Vendor No_]              NVARCHAR (20)    NOT NULL,
    [Priority]                       INT              NOT NULL,
    [Payment Method Code]            NVARCHAR (10)    NOT NULL,
    [Last Date Modified]             DATETIME         NOT NULL,
    [Application Method]             INT              NOT NULL,
    [Prices Including VAT]           TINYINT          NOT NULL,
    [Fax No_]                        NVARCHAR (30)    NOT NULL,
    [Telex Answer Back]              NVARCHAR (20)    NOT NULL,
    [VAT Registration No_]           NVARCHAR (20)    NOT NULL,
    [Gen_ Bus_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Picture]                        IMAGE            NULL,
    [Post Code]                      NVARCHAR (20)    NOT NULL,
    [County]                         NVARCHAR (30)    NOT NULL,
    [E-Mail]                         NVARCHAR (80)    NOT NULL,
    [Home Page]                      NVARCHAR (80)    NOT NULL,
    [No_ Series]                     NVARCHAR (10)    NOT NULL,
    [Tax Area Code]                  NVARCHAR (20)    NOT NULL,
    [Tax Liable]                     TINYINT          NOT NULL,
    [VAT Bus_ Posting Group]         NVARCHAR (10)    NOT NULL,
    [Block Payment Tolerance]        TINYINT          NOT NULL,
    [IC Partner Code]                NVARCHAR (20)    NOT NULL,
    [Prepayment %]                   DECIMAL (38, 20) NOT NULL,
    [Primary Contact No_]            NVARCHAR (20)    NOT NULL,
    [Responsibility Center]          NVARCHAR (10)    NOT NULL,
    [Location Code]                  NVARCHAR (10)    NOT NULL,
    [Lead Time Calculation]          VARCHAR (32)     NULL,
    [Base Calendar Code]             NVARCHAR (10)    NOT NULL,
    [UPS Zone]                       NVARCHAR (2)     NOT NULL,
    [Federal ID No_]                 NVARCHAR (30)    NOT NULL,
    [Bank Communication]             INT              NOT NULL,
    [Check Date Format]              INT              NOT NULL,
    [Check Date Separator]           INT              NOT NULL,
    [IRS 1099 Code]                  NVARCHAR (10)    NOT NULL,
    [Tax Identification Type]        INT              NOT NULL,
    [Certificate of Liability Date]  DATETIME         NOT NULL,
    [Business Name]                  NVARCHAR (100)   NOT NULL,
    [Encrypted Federal ID No_]       NVARCHAR (250)   NOT NULL,
    [Purch_ Order E-Mail]            NVARCHAR (80)    NULL,
    [GLN]                            NVARCHAR (13)    NULL,
    [Partner Type]                   INT              NULL,
    [Creditor No_]                   NVARCHAR (20)    NULL,
    [Preferred Bank Account]         NVARCHAR (10)    NULL,
    [Cash Flow Payment Terms Code]   NVARCHAR (10)    NULL,
    [RFC No_]                        NVARCHAR (13)    NULL,
    [CURP No_]                       NVARCHAR (18)    NULL,
    [State Inscription]              NVARCHAR (30)    NULL,
    [FATCA filing requirement]       TINYINT          NULL,
    [EDI Order]                      TINYINT          NULL,
    [Purch_ Pmt_ Advice Trade Ptnr_] NVARCHAR (20)    NULL,
    [E-Ship Agent Service]           NVARCHAR (30)    NULL,
    [Residential Delivery]           TINYINT          NULL,
    [IRS EIN Number]                 NVARCHAR (15)    NULL,
    [Shipping Payment Type]          INT              NULL,
    [Shipping Insurance]             INT              NULL,
    [External No_]                   NVARCHAR (20)    NULL,
    [E-Mail Rule Code]               NVARCHAR (10)    NULL,
    [Use E-Mail Rule for Order Addr] TINYINT          NULL,
    [Fast Pay Discount]              TINYINT          NULL
) ON [Data Filegroup 1] TEXTIMAGE_ON [Data Filegroup 1];

