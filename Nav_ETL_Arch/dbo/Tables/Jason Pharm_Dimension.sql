﻿CREATE TABLE [dbo].[Jason Pharm$Dimension] (
    [timestamp]                ROWVERSION    NULL,
    [Code]                     NVARCHAR (20) NOT NULL,
    [Name]                     NVARCHAR (30) NOT NULL,
    [Code Caption]             NVARCHAR (80) NULL,
    [Filter Caption]           NVARCHAR (80) NULL,
    [Description]              NVARCHAR (50) NOT NULL,
    [Blocked]                  TINYINT       NOT NULL,
    [Consolidation Code]       NVARCHAR (20) NOT NULL,
    [Map-to IC Dimension Code] NVARCHAR (20) NOT NULL
) ON [Data Filegroup 1];

