﻿CREATE TABLE [dbo].[Jason Pharm$Gen_ Business Posting Group] (
    [timestamp]                   ROWVERSION    NULL,
    [Code]                        NVARCHAR (10) NOT NULL,
    [Description]                 NVARCHAR (50) NOT NULL,
    [Def_ VAT Bus_ Posting Group] NVARCHAR (10) NOT NULL,
    [Auto Insert Default]         TINYINT       NOT NULL,
    [Picture]                     IMAGE         NULL,
    [Address 1]                   NVARCHAR (30) NOT NULL,
    [Address 2]                   NVARCHAR (30) NOT NULL,
    [Address 3]                   NVARCHAR (30) NOT NULL,
    [Address 4]                   NVARCHAR (30) NOT NULL,
    [Return Phone No_]            NVARCHAR (30) NOT NULL,
    [New Cust Info Packet No_]    NVARCHAR (20) NOT NULL,
    [Avalara Company ID]          NVARCHAR (25) NOT NULL
) ON [Data Filegroup 1] TEXTIMAGE_ON [Data Filegroup 1];

