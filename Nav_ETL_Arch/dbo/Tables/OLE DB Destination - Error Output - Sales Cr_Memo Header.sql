﻿CREATE TABLE [dbo].[OLE DB Destination - Error Output - Sales Cr_Memo Header] (
    [No_]                         NVARCHAR (20)    NULL,
    [Sell-to Customer No_]        NVARCHAR (20)    NULL,
    [Bill-to Customer No_]        NVARCHAR (20)    NULL,
    [Bill-to Name]                NVARCHAR (30)    NULL,
    [Bill-to Name 2]              NVARCHAR (30)    NULL,
    [Bill-to Address]             NVARCHAR (30)    NULL,
    [Bill-to Address 2]           NVARCHAR (30)    NULL,
    [Bill-to City]                NVARCHAR (30)    NULL,
    [Bill-to Contact]             NVARCHAR (30)    NULL,
    [Your Reference]              NVARCHAR (30)    NULL,
    [Ship-to Code]                NVARCHAR (10)    NULL,
    [Ship-to Name]                NVARCHAR (30)    NULL,
    [Ship-to Name 2]              NVARCHAR (30)    NULL,
    [Ship-to Address]             NVARCHAR (30)    NULL,
    [Ship-to Address 2]           NVARCHAR (30)    NULL,
    [Ship-to City]                NVARCHAR (30)    NULL,
    [Ship-to Contact]             NVARCHAR (30)    NULL,
    [Posting Date]                DATETIME         NULL,
    [Shipment Date]               DATETIME         NULL,
    [Posting Description]         NVARCHAR (50)    NULL,
    [Payment Terms Code]          NVARCHAR (10)    NULL,
    [Due Date]                    DATETIME         NULL,
    [Payment Discount %]          NUMERIC (38, 20) NULL,
    [Pmt_ Discount Date]          DATETIME         NULL,
    [Shipment Method Code]        NVARCHAR (10)    NULL,
    [Location Code]               NVARCHAR (10)    NULL,
    [Shortcut Dimension 1 Code]   NVARCHAR (20)    NULL,
    [Shortcut Dimension 2 Code]   NVARCHAR (20)    NULL,
    [Customer Posting Group]      NVARCHAR (10)    NULL,
    [Currency Code]               NVARCHAR (10)    NULL,
    [Currency Factor]             NUMERIC (38, 20) NULL,
    [Customer Price Group]        NVARCHAR (10)    NULL,
    [Prices Including VAT]        TINYINT          NULL,
    [Invoice Disc_ Code]          NVARCHAR (20)    NULL,
    [Customer Disc_ Group]        NVARCHAR (10)    NULL,
    [Language Code]               NVARCHAR (10)    NULL,
    [Salesperson Code]            NVARCHAR (10)    NULL,
    [No_ Printed]                 INT              NULL,
    [On Hold]                     NVARCHAR (3)     NULL,
    [Applies-to Doc_ Type]        INT              NULL,
    [Applies-to Doc_ No_]         NVARCHAR (20)    NULL,
    [Bal_ Account No_]            NVARCHAR (20)    NULL,
    [VAT Registration No_]        NVARCHAR (20)    NULL,
    [Reason Code]                 NVARCHAR (10)    NULL,
    [Gen_ Bus_ Posting Group]     NVARCHAR (10)    NULL,
    [EU 3-Party Trade]            TINYINT          NULL,
    [Transaction Type]            NVARCHAR (10)    NULL,
    [Transport Method]            NVARCHAR (10)    NULL,
    [VAT Country_Region Code]     NVARCHAR (1)     NULL,
    [Sell-to Customer Name]       NVARCHAR (30)    NULL,
    [Sell-to Customer Name 2]     NVARCHAR (30)    NULL,
    [Sell-to Address]             NVARCHAR (30)    NULL,
    [Sell-to Address 2]           NVARCHAR (30)    NULL,
    [Sell-to City]                NVARCHAR (30)    NULL,
    [Sell-to Contact]             NVARCHAR (30)    NULL,
    [Bill-to Post Code]           NVARCHAR (20)    NULL,
    [Bill-to County]              NVARCHAR (30)    NULL,
    [Bill-to Country_Region Code] NVARCHAR (1)     NULL,
    [Sell-to Post Code]           NVARCHAR (20)    NULL,
    [Sell-to County]              NVARCHAR (30)    NULL,
    [Sell-to Country_Region Code] NVARCHAR (1)     NULL,
    [Ship-to Post Code]           NVARCHAR (20)    NULL,
    [Ship-to County]              NVARCHAR (30)    NULL,
    [Ship-to Country_Region Code] NVARCHAR (1)     NULL,
    [Bal_ Account Type]           INT              NULL,
    [Exit Point]                  NVARCHAR (10)    NULL,
    [Correction]                  TINYINT          NULL,
    [Document Date]               DATETIME         NULL,
    [External Document No_]       NVARCHAR (20)    NULL,
    [Area]                        NVARCHAR (10)    NULL,
    [Transaction Specification]   NVARCHAR (10)    NULL,
    [Payment Method Code]         NVARCHAR (10)    NULL,
    [Pre-Assigned No_ Series]     NVARCHAR (10)    NULL,
    [No_ Series]                  NVARCHAR (10)    NULL,
    [Pre-Assigned No_]            NVARCHAR (20)    NULL,
    [User ID]                     NVARCHAR (20)    NULL,
    [Source Code]                 NVARCHAR (10)    NULL,
    [Tax Area Code]               NVARCHAR (20)    NULL,
    [Tax Liable]                  TINYINT          NULL,
    [VAT Bus_ Posting Group]      NVARCHAR (10)    NULL,
    [VAT Base Discount %]         NUMERIC (38, 20) NULL,
    [Campaign No_]                NVARCHAR (20)    NULL,
    [Sell-to Contact No_]         NVARCHAR (20)    NULL,
    [Bill-to Contact No_]         NVARCHAR (20)    NULL,
    [Responsibility Center]       NVARCHAR (10)    NULL,
    [Service Mgt_ Document]       TINYINT          NULL,
    [Return Order No_]            NVARCHAR (20)    NULL,
    [Return Order No_ Series]     NVARCHAR (10)    NULL,
    [Allow Line Disc_]            TINYINT          NULL,
    [Get Return Receipt Used]     TINYINT          NULL,
    [Ship-to UPS Zone]            NVARCHAR (2)     NULL,
    [Tax Exemption No_]           NVARCHAR (30)    NULL,
    [Salesperson 2]               NVARCHAR (10)    NULL,
    [Customer Service Rep_]       NVARCHAR (20)    NULL,
    [Order Time]                  DATETIME         NULL,
    [Package Code]                NVARCHAR (20)    NULL,
    [No_ of Packages]             INT              NULL,
    [Date Sent]                   DATETIME         NULL,
    [Time Sent]                   DATETIME         NULL,
    [BizTalk Sales Credit Memo]   TINYINT          NULL,
    [BizTalk Document Sent]       TINYINT          NULL,
    [Prepmt_ Cr_ Memo No_ Series] NVARCHAR (1)     NULL,
    [Prepayment Credit Memo]      NVARCHAR (1)     NULL,
    [Prepayment Order No_]        NVARCHAR (1)     NULL,
    [STE Transaction ID]          NVARCHAR (1)     NULL,
    [Avalara Status Code]         NVARCHAR (1)     NULL,
    [Leads Order]                 NVARCHAR (1)     NULL,
    [Escalate Order]              NVARCHAR (1)     NULL,
    [Amount Authorized]           NVARCHAR (1)     NULL,
    [Order Cancelled]             NVARCHAR (1)     NULL,
    [Authorization No_]           NVARCHAR (1)     NULL,
    [Escalate Original Order No_] NVARCHAR (1)     NULL,
    [ErrorCode]                   INT              NULL,
    [ErrorColumn]                 INT              NULL
);

