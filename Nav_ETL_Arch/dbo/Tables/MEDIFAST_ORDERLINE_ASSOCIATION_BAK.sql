﻿CREATE TABLE [dbo].[MEDIFAST_ORDERLINE_ASSOCIATION_BAK] (
    [MOA_ID]                BIGINT         NOT NULL,
    [MOA_SEQ]               INT            NOT NULL,
    [MOA_ASSOC_SEQ]         INT            NOT NULL,
    [MOA_ORH_ID]            BIGINT         NOT NULL,
    [MOA_ORDER_NBR]         NVARCHAR (40)  NULL,
    [MOA_ORL_ID]            BIGINT         NULL,
    [MOA_SKU_ID]            BIGINT         NOT NULL,
    [MOA_SKU]               NVARCHAR (128) NOT NULL,
    [MOA_ASSOC_SKU]         NVARCHAR (128) NOT NULL,
    [MOA_SKU_UOM]           NVARCHAR (40)  NULL,
    [MOA_ASSOC_SKU_UOM]     NVARCHAR (40)  NULL,
    [MOA_QTY]               INT            NULL,
    [MOA_ASSOC_QTY]         INT            NULL,
    [MOA_ORDER_TYPE]        NVARCHAR (40)  NULL,
    [MOA_ASSOC_TYPE]        NVARCHAR (40)  NULL,
    [MOA_TAX_RATE]          FLOAT (53)     NULL,
    [MOA_TAX_AMT]           MONEY          NULL,
    [MOA_SALE_PRICE_AMT]    MONEY          NULL,
    [MOA_EXT_PRICE_AMT]     MONEY          NULL,
    [MOA_LIST_PRICE_AMT]    MONEY          NULL,
    [MOA_SYS_TIMESTAMP]     DATETIME       NOT NULL,
    [MOA_REWARD_EARNED_AMT] MONEY          NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_MOA_ORDER_NBR]
    ON [dbo].[MEDIFAST_ORDERLINE_ASSOCIATION_BAK]([MOA_ORDER_NBR] ASC) WITH (FILLFACTOR = 90);

