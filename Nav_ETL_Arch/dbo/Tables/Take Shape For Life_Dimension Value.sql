﻿CREATE TABLE [dbo].[Take Shape For Life$Dimension Value] (
    [timestamp]                      ROWVERSION       NOT NULL,
    [Dimension Code]                 NVARCHAR (20)    NOT NULL,
    [Code]                           NVARCHAR (20)    NOT NULL,
    [Name]                           NVARCHAR (50)    NOT NULL,
    [Dimension Value Type]           INT              NOT NULL,
    [Totaling]                       NVARCHAR (250)   NOT NULL,
    [Blocked]                        TINYINT          NOT NULL,
    [Consolidation Code]             NVARCHAR (20)    NOT NULL,
    [Indentation]                    INT              NOT NULL,
    [Global Dimension No_]           INT              NOT NULL,
    [Map-to IC Dimension Code]       NVARCHAR (20)    NOT NULL,
    [Map-to IC Dimension Value Code] NVARCHAR (20)    NOT NULL,
    [Commission Rate %]              NUMERIC (38, 20) NOT NULL,
    [Dim_ Total]                     NUMERIC (38, 20) NOT NULL,
    [Dimension Value ID]             INT              NULL,
    CONSTRAINT [PK_Take Shape For Life$Dimension Value] PRIMARY KEY CLUSTERED ([timestamp] ASC) WITH (FILLFACTOR = 80)
);

