﻿CREATE TABLE [dbo].[MEDIFAST_ORDERLINE_ASSOCIATION] (
    [MOA_ID]                BIGINT         NOT NULL,
    [MOA_SEQ]               INT            NULL,
    [MOA_ASSOC_SEQ]         INT            NULL,
    [MOA_ORH_ID]            BIGINT         NULL,
    [MOA_ORDER_NBR]         NVARCHAR (40)  NULL,
    [MOA_ORL_ID]            BIGINT         NULL,
    [MOA_SKU_ID]            BIGINT         NULL,
    [MOA_SKU]               NVARCHAR (128) NOT NULL,
    [MOA_ASSOC_SKU]         NVARCHAR (128) NOT NULL,
    [MOA_SKU_UOM]           NVARCHAR (40)  NULL,
    [MOA_ASSOC_SKU_UOM]     NVARCHAR (40)  NULL,
    [MOA_QTY]               INT            NULL,
    [MOA_ASSOC_QTY]         INT            NULL,
    [MOA_ORDER_TYPE]        NVARCHAR (40)  NULL,
    [MOA_ASSOC_TYPE]        NVARCHAR (40)  NULL,
    [MOA_TAX_RATE]          FLOAT (53)     NULL,
    [MOA_TAX_AMT]           MONEY          NULL,
    [MOA_SALE_PRICE_AMT]    MONEY          NULL,
    [MOA_EXT_PRICE_AMT]     MONEY          NULL,
    [MOA_LIST_PRICE_AMT]    MONEY          NULL,
    [MOA_SYS_TIMESTAMP]     DATETIME       CONSTRAINT [DF__MEDIFAST___MOA_S__464936E5] DEFAULT (getdate()) NOT NULL,
    [MOA_REWARD_EARNED_AMT] MONEY          NULL,
    [DocumentNo]            VARCHAR (20)   NULL,
    CONSTRAINT [PK_MEDIFAST_ORDERLINE_ASSOCIATION] PRIMARY KEY CLUSTERED ([MOA_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_TS]
    ON [dbo].[MEDIFAST_ORDERLINE_ASSOCIATION]([MOA_SYS_TIMESTAMP] ASC)
    INCLUDE([MOA_ORL_ID], [MOA_SKU], [MOA_ASSOC_SKU], [MOA_ASSOC_SKU_UOM], [MOA_ASSOC_QTY], [DocumentNo]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_MEDIFAST_ORDERLINE_ASSOCIATION_MOA_ORDER_NBR]
    ON [dbo].[MEDIFAST_ORDERLINE_ASSOCIATION]([MOA_ORDER_NBR] ASC)
    INCLUDE([MOA_ID]) WITH (FILLFACTOR = 90);

