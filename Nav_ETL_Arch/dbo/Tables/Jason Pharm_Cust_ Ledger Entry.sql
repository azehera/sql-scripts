﻿CREATE TABLE [dbo].[Jason Pharm$Cust_ Ledger Entry] (
    [Entry No_]                     INT              NOT NULL,
    [Customer No_]                  NVARCHAR (20)    NOT NULL,
    [Posting Date]                  DATETIME         NOT NULL,
    [Document Type]                 INT              NOT NULL,
    [Document No_]                  NVARCHAR (20)    NOT NULL,
    [Description]                   NVARCHAR (50)    NOT NULL,
    [Currency Code]                 NVARCHAR (10)    NOT NULL,
    [Sales (LCY)]                   DECIMAL (38, 20) NOT NULL,
    [Profit (LCY)]                  DECIMAL (38, 20) NOT NULL,
    [Inv_ Discount (LCY)]           DECIMAL (38, 20) NOT NULL,
    [Sell-to Customer No_]          NVARCHAR (20)    NOT NULL,
    [Customer Posting Group]        NVARCHAR (10)    NOT NULL,
    [Global Dimension 1 Code]       NVARCHAR (20)    NOT NULL,
    [Global Dimension 2 Code]       NVARCHAR (20)    NOT NULL,
    [Salesperson Code]              NVARCHAR (10)    NOT NULL,
    [User ID]                       NVARCHAR (50)    NULL,
    [Source Code]                   NVARCHAR (10)    NOT NULL,
    [On Hold]                       NVARCHAR (3)     NOT NULL,
    [Applies-to Doc_ Type]          INT              NOT NULL,
    [Applies-to Doc_ No_]           NVARCHAR (20)    NOT NULL,
    [Open]                          TINYINT          NOT NULL,
    [Due Date]                      DATETIME         NOT NULL,
    [Pmt_ Discount Date]            DATETIME         NOT NULL,
    [Original Pmt_ Disc_ Possible]  DECIMAL (38, 20) NOT NULL,
    [Pmt_ Disc_ Given (LCY)]        DECIMAL (38, 20) NOT NULL,
    [Positive]                      TINYINT          NOT NULL,
    [Closed by Entry No_]           INT              NOT NULL,
    [Closed at Date]                DATETIME         NOT NULL,
    [Closed by Amount]              DECIMAL (38, 20) NOT NULL,
    [Applies-to ID]                 NVARCHAR (20)    NOT NULL,
    [Journal Batch Name]            NVARCHAR (10)    NOT NULL,
    [Reason Code]                   NVARCHAR (10)    NOT NULL,
    [Bal_ Account Type]             INT              NOT NULL,
    [Bal_ Account No_]              NVARCHAR (20)    NOT NULL,
    [Transaction No_]               INT              NOT NULL,
    [Closed by Amount (LCY)]        DECIMAL (38, 20) NOT NULL,
    [Document Date]                 DATETIME         NOT NULL,
    [External Document No_]         NVARCHAR (35)    NULL,
    [Calculate Interest]            TINYINT          NOT NULL,
    [Closing Interest Calculated]   TINYINT          NOT NULL,
    [No_ Series]                    NVARCHAR (10)    NOT NULL,
    [Closed by Currency Code]       NVARCHAR (10)    NOT NULL,
    [Closed by Currency Amount]     DECIMAL (38, 20) NOT NULL,
    [Adjusted Currency Factor]      DECIMAL (38, 20) NOT NULL,
    [Original Currency Factor]      DECIMAL (38, 20) NOT NULL,
    [Remaining Pmt_ Disc_ Possible] DECIMAL (38, 20) NOT NULL,
    [Pmt_ Disc_ Tolerance Date]     DATETIME         NOT NULL,
    [Max_ Payment Tolerance]        DECIMAL (38, 20) NOT NULL,
    [Last Issued Reminder Level]    INT              NOT NULL,
    [Accepted Payment Tolerance]    DECIMAL (38, 20) NOT NULL,
    [Accepted Pmt_ Disc_ Tolerance] TINYINT          NOT NULL,
    [Pmt_ Tolerance (LCY)]          DECIMAL (38, 20) NOT NULL,
    [Amount to Apply]               DECIMAL (38, 20) NOT NULL,
    [IC Partner Code]               NVARCHAR (20)    NOT NULL,
    [Applying Entry]                TINYINT          NOT NULL,
    [Reversed]                      TINYINT          NOT NULL,
    [Reversed by Entry No_]         INT              NOT NULL,
    [Reversed Entry No_]            INT              NOT NULL,
    [Salesperson 2]                 NVARCHAR (10)    NOT NULL,
    [Prepayment]                    TINYINT          NOT NULL,
    [Payment Method Code]           NVARCHAR (10)    NULL,
    [Applies-to Ext_ Doc_ No_]      NVARCHAR (35)    NULL,
    [Recipient Bank Account]        NVARCHAR (10)    NULL,
    [Message to Recipient]          NVARCHAR (140)   NULL,
    [Exported to Payment File]      TINYINT          NULL,
    [Dimension Set ID]              INT              NULL,
    [Direct Debit Mandate ID]       NVARCHAR (35)    NULL,
    CONSTRAINT [PK_Jason Pharm$Cust_ Ledger Entry] PRIMARY KEY CLUSTERED ([Entry No_] ASC) ON [Data Filegroup 1]
) ON [Data Filegroup 1];


GO
CREATE STATISTICS [Document Type]
    ON [dbo].[Jason Pharm$Cust_ Ledger Entry]([Document Type]);


GO
CREATE STATISTICS [Due Date]
    ON [dbo].[Jason Pharm$Cust_ Ledger Entry]([Due Date]);


GO
CREATE STATISTICS [Positive]
    ON [dbo].[Jason Pharm$Cust_ Ledger Entry]([Positive]);


GO
CREATE STATISTICS [Posting Date]
    ON [dbo].[Jason Pharm$Cust_ Ledger Entry]([Posting Date]);

