﻿CREATE TABLE [dbo].[Adjustment Entry] (
    [Entry No_]         INT              NOT NULL,
    [Document No_]      NVARCHAR (20)    NOT NULL,
    [Adjustment Type]   NVARCHAR (20)    NOT NULL,
    [Adjustment Code]   NVARCHAR (20)    NOT NULL,
    [Adjustment Amount] DECIMAL (38, 20) NOT NULL,
    [Order Created]     TINYINT          NOT NULL,
    CONSTRAINT [Adjustment Entry$0] PRIMARY KEY CLUSTERED ([Entry No_] ASC) WITH (FILLFACTOR = 92) ON [Data Filegroup 1]
) ON [Data Filegroup 1];

