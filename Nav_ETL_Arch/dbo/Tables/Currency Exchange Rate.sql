﻿CREATE TABLE [dbo].[Currency Exchange Rate] (
    [CurExhRateID]  INT             IDENTITY (1, 1) NOT NULL,
    [Insert Date]   SMALLDATETIME   CONSTRAINT [DF_Currency Exchange Rate_Date] DEFAULT (dateadd(day,datediff(day,'19000101',getdate()),'19000101')) NULL,
    [Currency Code] NVARCHAR (3)    NULL,
    [Exchange Rate] DECIMAL (18, 6) NULL,
    CONSTRAINT [PK_Currency Exchange Rate] PRIMARY KEY CLUSTERED ([CurExhRateID] ASC) WITH (FILLFACTOR = 80)
);

