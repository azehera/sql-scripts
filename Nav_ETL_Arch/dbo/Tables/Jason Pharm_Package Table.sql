﻿CREATE TABLE [dbo].[Jason Pharm$Package Table] (
    [Document No_]         NVARCHAR (20)    NOT NULL,
    [Package Tracking No_] NVARCHAR (40)    NOT NULL,
    [Line No_]             INT              NOT NULL,
    [Document Type]        INT              NULL,
    [Weight]               NUMERIC (38, 20) NULL,
    [Freight Charge]       NUMERIC (38, 20) NULL,
    [Status]               NVARCHAR (20)    NULL,
    [Ship Date]            DATETIME         NULL,
    [Shipping Agent]       NVARCHAR (20)    NULL,
    [Mode]                 NVARCHAR (20)    NULL,
    [Zone]                 NVARCHAR (3)     NULL,
    [Sales Order No_]      NVARCHAR (20)    NULL,
    CONSTRAINT [PK_Jason Pharm$Package Table] PRIMARY KEY CLUSTERED ([Document No_] ASC, [Package Tracking No_] ASC, [Line No_] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$Package Table_Sales Order No_]
    ON [dbo].[Jason Pharm$Package Table]([Sales Order No_] ASC)
    INCLUDE([Package Tracking No_], [Shipping Agent]) WITH (FILLFACTOR = 90);

