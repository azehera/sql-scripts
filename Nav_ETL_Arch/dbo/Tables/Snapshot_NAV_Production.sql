﻿CREATE TABLE [dbo].[Snapshot$NAV_Production] (
    [EntryNo]                      INT             IDENTITY (1, 1) NOT NULL,
    [RunDate]                      DATETIME        NULL,
    [Yesterday]                    DATETIME        NULL,
    [YesterdayPriorYr]             DATETIME        NULL,
    [ItemCategoryCode]             VARCHAR (20)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ProductGroupCode]             VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [LocationCode]                 VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [LocationClassifyCode]         VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [StandardCost]                 DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_StandardCost] DEFAULT ((0)) NOT NULL,
    [StandardCostPriorYr]          DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_StandardCostPriorYr] DEFAULT ((0)) NOT NULL,
    [UnitProductionToday]          DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_UnitProductionToday] DEFAULT ((0)) NOT NULL,
    [UnitProductionMTD]            DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_UnitProductionMTD] DEFAULT ((0)) NOT NULL,
    [UnitProductionYTD]            DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_UnitProductionYTD] DEFAULT ((0)) NOT NULL,
    [UnitProductionPriorYrToday]   DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_UnitProductionPriorYrToday] DEFAULT ((0)) NOT NULL,
    [UnitProductionPriorYrMTD]     DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_UnitProductionPriorYrMTD] DEFAULT ((0)) NOT NULL,
    [UnitProductionPriorYrYTD]     DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_UnitProductionPriorYrYTD] DEFAULT ((0)) NOT NULL,
    [AmountProductionToday]        DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_AmountProductionToday] DEFAULT ((0)) NOT NULL,
    [AmountProductionMTD]          DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_AmountProductionMTD] DEFAULT ((0)) NOT NULL,
    [AmountProductionYTD]          DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_AmountProductionYTD] DEFAULT ((0)) NOT NULL,
    [AmountProductionPriorYrToday] DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_AmountProductionPriorYrToday] DEFAULT ((0)) NOT NULL,
    [AmountProductionPriorYrMTD]   DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_AmountProductionPriorYrMTD] DEFAULT ((0)) NOT NULL,
    [AmountProductionPriorYrYTD]   DECIMAL (18, 6) CONSTRAINT [DF_Snapshot$NAV_Production_AmountProductionPriorYrYTD] DEFAULT ((0)) NOT NULL,
    [SortOrder]                    INT             CONSTRAINT [DF_Snapshot$NAV_Production_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Snapshot$NAV_Production] PRIMARY KEY CLUSTERED ([EntryNo] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Snapshot$NAV_Production_idx1]
    ON [dbo].[Snapshot$NAV_Production]([RunDate] ASC, [ItemCategoryCode] ASC, [ProductGroupCode] ASC, [LocationCode] ASC)
    INCLUDE([EntryNo]) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];

