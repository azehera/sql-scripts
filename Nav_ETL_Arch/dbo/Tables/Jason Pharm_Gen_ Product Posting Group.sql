﻿CREATE TABLE [dbo].[Jason Pharm$Gen_ Product Posting Group] (
    [timestamp]                    ROWVERSION    NULL,
    [Code]                         NVARCHAR (10) NOT NULL,
    [Description]                  NVARCHAR (50) NOT NULL,
    [Def_ VAT Prod_ Posting Group] NVARCHAR (10) NOT NULL,
    [Auto Insert Default]          TINYINT       NOT NULL
) ON [Data Filegroup 1];

