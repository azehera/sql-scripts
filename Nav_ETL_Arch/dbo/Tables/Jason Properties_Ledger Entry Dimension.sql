﻿CREATE TABLE [dbo].[Jason Properties$Ledger Entry Dimension] (
    [timestamp]            ROWVERSION    NOT NULL,
    [Table ID]             INT           NOT NULL,
    [Entry No_]            INT           NOT NULL,
    [Dimension Code]       NVARCHAR (20) NOT NULL,
    [Dimension Value Code] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_Jason Properties$Ledger Entry Dimension] PRIMARY KEY CLUSTERED ([timestamp] ASC) WITH (FILLFACTOR = 80)
);

