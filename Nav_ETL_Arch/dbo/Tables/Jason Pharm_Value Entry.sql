﻿CREATE TABLE [dbo].[Jason Pharm$Value Entry] (
    [Entry No_]                      INT              NOT NULL,
    [Item No_]                       NVARCHAR (20)    NOT NULL,
    [Posting Date]                   DATETIME         NOT NULL,
    [Item Ledger Entry Type]         INT              NOT NULL,
    [Source No_]                     NVARCHAR (20)    NOT NULL,
    [Document No_]                   NVARCHAR (20)    NOT NULL,
    [Description]                    NVARCHAR (50)    NOT NULL,
    [Location Code]                  NVARCHAR (10)    NOT NULL,
    [Inventory Posting Group]        NVARCHAR (10)    NOT NULL,
    [Source Posting Group]           NVARCHAR (10)    NOT NULL,
    [Item Ledger Entry No_]          INT              NOT NULL,
    [Valued Quantity]                DECIMAL (38, 20) NOT NULL,
    [Item Ledger Entry Quantity]     DECIMAL (38, 20) NOT NULL,
    [Invoiced Quantity]              DECIMAL (38, 20) NOT NULL,
    [Cost per Unit]                  DECIMAL (38, 20) NOT NULL,
    [Sales Amount (Actual)]          DECIMAL (38, 20) NOT NULL,
    [Salespers__Purch_ Code]         NVARCHAR (10)    NOT NULL,
    [Discount Amount]                DECIMAL (38, 20) NOT NULL,
    [User ID]                        NVARCHAR (50)    NULL,
    [Source Code]                    NVARCHAR (10)    NOT NULL,
    [Applies-to Entry]               INT              NOT NULL,
    [Global Dimension 1 Code]        NVARCHAR (20)    NOT NULL,
    [Global Dimension 2 Code]        NVARCHAR (20)    NOT NULL,
    [Source Type]                    INT              NOT NULL,
    [Cost Amount (Actual)]           DECIMAL (38, 20) NOT NULL,
    [Cost Posted to G_L]             DECIMAL (38, 20) NOT NULL,
    [Reason Code]                    NVARCHAR (10)    NOT NULL,
    [Drop Shipment]                  TINYINT          NOT NULL,
    [Journal Batch Name]             NVARCHAR (10)    NOT NULL,
    [Gen_ Bus_ Posting Group]        NVARCHAR (10)    NOT NULL,
    [Gen_ Prod_ Posting Group]       NVARCHAR (10)    NOT NULL,
    [Document Date]                  DATETIME         NOT NULL,
    [External Document No_]          NVARCHAR (35)    NULL,
    [Cost Amount (Actual) (ACY)]     DECIMAL (38, 20) NOT NULL,
    [Cost Posted to G_L (ACY)]       DECIMAL (38, 20) NOT NULL,
    [Cost per Unit (ACY)]            DECIMAL (38, 20) NOT NULL,
    [Expected Cost]                  TINYINT          NOT NULL,
    [Item Charge No_]                NVARCHAR (20)    NOT NULL,
    [Valued By Average Cost]         TINYINT          NOT NULL,
    [Partial Revaluation]            TINYINT          NOT NULL,
    [Inventoriable]                  TINYINT          NOT NULL,
    [Valuation Date]                 DATETIME         NOT NULL,
    [Entry Type]                     INT              NOT NULL,
    [Variance Type]                  INT              NOT NULL,
    [Purchase Amount (Actual)]       DECIMAL (38, 20) NOT NULL,
    [Purchase Amount (Expected)]     DECIMAL (38, 20) NOT NULL,
    [Sales Amount (Expected)]        DECIMAL (38, 20) NOT NULL,
    [Cost Amount (Expected)]         DECIMAL (38, 20) NOT NULL,
    [Cost Amount (Non-Invtbl_)]      DECIMAL (38, 20) NOT NULL,
    [Cost Amount (Expected) (ACY)]   DECIMAL (38, 20) NOT NULL,
    [Cost Amount (Non-Invtbl_)(ACY)] DECIMAL (38, 20) NOT NULL,
    [Expected Cost Posted to G_L]    DECIMAL (38, 20) NOT NULL,
    [Exp_ Cost Posted to G_L (ACY)]  DECIMAL (38, 20) NOT NULL,
    [Variant Code]                   NVARCHAR (10)    NOT NULL,
    [Adjustment]                     TINYINT          NOT NULL,
    [Capacity Ledger Entry No_]      INT              NOT NULL,
    [Type]                           INT              NOT NULL,
    [No_]                            NVARCHAR (20)    NOT NULL,
    [Return Reason Code]             NVARCHAR (10)    NOT NULL,
    [Order Type Code]                NVARCHAR (20)    NOT NULL,
    [Standstill Type Code]           NVARCHAR (20)    NOT NULL,
    [Document Type]                  INT              NOT NULL,
    [Document Line No_]              INT              NOT NULL,
    [Job No_]                        NVARCHAR (20)    NOT NULL,
    [Job Task No_]                   NVARCHAR (20)    NOT NULL,
    [Job Ledger Entry No_]           INT              NOT NULL,
    [Average Cost Exception]         TINYINT          NOT NULL,
    [Order Type]                     INT              NULL,
    [Order No_]                      NVARCHAR (20)    NULL,
    [Order Line No_]                 INT              NULL,
    [Dimension Set ID]               INT              NULL,
    CONSTRAINT [Jason Pharm$Value Entry$0] PRIMARY KEY CLUSTERED ([Entry No_] ASC) ON [Data Filegroup 1]
) ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [IX_Jason Pharm$Value Entry_PostingDate]
    ON [dbo].[Jason Pharm$Value Entry]([Posting Date] ASC) WITH (FILLFACTOR = 90)
    ON [Data Filegroup 1];


GO
CREATE NONCLUSTERED INDEX [ItemLedgEntryNo]
    ON [dbo].[Jason Pharm$Value Entry]([Item Ledger Entry No_] ASC) WITH (FILLFACTOR = 90)
    ON [PRIMARY];


GO
CREATE STATISTICS [Document Line No_]
    ON [dbo].[Jason Pharm$Value Entry]([Document Line No_]);


GO
CREATE STATISTICS [Drop Shipment]
    ON [dbo].[Jason Pharm$Value Entry]([Drop Shipment]);


GO
CREATE STATISTICS [Entry Type]
    ON [dbo].[Jason Pharm$Value Entry]([Entry Type]);


GO
CREATE STATISTICS [Posting Date]
    ON [dbo].[Jason Pharm$Value Entry]([Posting Date]);


GO
CREATE STATISTICS [Valuation Date]
    ON [dbo].[Jason Pharm$Value Entry]([Valuation Date]);

