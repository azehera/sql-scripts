﻿CREATE TABLE [dbo].[Snapshot$NAV_InventoryStdCost] (
    [EntryNo]               INT             IDENTITY (1, 1) NOT NULL,
    [RunDate]               DATETIME        NOT NULL,
    [ItemNo]                VARCHAR (20)    NOT NULL,
    [InventoryPostingGroup] VARCHAR (10)    CONSTRAINT [_InvPostGrp_Def] DEFAULT ('') NOT NULL,
    [ItemCategoryCode]      VARCHAR (10)    CONSTRAINT [_ItemCatCode_Def] DEFAULT ('') NOT NULL,
    [ProductGroupCode]      VARCHAR (10)    CONSTRAINT [_ProdGrpCode_Def] DEFAULT ('') NOT NULL,
    [StandardCost]          DECIMAL (18, 6) CONSTRAINT [_StdCost_Def] DEFAULT ((0)) NULL
);

