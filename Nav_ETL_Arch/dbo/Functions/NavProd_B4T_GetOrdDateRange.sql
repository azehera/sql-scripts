﻿



CREATE FUNCTION [dbo].[NavProd_B4T_GetOrdDateRange] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      COUNT(DISTINCT a.[transaction_id])
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_header] a
	join [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] b
	on a.transaction_id=b.transaction_id
	WHERE (transaction_type = 's') AND
	 item_code NOT IN ( 'ASF', 'RC', 'RCC' )and  transaction_date BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME))
  )

  SET @CREDITS =
  (
    SELECT
      COUNT(DISTINCT a.[transaction_id])
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_header] a
	join [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] b
	on a.transaction_id=b.transaction_id
	
	
	WHERE (transaction_type = 'r')
	AND item_code NOT IN ( 'ASF', 'RC', 'RCC' )  AND transaction_date BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME))
  )

  RETURN ISNULL(@DEBITS,0) + ISNULL(@CREDITS,0)
END




