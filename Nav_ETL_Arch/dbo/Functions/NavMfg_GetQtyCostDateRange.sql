﻿CREATE FUNCTION [dbo].[NavMfg_GetQtyCostDateRange] (@LocationCode VARCHAR(10), @ItemCatCode VARCHAR(20), @ProdGrpCode VARCHAR(10), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS TABLE
AS
RETURN

	-- Let's use an example to explain why we're setting the RunDate 1 day ahead
	-- when searching the Standard Cost historical table.
	--
	-- Let's assume we're calling this function with a date range of 1/1/12 to
	-- 6/7/12.
	--
	-- The reporting servers always have data from "yesterday". (Processes that
	-- replicate data from the NAV production server run overnight.) ILEs created
	-- with a posting date of 6/7/12 do not appear on the reporting servers until
	-- 6/8/12.
	--
	-- The daily routine that stores historical records for Item Standard Cost is
	-- stamping each record with RunDate = current day.  This means that a record
	-- created on 6/8/12 actually reflects data from 6/7/12.  So, to find the
	-- Standard Cost for an Item Ledger Entry with Posting Date 6/7/12, we have
	-- to search for historical standard cost recs stamped 6/8/12.

	SELECT
		  SUM(ILE.[Quantity]) AS TotalQty,
		  SUM(ILE.[Quantity] * ISNULL(ISC.StandardCost,0)) AS TotalCost
	FROM [Jason Pharm$Item Ledger Entry] ILE
	LEFT OUTER JOIN [Snapshot$NAV_InventoryStdCost] ISC
	ON (ISC.RunDate = DATEADD(DD, 1, ILE.[Posting Date])) AND
	   (ISC.ItemNo COLLATE DATABASE_DEFAULT = ILE.[Item No_])
	WHERE (ILE.[Location Code] = @LocationCode) AND
		  (ILE.[Item Category Code] = @ItemCatCode) AND
		  (ILE.[Product Group Code] = @ProdGrpCode) AND
		  (ILE.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
		  (ILE.[Entry Type] = 6)  --"Entry Type"::Output
