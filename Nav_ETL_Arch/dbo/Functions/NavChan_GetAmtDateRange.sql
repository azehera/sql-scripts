﻿CREATE FUNCTION [dbo].[NavChan_GetAmtDateRange] (@CPGCode AS VARCHAR(20), @SalesSrce AS VARCHAR(20), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      SUM(Amount)
	FROM [Jason Pharm$Sales Invoice Line] SalesInvLine2
	INNER JOIN [Jason Pharm$Sales Invoice Header] SalesInvHeader2 ON
	  SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
	WHERE (SalesInvHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesInvHeader2.[Customer Posting Group] = @CPGCode) AND
          (SalesInvHeader2.[Shortcut Dimension 1 Code] = @SalesSrce)
  )

  SET @CREDITS =
  (
    SELECT
      SUM(Amount)
	FROM [Jason Pharm$Sales Cr_Memo Line] SalesCrMemoLine2
	INNER JOIN [Jason Pharm$Sales Cr_Memo Header] SalesCrMemoHeader2 ON
	  SalesCrMemoHeader2.[No_] = SalesCrMemoLine2.[Document No_]
	WHERE (SalesCrMemoHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesCrMemoHeader2.[Customer Posting Group] = @CPGCode) AND
          (SalesCrMemoHeader2.[Shortcut Dimension 1 Code] = @SalesSrce)
  )

  RETURN ISNULL(@DEBITS,0) - ISNULL(@CREDITS,0)
END