﻿CREATE FUNCTION [dbo].[GetTrilogyTrainingVisitors]
(@Metric NVARCHAR (4000), @PostDate1 DATETIME, @PostDate2 DATETIME)
RETURNS INT
AS
 EXTERNAL NAME [BusIntel_ExecDash_NAV_Funnel].[UserDefinedFunctions].[GetTrilogyTrainingVisitors]


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFile', @value = N'WebVisitors.cs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'GetTrilogyTrainingVisitors';


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFileLine', @value = N'21', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'GetTrilogyTrainingVisitors';

