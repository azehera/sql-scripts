﻿CREATE FUNCTION [dbo].[NavFunnel_GetMWCCStoresOpen] (@PostDate1 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @DAY1COUNT INT
  DECLARE @DAY2COUNT INT
  
  DECLARE @PostDate2 DATETIME

  SET @PostDate2 = DATEADD(DD, -1, @PostDate1)

  SET @DAY1COUNT =
  (
    SELECT MWCCActiveStoreCount
    FROM [Snapshot$NAV_Funnel]
    WHERE Yesterday = @PostDate1
  )

  SET @DAY2COUNT =
  (
    SELECT MWCCActiveStoreCount
    FROM [Snapshot$NAV_Funnel]
    WHERE Yesterday = @PostDate2
  )

  RETURN ISNULL(@DAY1COUNT - @DAY2COUNT,0)
END