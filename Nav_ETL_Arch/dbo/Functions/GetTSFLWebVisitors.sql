﻿CREATE FUNCTION [dbo].[GetTSFLWebVisitors]
(@PostDate1 DATETIME)
RETURNS INT
AS
 EXTERNAL NAME [BusIntel_ExecDash_NAV_Funnel].[UserDefinedFunctions].[GetTSFLWebVisitors]


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFile', @value = N'WebVisitors.cs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'GetTSFLWebVisitors';


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFileLine', @value = N'131', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'GetTSFLWebVisitors';

