﻿CREATE FUNCTION [dbo].[NavProd_B4T_GetAmtDateRange_old] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      SUM(sold_price * qty)
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
	JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
	ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
	WHERE (transaction_type = 's') AND transaction_date BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME))
  )

  SET @CREDITS =
  (
    SELECT
      SUM(sold_price * qty)
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
	JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
	ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
	WHERE (transaction_type = 'r') AND transaction_date BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME))
  )

  RETURN ISNULL(@DEBITS,0) + ISNULL(@CREDITS,0)
END