﻿CREATE FUNCTION [dbo].[NavFunnel_GetMWCCApptsCreated] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @RECCOUNT INT

  SET @RECCOUNT = 
  (
    SELECT
      COUNT(*)
    FROM Book4Time_ETL.[dbo].[B4T_appointment] B4T_Appt
    LEFT OUTER JOIN Book4Time_ETL.[dbo].[B4T_Person] B4T_Person
    ON B4T_Person.[person_id] = B4T_Appt.customer_id
    WHERE ([cancellation_id] IS NULL) AND
          (B4T_Person.last_name <> 'Center') AND
          (createddate BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )

  RETURN ISNULL(@RECCOUNT,0)
END