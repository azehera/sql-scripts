﻿CREATE FUNCTION [dbo].[NavInvt_B4T_GetQtyDateRange_old] (@LocationID VARCHAR(20), @ProductID VARCHAR(20), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @Quantity DECIMAL(18,2)
  
  SET @Quantity =
  (
    SELECT
      SUM(quantity)
    FROM [Book4Time_ETL].[dbo].[B4T_Inventory_Transactions]
    WHERE (transaction_date BETWEEN @PostDate1 AND @PostDate2) AND
          (location_id = @LocationID) AND (product_id = @ProductID)
  )
  
  RETURN ISNULL(@Quantity,0)
END