﻿CREATE FUNCTION [dbo].[GetMedWebDoctorEmails]
(@PostDate1 DATETIME, @PostDate2 DATETIME)
RETURNS INT
AS
 EXTERNAL NAME [BusIntel_ExecDash_NAV_Funnel].[UserDefinedFunctions].[GetMedWebDoctorEmails]


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFile', @value = N'WebVisitors.cs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'GetMedWebDoctorEmails';


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFileLine', @value = N'169', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'GetMedWebDoctorEmails';

