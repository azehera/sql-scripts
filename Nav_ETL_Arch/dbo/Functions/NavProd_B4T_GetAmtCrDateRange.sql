﻿CREATE FUNCTION NavProd_B4T_GetAmtCrDateRange (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  RETURN
  (
    SELECT
      SUM(total_amt)
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_header]
	WHERE (transaction_type = 'r') AND transaction_date BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME))
  )
END