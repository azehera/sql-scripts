﻿
CREATE FUNCTION [dbo].[NavProd_B4T_GetICCNullQtyDateRange_old] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      SUM(qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],1))
    FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
    JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
    ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
    LEFT OUTER JOIN [Book4Time_ETL].[dbo].B4T_product_master B4TPM
    ON B4TPM.product_id = B4T_Detail.product_id
    LEFT OUTER JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item
    ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
    JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
    ON (ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = B4T_Detail.item_code) AND
       (ItemUOM.Code COLLATE DATABASE_DEFAULT = Item.[Sales Unit of Measure])
    WHERE (Item.[Item Category Code] IS NULL) AND
      (B4TPM.product_type_cd <> 'p') AND
	  (B4T_Header.[transaction_type] = 's') AND
	  (B4T_Header.[transaction_date] BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )

  SET @CREDITS =
  (
    SELECT
      SUM(qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],1))
    FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
    JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
    ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
    LEFT OUTER JOIN [Book4Time_ETL].[dbo].B4T_product_master B4TPM
    ON B4TPM.product_id = B4T_Detail.product_id
    LEFT OUTER JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item
    ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
    JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
    ON (ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = B4T_Detail.item_code) AND
       (ItemUOM.Code COLLATE DATABASE_DEFAULT = Item.[Sales Unit of Measure])
    WHERE (Item.[Item Category Code] IS NULL) AND
      (B4TPM.product_type_cd <> 'p') AND
	  (B4T_Header.[transaction_type] = 'r') AND
	  (B4T_Header.[transaction_date] BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )

  RETURN ISNULL(@DEBITS,0) + ISNULL(@CREDITS,0)
END
