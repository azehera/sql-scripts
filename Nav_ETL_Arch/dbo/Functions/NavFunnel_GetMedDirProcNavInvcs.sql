﻿
CREATE FUNCTION [dbo].[NavFunnel_GetMedDirProcNavInvcs] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @DEBITS INT
  
  SET @DEBITS = 
  (
    SELECT
      COUNT(DISTINCT(SalesInvHeader2.No_))
      FROM [Jason Pharm$Sales Invoice Header] SalesInvHeader2
      WHERE (SalesInvHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND (SalesInvHeader2.[Customer Posting Group] = 'MEDIFAST') AND
          (SalesInvHeader2.[Shortcut Dimension 1 Code] = 'WEB')

  )
  
  RETURN ISNULL(@DEBITS,0)
END
