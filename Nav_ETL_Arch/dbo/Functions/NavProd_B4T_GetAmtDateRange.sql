﻿




/******************************************************************************************
OBJECT NAME:      [dbo].[NavProd_B4T_GetAmtDateRange]]
DEVELOPER:        
DATE:             
DESCRIPTION:      
PARAMETERS/VARRIABLES:
NOTES:
------------------------------ MODIFICATIONS ----------------------------------------
DEVELOPER:  Kalpesh Patel
DATE:       8/7/2012
RFC#:       629
DESCRIPTION: /****** we don't need to calculate any thing for MWCC data from NAV_ETL so removing back below function*****/
            /****Remove ASF Itemcode from Total Sales for MWCC****/
-------------------------------------------------------------------------------------------
*******************************************************************************************/


CREATE FUNCTION [dbo].[NavProd_B4T_GetAmtDateRange] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)
  DECLARE @CREDITSNONITEM DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      SUM(sold_price * qty)
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
	JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
	ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
    WHERE (transaction_type = 's') AND
    item_code not in ('ASF','RC','RCC') AND /****Remove ASF Itemcode from Total Sales for MWCC****/
	transaction_date BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME))
  )

  SET @CREDITS =
  (
    SELECT
      SUM(sold_price * qty)
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
	JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
	ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
    WHERE (transaction_type = 'r') AND
    item_code not in ('ASF','RC','RCC')  AND /****Remove ASF Itemcode from Total Sales for MWCC****/
	transaction_date BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME))
  )
  
/**** Section1:@CREDITSNONITEM: A correction that was entered as Credit Memo's to give back the missing
      discounts to a bunch of MWCC's caused a discreapancy in Channel View, 
      because the product View saw the entered lines as NON-ITEM but there is 
      no corresponding Return Entries in B4T.  The total for these entries (from NAV) 
      will be added to the total B4T sales.  NaVProd_B4T_GetAmtDateRange was updated to 
      include code to do this.   *****/
      
/****** we don't need to calculate any thing for MWCC data from NAV_ETL so removing back below function*****/
  
 -- SET @CREDITSNONITEM=
  
 -- (  SELECT
 --    SUM( Amount)
	--FROM [Jason Pharm$Sales Cr_Memo Line] SalesCrMemoLine2
	--INNER JOIN [Jason Pharm$Sales Cr_Memo Header] SalesCrMemoHeader2 ON
	--  SalesCrMemoHeader2.[No_] COLLATE DATABASE_DEFAULT = SalesCrMemoLine2.[Document No_]
	--WHERE
 --         (SalesCrMemoLine2.Type <> 2) AND
 --         (SalesCrMemoHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) 
 --         AND
 --         (SalesCrMemoHeader2.[Customer Posting Group] = 'CORPCLINIC')
           
    --)

  RETURN ISNULL(@DEBITS,0) + ISNULL(@CREDITS,0)
END








