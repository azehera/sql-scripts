﻿
CREATE FUNCTION [dbo].[NavProd_B4T_GetICCAmtDateRange] (@ICC AS VARCHAR(20), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      SUM(sold_price * qty)
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
	JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
	ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
	JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item
	ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
	WHERE (Item.[Item Category Code] = @ICC) AND
	      (B4T_Header.[transaction_type] = 's') AND
	       item_code not in ('ASF','RC','RCC') AND
	      (B4T_Header.[transaction_date] BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )

  SET @CREDITS =
  (
    SELECT
      SUM(sold_price * qty)
    FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
    JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
    ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
    LEFT OUTER JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item
    ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
    WHERE (Item.[Item Category Code] = @ICC) AND
	      (B4T_Header.[transaction_type] = 'r') AND
	       item_code not in ('ASF','RC','RCC') AND
	      (B4T_Header.[transaction_date] BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )

  RETURN ISNULL(@DEBITS,0) + ISNULL(@CREDITS,0)
END
