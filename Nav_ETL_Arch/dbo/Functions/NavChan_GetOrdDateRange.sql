﻿
CREATE FUNCTION [dbo].[NavChan_GetOrdDateRange] (@CPGCode AS VARCHAR(20), @SalesSrce AS VARCHAR(20), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @DEBITS INT
  DECLARE @CREDITS INT

  SET @DEBITS = 
  (
    SELECT
      COUNT(DISTINCT(SalesInvHeader2.No_))
	FROM [Jason Pharm$Sales Invoice Header] SalesInvHeader2
	WHERE (SalesInvHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesInvHeader2.[Customer Posting Group] = @CPGCode) AND
          (SalesInvHeader2.[Shortcut Dimension 1 Code] = @SalesSrce)
  )

  SET @CREDITS =
  (
    SELECT
      COUNT(DISTINCT(SalesCrMemoHeader2.No_))
	FROM [Jason Pharm$Sales Cr_Memo Header] SalesCrMemoHeader2
	WHERE (SalesCrMemoHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesCrMemoHeader2.[Customer Posting Group] = @CPGCode) AND
          (SalesCrMemoHeader2.[Shortcut Dimension 1 Code] = @SalesSrce)
  )

  RETURN ISNULL(@DEBITS,0) + ISNULL(@CREDITS,0)
END
