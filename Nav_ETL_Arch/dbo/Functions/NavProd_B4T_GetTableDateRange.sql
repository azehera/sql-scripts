﻿CREATE FUNCTION NavProd_B4T_GetTableDateRange (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS TABLE
AS
  RETURN
  (
    SELECT
      *
	FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_header]
	WHERE transaction_date BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME))
  )