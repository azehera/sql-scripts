﻿




/******************************************************************************************
OBJECT NAME:      [dbo].[NavProd_B4T_GetICCMiscQtyDateRange]
DEVELOPER:        
DATE:             
DESCRIPTION:      
PARAMETERS/VARRIABLES:
NOTES:
------------------------------ MODIFICATIONS ----------------------------------------
DEVELOPER:  Kalpesh Patel
DATE:       8/2/2012
RFC#:       629
DESCRIPTION:  Item Category MISC we are calculating Voids in Quantity but
              not in sales so we have to remove from Quantity Section 1
-------------------------------------------------------------------------------------------
DEVELOPER:  Kalpesh Patel
DATE:       8/14/2012
RFC#:       635
DESCRIPTION:  This change is removing CORPCLINIC data that comes from NAV because that has 
to be calculated from B4T only.  The two different Item Categories NPD and MISC have been 
combined into one as NON-ITEMS. This chane was done separately in the same manner for 
Channel and Product Views. 
*******************************************************************************************/


CREATE FUNCTION [dbo].[NavProd_B4T_GetICCMiscQtyDateRange] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @QTY DECIMAL(18,2)

  SET @QTY = 
  (
    SELECT
      SUM(qty)
    FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
    JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
    ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
    LEFT OUTER JOIN [Book4Time_ETL].[dbo].B4T_product_master B4TPM
    ON B4TPM.product_id = B4T_Detail.product_id
    LEFT OUTER JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item
    ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
    WHERE (Item.[Item Category Code] IS NULL) AND
          (B4TPM.product_type_cd = 'p') AND
          (B4T_Header.[transaction_type] in ('s','r')) and   /*****Section 1******/
          item_code not in ('ASF','RC','RCC')  AND
	  (B4T_Header.[transaction_date] BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )
  RETURN ISNULL(@QTY,0)
END



