﻿
/******************************Modifications*********************************
Date: 3/22/2018
Developer: Menkir Haile
Change Desc: Change the data source from a precalculated [Snapshot$NAV_Funnel] table which is based on
			Odyssey datasource [dbo].[ODYSSEY_CUSTOMER] to Navision Invoced Sales.


**************************************************************************/


CREATE FUNCTION [dbo].[NavFunnel_GetActiveHealthCoachesHistory] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @TOTAL INT

  --SET @TOTAL =
  --(
  --  SELECT
  --    SUM(ActiveHealthCoachesDay)
  --  FROM [Snapshot$NAV_Funnel]
  --  WHERE (Yesterday BETWEEN @PostDate1 AND @PostDate2)
  --)

   SET @TOTAL =
  (
    ----------Find HC Kit Invoiced for the day (@PostDate)
	SELECT DISTINCT COUNT (SL.[Document No_]) 
	FROM NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] SL ( NOLOCK )
	WHERE SL.No_ IN ( '31100','31110','31011','31012','32051','31105' ) AND SL.[Posting Date]  BETWEEN CAST(@PostDate1 AS DATE) AND CAST(@PostDate2 AS DATE)
  )

  RETURN ISNULL(@TOTAL,0)
END