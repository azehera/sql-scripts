﻿CREATE FUNCTION [dbo].[NavProd_B4T_HIST_GetICCAmtDateRange] (@ICC AS VARCHAR(20), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @TOTAL DECIMAL(18,2)

  SET @TOTAL =
  (
    SELECT
      SUM(total_amt)
    FROM [Book4Time_ETL].[dbo].[B4T_HIST_Center_Sales_DailyAvg] B4T_History
    WHERE (B4T_History.item_cat_code = @ICC) AND
	  (B4T_History.[transaction_date] BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )

  RETURN ISNULL(@TOTAL,0)
END