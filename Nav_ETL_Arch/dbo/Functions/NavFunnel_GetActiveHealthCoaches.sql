﻿
/******************************Modifications*********************************
Date: 3/22/2018
Developer: RMenkir Haile
Change Desc: Change the data source fron [dbo].[ODYSSEY_CUSTOMER] to Navision Invoced Sales.


**************************************************************************/
CREATE FUNCTION [dbo].[NavFunnel_GetActiveHealthCoaches] (@PostDate DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @RECCOUNT INT

 /* The Older version of Coach Activation Count
  
   SET @Text1 = REPLACE(CONVERT(VARCHAR, @PostDate, 102), '.', '-') + '%'

  SET @RECCOUNT = 
  (
    SELECT
      COUNT(*)
    FROM [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]
    WHERE EFFECTIVE_DATE LIKE @Text1
  )
*/
  SET @RECCOUNT = 
  (
    ----------Find HC Kit Invoiced for the day (@PostDate)
	SELECT DISTINCT COUNT (SL.[Document No_])
	FROM NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] SL ( NOLOCK )
	WHERE SL.No_ IN ( '31100','31110','31011','31012','32051','31105' ) AND SL.[Posting Date] = CAST (@PostDate AS DATE)
  )
  
  RETURN ISNULL(@RECCOUNT,0)
END