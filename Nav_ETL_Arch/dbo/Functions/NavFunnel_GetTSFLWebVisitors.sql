﻿CREATE FUNCTION [dbo].[NavFunnel_GetTSFLWebVisitors] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @TOTAL INT

  SET @TOTAL =
  (
    SELECT
      SUM(TSFLWebVisitorsDay)
    FROM [Snapshot$NAV_Funnel]
    WHERE (Yesterday BETWEEN @PostDate1 AND @PostDate2)
  )

  RETURN ISNULL(@TOTAL,0)
END