﻿CREATE FUNCTION NavProd_GetQtyCrNonItemDateRange (@CPGCode AS VARCHAR(20), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  RETURN
  (
    SELECT
      SUM([Quantity (Base)])
	FROM [Jason Pharm$Sales Cr_Memo Line] SalesCrMemoLine2
	INNER JOIN [Jason Pharm$Sales Cr_Memo Header] SalesCrMemoHeader2 ON
	  SalesCrMemoHeader2.[No_] COLLATE DATABASE_DEFAULT = SalesCrMemoLine2.[Document No_]
	WHERE
          (SalesCrMemoLine2.Type <> 2) AND
          (SalesCrMemoHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesCrMemoHeader2.[Customer Posting Group] = @CPGCode)
  )
END
