﻿CREATE FUNCTION [dbo].[GetMedDirWebVisitors]
(@PostDate1 DATETIME)
RETURNS INT
AS
 EXTERNAL NAME [BusIntel_ExecDash_NAV_Funnel].[UserDefinedFunctions].[GetMedDirWebVisitors]


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFile', @value = N'WebVisitors.cs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'GetMedDirWebVisitors';


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFileLine', @value = N'93', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'FUNCTION', @level1name = N'GetMedDirWebVisitors';

