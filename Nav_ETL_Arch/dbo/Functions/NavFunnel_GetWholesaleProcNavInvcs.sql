﻿CREATE FUNCTION [dbo].[NavFunnel_GetWholesaleProcNavInvcs] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      COUNT(SalesInvHeader2.[No_])
    FROM [Jason Pharm$Sales Invoice Header] SalesInvHeader2
    WHERE (SalesInvHeader2.[Order Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesInvHeader2.[Customer Posting Group] = 'DOCTORS')
  )

  /* field Order Date does not exist in the table
  SET @CREDITS = 
  (
    SELECT
      COUNT(SalesCrMemoHeader2.[No_])
    FROM [Jason Pharm$Sales Cr_Memo Header] SalesCrMemoHeader2
    WHERE (SalesCrMemoHeader2.[Order Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesCrMemoHeader2.[Customer Posting Group] = 'DOCTORS')
  )

  RETURN ISNULL(@DEBITS,0) - ISNULL(@CREDITS,0)
  */
  
  RETURN ISNULL(@DEBITS,0)
END