﻿CREATE FUNCTION [dbo].[NavInvt_GetQtyDateRange] (@LocationID VARCHAR(20), @ItemNo VARCHAR(20), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @Quantity DECIMAL(18,2)
  
  SET @Quantity =
  (
    SELECT
      SUM([Quantity])
    FROM [Jason Pharm$Item Ledger Entry]
    WHERE ([Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
          ([Location Code] = @LocationID) AND ([Item No_] = @ItemNo) 
  )
  
  RETURN ISNULL(@Quantity,0)
END