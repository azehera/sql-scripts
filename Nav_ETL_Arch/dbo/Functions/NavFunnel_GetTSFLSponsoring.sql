﻿



CREATE FUNCTION [dbo].[NavFunnel_GetTSFLSponsoring] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
 SELECT
Count(Distinct SalesInvLine2.[Document No_])
	 FROM [Jason Pharm$Sales Invoice Line] SalesInvLine2
	 --DS @ 6/18/2013 - This was changed by BU team to use only Sales Invoice Line rather than Sales Invoice Header
WHERE
(SalesInvLine2.Type = 2) AND
(SalesInvLine2.[No_] IN ('31011','31012','32051','31100','31105','31110')) AND  ---Three New Items added as per TSFL Requirement on 7/13/2012 by kpatel
(SalesInvLine2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2)  AND
(SalesInvLine2.[Shortcut Dimension 2 Code] = 'TSFL') 
       
     
  )

  SET @CREDITS = 
  (
   SELECT
Count(Distinct SalesCrMemoLine2.[Document No_])

	FROM [Jason Pharm$Sales Cr_Memo Line] SalesCrMemoLine2
WHERE
(SalesCrMemoLine2.Type = 2) AND
(SalesCrMemoLine2.[No_] IN ('31011','31012','32051','31100','31105','31110')) AND  ---Three New Items added as per TSFL Requirement on 7/13/2012 by kpatel
(SalesCrMemoLine2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
(SalesCrMemoLine2.[Shortcut Dimension 2 Code] = 'TSFL') 
       
  )

  RETURN ISNULL(@DEBITS,0) - ISNULL(@CREDITS,0)
END





