﻿CREATE FUNCTION [dbo].[NavFunnel_GetTSFLSponsoring_bak] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      COUNT(DISTINCT SalesInvLine2.[Document No_])
	FROM [Jason Pharm$Sales Invoice Line] SalesInvLine2
	INNER JOIN [Jason Pharm$Sales Invoice Header] SalesInvHeader2 ON
	  SalesInvHeader2.[No_] COLLATE DATABASE_DEFAULT = SalesInvLine2.[Document No_]
	WHERE
          (SalesInvLine2.Type = 2) AND
          (SalesInvLine2.[No_] IN ('31011','31012','32051')) AND
          (SalesInvHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesInvHeader2.[Customer Posting Group] = 'TSFL')
  )

  SET @CREDITS = 
  (
    SELECT
      COUNT(DISTINCT SalesCrMemoLine2.[Document No_])
	FROM [Jason Pharm$Sales Cr_Memo Line] SalesCrMemoLine2
	INNER JOIN [Jason Pharm$Sales Cr_Memo Header] SalesCrMemoHeader2 ON
	  SalesCrMemoHeader2.[No_] COLLATE DATABASE_DEFAULT = SalesCrMemoLine2.[Document No_]
	WHERE
          (SalesCrMemoLine2.Type = 2) AND
          (SalesCrMemoLine2.[No_] IN ('31011','31012','32051')) AND
          (SalesCrMemoHeader2.[Posting Date] BETWEEN @PostDate1 AND @PostDate2) AND
          (SalesCrMemoHeader2.[Customer Posting Group] = 'TSFL')
  )

  RETURN ISNULL(@DEBITS,0) - ISNULL(@CREDITS,0)
END