﻿



CREATE FUNCTION [dbo].[NavProd_B4T_GetCostDateRange] (@PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @DEBITS DECIMAL(18,2)
  DECLARE @CREDITS DECIMAL(18,2)

  SET @DEBITS = 
  (
    SELECT
      SUM(Item.[Standard Cost] * (qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],1)))
    FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
    JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
    ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
    JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item
    ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
    JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
    ON (ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = B4T_Detail.item_code) AND
       (ItemUOM.Code COLLATE DATABASE_DEFAULT = Case when Item.[Sales Unit of Measure] in ('PK','CS') 
                                                     then [Base Unit of Measure] 
                                                     else [Sales Unit of Measure] end )/****For MWCC we need to convert boxes in eaches****/
    WHERE (B4T_Header.[transaction_type] = 's') AND item_code not in ('ASF','RC','RCC')and 
          (B4T_Header.[transaction_date] BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )

  SET @CREDITS =
  (
    SELECT
      SUM(Item.[Standard Cost] * (qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],1)))
    FROM [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
    JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
    ON B4T_Header.[transaction_id] = B4T_Detail.[transaction_id]
    JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item
    ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
    JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
    ON (ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = B4T_Detail.item_code) AND
       (ItemUOM.Code COLLATE DATABASE_DEFAULT = Case when Item.[Sales Unit of Measure] in ('PK','CS') 
                                                     then [Base Unit of Measure] 
                                                     else [Sales Unit of Measure] end )/****For MWCC we need to convert boxes in eaches****/
    WHERE (B4T_Header.[transaction_type] = 'r') AND item_code not in ('ASF','RC','RCC')and 
	  (B4T_Header.[transaction_date] BETWEEN CAST(FLOOR(CAST(@PostDate1 AS float)) AS DATETIME) AND DATEADD(MS, -2, CAST(CEILING(CAST(DATEADD(DD, 1, CAST(FLOOR(CAST(@PostDate2 AS float)) AS DATETIME)) AS float)) AS DATETIME)))
  )

  RETURN ISNULL(@DEBITS,0) + ISNULL(@CREDITS,0)
END




