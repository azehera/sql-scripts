﻿
CREATE FUNCTION [dbo].[NavFunnel_GetMWCCActiveStoreCount] (@PostDate AS DATETIME)
RETURNS INT
AS
BEGIN
  DECLARE @RECCOUNT INT
  DECLARE @DateBegin DATETIME

  SET @DateBegin = DATEADD(DD, -30, @PostDate)

  SET @RECCOUNT = 
  (
    SELECT COUNT(DISTINCT(location_id))
    FROM [BI_Reporting].[dbo].[B4T_location]
    where [OpenStores]=1
    
  )

  RETURN ISNULL(@RECCOUNT,0)
END
