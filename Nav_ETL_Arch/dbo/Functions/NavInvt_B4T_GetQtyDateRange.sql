﻿

/******************************************************************************************
OBJECT NAME:      [dbo].[NavInvt_B4T_GetQtyDateRange]
DEVELOPER:        
DATE:             
DESCRIPTION:      
PARAMETERS/VARRIABLES:
NOTES:
------------------------------ MODIFICATIONS ----------------------------------------
DEVELOPER:  Kalpesh Patel
DATE:       8/6/2012
RFC#:       629
DESCRIPTION: Section:1 Transaction date come with date and time so we need to make it time 00. 
-------------------------------------------------------------------------------------------
*******************************************************************************************/

CREATE FUNCTION [dbo].[NavInvt_B4T_GetQtyDateRange] (@LocationID VARCHAR(20), @ProductID VARCHAR(20), @PostDate1 AS DATETIME, @PostDate2 AS DATETIME)
RETURNS DECIMAL(18,2)
AS
BEGIN
  DECLARE @Quantity DECIMAL(18,2)
  
  SET @Quantity =
  (
    SELECT
      SUM(quantity)
    FROM [Book4Time_ETL].[dbo].[B4T_Inventory_Transactions]
    WHERE (DATEADD(D, 0, DATEDIFF(D, 0,transaction_date)) BETWEEN @PostDate1 AND @PostDate2) AND  /***Section 1***/
          (location_id = @LocationID) AND (product_id = @ProductID)
  )
  
  RETURN ISNULL(@Quantity,0)
END



