﻿/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_FactSalesKey] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Fact data for cube from Nav_ETL database     
PARAMETERS/VARRIABLES:
NOTES: 
Date: 9/18/2018 Developer :Ronak Shah
Description : Modify this SP to include RDCWMS data (Location ID 7)
*******************************************************************************************/
CREATE PROCEDURE [dbo].[BI_NAV_FactSalesKey_bef022219]
AS 
 SELECT  'USA' as Country,
            SalesInvHeader2.[Customer Posting Group] AS [CustomerPostingGroup] ,
            SalesInvHeader2.[Shortcut Dimension 1 Code] AS SalesChannel ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN 'NON-ITEM'
                 WHEN SalesInvLine2.Type = 2
                      AND ( SalesInvLine2.[Item Category Code] COLLATE DATABASE_DEFAULT NOT IN (
                            SELECT  Code
                            FROM    [NAV_ETL].dbo.[Jason Pharm$Item Category] ) )
                 THEN 'UNCATEGORIZED'
                 ELSE SalesInvLine2.[Item Category Code] END AS [ItemCategoryCode],
             CASE WHEN SalesInvLine2.Type <> 2 THEN 'NON-ITEM'
                 WHEN SalesInvLine2.Type = 2
                      AND ( SalesInvLine2.[Item Category Code] COLLATE DATABASE_DEFAULT NOT IN (
                            SELECT  Code
                            FROM    NAV_ETL.dbo.[Jason Pharm$Item Category] ) )
                 THEN 'UNCATEGORIZED'
                 when [MOA_ASSOC_SKU_UOM]='KT' then 'KITS'
                 ELSE SalesInvLine2.[Item Category Code]    
            END AS [ItemCategoryCodeSubcategory] ,
            SalesInvHeader2.[Sell-to Customer No_] AS SelltoCustomerID ,
            SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Ship-to Post Code], 5) AS ShiptoCustomerKey ,
            SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Sell-to Post Code],5) AS SelltoCustomerKey ,
            SalesInvLine2.No_ AS ItemCode ,
            isnull(Convert(varchar(50),[MOA_ASSOC_SKU]),SalesInvLine2.No_)COLLATE DATABASE_DEFAULT AS ItemCodeSubCategory ,
            [Document No_] AS DocumentNo ,
            [Line Discount Amount] AS LineDiscount ,
            ( [Inv_ Discount Amount] ) AS InvoiceDiscount ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN '0'
                 WHEN SalesInvLine2.[Item Category Code] = 'NPD' THEN '0'
                 ELSE ([Quantity]*SalesInvLine2.[Qty_ per Unit of Measure])
            END AS Units ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN '0'
                 WHEN SalesInvLine2.[Item Category Code] = 'NPD' THEN '0'
                 when [MOA_ASSOC_SKU_UOM]='KT' then 
                 case when row_number () over ( partition by [Document No_],[MOA_ASSOC_SKU] Order by [Document No_],[MOA_ASSOC_SKU])=1 then 1*MOA_ASSOC_QTY else 0 end 
                 ELSE ([Quantity]*SalesInvLine2.[Qty_ per Unit of Measure]) end as SubCategoryUnits 
            ,ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsPerBox ,
            ( [Quantity (Base)] / ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') ) AS BOX ,
            [Amount] ,
            [Line Amount] AS [Gross Amount] ,
            [Amount Including VAT] ,
            CASE WHEN SalesInvLine2.[Tax Group Code] = ' '
                 THEN ( [Amount Including VAT] + ( -1 * [Amount] ) )
                 ELSE ( [Amount Including VAT] - [Amount] )
            END AS TaxAmount ,
            SalesInvLine2.[Tax Group Code] ,
            CASE WHEN [Unit Cost (LCY)] = 0 THEN 0
                 ELSE ( [Unit Cost (LCY)]
                        / SalesInvLine2.[Qty_ per Unit of Measure] )
            END AS [Unit Cost (LCY)] ,
             Quantity,
            
            1 AS SalesType ,
            CASE WHEN SalesInvLine2.[Location Code] = 'SUNRISE' THEN 1
                 WHEN SalesInvLine2.[Location Code] = 'TDC' THEN 2
                 WHEN SalesInvLine2.[Location Code] = 'TSFL' THEN 3
                 WHEN SalesInvLine2.[Location Code] = 'TDCWMS' THEN 4
                 WHEN SalesInvLine2.[Location Code] = 'MDCWMS' THEN 5
				 WHEN SalesInvLine2.[Location Code] = 'RDCWMS' THEN 7
                 ELSE 6
            END AS LocationID,
            LEFT(SalesInvHeader2.[Sell-to Post Code], 4) AS RegionID,
            SalesInvHeader2.[Posting Date]
    FROM    [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Line] SalesInvLine2 WITH ( NOLOCK )
            INNER JOIN [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Header] SalesInvHeader2 WITH ( NOLOCK ) ON SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
            LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item WITH ( NOLOCK ) ON Item.No_ COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_]
            LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
            WITH ( NOLOCK ) ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_] )
                               AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
                                                              WHEN SalesInvLine2.[Item Category Code] = 'RTD'
                                                              THEN 'CS'
                                                              ELSE Item.[Sales Unit of Measure]
                                                              END )
             Left join [NAV_ETL].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION] a WITH ( NOLOCK )
             on  SalesInvLine2.No_ COLLATE DATABASE_DEFAULT =a.[MOA_SKU]
             and  CAST(a.[MOA_ORL_ID] AS VARCHAR(20))=[Order Item No_]
    WHERE   SalesInvHeader2.[Shortcut Dimension 1 Code] <> ' '
            AND SalesInvLine2.No_ NOT IN ( 'RC', 'RCC' )
            --and ( SalesInvHeader2.[Posting Date]>='2011-01-01')
            AND ( SalesInvHeader2.[Posting Date] >= DATEADD(dd,
                                                            DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                            0) )/*** Section 1***/
            AND [Customer Posting Group] not in ( 'CORPCLINIC','CANCO')
             and SalesInvLine2.No_ not in ('31011','31021','32035','34822','39240','40492','40540',
	                                                     '40550','40650','65050','65460','72755','74000','74001',
	                                                     '74002','74008','74050','74051','74058','74059','74100',
	                                                     '74101','74102','74108','74200','74201','74208','74300',
	                                                     '74350','74351','74400','74500','74600','74700','74850',
	                                                     '74855','74856','74860','74865','74866','74867','74868',
	                                                     '74870','74920','75005','75010','75030','75035','75040',
	                                                     '75050','75055','75060')
         
            
            
   UNION ALL
    
    
    SELECT  'USA' as Country,
            SalesInvHeader2.[Customer Posting Group] AS [CustomerPostingGroup] ,
            SalesInvHeader2.[Shortcut Dimension 1 Code] AS SalesChannel ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN 'NON-ITEM'
                 WHEN SalesInvLine2.Type = 2
                      AND ( SalesInvLine2.[Item Category Code] COLLATE DATABASE_DEFAULT NOT IN (
                            SELECT  Code
                            FROM   [NAV_ETL].dbo.[Jason Pharm$Item Category] ) )
                 THEN 'UNCATEGORIZED'
                 ELSE SalesInvLine2.[Item Category Code]
            END AS [ItemCategoryCode] ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN 'NON-ITEM'
                 WHEN SalesInvLine2.Type = 2
                      AND ( SalesInvLine2.[Item Category Code] COLLATE DATABASE_DEFAULT NOT IN (
                            SELECT  Code
                            FROM   NAV_ETL.dbo.[Jason Pharm$Item Category] ) )
                 THEN 'UNCATEGORIZED'
                 ELSE SalesInvLine2.[Item Category Code]
            END AS [ItemCategoryCodeSubcategory],
            SalesInvHeader2.[Sell-to Customer No_] AS SelltoCustomerID ,
            SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Ship-to Post Code], 5) AS ShiptoCustomerKey ,
            SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Sell-to Post Code],5) AS SelltoCustomerKey ,
            SalesInvLine2.No_ AS ItemCode ,
             SalesInvLine2.No_ AS ItemCodeSubcategory ,
            [Document No_] AS DocumentNo ,
            [Line Discount Amount] AS LineDiscount ,
            ( [Inv_ Discount Amount] * -1 ) AS InvoiceDiscount ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN '0'
                 WHEN SalesInvLine2.[Item Category Code] = 'NPD' THEN '0'
                 ELSE ([Quantity]*SalesInvLine2.[Qty_ per Unit of Measure])*-1
            END AS Units ,
             CASE WHEN SalesInvLine2.Type <> 2 THEN '0'
                 WHEN SalesInvLine2.[Item Category Code] = 'NPD' THEN '0'
                 when [MOA_ASSOC_SKU_UOM]='KT' then 
                 case when row_number () over ( partition by [Document No_] Order by [Document No_],SalesInvLine2.No_)=1 then -1*MOA_ASSOC_QTY else 0 end 
                 ELSE ([Quantity]*SalesInvLine2.[Qty_ per Unit of Measure])*-1 end as SubCategoryUnits,
            ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsperBox ,
            ( [Quantity (Base)] / ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') ) AS BOX ,
            [Amount] * -1 AS Amount ,
            ( [Line Amount] * -1 ) AS [Gross Amount] ,
            [Amount Including VAT] ,
            CASE WHEN SalesInvLine2.[Tax Group Code] = ' '
                 THEN ( [Amount Including VAT] + ( -1 * [Amount] ) )
                 ELSE ( [Amount Including VAT] - [Amount] ) * -1
            END AS TaxAmount ,
            SalesInvLine2.[Tax Group Code] ,
            CASE WHEN [Unit Cost (LCY)] = 0 THEN 0
                 ELSE ( [Unit Cost (LCY)]
                        / SalesInvLine2.[Qty_ per Unit of Measure] )
            END AS [Unit Cost (LCY)] ,
             Quantity * -1  AS Quantity ,
            2 AS SalesType ,
            CASE WHEN SalesInvLine2.[Location Code] = 'SUNRISE' THEN 1
                 WHEN SalesInvLine2.[Location Code] = 'TDC' THEN 2
                 WHEN SalesInvLine2.[Location Code] = 'TSFL' THEN 3
                 WHEN SalesInvLine2.[Location Code] = 'TDCWMS' THEN 4
                 WHEN SalesInvLine2.[Location Code] = 'MDCWMS' THEN 5
				 WHEN SalesInvLine2.[Location Code] = 'RDCWMS' THEN 7
                 ELSE 6
            END AS LocationID,
            LEFT(SalesInvHeader2.[Sell-to Post Code], 4) AS RegionID,
            SalesInvHeader2.[Posting Date]
    FROM    [NAV_ETL].dbo.[Jason Pharm$Sales Cr_Memo Line] SalesInvLine2 WITH ( NOLOCK )
            INNER JOIN [NAV_ETL].dbo.[Jason Pharm$Sales Cr_Memo Header] SalesInvHeader2 WITH ( NOLOCK ) ON SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
            LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item WITH ( NOLOCK ) ON Item.No_ COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_]
            LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
            WITH ( NOLOCK ) ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_] )
                               AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
                                                              WHEN SalesInvLine2.[Item Category Code] = 'RTD'
                                                              THEN 'CS'
                                                              ELSE Item.[Sales Unit of Measure]
                                                              END )
             Left join [NAV_ETL].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION] a WITH ( NOLOCK )
             on  SalesInvLine2.No_ COLLATE DATABASE_DEFAULT =a.[MOA_SKU]
             and  a.[MOA_ORL_ID] =[Order Item No_]                                                  
    WHERE   SalesInvHeader2.[Shortcut Dimension 1 Code] <> ' '
            AND SalesInvLine2.No_ NOT IN ( 'RC', 'RCC' )
            --and ( SalesInvHeader2.[Posting Date]>='2011-01-01')
            AND ( SalesInvHeader2.[Posting Date] >= DATEADD(dd,
                                                            DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                            0) )/*** Section 1***/
            AND [Customer Posting Group] not in ( 'CORPCLINIC','CANCO')
