﻿CREATE PROCEDURE [dbo].[BI_NAV_RecordCount]
AS
SELECT
  'Database',
  'Table',
  'DP-BIDB2',
  'DP-BIDB'

SELECT
  'NAV_ETL',
  'Jason Pharm$Item',
  COUNT(*)
FROM [NAV_ETL].[dbo].[Jason Pharm$Item]

SELECT
  'NAV_ETL',
  'Jason Pharm$Sales Cr_Memo Header',
  COUNT(*)
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] 

SELECT
  'NAV_ETL',
  'Jason Pharm$Sales Cr_Memo Line',
  COUNT(*)
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line]

SELECT
  'NAV_ETL',
  'Jason Pharm$Sales Invoice Header',
  COUNT(*)
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] 

SELECT
  'NAV_ETL',
  'Jason Pharm$Sales Invoice Line',
  COUNT(*)
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] 

SELECT
  'Book4Time_ETL',
  'B4T_product_master',
  COUNT(*)
FROM [Book4Time_ETL].[dbo].[B4T_product_master]

SELECT
  'Book4Time_ETL',
  'B4T_Inventory_Transactions',
  COUNT(*)
FROM [Book4Time_ETL].[dbo].B4T_Inventory_Transactions

SELECT
  'Book4Time_ETL',
  'B4T_transaction_log_detail',
  COUNT(*)
FROM [Book4Time_ETL].[dbo].B4T_transaction_log_detail
