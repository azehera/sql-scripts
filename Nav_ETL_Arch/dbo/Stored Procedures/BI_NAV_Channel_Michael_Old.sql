﻿



/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_Channel_Michael_Old]
DEVELOPER:        
DATE:             
DESCRIPTION:      
PARAMETERS/VARRIABLES:
NOTES:
------------------------------ MODIFICATIONS ----------------------------------------
DEVELOPER:  Kalpesh Patel
DATE:       8/7/2012
RFC#:       629
DESCRIPTION: Section 10: Remove Misc Item Category because now only NON-ITEM category insted of 'NPD' and 'MISC' and For NON-ITEMs no need to calculate QTY
            Section 11: Remove Misc Item Category because now only NON-ITEM category insted of 'NPD' and 'MISC' and For NON-ITEMs no need to calculate QTY
            Section 12: Remove Misc Item Category because now only NON-ITEM category insted of 'NPD' and 'MISC' and For NON-ITEMs no need to calculate QTY****/
-------------------------------------------------------------------------------------------
/*******************************************************************************************/


Create PROCEDURE [dbo].[BI_NAV_Channel_Michael_Old] (@DateEnd AS DATETIME)
AS
/****** MED0001 MRH 10/18/11: Business Intelligence / Executive Dashboard ******/

/****** SECTION 1:  INITIALIZATION *********************************************/

DECLARE @DateMonthBegin DATETIME
DECLARE @DateMonthEnd DATETIME
DECLARE @DateYearBegin DATETIME
DECLARE @DateYearEnd DATETIME
DECLARE @DateYesterday DATETIME
DECLARE @DatePriorYrMonthBegin DATETIME
DECLARE @DatePriorYrYearBegin DATETIME
DECLARE @DatePriorYrYesterday DATETIME
DECLARE @DaysMonth INT
DECLARE @DaysMonthElapsed INT
DECLARE @DaysMonthRemaining INT
DECLARE @DaysYear INT
DECLARE @DaysYearElapsed INT
DECLARE @DaysYearRemaining INT
DECLARE @ProcessTimeBegin DATETIME
DECLARE @ProcessTimeEnd DATETIME

SET @ProcessTimeBegin = GetDate()
SET @DateEnd = CAST(FLOOR(CAST(@DateEnd AS float)) AS DATETIME)
SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1

/****** SECTION 2:  INITIAL DATA LOAD ******************************************/

BEGIN TRANSACTION

DELETE FROM [Snapshot$NAV_Channel]
WHERE (RunDate = @DateEnd)

INSERT INTO [Snapshot$NAV_Channel]
  (RunDate,StartDateYear,StartDateMonth,Yesterday,YesterdayPriorYr,StartDateMonthPriorYr,StartDateYearPriorYr,
   CPG,SalesSrce,
   NetShipRevDay,NetShipRevMTD,
   InvSalesDay,InvSalesMTD,InvSalesPriorYrMTD,InvSalesYTD,InvSalesPriorYrYTD,
   InvUnitsDay,InvUnitsMTD,InvUnitsPriorYrMTD,InvUnitsYTD,InvUnitsPriorYrYTD,
   InvOrdersDay,InvOrdersMTD,InvOrdersPriorYrMTD,InvOrdersYTD,InvOrdersPriorYrYTD,
   InvExtCostDay,InvExtCostMTD,InvExtCostYTD)
SELECT
  @DateEnd,
  @DateYearBegin,
  @DateMonthBegin,
  @DateYesterday,
  @DatePriorYrYesterday,
  @DatePriorYrMonthBegin,
  @DatePriorYrYearBegin,
  CPG1.Code,
  DimValue1.Code,
  dbo.NavChan_GetNetShipRevDateRange (CPG1.Code, DimValue1.Code, @DateYesterday, @DateYesterday) AS _NetShipRevDay,
  dbo.NavChan_GetNetShipRevDateRange (CPG1.Code, DimValue1.Code, @DateMonthBegin, @DateYesterday) AS _NetShipRevMTD,
  dbo.NavChan_GetAmtDateRange (CPG1.Code, DimValue1.Code, @DateYesterday, @DateYesterday) AS _InvSalesDay,
  dbo.NavChan_GetAmtDateRange (CPG1.Code, DimValue1.Code, @DateMonthBegin, @DateYesterday) AS _InvSalesMTD,
  dbo.NavChan_GetAmtDateRange (CPG1.Code, DimValue1.Code, @DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrMTD,
  dbo.NavChan_GetAmtDateRange (CPG1.Code, DimValue1.Code, @DateYearBegin, @DateYesterday) AS _InvSalesYTD,
  dbo.NavChan_GetAmtDateRange (CPG1.Code, DimValue1.Code, @DatePriorYrYearBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrYTD,
  dbo.NavChan_GetQtyDateRange (CPG1.Code, DimValue1.Code, @DateYesterday, @DateYesterday) AS _InvUnitsDay,
  dbo.NavChan_GetQtyDateRange (CPG1.Code, DimValue1.Code, @DateMonthBegin, @DateYesterday) AS _InvUnitsMTD,
  dbo.NavChan_GetQtyDateRange (CPG1.Code, DimValue1.Code, @DatePriorYrMonthBegin, @DatePriorYrYesterday) as _InvUnitsPriorYrMTD,
  dbo.NavChan_GetQtyDateRange (CPG1.Code, DimValue1.Code, @DateYearBegin, @DateYesterday) AS _InvUnitsYTD,
  dbo.NavChan_GetQtyDateRange (CPG1.Code, DimValue1.Code, @DatePriorYrYearBegin, @DatePriorYrYesterday) as _InvUnitsPriorYrYTD,
  dbo.NavChan_GetOrdDateRange (CPG1.Code, DimValue1.Code, @DateYesterday, @DateYesterday) AS _InvOrdersDay,
  dbo.NavChan_GetOrdDateRange (CPG1.Code, DimValue1.Code, @DateMonthBegin, @DateYesterday) AS _InvOrdersMTD,
  dbo.NavChan_GetOrdDateRange (CPG1.Code, DimValue1.Code, @DatePriorYrMonthBegin, @DatePriorYrYesterday) as _InvOrdersPriorYrMTD,
  dbo.NavChan_GetOrdDateRange (CPG1.Code, DimValue1.Code, @DateYearBegin, @DateYesterday) AS _InvOrdersYTD,
  dbo.NavChan_GetOrdDateRange (CPG1.Code, DimValue1.Code, @DatePriorYrYearBegin, @DatePriorYrYesterday) as InvOrdersPriorYrYTD,
  dbo.NavChan_GetCostDateRange (CPG1.Code, DimValue1.Code, @DateYesterday, @DateYesterday) AS _InvExtCostDay,
  dbo.NavChan_GetCostDateRange (CPG1.Code, DimValue1.Code, @DateMonthBegin, @DateYesterday) AS _InvExtCostMTD,
  dbo.NavChan_GetCostDateRange (CPG1.Code, DimValue1.Code, @DateYearBegin, @DateYesterday) AS _InvExtCostYTD
FROM [Jason Pharm$Customer Posting Group] CPG1
JOIN [Jason Pharm$Dimension Value] DimValue1
  ON [Dimension Code] COLLATE DATABASE_DEFAULT = 'SALESSRCE'

COMMIT TRANSACTION

/****** SECTION 3:  UPDATE DATA FROM BOOK4TIME *********************************/

BEGIN TRANSACTION

UPDATE [NAV_ETL].[dbo].[Snapshot$NAV_Channel]
   SET [InvSalesDay] = dbo.NavProd_B4T_GetAmtDateRange (@DateYesterday, @DateYesterday)
      ,[InvOrdersDay] = dbo.NavProd_B4T_GetOrdDateRange (@DateYesterday, @DateYesterday)
      ,[InvCreditsDay] = dbo.NavProd_B4T_GetAmtCrDateRange (@DateYesterday, @DateYesterday)
      ,[InvUnitsDay] = dbo.NavProd_B4T_GetQtyDateRange (@DateYesterday, @DateYesterday)
      ,[InvExtCostDay] = dbo.NavProd_B4T_GetCostDateRange (@DateYesterday, @DateYesterday)
      ,[InvSalesMTD] = dbo.NavProd_B4T_GetAmtDateRange (@DateMonthBegin, @DateYesterday)
      ,[InvSalesPriorYrMTD] = dbo.NavProd_B4T_GetAmtDateRange (@DatePriorYrMonthBegin, @DatePriorYrYesterday)
      ,[InvOrdersMTD] = dbo.NavProd_B4T_GetOrdDateRange (@DateMonthBegin, @DateYesterday)
      ,[InvOrdersPriorYrMTD] = dbo.NavProd_B4T_GetOrdDateRange (@DatePriorYrMonthBegin, @DatePriorYrYesterday)
      ,[InvOrdersPriorYrYTD] = dbo.NavProd_B4T_GetOrdDateRange (@DatePriorYrYearBegin, @DatePriorYrYesterday)
      ,[InvUnitsMTD] = dbo.NavProd_B4T_GetQtyDateRange (@DateMonthBegin, @DateYesterday)
      ,[InvUnitsPriorYrMTD] = dbo.NavProd_B4T_GetQtyDateRange (@DatePriorYrMonthBegin, @DatePriorYrYesterday)
      ,[InvUnitsPriorYrYTD] = dbo.NavProd_B4T_GetQtyDateRange (@DatePriorYrYearBegin, @DatePriorYrYesterday)
      ,[InvExtCostMTD] = dbo.NavProd_B4T_GetCostDateRange (@DateMonthBegin, @DateYesterday)
      ,[InvSalesYTD] = dbo.NavProd_B4T_GetAmtDateRange (@DateYearBegin, @DateYesterday)
      ,[InvSalesPriorYrYTD] = dbo.NavProd_B4T_GetAmtDateRange (@DatePriorYrYearBegin, @DatePriorYrYesterday)
      ,[InvOrdersYTD] = dbo.NavProd_B4T_GetOrdDateRange (@DateYearBegin, @DateYesterday)
      ,[InvUnitsYTD] = dbo.NavProd_B4T_GetQtyDateRange (@DateYearBegin, @DateYesterday)
      ,[InvExtCostYTD] = dbo.NavProd_B4T_GetCostDateRange (@DateYearBegin, @DateYesterday)
   WHERE ([RunDate] = @DateEnd) AND ([CPG] = 'CORPCLINIC') AND ([SalesSrce] = 'CC')

COMMIT TRANSACTION

/****** SECTION 3a: REMOVE FROM AGGREGATE TABLE ROW MATCHING *******************/
/******             CPG=CORPCLINIC, SalesSrce=FRANCHISE **BECAUSE** ************/
/******             THE AGGREGATE DATA FROM B4T INCLUDES THIS ******************/

BEGIN TRANSACTION

DELETE FROM [NAV_ETL].[dbo].[Snapshot$NAV_Channel]
WHERE ([RunDate] = @DateEnd) AND ([CPG] = 'CORPCLINIC') AND ([SalesSrce] = 'FRANCHISE')

COMMIT TRANSACTION

/****** SECTION 4:  UPDATE DATA FROM BOOK4TIME WHERE ITEMS NOT FOUND IN NAV ****/
/******             IN NAV AND product_type_cd IN B4T_product_master IS "p" ****/

BEGIN TRANSACTION

UPDATE [NAV_ETL].[dbo].[Snapshot$NAV_Channel]
   SET [InvUnitsDay] = ISNULL([InvUnitsDay],0) /****+ dbo.NavProd_B4T_GetICCMiscQtyDateRange (@DateYesterday, @DateYesterday)****/ /***Section 10: Remove Misc Item Category because now only NON-ITEM category insted of 'NPD' and 'MISC' and For NON-ITEMs no need to calculate QTY****/
      ,[InvUnitsMTD] = ISNULL([InvUnitsMTD],0) /****+ dbo.NavProd_B4T_GetICCMiscQtyDateRange (@DateMonthBegin, @DateYesterday)****//***Section 11: Remove Misc Item Category because now only NON-ITEM category insted of 'NPD' and 'MISC' and For NON-ITEMs no need to calculate QTY****/
      ,[InvUnitsYTD] = ISNULL([InvUnitsYTD],0) /****+ dbo.NavProd_B4T_GetICCMiscQtyDateRange (@DateYearBegin, @DateYesterday)****//***Section 12: Remove Misc Item Category because now only NON-ITEM category insted of 'NPD' and 'MISC' and For NON-ITEMs no need to calculate QTY****/
   WHERE ([RunDate] = @DateEnd) AND ([CPG] = 'CORPCLINIC') AND ([SalesSrce] = 'CC')

COMMIT TRANSACTION

/****** SECTION 5:  UPDATE DATA FROM BOOK4TIME HISTORICAL TABLES ***************/

BEGIN TRANSACTION

UPDATE [NAV_ETL].[dbo].[Snapshot$NAV_Channel]
   SET [InvSalesDay] = ISNULL([InvSalesDay],0) + dbo.NavProd_B4T_HIST_GetAmtDateRange (@DateYesterday, @DateYesterday)
      ,[InvSalesMTD] = ISNULL([InvSalesMTD],0) + dbo.NavProd_B4T_HIST_GetAmtDateRange (@DateMonthBegin, @DateYesterday)
      ,[InvSalesPriorYrMTD] = ISNULL([InvSalesPriorYrMTD],0) + dbo.NavProd_B4T_HIST_GetAmtDateRange (@DatePriorYrMonthBegin, @DatePriorYrYesterday)
      ,[InvSalesYTD] = ISNULL([InvSalesYTD],0) + dbo.NavProd_B4T_HIST_GetAmtDateRange (@DateYearBegin, @DateYesterday)
      ,[InvSalesPriorYrYTD] = ISNULL([InvSalesPriorYrYTD],0) + dbo.NavProd_B4T_HIST_GetAmtDateRange (@DatePriorYrYearBegin, @DatePriorYrYesterday)
 WHERE ([RunDate] = @DateEnd) AND ([CPG] = 'CORPCLINIC') AND ([SalesSrce] = 'CC')

COMMIT TRANSACTION

/****** SECTION 6:  UPDATE PROJECTIONS *****************************************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Channel]
SET InvSalesMoProj = (InvSalesMTD / NULLIF(@DaysMonthElapsed,0)) * @DaysMonth
WHERE (RunDate = @DateEnd) AND (InvSalesMTD <> 0)

UPDATE [Snapshot$NAV_Channel]
SET NetShipRevMoProj = (NetShipRevMTD / NULLIF(@DaysMonthElapsed,0)) * @DaysMonth
WHERE (RunDate = @DateEnd) AND (NetShipRevMTD <> 0)

UPDATE [Snapshot$NAV_Channel]
SET InvSalesProjYE = (InvSalesYTD / NULLIF(@DaysYearElapsed,0)) * @DaysYear
WHERE (RunDate = @DateEnd) AND (InvSalesYTD <> 0)

COMMIT TRANSACTION

/****** SECTION 7:  HARDCODED SORT ORDER OF DIVISION PER DOMINIC ***************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Channel]
SET SortOrder = 1000
WHERE (CPG = 'TSFL') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder = 900
WHERE (CPG = 'MEDIFAST') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder = 800
WHERE (CPG = 'CORPCLINIC') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder = 700
WHERE (CPG = 'FRANCHISE') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder = 600
WHERE (CPG = 'DOCTORS') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder = -100
WHERE (CPG = 'CORPHEALTH') AND (SortOrder = 0)

COMMIT TRANSACTION

/****** SECTION 8:  HARDCODED SORT ORDER OF CHANNEL PER DOMINIC ****************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 1000
WHERE (SalesSrce = 'AUTO') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 900
WHERE (SalesSrce = 'CC') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 800
WHERE (SalesSrce = 'WEB') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 700
WHERE (SalesSrce = 'POS') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 600
WHERE (SalesSrce = 'FRANCHISE') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 500
WHERE (SalesSrce = 'WEST') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 400
WHERE (SalesSrce = 'CCDOC') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 300
WHERE (SalesSrce = 'WEBDOC') AND (SortOrder2 = 0)

UPDATE [Snapshot$NAV_Channel]
SET SortOrder2 = 200
WHERE (SalesSrce = 'TSFL') AND (SortOrder2 = 0)

COMMIT TRANSACTION

/****** SECTION 9:  REPORT DURATION OF QUERY ***********************************/

SET @ProcessTimeEnd = GetDate()
PRINT 'Elapsed time: '
PRINT DATEDIFF(SS,@ProcessTimeBegin,@ProcessTimeEnd)





