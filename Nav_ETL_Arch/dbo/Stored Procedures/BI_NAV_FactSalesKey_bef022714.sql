﻿



/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_FactSalesKey] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Fact data for cube from Nav_ETL database     
PARAMETERS/VARRIABLES:
NOTES: Selection 1 Loading Data for 30Days 

*******************************************************************************************/


CREATE PROCEDURE [dbo].[BI_NAV_FactSalesKey_bef022714]
AS 
    SELECT  'USA' as Country,
            SalesInvHeader2.[Customer Posting Group] AS [CustomerPostingGroup] ,
            SalesInvHeader2.[Shortcut Dimension 1 Code] AS SalesChannel ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN 'NON-ITEM'
                 WHEN SalesInvLine2.Type = 2
                      AND ( SalesInvLine2.[Item Category Code] COLLATE DATABASE_DEFAULT NOT IN (
                            SELECT  Code
                            FROM    NAV_ETL.dbo.[Jason Pharm$Item Category] ) )
                 THEN 'UNCATEGORIZED'
                 ELSE SalesInvLine2.[Item Category Code]
            END AS [ItemCategoryCode] ,
            SalesInvHeader2.[Sell-to Customer No_] AS SelltoCustomerID ,
            SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Ship-to Post Code], 5) AS ShiptoCustomerKey ,
            SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Sell-to Post Code],5) AS SelltoCustomerKey ,
            SalesInvLine2.No_ AS ItemCode ,
            [Document No_] AS DocumentNo ,
            [Line Discount Amount] AS LineDiscount ,
            ( [Inv_ Discount Amount] ) AS InvoiceDiscount ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN '0'
                 WHEN SalesInvLine2.[Item Category Code] = 'NPD' THEN '0'
                 ELSE ([Quantity]*SalesInvLine2.[Qty_ per Unit of Measure])
            END AS Units ,
            ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsPerBox ,
            ( [Quantity (Base)] / ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') ) AS BOX ,
            [Amount] ,
            [Line Amount] AS [Gross Amount] ,
            [Amount Including VAT] ,
            CASE WHEN SalesInvLine2.[Tax Group Code] = ' '
                 THEN ( [Amount Including VAT] + ( -1 * [Amount] ) )
                 ELSE ( [Amount Including VAT] - [Amount] )
            END AS TaxAmount ,
            SalesInvLine2.[Tax Group Code] ,
            CASE WHEN [Unit Cost (LCY)] = 0 THEN 0
                 ELSE ( [Unit Cost (LCY)]
                        / SalesInvLine2.[Qty_ per Unit of Measure] )
            END AS [Unit Cost (LCY)] ,
             Quantity,
            
            1 AS SalesType ,
            CASE WHEN SalesInvLine2.[Location Code] = 'SUNRISE' THEN 1
                 WHEN SalesInvLine2.[Location Code] = 'TDC' THEN 2
                 WHEN SalesInvLine2.[Location Code] = 'TSFL' THEN 3
                 WHEN SalesInvLine2.[Location Code] = 'TDCWMS' THEN 4
                 WHEN SalesInvLine2.[Location Code] = 'MDCWMS' THEN 5
                 ELSE 6
            END AS LocationID ,
            LEFT(SalesInvHeader2.[Sell-to Post Code], 4) AS RegionID ,
            SalesInvHeader2.[Posting Date]
    FROM    NAV_ETL.dbo.[Jason Pharm$Sales Invoice Line] SalesInvLine2 WITH ( NOLOCK )
            INNER JOIN NAV_ETL.dbo.[Jason Pharm$Sales Invoice Header] SalesInvHeader2 WITH ( NOLOCK ) ON SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
            LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item WITH ( NOLOCK ) ON Item.No_ COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_]
            LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
            WITH ( NOLOCK ) ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_] )
                               AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
                                                              WHEN SalesInvLine2.[Item Category Code] = 'RTD'
                                                              THEN 'CS'
                                                              ELSE Item.[Sales Unit of Measure]
                                                              END )
    WHERE   SalesInvHeader2.[Shortcut Dimension 1 Code] <> ' '
            AND SalesInvLine2.No_ NOT IN ( 'RC', 'RCC' )
            --and ( SalesInvHeader2.[Posting Date]>='2011-01-01')
            AND ( SalesInvHeader2.[Posting Date] >= DATEADD(dd,
                                                            DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                            0) )/*** Section 1***/
            AND [Customer Posting Group] <> 'CORPCLINIC'
    UNION ALL
    SELECT  'USA' as Country,
            SalesInvHeader2.[Customer Posting Group] AS [CustomerPostingGroup] ,
            SalesInvHeader2.[Shortcut Dimension 1 Code] AS SalesChannel ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN 'NON-ITEM'
                 WHEN SalesInvLine2.Type = 2
                      AND ( SalesInvLine2.[Item Category Code] COLLATE DATABASE_DEFAULT NOT IN (
                            SELECT  Code
                            FROM   NAV_ETL.dbo.[Jason Pharm$Item Category] ) )
                 THEN 'UNCATEGORIZED'
                 ELSE SalesInvLine2.[Item Category Code]
            END AS [ItemCategoryCode] ,
            SalesInvHeader2.[Sell-to Customer No_] AS SelltoCustomerID ,
            SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Ship-to Post Code], 5) AS ShiptoCustomerKey ,
            SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Sell-to Post Code],5) AS SelltoCustomerKey ,
            SalesInvLine2.No_ AS ItemCode ,
            [Document No_] AS DocumentNo ,
            [Line Discount Amount] AS LineDiscount ,
            ( [Inv_ Discount Amount] * -1 ) AS InvoiceDiscount ,
            CASE WHEN SalesInvLine2.Type <> 2 THEN '0'
                 WHEN SalesInvLine2.[Item Category Code] = 'NPD' THEN '0'
                 ELSE ([Quantity]*SalesInvLine2.[Qty_ per Unit of Measure])*-1
            END AS Units ,
            ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsperBox ,
            ( [Quantity (Base)] / ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') ) AS BOX ,
            [Amount] * -1 AS Amount ,
            ( [Line Amount] * -1 ) AS [Gross Amount] ,
            [Amount Including VAT] ,
            CASE WHEN SalesInvLine2.[Tax Group Code] = ' '
                 THEN ( [Amount Including VAT] + ( -1 * [Amount] ) )
                 ELSE ( [Amount Including VAT] - [Amount] ) * -1
            END AS TaxAmount ,
            SalesInvLine2.[Tax Group Code] ,
            CASE WHEN [Unit Cost (LCY)] = 0 THEN 0
                 ELSE ( [Unit Cost (LCY)]
                        / SalesInvLine2.[Qty_ per Unit of Measure] )
            END AS [Unit Cost (LCY)] ,
             Quantity * -1  AS Quantity ,
            2 AS SalesType ,
            CASE WHEN SalesInvLine2.[Location Code] = 'SUNRISE' THEN 1
                 WHEN SalesInvLine2.[Location Code] = 'TDC' THEN 2
                 WHEN SalesInvLine2.[Location Code] = 'TSFL' THEN 3
                 WHEN SalesInvLine2.[Location Code] = 'TDCWMS' THEN 4
                 WHEN SalesInvLine2.[Location Code] = 'MDCWMS' THEN 5
                 ELSE 6
            END AS LocationID ,
            LEFT(SalesInvHeader2.[Sell-to Post Code], 4) AS RegionID ,
            SalesInvHeader2.[Posting Date]
    FROM    NAV_ETL.dbo.[Jason Pharm$Sales Cr_Memo Line] SalesInvLine2 WITH ( NOLOCK )
            INNER JOIN NAV_ETL.dbo.[Jason Pharm$Sales Cr_Memo Header] SalesInvHeader2 WITH ( NOLOCK ) ON SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
            LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item WITH ( NOLOCK ) ON Item.No_ COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_]
            LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
            WITH ( NOLOCK ) ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_] )
                               AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
                                                              WHEN SalesInvLine2.[Item Category Code] = 'RTD'
                                                              THEN 'CS'
                                                              ELSE Item.[Sales Unit of Measure]
                                                              END )
    WHERE   SalesInvHeader2.[Shortcut Dimension 1 Code] <> ' '
            AND SalesInvLine2.No_ NOT IN ( 'RC', 'RCC' )
            --and ( SalesInvHeader2.[Posting Date]>='2011-01-01')
            AND ( SalesInvHeader2.[Posting Date] >= DATEADD(dd,
                                                            DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                            0) )/*** Section 1***/
            AND [Customer Posting Group] <> 'CORPCLINIC'
       

      
       
       



















































































































