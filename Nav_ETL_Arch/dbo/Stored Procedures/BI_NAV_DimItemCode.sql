﻿
/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_DimItemCode] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Dimesion data for cube     
PARAMETERS/VARIABLES:
NOTES: Fix applied to remove TAB from B4T Item Codes
	   Added temp table to allow refinement of item codes included
Modification Date : 03/08/2017 
DEVELOPER: Ronak Shah
NOTES: New Fields are required to add into Sales Cube so modified this stored procedure. 	   
Modification Date : 04/18/2017
DEVELOPER: Ronak Shah
NOTES: [Nav_ETL].[dbo].[Jason Pharm$Item].[Manufacturer Code]  include this field as [MKTG Brand]
DEVELOPER: Ronak Shah
Notes: Rewrite the entire stored procedure to get data from MDM_Reltio DB (Product Master tables)
Note: Convert Null value to Empty string so that SP won't Fail. 
DATE: 09/25/2020
Note: DEHA-528 Add logic to update Marketing Fields
*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_DimItemCode] 
as
truncate table BI_SSAS_Cubes.dbo.DimItemcode
truncate table BI_SSAS_Cubes.dbo.DimSubCategoryItemCode		-- This table is recreated by a SQL step in SSIS package



IF object_id('Tempdb..#ItemCode') Is Not Null DROP TABLE #ItemCode
Create table #ItemCode(
	[Itemcode] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[Product Group Code] [nvarchar](100) NULL,
	[Item Category Code] [nvarchar](100) NULL,
	[Discontinued] [nvarchar](25) NULL,
	[MKTG Type] [nvarchar](20) NULL,
	[MKTG Category] [nvarchar](20) NULL,
	[MKTG Subcategory] [nvarchar](20) NULL,
	[Launch Date] [datetime] NULL,
	[MKTG Status] [nvarchar](50) NULL,
	[MKTG Brand] [nvarchar](50) NULL
) 

INSERT #ItemCode(
	[Itemcode],
	[Description],
	[Product Group Code],
	[Item Category Code],
	[Discontinued],
	[MKTG Type],
	[MKTG Category],
	[MKTG Subcategory],
	[Launch Date],
	[MKTG Status],
	[MKTG Brand]
) 
SELECT 	ISNULL([Itemcode],'') as [Itemcode],
	[Description],
	ProductGroupCode as [Product Group Code],
	ItemCategoryCode as [Item Category Code],
	[Discontinued],
	[MKTG Type],
	[MKTG Category],
	[MKTG Subcategory],
	[Launch Date],
	[MKTG Status],
	[MKTG Brand]
FROM (
select distinct ItemID as Itemcode, ItemName_1 as Description, '' as ProductGroupCode, '' as ItemCategoryCode,
case when ItemStatus= 'Active' then 'No'
	when ItemStatus IN ('Inactive/Discontinued','Discontinued') then 'Yes'
	else ItemStatus 
	End as Discontinued,
	'' as [MKTG Type] , 
	'' as [MKTG Category], 
 	'' as [MKTG Subcategory] , 
	'' as [Launch Date] , 
	'' as [MKTG Status],
	'' as [MKTG Brand]
 
from 
[MDM_RELTIO].[MDM_Reltio].[dbo].[RELTIO_MDM_ITEM]
 

union

select distinct ItemSKU as ItemId, Name, '' as ProductGroupCode, '' as ItemCategoryCode,
case when IsItemdiscontinued= 'Active' then 'No'
	when IsItemdiscontinued IN ('Inactive/Discontinued','Discontinued') then 'Yes'
	else IsItemdiscontinued 
	End as Discontinued,
	MKTG_Type,
	MKTG_Category,
	MKTG_Subcategory,
	LaunchDate,
	MKTG_Status,
	MKTG_Brand
from [MDM_RELTIO].[MDM_Reltio].[dbo].[RELTIO_MDM_PRODUCT]

union 
 

select distinct KitSKU, KitName, '' as ProductGroupCode, '' as ItemCategoryCode,'' as Discontinued,
	'' as [MKTG Type] , 
	'' as [MKTG Category], 
 	'' as [MKTG Subcategory] , 
	'' as [Launch Date] , 
	'' as [MKTG Status],
	'' as [MKTG Brand]
from [MDM_RELTIO].[MDM_Reltio].[dbo].[RELTIO_MDM_KIT]
--where KitType != 'SUBKIT'  --Please confirm with MDM team and get it resolved. 
)DC


INSERT #ItemCode(
	[Itemcode],
	[Description]) 
	Values ('HOHDRABK','HOHDRABK')

 
/********Lookup from DimItemCode_Backup table*********************/



INSERT #ItemCode(
	[Itemcode],
	[Description],
	[Product Group Code],
	[Item Category Code],
	[Discontinued],
	[MKTG Type],
	[MKTG Category],
	[MKTG Subcategory],
	[Launch Date],
	[MKTG Status],
	[MKTG Brand]
) 
Select Itemcode,Description,[Product Group Code],
[Item Category Code],Discontinued,
[MKTG Type] , 
[MKTG Category], 
[MKTG Subcategory] , 
[Launch Date] , 
[MKTG Status],
[MKTG Brand] 
From BI_SSAS_Cubes.dbo.DimItemcode_Bkup
where Itemcode not in (Select ItemCode from #ItemCode)

/************** Delete Itemcode with different name but same ID ************/

;with cte as 
(SELECT  [Itemcode], [Description]
      ,ROW_NUMBER() over (Partition by ItemCode order by ItemCode) as no
  FROM #ItemCode)
  Delete from cte
  where no>1

/************** Insert into DimItemcode ********************/

insert into BI_SSAS_Cubes.dbo.DimItemcode 
(
Itemcode,Description,[Product Group Code],
[Item Category Code],Discontinued,
[MKTG Type] , 
[MKTG Category], 
[MKTG Subcategory] , 
[Launch Date] , 
[MKTG Status],
[MKTG Brand]  
) 
SELECT 
	[Itemcode],
	[Description],
	[Product Group Code],
	[Item Category Code],
	[Discontinued],
	[MKTG Type],
	[MKTG Category],
	[MKTG Subcategory],
	[Launch Date],
	[MKTG Status],
	[MKTG Brand]
from #ItemCode


update [BI_SSAS_Cubes].[dbo].[DimItemCode]
set [Product Group Code]='NON-ITEM'
where [Product Group Code] is null
  
update [BI_SSAS_Cubes].[dbo].[DimItemCode]
set [Item Category Code]='NON-ITEM'
where [Item Category Code] is null
  
update [BI_SSAS_Cubes].[dbo].[DimItemCode]
set [ItemCode]=' '
where [ItemCode] is null
  
update [BI_SSAS_Cubes].[dbo].[DimItemCode]
set [Item Category Code]='NON-ITEM'
where [Item Category Code]=' '
  
update [BI_SSAS_Cubes].[dbo].[DimItemCode]
set [Product Group Code]='NON-ITEM'
where [Product Group Code]=' '

Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Brand] =''
where [MKTG Brand] is null 


Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Type] =''
where [MKTG Type] is null 


Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Category] =''
where [MKTG Category] is null 


Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Subcategory] =''
where [MKTG Subcategory] is null 

Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Status] =''
where [MKTG Status] is null 

Update BI_SSAS_CUBES.dbo.DimItemCode
set discontinued = NULL
where discontinued =''


/* To update MKTG Fields */--DEHA-528
IF object_id('Tempdb..#Product') Is Not Null DROP TABLE #Product
select ItemSKU as ItemId, Name, '' as ProductGroupCode, '' as ItemCategoryCode,
	case when IsItemdiscontinued= 'Active' then 'No'
		when IsItemdiscontinued IN ('Inactive/Discontinued','Discontinued') then 'Yes'
		else IsItemdiscontinued 
		End as Discontinued,
		MKTG_Type,
		MKTG_Category,
		MKTG_Subcategory,
		LaunchDate,
		MKTG_Status,
		MKTG_Brand
		INTO #Product
	from [MDM_RELTIO].[MDM_Reltio].[dbo].[RELTIO_MDM_PRODUCT]

Update DI
set [MKTG Type] = p.MKTG_Type
from #Product p
Inner Join BI_SSAS_CUBES.dbo.DimItemCode DI
on p.ItemId = DI.Itemcode
where DI.[MKTG Type] =''

Update DI
set [MKTG Category] = p.MKTG_Category
from #Product p
Inner Join BI_SSAS_CUBES.dbo.DimItemCode DI
on p.ItemId = DI.Itemcode
where DI.[MKTG Category] =''


Update DI
set [MKTG Subcategory] = p.MKTG_Subcategory
from #Product p
Inner Join BI_SSAS_CUBES.dbo.DimItemCode DI
on p.ItemId = DI.Itemcode
where DI.[MKTG Subcategory] =''

Update DI
set [MKTG Brand] = p.MKTG_Brand
from #Product p
Inner Join BI_SSAS_CUBES.dbo.DimItemCode DI
on p.ItemId = DI.Itemcode
where DI.[MKTG Brand]  =''


Update DI
set [MKTG Status] = p.MKTG_Status
from #Product p
Inner Join BI_SSAS_CUBES.dbo.DimItemCode DI
on p.ItemId = DI.Itemcode
where DI.[MKTG Status]  =''






Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Brand] =''
where [MKTG Brand] is null 


Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Type] =''
where [MKTG Type] is null 


Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Category] =''
where [MKTG Category] is null 


Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Subcategory] =''
where [MKTG Subcategory] is null 

Update BI_SSAS_CUBES.dbo.DimItemCode
Set [MKTG Status] =''
where [MKTG Status] is null 

