﻿
--/*
--===============================================================================
--Author:         Emily Trainor
--Create date: 01/10/2012
---------------------------TSFL Tracker TSFL Sponsoring-------------------------
-------Pull data for TSFL Sponsoring Business Week Graph------------------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--BI_Reporting			XL_016_TSFL_LY_TY			               Select                       
--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------
--05/25/2012	  Emily Trainor					Updated to pull by order not SKU
--06/22/2012	  Emily Trainor					Updated to pull Business Week
--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--===============================================================================
--*/

Create procedure [dbo].[USP_XL_016_TSFL_Sponsoring_GetData] as 
set nocount on ;

-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = '01/01/' + Convert(varchar(4), DatePart(yyyy, DateAdd(yyyy, -1, GETDATE())))
---------------------Pulling TSFL Sponsoring Data ------------------------------------
SELECT 
 [Posting Date]
,[Calendar Week]
,[Calendar Year]
,[Orders]
INTO #a 
FROM [BI_Reporting].dbo.XL_016_TSFL_LY_TY
where [Posting Date] >= @DATE
and [Customer Posting Group] in ('TSFL')
and [No_] in('31011','31012','32051','31100','31105','31110')
order by [Posting Date] asc

-----Pull order count figure for orders containing sponsoring SKU aggregate to Bus. Week-----
Select
	 A.[Calendar Week]
	,A.[Calendar Year]
	,sum(B.[OrderCount]) as [Total Orders]
From #a A
	Inner join [BI_Reporting].dbo.XL_016_TSFL_LY_TY B
		on A.[Orders] = B.Orders
Group By 
	 A.[Calendar Year]
	,A.[Calendar Week]
Order by 
	 A.[Calendar Year]
	,A.[Calendar Week]

---clean up---
drop table #a