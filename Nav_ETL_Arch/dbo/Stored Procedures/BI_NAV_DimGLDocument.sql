﻿
CREATE procedure [dbo].[BI_NAV_DimGLDocument]
as
SET NOCOUNT ON

truncate table BI_SSAS_Cubes.dbo.DimGLDocument

Select 
     distinct c.[Document No_]COLLATE DATABASE_DEFAULT+''+[G_L Account No_] as DocumentID,
     c.[Document No_] COLLATE DATABASE_DEFAULT as [Document No_],
     [Vendor No_] COLLATE DATABASE_DEFAULT as [Vendor No_] ,
     case when Name='' then null else Name COLLATE DATABASE_DEFAULT end as VendorName  
         from NAV_ETL.[dbo].[Jason Pharm$Vendor Ledger Entry] a
              join  NAV_ETL.dbo.[Jason Pharm$Vendor] b
                    on a.[Vendor No_] COLLATE DATABASE_DEFAULT =b.No_
              right join  NAV_ETL.dbo.[Jason Pharm$G_L Entry] c
                    on c.[Document No_] COLLATE DATABASE_DEFAULT =a.[Document No_]
         where c.[Posting Date]>='2011-01-01'
               and [G_L Account No_]>'10000'
               --and  [G_L Account No_] not in ('96750','85401')


union all

Select 
      distinct c.[Document No_]+''+[G_L Account No_],
      c.[Document No_],
      [Vendor No_],
          case when Name='' then null else Name COLLATE DATABASE_DEFAULT end  as VendorName  
           from NAV_ETL.[dbo].[7 Crondall$Vendor Ledger Entry] a
                join  NAV_ETL.dbo.[7 Crondall$Vendor] b
                      on a.[Vendor No_] COLLATE DATABASE_DEFAULT =b.No_
                right join NAV_ETL.dbo.[7 Crondall$G_L Entry] c
                      on c.[Document No_] COLLATE DATABASE_DEFAULT =a.[Document No_]
            where c.[Posting Date]>='2011-01-01'
                  --and [G_L Account No_] not in ('96750','43500')
                  and [G_L Account No_]>'10000'



union all

Select 
       distinct c.[Document No_]+''+[G_L Account No_],
       c.[Document No_],
       [Vendor No_],
           case when Name='' then null else Name COLLATE DATABASE_DEFAULT end as VendorName  
            from NAV_ETL.[dbo].[Jason Enterprises$Vendor Ledger Entry] a
                 join NAV_ETL.dbo.[Jason Enterprises$Vendor] b
                      on a.[Vendor No_] COLLATE DATABASE_DEFAULT =b.No_
                 right join  NAV_ETL.dbo.[Jason Enterprises$G_L Entry] c
                      on c.[Document No_] COLLATE DATABASE_DEFAULT =a.[Document No_]
            where c.[Posting Date]>='2011-01-01'
                  and [G_L Account No_]>'10000'


union all

Select 
      distinct c.[Document No_]+''+[G_L Account No_] ,
      c.[Document No_],
      [Vendor No_],
          case when Name='' then null else Name COLLATE DATABASE_DEFAULT end as VendorName  
           from NAV_ETL.[dbo].[Jason Properties$Vendor Ledger Entry] a
                 join  NAV_ETL.dbo.[Jason Properties$Vendor] b
                       on a.[Vendor No_] COLLATE DATABASE_DEFAULT =b.No_
                 right join  NAV_ETL.dbo.[Jason Properties$G_L Entry] c
                       on c.[Document No_] COLLATE DATABASE_DEFAULT =a.[Document No_]
           where c.[Posting Date]>='2011-01-01'
                 --and [G_L Account No_] not in ('96750')
                 and [G_L Account No_]>'10000'



union all

Select 
      distinct c.[Document No_]+''+[G_L Account No_] ,
      c.[Document No_],
      [Vendor No_],
          case when Name='' then null else Name COLLATE DATABASE_DEFAULT end as VendorName 
           from NAV_ETL.[dbo].[Medifast Inc$Vendor Ledger Entry] a
                join  NAV_ETL.dbo.[Medifast Inc$Vendor] b
                      on a.[Vendor No_] COLLATE DATABASE_DEFAULT =b.No_
                right join  NAV_ETL.dbo.[Medifast Inc$G_L Entry] c
                      on c.[Document No_] COLLATE DATABASE_DEFAULT=a.[Document No_]
           where c.[Posting Date]>='2011-01-01'
                --and [G_L Account No_] not in ('96750')
                and [G_L Account No_]>'10000'


union all


Select 
      distinct c.[Document No_]+''+[G_L Account No_] ,
      c.[Document No_],
      [Vendor No_],
          case when Name='' then null else Name COLLATE DATABASE_DEFAULT end as VendorName 
           from NAV_ETL.[dbo].[Take Shape For Life$Vendor Ledger Entry] a
                 join  NAV_ETL.dbo.[Take Shape For Life$Vendor] b
                       on a.[Vendor No_] COLLATE DATABASE_DEFAULT =b.No_
                 right join  NAV_ETL.dbo.[Take Shape For Life$G_L Entry] c
                       on c.[Document No_] COLLATE DATABASE_DEFAULT =a.[Document No_]
            where c.[Posting Date]>='2011-01-01'
                  and [G_L Account No_]>'10000'


union all 

select Null, null,null,null





























