﻿






CREATE procedure [dbo].[BI_NAV_FactSalesKey_KitExplosion]
as 


IF object_id('tempdb..#A') is not null DROP TABLE #A
Select 
        SalesInvHeader2.[Customer Posting Group] as [CustomerPostingGroup],
        [Line No_],
        SalesInvHeader2.[Shortcut Dimension 1 Code] as SalesChannel,
        case when SalesInvLine2.Type<>2 then 'NON-ITEM'
             when SalesInvLine2.Type=2 and (SalesInvLine2.[Item Category Code] COLLATE DATABASE_DEFAULT NOT IN (SELECT Code FROM [Jason Pharm$Item Category]))
             then 'UNCATEGORIZED' else SalesInvLine2.[Item Category Code] end as [ItemCategoryCode] ,
        SalesInvHeader2.[Sell-to Customer No_] as SelltoCustomerID,
        SalesInvHeader2.[Sell-to Customer No_]+ ''
            + LEFT([Ship-to Post Code], 5) AS ShiptoCustomerKey ,
        SalesInvHeader2.[Sell-to Customer No_] + ''
            + LEFT([Sell-to Post Code],5) AS SelltoCustomerKey ,
        SalesInvLine2.No_ as ItemCode,
        [Document No_] as DocumentNo,
        [Line Discount Amount] as LineDiscount,
        ([Inv_ Discount Amount]) as InvoiceDiscount,
        case when SalesInvLine2.Type<>2 then '0' 
        when SalesInvLine2.[Item Category Code]='NPD' then '0' else  ([Quantity]*SalesInvLine2.[Qty_ per Unit of Measure])end  as Units,
        isnull(ItemUOM.[Qty_ per Unit of Measure],'1') as UnitsPerBox,
        ([Quantity (Base)]/ isnull(ItemUOM.[Qty_ per Unit of Measure],'1')) as BOX,
        [Amount], 
        [Line Amount]  as [Gross Amount], 
        [Amount Including VAT]
        ,case when SalesInvLine2.[Tax Group Code]=' ' then ([Amount Including VAT]+(-1*[Amount])) else ([Amount Including VAT]-[Amount])end  as TaxAmount
        ,SalesInvLine2.[Tax Group Code]
        ,Case when [Unit Cost (LCY)]=0 then 0 else ([Unit Cost (LCY)]/SalesInvLine2.[Qty_ per Unit of Measure]) end  as [Unit Cost (LCY)], 
        Quantity ,
        1 as SalesType,
        Case when SalesInvHeader2.[Location Code]='SUNRISE' then 1 
             when SalesInvHeader2.[Location Code]='TDC' then 2
             when SalesInvHeader2.[Location Code]='TSFL' then 3 
             when SalesInvHeader2.[Location Code]='TDCWMS' then 4 
             when SalesInvHeader2.[Location Code]='MDCWMS' then 5 else 6 end as LocationID,
       left(SalesInvHeader2.[Sell-to Post Code], 4) as RegionID
           ,SalesInvHeader2.[Posting Date]
                        into #A FROM [Jason Pharm$Sales Invoice Line] SalesInvLine2 with (nolock)
	                         INNER JOIN [Jason Pharm$Sales Invoice Header] SalesInvHeader2 with (nolock)
	                                    ON SalesInvHeader2.[No_] = SalesInvLine2.[Document No_]
	                         left JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item with (nolock)
	                                    ON Item.No_ COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_]
	                         left JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM with (nolock)
                                        ON (ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = SalesInvLine2.[No_]) 
                                        AND(ItemUOM.Code COLLATE DATABASE_DEFAULT = case when SalesInvLine2.[Item Category Code]='RTD' then 'CS' else Item.[Sales Unit of Measure] end )
                                
                                 where SalesInvHeader2.[Shortcut Dimension 1 Code]<>' ' 
                                 and SalesInvLine2.No_ not in ('RC','RCC')
	                          --and (SalesInvHeader2.[Posting Date]>='2011-01-01')
	                          and [Customer Posting Group]<>'CORPCLINIC'
	                           AND ( SalesInvHeader2.[Posting Date] >= DATEADD(dd,
                                                            DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                            0) )
	                          and SalesInvLine2.No_ in ('31011','31021','32035','34822','39240','40492','40540',
	                                                     '40550','40650','65050','65460','72755','74000','74001',
	                                                     '74002','74008','74050','74051','74058','74059','74100',
	                                                     '74101','74102','74108','74200','74201','74208','74300',
	                                                     '74350','74351','74400','74500','74600','74700','74850',
	                                                     '74855','74856','74860','74865','74866','74867','74868',
	                                                     '74870','74920','75005','75010','75030','75035','75040',
	                                                     '75050','75055','75060')
	                      
	                         
--EXECUTE AS LOGIN = 'ReportViewer' 
--EXECUTE AS LOGIN = 'ETL_SERVICE'
IF object_id('tempdb..#ItemInKit') is not null DROP TABLE #ItemInKit
select 
[Production BOM No_] as KitId
,[No_] as ItemInKit
,[Description]
,Quantity
,[Unit of Measure Code]
into #ItemInKit
from [NAV_ETL].[dbo].[Jason Pharm$Production BOM Line]
where 
[Production BOM No_]in ('31011','31021','32035','34822','39240','40492','40540',
	                                                     '40550','40650','65050','65460','72755','74000','74001',
	                                                     '74002','74008','74050','74051','74058','74059','74100',
	                                                     '74101','74102','74108','74200','74201','74208','74300',
	                                                     '74350','74351','74400','74500','74600','74700','74850',
	                                                     '74855','74856','74860','74865','74866','74867','74868',
	                                                     '74870','74920','75005','75010','75030','75035','75040',
	                                                     '75050','75055','75060') ------Kit Sku---
and 
Type=1 
---------------------------------------------RETAIL-------------------------------------------------------
---------bring in the selling price-------
IF object_id('tempdb..#PRICE_USD') is not null DROP TABLE #PRICE_USD
SELECT
MAx([Starting Date]) as  LatestPrice-------------may neeed adjustment of older sales-----
,[Item No_]
,[Unit of Measure Code]
,[Sales Code]
,'USD' As Currency
INTO #PRICE_USD
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Price]
WHERE [Sales Code] In('RETAIL')
----and [Starting Date]>='2011-01-01' 
GROUP BY [Item No_], [Unit of Measure Code], [Sales Code]

ALTER TABLE #PRICE_USD
ADD [Unit Price] Numeric(15,2)

UPDATE #PRICE_USD
SET [Unit Price] = P.[Unit Price]
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Price] P
WHERE #PRICE_USD.[Item No_] = P.[Item No_]
AND #PRICE_USD.LatestPrice = P.[Starting Date]
AND #PRICE_USD.[Unit of Measure Code] = P.[Unit of Measure Code]
AND P.[Sales Code]In('RETAIL')




----example of the kit above exploded-----
IF object_id('tempdb..#C') is not null DROP TABLE #C
Select LatestPrice,
KitId
,ItemInKit
,[Description]
,(Quantity*isnull([Qty_ per Unit of Measure],1)) as Units
,a.[Unit of Measure Code]
,(([Unit Price]*Quantity)/(Quantity*isnull([Qty_ per Unit of Measure],1))) as [Unit Price]
,Currency
,[Sales Code]
into #C from #ItemInKit a
 Join #PRICE_USD b
on a.ItemInKit = b.[Item No_]
and a.[Unit of Measure Code] = b.[Unit of Measure Code]
left JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM with (nolock)
                                        ON (ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = ItemInKit) 
                                        AND(ItemUOM.Code COLLATE DATABASE_DEFAULT = a.[Unit of Measure Code]  )


----------------------------------------WHOLE SALE------------------------------------------------

---------bring in the selling price-------
IF object_id('tempdb..#PRICE_USD1') is not null DROP TABLE #PRICE_USD1
SELECT
MAx([Starting Date]) as  LatestPrice-------------may neeed adjustment of older sales-----
,[Item No_]
,[Unit of Measure Code]
,[Sales Code]
,'USD' As Currency
INTO #PRICE_USD1
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Price]
WHERE [Sales Code] In('WHOLE SALE')
----and [Starting Date]>='2011-01-01' 
GROUP BY [Item No_], [Unit of Measure Code], [Sales Code]

ALTER TABLE #PRICE_USD1
ADD [Unit Price] Numeric(15,2)

UPDATE #PRICE_USD1
SET [Unit Price] = isnull(P.[Unit Price],0)
FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Price] P
WHERE #PRICE_USD1.[Item No_] = P.[Item No_]
AND #PRICE_USD1.LatestPrice = P.[Starting Date]
AND #PRICE_USD1.[Unit of Measure Code] = P.[Unit of Measure Code]
AND P.[Sales Code]In('WHOLE SALE')



----example of the kit above exploded-----
Insert into #C
Select LatestPrice,
KitId
,ItemInKit
,[Description]
,(Quantity*isnull([Qty_ per Unit of Measure],1)) as Units
,a.[Unit of Measure Code]
,(([Unit Price]*Quantity)/(Quantity*isnull([Qty_ per Unit of Measure],1))) as [Unit Price]
,Currency
,[Sales Code]
from #ItemInKit a
 Join #PRICE_USD1 b
on a.ItemInKit = b.[Item No_]
and a.[Unit of Measure Code] = b.[Unit of Measure Code]
left JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM with (nolock)
                                        ON (ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = ItemInKit) 
                                        AND(ItemUOM.Code COLLATE DATABASE_DEFAULT = a.[Unit of Measure Code]  )

IF object_id('tempdb..#D') is not null DROP TABLE #D

Select KitID,SUM(Units*[Unit Price]) as UnitePrice,SUM(Units) as UpdatedQuantity,[Sales Code] into #D from #C
Group by KitId,[Sales Code]
Order by KitId




insert into BI_SSAS_Cubes.dbo.FactSales (
[Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType]
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
	  ,[Order Date])
Select  'USA' as Country ,[CustomerPostingGroup],
         SalesChannel,
         [Item Category Code] ,
         [ItemCategoryCode] as [Item Category Code SubCategory],
         SelltoCustomerID,
         SelltoCustomerKey,
         ShiptoCustomerKey,
        ItemInKit as  ItemCode,
        d.KitId as ItemCodeSubCategory,
        DocumentNo,
        (LineDiscount/UpdatedQuantity) as LineDiscount,
        (InvoiceDiscount/UpdatedQuantity) as InvoiceDiscount,
        c.Units * a.Units  as Units,
        case when row_number () over (partition by DocumentNo,[Line No_] Order by DocumentNo,[Line No_],ItemInKit)=1 then 1*a.Units else 0 end  as SubCategoryUnits ,
        isnull(ItemUOM.[Qty_ per Unit of Measure],'1')as  UnitsPerBox,
         (c.Units* a.Units)/isnull(ItemUOM.[Qty_ per Unit of Measure],'1') as BOX,
         Case when [Amount]=0 then 0*c.[Unit Price]*c.Units else (((([Amount]/(UnitePrice))*100)* c.[Unit Price])/100)*c.Units end as Amount,
        Case when [Amount]=0 then 0*c.[Unit Price]*c.Units  else ((((([Amount]/(UnitePrice))*100)* c.[Unit Price])/100)*c.Units)+ ((LineDiscount/UpdatedQuantity)*c.Units)+((InvoiceDiscount/UpdatedQuantity)*c.Units) end [Gross Amount], 
        Case when [Amount]=0 then 0*c.[Unit Price]*c.Units  else ((((([Amount]/(UnitePrice))*100)* c.[Unit Price])/100)*c.Units)end[Amount Including VAT],
         0 as TaxAmount
        ,Item .[Tax Group Code]
        ,[Unit Cost] as  [Unit Cost (LCY)], 
       (c.Units*a.Units) as Quantity ,
        1 as SalesType,
        LocationID,
        RegionID
           ,[Posting Date]
		   ,[Posting Date] AS [Order Date]
		   from #A a
 join #D d
on a.ItemCode COLLATE DATABASE_DEFAULT =d.KitID 
and d.[Sales Code]= case when a.[CustomerPostingGroup]='DOCTORS' then 'WHOLE SALE' else 'RETAIL'end  
 join #C c
on a.ItemCode COLLATE DATABASE_DEFAULT=c.KitId
and c.[Sales Code]= case when a.[CustomerPostingGroup]='DOCTORS' then 'WHOLE SALE' else 'RETAIL'end  
JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item with (nolock)
	                                    ON Item.No_ COLLATE DATABASE_DEFAULT = c.ItemInKit
	                         left JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM with (nolock)
                                        ON (ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = c.ItemInKit) 
                                        AND(ItemUOM.Code COLLATE DATABASE_DEFAULT =Item.[Sales Unit of Measure] )
                            
                             
                                        
                                        
                                    
                                    
                                       














