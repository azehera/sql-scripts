﻿--/*
--===============================================================================
--Author:         Emily Trainor
--Create date: 01/10/2012
---------------------------TSFL Tracker TSFL Client Acquisition----------------
------Pull data for TSFL Client Acquisition Business Week Graph-----------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--BI_Reporting			XL_016_TSFL_LY_TY				            Select                      
--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------
--06/22/2012	  Emily Trainor					Updated to pull Business Week
--===============================================================================
--NOTES: Updated to pull in Business Week and Year from the TY_LY table. Eliminates
--       need for additional step of pulling in Bus. Week and Year to aggregate.
---------------------------------------------------------------------------------
--===============================================================================
--*/

Create procedure [dbo].[USP_XL_016_TSFL_ClientAcquisition_GetData] as 
set nocount on ;

-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = '01/01/' + Convert(varchar(4), DatePart(yyyy, DateAdd(yyyy, -1, GETDATE())));

---------------------Pulling TSFL Client Acquisition Data ------------------------------------
With A As 
(SELECT  [Posting Date], [Calendar Week], [Calendar Year], [Orders]
FROM [BI_Reporting].[dbo].[XL_016_TSFL_LY_TY] where [Posting Date] >= @Date And No_ In ('40491', '40650') And Dollars = '0')

Select 
	 A.[Calendar Week]
	,A.[Calendar Year]
	, sum(B.[OrderCount]) as [Total Orders]
From A 
	Inner join [BI_Reporting].dbo.XL_016_TSFL_LY_TY B on A.[Orders] = B.Orders
Group By A.[Calendar Year], A.[Calendar Week]
Order by A.[Calendar Year] ,A.[Calendar Week]