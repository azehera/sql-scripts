﻿

CREATE PROCEDURE [dbo].[BI_NAV_InventoryStdCost_byDate1]
(@RunDate AS DATETIME)
AS
/****** MED0001 MRH 01/23/12: Business Intelligence / Executive Dashboard ******/

DECLARE @Today DATETIME

SET @Today = CAST(FLOOR(CAST(@RunDate AS float)) AS DATETIME)

BEGIN TRANSACTION

DELETE FROM [NAV_ETL].[dbo].[Snapshot$NAV_InventoryStdCost]
WHERE (RunDate = @Today)

COMMIT TRANSACTION

BEGIN TRANSACTION

INSERT INTO [NAV_ETL].[dbo].[Snapshot$NAV_InventoryStdCost]
           ([RunDate]
           ,[ItemNo]
           ,[InventoryPostingGroup]
           ,[ItemCategoryCode]
           ,[ProductGroupCode]
           ,[StandardCost])
SELECT @Today
      ,No_ AS _ItemNo
      ,[Inventory Posting Group] AS _InvPostGrp
      ,[Item Category Code] AS _ItemCat
      ,[Product Group Code] AS _ProdGrpCode
      ,[Standard Cost] AS _StdCost
FROM [Jason Pharm$Item]

COMMIT TRANSACTION
