﻿


/******************************Modifications*********************************
Date: 09/09/014
Developer: Ron Baldwin
Change Desc: Change the DECLARE @Delete DATETIME to  DECLARE @Delete integer

**************************************************************************/



CREATE PROCEDURE [dbo].[BI_NAV_FactGL_Cube]
AS
    SET NOCOUNT ON
    DECLARE @Delete integer


    SET @Delete = ( SELECT  [CalendarMonthIDGLCube]
                    FROM    BI_SSAS_Cubes.dbo.BudgetData_CalendarYear
                  )



    DELETE  FROM BI_SSAS_Cubes.dbo.FactGL_cube
    WHERE   CalendarMonthID >= @delete

    SELECT  1 AS EntryNO ,
            CalendarMonthID ,
            a.[G_L Account No_] COLLATE DATABASE_DEFAULT AS [G_L Account No_] ,
            a.[Document No_] COLLATE DATABASE_DEFAULT + ''
            + a.[G_L Account No_]COLLATE DATABASE_DEFAULT AS DocumentID ,
            a.[Document No_]COLLATE DATABASE_DEFAULT AS [Document No_] ,
            a.[Global Dimension 1 Code]COLLATE DATABASE_DEFAULT AS [Global Dimension 1 Code] ,
            [Source Code] COLLATE DATABASE_DEFAULT AS [Source Code] ,
            CASE WHEN a.[Global Dimension 2 Code]COLLATE DATABASE_DEFAULT = 'FRANCHISE'
                 THEN 'JASON PHARM FRANCHISE'
                 WHEN a.[Global Dimension 2 Code]COLLATE DATABASE_DEFAULT = 'TSFL'
                 THEN 'TSFL'
                 WHEN a.[Global Dimension 2 Code]COLLATE DATABASE_DEFAULT = 'SALES'
                 THEN 'WHOLESALE'
                 ELSE a.[Global Dimension 2 Code]
            END AS [Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            SUM(a.[Amount]) * -1 AS NetTotal ,
            NULL AS Budget$ ,
            CASE WHEN a.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN a.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] a (NOLOCK)
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c (NOLOCK) ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              a.[Posting Date])) = c.CalendarDate
    WHERE   CalendarMonthID >= @delete
            AND a.[G_L Account No_] >= '10000'
--and  [G_L Account No_] not in ('96750','85401')
GROUP BY    CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] ,
            a.[Global Dimension 1 Code] ,
            a.[Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            [Source Code]
    UNION ALL
    SELECT  2 AS EntryNO ,
            CalendarMonthID ,
            a.[G_L Account No_] COLLATE DATABASE_DEFAULT ,
            a.[Document No_]COLLATE DATABASE_DEFAULT + ''
            + a.[G_L Account No_]COLLATE DATABASE_DEFAULT AS DocumentID ,
            a.[Document No_] COLLATE DATABASE_DEFAULT ,
            a.[Global Dimension 1 Code]COLLATE DATABASE_DEFAULT ,
            [Source Code] ,
            a.[Global Dimension 1 Code]COLLATE DATABASE_DEFAULT AS [Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            SUM(a.[Amount]) * -1 AS NetTotal ,
            NULL AS Budget$ ,
            CASE WHEN a.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN a.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.[dbo].[7 Crondall$G_L Entry] a (NOLOCK)
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c (NOLOCK) ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              a.[Posting Date])) = c.CalendarDate
    WHERE   a.[G_L Account No_] >= '10000'
--and [G_L Account No_] not in ('96750','43500')
            AND CalendarMonthID >= @delete
    GROUP BY CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] ,
            a.[Description] ,
            a.[Global Dimension 1 Code] ,
            a.[Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            [Source Code]
    UNION ALL
    SELECT  3 AS EntryNO ,
            CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] + '' + a.[G_L Account No_] AS DocumentID ,
            a.[Document No_] ,
            a.[Global Dimension 1 Code] ,
            [Source Code] ,
            a.[Global Dimension 1 Code] AS [Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            SUM(a.[Amount]) * -1 AS NetTotal ,
            NULL AS Budget$ ,
            CASE WHEN a.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN a.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.[dbo].[Jason Enterprises$G_L Entry] a (NOLOCK)
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c (NOLOCK) ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              a.[Posting Date])) = c.CalendarDate
    WHERE   a.[G_L Account No_] >= '10000'
            AND CalendarMonthID >= @delete
    GROUP BY CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] ,
            a.[Global Dimension 1 Code] ,
            a.[Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            [Source Code]
    UNION ALL
    SELECT  4 AS EntryNO ,
            CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] + '' + a.[G_L Account No_] AS DocumentID ,
            a.[Document No_] ,
            a.[Global Dimension 1 Code] ,
            [Source Code] ,
            CASE WHEN a.[Global Dimension 1 Code] = 'FRANCHIS'
                 THEN 'FRANCHISE'
                 ELSE 'MWCC'
            END AS [Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            SUM(a.[Amount]) * -1 AS NetTotal ,
            NULL AS Budget$ ,
            CASE WHEN a.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN a.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.[dbo].[Jason Properties$G_L Entry] a (NOLOCK)
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c (NOLOCK) ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              a.[Posting Date])) = c.CalendarDate
    WHERE   a.[G_L Account No_] >= '10000'
--and [G_L Account No_] not in ('96750')
            AND CalendarMonthID >= @delete
    GROUP BY CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] ,
            a.[Global Dimension 1 Code] ,
            CASE WHEN a.[Global Dimension 1 Code] = 'FRANCHIS'
                 THEN 'FRANCHISE'
                 ELSE 'MWCC'
            END ,
            a.[Transaction No_] ,
            [Source Code]
    UNION ALL
    SELECT  5 AS EntryNO ,
            CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] + '' + a.[G_L Account No_] AS DocumentID ,
            a.[Document No_] ,
            a.[Global Dimension 1 Code] ,
            [Source Code] ,
            a.[Global Dimension 1 Code] AS [Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            SUM(a.[Amount]) * -1 AS NetTotal ,
            NULL AS Budget$ ,
            CASE WHEN a.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN a.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.[dbo].[Medifast Inc$G_L Entry] a (NOLOCK)
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c (NOLOCK) ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              a.[Posting Date])) = c.CalendarDate
    WHERE   a.[G_L Account No_] >= '10000'
--and [G_L Account No_] not in ('96750')
            AND CalendarMonthID >= @delete
    GROUP BY CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] ,
            a.[Global Dimension 1 Code] ,
            a.[Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            [Source Code]
    UNION ALL
    SELECT  6 AS EntryNO ,
            CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] + '' + a.[G_L Account No_] AS DocumentID ,
            a.[Document No_] ,
            [Global Dimension 1 Code] ,
            [Source Code] ,
            a.[Global Dimension 1 Code] AS [Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            SUM(a.[Amount]) * -1 AS NetTotal ,
            NULL AS Budget$ ,
            CASE WHEN a.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN a.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.[dbo].[Take Shape For Life$G_L Entry] a (NOLOCK)
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c (NOLOCK) ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              a.[Posting Date])) = c.CalendarDate
    WHERE   a.[G_L Account No_] >= '10000'
            AND CalendarMonthID >= @delete
    GROUP BY CalendarMonthID ,
            a.[G_L Account No_] ,
            a.[Document No_] ,
            a.[Global Dimension 1 Code] ,
            a.[Global Dimension 2 Code] ,
            a.[Transaction No_] ,
            [Source Code]


































