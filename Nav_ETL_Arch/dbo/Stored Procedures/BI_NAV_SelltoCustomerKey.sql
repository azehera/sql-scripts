﻿

/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_SelltoCustomerKey]  
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Dimesion data for cube from NAVPROD database     
PARAMETERS/VARRIABLES:
NOTES:
Date : 03/31/2020 
Notes: Rewrite this stored procedure to get customer data from Sales Invoice Header & Customer Master (Reltio_MDM)

Date : 05/04/2020
Notes: Ignore Selltocustomerid with Null Value
*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_SelltoCustomerKey] 
as

truncate table [BI_SSAS_Cubes].[dbo].[DimSelltoCustomerKey]

IF object_id('Tempdb..#SelltoCustomerKey') Is Not Null DROP TABLE #SelltoCustomerKey;

CREATE TABLE #SelltoCustomerKey(
	[SelltoCustomerKey] [nvarchar](150) NULL,
	[SelltoCustomerID] [nvarchar](60) NULL,
	[Sell-to Customer Name] [nvarchar](110) NULL,
	[Sell-to Address] [nvarchar](50) NULL,
	[Sell-to City] [nvarchar](50) NULL,
	[Zip5] [nvarchar](20) NULL,
	[Zip3] [nvarchar](20) NULL,
	[Zip2] [nvarchar](20) NULL,
	[Sell-to County] [nvarchar](30) NULL,
	[Country] [nvarchar](10) NULL,
	[Payment Discount %] [decimal](38, 20) NULL,
	[Currency Code] [nvarchar](10) NULL,
	[Currency Factor] [decimal](38, 20) NULL,
	[Email] [nvarchar](80) NULL,
	[DMA Code] [varchar](50) NULL,
	[DMA Name] [varchar](50) NULL
) ON [PRIMARY];
Insert into #SelltoCustomerKey
(
[SelltoCustomerKey],
[SelltoCustomerID],
[Sell-to Customer Name],
[Sell-to Address],
[Sell-to City],
[Zip5],
[Zip3],
[Zip2],
[Sell-to County],
[Country],
[Payment Discount %],
[Currency Code],
[Currency Factor],
[Email],
[DMA Code],
[DMA Name]
)
SELECT LTRIM(RTRIM([SelltoCustomerKey])) as [SelltoCustomerKey],
LTRIM(RTRIM([SelltoCustomerID])) as [SelltoCustomerID],
LTRIM(RTRIM([Sell-to Customer Name])) as [Sell-to Customer Name],
LTRIM(RTRIM([Sell-to Address])) as [Sell-to Address],
LTRIM(RTRIM([Sell-to City])) as [Sell-to City],
LTRIM(RTRIM([Zip5])) as [Zip5],
LTRIM(RTRIM([Zip3])) as [Zip3],
LTRIM(RTRIM([Zip2])) as [Zip2],
LTRIM(RTRIM([Sell-to County])) as [Sell-to County],
LTRIM(RTRIM([Country])) as [Country],
[Payment Discount %],
LTRIM(RTRIM([Currency Code])) as [Currency Code],
[Currency Factor],
LTRIM(RTRIM([Email])) as [Email],
LTRIM(RTRIM([DMA Code])) as [DMA Code],
LTRIM(RTRIM([DMA Name])) as [DMA Name]  FROM ( 
SELECT *,	ROW_NUMBER() over (partition by [SelltoCustomerKey] Order by [SelltoCustomerID]) as nos
FROM (
select CONVERT(nvarchar,concat(RTRIM(LTRIM(a.[Sell-to Customer No_] COLLATE DATABASE_DEFAULT)),'',LEFT(replace(ltrim(rtrim(a.[Sell-to Post Code] COLLATE DATABASE_DEFAULT)),CHAR(10),'') ,5)))as [SelltoCustomerKey],
	CONVERT(nvarchar,a.[Sell-to Customer No_]) as [SelltoCustomerID], 
	CONVERT(nvarchar,a.[Sell-to Customer Name]) as [Sell-to Customer Name],
	CONVERT(nvarchar,a.[Sell-to Address]) as [Sell-to Address],
	CONVERT(nvarchar,'') as [Sell-to City],
	CONVERT(nvarchar,LEFT(a.[Sell-to Post Code],5)) [Zip5],
	CONVERT(nvarchar,LEFT(a.[Sell-to Post Code],3)) [Zip3],
	CONVERT(nvarchar,LEFT(a.[Sell-to Post Code],2)) [Zip2],
	CONVERT(nvarchar,[Ship-to County]) [Sell-to County],
	CONVERT(nvarchar,[Ship-to Country_Region Code]) [Country],
	CONVERT(decimal,0.0) [Payment Discount %],
	CONVERT(nvarchar,'') [Currency Code],
	CONVERT(decimal,0.0) [Currency Factor],
	CONVERT(nvarchar,'') [Email],
	CONVERT(varchar,'Unknown') [DMA Code],
	CONVERT(varchar,'Unknown') [DMA Name]
	 from [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] a
	     where [Customer Posting Group]<>'CORPCLINIC'
                     and [Posting Date]>='2011-01-01'
      UNION 
	 select CONVERT(nvarchar,concat(RTRIM(LTRIM(a.[Sell-to Customer No_] COLLATE DATABASE_DEFAULT)),'',LEFT(replace(ltrim(rtrim(a.[Sell-to Post Code] COLLATE DATABASE_DEFAULT)),CHAR(10),'') ,5)))as [SelltoCustomerKey],
	CONVERT(nvarchar,a.[Sell-to Customer No_]) as [SelltoCustomerID], 
	CONVERT(nvarchar,a.[Sell-to Customer Name]) as [Sell-to Customer Name],
	CONVERT(nvarchar,a.[Sell-to Address]) as [Sell-to Address],
	CONVERT(nvarchar,'') as [Sell-to City],
	CONVERT(nvarchar,LEFT(a.[Sell-to Post Code],5)) [Zip5],
	CONVERT(nvarchar,LEFT(a.[Sell-to Post Code],3)) [Zip3],
	CONVERT(nvarchar,LEFT(a.[Sell-to Post Code],2)) [Zip2],
	CONVERT(nvarchar,'') [Sell-to County],
	CONVERT(nvarchar,[Ship-to Country_Region Code]) [Country],
	CONVERT(decimal,0.0) [Payment Discount %],
	CONVERT(nvarchar,'') [Currency Code],
	CONVERT(decimal,0.0) [Currency Factor],
	CONVERT(nvarchar,'') [Email],
	CONVERT(varchar,'Unknown') [DMA Code],
	CONVERT(varchar,'Unknown') [DMA Name]
	 from [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] a
	     where [Customer Posting Group]<>'CORPCLINIC'
                     and [Posting Date]>='2011-01-01' )SUB
					 ) A 
					 where nos = 1
 
 

IF object_id('Tempdb..#SelltoCustomerKey_Reltio') Is Not Null DROP TABLE #SelltoCustomerKey_Reltio;
CREATE TABLE #SelltoCustomerKey_Reltio(
	[SelltoCustomerKey] [nvarchar](150) NULL,
	[SelltoCustomerID] [nvarchar](60) NULL,
	[Sell-to Customer Name] [nvarchar](110) NULL,
	[Sell-to Address] [nvarchar](50) NULL,
	[Sell-to City] [nvarchar](50) NULL,
	[Zip5] [nvarchar](20) NULL,
	[Zip3] [nvarchar](20) NULL,
	[Zip2] [nvarchar](20) NULL,
	[Sell-to County] [nvarchar](30) NULL,
	[Country] [nvarchar](10) NULL,
	[Payment Discount %] [decimal](38, 20) NULL,
	[Currency Code] [nvarchar](10) NULL,
	[Currency Factor] [decimal](38, 20) NULL,
	[Email] [nvarchar](80) NULL,
	[DMA Code] [varchar](50) NULL,
	[DMA Name] [varchar](50) NULL
) ON [PRIMARY];

/**********************Insert from Reltio MDM **************************/
with cte as 
(
		Select  CONVERT(nvarchar,concat(RTRIM(LTRIM(B.Medifast_customer_id COLLATE DATABASE_DEFAULT)) ,'',
										LEFT (replace(ltrim(rtrim(A.BillToPostalCode  COLLATE DATABASE_DEFAULT)),CHAR(10),'') ,5))) [SelltoCustomerKey],
		CONVERT(nvarchar,B.Medifast_customer_id) as [SelltoCustomerID],
		dense_rank() over (partition by RTRIM(LTRIM(concat(A.BillToFirstName COLLATE DATABASE_DEFAULT,' ',A.BillToLastName COLLATE DATABASE_DEFAULT))), 
		RTRIM(LTRIM(A.BillToAddressLine1)),RTRIM(LTRIM(A.Email)) order by case when (B.Medifast_customer_id is null and B.Medifast_customer_id='NULL')
		or ltrim(rtrim(B.Medifast_customer_id))<>'' then 1 else 2 end) as no,
		CONVERT(nvarchar,concat(A.BillToFirstName COLLATE DATABASE_DEFAULT,' ',A.BillToLastName COLLATE DATABASE_DEFAULT)) [Sell-to Customer Name],
		CONVERT(nvarchar,A.BillToAddressLine1) [Sell-to Address],
		CONVERT(nvarchar,A.BillToCity) [Sell-to City],
		CONVERT(nvarchar,LEFT(A.BillToPostalCode,5)) as Zip5,
		CONVERT(nvarchar,LEFT(A.BillToPostalCode,3)) as Zip3,
		CONVERT(nvarchar,LEFT(A.BillToPostalCode,2)) as Zip2,
		CONVERT(nvarchar,A.BillToStateCode) [Sell-to County],
		CONVERT(nvarchar,A.BillToCountryCode) [Country],
		CONVERT(nvarchar,A.Email) [Email],
		CONVERT(decimal,0.0) as[Payment Discount %],
		CONVERT(nvarchar,'') [Currency Code],
		CONVERT(decimal,0.0) as [Currency Factor],
		CONVERT(varchar,isnull([DMA Code],'Unknown')) as [DMA Code],
		CONVERT(varchar,isnull([DMA Name],'Unknown')) as [DMA Name]
		from mdm_reltio.dbo.RELTIO_MDM_CUSTOMERS A (nolock)
		LEFT JOIN mdm_reltio.[dbo].[RELTIO_GROUP_MDM_CUSTOMERS_ALL] B (nolock) on A.Reltio_Id=B.Reltio_Id COLLATE SQL_Latin1_General_CP1_CI_AS
		left join [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] D with (nolock)
			on  LEFT (A.BillToPostalCode,5)=D.zipcode COLLATE SQL_Latin1_General_CP1_CI_AS and [PrimaryRecord]='P'
		left join [Zip_codes_deluxe].[dbo].[DMA_Data] c with (nolock)
			on LEFT (A.BillToPostalCode,5)=c.ZipCode COLLATE SQL_Latin1_General_CP1_CI_AS
		where A.ReltioStatus='Active' and B.ReltioStatus='Active' and B.Sources not in( 'ReltioCleanser','ExactTarget','Reltio','Hyperwallet')				
)
Insert into #SelltoCustomerKey_Reltio
(
[SelltoCustomerKey],
[SelltoCustomerID],
[Sell-to Customer Name],
[Sell-to Address],
[Sell-to City],
[Zip5],
[Zip3],
[Zip2],
[Sell-to County],
[Country],
[Payment Discount %],
[Currency Code],
[Currency Factor],
[Email],
[DMA Code],
[DMA Name]
)
Select LTRIM(RTRIM([SelltoCustomerKey])) as [SelltoCustomerKey]
		,LTRIM(RTRIM([SelltoCustomerID])) as [SelltoCustomerID]
		,LTRIM(RTRIM([Sell-to Customer Name])) as [Sell-to Customer Name]
		,LTRIM(RTRIM([Sell-to Address])) as [Sell-to Address]
		,LTRIM(RTRIM([Sell-to City])) as [Sell-to City]
		,LTRIM(RTRIM(Zip5)) as Zip5
		,LTRIM(RTRIM(Zip3)) as Zip3
		,LTRIM(RTRIM(Zip2)) as Zip2
		,LTRIM(RTRIM([Sell-to County])) as [Sell-to County]
		,LTRIM(RTRIM([Country])) as [Country]
		,[Payment Discount %]
		,LTRIM(RTRIM([Currency Code])) as [Currency Code]
		,[Currency Factor]
		,LTRIM(RTRIM([Email])) as [Email]
		,LTRIM(RTRIM([DMA Code])) as [DMA Code]
		,LTRIM(RTRIM([DMA Name])) as [DMA Name]
		from cte where no=1

 

Update SK
 SET SK.[Sell-to Customer Name] = SR.[Sell-to Customer Name]
		,SK.[Sell-to Address]=SR.[Sell-to Address]
		,SK.[Sell-to City]=SR.[Sell-to City]
		,SK.Zip5=SR.Zip5
		,SK.Zip3=SR.Zip3
		,SK.Zip2=SR.Zip2
		,SK.[Sell-to County]=SR.[Sell-to County]
		,SK.[Country]=SR.[Country]
		,SK.[Payment Discount %]=SR.[Payment Discount %]
		,SK.[Currency Code]=SR.[Currency Code]
		,SK.[Currency Factor]=SR.[Currency Factor]
		,SK.[Email]=SR.[Email]
		,SK.[DMA Code]=SR.[DMA Code]
		,SK.[DMA Name] =SR.[DMA Name]
from #SelltoCustomerKey SK
Inner Join #SelltoCustomerKey_Reltio SR
on case when SK.SelltoCustomerID like 'TSFL-%' THEN SUBSTRING(SK.SelltoCustomerID,6,20)
                     when SK.SelltoCustomerID like 'TSFL%' THEN SUBSTRING(SK.SelltoCustomerID,5,20)
					 ELSE SK.SelltoCustomerID END = SR.SelltoCustomerID  
 

/********************* Data from #SelltoCustomerKey**************************/
INSERT INTO [BI_SSAS_Cubes].[dbo].[DimSelltoCustomerKey]
           ([SelltoCustomerKey]
           ,[SelltoCustomerID]
           ,[Sell-to Customer Name]
           ,[Sell-to Address]
           ,[Sell-to City]
           ,[Zip5]
           ,[Zip3]
           ,[Zip2]
           ,[Sell-to County]
           ,[Country]
           ,[Payment Discount %]
           ,[Currency Code]
           ,[Currency Factor]
           ,[Email]
           ,[DMA Code]
           ,[DMA Name])
Select REPLACE([SelltoCustomerKey], char(9), '') as [SelltoCustomerKey],
REPLACE([SelltoCustomerID], char(9), '') as [SelltoCustomerID],
REPLACE([Sell-to Customer Name], char(9), '') as [Sell-to Customer Name],
REPLACE([Sell-to Address], char(9), '') as [Sell-to Address],
REPLACE([Sell-to City], char(9), '') as [Sell-to City],
REPLACE([Zip5], char(9), '') as [Zip5],
REPLACE([Zip3], char(9), '') as [Zip3],
REPLACE([Zip2], char(9), '') as [Zip2],
REPLACE([Sell-to County], char(9), '') as [Sell-to County],
REPLACE([Country], char(9), '') as [Country],
[Payment Discount %],
REPLACE([Currency Code], char(9), '') as [Currency Code],
[Currency Factor],
REPLACE([Email], char(9), '') as [Email],
REPLACE([DMA Code], char(9), '') as [DMA Code],
REPLACE([DMA Name], char(9), '') as [DMA Name]
from #SelltoCustomerKey
Where [SelltoCustomerID] is not null

/* Update to remove Null Values */

Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set Zip5 = ''
where Zip5 is null

Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set Zip3 = ''
where Zip3 is null

Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set Zip2 = ''
where Zip2 is null

Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set Email = ''
where Email is null

Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set [Sell-to City] = NULL
where [Sell-to City] ='' 


Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set [Sell-to County] = NULL
where [Sell-to County] ='' 

Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set Country = NULL
where Country ='' 



Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set [Sell-to Customer Name] = REPLACE([Sell-to Customer Name], CHAR(13), '')
where (RTRIM([Sell-to Customer Name]) LIKE '%' + CHAR(13) +  '%')


Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set [Sell-to Customer Name] = REPLACE([Sell-to Customer Name], CHAR(9), '')
where (RTRIM([Sell-to Customer Name]) LIKE '%' + CHAR(9) +  '%')


Update [BI_SSAS_Cubes].dbo.DimSelltoCustomerKey
set [Sell-to Customer Name] = REPLACE([Sell-to Customer Name], CHAR(10), '')
where (RTRIM([Sell-to Customer Name]) LIKE '%' + CHAR(10) +  '%')