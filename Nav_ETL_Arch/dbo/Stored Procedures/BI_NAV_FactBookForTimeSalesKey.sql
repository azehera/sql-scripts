﻿/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_FactBookForTimeSalesKey] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Fact data for cube from BookforTime database     
PARAMETERS/VARRIABLES:
NOTES: Removes Tab Characters from ItemCode 

*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_FactBookForTimeSalesKey]
AS
    ( SELECT    'USA' as Country,
                'MWCC' AS [CustomerPostingGroup] ,
                'CC' AS SalesChannel ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 's' )
                     THEN 'NON-ITEM'
                     ELSE Item.[Item Category Code] COLLATE DATABASE_DEFAULT
                END AS [ItemCategoryCode] ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 's' )
                     THEN 'NON-ITEM'
                     ELSE Item.[Item Category Code] COLLATE DATABASE_DEFAULT
                END AS [ItemCategoryCodeSubcategory] ,
                CONVERT(VARCHAR, [customer_id]) AS SelltoCustomerID ,
                CONVERT(VARCHAR, [customer_id]) + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS ShiptoCustomerKey ,
                CONVERT(VARCHAR, [customer_id]) + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS selltoCustomerkey ,
                RTRIM(REPLACE(item_code, CHAR(9), '')) AS ItemCode ,
                RTRIM(REPLACE(item_code, CHAR(9), '')) AS ItemCodeSubcategory,
                CAST(B4T_Header.transaction_id AS VARCHAR(20)) AS DocumentNo ,
                CAST(( discount_amt * Qty
                       * ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) AS DECIMAL(18,
                                                              2)) AS LineDiscount ,
                0.00 AS InvoiceDiscount ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 's' ) THEN '0'
                     WHEN Item.[Item Category Code] = 'RTD' THEN QTY
                     ELSE ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                END AS Units ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 's' ) THEN '0'
                     WHEN Item.[Item Category Code] = 'RTD' THEN QTY
                     ELSE ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                END AS SubCategoryUnits ,
                ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsPerBox ,
                CASE WHEN Item.[Item Category Code] = 'RTD'
                     THEN ( QTY / ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                     ELSE ( ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                           '1') )
                            / ( ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) )
                END AS Box ,
                ( sold_price * qty ) AS Amount ,
                ( sold_price * qty ) + 0.00 AS [Gross Amount] ,
                ( ( sold_price * qty ) + tax_amount ) AS [Amount Including VAT] ,
                tax_amount AS TaxAmount ,
                CASE WHEN tax_amount = 0 THEN 'NON TAXABL'
                     ELSE 'TAXABLE'
                END AS [Tax Group Code] ,
                [StandardCost] AS [Unit Cost(LCY)] ,
                ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) AS Quantity ,
                1 AS SalesType ,
                location_code AS LocationID ,
                CONVERT (VARCHAR(20), location_code) AS RegionID ,
                DATEADD(D, 0, DATEDIFF(D, 0, transaction_date)) AS [Posting Date]
 FROM      [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
                WITH ( NOLOCK )
                JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
                WITH ( NOLOCK ) ON CAST(B4T_Header.transaction_id AS VARCHAR(20)) = CAST(B4T_Detail.transaction_id AS VARCHAR(20))
                LEFT JOIN [Book4Time_ETL].dbo.[B4T_transaction_log_detail_tax] B4T_Tax
                WITH ( NOLOCK ) ON B4T_Tax.transaction_line_id = B4T_Detail.transaction_line_id
                LEFT OUTER JOIN [Book4Time_ETL].[dbo].B4T_product_master B4TPM
                WITH ( NOLOCK ) ON B4TPM.product_id = B4T_Detail.product_id
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item WITH ( NOLOCK ) ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
                WITH ( NOLOCK ) ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = B4T_Detail.item_code )
                                   AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
                                                              WHEN Item.[Sales Unit of Measure] IN (
                                                              'PK', 'CS' )
                                                              THEN [Base Unit of Measure]
                                                              WHEN Item.[Item Category Code] = 'RTD'
                                                              THEN 'CS'
                                                              ELSE [Sales Unit of Measure]
                                                              END )
                LEFT JOIN [NAV_ETL].[dbo].[Snapshot$NAV_InventoryStdCost] isc
                WITH ( NOLOCK ) ON B4T_Detail.item_code = isc.ItemNo COLLATE DATABASE_DEFAULT
                                   AND ( SELECT CONVERT(VARCHAR(10), B4T_Header.Transaction_Date, 121)
                                       ) = isc.RunDate
                LEFT JOIN [Book4Time_ETL].[dbo].[B4T_location] Location WITH ( NOLOCK ) ON B4T_Header.location_id = Location.location_id
                LEFT JOIN BI_SSAS_Cubes.dbo.DimselltoCustomerKey cust WITH ( NOLOCK ) ON CONVERT(VARCHAR, [customer_id])
                                                              + ''
                                                              + CONVERT(NVARCHAR, B4T_Header.location_id) = cust.SelltoCustomerKey
            --   left join BI_SSAS_Cubes.dbo.MDM m
            --on CAST(B4T_Detail.transaction_id AS VARCHAR(20))=m.DocNo
      WHERE     B4T_Header.transaction_type = 's'
                AND item_code NOT IN ( 'ASF', 'RC', 'RCC','HOUSE','BAD DEBT' )
                --AND DATEADD(D, 0, DATEDIFF(D, 0, transaction_date))>='2011-01-01')
                AND DATEADD(D, 0, DATEDIFF(D, 0, transaction_date)) >= DATEADD(dd,
                                                              DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                              0)
    )/*** Section 1***/
    UNION ALL
    ( SELECT    'USA' as Country,
                'MWCC' AS [CustomerPostingGroup] ,
                'CC' AS SalesChannel ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 'r' )
                     THEN 'NON-ITEM'
                     ELSE Item.[Item Category Code] COLLATE DATABASE_DEFAULT
                END AS [ItemCategoryCode] ,
                 CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 'r' )
                     THEN 'NON-ITEM'
                     ELSE Item.[Item Category Code] COLLATE DATABASE_DEFAULT
                END AS [ItemCategoryCodeSubcategory],
                CONVERT(VARCHAR, [customer_id]) AS SelltoCustomerID ,
                CONVERT(VARCHAR, [customer_id]) + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS ShiptoCustomerKey ,
                CONVERT(VARCHAR, [customer_id]) + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS selltoCustomerkey ,
                RTRIM(REPLACE(item_code, CHAR(9), '')) AS ItemCode ,
                RTRIM(REPLACE(item_code, CHAR(9), '')) AS ItemCodeSubcategory,
                CAST(B4T_Header.transaction_id AS VARCHAR(20)) AS DocumentNo ,
                CAST(( discount_amt * Qty
                       * ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) AS DECIMAL(18,
                                                              2)) AS LineDiscount ,
                0.00 AS InvoiceDiscount ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 'r' ) THEN '0'
                     WHEN Item.[Item Category Code] = 'RTD' THEN QTY
                     ELSE ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                END AS Units ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 'r' ) THEN '0'
                     WHEN Item.[Item Category Code] = 'RTD' THEN QTY
                     ELSE ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                END AS SubCategoryUnits ,
                ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsPerBox ,
                CASE WHEN Item.[Item Category Code] = 'RTD'
                     THEN ( QTY / ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                     ELSE ( ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                           '1') )
                            / ( ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) )
                END AS Box ,
                ( sold_price * qty ) AS Amount ,
                ( sold_price * qty ) + 0.00 AS [Gross Amount] ,
                ( ( sold_price * qty ) + tax_amount ) AS [Amount Including VAT] ,
                tax_amount AS TaxAmount ,
                CASE WHEN tax_amount = 0 THEN 'NON TAXABL'
                     ELSE 'TAXABLE'
                END AS [Tax Group Code] ,
                [StandardCost] AS [Unit Cost(LCY)] ,
                ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) AS Quantity ,
                2 AS SalesType ,
                location_code AS LocationID ,
                CONVERT (VARCHAR(20), location_code) AS RegionID ,
                DATEADD(D, 0, DATEDIFF(D, 0, transaction_date)) AS [Posting Date]
FROM      [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
                WITH ( NOLOCK )
                JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
                WITH ( NOLOCK ) ON CAST(B4T_Header.transaction_id AS VARCHAR(20)) = CAST(B4T_Detail.transaction_id AS VARCHAR(20))
                LEFT JOIN [Book4Time_ETL].dbo.[B4T_transaction_log_detail_tax] B4T_Tax
                WITH ( NOLOCK ) ON B4T_Tax.transaction_line_id = B4T_Detail.transaction_line_id
                LEFT OUTER JOIN [Book4Time_ETL].[dbo].B4T_product_master B4TPM
                WITH ( NOLOCK ) ON B4TPM.product_id = B4T_Detail.product_id
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item WITH ( NOLOCK ) ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
                WITH ( NOLOCK ) ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = B4T_Detail.item_code )
                                   AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
                                                              WHEN Item.[Sales Unit of Measure] IN (
                                                              'PK', 'CS' )
                                                              THEN [Base Unit of Measure]
                                                              WHEN Item.[Item Category Code] = 'RTD'
                                                              THEN 'CS'
                                                              ELSE [Sales Unit of Measure]
                                                              END )
                LEFT JOIN [NAV_ETL].[dbo].[Snapshot$NAV_InventoryStdCost] isc
                WITH ( NOLOCK ) ON B4T_Detail.item_code = isc.ItemNo COLLATE DATABASE_DEFAULT
                                   AND ( SELECT CONVERT(VARCHAR(10), B4T_Header.Transaction_Date, 121)
                                       ) = isc.RunDate
                LEFT JOIN [Book4Time_ETL].[dbo].[B4T_location] Location WITH ( NOLOCK ) ON B4T_Header.location_id = Location.location_id
                LEFT JOIN BI_SSAS_Cubes.dbo.DimselltoCustomerKey cust WITH ( NOLOCK ) ON CONVERT(VARCHAR, [customer_id])
                                                              + ''
                                                              + CONVERT(NVARCHAR, B4T_Header.location_id) = cust.SelltoCustomerKey
            --    left join BI_SSAS_Cubes.dbo.MDM m
            --on CAST(B4T_Detail.transaction_id AS VARCHAR(20))=m.DocNo
      WHERE     B4T_Header.transaction_type = 'r'
                AND item_code NOT IN ( 'ASF', 'RC', 'RCC','HOUSE','BAD DEBT'  )
                --AND DATEADD(D, 0, DATEDIFF(D, 0, transaction_date))>='2011-01-01')
                AND DATEADD(D, 0, DATEDIFF(D, 0, transaction_date)) >= DATEADD(dd,
                                                              DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                              0)
    )/*** Section 1***/


union all

Select 'USA' as Country,
                'MWCC' AS [CustomerPostingGroup] ,
                'CC' AS SalesChannel ,
                'MWCC Coupon' [ItemCategoryCode] ,
                'MWCC Coupon' [ItemCategoryCodeSubcategory],
                 SelltoCustomerID ,
                SelltoCustomerID + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS ShiptoCustomerKey ,
                SelltoCustomerID + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS selltoCustomerkey ,
                'MWCC Coupon' AS ItemCode ,
                'MWCC Coupon' AS ItemCodeSubcategory,
                CAST(B4T_Header.transaction_id AS VARCHAR(20)) AS DocumentNo ,
                0.00 AS LineDiscount ,
                Coupon*-1 AS InvoiceDiscount ,
                0 as  Units ,
                0 as SubCategoryUnits,
                0 AS UnitsPerBox ,
                0 as  Box ,
                Coupon*-1 as  Amount ,
                Coupon*-1 AS [Gross Amount] ,
                Coupon*-1 as  [Amount Including VAT] ,
                0.00 as TaxAmount ,
                'NON TAXABL'AS [Tax Group Code] ,
                0.00 AS [Unit Cost(LCY)] ,
                0 AS Quantity ,
                2 AS SalesType ,
                location_code AS LocationID ,
                CONVERT (VARCHAR(20), location_code) AS RegionID ,
                DATEADD(D, 0, DATEDIFF(D, 0, a.transaction_date)) AS [Posting Date]
                from [Book4Time_ETL].[dbo].[B4T_TransactionSummaryList] a 
left join [BI_SSAS_Cubes].[dbo].[DimSalesOrder] b
on convert(varchar(10),a.transaction_id)=b.DocumentNo
left join [Book4Time_ETL].[dbo].[B4T_transaction_log_header]  B4T_Header
on a.transaction_id= B4T_Header.transaction_id
--left join BI_SSAS_Cubes.dbo.MDM m
--            on b.DocumentNo=m.DocNo
where COUPON>0
   --AND DATEADD(D, 0, DATEDIFF(D, 0, a.transaction_date))>='2011-01-01'
   AND DATEADD(D, 0, DATEDIFF(D, 0, a.transaction_date)) >= DATEADD(dd,
                                                              DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                             0)
