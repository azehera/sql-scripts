﻿



/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_ShiptoCustomerKey]  
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Dimesion data for cube from NAV_ETL database     
PARAMETERS/VARRIABLES:
NOTES:
Modified 8/14/2014 kalpesh Patel
Date : 04/01/2020
Notes: Rewrite the entire stored procedure to get data from Sales Invoice Header & Customer Master data(MDM_Reltio).


Date : 05/04/2020
Notes: Ignore Selltocustomerid with Null Value
*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_ShiptoCustomerKey]
AS 
 truncate table [BI_SSAS_Cubes].[dbo].[DimShiptoCustomerKey]


IF object_id('Tempdb..#ShiptoCustomerKey') Is Not Null DROP TABLE #ShiptoCustomerKey;

CREATE TABLE #ShiptoCustomerKey(
	[ShiptoCustomerKey] [nvarchar](150) NULL,
	[SelltoCustomerID] [nvarchar](60) NULL,
	[Ship-to Customer Name] [nvarchar](110) NULL,
	[Ship-to City] [nvarchar](50) NULL,
	[Zip5] [nvarchar](20) NULL,
	[Zip3] [nvarchar](20) NULL,
	[Zip2] [nvarchar](20) NULL,
	[Country] [nvarchar](10) NULL,
	[Payment Discount %] [decimal](38, 20) NULL,
	[Currency Code] [nvarchar](10) NULL,
	[Currency Factor] [decimal](38, 20) NULL,
	[Ship-to County] [nvarchar](30) NULL,
	[Email] [nvarchar](80) NULL,
	[DMA Code] [varchar](50) NULL,
	[DMA Name] [varchar](50) NULL
) ON [PRIMARY];


INSERT #ShiptoCustomerKey
(
[ShiptoCustomerKey],
[SelltoCustomerID] ,
[Ship-to Customer Name] ,
[Ship-to City] ,
[Zip5] ,
[Zip3] ,
[Zip2] ,
[Country] ,
[Ship-to County] ,
[Email] ,
[DMA Code] ,
[DMA Name] 
)

SELECT LTRIM(RTRIM([ShiptoCustomerKey])) as [ShiptoCustomerKey],
LTRIM(RTRIM([SelltoCustomerID])) as [SelltoCustomerID],
LTRIM(RTRIM([Ship-to Customer Name])) as [Ship-to Customer Name],
LTRIM(RTRIM([Ship-to City])) as [Ship-to City],
LTRIM(RTRIM([Zip5])) as [Zip5],
LTRIM(RTRIM([Zip3])) as [Zip3],
LTRIM(RTRIM([Zip2])) as [Zip2],
LTRIM(RTRIM([Country])) as [Country],
LTRIM(RTRIM([Ship-to County])) as [Ship-to County],
LTRIM(RTRIM([Email])) as [Email],
LTRIM(RTRIM([DMA Code])) as [DMA Code],
LTRIM(RTRIM([DMA Name])) as [DMA Name] 
FROM ( 
SELECT *, ROW_NUMBER() over (partition by [ShiptoCustomerKey] Order by [SelltoCustomerID]) as nox
 FROM (
select CONVERT(nvarchar,concat(RTRIM(LTRIM(a.[Sell-to Customer No_] COLLATE DATABASE_DEFAULT )),'',LEFT(replace(ltrim(rtrim(a.[Ship-to Post Code] COLLATE DATABASE_DEFAULT )),CHAR(10),'') ,5))) as [ShiptoCustomerKey],
	CONVERT(nvarchar,a.[Sell-to Customer No_]) as [SelltoCustomerID], 
	CONVERT(nvarchar,a.[Ship-to Name]) as [Ship-to Customer Name],
	CONVERT(nvarchar,'') as [Ship-to City],
	CONVERT(nvarchar,LEFT(a.[Ship-to Post Code],5)) [Zip5],
	CONVERT(nvarchar,LEFT(a.[Ship-to Post Code],3)) [Zip3],
	CONVERT(nvarchar,LEFT(a.[Ship-to Post Code],2)) [Zip2],
	CONVERT(nvarchar,[Ship-to County]) [Ship-to County],
	CONVERT(nvarchar,[Ship-to Country_Region Code]) [Country],
    CONVERT(nvarchar,'') [Email],
	CONVERT(varchar,'Unknown') [DMA Code],
	CONVERT(varchar,'Unknown') [DMA Name]
	 from [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] a
	     where [Customer Posting Group]<>'CORPCLINIC'
                     and [Posting Date]>='2011-01-01'
UNION
select CONVERT(nvarchar,concat(RTRIM(LTRIM(a.[Sell-to Customer No_]  COLLATE DATABASE_DEFAULT)),'',LEFT(replace(ltrim(rtrim(a.[Ship-to Post Code]  COLLATE DATABASE_DEFAULT)),CHAR(10),'') ,5))) as [ShiptoCustomerKey],
	CONVERT(nvarchar,a.[Sell-to Customer No_]) as [SelltoCustomerID], 
	CONVERT(nvarchar,a.[Ship-to Name]) as [Ship-to Customer Name],
	CONVERT(nvarchar,'') as [Ship-to City],
	CONVERT(nvarchar,LEFT(a.[Ship-to Post Code],5)) [Zip5],
	CONVERT(nvarchar,LEFT(a.[Ship-to Post Code],3)) [Zip3],
	CONVERT(nvarchar,LEFT(a.[Ship-to Post Code],2)) [Zip2],
	CONVERT(nvarchar,'') [Ship-to County],
	CONVERT(nvarchar,[Ship-to Country_Region Code]) [Country],
    CONVERT(nvarchar,'') [Email],
	CONVERT(varchar,'Unknown') [DMA Code],
	CONVERT(varchar,'Unknown') [DMA Name]
	 from [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] a
	     where [Customer Posting Group]<>'CORPCLINIC'
                     and [Posting Date]>='2011-01-01' )SUB
					 ) A 
					 where nox = 1



 

IF object_id('Tempdb..#ShiptoCustomerKey_Reltio') Is Not Null DROP TABLE #ShiptoCustomerKey_Reltio;
CREATE TABLE #ShiptoCustomerKey_Reltio(
[ShiptoCustomerKey] [nvarchar](150) NULL,
[SelltoCustomerID] [nvarchar](60) NULL,
[Ship-to Customer Name] [nvarchar](110) NULL,
[Ship-to City] [nvarchar](50) NULL,
[Zip5] [nvarchar](20) NULL,
[Zip3] [nvarchar](20) NULL,
[Zip2] [nvarchar](20) NULL,
[Ship-to County] [nvarchar](30) NULL,
[Country] [nvarchar](10) NULL,
[Payment Discount %] [decimal](38, 20) NULL,
[Currency Code] [nvarchar](10) NULL,
[Currency Factor] [decimal](38, 20) NULL,
[Email] [nvarchar](80) NULL,
[DMA Code] [varchar](50) NULL,
[DMA Name] [varchar](50) NULL
) ON [PRIMARY];

/**********************Insert from Reltio MDM **************************/
with cte as 
(
Select distinct
 CONVERT(nvarchar,RTRIM(LTRIM(B.Medifast_customer_id COLLATE DATABASE_DEFAULT)) +''+ LEFT(RTRIM(LTRIM(A.ShipToPostalCode COLLATE DATABASE_DEFAULT)),5))  ShiptoCustomerKey,
 CONVERT(nvarchar,B.Medifast_customer_id) as [SelltoCustomerID],
dense_rank() over (partition by CONVERT(nvarchar,RTRIM(LTRIM(B.Medifast_customer_id COLLATE DATABASE_DEFAULT)) +''+ LEFT(RTRIM(LTRIM(A.ShipToPostalCode COLLATE DATABASE_DEFAULT)),5)) ,RTRIM(LTRIM(A.Email)) 
order by case when (B.Medifast_customer_id is null and B.Medifast_customer_id='NULL') or ltrim(rtrim(B.Medifast_customer_id))<>'' then 1 else 2 end) as no,
 CONVERT(nvarchar,(A.ShipToFirstName+' '+A.ShipToLastName)) [Ship-to Customer Name],
CONVERT(nvarchar,A.ShipToCity) [Ship-to City],
CONVERT(nvarchar,LEFT(A.ShipToPostalCode,5)) as Zip5,
CONVERT(nvarchar,LEFT(A.ShipToPostalCode,3)) as Zip3,
CONVERT(nvarchar,LEFT(A.ShipToPostalCode,2)) as Zip2,
CONVERT(nvarchar,A.ShipToStateCode) [Ship-to County],
CONVERT(nvarchar,A.ShipToCountryCode) [Country],
CONVERT(nvarchar,A.Email) [Email],
CONVERT(decimal,0.0) as[Payment Discount %],
CONVERT(nvarchar,'') [Currency Code],
CONVERT(decimal,0.0) as [Currency Factor],
CONVERT(varchar,isnull([DMA Code],'Unknown')) as [DMA Code],
CONVERT(varchar,isnull([DMA Name],'Unknown')) as [DMA Name]
from mdm_reltio.dbo.RELTIO_MDM_CUSTOMERS A (nolock)
LEFT JOIN mdm_reltio.[dbo].[RELTIO_GROUP_MDM_CUSTOMERS_ALL] B (nolock) on A.Reltio_Id=B.Reltio_Id COLLATE SQL_Latin1_General_CP1_CI_AS
left join [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] D with (nolock)
    on  LEFT (A.BillToPostalCode,5)=D.zipcode COLLATE SQL_Latin1_General_CP1_CI_AS and [PrimaryRecord]='P'
left join [Zip_codes_deluxe].[dbo].[DMA_Data] c with (nolock)
	on LEFT (A.BillToPostalCode,5)=c.ZipCode COLLATE SQL_Latin1_General_CP1_CI_AS
where A.ReltioStatus='Active' and B.ReltioStatus='Active' and B.Sources not in('ReltioCleanser','ExactTarget','Reltio','Hyperwallet')				
		
)
Insert into #ShiptoCustomerKey_Reltio
(
[ShiptoCustomerKey],
[SelltoCustomerID],
[Ship-to Customer Name],
[Ship-to City],
[Zip5],
[Zip3],
[Zip2],
[Ship-to County],
[Country],
[Payment Discount %],
[Currency Code],
[Currency Factor],
[Email],
[DMA Code],
[DMA Name]
)
Select LTRIM(RTRIM([ShiptoCustomerKey])) as [ShiptoCustomerKey]
		,LTRIM(RTRIM([SelltoCustomerID])) as [SelltoCustomerID]
		,LTRIM(RTRIM([Ship-to Customer Name])) as [Ship-to Customer Name]
		,LTRIM(RTRIM([Ship-to City])) as [Ship-to City]
		,LTRIM(RTRIM(Zip5)) as Zip5
		,LTRIM(RTRIM(Zip3)) as Zip3
		,LTRIM(RTRIM(Zip2)) as Zip2
		,LTRIM(RTRIM([Ship-to County])) as [Ship-to County]
		,LTRIM(RTRIM([Country])) as [Country]
		,[Payment Discount %]
		,LTRIM(RTRIM([Currency Code])) as [Currency Code]
		,[Currency Factor]
		,LTRIM(RTRIM([Email])) as [Email]
		,LTRIM(RTRIM([DMA Code])) as [DMA Code]
		,LTRIM(RTRIM([DMA Name])) as [DMA Name]
		--into #ShiptoCustomerKey
		from cte where no=1



Update SK
 SET SK.[Ship-to Customer Name] = SR.[Ship-to Customer Name]
		,SK.[Ship-to City]=SR.[Ship-to City]
		,SK.Zip5=SR.Zip5
		,SK.Zip3=SR.Zip3
		,SK.Zip2=SR.Zip2
		,SK.[Ship-to County]=SR.[Ship-to County]
		,SK.[Country]=SR.[Country]
		,SK.[Payment Discount %]=SR.[Payment Discount %]
		,SK.[Currency Code]=SR.[Currency Code]
		,SK.[Currency Factor]=SR.[Currency Factor]
		,SK.[Email]=SR.[Email]
		,SK.[DMA Code]=SR.[DMA Code]
		,SK.[DMA Name] =SR.[DMA Name]
from #ShiptoCustomerKey SK
Inner Join #ShiptoCustomerKey_Reltio SR
on case when SK.SelltoCustomerID like 'TSFL-%' THEN SUBSTRING(SK.SelltoCustomerID,6,20)
                     when SK.SelltoCustomerID like 'TSFL%' THEN SUBSTRING(SK.SelltoCustomerID,5,20)
					 ELSE SK.SelltoCustomerID END = SR.SelltoCustomerID  

INSERT [BI_SSAS_CUBES].[dbo].[DimShiptoCustomerKey]
(
[ShiptoCustomerKey],
[SelltoCustomerID],
[Ship-to Customer Name],
[Ship-to City],
[Zip5],
[Zip3],
[Zip2],
[Country],
[Payment Discount %],
[Currency Code],
[Currency Factor],
[Ship-to County],
[Email],
[DMA Code],
[DMA Name]
)
select REPLACE([ShiptoCustomerKey], char(9), '') as [ShiptoCustomerKey]
      ,REPLACE([SelltoCustomerID], char(9), '') as [SelltoCustomerID]
      ,REPLACE([Ship-to Customer Name], char(9), '') as [Ship-to Customer Name]
      ,REPLACE([Ship-to City], char(9), '') as [Ship-to City]
      ,REPLACE([Zip5], char(9), '') as [Zip5]
      ,REPLACE([Zip3], char(9), '') as [Zip3]
      ,REPLACE([Zip2], char(9), '') as [Zip2]
      ,REPLACE([Country], char(9), '') as [Country]
      ,[Payment Discount %]
      ,REPLACE([Currency Code], char(9), '') as [Currency Code]
      ,[Currency Factor]
      ,REPLACE([Ship-to County], char(9), '') as [Ship-to County]
      ,REPLACE([Email], char(9), '') as [Email]
      ,REPLACE([DMA Code], char(9), '') as [DMA Code]
      ,REPLACE([DMA Name], char(9), '') as [DMA Name] from #ShiptoCustomerKey SK
	  Where [SelltoCustomerID] IS NOT NULL

 
/* Update to remove Null Values */

Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set Zip5 = ''
where Zip5 is null

Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set Zip3 = ''
where Zip3 is null

Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set Zip2 = ''
where Zip2 is null

Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set Email = ''
where Email is null

Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set [Ship-to City] = NULL
where [Ship-to City] ='' 


Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set [Ship-to County] = NULL
where [Ship-to County] ='' 

Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set Country = NULL
where Country ='' 

Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set [Ship-to Customer Name] = NULL
where [Ship-to Customer Name] ='' 

Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set [Ship-to Customer Name] = REPLACE([Ship-to Customer Name], CHAR(13), '')
where (RTRIM([Ship-to Customer Name]) LIKE '%' + CHAR(13) +  '%')


Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set [Ship-to Customer Name] = REPLACE([Ship-to Customer Name], CHAR(9), '')
where (RTRIM([Ship-to Customer Name]) LIKE '%' + CHAR(9) +  '%')


Update [BI_SSAS_Cubes].dbo.DimShiptoCustomerKey
set [Ship-to Customer Name] = REPLACE([Ship-to Customer Name], CHAR(10), '')
where (RTRIM([Ship-to Customer Name]) LIKE '%' + CHAR(10) +  '%')
