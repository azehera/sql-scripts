﻿/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_FactSalesKey] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Fact data for cube from Nav_ETL database     
PARAMETERS/VARRIABLES:
NOTES: 
Date: 9/18/2018 Developer :Ronak Shah
Description : Modify this SP to include RDCWMS data (Location ID 7)
NOTES:
Date : 02/23/2019 Developer: Ronak Shah
Description: Add Below condition to existing sp to capture new way KIT expansion information
Condition: and SalesInvHeader2.[External Document No_] COLLATE DATABASE_DEFAULT =a.MOA_ORDER_NBR
NOTES:
Date :08/27/2019 Developer: Ronak Shah
Description: Add additional columns to factsales table so need to populate these columns
Addtional column details: Order date
Date  :10/07/2019 Developer: Ronak Shah
Description: Add Arubutus WMS to this SP
Date : 04/01/2020 Developer: Ronak Shah
Description: Modify the stored procedure to get data from Item Master & Staging table
Date : 05/04/2020 Developer: Ronak Shah
Notes: Ignore Selltocustomerid with Null Value
Date : 05/07/2020 Developer :Ronak Shah
Notes: Include distinct logic in @ItemUOM table Variable to avoid duplicate records
Date :05/22/2020 Developer :Ronak Shah
Notes : Include LTRIM RTRIM logic in SelltoCustomerkey & Shiptocustomerkey 
Date: 04/07/2021 Developer: Atya DEHA-1135
Description: The warehouse code on Oracle will be USA_HDG and Location code will be HDGWMS
*******************************************************************************************/
CREATE PROCEDURE [dbo].[BI_NAV_FactSalesKey]
AS 

 
 DECLARE @ItemUOM TABLE
(
  [Item No_] Varchar(100), 
  Code Varchar(100),
  [Qty_ per Unit of Measure] Decimal (12,6)
)

INSERT @ItemUOM ([Item No_],Code,[Qty_ per Unit of Measure])
select distinct ItemSKU as [Item No_], Case When Code ='Box' THEN 'BX' 
                     When Code = 'Case' Then 'CS'
                     Else Code END as Code,
  Quantity [Qty_ per Unit of Measure]
from [MDM_Reltio].[dbo].[RELTIO_MDM_Product] with(NOLOCK)
cross apply
(
  values
  (Box_Code, Box_Quantity),
  (Case_Code, Case_Quantity),
  ('EA',1)
) c (Code, Quantity);

Declare @MOA TABLE
(
[MOA_ASSOC_SKU_UOM] nvarchar(40),
[MOA_ASSOC_SKU] nvarchar(128),
MOA_ASSOC_QTY int,
[MOA_SKU] nvarchar(128),
[MOA_ORL_ID] Bigint,
DocumentNo Varchar(20) 
)

Insert @MOA  ([MOA_ASSOC_SKU_UOM],[MOA_ASSOC_SKU],MOA_ASSOC_QTY,[MOA_SKU],[MOA_ORL_ID],DocumentNo)
SELECT distinct [MOA_ASSOC_SKU_UOM],[MOA_ASSOC_SKU],MOA_ASSOC_QTY,[MOA_SKU],[MOA_ORL_ID]+1 as [MOA_ORL_ID],DocumentNo
FROM [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION] with(NOLOCK)   
where MOA_SYS_TIMESTAMP >=Getdate()-60

  
SELECT   Case When STG.[Ship-to Country_Region Code] IN ('US','') THEN 'USA' ELSE STG.[Ship-to Country_Region Code] END as Country,
            STG.[Customer Posting Group] AS [CustomerPostingGroup] ,
            STG.[Shortcut Dimension 1 Code] AS SalesChannel ,
           Case When STG.[Item Category Code] IS NULL OR LEN(RTRIM(LTRIM(STG.[Item Category Code])))= 0 THEN 'NON-ITEM' ELSE STG.[Item Category Code] END as [ItemCategoryCode],
           Case When STG.[Item Category Code] IS NULL OR LEN(RTRIM(LTRIM(STG.[Item Category Code])))= 0 THEN 'NON-ITEM' 
		       when [MOA_ASSOC_SKU_UOM]='KT' then 'KITS'
                 ELSE STG.[Item Category Code]    
            END AS [ItemCategoryCodeSubcategory] ,
            STG.[Sell-to Customer No_] AS SelltoCustomerID ,
            CONVERT(nvarchar,concat(RTRIM(LTRIM(STG.[Sell-to Customer No_] COLLATE DATABASE_DEFAULT )),'',LEFT(replace(ltrim(rtrim(STG.[Ship-to Post Code] COLLATE DATABASE_DEFAULT )),CHAR(10),'') ,5))) as [ShiptoCustomerKey],
            CONVERT(nvarchar,concat(RTRIM(LTRIM(STG.[Sell-to Customer No_] COLLATE DATABASE_DEFAULT)),'',LEFT(replace(ltrim(rtrim(STG.[Sell-to Post Code] COLLATE DATABASE_DEFAULT)),CHAR(10),'') ,5)))as [SelltoCustomerKey],  
            STG.No_ AS ItemCode ,
            isnull(Convert(varchar(50),[MOA_ASSOC_SKU]),STG.No_)COLLATE DATABASE_DEFAULT AS ItemCodeSubCategory ,
            STG.[Document No_] AS DocumentNo ,
            STG.[Line Discount Amount] AS LineDiscount ,
            ( STG.[Inv_ Discount Amount] ) AS InvoiceDiscount ,
             Case When STG.[Item Category Code] IS NULL OR LEN(RTRIM(LTRIM(STG.[Item Category Code])))= 0 THEN  '0'
                 ELSE (STG.[Quantity]*STG.[Qty_ per Unit of Measure])
            END AS Units ,
            CASE When STG.[Item Category Code] IS NULL OR LEN(RTRIM(LTRIM(STG.[Item Category Code])))= 0 THEN  '0'
                 when [MOA_ASSOC_SKU_UOM]='KT' then 
                 case when row_number () over ( partition by [Document No_],[MOA_ASSOC_SKU] Order by [Document No_],[MOA_ASSOC_SKU])=1 then 1*MOA_ASSOC_QTY else 0 end 
                 ELSE (STG.[Quantity]*STG.[Qty_ per Unit of Measure]) end as SubCategoryUnits 
            ,ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsPerBox ,
           Case When ISNULL(ItemUOM.[Qty_ per Unit of Measure],'1') != 0 Then   ( STG.[Quantity (Base)] / ISNULL(ItemUOM.[Qty_ per Unit of Measure],'1') ) 
		              ELSE 0 END AS BOX ,  --Update this logic to avoid divide by 0 
		    STG.[Amount] ,
            STG.[Line Amount] AS [Gross Amount] ,
            STG.[Amount Including VAT] ,
            CASE WHEN STG.[Tax Group Code] = ' '
                 THEN ( STG.[Amount Including VAT] + ( -1 * STG.[Amount] ) )
                 ELSE ( STG.[Amount Including VAT] - STG.[Amount] )
            END AS TaxAmount ,
            STG.[Tax Group Code] ,
            CASE WHEN STG.[Unit Cost (LCY)] = 0 THEN 0
			       When STG.[Qty_ per Unit of Measure] != 0.00 THEN (STG.[Unit Cost (LCY)]/ STG.[Qty_ per Unit of Measure] ) 
				   ELSE 0.00
             END AS [Unit Cost (LCY)] , --Update this logic to avoid divide by 0 
            STG.Quantity,
            1 AS SalesType ,
            CASE WHEN STG.[Location Code] = 'SUNRISE' THEN 1
                 WHEN STG.[Location Code] = 'TDC' THEN 2
                 WHEN STG.[Location Code] = 'TSFL' THEN 3
                 WHEN STG.[Location Code] = 'TDCWMS' THEN 4
                 WHEN STG.[Location Code] = 'MDCWMS' THEN 5
				 WHEN STG.[Location Code] = 'RDCWMS' THEN 7
				 WHEN STG.[Location Code] = 'ARBWMS' THEN 8
				 WHEN STG.[Location Code] = 'HK DC' THEN 9
				 WHEN STG.[Location Code] = 'Medifast HQ' THEN 10
				 WHEN STG.[Location Code] = 'SGP' THEN 11
				 WHEN STG.[Location Code] = 'HDGWMS' THEN 12		--DEHA-1135
                 ELSE 6
            END AS LocationID,
            LEFT(STG.[Sell-to Post Code], 4) AS RegionID,
            STG.[Posting Date],
			 Case When STG.[Order Date] IS NULL THEN STG.[Posting Date] ElSE STG.[Order Date] END as [Order Date] 
From BI_SSAS_CUBES.dbo.Factsales_STG STG with(NOLOCK)
LEFT JOIN @ItemUOM ItemUOM 
             ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = STG.[No_] )
				AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
										WHEN STG.[Item Category Code] = 'RTD'
										THEN 'CS'
										ELSE STG.[Unit of Measure]
										END )
Left join @MOA a  --This Query is for to avoid duplicate order numbers
			ON STG.No_ COLLATE DATABASE_DEFAULT =a.[MOA_SKU]
				and  CAST(a.[MOA_ORL_ID] AS VARCHAR(20))=[Order Item No_]
				and STG.[Document No_] COLLATE DATABASE_DEFAULT =a.DocumentNo
WHERE  [Posting Date] >=DATEADD(dd,DATEDIFF(dd,0,GETDATE() - 30),0) --AND [Posting Date] <=DATEADD(dd,DATEDIFF(dd,0,GETDATE() -1),0)
      AND  STG.RecordType ='Invoice_Record' and [Shortcut Dimension 1 Code] <> ' '
	 AND [Customer Posting Group] not in ( 'CORPCLINIC','CANCO')
     and  No_ not in ('31011','31021','32035','34822','39240','40492','40540',
	                '40550','40650','65050','65460','72755','74000','74001',
	                '74002','74008','74050','74051','74058','74059','74100',
	                '74101','74102','74108','74200','74201','74208','74300',
	                '74350','74351','74400','74500','74600','74700','74850',
	                '74855','74856','74860','74865','74866','74867','74868',
	                '74870','74920','75005','75010','75030','75035','75040',
	                '75050','75055','75060','RC', 'RCC')
AND STG.[Sell-to Customer No_] IS NOT NULL

UNION ALL


SELECT  Case When STG.[Ship-to Country_Region Code] IN ('US','') THEN 'USA' ELSE STG.[Ship-to Country_Region Code] END as Country,
            STG.[Customer Posting Group] AS [CustomerPostingGroup] ,
            STG.[Shortcut Dimension 1 Code] AS SalesChannel ,
            Case When STG.[Item Category Code] IS NULL OR LEN(RTRIM(LTRIM(STG.[Item Category Code])))= 0 THEN 'NON-ITEM' ELSE STG.[Item Category Code] END as [ItemCategoryCode],
           Case When STG.[Item Category Code] IS NULL OR LEN(RTRIM(LTRIM(STG.[Item Category Code])))= 0 THEN 'NON-ITEM' 
		       when [MOA_ASSOC_SKU_UOM]='KT' then 'KITS'
                 ELSE STG.[Item Category Code]    
            END AS [ItemCategoryCodeSubcategory] ,
            STG.[Sell-to Customer No_] AS SelltoCustomerID ,
            CONVERT(nvarchar,concat(RTRIM(LTRIM(STG.[Sell-to Customer No_] COLLATE DATABASE_DEFAULT )),'',LEFT(replace(ltrim(rtrim(STG.[Ship-to Post Code] COLLATE DATABASE_DEFAULT )),CHAR(10),'') ,5))) as [ShiptoCustomerKey],
            CONVERT(nvarchar,concat(RTRIM(LTRIM(STG.[Sell-to Customer No_] COLLATE DATABASE_DEFAULT)),'',LEFT(replace(ltrim(rtrim(STG.[Sell-to Post Code] COLLATE DATABASE_DEFAULT)),CHAR(10),'') ,5))) AS SelltoCustomerKey ,
            STG.No_ AS ItemCode ,
            STG.No_ AS ItemCodeSubcategory ,
            [Document No_] AS DocumentNo ,
            [Line Discount Amount] AS LineDiscount ,
            ( [Inv_ Discount Amount] * -1 ) AS InvoiceDiscount ,
            Case When STG.[Item Category Code] IS NULL OR LEN(RTRIM(LTRIM(STG.[Item Category Code])))= 0 THEN  '0'
                 ELSE (STG.[Quantity]*STG.[Qty_ per Unit of Measure])*-1
            END AS Units ,
            CASE When STG.[Item Category Code] IS NULL OR LEN(RTRIM(LTRIM(STG.[Item Category Code])))= 0 THEN  '0'
                 when [MOA_ASSOC_SKU_UOM]='KT' then 
                 case when row_number () over ( partition by [Document No_],[MOA_ASSOC_SKU] Order by [Document No_],[MOA_ASSOC_SKU])=1 then 1*MOA_ASSOC_QTY else 0 end 
                 ELSE (STG.[Quantity]*STG.[Qty_ per Unit of Measure])*-1 end as SubCategoryUnits 
            ,ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsPerBox ,
           Case When ISNULL(ItemUOM.[Qty_ per Unit of Measure],'1') != 0 Then   ( STG.[Quantity (Base)] / ISNULL(ItemUOM.[Qty_ per Unit of Measure],'1') ) 
		              ELSE 0 END AS BOX ,  --Update this logic to avoid divide by 0 
		    [Amount]*-1  AS Amount ,
            ( [Line Amount]*-1  ) AS [Gross Amount] ,
            [Amount Including VAT] ,
            CASE WHEN STG.[Tax Group Code] = ' '
                 THEN ( [Amount Including VAT] + ( -1 * [Amount] ) )
                 ELSE ( [Amount Including VAT] - [Amount] ) *-1
            END AS TaxAmount ,
            STG.[Tax Group Code] ,
           CASE WHEN STG.[Unit Cost (LCY)] = 0 THEN 0
			       When STG.[Qty_ per Unit of Measure] != 0.00 THEN (STG.[Unit Cost (LCY)]/ STG.[Qty_ per Unit of Measure] )  
				   ELSE 0.00
             END AS [Unit Cost (LCY)] , --Update this logic to avoid divide by 0 
             Quantity * -1  AS Quantity ,
            2 AS SalesType ,
            CASE WHEN STG.[Location Code] = 'SUNRISE' THEN 1
                 WHEN STG.[Location Code] = 'TDC' THEN 2
                 WHEN STG.[Location Code] = 'TSFL' THEN 3
                 WHEN STG.[Location Code] = 'TDCWMS' THEN 4
                 WHEN STG.[Location Code] = 'MDCWMS' THEN 5
				 WHEN STG.[Location Code] = 'RDCWMS' THEN 7
				 WHEN STG.[Location Code] = 'ARBWMS' THEN 8
				 WHEN STG.[Location Code] = 'HK DC' THEN 9
				 WHEN STG.[Location Code] = 'Medifast HQ' THEN 10
				 WHEN STG.[Location Code] = 'SGP' THEN 11
				 WHEN STG.[Location Code] = 'HDGWMS' THEN 12		--DEHA-1135
                 ELSE 6
            END AS LocationID,
            LEFT(STG.[Sell-to Post Code], 4) AS RegionID,
            STG.[Posting Date],
			STG.[Posting Date] as OrderDate
From BI_SSAS_CUBES.dbo.Factsales_STG STG
LEFT JOIN @ItemUOM ItemUOM  
             ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = STG.[No_] )
				AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
										WHEN STG.[Item Category Code] = 'RTD'
										THEN 'CS'
										ELSE STG.[Unit of Measure]
										END )
Left join @MOA a  --This Query is for to avoid duplicate order numbers
			ON STG.No_ COLLATE DATABASE_DEFAULT =a.[MOA_SKU]
				and  CAST(a.[MOA_ORL_ID] AS VARCHAR(20))=[Order Item No_]
				and STG.[Document No_] COLLATE DATABASE_DEFAULT =a.DocumentNo
WHERE   [Posting Date] >=DATEADD(dd,DATEDIFF(dd,0,GETDATE() -30),0) --AND [Posting Date] <=DATEADD(dd,DATEDIFF(dd,0,GETDATE() -1),0)
        AND  STG.RecordType ='Return_Invoice_Record' and [Shortcut Dimension 1 Code] <> ' '
        AND [Customer Posting Group] not in ( 'CORPCLINIC','CANCO')
        and  No_ not in ('31011','31021','32035','34822','39240','40492','40540',
	                                                     '40550','40650','65050','65460','72755','74000','74001',
	                                                     '74002','74008','74050','74051','74058','74059','74100',
	                                                     '74101','74102','74108','74200','74201','74208','74300',
	                                                     '74350','74351','74400','74500','74600','74700','74850',
	                                                     '74855','74856','74860','74865','74866','74867','74868',
	                                                     '74870','74920','75005','75010','75030','75035','75040',
	                                                     '75050','75055','75060','RC', 'RCC')
            AND STG.[Sell-to Customer No_] IS NOT NULL

