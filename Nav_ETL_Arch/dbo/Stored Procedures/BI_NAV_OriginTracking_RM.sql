﻿CREATE PROCEDURE [dbo].[BI_NAV_OriginTracking_RM] (@DateEnd AS DATETIME)
AS
/****** MED0001 MRH 2/22/12: Business Intelligence / Executive Dashboard ******/

DECLARE @DateMonthBegin DATETIME
DECLARE @DateMonthEnd DATETIME
DECLARE @DateYearBegin DATETIME
DECLARE @DateYearEnd DATETIME
DECLARE @DateYesterday DATETIME
DECLARE @DatePriorYrMonthBegin DATETIME
DECLARE @DatePriorYrYearBegin DATETIME
DECLARE @DatePriorYrYesterday DATETIME
DECLARE @DaysMonth INT
DECLARE @DaysMonthElapsed INT
DECLARE @DaysMonthRemaining INT
DECLARE @DaysYear INT
DECLARE @DaysYearElapsed INT
DECLARE @DaysYearRemaining INT

SET @DateEnd = CAST(FLOOR(CAST(@DateEnd AS float)) AS DATETIME)
SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1

BEGIN TRANSACTION

DELETE FROM [Snapshot$NAV_OriginTracking_RM]
WHERE (RunDate = @DateEnd)

COMMIT TRANSACTION

/*** Initial insert ***/

BEGIN TRANSACTION

INSERT INTO [Snapshot$NAV_OriginTracking_RM]
  ([RunDate],[Yesterday],
   [Posting Date],[Entry Type],[Document Type],[Document No_],
   [Item No_],[Unit of Measure Code],[Expiration Date],[Lot No_],[Location Code],
   [Quantity],[Invoiced Quantity],[Remaining Quantity],[Qty_ per Unit of Measure],
   [Completely Invoiced],[Cost Amount (Expected)],[Cost Amount (Actual)],[Open])
SELECT
  @DateEnd,
  @DateYesterday,
  ILE.[Posting Date] AS _PostingDate,
  CASE ILE.[Entry Type]
    WHEN 0 THEN 'Purchase'
    WHEN 1 THEN 'Sale'
    WHEN 2 THEN 'Positive Adjmt.'
    WHEN 3 THEN 'Negative Adjmt.'
    WHEN 4 THEN 'Transfer'
    WHEN 5 THEN 'Consumption'
    WHEN 6 THEN 'Output'
    ELSE ''
  END AS _EntryType,
  CASE ILE.[Document Type]
    WHEN 1 THEN 'Sales Shipment'
    WHEN 2 THEN 'Sales Invoice'
    WHEN 3 THEN 'Sales Return Receipt'
    WHEN 4 THEN 'Sales Credit Memo'
    WHEN 5 THEN 'Purchase Receipt'
    WHEN 6 THEN 'Purchase Invoice'
    WHEN 7 THEN 'Purchase Return Shipment'
    WHEN 8 THEN 'Purchase Credit Memo'
    WHEN 9 THEN 'Transfer Shipment'
    WHEN 10 THEN 'Transfer Receipt'
    WHEN 11 THEN 'Service Shipment'
    WHEN 12 THEN 'Service Invoice'
    WHEN 13 THEN 'Service Credit Memo'
    WHEN 14 THEN 'Sales Shipment'
    WHEN 15 THEN 'Sales Shipment'
    ELSE ''
  END AS _DocType,
  ILE.[Document No_] AS _DocNo,
  ILE.[Item No_] AS _ItemNo,
  ISNULL(Item.[Base Unit of Measure],'') AS BaseUom,
  ILE.[Expiration Date] AS _ExpDate,
  ILE.[Lot No_] AS _LotNo,
  ILE.[Location Code] AS _LocCode,
  ILE.[Quantity] AS _Qty,
  ILE.[Invoiced Quantity] AS _InvcdQty,
  ILE.[Remaining Quantity] AS _RemQty,
  ILE.[Qty_ per Unit of Measure] AS _QtyPerUom,
  ILE.[Completely Invoiced] AS _CompletelyInvcd,
  (
    SELECT ISNULL(SUM([Cost Amount (Expected)]),0)
    FROM [Jason Pharm$Value Entry] VE
    WHERE VE.[Item Ledger Entry No_] = ILE.[Entry No_]
  ) AS _CostAmtExp,
  (
    SELECT ISNULL(SUM([Cost Amount (Actual)]),0)
    FROM [Jason Pharm$Value Entry] VE
    WHERE VE.[Item Ledger Entry No_] = ILE.[Entry No_]
  ) AS _CostAmtAct,
  ILE.[Open] AS OpenField
FROM [Jason Pharm$Item Ledger Entry] ILE
LEFT OUTER JOIN [Jason Pharm$Item] Item
ON Item.No_ COLLATE DATABASE_DEFAULT = ILE.[Item No_]
WHERE (ILE.[Item Tracking] <> '') AND (ILE.[Open] = 1) AND (ILE.[Remaining Quantity] > 0)

COMMIT TRANSACTION