﻿





/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_FactBookForTimeSalesKey] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Fact data for cube from BookforTime database     
PARAMETERS/VARRIABLES:
NOTES:Selection 1 Loading Data for 30 days 

*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_FactBookForTimeSalesKey_bef022714]
AS
    ( SELECT    'USA' as Country,
                'MWCC' AS [CustomerPostingGroup] ,
                'CC' AS SalesChannel ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 's' )
                     THEN 'NON-ITEM'
                     ELSE Item.[Item Category Code] COLLATE DATABASE_DEFAULT
                END AS [ItemCategoryCode] ,
                CONVERT(VARCHAR, [customer_id]) AS SelltoCustomerID ,
                CONVERT(VARCHAR, [customer_id]) + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS ShiptoCustomerKey ,
                CONVERT(VARCHAR, [customer_id]) + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS selltoCustomerkey ,
                item_code AS ItemCode ,
                CAST(B4T_Header.transaction_id AS VARCHAR(20)) AS DocumentNo ,
                CAST(( discount_amt * Qty
                       * ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) AS DECIMAL(18,
                                                              2)) AS LineDiscount ,
                0.00 AS InvoiceDiscount ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 's' ) THEN '0'
                     WHEN Item.[Item Category Code] = 'RTD' THEN QTY
                     ELSE ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                END AS Units ,
                ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsPerBox ,
                CASE WHEN Item.[Item Category Code] = 'RTD'
                     THEN ( QTY / ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                     ELSE ( ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                           '1') )
                            / ( ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) )
                END AS Box ,
                ( sold_price * qty ) AS Amount ,
                ( sold_price * qty ) + 0.00 AS [Gross Amount] ,
                ( ( sold_price * qty ) + tax_amount ) AS [Amount Including VAT] ,
                tax_amount AS TaxAmount ,
                CASE WHEN tax_amount = 0 THEN 'NON TAXABL'
                     ELSE 'TAXABLE'
                END AS [Tax Group Code] ,
                [StandardCost] AS [Unit Cost(LCY)] ,
                ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) AS Quantity ,
                1 AS SalesType ,
                location_code AS LocationID ,
                CONVERT (VARCHAR(20), location_code) AS RegionID ,
                DATEADD(D, 0, DATEDIFF(D, 0, transaction_date)) AS [Posting Date]
      FROM      [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
                WITH ( NOLOCK )
                JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
                WITH ( NOLOCK ) ON CAST(B4T_Header.transaction_id AS VARCHAR(20)) = CAST(B4T_Detail.transaction_id AS VARCHAR(20))
                LEFT JOIN [Book4Time_ETL].dbo.[B4T_transaction_log_detail_tax] B4T_Tax
                WITH ( NOLOCK ) ON B4T_Tax.transaction_line_id = B4T_Detail.transaction_line_id
                LEFT OUTER JOIN [Book4Time_ETL].[dbo].B4T_product_master B4TPM
                WITH ( NOLOCK ) ON B4TPM.product_id = B4T_Detail.product_id
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item WITH ( NOLOCK ) ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
                WITH ( NOLOCK ) ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = B4T_Detail.item_code )
                                   AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
                                                              WHEN Item.[Sales Unit of Measure] IN (
                                                              'PK', 'CS' )
                                                              THEN [Base Unit of Measure]
                                                              WHEN Item.[Item Category Code] = 'RTD'
                                                              THEN 'CS'
                                                              ELSE [Sales Unit of Measure]
                                                              END )
                LEFT JOIN [NAV_ETL].[dbo].[Snapshot$NAV_InventoryStdCost] isc
                WITH ( NOLOCK ) ON B4T_Detail.item_code = isc.ItemNo COLLATE DATABASE_DEFAULT
                                   AND ( SELECT CONVERT(VARCHAR(10), B4T_Header.Transaction_Date, 121)
                                       ) = isc.RunDate
                LEFT JOIN [Book4Time_ETL].[dbo].[B4T_location] Location WITH ( NOLOCK ) ON B4T_Header.location_id = Location.location_id
                LEFT JOIN BI_SSAS_Cubes.dbo.DimselltoCustomerKey cust WITH ( NOLOCK ) ON CONVERT(VARCHAR, [customer_id])
                                                              + ''
                                                              + CONVERT(NVARCHAR, B4T_Header.location_id) = cust.SelltoCustomerKey
      WHERE     B4T_Header.transaction_type = 's'
                AND item_code NOT IN ( 'ASF', 'RC', 'RCC' )
                --AND DATEADD(D, 0, DATEDIFF(D, 0, transaction_date))>='2011-01-01'
                AND DATEADD(D, 0, DATEDIFF(D, 0, transaction_date)) >= DATEADD(dd,
                                                              DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                              0)
    )/*** Section 1***/
    UNION ALL
    ( SELECT    'USA' as Country,
                'MWCC' AS [CustomerPostingGroup] ,
                'CC' AS SalesChannel ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 'r' )
                     THEN 'NON-ITEM'
                     ELSE Item.[Item Category Code] COLLATE DATABASE_DEFAULT
                END AS [ItemCategoryCode] ,
                CONVERT(VARCHAR, [customer_id]) AS SelltoCustomerID ,
                CONVERT(VARCHAR, [customer_id]) + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS ShiptoCustomerKey ,
                CONVERT(VARCHAR, [customer_id]) + ''
                + CONVERT(VARCHAR, B4T_Header.location_ID) AS selltoCustomerkey ,
                item_code AS ItemCode ,
                CAST(B4T_Header.transaction_id AS VARCHAR(20)) AS DocumentNo ,
                CAST(( discount_amt * Qty
                       * ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) AS DECIMAL(18,
                                                              2)) AS LineDiscount ,
                0.00 AS InvoiceDiscount ,
                CASE WHEN ( Item.[Item Category Code] IS NULL )
                          AND ( B4T_Header.[transaction_type] = 'r' ) THEN '0'
                     WHEN Item.[Item Category Code] = 'RTD' THEN QTY
                     ELSE ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                END AS Units ,
                ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') AS UnitsPerBox ,
                CASE WHEN Item.[Item Category Code] = 'RTD'
                     THEN ( QTY / ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                         '1') )
                     ELSE ( ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure],
                                           '1') )
                            / ( ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) )
                END AS Box ,
                ( sold_price * qty ) AS Amount ,
                ( sold_price * qty ) + 0.00 AS [Gross Amount] ,
                ( ( sold_price * qty ) + tax_amount ) AS [Amount Including VAT] ,
                tax_amount AS TaxAmount ,
                CASE WHEN tax_amount = 0 THEN 'NON TAXABL'
                     ELSE 'TAXABLE'
                END AS [Tax Group Code] ,
                [StandardCost] AS [Unit Cost(LCY)] ,
                ( Qty * ISNULL(ItemUOM.[Qty_ per Unit of Measure], '1') ) AS Quantity ,
                2 AS SalesType ,
                location_code AS LocationID ,
                CONVERT (VARCHAR(20), location_code) AS RegionID ,
                DATEADD(D, 0, DATEDIFF(D, 0, transaction_date)) AS [Posting Date]
      FROM      [Book4Time_ETL].[dbo].[B4T_transaction_log_detail] B4T_Detail
                WITH ( NOLOCK )
                JOIN [Book4Time_ETL].[dbo].[B4T_transaction_log_header] B4T_Header
                WITH ( NOLOCK ) ON CAST(B4T_Header.transaction_id AS VARCHAR(20)) = CAST(B4T_Detail.transaction_id AS VARCHAR(20))
                LEFT JOIN [Book4Time_ETL].dbo.[B4T_transaction_log_detail_tax] B4T_Tax
                WITH ( NOLOCK ) ON B4T_Tax.transaction_line_id = B4T_Detail.transaction_line_id
                LEFT OUTER JOIN [Book4Time_ETL].[dbo].B4T_product_master B4TPM
                WITH ( NOLOCK ) ON B4TPM.product_id = B4T_Detail.product_id
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item] Item WITH ( NOLOCK ) ON Item.No_ COLLATE DATABASE_DEFAULT = B4T_Detail.item_code
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Item Unit of Measure] ItemUOM
                WITH ( NOLOCK ) ON ( ItemUOM.[Item No_] COLLATE DATABASE_DEFAULT = B4T_Detail.item_code )
                                   AND ( ItemUOM.Code COLLATE DATABASE_DEFAULT = CASE
                                                              WHEN Item.[Sales Unit of Measure] IN (
                                                              'PK', 'CS' )
                                                              THEN [Base Unit of Measure]
                                                              WHEN Item.[Item Category Code] = 'RTD'
                                                              THEN 'CS'
                                                              ELSE [Sales Unit of Measure]
                                                              END )
                LEFT JOIN [NAV_ETL].[dbo].[Snapshot$NAV_InventoryStdCost] isc
                WITH ( NOLOCK ) ON B4T_Detail.item_code = isc.ItemNo COLLATE DATABASE_DEFAULT
                                   AND ( SELECT CONVERT(VARCHAR(10), B4T_Header.Transaction_Date, 121)
                                       ) = isc.RunDate
                LEFT JOIN [Book4Time_ETL].[dbo].[B4T_location] Location WITH ( NOLOCK ) ON B4T_Header.location_id = Location.location_id
                LEFT JOIN BI_SSAS_Cubes.dbo.DimselltoCustomerKey cust WITH ( NOLOCK ) ON CONVERT(VARCHAR, [customer_id])
                                                              + ''
                                                              + CONVERT(NVARCHAR, B4T_Header.location_id) = cust.SelltoCustomerKey
      WHERE     B4T_Header.transaction_type = 'r'
                AND item_code NOT IN ( 'ASF', 'RC', 'RCC' )
                --AND DATEADD(D, 0, DATEDIFF(D, 0, transaction_date))>='2011-01-01')
                AND DATEADD(D, 0, DATEDIFF(D, 0, transaction_date)) >= DATEADD(dd,
                                                              DATEDIFF(dd, 0,
                                                              GETDATE() - 30),
                                                              0)
    )/*** Section 1***/























































































































