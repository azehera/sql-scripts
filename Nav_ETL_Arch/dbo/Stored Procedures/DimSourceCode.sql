﻿
CREATE Procedure [dbo].[DimSourceCode]


as
SET NOCOUNT ON



Select
distinct [Source Code] COLLATE DATABASE_DEFAULT  as [Source Code]
From [NAV_ETL].[dbo].[Jason Pharm$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate
Where 
 a.[G_L Account No_] >='10000'
---and  [G_L Account No_] not in ('96750','85401')----comment out on 2013-09-03 Luc Emond-----



union all 
select 
distinct [Source Code] 
From  NAV_ETL.[dbo].[7 Crondall$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate
where  a.[G_L Account No_] >='10000'
----and [G_L Account No_] not in ('96750','43500')----comment out on 2013-09-03 Luc Emond-----




union all

select 
distinct [Source Code]
From NAV_ETL.[dbo].[Jason Enterprises$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate
where  a.[G_L Account No_] >='10000'


union all
select 
distinct [Source Code]
From NAV_ETL.[dbo].[Jason Properties$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate
where  a.[G_L Account No_] >='10000'
--and [G_L Account No_] not in ('96750')----comment out on 2013-09-03 Luc Emond-----


union all

select 
distinct [Source Code]
 From NAV_ETL.[dbo].[Medifast Inc$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate
where  a.[G_L Account No_] >='10000'
--and [G_L Account No_] not in ('96750')----comment out on 2013-09-03 Luc Emond-----


union all
select 
distinct [Source Code]
From NAV_ETL.[dbo].[Take Shape For Life$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate
where  a.[G_L Account No_] >='10000'









