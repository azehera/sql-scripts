﻿


/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_MedifastNutritionSelltoCustomerKey]  
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Dimesion data for cube from NAVPROD database     
PARAMETERS/VARRIABLES:
NOTES:


Date : 05/04/2020
Notes: Ignore Selltocustomerid with Null Value

*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_MedifastNutritionSelltoCustomerKey] 
as

with cte as (SELECT 
                 CONVERT(varchar,[Sell-to Customer No_])+''+LEFT ([Sell-to Post Code],7) as ShiptoCustomerKey
                ,[Sell-to Customer No_] as SelltoCustomerID
                ,ROW_NUMBER() over (partition by [Sell-to Customer No_],LEFT ([Sell-to Post Code],7) Order by [Sell-to Customer No_]) as no
                ,[Sell-to Customer Name]
                ,[Sell-to Address]
                ,[Sell-to City] as [Sell-to City]
                ,[Sell-to County]
                ,LEFT ([Sell-to Post Code],7) as Zip5
                ,LEFT([Sell-to Post Code],3) as Zip3
                ,LEFT([Sell-to Post Code],2) as Zip2
                ,[Sell-to Country_Region Code] as Country
                ,[Payment Discount %]
                ,[Currency Code]
                ,[Currency Factor]
                ,'CANADA' as [DMA Code]
                ,'CANADA'  as [DMA Name]
                FROM NAV_ETL_ARCH.dbo.[Medifast Nutrition$Sales Invoice Header] 
                where [Customer Posting Group]<>'CORPCLINIC'
				AND [Sell-to Customer No_] IS NOT NULL
                     )
              select CONVERT(nvarchar,ShiptoCustomerKey) as SelltoCustomerKey 
                    ,SelltoCustomerID 
                    ,[Sell-to Customer Name]
                    , [Sell-to Address]
                    ,[Sell-to City]
                    ,case when [Sell-to County]=' ' then null 
                               else [Sell-to County] end as [Sell-to County]
                    ,Zip5
                    ,Zip3
                    ,Zip2
                    ,Country
                    ,[Payment Discount %]
                    ,a.[Currency Code]
                    ,[Currency Factor]
                    ,isnull([E-Mail],' ') as [E-Mail]
                    ,[DMA Code],
                     [DMA Name]
                     from cte a
                         left join NAV_ETL_ARCH.dbo.[Medifast Nutrition$Customer] c
	                          on a.SelltoCustomerID COLLATE DATABASE_DEFAULT=c.No_
	                          where no=1 


