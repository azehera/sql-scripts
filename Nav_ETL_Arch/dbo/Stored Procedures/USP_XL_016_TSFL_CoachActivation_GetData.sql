﻿--/*
--===============================================================================
--Author:         Emily Trainor
--Create date: 11/1/2012
---------------------------TSFL Tracker TSFL Coach Activation------------------
-------Pull data for TSFL Coach Activation Business Week Graph-----------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--ODYSSEY_ETL			ODYSSEY_CUSTOMER					       Select                        
--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------

--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--===============================================================================
--*/

CREATE procedure [dbo].[USP_XL_016_TSFL_CoachActivation_GetData] as 
set nocount on ;

-------------------------------Declare Date--------------------
DECLARE @DATE varchar(10)
SET @DATE = Datepart(yyyy, getdate())-1

-------------------------------Declare Date--------------------
DECLARE @DATE2 varchar(10)
SET @DATE2 = (Select Convert (Varchar (10),DATEADD(wk, DATEDIFF(wk,0,getdate()), 0),121))
---------------------Pulling TSFL Coach Activation Data ------------------
Select 
 Convert (Varchar (10),[EFFECTIVE_DATE],121)as [EFFECTIVE_DATE]
,Count([CUSTOMER_ID])as COACHES_ACTIVATED
INTO #a 
From [ODYSSEY_ETL].[dbo].[ODYSSEY_CUSTOMER]
Where convert(varchar(10),[EFFECTIVE_DATE],121) >= @Date
and convert(varchar(10),[EFFECTIVE_DATE],121) < @DATE2
Group By [EFFECTIVE_DATE]
Order By [EFFECTIVE_DATE] asc

--------------Aggregation to Business Week and Year for Excel Template (Business Graphs)------
Select 
  [CalendarWeek]
,[CalendarYear]
,sum([COACHES_ACTIVATED]) as COACHES_ACTIVATED
From #a A
inner join 
	[BI_Reporting].[dbo].[calendar] B 
	on A.[EFFECTIVE_DATE]= B.[CalendarDate]
Group By 
 [CalendarYear]
,[CalendarWeek]
Order by 
 [CalendarYear]
,[CalendarWeek]

---clean up---
drop table #A