﻿



/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_MedifastNutritionShiptoCustomerKey]
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Dimesion data for cube from NAV_ETL database     
PARAMETERS/VARRIABLES:
NOTES:


Date : 05/04/2020
Notes: Ignore Selltocustomerid with Null Value

*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_MedifastNutritionShiptoCustomerKey]
AS 
    WITH    cte
              AS ( SELECT   CONVERT(VARCHAR, [Sell-to Customer No_]) + ''
                            + LEFT([Ship-to Post Code], 7) AS ShiptoCustomerKey ,
                            [Sell-to Customer No_] AS SelltoCustomerID ,
                            ROW_NUMBER() OVER ( PARTITION BY [Sell-to Customer No_],
                                                LEFT([Ship-to Post Code], 7) ORDER BY [Sell-to Customer No_] ) AS no ,
                            [Ship-to Name] ,
                            [Ship-to City] AS [Ship-to City] ,
                            LEFT([Ship-to Post Code], 7) AS Zip5 ,
                            LEFT([Ship-to Post Code], 3) AS Zip3 ,
                            LEFT([Ship-to Post Code], 2) AS Zip2 ,
                            [Ship-to Country_Region Code] AS Country ,
                            [Payment Discount %] ,
                            [Currency Code] ,
                            [Currency Factor] ,
                            [Ship-to County] ,
                            'CANADA' AS [DMA Code] ,
                            'CANADA' AS [DMA Name]
                   FROM     NAV_ETL_ARCH.dbo.[Medifast Nutrition$Sales Invoice Header]  a
                        WHERE    [Customer Posting Group] <> 'CORPCLINIC'
						AND [Sell-to Customer No_] IS NOT NULL
                 )
        SELECT  CONVERT(NVARCHAR, ShiptoCustomerKey) AS ShiptoCustomerKey ,
                SelltoCustomerID ,
               [Ship-to Name]as[Ship-to Name] ,
                [Ship-to City] as [Ship-to City] ,
                [Payment Discount %] ,
                a.[Currency Code] ,
                [Currency Factor] ,
                Zip5
                ,Zip3
                ,Zip2,
                Country ,
                CASE WHEN [Ship-to County] = ' ' THEN NULL
                     ELSE [Ship-to County]
                END AS [Ship-to County] ,
                ISNULL([E-Mail], ' ') AS [E-Mail] ,
                [DMA Code] ,
                [DMA Name]
        FROM    cte a
                LEFT JOIN NAV_ETL_ARCH.dbo.[Medifast Nutrition$Customer] c ON a.SelltoCustomerID COLLATE DATABASE_DEFAULT = c.No_
        WHERE   no = 1 


