﻿

CREATE PROCEDURE [dbo].[BI_NAV_Funnel] (@DateEnd AS DATETIME)
AS
/****** MED0001 MRH 11/11/11: Business Intelligence / Executive Dashboard ******/

/**********************************************************************************
Modified by: Kalpesh Patel
Date: 01/31/2014
Related RFC: #1120
description: Need to remove WholesaleWebVisitors from Funnel because its free web service 
which is failing since long time and we don't have any support for that.
-------------------------------------------------------------------------------------
Modified: Derald Smith
Date: 05/27/20145
Desc: Added a TRY...CATCH to account for the coremetrics web service api call being
	non-fuctional. The URL that has to be tested is --https://welcome.coremetrics.com/analyticswebapp/api'
	if non-functional, then stored procedure will return 0 counts for trilogy training
	visitors.

***********************************************************************************/

DECLARE @DateMonthBegin DATETIME
DECLARE @DateMonthBeginPrior DATETIME
DECLARE @DateMonthEnd DATETIME
DECLARE @DateYearBegin DATETIME
DECLARE @DateYearEnd DATETIME
DECLARE @DateYesterday DATETIME
DECLARE @DatePriorYrMonthBegin DATETIME
DECLARE @DatePriorYrYearBegin DATETIME
DECLARE @DatePriorYrYesterday DATETIME
DECLARE @DaysMonth INT
DECLARE @DaysMonthElapsed INT
DECLARE @DaysMonthRemaining INT
DECLARE @DaysYear INT
DECLARE @DaysYearElapsed INT
DECLARE @DaysYearRemaining INT
DECLARE @ProcessTimeBegin DATETIME
DECLARE @ProcessTimeEnd DATETIME

SET @ProcessTimeBegin = GetDate()
SET @DateEnd = CAST(FLOOR(CAST(@DateEnd AS float)) AS DATETIME)
SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
SET @DateMonthBeginPrior = DATEADD(MM, -1, @DateMonthBegin)
SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1

BEGIN TRANSACTION

DELETE FROM [Snapshot$NAV_Funnel]
WHERE (RunDate = @DateEnd)

BEGIN try
INSERT INTO [Snapshot$NAV_Funnel]
  (RunDate,StartDateYear,StartDateMonth,StartDatePriorMonth,Yesterday,YesterdayPriorYr,StartDateMonthPriorYr,StartDateYearPriorYr,
   TTSVisitorsDay,TTSVisitorsMTD,TTSVisitorsPriorMTD,TTSVisitorsYTD,TTSVisitorsPriorYTD,
   TTSnewVisitsDay,TTSnewVisitsMTD,TTSnewVisitsPriorMTD,TTSnewVisitsYTD,TTSnewVisitsPriorYTD,
   WholesaleWebVisitorsDay,WholesaleWebVisitorsPriorMTD,WholesaleWebVisitorsPriorYTD,
   MedClientAcquisitionDay,MedClientAcquisitionMTD,MedClientAcquisitionPriorMTD,MedClientAcquisitionYTD,MedClientAcquisitionPriorYTD,
   MedDirProcNavInvcsDay,MedDirProcNavInvcsMTD,MedDirProcNavInvcsPriorMTD,MedDirProcNavInvcsYTD,MedDirProcNavInvcsPriorYTD,
   TSFLClientAcquisitionDay,TSFLClientAcquisitionMTD,TSFLClientAcquisitionPriorMTD,TSFLClientAcquisitionYTD,TSFLClientAcquisitionPriorYTD,
   TSFLSponsoringDay,TSFLSponsoringMTD,TSFLSponsoringPriorMTD,TSFLSponsoringYTD,TSFLSponsoringPriorYTD,
   TSFLWebCoachRqstDay,TSFLWebCoachRqstMTD,TSFLWebCoachRqstPriorMTD,TSFLWebCoachRqstYTD,TSFLWebCoachRqstPriorYTD,
   WholesaleProcNavInvcsDay,WholesaleProcNavInvcsMTD,WholesaleProcNavInvcsPriorMTD,WholesaleProcNavInvcsYTD,WholesaleProcNavInvcsPriorYTD,
   ActiveHealthCoachesDay,ActiveHealthCoachesPriorMTD,ActiveHealthCoachesPriorYTD,
   MWCCActiveStoreCount,
   MedDirWebVisitorsDay,MedDirWebVisitorsPriorMTD,MedDirWebVisitorsPriorYTD,
   TSFLWebVisitorsDay,
   MWCCApptsCreatedDay,MWCCApptsCreatedMTD,MWCCApptsCreatedPriorMTD,MWCCApptsCreatedYTD,MWCCApptsCreatedPriorYTD)
SELECT
  @DateEnd,
  @DateYearBegin,
  @DateMonthBegin,
  @DateMonthBeginPrior,
  @DateYesterday,
  @DatePriorYrYesterday,
  @DatePriorYrMonthBegin,
  @DatePriorYrYearBegin,
  dbo.GetTrilogyTrainingVisitors ('ga:visitors', @DateYesterday, @DateYesterday) AS _TTSVisitorsDay,
  dbo.GetTrilogyTrainingVisitors ('ga:visitors', @DateMonthBegin, @DateYesterday) AS _TTSVisitorsMTD,
  dbo.GetTrilogyTrainingVisitors ('ga:visitors', @DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _TTSVisitorsPriorMTD,
  dbo.GetTrilogyTrainingVisitors ('ga:visitors', @DateYearBegin, @DateYesterday) AS _TTSVisitorsYTD,
  dbo.GetTrilogyTrainingVisitors ('ga:visitors', @DatePriorYrYearBegin, @DatePriorYrYesterday) AS _TTSVisitorsPriorYTD,
  dbo.GetTrilogyTrainingVisitors ('ga:newVisits', @DateYesterday, @DateYesterday) AS _TTSnewVisitsDay,
  dbo.GetTrilogyTrainingVisitors ('ga:newVisits', @DateMonthBegin, @DateYesterday) AS _TTSnewVisitsMTD,
  dbo.GetTrilogyTrainingVisitors ('ga:newVisits', @DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _TTSnewVisitsPriorMTD,
  dbo.GetTrilogyTrainingVisitors ('ga:newVisits', @DateYearBegin, @DateYesterday) AS _TTSnewVisitsYTD,
  dbo.GetTrilogyTrainingVisitors ('ga:newVisits', @DatePriorYrYearBegin, @DatePriorYrYesterday) AS _TTSnewVisitsPriorYTD,
  /************************ Kalpesh Patel - 1/31/2014 - Removed the WebVisitors function call ******************/
  0 AS _WholesaleWebVisitorsDay,--dbo.GetMedWebDoctorEmails (@DateYesterday, @DateYesterday) 
  0  AS _WholesaleWebVisitorsPriorMTD,--dbo.NavFunnel_GetMedWebDoctorEmails (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _WholesaleWebVisitorsPriorMTD,
  0 AS _WholesaleWebVisitorsPriorYTD,--dbo.NavFunnel_GetMedWebDoctorEmails (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _WholesaleWebVisitorsPriorYTD,
  dbo.NavFunnel_GetMedClientAcquisition (@DateYesterday, @DateYesterday) AS _MedClientAcquisitionDay,
  dbo.NavFunnel_GetMedClientAcquisition (@DateMonthBegin, @DateYesterday) AS _MedClientAcquisitionMTD,
  dbo.NavFunnel_GetMedClientAcquisition (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _MedClientAcquisitionPriorMTD,
  dbo.NavFunnel_GetMedClientAcquisition (@DateYearBegin, @DateYesterday) AS _MedClientAcquisitionYTD,
  dbo.NavFunnel_GetMedClientAcquisition (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _MedClientAcquisitionPriorYTD,
  dbo.NavFunnel_GetMedDirProcNavInvcs (@DateYesterday, @DateYesterday) AS _MedDirProcNavInvcsDay,
  dbo.NavFunnel_GetMedDirProcNavInvcs (@DateMonthBegin, @DateYesterday) AS _MedDirProcNavInvcsMTD,
  dbo.NavFunnel_GetMedDirProcNavInvcs (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _MedDirProcNavInvcsPriorMTD,
  dbo.NavFunnel_GetMedDirProcNavInvcs (@DateYearBegin, @DateYesterday) AS _MedDirProcNavInvcsYTD,
  dbo.NavFunnel_GetMedDirProcNavInvcs (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _MedDirProcNavInvcsPriorYTD,
  dbo.NavFunnel_GetTSFLClientAcquisition (@DateYesterday, @DateYesterday) AS _TSFLClientAcquisitionDay,
  dbo.NavFunnel_GetTSFLClientAcquisition (@DateMonthBegin, @DateYesterday) AS _TSFLClientAcquisitionMTD,
  dbo.NavFunnel_GetTSFLClientAcquisition (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _TSFLClientAcquisitionPriorMTD,
  dbo.NavFunnel_GetTSFLClientAcquisition (@DateYearBegin, @DateYesterday) AS _TSFLClientAcquisitionYTD,
  dbo.NavFunnel_GetTSFLClientAcquisition (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _TSFLClientAcquisitionPriorYTD,
  dbo.NavFunnel_GetTSFLSponsoring(@DateYesterday,@DateYesterday) AS _HealthCoachRecruitDay,
  dbo.NavFunnel_GetTSFLSponsoring(@DateMonthBegin,@DateYesterday) AS _HealthCoachRecruitMTD,
  dbo.NavFunnel_GetTSFLSponsoring(@DatePriorYrMonthBegin,@DatePriorYrYesterday) AS _HealthCoachRecruitPriorMTD,
  dbo.NavFunnel_GetTSFLSponsoring(@DateYearBegin,@DateYesterday) AS _HealthCoachRecruitYTD,
  dbo.NavFunnel_GetTSFLSponsoring(@DatePriorYrYearBegin,@DatePriorYrYesterday) AS _HealthCoachRecruitPriorYTD,
  dbo.NavFunnel_GetTSFLWebCoachRqst(@DateYesterday,@DateYesterday) AS _TSFLWebCoachRqstDay,
  dbo.NavFunnel_GetTSFLWebCoachRqst(@DateMonthBegin,@DateYesterday) AS _TSFLWebCoachRqstMTD,
  dbo.NavFunnel_GetTSFLWebCoachRqst(@DatePriorYrMonthBegin,@DatePriorYrYesterday) AS _TSFLWebCoachRqstPriorMTD,
  dbo.NavFunnel_GetTSFLWebCoachRqst(@DateYearBegin,@DateYesterday) AS _TSFLWebCoachRqstYTD,
  dbo.NavFunnel_GetTSFLWebCoachRqst(@DatePriorYrYearBegin,@DatePriorYrYesterday) AS _TSFLWebCoachRqstPriorYTD,
  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DateYesterday, @DateYesterday) AS _WholesaleProcNavInvcsDay,
  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DateMonthBegin, @DateYesterday) AS _WholesaleProcNavInvcsMTD,
  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _WholesaleProcNavInvcsPriorMTD,
  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DateYearBegin, @DateYesterday) AS _WholesaleProcNavInvcsYTD,
  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _WholesaleProcNavInvcsPriorYTD,
  dbo.NavFunnel_GetActiveHealthCoaches (@DateYesterday) AS _ActiveHealthCoachesDay,
  dbo.NavFunnel_GetActiveHealthCoachesHistory (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _ActiveHealthCoachesPriorMTD,
  dbo.NavFunnel_GetActiveHealthCoachesHistory (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _ActiveHealthCoachesPriorYTD,
  dbo.NavFunnel_GetMWCCActiveStoreCount (@DateYesterday) AS _MWCCActiveStoreCount,
  dbo.GetMedDirWebVisitors (@DateYesterday) AS _MedDirWebVisitorsDay,
  dbo.NavFunnel_GetMedDirWebVisitors (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _MedDirWebVisitorsPriorMTD,
  dbo.NavFunnel_GetMedDirWebVisitors (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _MedDirWebVisitorsPriorYTD,
  dbo.GetTSFLWebVisitors (@DateYesterday) AS _TSFLWebVisitorsDay,
  dbo.NavFunnel_GetMWCCApptsCreated (@DateYesterday, @DateYesterday) AS _MWCCApptsCreatedDay,
  dbo.NavFunnel_GetMWCCApptsCreated (@DateMonthBegin, @DateYesterday) AS _MWCCApptsCreatedMTD,
  dbo.NavFunnel_GetMWCCApptsCreated (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _MWCCApptsCreatedPriorMTD,
  dbo.NavFunnel_GetMWCCApptsCreated (@DateYearBegin, @DateYesterday) AS _MWCCApptsCreatedYTD,
  dbo.NavFunnel_GetMWCCApptsCreated (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _MWCCApptsCreatedPriorYTD
END TRY
BEGIN CATCH
	IF @@ERROR =  6522
		BEGIN
			INSERT INTO [Snapshot$NAV_Funnel]
			  (RunDate,StartDateYear,StartDateMonth,StartDatePriorMonth,Yesterday,YesterdayPriorYr,StartDateMonthPriorYr,StartDateYearPriorYr,
			   TTSVisitorsDay,TTSVisitorsMTD,TTSVisitorsPriorMTD,TTSVisitorsYTD,TTSVisitorsPriorYTD,
			   TTSnewVisitsDay,TTSnewVisitsMTD,TTSnewVisitsPriorMTD,TTSnewVisitsYTD,TTSnewVisitsPriorYTD,
			   WholesaleWebVisitorsDay,WholesaleWebVisitorsPriorMTD,WholesaleWebVisitorsPriorYTD,
			   MedClientAcquisitionDay,MedClientAcquisitionMTD,MedClientAcquisitionPriorMTD,MedClientAcquisitionYTD,MedClientAcquisitionPriorYTD,
			   MedDirProcNavInvcsDay,MedDirProcNavInvcsMTD,MedDirProcNavInvcsPriorMTD,MedDirProcNavInvcsYTD,MedDirProcNavInvcsPriorYTD,
			   TSFLClientAcquisitionDay,TSFLClientAcquisitionMTD,TSFLClientAcquisitionPriorMTD,TSFLClientAcquisitionYTD,TSFLClientAcquisitionPriorYTD,
			   TSFLSponsoringDay,TSFLSponsoringMTD,TSFLSponsoringPriorMTD,TSFLSponsoringYTD,TSFLSponsoringPriorYTD,
			   TSFLWebCoachRqstDay,TSFLWebCoachRqstMTD,TSFLWebCoachRqstPriorMTD,TSFLWebCoachRqstYTD,TSFLWebCoachRqstPriorYTD,
			   WholesaleProcNavInvcsDay,WholesaleProcNavInvcsMTD,WholesaleProcNavInvcsPriorMTD,WholesaleProcNavInvcsYTD,WholesaleProcNavInvcsPriorYTD,
			   ActiveHealthCoachesDay,ActiveHealthCoachesPriorMTD,ActiveHealthCoachesPriorYTD,
			   MWCCActiveStoreCount,
			   MedDirWebVisitorsDay,MedDirWebVisitorsPriorMTD,MedDirWebVisitorsPriorYTD,
			   TSFLWebVisitorsDay,
			   MWCCApptsCreatedDay,MWCCApptsCreatedMTD,MWCCApptsCreatedPriorMTD,MWCCApptsCreatedYTD,MWCCApptsCreatedPriorYTD)
			SELECT
			  @DateEnd,
			  @DateYearBegin,
			  @DateMonthBegin,
			  @DateMonthBeginPrior,
			  @DateYesterday,
			  @DatePriorYrYesterday,
			  @DatePriorYrMonthBegin,
			  @DatePriorYrYearBegin,
			  0 AS _TTSVisitorsDay,
			  0 AS _TTSVisitorsMTD,
			  0 AS _TTSVisitorsPriorMTD,
			  0 AS _TTSVisitorsYTD,
			  0 AS _TTSVisitorsPriorYTD,
			  0 AS _TTSnewVisitsDay,
			  0 AS _TTSnewVisitsMTD,
			  0 AS _TTSnewVisitsPriorMTD,
			  0 AS _TTSnewVisitsYTD,
			  0 AS _TTSnewVisitsPriorYTD,
			  /************************ Kalpesh Patel - 1/31/2014 - Removed the WebVisitors function call ******************/
			  0 AS _WholesaleWebVisitorsDay,--dbo.GetMedWebDoctorEmails (@DateYesterday, @DateYesterday) 
			  0  AS _WholesaleWebVisitorsPriorMTD,--dbo.NavFunnel_GetMedWebDoctorEmails (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _WholesaleWebVisitorsPriorMTD,
			  0 AS _WholesaleWebVisitorsPriorYTD,--dbo.NavFunnel_GetMedWebDoctorEmails (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _WholesaleWebVisitorsPriorYTD,
			  dbo.NavFunnel_GetMedClientAcquisition (@DateYesterday, @DateYesterday) AS _MedClientAcquisitionDay,
			  dbo.NavFunnel_GetMedClientAcquisition (@DateMonthBegin, @DateYesterday) AS _MedClientAcquisitionMTD,
			  dbo.NavFunnel_GetMedClientAcquisition (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _MedClientAcquisitionPriorMTD,
			  dbo.NavFunnel_GetMedClientAcquisition (@DateYearBegin, @DateYesterday) AS _MedClientAcquisitionYTD,
			  dbo.NavFunnel_GetMedClientAcquisition (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _MedClientAcquisitionPriorYTD,
			  dbo.NavFunnel_GetMedDirProcNavInvcs (@DateYesterday, @DateYesterday) AS _MedDirProcNavInvcsDay,
			  dbo.NavFunnel_GetMedDirProcNavInvcs (@DateMonthBegin, @DateYesterday) AS _MedDirProcNavInvcsMTD,
			  dbo.NavFunnel_GetMedDirProcNavInvcs (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _MedDirProcNavInvcsPriorMTD,
			  dbo.NavFunnel_GetMedDirProcNavInvcs (@DateYearBegin, @DateYesterday) AS _MedDirProcNavInvcsYTD,
			  dbo.NavFunnel_GetMedDirProcNavInvcs (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _MedDirProcNavInvcsPriorYTD,
			  dbo.NavFunnel_GetTSFLClientAcquisition (@DateYesterday, @DateYesterday) AS _TSFLClientAcquisitionDay,
			  dbo.NavFunnel_GetTSFLClientAcquisition (@DateMonthBegin, @DateYesterday) AS _TSFLClientAcquisitionMTD,
			  dbo.NavFunnel_GetTSFLClientAcquisition (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _TSFLClientAcquisitionPriorMTD,
			  dbo.NavFunnel_GetTSFLClientAcquisition (@DateYearBegin, @DateYesterday) AS _TSFLClientAcquisitionYTD,
			  dbo.NavFunnel_GetTSFLClientAcquisition (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _TSFLClientAcquisitionPriorYTD,
			  dbo.NavFunnel_GetTSFLSponsoring(@DateYesterday,@DateYesterday) AS _HealthCoachRecruitDay,
			  dbo.NavFunnel_GetTSFLSponsoring(@DateMonthBegin,@DateYesterday) AS _HealthCoachRecruitMTD,
			  dbo.NavFunnel_GetTSFLSponsoring(@DatePriorYrMonthBegin,@DatePriorYrYesterday) AS _HealthCoachRecruitPriorMTD,
			  dbo.NavFunnel_GetTSFLSponsoring(@DateYearBegin,@DateYesterday) AS _HealthCoachRecruitYTD,
			  dbo.NavFunnel_GetTSFLSponsoring(@DatePriorYrYearBegin,@DatePriorYrYesterday) AS _HealthCoachRecruitPriorYTD,
			  dbo.NavFunnel_GetTSFLWebCoachRqst(@DateYesterday,@DateYesterday) AS _TSFLWebCoachRqstDay,
			  dbo.NavFunnel_GetTSFLWebCoachRqst(@DateMonthBegin,@DateYesterday) AS _TSFLWebCoachRqstMTD,
			  dbo.NavFunnel_GetTSFLWebCoachRqst(@DatePriorYrMonthBegin,@DatePriorYrYesterday) AS _TSFLWebCoachRqstPriorMTD,
			  dbo.NavFunnel_GetTSFLWebCoachRqst(@DateYearBegin,@DateYesterday) AS _TSFLWebCoachRqstYTD,
			  dbo.NavFunnel_GetTSFLWebCoachRqst(@DatePriorYrYearBegin,@DatePriorYrYesterday) AS _TSFLWebCoachRqstPriorYTD,
			  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DateYesterday, @DateYesterday) AS _WholesaleProcNavInvcsDay,
			  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DateMonthBegin, @DateYesterday) AS _WholesaleProcNavInvcsMTD,
			  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _WholesaleProcNavInvcsPriorMTD,
			  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DateYearBegin, @DateYesterday) AS _WholesaleProcNavInvcsYTD,
			  dbo.NavFunnel_GetWholesaleProcNavInvcs (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _WholesaleProcNavInvcsPriorYTD,
			  dbo.NavFunnel_GetActiveHealthCoaches (@DateYesterday) AS _ActiveHealthCoachesDay,
			  dbo.NavFunnel_GetActiveHealthCoachesHistory (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _ActiveHealthCoachesPriorMTD,
			  dbo.NavFunnel_GetActiveHealthCoachesHistory (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _ActiveHealthCoachesPriorYTD,
			  dbo.NavFunnel_GetMWCCActiveStoreCount (@DateYesterday) AS _MWCCActiveStoreCount,
			  dbo.GetMedDirWebVisitors (@DateYesterday) AS _MedDirWebVisitorsDay,
			  dbo.NavFunnel_GetMedDirWebVisitors (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _MedDirWebVisitorsPriorMTD,
			  dbo.NavFunnel_GetMedDirWebVisitors (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _MedDirWebVisitorsPriorYTD,
			  dbo.GetTSFLWebVisitors (@DateYesterday) AS _TSFLWebVisitorsDay,
			  dbo.NavFunnel_GetMWCCApptsCreated (@DateYesterday, @DateYesterday) AS _MWCCApptsCreatedDay,
			  dbo.NavFunnel_GetMWCCApptsCreated (@DateMonthBegin, @DateYesterday) AS _MWCCApptsCreatedMTD,
			  dbo.NavFunnel_GetMWCCApptsCreated (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _MWCCApptsCreatedPriorMTD,
			  dbo.NavFunnel_GetMWCCApptsCreated (@DateYearBegin, @DateYesterday) AS _MWCCApptsCreatedYTD,
			  dbo.NavFunnel_GetMWCCApptsCreated (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _MWCCApptsCreatedPriorYTD

		END
 END CATCH


COMMIT TRANSACTION

/****** update MWCCStoresOpenDay ***********************************************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Funnel]
SET
  MWCCStoresOpenDay = dbo.NavFunnel_GetMWCCStoresOpen (@DateYesterday)
WHERE RunDate = @DateEnd

COMMIT TRANSACTION

/****** where history is determined using records in [Snapshot$NAV_Funnel], ****/
/****** update MTD and YTD after the daily value has been added ****************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Funnel]
SET
  ActiveHealthCoachesMTD = dbo.NavFunnel_GetActiveHealthCoachesHistory (@DateMonthBegin, @DateYesterday),
  ActiveHealthCoachesYTD = dbo.NavFunnel_GetActiveHealthCoachesHistory (@DateYearBegin, @DateYesterday),

/************************ Kalpesh Patel - 1/31/2014 - Removed the WebVisitors function call ******************/
  WholesaleWebVisitorsMTD = 0,--dbo.NavFunnel_GetMedWebDoctorEmails (@DateMonthBegin, @DateYesterday),
  WholesaleWebVisitorsYTD = 0,--dbo.NavFunnel_GetMedWebDoctorEmails (@DateYearBegin, @DateYesterday),

  MedDirWebVisitorsMTD = dbo.NavFunnel_GetMedDirWebVisitors (@DateMonthBegin, @DateYesterday),
  MedDirWebVisitorsYTD = dbo.NavFunnel_GetMedDirWebVisitors (@DateYearBegin, @DateYesterday),

  MWCCStoresOpenMTD = dbo.NavFunnel_GetMWCCStoresOpenHistory (@DateMonthBegin, @DateYesterday),
  MWCCStoresOpenPriorMTD = dbo.NavFunnel_GetMWCCStoresOpenHistory (@DatePriorYrMonthBegin, @DatePriorYrYesterday),
  MWCCStoresOpenYTD = dbo.NavFunnel_GetMWCCStoresOpenHistory (@DateYearBegin, @DateYesterday),
  MWCCStoresOpenPriorYTD = dbo.NavFunnel_GetMWCCStoresOpenHistory (@DatePriorYrYearBegin, @DatePriorYrYesterday),

  TSFLWebVisitorsMTD = dbo.NavFunnel_GetTSFLWebVisitors (@DateMonthBegin, @DateYesterday),
  TSFLWebVisitorsPriorMTD = dbo.NavFunnel_GetTSFLWebVisitors (@DatePriorYrMonthBegin, @DatePriorYrYesterday),
  TSFLWebVisitorsYTD = dbo.NavFunnel_GetTSFLWebVisitors (@DateYearBegin, @DateYesterday),
  TSFLWebVisitorsPriorYTD = dbo.NavFunnel_GetTSFLWebVisitors (@DatePriorYrYearBegin, @DatePriorYrYesterday)

WHERE RunDate = @DateEnd

COMMIT TRANSACTION

/****** REPORT DURATION OF QUERY ***********************************************/

SET @ProcessTimeEnd = GetDate()
PRINT 'Elapsed time: '
PRINT DATEDIFF(SS,@ProcessTimeBegin,@ProcessTimeEnd)
