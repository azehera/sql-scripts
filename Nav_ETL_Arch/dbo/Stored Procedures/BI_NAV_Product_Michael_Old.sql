﻿


/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_Product] 
DEVELOPER:        
DATE:             
DESCRIPTION:      
PARAMETERS/VARRIABLES:
NOTES:
------------------------------ MODIFICATIONS ----------------------------------------
DEVELOPER:  Kalpesh Patel
DATE:       8/7/2012
RFC#:       629
DESCRIPTION: Section:13 Section 13:Removing CORPCLINIC data because we have to calculate from BookforTime database only. 
             Section 14:there was before 2 different ItemCategory for NON-ITEMS but Now only one
             Section 15:Including All B4T ITEMS WITH NO NAV ITEM CATEGORY CODE in one Item Category call NON-ITEMS so change in Function NavProd_B4T_GetICCNullAmtDateRange
             Section 16:Including All B4T ITEMS WITH NO NAV ITEM CATEGORY CODE in one Item Category call NON-ITEMS so change in Functions NavProd_B4T_GetICCNullQtyDateRange
             Section 17:Including All B4T ITEMS WITH NO NAV ITEM CATEGORY CODE in one Item Category call NON-ITEMS so change in Functions NavProd_B4T_GetICCNullCostDateRange
             Section 7: Removing Section 7 because we don't want seprate NON-ITEMS in 2 diffrent category 
             Section 18:Including All B4T ITEMS WITH NO NAV ITEM CATEGORY CODE in one Item Category call NON-ITEMS so change in ItemCategoryCode Update*****/
-------------------------------------------------------------------------------------------
/*******************************************************************************************/


Create PROCEDURE [dbo].[BI_NAV_Product_Michael_Old] (@DateEnd AS DATETIME)
AS
/****** MED0001 MRH 11/10/11: Business Intelligence / Executive Dashboard ******/

/****** SECTION 1:  INITIALIZATION *********************************************/

DECLARE @DateMonthBegin DATETIME
DECLARE @DateMonthEnd DATETIME
DECLARE @DateYearBegin DATETIME
DECLARE @DateYearEnd DATETIME
DECLARE @DateYesterday DATETIME
DECLARE @DatePriorYrMonthBegin DATETIME
DECLARE @DatePriorYrYearBegin DATETIME
DECLARE @DatePriorYrYesterday DATETIME
DECLARE @DaysMonth INT
DECLARE @DaysMonthElapsed INT
DECLARE @DaysMonthRemaining INT
DECLARE @DaysYear INT
DECLARE @DaysYearElapsed INT
DECLARE @DaysYearRemaining INT
DECLARE @ProcessTimeBegin DATETIME
DECLARE @ProcessTimeEnd DATETIME

SET @ProcessTimeBegin = GetDate()
SET @DateEnd = CAST(FLOOR(CAST(@DateEnd AS float)) AS DATETIME)
SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1

/****** SECTION 2:  INITIAL DATA LOAD ******************************************/

BEGIN TRANSACTION

DELETE FROM [Snapshot$NAV_Product]
WHERE (RunDate = @DateEnd)

INSERT INTO [Snapshot$NAV_Product]
  (RunDate,StartDateYear,StartDateMonth,Yesterday,YesterdayPriorYr,StartDateMonthPriorYr,StartDateYearPriorYr,
   CPG,ItemCategoryCode,
   InvSalesDay,InvCreditsDay,InvSalesMTD,InvSalesPriorYrMTD,InvSalesYTD,InvSalesPriorYrYTD,
   InvUnitsDay,InvUnitsMTD,InvUnitsYTD,
   InvOrdersDay,InvOrdersMTD,InvOrdersYTD,
   InvExtCostDay,InvExtCostMTD,InvExtCostYTD)
SELECT
  @DateEnd,
  @DateYearBegin,
  @DateMonthBegin,
  @DateYesterday,
  @DatePriorYrYesterday,
  @DatePriorYrMonthBegin,
  @DatePriorYrYearBegin,
  CPG1.Code,
  IC.Code,
  dbo.NavProd_GetAmtItemDateRange (CPG1.Code, IC.Code, @DateYesterday, @DateYesterday) AS _InvSalesDay,
  dbo.NavProd_GetAmtCrItemDateRange (CPG1.Code, IC.Code, @DateYesterday, @DateYesterday) AS _InvCreditsDay,
  dbo.NavProd_GetAmtItemDateRange (CPG1.Code, IC.Code, @DateMonthBegin, @DateYesterday) AS _InvSalesMTD,
  dbo.NavProd_GetAmtItemDateRange (CPG1.Code, IC.Code, @DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrMTD,
  dbo.NavProd_GetAmtItemDateRange (CPG1.Code, IC.Code, @DateYearBegin, @DateYesterday) AS _InvSalesYTD,
  dbo.NavProd_GetAmtItemDateRange (CPG1.Code, IC.Code, @DatePriorYrYearBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrYTD,
  dbo.NavProd_GetQtyItemDateRange (CPG1.Code, IC.Code, @DateYesterday, @DateYesterday) AS _InvUnitsDay,
  dbo.NavProd_GetQtyItemDateRange (CPG1.Code, IC.Code, @DateMonthBegin, @DateYesterday) AS _InvUnitsMTD,
  dbo.NavProd_GetQtyItemDateRange (CPG1.Code, IC.Code, @DateYearBegin, @DateYesterday) AS _InvUnitsYTD,
  dbo.NavProd_GetOrdItemDateRange (CPG1.Code, IC.Code, @DateYesterday, @DateYesterday) AS _InvOrdersDay,
  dbo.NavProd_GetOrdItemDateRange (CPG1.Code, IC.Code, @DateMonthBegin, @DateYesterday) AS _InvOrdersMTD,
  dbo.NavProd_GetOrdItemDateRange (CPG1.Code, IC.Code, @DateYearBegin, @DateYesterday) AS _InvOrdersYTD,
  dbo.NavProd_GetCostItemDateRange (CPG1.Code, IC.Code, @DateYesterday, @DateYesterday) AS _InvExtCostDay,
  dbo.NavProd_GetCostItemDateRange (CPG1.Code, IC.Code, @DateMonthBegin, @DateYesterday) AS _InvExtCostMTD,
  dbo.NavProd_GetCostItemDateRange (CPG1.Code, IC.Code, @DateYearBegin, @DateYesterday) AS _InvExtCostYTD
FROM [Jason Pharm$Customer Posting Group] CPG1
JOIN [Jason Pharm$Item Category] IC
ON IC.Code IS NOT NULL

COMMIT TRANSACTION

/****** SECTION 3:  NON-PRODUCT SALES ******************************************/

BEGIN TRANSACTION

INSERT INTO [Snapshot$NAV_Product]
  (RunDate,StartDateYear,StartDateMonth,Yesterday,YesterdayPriorYr,StartDateMonthPriorYr,StartDateYearPriorYr,
   CPG,ItemCategoryCode,
   InvSalesDay,InvCreditsDay,InvSalesMTD,InvSalesPriorYrMTD,InvSalesYTD,InvSalesPriorYrYTD,
   InvUnitsDay,InvUnitsMTD,InvUnitsYTD,
   InvOrdersDay,InvOrdersMTD,InvOrdersYTD,
   InvExtCostDay,InvExtCostMTD,InvExtCostYTD)
SELECT
  @DateEnd,
  @DateYearBegin,
  @DateMonthBegin,
  @DateYesterday,
  @DatePriorYrYesterday,
  @DatePriorYrMonthBegin,
  @DatePriorYrYearBegin,
  CPG1.Code,
  'NON-ITEM',
  dbo.NavProd_GetAmtNonItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvSalesDay,
  dbo.NavProd_GetAmtCrNonItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvCreditsDay,
  dbo.NavProd_GetAmtNonItemDateRange (CPG1.Code, @DateMonthBegin, @DateYesterday) AS _InvSalesMTD,
  dbo.NavProd_GetAmtNonItemDateRange (CPG1.Code, @DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrMTD,
  dbo.NavProd_GetAmtNonItemDateRange (CPG1.Code, @DateYearBegin, @DateYesterday) AS _InvSalesYTD,
  dbo.NavProd_GetAmtNonItemDateRange (CPG1.Code, @DatePriorYrYearBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrYTD,
  0, --dbo.NavProd_GetQtyNonItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvUnitsDay,  --commented out per Aydan
  0, --dbo.NavProd_GetQtyNonItemDateRange (CPG1.Code, @DateMonthBegin, @DateYesterday) AS _InvUnitsMTD,  --commented out per Aydan
  0, --dbo.NavProd_GetQtyNonItemDateRange (CPG1.Code, @DateYearBegin, @DateYesterday) AS _InvUnitsYTD,  --commented out per Aydan
  0, --dbo.NavProd_GetOrdNonItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvOrdersDay,  --commented out per Aydan 1/19/12
  0, --dbo.NavProd_GetOrdNonItemDateRange (CPG1.Code, @DateMonthBegin, @DateYesterday) AS _InvOrdersMTD,  --commented out per Aydan 1/19/12
  0, --dbo.NavProd_GetOrdNonItemDateRange (CPG1.Code, @DateYearBegin, @DateYesterday) AS _InvOrdersYTD,  --commented out per Aydan 1/19/12
  dbo.NavProd_GetCostNonItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvExtCostDay,
  dbo.NavProd_GetCostNonItemDateRange (CPG1.Code, @DateMonthBegin, @DateYesterday) AS _InvExtCostMTD,
  dbo.NavProd_GetCostNonItemDateRange (CPG1.Code, @DateYearBegin, @DateYesterday) AS _InvExtCostYTD
FROM [Jason Pharm$Customer Posting Group] CPG1
where CPG1.Code<>'CORPCLINIC' /***** Section 13:Removing CORPCLINIC data because we have to calculate from BookforTime database only ******/


COMMIT TRANSACTION

/****** SECTION 4:  UNCATEGORIZED PRODUCT SALES ********************************/
/****** historical sales lines lacking an Item Category Code *******************/

BEGIN TRANSACTION

INSERT INTO [Snapshot$NAV_Product]
  (RunDate,StartDateYear,StartDateMonth,Yesterday,YesterdayPriorYr,StartDateMonthPriorYr,StartDateYearPriorYr,
   CPG,ItemCategoryCode,
   InvSalesDay,InvSalesMTD,InvSalesPriorYrMTD,InvSalesYTD,InvSalesPriorYrYTD,
   InvUnitsDay,InvUnitsMTD,InvUnitsYTD,
   InvOrdersDay,InvOrdersMTD,InvOrdersYTD,
   InvExtCostDay,InvExtCostMTD,InvExtCostYTD)
SELECT
  @DateEnd,
  @DateYearBegin,
  @DateMonthBegin,
  @DateYesterday,
  @DatePriorYrYesterday,
  @DatePriorYrMonthBegin,
  @DatePriorYrYearBegin,
  CPG1.Code,
  'UNCATEGORIZED',
  dbo.NavProd_GetAmtUnCatItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvSalesDay,
  dbo.NavProd_GetAmtUnCatItemDateRange (CPG1.Code, @DateMonthBegin, @DateYesterday) AS _InvSalesMTD,
  dbo.NavProd_GetAmtUnCatItemDateRange (CPG1.Code, @DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrMTD,
  dbo.NavProd_GetAmtUnCatItemDateRange (CPG1.Code, @DateYearBegin, @DateYesterday) AS _InvSalesYTD,
  dbo.NavProd_GetAmtUnCatItemDateRange (CPG1.Code, @DatePriorYrYearBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrYTD,
  dbo.NavProd_GetQtyUnCatItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvUnitsDay,
  dbo.NavProd_GetQtyUnCatItemDateRange (CPG1.Code, @DateMonthBegin, @DateYesterday) AS _InvUnitsMTD,
  dbo.NavProd_GetQtyUnCatItemDateRange (CPG1.Code, @DateYearBegin, @DateYesterday) AS _InvUnitsYTD,
  dbo.NavProd_GetOrdUnCatItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvOrdersDay,
  dbo.NavProd_GetOrdUnCatItemDateRange (CPG1.Code, @DateMonthBegin, @DateYesterday) AS _InvOrdersMTD,
  dbo.NavProd_GetOrdUnCatItemDateRange (CPG1.Code, @DateYearBegin, @DateYesterday) AS _InvOrdersYTD,
  dbo.NavProd_GetCostUnCatItemDateRange (CPG1.Code, @DateYesterday, @DateYesterday) AS _InvExtCostDay,
  dbo.NavProd_GetCostUnCatItemDateRange (CPG1.Code, @DateMonthBegin, @DateYesterday) AS _InvExtCostMTD,
  dbo.NavProd_GetCostUnCatItemDateRange (CPG1.Code, @DateYearBegin, @DateYesterday) AS _InvExtCostYTD
FROM [Jason Pharm$Customer Posting Group] CPG1

COMMIT TRANSACTION

/****** SECTION 5:  UPDATE DATA FROM BOOK4TIME *********************************/

BEGIN TRANSACTION

DECLARE @ICC VARCHAR(20)

DECLARE CategoryCursor CURSOR FAST_FORWARD FOR
SELECT DISTINCT(Code) FROM [Jason Pharm$Item Category] WHERE (Code <> '') AND (Code <> ' ')

OPEN CategoryCursor
FETCH NEXT FROM CategoryCursor INTO @ICC

WHILE @@FETCH_STATUS = 0
BEGIN

UPDATE [Snapshot$NAV_Product]
   SET [InvSalesDay] = dbo.NavProd_B4T_GetICCAmtDateRange (@ICC, @DateYesterday, @DateYesterday)
      ,[InvUnitsDay] = dbo.NavProd_B4T_GetICCQtyDateRange (@ICC, @DateYesterday, @DateYesterday)
      ,[InvOrdersDay] = dbo.NavProd_B4T_GetICCOrdDateRange (@ICC, @DateYesterday, @DateYesterday)
      ,[InvExtCostDay] = dbo.NavProd_B4T_GetICCCostDateRange (@ICC, @DateYesterday, @DateYesterday)
      ,[InvSalesMTD] = dbo.NavProd_B4T_GetICCAmtDateRange (@ICC, @DateMonthBegin, @DateYesterday)
      ,[InvSalesPriorYrMTD] = dbo.NavProd_B4T_GetICCAmtDateRange (@ICC, @DatePriorYrMonthBegin, @DatePriorYrYesterday)
      ,[InvUnitsMTD] = dbo.NavProd_B4T_GetICCQtyDateRange (@ICC, @DateMonthBegin, @DateYesterday)
      ,[InvOrdersMTD] = dbo.NavProd_B4T_GetICCOrdDateRange (@ICC, @DateMonthBegin, @DateYesterday)
      ,[InvExtCostMTD] = dbo.NavProd_B4T_GetICCCostDateRange (@ICC, @DateMonthBegin, @DateYesterday)
      ,[InvSalesYTD] = dbo.NavProd_B4T_GetICCAmtDateRange (@ICC, @DateYearBegin, @DateYesterday)
      ,[InvSalesPriorYrYTD] = dbo.NavProd_B4T_GetICCAmtDateRange (@ICC, @DatePriorYrYearBegin, @DatePriorYrYesterday)
      ,[InvUnitsYTD] = dbo.NavProd_B4T_GetICCQtyDateRange (@ICC, @DateYearBegin, @DateYesterday)
      ,[InvOrdersYTD] = dbo.NavProd_B4T_GetICCOrdDateRange (@ICC, @DateYearBegin, @DateYesterday)
      ,[InvExtCostYTD] = dbo.NavProd_B4T_GetICCCostDateRange (@ICC, @DateYearBegin, @DateYesterday)
 WHERE ([RunDate] = @DateEnd) AND ([CPG] = 'CORPCLINIC') AND (ItemCategoryCode = @ICC)

FETCH NEXT FROM CategoryCursor INTO @ICC
END

CLOSE CategoryCursor
DEALLOCATE CategoryCursor

COMMIT TRANSACTION

/****** SECTION 6:  B4T ITEMS WITH NO NAV ITEM CATEGORY CODE *******************/

BEGIN TRANSACTION

INSERT INTO [Snapshot$NAV_Product]
  (RunDate,StartDateYear,StartDateMonth,Yesterday,YesterdayPriorYr,StartDateMonthPriorYr,StartDateYearPriorYr,
   CPG,ItemCategoryCode,
   InvSalesDay,InvUnitsDay,InvOrdersDay,InvExtCostDay,
   InvSalesMTD,InvSalesPriorYrMTD,InvUnitsMTD,InvOrdersMTD,InvExtCostMTD,
   InvSalesYTD,InvSalesPriorYrYTD,InvUnitsYTD,InvOrdersYTD,InvExtCostYTD)
SELECT
  @DateEnd,
  @DateYearBegin,
  @DateMonthBegin,
  @DateYesterday,
  @DatePriorYrYesterday,
  @DatePriorYrMonthBegin,
  @DatePriorYrYearBegin,
  'CORPCLINIC',
  'NON-ITEM',   /***** Section 14:there was before 2 different ItemCategory for NON-ITEMS but Now only one*****/
  dbo.NavProd_B4T_GetICCNullAmtDateRange (@DateYesterday, @DateYesterday) AS _InvSalesDay, /****Section 15:Including All B4T ITEMS WITH NO NAV ITEM CATEGORY CODE in one Item Category call NON-ITEMS so change in Functions*****/
  dbo.NavProd_B4T_GetICCNullQtyDateRange (@DateYesterday, @DateYesterday) AS _InvUnitsDay, /****Section 16:Including All B4T ITEMS WITH NO NAV ITEM CATEGORY CODE in one Item Category call NON-ITEMS so change in Functions*****/
  0, --dbo.NavProd_B4T_GetICCNullOrdDateRange (@DateYesterday, @DateYesterday) AS _InvOrdersDay,  --commented out per Aydan 1/19/12
  dbo.NavProd_B4T_GetICCNullCostDateRange (@DateYesterday, @DateYesterday) AS _InvExtCostDay, /****Section 17:Including All B4T ITEMS WITH NO NAV ITEM CATEGORY CODE in one Item Category call NON-ITEMS  so change in Functions*****/
  dbo.NavProd_B4T_GetICCNullAmtDateRange (@DateMonthBegin, @DateYesterday) AS _InvSalesMTD,
  dbo.NavProd_B4T_GetICCNullAmtDateRange (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrMTD,
  dbo.NavProd_B4T_GetICCNullQtyDateRange (@DateMonthBegin, @DateYesterday) AS _InvUnitsMTD,
  0, --dbo.NavProd_B4T_GetICCNullOrdDateRange (@DateMonthBegin, @DateYesterday) AS _InvOrdersMTD,  --commented out per Aydan 1/19/12
  dbo.NavProd_B4T_GetICCNullCostDateRange (@DateMonthBegin, @DateYesterday) AS _InvExtCostMTD,
  dbo.NavProd_B4T_GetICCNullAmtDateRange (@DateYearBegin, @DateYesterday) AS _InvSalesYTD,
  dbo.NavProd_B4T_GetICCNullAmtDateRange (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrYTD,
  dbo.NavProd_B4T_GetICCNullQtyDateRange (@DateYearBegin, @DateYesterday) AS _InvUnitsYTD,
  0, --dbo.NavProd_B4T_GetICCNullOrdDateRange (@DateYearBegin, @DateYesterday) AS _InvOrdersYTD  --commented out per Aydan 1/19/12
  dbo.NavProd_B4T_GetICCNullCostDateRange (@DateYearBegin, @DateYesterday) AS _InvExtCostYTD
  
COMMIT TRANSACTION

/****** SECTION 7 **************************************************************/
/****** B4T ITEMS WHERE product_type_cd in product_master table is "p" *********/
/****** these items were not found in NAV                              *********/
/****** this section of code duplicated from above, "B4T ITEMS WITH    *********/
/****** NO NAV ITEM CATEGORY CODE"                                     *********/

/******Removing Section 7 because we don't want seprate NON-ITEMS in 2 diffrent category*****/

--BEGIN TRANSACTION

--INSERT INTO [Snapshot$NAV_Product]
--  (RunDate,StartDateYear,StartDateMonth,Yesterday,YesterdayPriorYr,StartDateMonthPriorYr,StartDateYearPriorYr,
--   CPG,ItemCategoryCode,
--   InvSalesDay,InvUnitsDay,InvOrdersDay,InvExtCostDay,
--   InvSalesMTD,InvSalesPriorYrMTD,InvUnitsMTD,InvOrdersMTD,InvExtCostMTD,
--   InvSalesYTD,InvSalesPriorYrYTD,InvUnitsYTD,InvOrdersYTD,InvExtCostYTD)
--SELECT
--  @DateEnd,
--  @DateYearBegin,
--  @DateMonthBegin,
--  @DateYesterday,
--  @DatePriorYrYesterday,
--  @DatePriorYrMonthBegin,
--  @DatePriorYrYearBegin,
--  'CORPCLINIC',
--  'MISC',
--  dbo.NavProd_B4T_GetICCMiscAmtDateRange (@DateYesterday, @DateYesterday) AS _InvSalesDay,
--  dbo.NavProd_B4T_GetICCMiscQtyDateRange (@DateYesterday, @DateYesterday) AS _InvUnitsDay,
--  0, --dbo.NavProd_B4T_GetICCMiscOrdDateRange (@DateYesterday, @DateYesterday) AS _InvOrdersDay,
--  dbo.NavProd_B4T_GetICCMiscCostDateRange (@DateYesterday, @DateYesterday) AS _InvExtCostDay,
--  dbo.NavProd_B4T_GetICCMiscAmtDateRange (@DateMonthBegin, @DateYesterday) AS _InvSalesMTD,
--  dbo.NavProd_B4T_GetICCMiscAmtDateRange (@DatePriorYrMonthBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrMTD,
--  dbo.NavProd_B4T_GetICCMiscQtyDateRange (@DateMonthBegin, @DateYesterday) AS _InvUnitsMTD,
--  0, --dbo.NavProd_B4T_GetICCMiscOrdDateRange (@DateMonthBegin, @DateYesterday) AS _InvOrdersMTD,
--  dbo.NavProd_B4T_GetICCMiscCostDateRange (@DateMonthBegin, @DateYesterday) AS _InvExtCostMTD,
--  dbo.NavProd_B4T_GetICCMiscAmtDateRange (@DateYearBegin, @DateYesterday) AS _InvSalesYTD,
--  dbo.NavProd_B4T_GetICCMiscAmtDateRange (@DatePriorYrYearBegin, @DatePriorYrYesterday) AS _InvSalesPriorYrYTD,
--  dbo.NavProd_B4T_GetICCMiscQtyDateRange (@DateYearBegin, @DateYesterday) AS _InvUnitsYTD,
--  0, --dbo.NavProd_B4T_GetICCMiscOrdDateRange (@DateYearBegin, @DateYesterday) AS _InvOrdersYTD
--  dbo.NavProd_B4T_GetICCMiscCostDateRange (@DateYearBegin, @DateYesterday) AS _InvExtCostYTD
  
--COMMIT TRANSACTION

/****** SECTION 8:  UPDATE DATA FROM BOOK4TIME HISTORICAL TABLES ***************/

BEGIN TRANSACTION

DECLARE CategoryCursor CURSOR FAST_FORWARD FOR
SELECT DISTINCT(Code) FROM [Jason Pharm$Item Category] WHERE (Code <> '') AND (Code <> ' ')

OPEN CategoryCursor
FETCH NEXT FROM CategoryCursor INTO @ICC

WHILE @@FETCH_STATUS = 0
BEGIN

UPDATE [Snapshot$NAV_Product]
   SET [InvSalesDay] = ISNULL([InvSalesDay],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange (@ICC, @DateYesterday, @DateYesterday)
      ,[InvSalesMTD] = ISNULL([InvSalesMTD],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange (@ICC, @DateMonthBegin, @DateYesterday)
      ,[InvSalesPriorYrMTD] = ISNULL([InvSalesPriorYrMTD],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange (@ICC, @DatePriorYrMonthBegin, @DatePriorYrYesterday)
      ,[InvSalesYTD] = ISNULL([InvSalesYTD],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange (@ICC, @DateYearBegin, @DateYesterday)
      ,[InvSalesPriorYrYTD] = ISNULL([InvSalesPriorYrYTD],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange (@ICC, @DatePriorYrYearBegin, @DatePriorYrYesterday)
 WHERE ([RunDate] = @DateEnd) AND ([CPG] = 'CORPCLINIC') AND (ItemCategoryCode = @ICC)

FETCH NEXT FROM CategoryCursor INTO @ICC
END

CLOSE CategoryCursor
DEALLOCATE CategoryCursor

COMMIT TRANSACTION

/****** SECTION 9:  UPDATE DATA FROM BOOK4TIME HISTORICAL **********************/
/****** TABLES FOR ITEMS WITH NO NAV ITEM CATEGORY CODE ************************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Product]
   SET [InvSalesDay] = ISNULL([InvSalesDay],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange ('NPD', @DateYesterday, @DateYesterday)
      ,[InvSalesMTD] = ISNULL([InvSalesMTD],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange ('NPD', @DateMonthBegin, @DateYesterday)
      ,[InvSalesPriorYrMTD] = ISNULL([InvSalesPriorYrMTD],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange ('NPD', @DatePriorYrMonthBegin, @DatePriorYrYesterday)
      ,[InvSalesYTD] = ISNULL([InvSalesYTD],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange ('NPD', @DateYearBegin, @DateYesterday)
      ,[InvSalesPriorYrYTD] = ISNULL([InvSalesPriorYrYTD],0) + dbo.NavProd_B4T_HIST_GetICCAmtDateRange ('NPD', @DatePriorYrYearBegin, @DatePriorYrYesterday)
 WHERE ([RunDate] = @DateEnd) AND ([CPG] = 'CORPCLINIC') AND (ItemCategoryCode = 'NON-ITEM') /****Section 18:Including All B4T ITEMS WITH NO NAV ITEM CATEGORY CODE in one Item Category call NON-ITEMS so change in ItemCategoryCode Update*****/

COMMIT TRANSACTION

/****** SECTION 10:  UPDATE PROJECTIONS ****************************************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Product]
SET InvSalesMoProj = (InvSalesMTD / NULLIF(@DaysMonthElapsed,0)) * @DaysMonth
WHERE (RunDate = @DateEnd) AND (InvSalesMTD <> 0)

UPDATE [Snapshot$NAV_Product]
SET InvSalesProjYE = (InvSalesYTD / NULLIF(@DaysYearElapsed,0)) * @DaysYear
WHERE (RunDate = @DateEnd) AND (InvSalesYTD <> 0)

COMMIT TRANSACTION

/****** SECTION 11:  HARDCODED SORT; ORDER OF DIVISION PER DOMINIC *************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Product]
SET SortOrder = 1000
WHERE (CPG = 'TSFL') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product]
SET SortOrder = 900
WHERE (CPG = 'MEDIFAST') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product]
SET SortOrder = 800
WHERE (CPG = 'CORPCLINIC') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product]
SET SortOrder = 700
WHERE (CPG = 'FRANCHISE') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product]
SET SortOrder = 600
WHERE (CPG = 'DOCTORS') AND (SortOrder = 0)

UPDATE [Snapshot$NAV_Product]
SET SortOrder = -100
WHERE (CPG = 'CORPHEALTH') AND (SortOrder = 0)

COMMIT TRANSACTION

/****** SECTION 12:  REPORT DURATION OF QUERY **********************************/

SET @ProcessTimeEnd = GetDate()
PRINT 'Elapsed time: '
PRINT DATEDIFF(SS,@ProcessTimeBegin,@ProcessTimeEnd)




