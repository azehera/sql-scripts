﻿

/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_DimItemCategoryCode] 
DEVELOPER:  Kalpesh Patel      
DATE:  04/09/2012           
DESCRIPTION: Loading Dimesion data for cube     
PARAMETERS/VARRIABLES:
NOTES:

*******************************************************************************************/


CREATE PROCEDURE [dbo].[BI_NAV_DimItemCategoryCode] 
as

truncate table BI_SSAS_Cubes.dbo.[DimJason Pharm$Item Category]
/******DimItemcode from Nav_ETL *******/

insert into  BI_SSAS_Cubes.dbo.[DimJason Pharm$Item Category]
Select 
      Distinct ItemCategoryCode from BI_SSAS_Cubes.dbo.FactSales
      



