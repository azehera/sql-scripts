﻿




CREATE PROCEDURE [dbo].[BI_NAV_FactGL_CubeBudget]
AS
    SET NOCOUNT ON

    DECLARE @Delete DATETIME


    SET @Delete = ( SELECT  [CalendarMonthIDGLCube]
                    FROM    BI_SSAS_Cubes.dbo.BudgetData_CalendarYear
                  )



    SELECT  1 AS EntryNO ,
            CalendarMonthID ,
            b.[G_L Account No_] COLLATE DATABASE_DEFAULT AS [G_L Account No_] ,
            NULL AS DocumentID ,
            NULL AS [Document No_] ,
            [Global Dimension 1 Code] COLLATE DATABASE_DEFAULT AS [Global Dimension 1 Code] ,
            CASE WHEN [Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'FRANCHISE'
                 THEN 'JASON PHARM FRANCHISE'
                 WHEN [Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'TSFL'
                 THEN 'TSFL'
                 WHEN [Global Dimension 2 Code] COLLATE DATABASE_DEFAULT = 'SALES'
                 THEN 'WHOLESALE'
                 ELSE [Global Dimension 2 Code]
            END AS [Global Dimension 2 Code] ,
            NULL AS [Transaction No_] ,
            NULL AS [NetTotal] ,
            SUM(b.Amount) * -1 AS Budget$ ,
            CASE WHEN b.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN b.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.[dbo].[Jason Pharm$G_L Budget Entry] b
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              b.[Date])) = c.CalendarDate
    WHERE   CalendarMonthID >= @Delete
--and  b.[G_L Account No_] not in ('96750','85401')
            AND ( [Budget Name] = 'V2-12BUDGT'
                  OR ( [Budget Name] = 'BUDGET'
                       AND YEAR(Date) > '2012'
                     )
                )
    GROUP BY CalendarMonthID ,
            b.[G_L Account No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 2 Code]
    UNION ALL
    SELECT  2 AS EntryNO ,
            CalendarMonthID ,
            b.[G_L Account No_] ,
            NULL AS DocumentID ,
            NULL AS [Document No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 1 Code] AS [Global Dimension 2 Code] ,
            NULL AS [Transaction No_] ,
            NULL AS [NetTotal] ,
            SUM(b.Amount) * -1 AS Budget$ ,
            CASE WHEN b.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN b.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.dbo.[7 Crondall$G_L Budget Entry] b
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              b.[Date])) = c.CalendarDate
    WHERE   CalendarMonthID >= @Delete
--and  b.[G_L Account No_] not in ('96750','43500')
            AND ( [Budget Name] = 'V2-12BUDGT'
                  OR ( [Budget Name] = 'BUDGET'
                       AND YEAR(Date) > '2012'
                     )
                )
    GROUP BY CalendarMonthID ,
            b.[G_L Account No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 2 Code]
    UNION ALL
    SELECT  3 AS EntryNO ,
            CalendarMonthID ,
            b.[G_L Account No_] ,
            NULL AS DocumentID ,
            NULL AS [Document No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 1 Code] AS [Global Dimension 2 Code] ,
            NULL AS [Transaction No_] ,
            NULL AS [NetTotal] ,
            SUM(b.Amount) * -1 AS Budget$ ,
            CASE WHEN b.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN b.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.dbo.[Jason Enterprises$G_L Budget Entry] b
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              b.[Date])) = c.CalendarDate
    WHERE   CalendarMonthID >= @Delete
            AND ( [Budget Name] = 'V2-12BUDGT'
                  OR ( [Budget Name] = 'BUDGET'
                       AND YEAR(Date) > '2012'
                     )
                )
    GROUP BY CalendarMonthID ,
            b.[G_L Account No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 2 Code]
    UNION ALL
    SELECT  4 AS EntryNO ,
            CalendarMonthID ,
            b.[G_L Account No_] ,
            NULL AS DocumentID ,
            NULL AS [Document No_] ,
            [Global Dimension 1 Code] ,
            CASE WHEN [Global Dimension 1 Code] COLLATE DATABASE_DEFAULT = 'FRANCHIS'
                 THEN 'FRANCHISE'
                 ELSE 'MWCC'
            END AS [Global Dimension 2 Code] ,
            NULL AS [Transaction No_] ,
            NULL AS [NetTotal] ,
            SUM(b.Amount) * -1 AS Budget$ ,
            CASE WHEN b.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN b.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.dbo.[Jason Properties$G_L Budget Entry] b
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              b.[Date])) = c.CalendarDate
    WHERE   CalendarMonthID >= @Delete
--and [G_L Account No_] not in ('96750')
            AND ( [Budget Name] = 'V2-12BUDGT'
                  OR ( [Budget Name] = 'BUDGET'
                       AND YEAR(Date) > '2012'
                     )
                )
    GROUP BY CalendarMonthID ,
            b.[G_L Account No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 2 Code]
    UNION ALL
    SELECT  5 AS EntryNO ,
            CalendarMonthID ,
            b.[G_L Account No_] ,
            NULL AS DocumentID ,
            NULL AS [Document No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 1 Code] AS [Global Dimension 2 Code] ,
            NULL AS [Transaction No_] ,
            NULL AS [NetTotal] ,
            SUM(b.Amount) * -1 AS Budget$ ,
            CASE WHEN b.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN b.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.dbo.[Medifast Inc$G_L Budget Entry] b
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              b.[Date])) = c.CalendarDate
    WHERE   CalendarMonthID >= @Delete
--and [G_L Account No_] not in ('96750')
            AND ( [Budget Name] = 'V2-12BUDGT'
                  OR ( [Budget Name] = 'BUDGET'
                       AND YEAR(Date) > '2012'
                     )
                )
    GROUP BY CalendarMonthID ,
            b.[G_L Account No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 2 Code]
    UNION ALL
    SELECT  6 AS EntryNO ,
            CalendarMonthID ,
            b.[G_L Account No_] ,
            NULL AS DocumentID ,
            NULL AS [Document No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 1 Code]COLLATE DATABASE_DEFAULT AS [Global Dimension 2 Code] ,
            NULL AS [Transaction No_] ,
            NULL AS [NetTotal] ,
            SUM(b.Amount) * -1 AS Budget$ ,
            CASE WHEN b.[G_L Account No_] BETWEEN '10000' AND '39999' THEN 1
                 WHEN b.[G_L Account No_] BETWEEN '40000' AND '99999' THEN 2
                 ELSE 3
            END AS AccountsCategory
    FROM    NAV_ETL.dbo.[Take Shape For Life$G_L Budget Entry] b
            LEFT JOIN [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0,
                                                              DATEDIFF(D, 0,
                                                              b.[Date])) = c.CalendarDate
    WHERE   CalendarMonthID >= @Delete
            AND ( [Budget Name] = 'V2-12BUDGT'
                  OR ( [Budget Name] = 'BUDGET'
                       AND YEAR(Date) > '2012'
                     )
                )
    GROUP BY CalendarMonthID ,
            b.[G_L Account No_] ,
            [Global Dimension 1 Code] ,
            [Global Dimension 2 Code]























