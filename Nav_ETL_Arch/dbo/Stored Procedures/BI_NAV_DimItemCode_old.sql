﻿









/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_DimItemCode] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Dimesion data for cube     
PARAMETERS/VARRIABLES:
NOTES:

*******************************************************************************************/


CREATE PROCEDURE [dbo].[BI_NAV_DimItemCode_old] 
as

truncate table BI_SSAS_Cubes.dbo.DimItemcode
/******DimItemcode from Nav_ETL *******/

insert into  BI_SSAS_Cubes.dbo.DimItemcode
Select 
      No_ as DimItemcode, 
      Description ,[Product Group Code],[Item Category Code]
      from Nav_ETL.dbo.[Jason Pharm$Item]
      
 Union all
 
 Select ' ' as Itemcode,
        'Unknown' as Description , ' ' as [Product Group Code],' ' as [Item Category Code]


 
  
     
/******DimItemcode from BookforTime which is not in Nav_ETL ********/
insert into  BI_SSAS_Cubes.dbo.DimItemcode
Select 
     distinct sku as Item_code,
     product_name as Description,' ' as [Product Group Code],' ' as [Item Category Code]
      from Book4Time_ETL.dbo.B4T_product_master 
     where sku not in 
                 (Select No_ COLLATE DATABASE_DEFAULT 
                          from Nav_ETL.dbo.[Jason Pharm$Item])
                          
 Update BI_SSAS_Cubes.dbo.DimItemcode
 set Itemcode='VEW1'
 where Itemcode='VEW '


            
/******DimItemcode from resources table ******/
insert into BI_SSAS_Cubes.dbo.DimItemcode
Select [No_] as DimItemcode,
        [Name] as description ,' ' as [Product Group Code],' ' as [Item Category Code]
        from Nav_ETL.dbo.[Jason Pharm$Resource]
        
        
/*****DimItemcode from G/L accound*******/
insert into BI_SSAS_Cubes.dbo.DimItemcode
Select 
      No_ as DimItemcode, 
      Name as Description , ' ' as [Product Group Code] ,' ' as [Item Category Code]
       from Nav_ETL.dbo.[Jason Pharm$G_L Account]
       where No_ COLLATE DATABASE_DEFAULT  not in(Select Itemcode from  BI_SSAS_Cubes.dbo.DimItemcode)
       
 /*****Missing Itemcode in ItemTable fro Debit*****/
 
 insert into BI_SSAS_Cubes.dbo.DimItemcode
 SELECT 
      distinct [No_]
     ,a.[Description],b.[Product Group Code],b.[Item Category Code]
 FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Line] a
   left join BI_SSAS_Cubes.dbo.DimItemcode b
   on a.No_=b.ItemCode
  where Itemcode is null
  
/*****Missing Itemcode in ItemTable fro Credit*****/
insert into BI_SSAS_Cubes.dbo.DimItemcode
SELECT 
      distinct [No_]
     ,a.[Description],b.[Product Group Code],b.[Item Category Code]
 FROM [NAV_ETL].dbo.[Jason Pharm$Sales Cr_Memo Line] a
   left join BI_SSAS_Cubes.dbo.DimItemcode b
   on a.No_=b.ItemCode
  where Itemcode is null
  
/****Delete Itemcode with different Name******/
;with cte as (SELECT  [Itemcode]
      ,[Description],[Product Group Code]
      ,ROW_NUMBER() over (Partition by ItemCode order by ItemCode) as no
  FROM [BI_SSAS_Cubes].[dbo].[DimItemCode])
  Delete from cte
  where no>1
  
  
/****Delete Itemcode with ASF ******/

Delete from BI_SSAS_Cubes.dbo.DimItemcode
where ItemCode='ASF'

update [BI_SSAS_Cubes].[dbo].[DimItemCode]
  set [Product Group Code]=' '
  where [Product Group Code] is null
  
  update [BI_SSAS_Cubes].[dbo].[DimItemCode]
  set [Item Category Code]=' '
  where [Item Category Code] is null
  
   update [BI_SSAS_Cubes].[dbo].[DimItemCode]
  set [ItemCode]=' '
  where [ItemCode] is null
























