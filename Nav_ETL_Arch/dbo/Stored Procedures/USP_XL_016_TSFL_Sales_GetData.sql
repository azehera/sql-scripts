﻿

--/*
--===============================================================================
--Author:         Emily Trainor
--Create date: 11/1/2012
---------------------------TSFL Tracker TSFL Sales-----------------------------
-------Pull data for TSFL Sales Business Week Graph----------------------------
--==============================================================================    
--REFERENCES
--Database              Table/View/UDF                             Action            
---------------------------------------------------------------------------------
--BI_Reporting			XL_016_TSFL_LY_TY				               Select                  
--===============================================================================
--REVISION LOG
--Date            Name                          Change
---------------------------------------------------------------------------------
--06/22/2012	  Emily Trainor					Updated to pull Business Week
--===============================================================================
--NOTES:
---------------------------------------------------------------------------------
--===============================================================================
--*/

Create procedure [dbo].[USP_XL_016_TSFL_Sales_GetData] as 
set nocount on ;

-----------------------------Declare Date--------------------
DECLARE @DATE datetime
SET @DATE = '01/01/' + Convert(varchar(4), DatePart(yyyy, DateAdd(yyyy, -1, GETDATE())))

---------------------Pulling TSFL Sales Data ------------------------------------
SELECT 
 [Calendar Week]
,[Calendar Year]
,sum([Dollars]) as Dollars
FROM [BI_Reporting].dbo.XL_016_TSFL_LY_TY
where [Posting Date] >= @DATE
and [Customer Posting Group] ='TSFL'
Group By 
 [Calendar Year]
,[Calendar Week]
Order by 
 [Calendar Year]
,[Calendar Week]