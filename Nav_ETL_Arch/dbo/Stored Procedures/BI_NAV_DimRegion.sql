﻿




/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_DimRegion]  
DEVELOPER:  Kalpesh Patel      
DATE:  10/17/2012           
DESCRIPTION: Loading BI_NAV_DimRegion data for cube     
PARAMETERS/VARRIABLES:
NOTES:

Note for Oracle ERP :

1 When move to production remove book4time commented section.
2.Create Views for MedNutrtion Tables

*******************************************************************************************/


CREATE PROCEDURE [dbo].[BI_NAV_DimRegion] 
as



Truncate table BI_SSAS_Cubes.dbo.DimRegion

 
insert into BI_SSAS_Cubes.dbo.DimRegion

SELECT 
      distinct location_code
     ,[region_name]
           FROM [Book4Time_ETL].[dbo].[B4T_region] a
                 join [Book4Time_ETL].dbo.B4T_region_locations b
                       on a.region_id=b.region_id
                 join [Book4Time_ETL].dbo.B4T_location c
                       on c.location_id=b.location_id
                       
delete FROM [BI_SSAS_Cubes].[dbo].[DimRegion]
  where Region='Other' and RegionID='9679'
 

insert into BI_SSAS_Cubes.dbo.DimRegion

Select 
      distinct left([Sell-to Post Code],4)
      ,case when region is null or Region=' '
            then 'Other' else Region end as Region 
            from [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Header] 
                 left join Zip_codes_deluxe.dbo.ZipCodeDatabase_DELUXE b
                           on left([Sell-to Post Code], 4) COLLATE DATABASE_DEFAULT =left(b.ZipCode, 4) COLLATE DATABASE_DEFAULT 
  
delete FROM [BI_SSAS_Cubes].[dbo].[DimRegion]
  where Region='Other' and RegionID='9679'
                
insert into BI_SSAS_Cubes.dbo.DimRegion              
                
Select 
     distinct left([Sell-to Post Code],4)
     ,case when region is null or Region=' ' 
           then 'Other' else Region end as Region 
           from  [NAV_ETL].dbo.[Jason Pharm$Sales Cr_Memo Header]
                 left join Zip_codes_deluxe.dbo.ZipCodeDatabase_DELUXE b
                           on left([Sell-to Post Code], 4) COLLATE DATABASE_DEFAULT =left(b.ZipCode, 4) COLLATE DATABASE_DEFAULT 
                           
 delete FROM [BI_SSAS_Cubes].[dbo].[DimRegion]
  where Region='Other' and RegionID='9679'
   

 insert into BI_SSAS_Cubes.dbo.DimRegion (RegionID,Region)

Select 
      distinct left([Sell-to Post Code],7)
      ,'CANADA' as Region 
            from NAV_ETL.dbo.[Medifast Nutrition$Sales Invoice Header]              
                
insert into BI_SSAS_Cubes.dbo.DimRegion (RegionID,Region)
Select 
      distinct left([Sell-to Post Code],7)
      ,'CANADA' as Region 
            from NAV_ETL.dbo.[Medifast Nutrition$Sales Cr_Memo Header]              
   
                 
                
 ;with CTE as 
         (select 
                 RegionID
                 ,Region
                 ,ROW_NUMBER() over (partition by RegionID Order bY RegionID)as NO 
                 from BI_SSAS_Cubes.dbo.DimRegion)
          Delete From CTE where NO >1
                
Delete from BI_SSAS_Cubes.dbo.DimRegion
where RegionID is null               

