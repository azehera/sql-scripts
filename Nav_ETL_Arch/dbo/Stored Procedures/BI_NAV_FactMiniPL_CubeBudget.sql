﻿





















CREATE Procedure [dbo].[BI_NAV_FactMiniPL_CubeBudget]
as

SET NOCOUNT ON
Declare @Load int
declare @Delete int

Set @Delete=(Select [CalendarMonthIDGLCube] from BI_SSAS_Cubes.dbo.BudgetData_CalendarYear )
Set @Load = (Select CalendarMonthIDminiPL From BI_SSAS_Cubes.dbo.BudgetData_CalendarYear)



Select
1 AS EntryNO
,CalendarMonthID
,b.[G_L Account No_]        
,null as DocumentID
,null as [Document No_]
,[Global Dimension 1 Code]
,Case when [Global Dimension 2 Code] COLLATE DATABASE_DEFAULT ='FRANCHISE' then 'JASON PHARM FRANCHISE'
when [Global Dimension 2 Code] COLLATE DATABASE_DEFAULT ='TSFL' then 'TSFL'
when [Global Dimension 2 Code] COLLATE DATABASE_DEFAULT ='SALES' then 'WHOLESALE' else [Global Dimension 2 Code] end as [Global Dimension 2 Code]
,null as [Transaction No_]
,null as [NetTotal]
,case when CalendarMonthID>=@Load then 0 else (Sum(b.[Amount])*-1) end  As Budget$
 From 
    [dbo].[Jason Pharm$G_L Budget Entry] b
   left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,b.[Date]))=c.CalendarDate
Where CalendarMonthID>=@Delete
and  b.[G_L Account No_] not in ('96750','85401')
and  ([Budget Name] ='V2-12BUDGT' or ([Budget Name] ='BUDGET' and YEAR(Date)>'2012'))
group by CalendarMonthID
,b.[G_L Account No_],[Global Dimension 1 Code]
,[Global Dimension 2 Code] 


    

union all 

Select
2 AS EntryNO
,CalendarMonthID
,b.[G_L Account No_]        
,null as DocumentID
,null as [Document No_]
,[Global Dimension 1 Code]
,[Global Dimension 2 Code] 
,null as [Transaction No_]
,null as [NetTotal]
,case when CalendarMonthID>=@Load then 0 else (Sum(b.[Amount])*-1) end  As Budget$
 From 
    NAV_ETL.dbo.[7 Crondall$G_L Budget Entry] b
   left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,b.[Date]))=c.CalendarDate
Where CalendarMonthID>=@Delete  
and  b.[G_L Account No_] not in ('96750','43500')
and  ([Budget Name] ='V2-12BUDGT'or ([Budget Name] ='BUDGET' and YEAR(Date)>'2012'))
group by CalendarMonthID
,b.[G_L Account No_],[Global Dimension 1 Code]
,[Global Dimension 2 Code]


union all 

Select
3 AS EntryNO
,CalendarMonthID
,b.[G_L Account No_]        
,null as DocumentID
,null as [Document No_]
,[Global Dimension 1 Code]
,[Global Dimension 2 Code] 
,null as [Transaction No_]
,null as [NetTotal]
,case when CalendarMonthID>=@Load then 0 else (Sum(b.[Amount])*-1) end  As Budget$
 From 
    NAV_ETL.dbo.[Jason Enterprises$G_L Budget Entry] b
   left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,b.[Date]))=c.CalendarDate
Where CalendarMonthID>=@Delete
and  ([Budget Name] ='V2-12BUDGT'or ([Budget Name] ='BUDGET' and YEAR(Date)>'2012'))
group by CalendarMonthID
,b.[G_L Account No_],[Global Dimension 1 Code]
,[Global Dimension 2 Code]


union all 

Select
4 AS EntryNO
,CalendarMonthID
,b.[G_L Account No_]        
,null as DocumentID
,null as [Document No_]
,[Global Dimension 1 Code]
,case when [Global Dimension 1 Code] COLLATE DATABASE_DEFAULT='FRANCHIS' then 'FRANCHISE'
else 'MWCC' end as [Global Dimension 2 Code] 
,null as [Transaction No_]
,null as [NetTotal]
,case when CalendarMonthID>=@Load then 0 else (Sum(b.[Amount])*-1) end  As Budget$
 From 
    NAV_ETL.dbo.[Jason Properties$G_L Budget Entry] b
   left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,b.[Date]))=c.CalendarDate
Where CalendarMonthID>=@Delete 
and [G_L Account No_] not in ('96750')
and  ([Budget Name] ='V2-12BUDGT'or ([Budget Name] ='BUDGET' and YEAR(Date)>'2012'))
group by CalendarMonthID
,b.[G_L Account No_],[Global Dimension 1 Code]
,[Global Dimension 2 Code]


union all 

Select
5 AS EntryNO
,CalendarMonthID
,b.[G_L Account No_] COLLATE DATABASE_DEFAULT       
,null as DocumentID
,null as [Document No_]
,[Global Dimension 1 Code] COLLATE DATABASE_DEFAULT
,[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT
,null as [Transaction No_]
,null as [NetTotal]
,case when CalendarMonthID>=@Load then 0 else (Sum(b.[Amount])*-1) end  As Budget$
 From 
    NAV_ETL.dbo.[Medifast Inc$G_L Budget Entry] b
   left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,b.[Date]))=c.CalendarDate
Where CalendarMonthID>=@Delete 
and [G_L Account No_] not in ('96750')
and  ([Budget Name] ='V2-12BUDGT'or ([Budget Name] ='BUDGET' and YEAR(Date)>'2012'))
group by CalendarMonthID
,b.[G_L Account No_],[Global Dimension 1 Code]
,[Global Dimension 2 Code]

union all 

Select
6 AS EntryNO
,CalendarMonthID
,b.[G_L Account No_]        
,null as DocumentID
,null as [Document No_]
,'TSFL' COLLATE DATABASE_DEFAULT as [Global Dimension 1 Code]
,'TSFL' COLLATE DATABASE_DEFAULT as [Global Dimension 2 Code] 
, null as [Transaction No_]
,null as [NetTotal]
,case when CalendarMonthID>=@Load then 0 else (Sum(b.[Amount])*-1) end  As Budget$
 From 
   NAV_ETL.dbo.[Take Shape For Life$G_L Budget Entry] b
   left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,b.[Date]))=c.CalendarDate
Where CalendarMonthID>=@Delete 
and  ([Budget Name] ='V2-12BUDGT'or ([Budget Name] ='BUDGET' and YEAR(Date)>'2012'))
group by CalendarMonthID
,b.[G_L Account No_],[Global Dimension 1 Code]
,[Global Dimension 2 Code]



















