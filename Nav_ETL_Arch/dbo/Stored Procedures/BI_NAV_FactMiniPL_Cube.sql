﻿








CREATE Procedure [dbo].[BI_NAV_FactMiniPL_Cube]
as
SET NOCOUNT ON
Declare @Load int
declare @Delete int
---@LyDate Datetime, @TyDate DateTime
Set @Delete=(Select [CalendarMonthIDGLCube] from BI_SSAS_Cubes.dbo.BudgetData_CalendarYear )
Set @Load = (Select CalendarMonthIDminiPL From BI_SSAS_Cubes.dbo.BudgetData_CalendarYear)

delete from  BI_SSAS_Cubes.dbo.FactMiniPL_cube
where CalendarMonthID>=@delete

Select
1 AS EntryNO
,CalendarMonthID
,a.[G_L Account No_] COLLATE DATABASE_DEFAULT as [G_L Account No_]  
,a.[Document No_] COLLATE DATABASE_DEFAULT+''+a.[G_L Account No_] COLLATE DATABASE_DEFAULT as DocumentID
,a.[Document No_] COLLATE DATABASE_DEFAULT as [Document No_]
,a.[Global Dimension 1 Code] COLLATE DATABASE_DEFAULT as [Global Dimension 1 Code]
,Case when a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT ='FRANCHISE' then 'JASON PHARM FRANCHISE'
when a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT ='TSFL' then 'TSFL'
when a.[Global Dimension 2 Code] COLLATE DATABASE_DEFAULT='SALES' then 'WHOLESALE' else a.[Global Dimension 2 Code] end as [Global Dimension 2 Code]
,a.[Transaction No_]
,case when CalendarMonthID>=@Load then 0 else (Sum(a.[Amount])*-1) end  As NetTotal
,null as Budget$
 From  NAV_ETL.[dbo].[Jason Pharm$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate
Where CalendarMonthID>=@Delete
and a.[G_L Account No_] >='40000'
and  [G_L Account No_] not in ('96750','85401')
Group By
CalendarMonthID 
,a.[G_L Account No_] 
,a.[Document No_]
,a.[Global Dimension 1 Code]
,a.[Global Dimension 2 Code]
,a.[Transaction No_]


union all 
select 
2 AS EntryNO
,CalendarMonthID
,a.[G_L Account No_]  COLLATE DATABASE_DEFAULT     
,a.[Document No_] COLLATE DATABASE_DEFAULT+''+a.[G_L Account No_] COLLATE DATABASE_DEFAULT  as DocumentID
,a.[Document No_] COLLATE DATABASE_DEFAULT
,a.[Global Dimension 1 Code] COLLATE DATABASE_DEFAULT
,a.[Global Dimension 2 Code]  COLLATE DATABASE_DEFAULT
,a.[Transaction No_]
,case when CalendarMonthID>=@Load then 0 else (Sum(a.[Amount])*-1) end  As NetTotal
,null as Budget$
From NAV_ETL.[dbo].[7 Crondall$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate 
where  a.[G_L Account No_] >='40000'
and [G_L Account No_] not in ('96750','43500')
and CalendarMonthID>=@Delete
Group By
CalendarMonthID
,a.[G_L Account No_]
,a.[Document No_]
,a.[Description]
,a.[Global Dimension 1 Code]
,a.[Global Dimension 2 Code]
,a.[Transaction No_]


union all

select 
3 AS EntryNO
,CalendarMonthID
,a.[G_L Account No_]       
,a.[Document No_]+''+a.[G_L Account No_]  as DocumentID
,a.[Document No_]
,a.[Global Dimension 1 Code]
,a.[Global Dimension 2 Code] 
,a.[Transaction No_]
,case when CalendarMonthID>=@Load then 0 else (Sum(a.[Amount])*-1) end  As NetTotal
,null as Budget$
From NAV_ETL.[dbo].[Jason Enterprises$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate 
where  a.[G_L Account No_] >='40000'
and CalendarMonthID>=@Delete
Group By
CalendarMonthID
,a.[G_L Account No_]
,a.[Document No_]
,a.[Global Dimension 1 Code]
,a.[Global Dimension 2 Code]
,a.[Transaction No_]


union all
select 
4 AS EntryNO
,CalendarMonthID
,a.[G_L Account No_]      
,a.[Document No_]+''+a.[G_L Account No_]  as DocumentID
,a.[Document No_]
,a.[Global Dimension 1 Code]
,case when a.[Global Dimension 1 Code]='FRANCHIS' then 'FRANCHISE'
else 'MWCC' end as [Global Dimension 2 Code] 
,a.[Transaction No_]
,case when CalendarMonthID>=@Load then 0 else (Sum(a.[Amount])*-1) end  As NetTotal 
,null as Budget$
From  NAV_ETL.[dbo].[Jason Properties$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate 
where  a.[G_L Account No_] >='40000'
and [G_L Account No_] not in ('96750')
and CalendarMonthID>=@Delete
Group By
CalendarMonthID
,a.[G_L Account No_]
,a.[Document No_]
,a.[Global Dimension 1 Code]
,case when a.[Global Dimension 1 Code]='FRANCHIS' then 'FRANCHISE'
else 'MWCC' end 
,a.[Transaction No_]

union all

select 
5 AS EntryNO
,CalendarMonthID
,a.[G_L Account No_]       
,a.[Document No_]+''+a.[G_L Account No_]  as DocumentID
,a.[Document No_]
,a.[Global Dimension 1 Code]
,a.[Global Dimension 2 Code] 
,a.[Transaction No_]
,case when CalendarMonthID>=@Load then 0 else (Sum(a.[Amount])*-1) end  As NetTotal
,null as Budget$
 From  NAV_ETL.[dbo].[Medifast Inc$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate 
where  a.[G_L Account No_] >='40000'
and [G_L Account No_] not in ('96750')
and CalendarMonthID>=@Delete
Group By
CalendarMonthID
,a.[G_L Account No_]
,a.[Document No_]
,a.[Global Dimension 1 Code]
,a.[Global Dimension 2 Code] 
,a.[Transaction No_]

union all
select 
6 AS EntryNO
,CalendarMonthID
,a.[G_L Account No_]      
,a.[Document No_]+''+a.[G_L Account No_]  as DocumentID
,a.[Document No_]
,'TSFL' as [Global Dimension 1 Code]
,'TSFL' as [Global Dimension 2 Code] 
,a.[Transaction No_]
,case when CalendarMonthID>=@load then 0 else (Sum(a.[Amount])*-1) end  As NetTotal
,null as Budget$
From  NAV_ETL.[dbo].[Take Shape For Life$G_L Entry] a 
left join [BI_SSAS_Cubes].[dbo].[DimCalendar] c
on DATEADD(D, 0, DATEDIFF(D, 0,a.[Posting Date]))=c.CalendarDate 
where  a.[G_L Account No_] >='40000'
and CalendarMonthID>=@Delete
Group By
CalendarMonthID
,a.[G_L Account No_]
,a.[Document No_]
,a.[Global Dimension 1 Code]
,a.[Global Dimension 2 Code] 
,a.[Transaction No_]


























