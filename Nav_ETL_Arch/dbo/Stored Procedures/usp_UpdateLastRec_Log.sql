﻿CREATE procedure [dbo].[usp_UpdateLastRec_Log] as


Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([Entry No_]) from [NAV_ETL].dbo.[Jason Pharm$G_L Entry]), [DateModified] = GETDATE() where [TableName] = 'GL_Entry'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([Entry No_]) from [NAV_ETL].dbo.[Jason Pharm$Item Ledger Entry]), [DateModified] = GETDATE() where [TableName] = 'Item_Ledger_Entry'

--Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([Entry No_]) from [NAV_ETL].dbo.[Jason Pharm$Pending Orders Processed]), [DateModified] = GETDATE() where [TableName] = 'Pending_Orders_Processed'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([No_]) from [NAV_ETL].dbo.[Jason Pharm$Sales Header]), [DateModified] = GETDATE() where [TableName] = 'Sales_Header'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([Line No_]) from [NAV_ETL].dbo.[Jason Pharm$Sales Line]), [DateModified] = GETDATE() where [TableName] = 'Sales_Line'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([No_]) from [NAV_ETL].dbo.[Jason Pharm$Sales Header Archive]), [DateModified] = GETDATE() where [TableName] = 'Sales_Header_Archieve'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([Line No_]) from [NAV_ETL].dbo.[Jason Pharm$Sales Line Archive]), [DateModified] = GETDATE() where [TableName] = 'Sales_Line_Archieve'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([Entry No_]) from [NAV_ETL].dbo.[Jason Pharm$Value Entry]), [DateModified] = GETDATE() where [TableName] = 'Value_Entry'

Update dbo.[tbl_LastRec_log] Set MaxCountValue = (select max([Entry No_]) from [NAV_ETL].dbo.[Jason Pharm$Cust_ Ledger Entry]), [DateModified] = GETDATE() where [TableName] = 'Cust_Ledger_Entry'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([No_]) from [NAV_ETL].dbo.[Jason Pharm$Sales Invoice Header]), [DateModified] = GETDATE() where [TableName] = 'Sales_Invoice_Header'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([No_]) from [NAV_ETL].dbo.[Jason Pharm$Sales Cr_Memo Header]), [DateModified] = GETDATE() where [TableName] = 'Sales_Cr_Memo_Header'

Update dbo.[tbl_LastRec_log] Set MaxCountValue =(Select Max([No_]) from [NAV_ETL].dbo.[Jason Pharm$Sales Cr_Memo Line]), [DateModified] = GETDATE() where [TableName] = 'Sales_Cr_Memo_Line'

Update dbo.[tbl_LastRec_log] Set MaxCountValue = (select max([No_]) from [NAV_ETL].dbo.[Jason Pharm$Sales Shipment Header]), [DateModified] = GETDATE() where [TableName] = 'Sales_Shipment_Header'

Update dbo.[tbl_LastRec_log] Set MaxCountValue = (select max([No_]) from [NAV_ETL].dbo.[Jason Pharm$Return Receipt Header]), [DateModified] = GETDATE() where [TableName] = 'Return_Receipt_Header'


	