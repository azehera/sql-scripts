﻿

--USE NAV_ETL
--go

CREATE PROCEDURE [dbo].[usp_navision_line_discount_report]
as
DECLARE @StartOfMonth  DATETIME, @EndOfMonth   DATETIME


SET @StartOfMonth = DATEADD(M,-1,GETDATE())
SET @EndOfMonth = DATEADD(M,-1,GETDATE())

--set the first day and last day of the month
SET @ENDOfMonth = DateAdd(Month, 1, @StartOfMonth - Day(@StartOfMonth) + 1) -1
SET @StartOfMonth = DateAdd(Day, 1, @EndOfMonth - Day(@EndOfMonth) + 1) -1

--get the begining of the day and the end of the day
set @StartOfMonth = DATEADD(DAY, DATEDIFF(DAY, '19000101', @StartOfMonth), '19000101') 
SET @EndOfMonth = DATEADD(DAY, DATEDIFF(DAY, '18991231', @EndOfMonth), '19000101')

SELECT [Posting Date],
[Document No_],
[Line No_],[Sell-to Customer No_],Type, No_, [Location Code], [Posting Group],
[Shipment Date], Description,[Unit of Measure],Quantity,
[Unit Price],[Unit Cost],[Line Discount %],[Line Amount],[Shortcut Dimension 1 Code] AS [Sales Source Code],
[Shortcut Dimension 2 Code] AS [Department Code],[Customer Price Group],[Bill-to Customer No_],[Inv_ Discount Amount],
[Gen_ Bus_ Posting Group],[Gen_ Prod_ Posting Group],[Tax Area Code],[Tax Liable],[Tax Group Code],
[VAT Base Amount] AS [Tax Base Amount],[Line Amount] AS [Line Amount Excl. Tax],[VAT Difference] AS [Tax Difference],
[Qty_ per Unit of Measure],[Unit of Measure Code],[Item Category Code],[Product Group Code],
[Customer Disc_ Group],[Kit Item],[Build Kit],Volume,[Commission Volume],[Volume Discount],
[COA Customer No],[Cash on Account Amount]
 FROM NAV_ETL.[dbo].[Jason Pharm$Sales Invoice Line] WITH (NOLOCK)
WHERE [Line Discount %] = 100
AND [Posting Date] >= @StartOfMonth AND [Posting Date] < @EndOfMonth
ORDER BY [Document No_]

--GRANT EXECUTE ON usp_navision_line_discount_report TO ReportViewer
