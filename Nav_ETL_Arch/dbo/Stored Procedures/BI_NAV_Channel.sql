﻿



/*
--==================================================================================
Author:         Kalpesh Patel
Create date: 01/14/2014
-----------------------------dbo.BI_NAV_Channel_SalesCube-------------------
Create Daily Flash reports from Cube data
--==================================================================================   
REFERENCES
Database              Table/View/UDF                            Action            
------------------------------------------------------------------------------------
[BI_SSAS_Cubes]       [dbo].[FactSales]                        Select
[NAV_ETL]       [dbo].[Snapshot$NAV_Channel]   Insert
                        
==================================================================================
REVISION LOG
Date           Name                          Change
------------------------------------------------------------------------------------

==================================================================================
NOTES:
------------------------------------------------------------------------------------
==================================================================================
*/



/****** Script for SelectTopNRows command from SSMS  ******/
CREATE PROCEDURE [dbo].[BI_NAV_Channel] ( @DateEnd AS DATETIME )
AS
    DECLARE @DateMonthBegin DATETIME
    DECLARE @DateMonthEnd DATETIME
    DECLARE @DateYearBegin DATETIME
    DECLARE @DateYearEnd DATETIME
    DECLARE @DateYesterday DATETIME
    DECLARE @DatePriorYrMonthBegin DATETIME
    DECLARE @DatePriorYrYearBegin DATETIME
    DECLARE @DatePriorYrYesterday DATETIME
    DECLARE @DaysMonth INT
    DECLARE @DaysMonthElapsed INT
    DECLARE @DaysMonthRemaining INT
    DECLARE @DaysYear INT
    DECLARE @DaysYearElapsed INT
    DECLARE @DaysYearRemaining INT
    DECLARE @ProcessTimeBegin DATETIME
    DECLARE @ProcessTimeEnd DATETIME

    SET @ProcessTimeBegin = GETDATE()
    SET @DateEnd = CAST(FLOOR(CAST(@DateEnd AS FLOAT)) AS DATETIME)
    SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
    SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
    SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
    SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
    SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
    SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
    SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
    SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
    SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
    SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
    SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
    SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
    SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
    SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1




    DELETE  FROM [Snapshot$NAV_Channel]
    WHERE   ( RunDate = @DateEnd )

---- Calculate Yesterday Sales,Units,Cost and Orders info.---------------

    IF OBJECT_ID('Tempdb..#A') IS NOT NULL
        DROP TABLE #A
    SELECT  @DateYesterday AS ReportDate ,
            Country,
            [CustomerPostingGroup] ,
            [SalesChannel] ,
            ISNULL(SUM([Amount]), 0) AS [Invcd. Sales Yesterday] ,
            ISNULL(SUM(Units), 0) AS [Invcd. Units Yesterday] ,
            ISNULL(SUM([Unit Cost (LCY)] * Units), 0) AS InvExtCostYesterday ,
            ISNULL(COUNT(DISTINCT DocumentNo), 0) AS [Invcd. Orders Yesterday]
    INTO    #A
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] = @DateYesterday
    GROUP BY Country,[CustomerPostingGroup] ,
            [SalesChannel]
  
---- Calculate MTD Sales,Units,Cost and Orders info.---------------      

    IF OBJECT_ID('Tempdb..#B') IS NOT NULL
        DROP TABLE #B    
    SELECT  @DateYesterday AS ReportDate ,
            Country,
            [CustomerPostingGroup] ,
            [SalesChannel] ,
            ISNULL(SUM([Amount]), 0) AS [Invcd. Sales MTD] ,
            ISNULL(SUM(Units), 0) AS [Invcd. Units MTD] ,
            ISNULL(SUM([Unit Cost (LCY)] * Units), 0) AS InvExtCostMTD ,
            ISNULL(COUNT(DISTINCT DocumentNo), 0) AS [Invcd. Orders MTD]
    INTO    #B
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] BETWEEN @DateMonthBegin
                           AND     @DateYesterday
    GROUP BY Country,[CustomerPostingGroup] ,
            [SalesChannel]

---- Calculate Prior Year MTD Sales,Units,Cost and Orders info.---------------      
 
    IF OBJECT_ID('Tempdb..#C') IS NOT NULL
        DROP TABLE #C 
    SELECT  @DateYesterday AS ReportDate ,
            Country,
            [CustomerPostingGroup] ,
            [SalesChannel] ,
            ISNULL(SUM([Amount]), 0) AS [Invcd. Sales Prior Year MTD] ,
            ISNULL(SUM([Units]), 0) AS [Invcd. Units Prior Year MTD] ,
            ISNULL(COUNT(DISTINCT DocumentNo), 0) AS [Invcd. Orders Prior Year MTD]
    INTO    #C
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] BETWEEN @DatePriorYrMonthBegin
                           AND     @DatePriorYrYesterday
    GROUP BY Country,[CustomerPostingGroup] ,
            [SalesChannel]
 
---- Calculate YTD Sales,Units,Cost and Orders info.---------------     
  
    IF OBJECT_ID('Tempdb..#D') IS NOT NULL
        DROP TABLE #D      
    SELECT  @DateYesterday AS ReportDate ,
            Country,
            [CustomerPostingGroup] ,
            [SalesChannel] ,
            ISNULL(SUM([Amount]), 0) AS [Invcd. Sales YTD] ,
            ISNULL(SUM(Units), 0) AS [Invcd. Units YTD] ,
            ISNULL(SUM([Unit Cost (LCY)] * Units), 0) AS InvExtCostYTD ,
            ISNULL(COUNT(DISTINCT DocumentNo), 0) AS [Invcd. Orders YTD]
    INTO    #D
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] BETWEEN @DateYearBegin
                           AND     @DateYesterday
    GROUP BY Country,[CustomerPostingGroup] ,
            [SalesChannel]

---- Calculate Prior Year YTD Sales,Units,Cost and Orders info.---------------     
  
    IF OBJECT_ID('Tempdb..#E') IS NOT NULL
        DROP TABLE #E       
    SELECT  @DateYesterday AS ReportDate ,
            Country,
            [CustomerPostingGroup] ,
            [SalesChannel] ,
            ISNULL(SUM([Amount]), 0) AS [Invcd. Sales Prior Year YTD] ,
            ISNULL(SUM([Units]), 0) AS [Invcd. Units Prior Year YTD] ,
            ISNULL(COUNT(DISTINCT DocumentNo), 0) AS [Invcd. Orders Prior Year YTD]
    INTO    #E
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] BETWEEN @DatePriorYrYearBegin
                           AND     @DatePriorYrYesterday
    GROUP BY Country,[CustomerPostingGroup] ,
            [SalesChannel]
      
---- Calculate Shipping Revenue for Yesterday--------------- 
       
    IF OBJECT_ID('Tempdb..#F') IS NOT NULL
        DROP TABLE #F
    SELECT  @DateYesterday AS ReportDate ,
            Country,
            [CustomerPostingGroup] ,
            [SalesChannel] ,
            ISNULL(SUM([Amount]), 0) AS [Net Shipping Revenue Yesterday]
    INTO    #F
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] = @DateYesterday
            AND ItemCode = '63000'
    GROUP BY Country,[CustomerPostingGroup] ,
            [SalesChannel]
      
---- Calculate Shipping Revenue for MTD--------------- 
      
    IF OBJECT_ID('Tempdb..#G') IS NOT NULL
        DROP TABLE #G    
    SELECT  @DateYesterday AS ReportDate ,
            Country,
            [CustomerPostingGroup] ,
            [SalesChannel] ,
            ISNULL(SUM([Amount]), 0) AS [Net Shipping Revenue MTD]
    INTO    #G
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] BETWEEN @DateMonthBegin
                           AND     @DateYesterday
            AND ItemCode = '63000'
    GROUP BY Country,[CustomerPostingGroup] ,
            [SalesChannel]

---- Calculate Sales Credit for Yesterday--------------- 
 
    IF OBJECT_ID('Tempdb..#I') IS NOT NULL
        DROP TABLE #I
    SELECT  @DateYesterday AS ReportDate ,
            Country,
            [CustomerPostingGroup] ,
            [SalesChannel] ,
            ISNULL(SUM([Amount]), 0) AS [Invcd. SalesCredit Yesterday]
    INTO    #I
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] = @DateYesterday
            AND SalesType = 2
    GROUP BY Country,[CustomerPostingGroup] ,
            [SalesChannel]
      
---- Find all Channel and Customer Posting Group ---------------      
    IF OBJECT_ID('Tempdb..#H') IS NOT NULL
        DROP TABLE #H     
    SELECT DISTINCT
            Country,CustomerPostingGroup AS [Customer Posting Group] ,
            SalesChannel AS Channel
    INTO    #H
    FROM    [BI_SSAS_Cubes].[dbo].[FactSales]
    WHERE   [Posting Date] BETWEEN @DatePriorYrYearBegin
                           AND     @DateYesterday

----Insert Data into [dbo].[Snapshot$NAV_Channel] for Flash----
    INSERT  INTO [dbo].[Snapshot$NAV_Channel]
            SELECT  @DateEnd AS RunDate ,
                    @DateYesterday AS [Yesterday] ,
                    [Customer Posting Group] AS CPG ,
                    Channel AS [SalesSrce] ,
                    CONVERT(DECIMAL(18, 2), ISNULL([Invcd. Sales Yesterday], 0)) AS [InvSalesDay] ,
                    ISNULL([Invcd. Units Yesterday], 0) AS [InvUnitsDay] ,
                    ISNULL([Invcd. Orders Yesterday], 0) AS [InvOrdersDay] ,
                    CONVERT(DECIMAL(18, 2), ISNULL([Net Shipping Revenue Yesterday],
                                                   0)) AS [NetShipRevDay] ,
                    CONVERT(DECIMAL(18, 2), ISNULL([Invcd. Sales MTD], 0)) AS [InvSalesMTD] ,
                    CONVERT(DECIMAL(18, 2), ISNULL([Invcd. Sales Prior Year MTD],
                                                   0)) AS [InvSalesPriorYrMTD] ,
                    CONVERT(DECIMAL(18, 2), ISNULL([Net Shipping Revenue MTD],
                                                   0)) AS [NetShipRevMTD] ,
                    CONVERT(DECIMAL(18, 2), ISNULL(( [Invcd. Sales MTD]
                                                     / NULLIF(@DaysMonthElapsed,
                                                              0) )
                                                   * @DaysMonth, 0)) AS [InvSalesMoProj] ,
                    CONVERT(DECIMAL(18, 2), ISNULL(( [Net Shipping Revenue MTD]
                                                     / NULLIF(@DaysMonthElapsed,
                                                              0) )
                                                   * @DaysMonth, 0)) AS [NetShipRevMoProj] ,
                    ISNULL([Invcd. Units MTD], 0) AS [InvUnitsMTD] ,
                    ISNULL([Invcd. Units Prior Year MTD], 0) AS [InvUnitsPriorYrMTD] ,
                    ISNULL([Invcd. Orders MTD], 0) AS [InvOrdersMTD] ,
                    ISNULL([Invcd. Orders Prior Year MTD], 0) AS [InvOrdersPriorYrMTD] ,
                    CONVERT(DECIMAL(18, 2), ISNULL([Invcd. Sales YTD], 0)) AS [InvSalesYTD] ,
                    CONVERT(DECIMAL(18, 2), ISNULL([Invcd. Sales Prior Year YTD],
                                                   0)) AS [InvSalesPriorYrYTD] ,
                    CONVERT(DECIMAL(18, 2), ISNULL(( [Invcd. Sales YTD]
                                                     / NULLIF(@DaysYearElapsed,
                                                              0) ) * @DaysYear,
                                                   0)) AS [InvSalesProjYE] ,
                    ISNULL([Invcd. Units YTD], 0) AS [InvUnitsYTD] ,
                    ISNULL([Invcd. Units Prior Year YTD], 0) AS [InvUnitsPriorYrYTD] ,
                    ISNULL([Invcd. Orders YTD], 0) AS [InvOrdersYTD] ,
                    ISNULL([Invcd. Orders Prior Year YTD], 0) AS [InvOrdersPriorYrYTD] ,
                    @DateYearBegin AS [StartDateYear] ,
                    @DateMonthBegin AS [StartDateMonth] ,
                    @DatePriorYrYearBegin AS [StartDateYearPriorYr] ,
                    @DatePriorYrMonthBegin AS [StartDateMonthPriorYr] ,
                    @DatePriorYrYesterday AS [YesterdayPriorYr] ,
                    CONVERT(DECIMAL(18, 2), ISNULL(InvExtCostYesterday, 0)) AS [InvExtCostDay] ,
                    CONVERT(DECIMAL(18, 2), ISNULL(InvExtCostMTD, 0)) AS [InvExtCostMTD] ,
                    CONVERT(DECIMAL(18, 2), ISNULL(InvExtCostYTD, 0)) AS [InvExtCostYTD] ,
                    CONVERT(DECIMAL(18, 2), ISNULL([Invcd. SalesCredit Yesterday],
                                                   0)) AS [InvCreditsDay] ,
                    0 AS [SortOrder] ,
                    0 AS [SortOrder2] ,
                    0 AS [VarianceMTD] ,
                    0 AS [VarianceYTD],
                    h.Country
            FROM    #H h
                    LEFT JOIN #A a ON h.[Customer Posting Group] = a.[CustomerPostingGroup]
                                      AND h.Channel = a.[SalesChannel]
                                      AND h.Country = a.[Country]
                    LEFT JOIN #F f ON h.[Customer Posting Group] = f.[CustomerPostingGroup]
                                      AND h.Channel = f.[SalesChannel]
                                      AND h.Country = f.[Country]
                    LEFT JOIN #B b ON h.[Customer Posting Group] = b.[CustomerPostingGroup]
                                      AND h.Channel = b.[SalesChannel]
                                      AND h.Country = b.[Country]
                    LEFT JOIN #C c ON h.[Customer Posting Group] = c.[CustomerPostingGroup]
                                      AND h.Channel = c.[SalesChannel]
                                      AND h.Country = c.[Country]
                    LEFT JOIN #G g ON h.[Customer Posting Group] = g.[CustomerPostingGroup]
                                      AND h.Channel = g.[SalesChannel]
                                      AND h.Country = g.[Country]
                    LEFT JOIN #D d ON h.[Customer Posting Group] = d.[CustomerPostingGroup]
                                      AND h.Channel = d.[SalesChannel]
                                      AND h.Country = d.[Country]
                    LEFT JOIN #E e ON h.[Customer Posting Group] = e.[CustomerPostingGroup]
                                      AND h.Channel = e.[SalesChannel]
                                      AND h.Country = e.[Country]
                    LEFT JOIN #I i ON h.[Customer Posting Group] = i.[CustomerPostingGroup]
                                      AND h.Channel = i.[SalesChannel]
                                      AND h.Country = i.[Country]
            WHERE   ( [Invcd. Sales Yesterday] IS NOT NULL
                      OR [Invcd. Units Yesterday] IS NOT NULL
                      OR [Invcd. Orders Yesterday] IS NOT NULL
                      OR [Net Shipping Revenue Yesterday] IS NOT NULL
                      OR [Invcd. Sales MTD] IS NOT NULL
                      OR [Invcd. Sales Prior Year MTD] IS NOT NULL
                      OR [Net Shipping Revenue MTD] IS NOT NULL
                      OR [Invcd. Units MTD] IS NOT NULL
                      OR [Invcd. Orders MTD] IS NOT NULL
                      OR [Invcd. Sales YTD] IS NOT NULL
                      OR [Invcd. Sales Prior Year YTD] IS NOT NULL
                      OR [Invcd. Units YTD] IS NOT NULL
                      OR [Invcd. Orders YTD] IS NOT NULL
                      OR [Invcd. SalesCredit Yesterday] IS NOT NULL
                    )
      


    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder = 1000
    WHERE   ( CPG = 'TSFL' )
            AND ( SortOrder = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder = 900
    WHERE   ( CPG = 'MEDIFAST' )
            AND ( SortOrder = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder = 800
    WHERE   ( CPG = 'MWCC' )
            AND ( SortOrder = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder = 700
    WHERE   ( CPG = 'FRANCHISE' )
            AND ( SortOrder = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder = 600
    WHERE   ( CPG = 'DOCTORS' )
            AND ( SortOrder = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder = -100
    WHERE   ( CPG = 'CORPHEALTH' )
            AND ( SortOrder = 0 )



/****** SECTION 8:  HARDCODED SORT ORDER OF CHANNEL PER DOMINIC ****************/


    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 1000
    WHERE   ( SalesSrce = 'AUTO' )
            AND ( SortOrder2 = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 900
    WHERE   ( SalesSrce = 'CC' )
            AND ( SortOrder2 = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 800
    WHERE   ( SalesSrce = 'WEB' )
            AND ( SortOrder2 = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 700
    WHERE   ( SalesSrce = 'POS' )
            AND ( SortOrder2 = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 600
    WHERE   ( SalesSrce = 'FRANCHISE' )
            AND ( SortOrder2 = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 500
    WHERE   ( SalesSrce = 'WEST' )
            AND ( SortOrder2 = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 400
    WHERE   ( SalesSrce = 'CCDOC' )
            AND ( SortOrder2 = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 300
    WHERE   ( SalesSrce = 'WEBDOC' )
            AND ( SortOrder2 = 0 )

    UPDATE  [Snapshot$NAV_Channel]
    SET     SortOrder2 = 200
    WHERE   ( SalesSrce = 'TSFL' )
            AND ( SortOrder2 = 0 )


/****** SECTION 9:  REPORT DURATION OF QUERY ***********************************/

    SET @ProcessTimeEnd = GETDATE()
    PRINT 'Elapsed time: '
    PRINT DATEDIFF(SS, @ProcessTimeBegin, @ProcessTimeEnd)
   
      
      

      
      

      








