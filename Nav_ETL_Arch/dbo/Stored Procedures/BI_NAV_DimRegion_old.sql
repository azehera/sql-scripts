﻿








/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_DimRegion_old]  
DEVELOPER:  Kalpesh Patel      
DATE:  10/17/2012           
DESCRIPTION: Loading BI_NAV_DimRegion_old data for cube     
PARAMETERS/VARRIABLES:
NOTES:

*******************************************************************************************/


CREATE PROCEDURE [dbo].[BI_NAV_DimRegion_old] 
as



Truncate table BI_SSAS_Cubes.dbo.DimRegion


insert into BI_SSAS_Cubes.dbo.DimRegion

SELECT 
      distinct location_code
     ,[region_name]
           FROM [Book4Time_ETL].[dbo].[B4T_region] a
                 join [Book4Time_ETL].dbo.B4T_region_locations b
                       on a.region_id=b.region_id
                 join [Book4Time_ETL].dbo.B4T_location c
                       on c.location_id=b.location_id
                       
delete FROM [BI_SSAS_Cubes].[dbo].[DimRegion]
  where Region='Other' and RegionID='9679'
  
insert into BI_SSAS_Cubes.dbo.DimRegion

Select 
      distinct left([Sell-to Post Code],4)
      ,case when region is null or Region=' '
            then 'Other' else Region end as Region 
            from dbo.[Jason Pharm$Sales Invoice Header] 
                 left join Zip_codes_deluxe.dbo.ZipCodeDatabase_DELUXE b
                           on left([Sell-to Post Code], 4)=left(b.ZipCode, 4)
  
delete FROM [BI_SSAS_Cubes].[dbo].[DimRegion]
  where Region='Other' and RegionID='9679'
                
insert into BI_SSAS_Cubes.dbo.DimRegion              
                
Select 
     distinct left([Sell-to Post Code],4)
     ,case when region is null or Region=' ' 
           then 'Other' else Region end as Region 
           from  dbo.[Jason Pharm$Sales Cr_Memo Header]
                 left join Zip_codes_deluxe.dbo.ZipCodeDatabase_DELUXE b
                           on left([Sell-to Post Code], 4)=left(b.ZipCode, 4)
                           
 delete FROM [BI_SSAS_Cubes].[dbo].[DimRegion]
  where Region='Other' and RegionID='9679'
                
                
               
                
                
 ;with CTE as 
         (select 
                 RegionID
                 ,Region
                 ,ROW_NUMBER() over (partition by RegionID Order bY RegionID)as NO 
                 from BI_SSAS_Cubes.dbo.DimRegion)
          Delete From CTE where NO >1
                
                

                






