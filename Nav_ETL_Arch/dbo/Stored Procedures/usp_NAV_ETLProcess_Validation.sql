﻿

CREATE Procedure [dbo].[usp_NAV_ETLProcess_Validation]

as

DELETE FROM DBAWork.dbo.tbl_ETLProcesses_Validation where DBName = 'NAV_ETL' and CheckDate >= left(GETDATE(),11)

--select * FROM DBAWork.dbo.tbl_ETLProcesses_Validation where DBName = 'NAV_ETL' and CheckDate >= left(GETDATE(),11)

--G_L Entry
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate,
DBName)
SELECT 'G_L Entry - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$G_L Entry] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101'))-
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$G_L Entry] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Value Entry
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Value Entry - Diff Results',
CAST((SELECT COUNT(*) FROM [NAV_ETL].dbo.[Jason Pharm$Value Entry] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101'))-
(SELECT COUNT(*) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Value Entry] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101'))as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--Item
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Item - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Item] WHERE [Last Date Modified] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) -
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Item] WHERE [Last Date Modified] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--Adjustment Entry
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Adjustment Entry - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Adjustment Entry] WHERE [Order Created] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101') ) - 
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Adjustment Entry] WHERE [Order Created] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--Cust_ Ledger Entry
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Cust_ Ledger Entry - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Item Ledger Entry] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101') )-
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Item Ledger Entry] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--Pending Orders Processed
--INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
--ETL_Process, 
--ResultCount, 
--CheckDate, DBName)
--SELECT 'Pending Orders Processed - Diff Results',
--CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Pending Orders Processed] with (nolock)) -
--(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Pending Orders Processed] with (nolock)) as nvarchar(255)),
--GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Return Receipt Header
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Return Receipt Header - Diff Results', 
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Return Receipt Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) -
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Return Receipt Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Return Receipt Line
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Return Receipt Line - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Return Receipt Line] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101'))-
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Return Receipt Line] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Cr_Memo Header
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Sales Cr_Memo Header - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Sales Cr_Memo Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) -
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Cr_Memo Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101'))as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Cr_Memo Line
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)SELECT 'Sales Cr_Memo Line - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Sales Cr_Memo Line] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101'))-
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Cr_Memo Line] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Header
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Sales Header - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Sales Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101'))-
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Header Archive
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Sales Header Archive - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Sales Header Archive] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101'))-
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Header Archive] with (nolock) WHERE [Posting Date]>= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Invoice Header
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Sales Invoice Header - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Sales Invoice Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) -
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Invoice Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Invoice Line
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Sales Invoice Line - Diff Results', 
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Sales Invoice Line] with (nolock) WHERE [Shipment Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) -
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Invoice Line] with (nolock) WHERE [Shipment Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Line
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Sales Line - Diff Results',
CAST((SELECT COUNT(1) FROM NAV_ETL.dbo.[Jason Pharm$Sales Line] with (nolock) WHERE  [FA Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) -
(SELECT COUNT(1) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Line] with (nolock) WHERE [FA Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Line Archive
--INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
--ETL_Process, 
--ResultCount, 
--CheckDate, DBName)
--SELECT 'Sales Line Archive - Diff Results',
--CAST((SELECT COUNT(*) FROM NAV_ETL.dbo.[Jason Pharm$Sales Line Archive]with (nolock)) - 
--(SELECT COUNT(*) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Line Archive] with (nolock)) as nvarchar(255)),
--GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Shipment Header
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Sales Shipment Header - Diff Results',
CAST((SELECT COUNT(*) FROM NAV_ETL.dbo.[Jason Pharm$Sales Shipment Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) - 
(SELECT COUNT(*) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Shipment Header] with (nolock) WHERE [Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'

--dbo.Jason Pharm$Sales Shipment Line
INSERT INTO DBAWork.dbo.tbl_ETLProcesses_Validation (
ETL_Process, 
ResultCount, 
CheckDate, DBName)
SELECT 'Sales Shipment Line - Diff Results',
CAST((SELECT COUNT(*) FROM [NAV_ETL].dbo.[Jason Pharm$Sales Shipment Line] with (nolock) WHERE [FA Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) - 
(SELECT COUNT(*) FROM NAVISION_PROD.[NAVPROD].dbo.[Jason Pharm$Sales Shipment Line] with (nolock) WHERE [FA Posting Date] >= DATEADD(DAY, DATEDIFF(DAY, '19000101', DATEADD(d,-1,Getdate())), '19000101')) as nvarchar(255)),
GETDATE(), 'NAV_ETL'


select * from DBAWork.dbo.tbl_ETLProcesses_Validation
/****************** Send Email ***********************************/
/*
EXEC msdb.dbo.sp_send_dbmail
	@profile_name = 'SQL Admin',
	@recipients   = 'SQLBackupNotification@choosemedifast.com',
	@subject      = 'ETL Processes Vaidation Report (Server:DP-BIDB Server)',
	@body = 'ETL Processes Vaidation Report Summary for(Server:DP-BIDB Server)',
	@query = 'select * from DBAWork.dbo.tbl_ETLProcesses_Validation' ,
    --@attach_query_result_as_file = 1,
    @query_result_width = 1000,
    @Importance = 'High',
    @body_format = 'HTML'
*/

