﻿


CREATE PROCEDURE [dbo].[BI_NAV_Production] (@DateEnd AS DATETIME)
AS
/****** MED0001 MRH 06/07/12: Business Intelligence / Executive Dashboard ******/

/****** MED0002 MRH 06/11/12: Initial record creation and grouping was based on ILEs for yesterday;
                                this does not account for groupings with MTD and YTD totals.
                              In the section "update amount fields," removed extension by Qty. per Unit of Measure
                                see NAV's online help re: Quantity field and Item's Base Unit of Measure               
        Added OM BLEND as New Location kpatel 05/09/2013       ******/
                                

DECLARE @DateMonthBegin DATETIME
DECLARE @DateMonthEnd DATETIME
DECLARE @DateYearBegin DATETIME
DECLARE @DateYearEnd DATETIME
DECLARE @DateYesterday DATETIME
DECLARE @DatePriorYrMonthBegin DATETIME
DECLARE @DatePriorYrYearBegin DATETIME
DECLARE @DatePriorYrYesterday DATETIME
DECLARE @DaysMonth INT
DECLARE @DaysMonthElapsed INT
DECLARE @DaysMonthRemaining INT
DECLARE @DaysYear INT
DECLARE @DaysYearElapsed INT
DECLARE @DaysYearRemaining INT
DECLARE @ProcessTimeBegin DATETIME
DECLARE @ProcessTimeEnd DATETIME

SET @ProcessTimeBegin = GetDate()
SET @DateEnd = CAST(FLOOR(CAST(@DateEnd AS float)) AS DATETIME)
SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1

/****** if query is re-run, delete records for RunDate *************************/

BEGIN TRANSACTION

DELETE FROM [Snapshot$NAV_Production]
WHERE (RunDate = @DateEnd)

COMMIT TRANSACTION

/****** initial population of data for RunDate *********************************/

BEGIN TRANSACTION

INSERT INTO [Snapshot$NAV_Production]
  (RunDate,Yesterday,YesterdayPriorYr,
   LocationCode,LocationClassifyCode,ItemCategoryCode,ProductGroupCode)
SELECT
  @DateEnd,
  @DateYesterday,
  @DatePriorYrYesterday,
  Location.Code AS _LocationCode,
  Location.Code AS _LocationClassifyCode,
  ICC.Code AS _ItemCategoryCode,
  PG.Code AS _ProductGroupCode
FROM [Jason Pharm$Location] Location
JOIN [Jason Pharm$Item Category] ICC
ON ICC.Code <> ''
JOIN [Jason Pharm$Product Group] PG
ON PG.[Item Category Code] = ICC.Code

COMMIT TRANSACTION

/****** get LocationClassifyCode ***********************************************/
/******          hard-coded per Dominic    *************************************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Production]
SET LocationClassifyCode = 'OTHER'
WHERE ([RunDate] = @DateEnd) AND
      ([LocationCode] NOT IN ('OWINGS','SUNRISE','TDC','OM BLEND'))

COMMIT TRANSACTION

/****** get quantities, amounts for date ranges ********************************/

BEGIN TRANSACTION

DECLARE @LocCode VARCHAR(10)
DECLARE @ItemCatCode VARCHAR(10)
DECLARE @ProdGrpCode VARCHAR(10)

DECLARE Cursor1 CURSOR FAST_FORWARD FOR
SELECT LocationCode, ItemCategoryCode, ProductGroupCode
FROM [Snapshot$NAV_Production]
WHERE RunDate = @DateEnd

OPEN Cursor1
FETCH NEXT FROM Cursor1 INTO @LocCode, @ItemCatCode, @ProdGrpCode

WHILE @@FETCH_STATUS = 0
BEGIN

UPDATE [Snapshot$NAV_Production]
SET UnitProductionToday = ISNULL(TotalQty,0),
    AmountProductionToday = ISNULL(TotalCost,0)
FROM [Snapshot$NAV_Production]
LEFT OUTER JOIN dbo.NavMfg_GetQtyCostDateRange (@LocCode,@ItemCatCode,@ProdGrpCode,@DateYesterday,@DateYesterday)
ON TotalQty <> -999999999
WHERE (RunDate = @DateEnd) AND
      (LocationCode = @LocCode) AND
      (ItemCategoryCode = @ItemCatCode) AND
      (ProductGroupCode = @ProdGrpCode)

UPDATE [Snapshot$NAV_Production]
SET UnitProductionMTD = ISNULL(TotalQty,0),
    AmountProductionMTD = ISNULL(TotalCost,0)
FROM [Snapshot$NAV_Production]
LEFT OUTER JOIN dbo.NavMfg_GetQtyCostDateRange (@LocCode,@ItemCatCode,@ProdGrpCode,@DateMonthBegin,@DateYesterday)
ON TotalQty <> -999999999
WHERE (RunDate = @DateEnd) AND
      (LocationCode = @LocCode) AND
      (ItemCategoryCode = @ItemCatCode) AND
      (ProductGroupCode = @ProdGrpCode)

UPDATE [Snapshot$NAV_Production]
SET UnitProductionYTD = ISNULL(TotalQty,0),
    AmountProductionYTD = ISNULL(TotalCost,0)
FROM [Snapshot$NAV_Production]
LEFT OUTER JOIN dbo.NavMfg_GetQtyCostDateRange (@LocCode,@ItemCatCode,@ProdGrpCode,@DateYearBegin,@DateYesterday)
ON TotalQty <> -999999999
WHERE (RunDate = @DateEnd) AND
      (LocationCode = @LocCode) AND
      (ItemCategoryCode = @ItemCatCode) AND
      (ProductGroupCode = @ProdGrpCode)

UPDATE [Snapshot$NAV_Production]
SET UnitProductionPriorYrToday = ISNULL(TotalQty,0),
    AmountProductionPriorYrToday = ISNULL(TotalCost,0)
FROM [Snapshot$NAV_Production]
LEFT OUTER JOIN dbo.NavMfg_GetQtyCostDateRange (@LocCode,@ItemCatCode,@ProdGrpCode,@DatePriorYrYesterday,@DatePriorYrYesterday)
ON TotalQty <> -999999999
WHERE (RunDate = @DateEnd) AND
      (LocationCode = @LocCode) AND
      (ItemCategoryCode = @ItemCatCode) AND
      (ProductGroupCode = @ProdGrpCode)

UPDATE [Snapshot$NAV_Production]
SET UnitProductionPriorYrMTD = ISNULL(TotalQty,0),
    AmountProductionPriorYrMTD = ISNULL(TotalCost,0)
FROM [Snapshot$NAV_Production]
LEFT OUTER JOIN dbo.NavMfg_GetQtyCostDateRange (@LocCode,@ItemCatCode,@ProdGrpCode,@DatePriorYrMonthBegin,@DatePriorYrYesterday)
ON TotalQty <> -999999999
WHERE (RunDate = @DateEnd) AND
      (LocationCode = @LocCode) AND
      (ItemCategoryCode = @ItemCatCode) AND
      (ProductGroupCode = @ProdGrpCode)

UPDATE [Snapshot$NAV_Production]
SET UnitProductionPriorYrYTD = ISNULL(TotalQty,0),
    AmountProductionPriorYrYTD = ISNULL(TotalCost,0)
FROM [Snapshot$NAV_Production]
LEFT OUTER JOIN dbo.NavMfg_GetQtyCostDateRange (@LocCode,@ItemCatCode,@ProdGrpCode,@DatePriorYrYearBegin,@DatePriorYrYesterday)
ON TotalQty <> -999999999
WHERE (RunDate = @DateEnd) AND
      (LocationCode = @LocCode) AND
      (ItemCategoryCode = @ItemCatCode) AND
      (ProductGroupCode = @ProdGrpCode)

FETCH NEXT FROM Cursor1 INTO @LocCode, @ItemCatCode, @ProdGrpCode
END

CLOSE Cursor1
DEALLOCATE Cursor1

COMMIT TRANSACTION

/****** delete records with empty values ***************************************/

BEGIN TRANSACTION

DELETE FROM [Snapshot$NAV_Production]
WHERE (UnitProductionToday = 0) AND
      (UnitProductionMTD = 0) AND
      (UnitProductionYTD = 0) AND
      (UnitProductionPriorYrToday = 0) AND
      (UnitProductionPriorYrMTD = 0) AND
      (UnitProductionPriorYrYTD = 0) AND
      (AmountProductionToday = 0) AND
      (AmountProductionMTD = 0) AND
      (AmountProductionYTD = 0) AND
      (AmountProductionPriorYrToday = 0) AND
      (AmountProductionPriorYrMTD = 0) AND
      (AmountProductionPriorYrYTD = 0) AND
      (RunDate = @DateEnd)

COMMIT TRANSACTION

/****** hardcoded sort order of location per Luc *******************************/

BEGIN TRANSACTION

UPDATE [Snapshot$NAV_Production]
SET SortOrder = 1000
WHERE (SortOrder = 0) AND (LocationClassifyCode = 'OWINGS')

UPDATE [Snapshot$NAV_Production]
SET SortOrder = 900
WHERE (SortOrder = 0) AND (LocationClassifyCode = 'OM BLEND')

UPDATE [Snapshot$NAV_Production]
SET SortOrder = 800
WHERE (SortOrder = 0) AND (LocationClassifyCode = 'OTHER')

UPDATE [Snapshot$NAV_Production]
SET SortOrder = 600
WHERE (SortOrder = 0) AND (LocationClassifyCode = 'SUNRISE')

UPDATE [Snapshot$NAV_Production]
SET SortOrder = 400
WHERE (SortOrder = 0) AND (LocationClassifyCode = 'TDC')

COMMIT TRANSACTION

/****** report duration of query ***********************************************/

SET @ProcessTimeEnd = GetDate()
PRINT 'Elapsed time (sec): '
PRINT DATEDIFF(SS,@ProcessTimeBegin,@ProcessTimeEnd)
PRINT 'Elapsed time (min): '
PRINT DATEDIFF(MI,@ProcessTimeBegin,@ProcessTimeEnd)


