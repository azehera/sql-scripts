﻿









/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_SelltoCustomerKey]  
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Dimesion data for cube from NAVPROD database     
PARAMETERS/VARRIABLES:
NOTES:

*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_SelltoCustomerKey_bak] 
as

with cte as (SELECT 
                 CONVERT(varchar,[Sell-to Customer No_])+''+LEFT ([Sell-to Post Code],5) as ShiptoCustomerKey
                ,[Sell-to Customer No_] as SelltoCustomerID
                ,ROW_NUMBER() over (partition by [Sell-to Customer No_],LEFT ([Sell-to Post Code],5) Order by [Sell-to Customer No_]) as no
                ,[Sell-to Customer Name]
                ,[Sell-to Address]
                ,Convert (Varchar,[Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] .city) as [Sell-to City]
                ,[Sell-to County]
                ,LEFT ([Sell-to Post Code],5) as Zip5
                ,LEFT([Sell-to Post Code],3) as Zip3
                ,LEFT([Sell-to Post Code],2) as Zip2
                ,[Sell-to Country_Region Code] as Country
                ,[Payment Discount %]
                ,[Currency Code]
                ,[Currency Factor],
                isnull([DMA Code],'Unknown') as [DMA Code],
                 isnull([DMA Name],'Unknown') as [DMA Name]
            
                FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] 
                    left join [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] 
                on  LEFT ([Sell-to Post Code],5)=zipcode COLLATE DATABASE_DEFAULT 
                and [PrimaryRecord]='P'
                left join Zip_codes_deluxe.dbo.DMA_Data c
                on LEFT ([Sell-to Post Code],5)=c.ZipCode COLLATE DATABASE_DEFAULT 
             
                
                     where [Customer Posting Group]<>'CORPCLINIC'
                     )
              select CONVERT(nvarchar,ShiptoCustomerKey) as SelltoCustomerKey 
                    ,SelltoCustomerID 
                    ,[Sell-to Customer Name]
                    , [Sell-to Address]
                    ,[Sell-to City]
                    ,case when [Sell-to County]=' ' then null 
                               else [Sell-to County] end as [Sell-to County]
                    ,Zip5
                    ,Zip3
                    ,Zip2
                    ,Country
                    ,[Payment Discount %]
                    ,a.[Currency Code]
                    ,[Currency Factor]
                    ,isnull([E-Mail],' ') as [E-Mail]
                    ,[DMA Code],
                     [DMA Name]
         
                    from cte a
                         left join [NAV_ETL].[dbo].[Jason Pharm$Customer] c
	                          on a.SelltoCustomerID COLLATE DATABASE_DEFAULT=c.No_
	                          where no=1 



































