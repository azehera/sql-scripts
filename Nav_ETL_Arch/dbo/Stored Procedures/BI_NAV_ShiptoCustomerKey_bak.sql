﻿











/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_ShiptoCustomerKey]  
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Loading Dimesion data for cube from NAV_ETL database     
PARAMETERS/VARRIABLES:
NOTES:

*******************************************************************************************/

CREATE PROCEDURE [dbo].[BI_NAV_ShiptoCustomerKey_bak]
AS 
    WITH    cte
              AS ( SELECT   CONVERT(VARCHAR, [Sell-to Customer No_]) + ''
                            + LEFT([Ship-to Post Code], 5) AS ShiptoCustomerKey ,
                            [Sell-to Customer No_] AS SelltoCustomerID ,
                            ROW_NUMBER() OVER ( PARTITION BY [Sell-to Customer No_],
                                                LEFT([Ship-to Post Code], 5) ORDER BY [Sell-to Customer No_] ) AS no ,
                            [Ship-to Name] ,
                            CONVERT (VARCHAR, [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE].city) AS [Ship-to City] ,
                            LEFT([Ship-to Post Code], 5) AS Zip5 ,
                            LEFT([Ship-to Post Code], 3) AS Zip3 ,
                            LEFT([Ship-to Post Code], 2) AS Zip2 ,
                            [Ship-to Country_Region Code] AS Country ,
                            [Payment Discount %] ,
                            [Currency Code] ,
                            [Currency Factor] ,
                            [Ship-to County] ,
                            ISNULL([DMA Code], 'Unknown') AS [DMA Code] ,
                            ISNULL([DMA Name], 'Unknown') AS [DMA Name]
                   FROM     [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] a
                            LEFT JOIN [Zip_codes_deluxe].[dbo].[ZipCodeDatabase_DELUXE] ON LEFT(a.[Ship-to Post Code],
                                                              5) = zipcode COLLATE DATABASE_DEFAULT 
                                                              AND [PrimaryRecord] = 'P'
                            LEFT JOIN Zip_codes_deluxe.dbo.DMA_Data c ON LEFT([Ship-to Post Code],
                                                              5) = c.ZipCode COLLATE DATABASE_DEFAULT 
                   WHERE    [Customer Posting Group] <> 'CORPCLINIC'
                 )
        SELECT  CONVERT(NVARCHAR, ShiptoCustomerKey) AS ShiptoCustomerKey ,
                SelltoCustomerID ,
               [Ship-to Name]as[Ship-to Name] ,
                [Ship-to City] as [Ship-to City] ,
                [Payment Discount %] ,
                a.[Currency Code] ,
                [Currency Factor] ,
                Zip5
                ,Zip3
                ,Zip2,
                Country ,
                CASE WHEN [Ship-to County] = ' ' THEN NULL
                     ELSE [Ship-to County]
                END AS [Ship-to County] ,
                ISNULL([E-Mail], ' ') AS [E-Mail] ,
                [DMA Code] ,
                [DMA Name]
        FROM    cte a
                LEFT JOIN [NAV_ETL].[dbo].[Jason Pharm$Customer] c ON a.SelltoCustomerID COLLATE DATABASE_DEFAULT = c.No_
        WHERE   no = 1 








































