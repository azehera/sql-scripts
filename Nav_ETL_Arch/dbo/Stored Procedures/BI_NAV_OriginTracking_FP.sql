﻿CREATE PROCEDURE [dbo].[BI_NAV_OriginTracking_FP] (@DateEnd AS DATETIME)
AS
/****** MED0001 MRH 2/22/12: Business Intelligence / Executive Dashboard ******/

DECLARE @DateMonthBegin DATETIME
DECLARE @DateMonthEnd DATETIME
DECLARE @DateYearBegin DATETIME
DECLARE @DateYearEnd DATETIME
DECLARE @DateYesterday DATETIME
DECLARE @DatePriorYrMonthBegin DATETIME
DECLARE @DatePriorYrYearBegin DATETIME
DECLARE @DatePriorYrYesterday DATETIME
DECLARE @DaysMonth INT
DECLARE @DaysMonthElapsed INT
DECLARE @DaysMonthRemaining INT
DECLARE @DaysYear INT
DECLARE @DaysYearElapsed INT
DECLARE @DaysYearRemaining INT

SET @DateEnd = CAST(FLOOR(CAST(@DateEnd AS float)) AS DATETIME)
SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1

BEGIN TRANSACTION

DELETE FROM [Snapshot$NAV_OriginTracking_FP]
WHERE (RunDate = @DateEnd)

COMMIT TRANSACTION

/*** Initial insert ***/

BEGIN TRANSACTION

INSERT INTO [Snapshot$NAV_OriginTracking_FP]
  ([RunDate],[Yesterday],
   [Location Code],[Item No_],[Document No_],[Unit of Measure Code],[Expiration Date],[Quantity (Base)],Description)
SELECT
  @DateEnd AS _RunDate,
  @DateYesterday AS _Yesterday,
  OTE.[Location Code] AS _LocCode,
  OTE.[Item No_] AS _ItemNo,
  OTE.[Document No_] AS _DocNo,
  OTE.[Unit of Measure Code] AS _Uom,
  OTE.[Expiration Date] AS _ExpDate,
  SUM(OTE.[Quantity (Base)]) AS _Qty,
  ISNULL((
    SELECT ISNULL(Description,'')
    FROM [Jason Pharm$Item] Item
    WHERE Item.No_ COLLATE DATABASE_DEFAULT = OTE.[Item No_]
  ),'') AS _Desc
FROM [Jason Pharm$Origin Tracking Entry] OTE
WHERE [Close Date] < '1/1/1800'
GROUP BY [Location Code],[Item No_],[Unit of Measure Code],[Document No_],[Expiration Date]

COMMIT TRANSACTION