﻿
/****************************************************************************
Modifications:

8/20/2014	D. Smith	See Comment #1 below regarding PointRate = 1

*****************************************************************************/
CREATE PROCEDURE [dbo].[BI_NAV_Inventory] ( @DateEnd AS DATETIME )
AS /****** MED0001 MRH 01/31/12: Business Intelligence / Executive Dashboard ******/

    DECLARE @DateBegin DATETIME
    DECLARE @DateMonthBegin DATETIME
    DECLARE @DateMonthEnd DATETIME
    DECLARE @DatePriorMonthEnd DATETIME
    DECLARE @DateYearBegin DATETIME
    DECLARE @DateYearEnd DATETIME
    DECLARE @DateYesterday DATETIME
    DECLARE @DatePriorYrMonthBegin DATETIME
    DECLARE @DatePriorYrYearBegin DATETIME
    DECLARE @DatePriorYrYearEnd DATETIME
    DECLARE @DatePriorYrYesterday DATETIME
    DECLARE @DaysMonth INT
    DECLARE @DaysMonthElapsed INT
    DECLARE @DaysMonthRemaining INT
    DECLARE @DaysYear INT
    DECLARE @DaysYearElapsed INT
    DECLARE @DaysYearRemaining INT
    DECLARE @ProcessTimeBegin DATETIME
    DECLARE @ProcessTimeEnd DATETIME

    SET @ProcessTimeBegin = GETDATE()
    SET @DateEnd = CAST(FLOOR(CAST(@DateEnd AS FLOAT)) AS DATETIME)
    SET @DateBegin = '1/1/1753'
    SET @DateYesterday = DATEADD(DD, -1, @DateEnd)
    SET @DateMonthBegin = DATEADD(MM, DATEDIFF(MM, 0, @DateYesterday), 0)
    SET @DateMonthEnd = DATEADD(DD, -1, DATEADD(MM, 1, @DateMonthBegin))
    SET @DatePriorMonthEnd = DATEADD(DD, -1, @DateMonthBegin)
    SET @DateYearBegin = DATEADD(YY, DATEDIFF(YY, 0, @DateYesterday), 0)
    SET @DateYearEnd = DATEADD(DD, -1, DATEADD(YY, 1, @DateYearBegin))
    SET @DatePriorYrYesterday = DATEADD(YY, -1, @DateYesterday)
    SET @DatePriorYrMonthBegin = DATEADD(YY, -1, @DateMonthBegin)
    SET @DatePriorYrYearBegin = DATEADD(YY, -1, @DateYearBegin)
    SET @DatePriorYrYearEnd = DATEADD(DD, -1, @DateYearBegin)
    SET @DaysMonthElapsed = DATEDIFF(DD, @DateMonthBegin, @DateYesterday) + 1
    SET @DaysMonthRemaining = DATEDIFF(DD, @DateYesterday, @DateMonthEnd)
    SET @DaysMonth = DATEDIFF(DD, @DateMonthBegin, @DateMonthEnd) + 1
    SET @DaysYearElapsed = DATEDIFF(DD, @DateYearBegin, @DateYesterday) + 1
    SET @DaysYearRemaining = DATEDIFF(DD, @DateYesterday, @DateYearEnd)
    SET @DaysYear = DATEDIFF(DD, @DateYearBegin, @DateYearEnd) + 1

/*** delete existing records ***/

    BEGIN TRANSACTION

    DELETE  FROM [Snapshot$NAV_Inventory]
    WHERE   RunDate = @DateEnd

    COMMIT TRANSACTION

/*** insert a fresh set of groupings of Location, Product from B4T; include qty on hand for specific dates ***/

    BEGIN TRANSACTION

    INSERT  INTO [Snapshot$NAV_Inventory]
            ( RunDate ,
              Yesterday ,
              YesterdayPriorYr ,
              EndingDatePriorMonth ,
              EndingDatePriorYear ,
              InvLocation ,
              B4T_ItemNo ,
              ItemNo ,
              InvPostClass ,
              InvPostGroup ,
              ItemCategoryCode ,
              ProductGroupCode ,
              StandardCost ,
              QtyPerUnitOfMeasure ,
              OnHandQtyDay ,
              OnHandQtyPriorYrDay ,
              OnHandQtyPriorMonthEnd ,
              OnHandQtyPriorYearEnd ,
              OnHandQtyBaseDay ,
              OnHandQtyBasePriorYrDay ,
              OnHandQtyBasePriorMonthEnd ,
              OnHandQtyBasePriorYearEnd ,
              OnHandAmtDay ,
              OnHandAmtPriorYrDay ,
              OnHandAmtPriorMonthEnd ,
              OnHandAmtPriorYearEnd
            )
            SELECT  @DateEnd ,
                    @DateYesterday ,
                    @DatePriorYrYesterday ,
                    @DatePriorMonthEnd ,
                    @DatePriorYrYearEnd ,
                    B4TIT.location_id _InvLocation ,
                    B4TIT.product_id _B4T_ItemNo ,
                    (  /* retrieving NAV item number from B4T reference table */ SELECT
                                                              ISNULL(sku, '')
                                                              FROM
                                                              [Book4Time_ETL].[dbo].[B4T_product_master] B4TPM
                                                              WHERE
                                                              B4TPM.product_id = B4TIT.product_id
                    ) AS _ItemNo ,
                    '' AS _InvPostClass ,
                    '' AS _InvPostGroup ,
                    '' AS _ItemCategoryCode ,
                    '' AS _ProductGroupCode ,
                    0 AS _StandardCost ,
                    1 AS _QtyPerUnitOfMeasure ,
                    dbo.NavInvt_B4T_GetQtyDateRange(B4TIT.location_id,
                                                    B4TIT.product_id,
                                                    @DateBegin, @DateYesterday) AS _OnHandQtyDay ,
                    dbo.NavInvt_B4T_GetQtyDateRange(B4TIT.location_id,
                                                    B4TIT.product_id,
                                                    @DateBegin,
                                                    @DatePriorYrYesterday) AS _OnHandQtyPriorYrDay ,
                    dbo.NavInvt_B4T_GetQtyDateRange(B4TIT.location_id,
                                                    B4TIT.product_id,
                                                    @DateBegin,
                                                    @DatePriorMonthEnd) AS _OnHandQtyPriorMonthEnd ,
                    dbo.NavInvt_B4T_GetQtyDateRange(B4TIT.location_id,
                                                    B4TIT.product_id,
                                                    @DateBegin,
                                                    @DatePriorYrYearEnd) AS _OnHandQtyPriorYearEnd ,
                    0 AS _OnHandQtyBaseDay ,
                    0 AS _OnHandQtyBasePriorYrDay ,
                    0 AS _OnHandQtyBasePriorMonthEnd ,
                    0 AS _OnHandQtyBasePriorYearEnd ,
                    0 AS _OnHandAmtDay ,
                    0 AS _OnHandAmtPriorYrDay ,
                    0 AS _OnHandAmtPriorMonthEnd ,
                    0 AS _OnHandAmtPriorYearEnd
            FROM    [Book4Time_ETL].[dbo].[B4T_Inventory_Transactions] B4TIT
            GROUP BY location_id ,
                    product_id

    COMMIT TRANSACTION

/*** insert a fresh set of groupings of Location, Product from NAV; include qty on hand for specific dates ***/

    BEGIN TRANSACTION

    INSERT  INTO [Snapshot$NAV_Inventory]
            ( RunDate ,
              Yesterday ,
              YesterdayPriorYr ,
              EndingDatePriorMonth ,
              EndingDatePriorYear ,
              InvLocation ,
              ItemNo ,
              NavItemNo ,
              B4T_ItemNo ,
              InvPostClass ,
              InvPostGroup ,
              ItemCategoryCode ,
              ProductGroupCode ,
              StandardCost ,
              QtyPerUnitOfMeasure ,
              OnHandQtyDay ,
              OnHandQtyPriorYrDay ,
              OnHandQtyPriorMonthEnd ,
              OnHandQtyPriorYearEnd ,
              OnHandQtyBaseDay ,
              OnHandQtyBasePriorYrDay ,
              OnHandQtyBasePriorMonthEnd ,
              OnHandQtyBasePriorYearEnd ,
              OnHandAmtDay ,
              OnHandAmtPriorYrDay ,
              OnHandAmtPriorMonthEnd ,
              OnHandAmtPriorYearEnd
            )
            SELECT  @DateEnd ,
                    @DateYesterday ,
                    @DatePriorYrYesterday ,
                    @DatePriorMonthEnd ,
                    @DatePriorYrYearEnd ,
                    ILE.[Location Code] AS _InvLocation ,
                    ILE.[Item No_] AS _ItemNo ,
                    ILE.[Item No_] AS _NavItemNo ,
                    '' AS _B4T_ItemNo ,
                    '' AS _InvPostClass ,
                    '' AS _InvPostGroup ,
                    '' AS _ItemCategoryCode ,
                    '' AS _ProductGroupCode ,
                    0 AS _StandardCost ,
                    1 AS _QtyPerUnitOfMeasure ,
                    dbo.NavInvt_GetQtyDateRange(ILE.[Location Code],
                                                ILE.[Item No_], @DateBegin,
                                                @DateYesterday) AS _OnHandQtyDay ,
                    dbo.NavInvt_GetQtyDateRange(ILE.[Location Code],
                                                ILE.[Item No_], @DateBegin,
                                                @DatePriorYrYesterday) AS _OnHandQtyPriorYrDay ,
                    dbo.NavInvt_GetQtyDateRange(ILE.[Location Code],
                                                ILE.[Item No_], @DateBegin,
                                                @DatePriorMonthEnd) AS _OnHandQtyPriorMonthEnd ,
                    dbo.NavInvt_GetQtyDateRange(ILE.[Location Code],
                                                ILE.[Item No_], @DateBegin,
                                                @DatePriorYrYearEnd) AS _OnHandQtyPriorYearEnd ,
                    0 AS _OnHandQtyBaseDay ,
                    0 AS _OnHandQtyBasePriorYrDay ,
                    0 AS _OnHandQtyBasePriorMonthEnd ,
                    0 AS _OnHandQtyBasePriorYearEnd ,
                    0 AS _OnHandAmtDay ,
                    0 AS _OnHandAmtPriorYrDay ,
                    0 AS _OnHandAmtPriorMonthEnd ,
                    0 AS _OnHandAmtPriorYearEnd
            FROM    [Jason Pharm$Item Ledger Entry] ILE
            GROUP BY [Location Code] ,
                    [Item No_]

    COMMIT TRANSACTION

/*** retrieve
	NAV item number
	Inventory Posting Group
	Item Category Code
	Product Group Code
	Inventory Posting Class
***/

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     NavItemNo = ( SELECT    No_
                          FROM      [Jason Pharm$Item] Item
                          WHERE     Item.No_ COLLATE DATABASE_DEFAULT = [Snapshot$NAV_Inventory].ItemNo
                        )
    WHERE   ( RunDate = @DateEnd )
            AND ( NavItemNo IS NULL )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     InvPostGroup = ( SELECT [Inventory Posting Group]
                             FROM   [Jason Pharm$Item] Item
                             WHERE  Item.No_ COLLATE DATABASE_DEFAULT = [Snapshot$NAV_Inventory].NavItemNo
                           )
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( NavItemNo <> '' )
            AND ( RunDate = @DateEnd )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     ItemCategoryCode = ( SELECT [Item Category Code]
                                 FROM   [Jason Pharm$Item] Item
                                 WHERE  Item.No_ COLLATE DATABASE_DEFAULT = [Snapshot$NAV_Inventory].NavItemNo
                               )
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( NavItemNo <> '' )
            AND ( RunDate = @DateEnd )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     ProductGroupCode = ( SELECT [Product Group Code]
                                 FROM   [Jason Pharm$Item] Item
                                 WHERE  Item.No_ COLLATE DATABASE_DEFAULT = [Snapshot$NAV_Inventory].NavItemNo
                               )
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( NavItemNo <> '' )
            AND ( RunDate = @DateEnd )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     InvPostClass = ( SELECT NavInvPosting.InvPostClass
                             FROM   [Snapshot$NAV_InventoryPosting] NavInvPosting
                             WHERE  NavInvPosting.InvPostGroup COLLATE DATABASE_DEFAULT = [Snapshot$NAV_Inventory].InvPostGroup
                           )
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( NavItemNo <> '' )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

/*** retrieve location name ***/

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     InvLocationName = ISNULL(( SELECT   location_name
                                       FROM     [Book4Time_ETL].[dbo].B4T_location B4TLoc
                                       WHERE    CAST(B4TLoc.location_id AS VARCHAR(50)) COLLATE DATABASE_DEFAULT = [Snapshot$NAV_Inventory].InvLocation
                                     ), '')
    WHERE   ( B4T_ItemNo <> '' )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     InvLocationName = ISNULL(( SELECT   Name
                                       FROM     [NAV_ETL].[dbo].[Jason Pharm$Location] Loc
                                       WHERE    Loc.Code COLLATE DATABASE_DEFAULT = [Snapshot$NAV_Inventory].InvLocation
                                     ), '')
    WHERE   B4T_ItemNo = ''

    COMMIT TRANSACTION

/*** retrieve qty per unit of measure from B4T data ***/

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     QtyPerUnitOfMeasure = ( CAST(ISNULL(( SELECT    B4TPM.[point_rate]
                                                  FROM      [Book4Time_ETL].[dbo].[B4T_product_master] B4TPM
                                                  WHERE     ( B4TPM.product_id = [B4T_ItemNo] )
                                       --#1. D.Smith (this is believed no longer needed as no records fit this condition, but the filter causes an issue with products not sold as each)
                                                            AND ( ISNUMERIC(B4TPM.point_rate) = 1 )
                                                ), 1) AS DECIMAL(18, 6)) )
    WHERE   ( RunDate = @DateEnd )
            AND ( B4T_ItemNo <> '' )

    COMMIT TRANSACTION

/*** get extended quantity (qty per base unit of measure) ***/

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     OnHandQtyBaseDay = OnHandQtyDay * QtyPerUnitOfMeasure
    WHERE   RunDate = @DateEnd

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     OnHandQtyBasePriorYrDay = OnHandQtyPriorYrDay
            * QtyPerUnitOfMeasure
    WHERE   RunDate = @DateEnd

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     OnHandQtyBasePriorMonthEnd = OnHandQtyPriorMonthEnd
            * QtyPerUnitOfMeasure
    WHERE   RunDate = @DateEnd

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     OnHandQtyBasePriorYearEnd = OnHandQtyPriorYearEnd
            * QtyPerUnitOfMeasure
    WHERE   RunDate = @DateEnd

    COMMIT TRANSACTION

/*** retrieve cost from historical standard cost table ***/

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     StandardCost = ISNULL(( SELECT  StandardCost
                                    FROM    [Snapshot$NAV_InventoryStdCost] InvStdCost
                                    WHERE   ( InvStdCost.ItemNo COLLATE DATABASE_DEFAULT = NavItemNo )
                                            AND ( InvStdCost.RunDate = @DateEnd )
                                  ), 0)
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( NavItemNo <> '' )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     StandardCostPriorYrDay = ISNULL((
  /*** RunDate will be 1 day after the day of ***/ SELECT   StandardCost
                                                   FROM     [Snapshot$NAV_InventoryStdCost] InvStdCost
                                                   WHERE    ( InvStdCost.ItemNo COLLATE DATABASE_DEFAULT = NavItemNo )
                                                            AND ( InvStdCost.RunDate = DATEADD(DD,
                                                              1,
                                                              @DatePriorYrYesterday) )
                                            ), 0)
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( NavItemNo <> '' )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     StandardCostPriorMonthEnd = ISNULL(( SELECT StandardCost
                                                 FROM   [Snapshot$NAV_InventoryStdCost] InvStdCost
                                                 WHERE  ( InvStdCost.ItemNo COLLATE DATABASE_DEFAULT = NavItemNo )
                                                        AND ( InvStdCost.RunDate = DATEADD(DD,
                                                              1,
                                                              @DatePriorMonthEnd) )
                                               ), 0)
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( NavItemNo <> '' )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     StandardCostPriorYearEnd = ISNULL(( SELECT  StandardCost
                                                FROM    [Snapshot$NAV_InventoryStdCost] InvStdCost
                                                WHERE   ( InvStdCost.ItemNo COLLATE DATABASE_DEFAULT = NavItemNo )
                                                        AND ( InvStdCost.RunDate = DATEADD(DD,
                                                              1,
                                                              @DatePriorYrYearEnd) )
                                              ), 0)
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( NavItemNo <> '' )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

/*** update amount fields with standard cost * base qty ***/

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     OnHandAmtDay = ISNULL(OnHandQtyBaseDay * StandardCost, 0)
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     OnHandAmtPriorYrDay = ISNULL(OnHandQtyBasePriorYrDay
                                         * StandardCostPriorYrDay, 0)
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     OnHandAmtPriorMonthEnd = ISNULL(OnHandQtyBasePriorMonthEnd
                                            * StandardCostPriorMonthEnd, 0)
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     OnHandAmtPriorYearEnd = ISNULL(OnHandQtyBasePriorYearEnd
                                           * StandardCostPriorYearEnd, 0)
    WHERE   ( NavItemNo IS NOT NULL )
            AND ( RunDate = @DateEnd )

    COMMIT TRANSACTION

/*** update InvLocationType field, which is used to sort results in Inventory View ***/

    BEGIN TRANSACTION

    UPDATE  [Snapshot$NAV_Inventory]
    SET     InvLocationType = 'NAV'
    WHERE   ( RunDate = @DateEnd )
            AND ( InvLocation COLLATE DATABASE_DEFAULT IN (
                  SELECT    Code
                  FROM      [Jason Pharm$Location] ) )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     InvLocationType = 'MWCC'
    WHERE   ( RunDate = @DateEnd )
            AND ( InvLocationType IS NULL )

    COMMIT TRANSACTION

/*** forecasting fields ***/

/*** per Marc Kepler 2/20/12, forecasting date range to be Sunday + 4W ***/

    DECLARE @Date1 DATETIME

    IF ( DATEPART(WEEKDAY, @DateYesterday) = 1 )
        SET @Date1 = @DateYesterday
    ELSE
        SET @Date1 = DATEADD(DD, 8 - DATEPART(WEEKDAY, @DateYesterday),
                             @DateYesterday)

    UPDATE  [Snapshot$NAV_Inventory]
    SET     ForecastQtyCurrentMonth = ( SELECT  dbo.NavInvt_GetForecastQtyDateRange([Snapshot$NAV_Inventory].InvLocation,
                                                              [Snapshot$NAV_Inventory].NavItemNo,
                                                              @Date1,
                                                              DATEADD(DD, 27,
                                                              @Date1))
                                      )
    WHERE   ( RunDate = @DateEnd )
            AND ( NavItemNo IS NOT NULL )
            AND ( InvPostClass = 'FG' )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     [OnHandForecastDays] = ISNULL(OnHandQtyBaseDay
                                          / ( [ForecastQtyCurrentMonth] / 28 ),
                                          0)
    WHERE   ( RunDate = @DateEnd )
            AND ( [NavItemNo] IS NOT NULL )
            AND ( [ForecastQtyCurrentMonth] > 0 )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     [OnHandForecastDaysPriorYrDay] = ISNULL(OnHandQtyBasePriorYrDay
                                                    / ( [ForecastQtyCurrentMonth]
                                                        / 28 ), 0)
    WHERE   ( RunDate = @DateEnd )
            AND ( [NavItemNo] IS NOT NULL )
            AND ( [ForecastQtyCurrentMonth] > 0 )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     [OnHandForecastDaysPriorMonthEnd] = ISNULL(OnHandQtyBasePriorMonthEnd
                                                       / ( [ForecastQtyCurrentMonth]
                                                           / 28 ), 0)
    WHERE   ( RunDate = @DateEnd )
            AND ( [NavItemNo] IS NOT NULL )
            AND ( [ForecastQtyCurrentMonth] > 0 )

    UPDATE  [Snapshot$NAV_Inventory]
    SET     [OnHandForecastDaysPriorYearEnd] = ISNULL(OnHandQtyBasePriorYearEnd
                                                      / ( [ForecastQtyCurrentMonth]
                                                          / 28 ), 0)
    WHERE   ( RunDate = @DateEnd )
            AND ( [NavItemNo] IS NOT NULL )
            AND ( [ForecastQtyCurrentMonth] > 0 )

/****** REPORT DURATION OF QUERY ***********************************************/

    SET @ProcessTimeEnd = GETDATE()
    PRINT 'Elapsed time: '
    PRINT DATEDIFF(SS, @ProcessTimeBegin, @ProcessTimeEnd)
