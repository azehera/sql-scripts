﻿
CREATE PROCEDURE [dbo].[BI_NAV_DimGLCategory_Back] 
as
SET NOCOUNT ON

truncate table BI_SSAS_Cubes.dbo.DimGLCategory

Select distinct 
a.[G_L Account No_] COLLATE DATABASE_DEFAULT as [G_L Account No_]
,d.[Name] COLLATE DATABASE_DEFAULT as [G_L Name]
,Case When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '10000' and '11999' Then 'Cash and Cash Equivalents'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12001' and '12383' Then 'Short Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12501' and '12999' Then 'Affiliate Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13001' and '13499' Then 'Account Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13501' and '13748' Then 'Notes Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13750' and '13998' Then 'Non-Trade Accounts Receivables'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '14000' and '14998' Then 'Inventory'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '15000' and '15997' Then 'Pre-Paid Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '16000' and '16998' Then 'Long-Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17001' and '17007' Then 'Fixed Asset In Process'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17009' and '17098' Then 'Building and Land'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17100' and '17198' Then 'Computers'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17200' and '17298' Then 'Design And Development'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17300' and '17498' Then 'Furniture and Fixtures'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17500' and '17598' Then 'Leasehold Improvments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17600' and '17698' Then 'Machinery and Equipment'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17700' and '17798' Then 'Software and Website'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17800' and '17898' Then 'Vehicles'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17900' and '17997' Then 'Video'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '18000' and '18998' Then 'Intangile Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '19000' and '19997' Then 'Other Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '20003' and '20998' Then 'Accounts Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '21000' and '21998' Then 'Payroll Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '22000' and '22998' Then 'Current Portion of LT Debt'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '23000' and '24997' Then 'Affilate Payables'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '25000' and '29997' Then 'Non-Current Liabilities'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '30001' and '32998' Then 'Capital Stock'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '33000' and '33998' Then 'Additional Paid-In-Capital'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '34000' and '35998' Then 'Retained Earnings'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '36000' and '39994' Then 'Accum Other Comprehensive Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '39996' and '39997' Then 'Treasury Stock'

      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '40001' and '49998' Then 'Total Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '50001' and '59987' Then 'Total Cost of Goods Sold'
      When a.[G_L Account No_]  COLLATE DATABASE_DEFAULT Between '60001' and '64998' Then 'Total Direct Costs'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '65001' and '69996' Then 'Total Operating Costs'----CHANGED 65996 TO 69996 2012-06-20 LUC EMOND---
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '70002' and '74998' Then 'Total Personnel Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '75001' and '75998' Then 'Total Sales & Marketing Exp'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '76501' and '79998' Then 'Total Communication'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '80001' and '82998' Then 'Total Vehicle Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '83001' and '85998' Then 'Total Office Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '86001' and '89996' Then 'Total Other Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '91001' and '94996' Then 'Total Other Income' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '95001' and '99997' Then 'Total Other Expenses' 
      Else d.[Name] COLLATE DATABASE_DEFAULT End AS RollUpName
      ,Case When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '40000' and '64999' Then 'GrossProfit' Else 'SG&A' end As Category
From NAV_ETL.[dbo].[Jason Pharm$G_L Entry] a 
  Left Outer Join 
     NAV_ETL.[dbo].[Jason Pharm$G_L Account] d
     on a.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
     where  a.[G_L Account No_] >='10000'
and a.[Posting Date]>='2011-01-01'


union all

Select distinct 
a.[G_L Account No_] COLLATE DATABASE_DEFAULT  as [G_L Account No_]
,d.[Name] COLLATE DATABASE_DEFAULT as [G_L Name]
,Case When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '10000' and '11999' Then 'Cash and Cash Equivalents'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12001' and '12383' Then 'Short Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12501' and '12999' Then 'Affiliate Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13001' and '13499' Then 'Account Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13501' and '13748' Then 'Notes Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13750' and '13998' Then 'Non-Trade Accounts Receivables'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '14000' and '14998' Then 'Inventory'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '15000' and '15997' Then 'Pre-Paid Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '16000' and '16998' Then 'Long-Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17001' and '17007' Then 'Fixed Asset In Process'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17009' and '17098' Then 'Building and Land'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17100' and '17198' Then 'Computers'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17200' and '17298' Then 'Design And Development'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17300' and '17498' Then 'Furniture and Fixtures'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17500' and '17598' Then 'Leasehold Improvments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17600' and '17698' Then 'Machinery and Equipment'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17700' and '17798' Then 'Software and Website'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17800' and '17898' Then 'Vehicles'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17900' and '17997' Then 'Video'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '18000' and '18998' Then 'Intangile Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '19000' and '19997' Then 'Other Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '20003' and '20998' Then 'Accounts Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '21000' and '21998' Then 'Payroll Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '22000' and '22998' Then 'Current Portion of LT Debt'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '23000' and '24997' Then 'Affilate Payables'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '25000' and '29997' Then 'Non-Current Liabilities'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '30001' and '32998' Then 'Capital Stock'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '33000' and '33998' Then 'Additional Paid-In-Capital'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '34000' and '35998' Then 'Retained Earnings'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '36000' and '39994' Then 'Accum Other Comprehensive Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '39996' and '39997' Then 'Treasury Stock'

      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '40001' and '49998' Then 'Total Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '50001' and '59987' Then 'Total Cost of Goods Sold'
      When a.[G_L Account No_]  COLLATE DATABASE_DEFAULT Between '60001' and '64998' Then 'Total Direct Costs'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '65001' and '69996' Then 'Total Operating Costs'----CHANGED 65996 TO 69996 2012-06-20 LUC EMOND---
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '70002' and '74998' Then 'Total Personnel Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '75001' and '75998' Then 'Total Sales & Marketing Exp'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '76501' and '79998' Then 'Total Communication'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '80001' and '82998' Then 'Total Vehicle Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '83001' and '85998' Then 'Total Office Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '86001' and '89996' Then 'Total Other Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '91001' and '94996' Then 'Total Other Income' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '95001' and '99997' Then 'Total Other Expenses' 
      Else d.[Name] COLLATE DATABASE_DEFAULT End AS RollUpName
      ,Case When [G_L Account No_] COLLATE DATABASE_DEFAULT Between '40000' and '64999' Then 'GrossProfit' Else 'SG&A' end As Category
From  NAV_ETL.[dbo].[7 Crondall$G_L Entry] a 
  Left Outer Join 
      NAV_ETL.[dbo].[7 Crondall$G_L Account] d
     on a.[G_L Account No_]COLLATE DATABASE_DEFAULT = d.[No_]
where  a.[G_L Account No_] >='10000'
and a.[Posting Date]>='2011-01-01'
 --and [G_L Account No_] not in ('96750','43500')
and a.[G_L Account No_] not in (Select [G_L Account No_] COLLATE DATABASE_DEFAULT  from BI_SSAS_Cubes.dbo.DimGLCategory)


union all

Select
distinct a.[G_L Account No_] as [G_L Account No_]
,d.[Name] COLLATE DATABASE_DEFAULT as [G_L Name]
,Case When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '10000' and '11999' Then 'Cash and Cash Equivalents'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12001' and '12383' Then 'Short Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12501' and '12999' Then 'Affiliate Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13001' and '13499' Then 'Account Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13501' and '13748' Then 'Notes Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13750' and '13998' Then 'Non-Trade Accounts Receivables'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '14000' and '14998' Then 'Inventory'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '15000' and '15997' Then 'Pre-Paid Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '16000' and '16998' Then 'Long-Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17001' and '17007' Then 'Fixed Asset In Process'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17009' and '17098' Then 'Building and Land'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17100' and '17198' Then 'Computers'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17200' and '17298' Then 'Design And Development'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17300' and '17498' Then 'Furniture and Fixtures'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17500' and '17598' Then 'Leasehold Improvments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17600' and '17698' Then 'Machinery and Equipment'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17700' and '17798' Then 'Software and Website'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17800' and '17898' Then 'Vehicles'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17900' and '17997' Then 'Video'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '18000' and '18998' Then 'Intangile Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '19000' and '19997' Then 'Other Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '20003' and '20998' Then 'Accounts Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '21000' and '21998' Then 'Payroll Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '22000' and '22998' Then 'Current Portion of LT Debt'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '23000' and '24997' Then 'Affilate Payables'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '25000' and '29997' Then 'Non-Current Liabilities'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '30001' and '32998' Then 'Capital Stock'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '33000' and '33998' Then 'Additional Paid-In-Capital'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '34000' and '35998' Then 'Retained Earnings'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '36000' and '39994' Then 'Accum Other Comprehensive Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '39996' and '39997' Then 'Treasury Stock'

      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '40001' and '49998' Then 'Total Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '50001' and '59987' Then 'Total Cost of Goods Sold'
      When a.[G_L Account No_]  COLLATE DATABASE_DEFAULT Between '60001' and '64998' Then 'Total Direct Costs'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '65001' and '69996' Then 'Total Operating Costs'----CHANGED 65996 TO 69996 2012-06-20 LUC EMOND---
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '70002' and '74998' Then 'Total Personnel Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '75001' and '75998' Then 'Total Sales & Marketing Exp'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '76501' and '79998' Then 'Total Communication'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '80001' and '82998' Then 'Total Vehicle Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '83001' and '85998' Then 'Total Office Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '86001' and '89996' Then 'Total Other Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '91001' and '94996' Then 'Total Other Income' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '95001' and '99997' Then 'Total Other Expenses' 
      Else d.[Name] COLLATE DATABASE_DEFAULT End AS RollUpName
      ,Case When [G_L Account No_] Between '40000' and '64999' Then 'GrossProfit' Else 'SG&A' end As Category
 From NAV_ETL.[dbo].[Jason Enterprises$G_L Entry] a 
 Left Outer Join 
     NAV_ETL.[dbo].[Jason Enterprises$G_L Account] d
     on a.[G_L Account No_] COLLATE DATABASE_DEFAULT = d.[No_]
where  a.[G_L Account No_] >='10000'
and a.[Posting Date]>='2011-01-01'
and a.[G_L Account No_] not in (Select [G_L Account No_] COLLATE DATABASE_DEFAULT from BI_SSAS_Cubes.dbo.DimGLCategory)


union all

Select distinct a.[G_L Account No_] as [G_L Account No_]
,d.[Name] COLLATE DATABASE_DEFAULT as [G_L Name]
,Case When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '10000' and '11999' Then 'Cash and Cash Equivalents'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12001' and '12383' Then 'Short Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12501' and '12999' Then 'Affiliate Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13001' and '13499' Then 'Account Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13501' and '13748' Then 'Notes Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13750' and '13998' Then 'Non-Trade Accounts Receivables'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '14000' and '14998' Then 'Inventory'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '15000' and '15997' Then 'Pre-Paid Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '16000' and '16998' Then 'Long-Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17001' and '17007' Then 'Fixed Asset In Process'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17009' and '17098' Then 'Building and Land'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17100' and '17198' Then 'Computers'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17200' and '17298' Then 'Design And Development'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17300' and '17498' Then 'Furniture and Fixtures'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17500' and '17598' Then 'Leasehold Improvments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17600' and '17698' Then 'Machinery and Equipment'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17700' and '17798' Then 'Software and Website'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17800' and '17898' Then 'Vehicles'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17900' and '17997' Then 'Video'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '18000' and '18998' Then 'Intangile Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '19000' and '19997' Then 'Other Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '20003' and '20998' Then 'Accounts Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '21000' and '21998' Then 'Payroll Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '22000' and '22998' Then 'Current Portion of LT Debt'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '23000' and '24997' Then 'Affilate Payables'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '25000' and '29997' Then 'Non-Current Liabilities'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '30001' and '32998' Then 'Capital Stock'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '33000' and '33998' Then 'Additional Paid-In-Capital'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '34000' and '35998' Then 'Retained Earnings'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '36000' and '39994' Then 'Accum Other Comprehensive Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '39996' and '39997' Then 'Treasury Stock'

      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '40001' and '49998' Then 'Total Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '50001' and '59987' Then 'Total Cost of Goods Sold'
      When a.[G_L Account No_]  COLLATE DATABASE_DEFAULT Between '60001' and '64998' Then 'Total Direct Costs'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '65001' and '69996' Then 'Total Operating Costs'----CHANGED 65996 TO 69996 2012-06-20 LUC EMOND---
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '70002' and '74998' Then 'Total Personnel Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '75001' and '75998' Then 'Total Sales & Marketing Exp'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '76501' and '79998' Then 'Total Communication'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '80001' and '82998' Then 'Total Vehicle Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '83001' and '85998' Then 'Total Office Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '86001' and '89996' Then 'Total Other Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '91001' and '94996' Then 'Total Other Income' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '95001' and '99997' Then 'Total Other Expenses' 
      Else d.[Name] COLLATE DATABASE_DEFAULT End AS RollUpName
      ,Case When [G_L Account No_] Between '40000' and '64999' Then 'GrossProfit' Else 'SG&A' end As Category
From NAV_ETL.[dbo].[Jason Properties$G_L Entry] a 
 Left Outer Join 
     NAV_ETL.[dbo].[Jason Properties$G_L Account] d
     on a.[G_L Account No_] = d.[No_]
where  a.[G_L Account No_] >='10000'
and a.[Posting Date]>='2011-01-01'
--and [G_L Account No_] not in ('96750')
and a.[G_L Account No_] not in (Select [G_L Account No_] COLLATE DATABASE_DEFAULT from BI_SSAS_Cubes.dbo.DimGLCategory)


union all

Select distinct a.[G_L Account No_] as [G_L Account No_]
,d.[Name] COLLATE DATABASE_DEFAULT as [G_L Name]
,Case When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '10000' and '11999' Then 'Cash and Cash Equivalents'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12001' and '12383' Then 'Short Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12501' and '12999' Then 'Affiliate Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13001' and '13499' Then 'Account Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13501' and '13748' Then 'Notes Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13750' and '13998' Then 'Non-Trade Accounts Receivables'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '14000' and '14998' Then 'Inventory'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '15000' and '15997' Then 'Pre-Paid Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '16000' and '16998' Then 'Long-Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17001' and '17007' Then 'Fixed Asset In Process'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17009' and '17098' Then 'Building and Land'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17100' and '17198' Then 'Computers'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17200' and '17298' Then 'Design And Development'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17300' and '17498' Then 'Furniture and Fixtures'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17500' and '17598' Then 'Leasehold Improvments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17600' and '17698' Then 'Machinery and Equipment'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17700' and '17798' Then 'Software and Website'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17800' and '17898' Then 'Vehicles'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17900' and '17997' Then 'Video'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '18000' and '18998' Then 'Intangile Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '19000' and '19997' Then 'Other Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '20003' and '20998' Then 'Accounts Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '21000' and '21998' Then 'Payroll Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '22000' and '22998' Then 'Current Portion of LT Debt'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '23000' and '24997' Then 'Affilate Payables'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '25000' and '29997' Then 'Non-Current Liabilities'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '30001' and '32998' Then 'Capital Stock'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '33000' and '33998' Then 'Additional Paid-In-Capital'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '34000' and '35998' Then 'Retained Earnings'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '36000' and '39994' Then 'Accum Other Comprehensive Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '39996' and '39997' Then 'Treasury Stock'

      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '40001' and '49998' Then 'Total Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '50001' and '59987' Then 'Total Cost of Goods Sold'
      When a.[G_L Account No_]  COLLATE DATABASE_DEFAULT Between '60001' and '64998' Then 'Total Direct Costs'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '65001' and '69996' Then 'Total Operating Costs'----CHANGED 65996 TO 69996 2012-06-20 LUC EMOND---
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '70002' and '74998' Then 'Total Personnel Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '75001' and '75998' Then 'Total Sales & Marketing Exp'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '76501' and '79998' Then 'Total Communication'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '80001' and '82998' Then 'Total Vehicle Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '83001' and '85998' Then 'Total Office Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '86001' and '89996' Then 'Total Other Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '91001' and '94996' Then 'Total Other Income' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '95001' and '99997' Then 'Total Other Expenses' 
      Else d.[Name] COLLATE DATABASE_DEFAULT End AS RollUpName
      ,Case When [G_L Account No_] Between '40000' and '64999' Then 'GrossProfit' Else 'SG&A' end As Category
 From NAV_ETL.[dbo].[Medifast Inc$G_L Entry] a 
 Left Outer Join 
     NAV_ETL.[dbo].[Medifast Inc$G_L Account] d
     on a.[G_L Account No_] = d.[No_]
    
where  a.[G_L Account No_] >='10000'
and a.[Posting Date]>='2011-01-01'
--and [G_L Account No_] not in ('96750')
and a.[G_L Account No_] not in (Select [G_L Account No_] COLLATE DATABASE_DEFAULT from BI_SSAS_Cubes.dbo.DimGLCategory)


union all
Select distinct a.[G_L Account No_] as [G_L Account No_]
,d.[Name] COLLATE DATABASE_DEFAULT as [G_L Name]
,Case When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '10000' and '11999' Then 'Cash and Cash Equivalents'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12001' and '12383' Then 'Short Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '12501' and '12999' Then 'Affiliate Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13001' and '13499' Then 'Account Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13501' and '13748' Then 'Notes Receivables'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '13750' and '13998' Then 'Non-Trade Accounts Receivables'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '14000' and '14998' Then 'Inventory'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '15000' and '15997' Then 'Pre-Paid Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '16000' and '16998' Then 'Long-Term Investments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17001' and '17007' Then 'Fixed Asset In Process'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17009' and '17098' Then 'Building and Land'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17100' and '17198' Then 'Computers'
     
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17200' and '17298' Then 'Design And Development'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17300' and '17498' Then 'Furniture and Fixtures'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17500' and '17598' Then 'Leasehold Improvments'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17600' and '17698' Then 'Machinery and Equipment'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17700' and '17798' Then 'Software and Website'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17800' and '17898' Then 'Vehicles'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '17900' and '17997' Then 'Video'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '18000' and '18998' Then 'Intangile Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '19000' and '19997' Then 'Other Assets'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '20003' and '20998' Then 'Accounts Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '21000' and '21998' Then 'Payroll Payable'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '22000' and '22998' Then 'Current Portion of LT Debt'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '23000' and '24997' Then 'Affilate Payables'
          
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '25000' and '29997' Then 'Non-Current Liabilities'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '30001' and '32998' Then 'Capital Stock'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '33000' and '33998' Then 'Additional Paid-In-Capital'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '34000' and '35998' Then 'Retained Earnings'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '36000' and '39994' Then 'Accum Other Comprehensive Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '39996' and '39997' Then 'Treasury Stock'

      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '40001' and '49998' Then 'Total Income'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '50001' and '59987' Then 'Total Cost of Goods Sold'
      When a.[G_L Account No_]  COLLATE DATABASE_DEFAULT Between '60001' and '64998' Then 'Total Direct Costs'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '65001' and '69996' Then 'Total Operating Costs'----CHANGED 65996 TO 69996 2012-06-20 LUC EMOND---
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '70002' and '74998' Then 'Total Personnel Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '75001' and '75998' Then 'Total Sales & Marketing Exp'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '76501' and '79998' Then 'Total Communication'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '80001' and '82998' Then 'Total Vehicle Expenses'
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '83001' and '85998' Then 'Total Office Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT  Between '86001' and '89996' Then 'Total Other Expenses' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '91001' and '94996' Then 'Total Other Income' 
      When a.[G_L Account No_] COLLATE DATABASE_DEFAULT Between '95001' and '99997' Then 'Total Other Expenses' 
      Else d.[Name] COLLATE DATABASE_DEFAULT End AS RollUpName
      ,Case When [G_L Account No_] Between '40000' and '64999' Then 'GrossProfit' Else 'SG&A' end As Category
From NAV_ETL.[dbo].[Take Shape For Life$G_L Entry] a 
 Left Outer Join 
     NAV_ETL.[dbo].[Take Shape For Life$G_L Account] d
     on a.[G_L Account No_] = d.[No_]

where  a.[G_L Account No_] >='10000'
and a.[Posting Date]>='2011-01-01'
and a.[G_L Account No_] not in (Select [G_L Account No_] COLLATE DATABASE_DEFAULT from BI_SSAS_Cubes.dbo.DimGLCategory)


--;with CTE as (Select [G_L Account No_],
--[G_L Name],
--RollUpName,
--Category,
--Row_number() over (partition by [G_L Account No_] order by [G_L Account No_]) as no
--from BI_SSAS_Cubes.dbo.DimGLCategory)
--Select *   from CTE where no>1



INSERT into BI_SSAS_Cubes.dbo.DimGLCategory ([G_L Account No_],[G_L Name], [RollUpName], Category)
    VALUES ('70720', 'Commission Exp Med Provider', 'Total Personnel Expenses', 'SG&A'),
           ('74001', 'Meals- Travel/External', 'Total Personnel Expenses', 'SG&A'),
           ('75150', 'Advertising- Canada', 'Total Sales & Marketing Exp', 'SG&A'),
           ('76500', 'Call Center Services', 'Call Center Services', 'SG&A')










