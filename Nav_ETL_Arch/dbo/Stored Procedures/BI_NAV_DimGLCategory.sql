﻿
CREATE PROCEDURE [dbo].[BI_NAV_DimGLCategory]
AS
SET NOCOUNT ON

Declare @Delete integer
Set @Delete = (SELECT  [CalendarMonthIDGLCube] From BI_SSAS_Cubes.dbo.BudgetData_CalendarYear)

DECLARE @GLEntryAccounts TABLE (AccountNo varchar(20))

Insert Into @GLEntryAccounts
Select distinct e.[G_L Account No_] COLLATE DATABASE_DEFAULT From NAV_ETL.[dbo].[Jason Pharm$G_L Entry] e (NOLOCK) 
Where e.[G_L Account No_] >= '10000' and e.[Posting Date] >= '2011-01-01' OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct e.[G_L Account No_] COLLATE DATABASE_DEFAULT From NAV_ETL.[dbo].[7 Crondall$G_L Entry] e (NOLOCK)
Where e.[G_L Account No_] >= '10000' and e.[Posting Date] >= '2011-01-01' OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct e.[G_L Account No_] COLLATE DATABASE_DEFAULT From NAV_ETL.[dbo].[Jason Enterprises$G_L Entry] e (NOLOCK)
Where e.[G_L Account No_] >= '10000' and e.[Posting Date] >= '2011-01-01' OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct e.[G_L Account No_] COLLATE DATABASE_DEFAULT From NAV_ETL.[dbo].[Jason Properties$G_L Entry] e (NOLOCK)
Where e.[G_L Account No_] >= '10000' and e.[Posting Date] >= '2011-01-01' OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct e.[G_L Account No_] COLLATE DATABASE_DEFAULT From NAV_ETL.[dbo].[Medifast Inc$G_L Entry] e (NOLOCK)
Where e.[G_L Account No_] >= '10000' and e.[Posting Date] >= '2011-01-01' OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct e.[G_L Account No_] COLLATE DATABASE_DEFAULT From NAV_ETL.[dbo].[Take Shape For Life$G_L Entry] e (NOLOCK)
Where e.[G_L Account No_] >= '10000' and e.[Posting Date] >= '2011-01-01' OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct b.[G_L Account No_] From NAV_ETL.[dbo].[Jason Pharm$G_L Budget Entry] b (NOLOCK)
Left Join [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0, DATEDIFF(D, 0, b.[Date])) = c.CalendarDate
Where CalendarMonthID >= @Delete And ([Budget Name] = 'V2-12BUDGT' OR ([Budget Name] = 'BUDGET' AND YEAR(Date) > '2012')) OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct b.[G_L Account No_] From NAV_ETL.dbo.[7 Crondall$G_L Budget Entry] b (NOLOCK)
Left Join [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0, DATEDIFF(D, 0, b.[Date])) = c.CalendarDate 
Where CalendarMonthID >= @Delete And ([Budget Name] = 'V2-12BUDGT' OR ( [Budget Name] = 'BUDGET' AND YEAR(Date) > '2012')) OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct b.[G_L Account No_] From NAV_ETL.dbo.[Jason Enterprises$G_L Budget Entry] b (NOLOCK)
Left Join [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0, DATEDIFF(D, 0, b.[Date])) = c.CalendarDate
Where CalendarMonthID >= @Delete And ([Budget Name] = 'V2-12BUDGT' OR ([Budget Name] = 'BUDGET' AND YEAR(Date) > '2012')) OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct b.[G_L Account No_] From NAV_ETL.dbo.[Jason Properties$G_L Budget Entry] b (NOLOCK)
Left Join [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0, DATEDIFF(D, 0, b.[Date])) = c.CalendarDate
Where CalendarMonthID >= @Delete And ([Budget Name] = 'V2-12BUDGT' OR ([Budget Name] = 'BUDGET' AND YEAR(Date) > '2012')) OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct b.[G_L Account No_] From NAV_ETL.dbo.[Medifast Inc$G_L Budget Entry] b (NOLOCK)
Left Join [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0, DATEDIFF(D, 0, b.[Date])) = c.CalendarDate
Where CalendarMonthID >= @Delete And ([Budget Name] = 'V2-12BUDGT' OR ([Budget Name] = 'BUDGET' AND YEAR(Date) > '2012')) OPTION (MAXDOP 1)

Insert Into @GLEntryAccounts
Select distinct b.[G_L Account No_] From NAV_ETL.dbo.[Take Shape For Life$G_L Budget Entry] b (NOLOCK)
Left Join [BI_SSAS_Cubes].[dbo].[DimCalendar] c ON DATEADD(D, 0, DATEDIFF(D, 0, b.[Date])) = c.CalendarDate
Where CalendarMonthID >= @Delete And ([Budget Name] = 'V2-12BUDGT' OR ( [Budget Name] = 'BUDGET' AND YEAR(Date) > '2012')) OPTION (MAXDOP 1)

DECLARE @GLAccounts TABLE (AccountNo varchar(20), AccountName varchar(50))

Insert Into @GLAccounts
Select a.[No_], a.[Name] from NAV_ETL.[dbo].[Jason Pharm$G_L Account] a left join @GLAccounts t on a.[No_] COLLATE DATABASE_DEFAULT = t.AccountNo where t.AccountNo IS NULL
Insert Into @GLAccounts
Select a.[No_], a.[Name] from NAV_ETL.[dbo].[7 Crondall$G_L Account] a left join @GLAccounts t on a.[No_] COLLATE DATABASE_DEFAULT = t.AccountNo where t.AccountNo IS NULL
Insert Into @GLAccounts
Select a.[No_], a.[Name] from NAV_ETL.[dbo].[Jason Enterprises$G_L Account] a left join @GLAccounts t on a.[No_] COLLATE DATABASE_DEFAULT = t.AccountNo where t.AccountNo IS NULL
Insert Into @GLAccounts
Select a.[No_], a.[Name] from NAV_ETL.[dbo].[Jason Properties$G_L Account] a left join @GLAccounts t on a.[No_] COLLATE DATABASE_DEFAULT = t.AccountNo where t.AccountNo IS NULL
Insert Into @GLAccounts
Select a.[No_], a.[Name] from NAV_ETL.[dbo].[Medifast Inc$G_L Account] a left join @GLAccounts t on a.[No_] COLLATE DATABASE_DEFAULT = t.AccountNo where t.AccountNo IS NULL
Insert Into @GLAccounts
Select a.[No_], a.[Name] from NAV_ETL.[dbo].[Take Shape For Life$G_L Account] a left join @GLAccounts t on a.[No_] COLLATE DATABASE_DEFAULT = t.AccountNo where t.AccountNo IS NULL

Truncate table BI_SSAS_Cubes.dbo.DimGLCategory

--Insert into BI_SSAS_Cubes.dbo.DimGLCategory -- Comment this line when using with SSIS package

Select distinct a.AccountNo as [G_L Account No_], a.AccountName as [G_L Name],
Case When a.AccountNo Between '10000' and '11999' Then 'Cash and Cash Equivalents'
     When a.AccountNo Between '12001' and '12383' Then 'Short Term Investments'
     When a.AccountNo Between '12501' and '12999' Then 'Affiliate Receivables'
     When a.AccountNo Between '13001' and '13499' Then 'Account Receivables'
     When a.AccountNo Between '13501' and '13748' Then 'Notes Receivables'
     When a.AccountNo Between '13750' and '13998' Then 'Non-Trade Accounts Receivables'
     
     When a.AccountNo Between '14000' and '14998' Then 'Inventory'
     When a.AccountNo Between '15000' and '15997' Then 'Pre-Paid Expenses'
     When a.AccountNo Between '16000' and '16998' Then 'Long-Term Investments'
     When a.AccountNo Between '17001' and '17007' Then 'Fixed Asset In Process'
     When a.AccountNo Between '17009' and '17098' Then 'Building and Land'
     When a.AccountNo Between '17100' and '17198' Then 'Computers'
     
     When a.AccountNo Between '17200' and '17298' Then 'Design And Development'
     When a.AccountNo Between '17300' and '17498' Then 'Furniture and Fixtures'
     When a.AccountNo Between '17500' and '17598' Then 'Leasehold Improvments'
     When a.AccountNo Between '17600' and '17698' Then 'Machinery and Equipment'
     When a.AccountNo Between '17700' and '17798' Then 'Software and Website'
     When a.AccountNo Between '17800' and '17898' Then 'Vehicles'
     When a.AccountNo Between '17900' and '17997' Then 'Video'
          
     When a.AccountNo Between '18000' and '18998' Then 'Intangile Assets'
     When a.AccountNo Between '19000' and '19997' Then 'Other Assets'
     When a.AccountNo Between '20003' and '20998' Then 'Accounts Payable'
     When a.AccountNo Between '21000' and '21998' Then 'Payroll Payable'
     When a.AccountNo Between '22000' and '22998' Then 'Current Portion of LT Debt'
     When a.AccountNo Between '23000' and '24997' Then 'Affilate Payables'
          
     When a.AccountNo Between '25000' and '29997' Then 'Non-Current Liabilities'
     When a.AccountNo Between '30001' and '32998' Then 'Capital Stock'
     When a.AccountNo Between '33000' and '33998' Then 'Additional Paid-In-Capital'
     When a.AccountNo Between '34000' and '35998' Then 'Retained Earnings'
     When a.AccountNo Between '36000' and '39994' Then 'Accum Other Comprehensive Income'
     When a.AccountNo Between '39996' and '39997' Then 'Treasury Stock'

     When a.AccountNo Between '40001' and '49998' Then 'Total Income'
     When a.AccountNo Between '50001' and '59980' Then 'Total Cost of Goods Sold'
     When a.AccountNo Between '60001' and '64998' Then 'Total Direct Costs'
     When a.AccountNo Between '65001' and '69996' Then 'Total Other Cost of Sales'
     When a.AccountNo Between '70002' and '74998' Then 'Total Personnel Expenses'
     When a.AccountNo Between '75001' and '75998' Then 'Total Sales & Marketing Exp'
     When a.AccountNo Between '76500' and '79998' Then 'Total Communication'
     When a.AccountNo Between '80001' and '82998' Then 'Total Vehicle Expenses'
     When a.AccountNo Between '83001' and '85998' Then 'Total Office Expenses'
     
     When a.AccountNo Between '86001' and '89996' and NOT a.AccountNo in ('86201') Then 'Total Other Expenses - SG&A'
     
     When a.AccountNo Between '95000' and '99997' and NOT a.AccountNo in ('96750','96800','97002','98000') Then 'Total Other Expenses - SG&A'
     
     When a.AccountNo in ('86201','96750','96800','97002','98000') Then 'Total Other Expenses - Other'
     When a.AccountNo in ('91000','91050','91400','91500','92000','92001','93000','94005',
						  '94010','94020','94025','94100','94105') Then 'Total Other Income'

     Else 'Unassigned' End AS RollUpName,
     
Case When a.AccountNo Between '10000' and '39999' Then 'BalSheet'
     When a.AccountNo Between '40000' and '59988' Then 'GrossProfit'
     When a.AccountNo Between '60000' and '69997' Then 'OperExpense'
     When a.AccountNo Between '70000' and '86200' Then 'SG&A'
     When a.AccountNo Between '86201' and '86201' Then 'Other'
     When a.AccountNo Between '86202' and '89997' Then 'SG&A'
     When a.AccountNo Between '90000' and '99998' Then 'Other'
     Else 'Unassigned' End As Category
     
From @GLEntryAccounts e left join @GLAccounts a on e.AccountNo = a.AccountNo
