﻿
create PROCEDURE usp_InsertDailyExchangeRate
@ExchangeRate DECIMAL(18,6), @CurrencyCode NVARCHAR(3)
AS

/*************************************************
Develoepr: D. Smith
Date: 2/4/2014
Desc: Used to inster new exchange rate to be used for
conversion in reporting.

*************************************************/


--Clear the previously insert rate for today:

DELETE FROM dbo.[Currency Exchange Rate] WHERE [Insert Date] = (dateadd(day,datediff(day,'19000101',getdate()),'19000101'))

--Insert Today's Rate

INSERT INTO dbo.[Currency Exchange Rate]
        ( [Insert Date] ,
          [Currency Code] ,
          [Exchange Rate]
        )
VALUES  ( (dateadd(day,datediff(day,'19000101',getdate()),'19000101')) , -- Insert Date - smalldatetime
          @CurrencyCode , -- Currency Code - nvarchar(3)
          @ExchangeRate  -- Exchange Rate - numeric
        )