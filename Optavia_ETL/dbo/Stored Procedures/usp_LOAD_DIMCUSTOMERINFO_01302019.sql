﻿

/***************************************************************************
Object: DIM_CUSTOMER_INFO
Date: 10/27/2017
Developer(s): Micah W.
Description: Load DIM_CUSTOMER_INFO from various sources
=============================================================================
MODIFICATIONS:
Micah Williams 02-26-2018  - Added Cast As Date to Adjusted Start Date and removed the customertype=1 filter for adjusted start date
Micah Williams 03-08-2018 - Added filter for DATE_LAST_KIT_PURCHASE for when...WHERE ORH_TYPE_CD = 'ORD'
Micah Williams 03-23-2018 - Removed  WHERE  CustomerTypeID = 1  filter for  DATE_LAST_KIT_PURCHASE
Micah Williams 03-29-2018 - add filter to remove Medifast_Customer_Number = '5'
Micah Williams 05-10-2018 - include Active Earning Coach in the Optavia Cube Population (since Dec 2017)
Micah Williams 06-09-2018 - Remove customers with CustomerStatusID = 0 (delted status)..Keeping deleted status customers caused duplicates.
Micah Williams 06-18-2018  - Removing Test Account 777
Micah W 10-03-2018 - Removing Test Account 111
=============================================================================

******************************************************************************/

create procedure [dbo].[usp_LOAD_DIMCUSTOMERINFO_01302019] 

as

--- Get [DATE_LAST_KIT_PURCHASE]
IF OBJECT_ID('Tempdb..#DLKP') IS NOT NULL DROP TABLE #DLKP
SELECT  UA.USA_CUST_NBR [CUSTOMER_NUMBER] ,
        CAST(OH.ORH_CREATE_DT AS DATE) [DATE_LAST_KIT_PURCHASE],
		ROW_NUMBER() OVER ( PARTITION BY UA.USA_CUST_NBR ORDER BY OH.ORH_CREATE_DT DESC ) AS [RowNumber]
INTO #DLKP
FROM    [prdmartini_STORE_repl].[dbo].[V_ORDER_HEADER] OH WITH ( NOLOCK )
        JOIN [prdmartini_STORE_repl].[dbo].ORDER_LINE OL WITH ( NOLOCK ) ON ORL_ORH_ID = ORH_ID
        JOIN [prdmartini_STORE_repl].[dbo].[USER_ACCOUNT] UA WITH ( NOLOCK ) ON OH.[ORH_CREATED_FOR] = UA.[USA_ID]
		AND UA.USA_CUST_NBR IN ( SELECT Field1
                                 FROM   EXIGO_ETL.dbo.Customers
                                 --WHERE  CustomerTypeID = 1 
								 )
        JOIN [prdmartini_MAIN_repl].[dbo].SKU s ON s.SKU_ID = OL.ORL_SKU_ID AND s.SKU_CODE IN ( '31011_KT', '31012_KT', '32051_KT', '31100_KT',
                        '31105_KT', '31110_KT' )
WHERE ORH_TYPE_CD = 'ORD'


IF OBJECT_ID('Tempdb..#DLKP_final') IS NOT NULL DROP TABLE #DLKP_final
SELECT *
INTO #DLKP_final
FROM #DLKP
WHERE rowNumber = '1'

--Adjusted Start Date
IF OBJECT_ID('Tempdb..#DAS') IS NOT NULL DROP TABLE #DAS
SELECT  C.Field1 [CUSTOMER_NUMBER] ,
        CASE WHEN [DATE_ADJUSTED_START] IS NOT NULL THEN [DATE_ADJUSTED_START]
             ELSE CAST(C.Date2 AS DATE)   END AS [DATE_ADJUSTED_START]   ---Added CAST(C.Date2 AS DATE)  02-26-2018
INTO #DAS
FROM    OPTAVIA_CUBE.[dbo].[Historic_DATE_ADJUSTED_START] H
        RIGHT JOIN EXIGO_ETL.dbo.Customers C ON C.Field1 = H.[CUSTOMER_NUMBER]
WHERE   --C.CustomerTypeID = '1'                                             --Removed this filter 02-28-2018..We want the adjusted start date for coaches and clients
        --AND 
		C.Field1 != ''
		AND C.CustomerStatusID != '0' --added 06-09-2018 Micah W.;


--Get Customer from Historic Coaches
IF OBJECT_ID('Tempdb..#ExHist') IS NOT NULL DROP TABLE #ExHist
SELECT DISTINCT MedifastCustomerNumber 
INTO #ExHist
FROM EXIGO.ExigoSyncSQL_PRD.CommissionContext.PeriodVolumesCustomers
WHERE CustomerTypeID = '1'

--Get ACTIVE Eanring Customers
IF OBJECT_ID('Tempdb..#ExAE') IS NOT NULL DROP TABLE #ExAE
SELECT DISTINCT CustomerID
INTO #ExAE
FROM EXIGO_ETL.dbo.PeriodVolumes
WHERE volume130 = 1.00


--Get Coach List
IF OBJECT_ID('Tempdb..#CL') IS NOT NULL DROP TABLE #CL
SELECT TV.Medifast_Customer_Number
INTO #CL
FROM Exigo_ETL.dbo.V_Tree_View TV
WHERE TV.COMMISSION_PERIOD = CAST(EOMONTH(GETDATE()) AS DATE) AND TV.CustomerTypeID = '1'
 

---Populate Work Table
TRUNCATE TABLE [OPTAVIA_CUBE].[dbo].DIM_CUSTOMER_INFO_work

insert INTO [OPTAVIA_CUBE].[dbo].DIM_CUSTOMER_INFO_work
SELECT  DISTINCT Field1 [CUSTOMER_NUMBER] ,
        CustomerID [EXIGO_CUSTOMER_NUMBER] ,
        FirstName [NAME_FIRST] ,
        LastName [NAME_LAST] ,
        FirstName + ' ' + LastName [NAME_FULL] ,
        C.MainAddress1 [ADDRESS1] ,
        C.MainCity [ADDRESS_CITY] ,
        C.MainCounty [ADDRESS_COUNTY] ,
        C.MainState [ADDRESS_STATE] ,
        ISNULL(LEFT(C.MainZip,5),0) [ADDRESS_ZIP_CODE] ,
        C.MainCountry [ADDRESS_COUNTRY] ,
        NULL [DATE_TERMI NATION] ,
        Date3 [DATE_REVERSION] ,
        Date4 [DATE_REINSTATEMENT] ,
        Date1 [DATE_ENTRY] ,
        NULL [DATE_CERTIFIED] , --Rachana
        Date5 [DATE_RENEWAL] ,
        D.DATE_LAST_KIT_PURCHASE , 
        DAS.[DATE_ADJUSTED_START]  , 
        ISNULL(DATEDIFF(MONTH,DAS.[DATE_ADJUSTED_START],CAST(Cast (DATEADD(DAY, -(DAY(GETDATE())), GETDATE()) AS DATE) AS DATE)),0) [COACH_LONGEVITY] , 
		MONTH([DATE_ADJUSTED_START]) [DRAFT_MONTH],
	    YEAR([DATE_ADJUSTED_START]) [DRAFT_YEAR],
        NULL [W9_COMPLETE] , --Rachana
        NULL [W9_COMPLETE_DATE] , -- Rachana
        C.Phone [MAIN_PHONE] ,
        C.Email [EMAIL1_ADDRESS]

FROM    EXIGO_ETL.dbo.Customers C
        LEFT JOIN #DLKP_final D ON D.[CUSTOMER_NUMBER] = C.Field1
		LEFT JOIN #DAS DAS ON DAS.[CUSTOMER_NUMBER] = C.Field1
WHERE   Field1 != ''
        AND (C.Field1 IN (SELECT Medifast_Customer_Number FROM #CL) 
		OR C.Field1 IN (SELECT MedifastCustomerNumber FROM #ExHist)
		OR C.CustomerID IN (SELECT  CustomerID	FROM #ExAE))
		AND C.Field1 NOT IN (SELECT C.Field1 FROM EXIGO_ETL.dbo.Customers C WHERE C.CustomerStatusID = '0') --added 06-09-2018 Micah W.
		AND C.Field1 NOT IN ('5','777','111') --added 03-29-2018, 06-18-2018,10-03-2018


------Union Exigo Data with TSFL cube data
IF OBJECT_ID('Tempdb..#prep1') IS NOT NULL DROP TABLE #prep1
select [CUSTOMER_NUMBER]
      ,[EXIGO_CUSTOMER_NUMBER]
      ,[NAME_FIRST]
      ,[NAME_LAST]
      ,[NAME_FULL]
      ,[ADDRESS1]
      ,[ADDRESS_CITY]
      ,[ADDRESS_COUNTY]
      ,[ADDRESS_STATE]
      ,[ADDRESS_ZIP_CODE]
      ,[ADDRESS_COUNTRY]
      ,[DATE_TERMINATION]
      ,[DATE_REVERSION]
      ,[DATE_REINSTATEMENT]
     ,[DATE_ENTRY]
      ,[DATE_CERTIFIED]
      ,[DATE_RENEWAL]
      ,[DATE_LAST_KIT_PURCHASE]
      ,[DATE_ADJUSTED_START]
      ,[COACH_LONGEVITY]
      ,[DRAFT_MONTH]
      ,[DRAFT_YEAR]
      ,[W9_COMPLETE]
      ,[W9_COMPLETE_DATE]
      ,[MAIN_PHONE]
      ,[EMAIL1_ADDRESS]
	  ,'exigo' as 'source'
into #prep1
from dbo.DIM_CUSTOMER_INFO_work

union 

select [CUSTOMER_NUMBER]
      ,[EXIGO_CUSTOMER_NUMBER]
      ,[NAME_FIRST]
      ,[NAME_LAST]
      ,[NAME_FULL]
      ,[ADDRESS1]
      ,[ADDRESS_CITY]
      ,[ADDRESS_COUNTY]
      ,[ADDRESS_STATE]
      ,[ADDRESS_ZIP_CODE]
      ,[ADDRESS_COUNTRY]
      ,[DATE_TERMINATION]
      ,[DATE_REVERSION]
      ,[DATE_REINSTATEMENT]
     ,[DATE_ENTRY]
      ,[DATE_CERTIFIED]
      ,[DATE_RENEWAL]
      ,[DATE_LAST_KIT_PURCHASE]
      ,[DATE_ADJUSTED_START]
      ,[COACH_LONGEVITY]
      ,[DRAFT_MONTH]
      ,[DRAFT_YEAR]
      ,[W9_COMPLETE]
      ,[W9_COMPLETE_DATE]
      ,[MAIN_PHONE]
      ,[EMAIL1_ADDRESS]
	  ,'tsfl_cube' as 'source'
from [dbo].[Historic_DIM_CUSTOMER_INFO]




if object_id('Tempdb..#prep2') is not null drop table #prep2
select * ,
row_number() over ( partition by CUSTOMER_NUMBER order by source asc ) as [RowNumber]
into #prep2
from #prep1


--Select All 
truncate table OPTAVIA_CUBE.dbo.DIM_CUSTOMER_INFO

insert into [OPTAVIA_CUBE].[dbo].DIM_CUSTOMER_INFO
select  [CUSTOMER_NUMBER] ,
        isnull([EXIGO_CUSTOMER_NUMBER],'') ,
        isnull([NAME_FIRST],'Unknown') ,
        isnull([NAME_LAST],'Unknown') ,
        isnull([NAME_FULL],'Unknown') ,
        isnull([ADDRESS1],'Unknown') ,
        isnull([ADDRESS_CITY],'Unknown') ,
        isnull([ADDRESS_COUNTY],'Unknown') ,
        isnull([ADDRESS_STATE],'Unknown') ,
        isnull([ADDRESS_ZIP_CODE],'Unknown') ,
        isnull([ADDRESS_COUNTRY],'Unknown') ,
        [DATE_TERMINATION] ,
        [DATE_REVERSION] ,
        [DATE_REINSTATEMENT] ,
        [DATE_ENTRY] ,
        [DATE_CERTIFIED] ,
        [DATE_RENEWAL] ,
        [DATE_LAST_KIT_PURCHASE] ,
        [DATE_ADJUSTED_START] ,
        [COACH_LONGEVITY] ,
        [DRAFT_MONTH] ,
        [DRAFT_YEAR] ,
        isnull([W9_COMPLETE],'') ,
        [W9_COMPLETE_DATE] ,
        isnull([MAIN_PHONE],'') ,
        isnull([EMAIL1_ADDRESS],''),
		case when C.Field3 = '1' then 1
		else 0 end as IS_CERTIFIED,
		case when C.field5 != '' then 1
		else 0 end  as IS_HEALTHPROFESSIONAL
from    #prep2 P
left join EXIGO_ETL.dbo.Customers C on P.CUSTOMER_NUMBER = C.Field1
where   RowNumber = '1'
and [CUSTOMER_NUMBER] not in ('5','777','111') --added 03-28-2018, 06-18-2018, 10-03-2018
--AND C.CustomerStatusID != '0' --Micah Williams 06-09-2018
