﻿


/***************************************************************************
Object: [[usp_LOAD_FACT_OPTAVIA]]
Date: 10/30/2017
Developer(s): Micah W.
Description: [usp_LOAD_FACT_OPTAVIA] from various sources
=============================================================================
MODIFICATIONS:
Micah W 01-04-2017 - Added condition to @closedPeriod CHeck ....AND PeriodTypeID = '1'
Micah W 01-30-2018 - replaced filter c.customertypeID = '1'  to   cast(pv.Volume108 as int) = '1'
Micah W 03-15-2018 - Add Rank_ID to the Fact table (ticket 21539478)
Micah W 03-29-2018 - add filter to remove Medifast_Customer_Number = '5'
Micah W 05-10-2018 - include Active Earning Coach in the Optavia Cube Population (since Dec 2017)
Micah W 06-09-2018 - Remove customers with CustomerStatusID = 0 (delted status)..Keeping deleted status customers caused duplicates.
Micah W 06-18-2018 - Removing Test Account 777
Micah W 06-18-2018 - Removing Test Account 111
Micah W 11-14-2018 - Add Column for [Ordering Entities] Work Front Ticket 21548477
Micah W 11-14-2018 - Add Coach Bonus Columns Ticket 21547381
Micah W 02-18-2019 - Fix Join for bonus data ITSD-1266
Micah W 02-28-2019 - Add Column for [Qualifying_Points]
Micah W 04-27-2019 - Removed Test Customer 20000224217,20000231249 This is a SG/HK Test customers
 
=============================================================================

******************************************************************************/

CREATE PROCEDURE [dbo].[usp_LOAD_FACT_OPTAVIA] 

@cp DATE

AS

		EXEC [usp_LOAD_DIM_BONUS] --This SP populates the bonus table, the fact table get populated from that table


		delete FROM OPTAVIA_CUBE.dbo.FACT_OPTAVIA 
		WHERE COMMISSION_PERIOD >=  '11-30-2017' 
		AND COMMISSION_PERIOD >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE) --Last Day of previous month

		INSERT INTO OPTAVIA_CUBE.dbo.FACT_OPTAVIA(
		CUSTOMER_NUMBER,
		COMMISSION_PERIOD,
		PV,
		FLV,
		GV,
		[PCV],-----https://medifast.atlassian.net/browse/ITSD-8209
		[FCV],-----https://medifast.atlassian.net/browse/ITSD-8209
		[GCV],-----https://medifast.atlassian.net/browse/ITSD-8209
		NEW_HEALTH_COACHES,
		NEW_CLIENTS,
		TOTAL_COMMISSIONS,
		SC_COUNT,
		ED_COUNT,
		FIBC_COUNT,
		FIBL_COUNT,
		RANK_ID,
		Ordering_Entities,
		First_Bonus,
        Certification_Bonus,
        Client_Support_Bonus_Monthly,
        Team_Growth_Bonus,
        Executive_Director_Generation_Bonus,
        National_Elite_Leadership_Bonus,
		Global_Elite_Leadership_Bonus,
		Presidential_Elite_Leadership_Bonus,
		Health_Coach_Consistency_Bonus,
		FIBC_Consistency_Bonus,
		Monthly_Adjustments,
		Business_Builder_Bonus,
		Client_Support_Bonus_Weekly,
		Client_Acquisition_Bonus,
		Assist_Bonus,
		Weekly_Adjustments,
		Qualifying_Points)


		SELECT DISTINCT C.Field1 AS CUSTOMER_NUMBER,
		CAST(P.EndDate AS DATE) AS COMMISSION_PERIOD,
		PV.Volume1 AS [PV],
		PV.Volume2 AS [FLV],
		PV.Volume3 AS [GLV],
		PV.Volume158 AS [PCV],-----https://medifast.atlassian.net/browse/ITSD-8209
		PV.Volume159 AS [FCV],-----https://medifast.atlassian.net/browse/ITSD-8209
		PV.Volume160 AS [GCV],-----https://medifast.atlassian.net/browse/ITSD-8209
		PV.volume113 AS [NewHealthCoaches],
		PV.volume114 AS NewClients,
		PV.Volume112 AS [TOTAL_Commissions],
		PV.volume31 AS [SC_Count],
		PV.volume32 AS [ED_Count],
		PV.volume33 AS [FibcCount],
		PV. Volume34 AS [FiblCount],
		PV.PaidRankID AS [Rank_ID],
		PV.volume35 AS [Ordering Entities],
		DB.[First_Bonus],
        DB.[Certification_Bonus],
        DB.[Client_Support_Bonus_Monthly],
        DB.[Team_Growth_Bonus],
        DB.[Executive_Director_Generation_Bonus],
        DB.[National_Elite_Leadership_Bonus],
		DB.[Global_Elite_Leadership_Bonus],
		DB.[Presidential_Elite_Leadership_Bonus],
		DB.[Health_Coach_Consistency_Bonus],
		DB.[FIBC_Consistency_Bonus],
		DB.[Monthly_Adjustments],
		DB.[Business_Builder_Bonus],
		DB.[Client_Support_Bonus_Weekly],
		DB.[Client_Acquisition_Bonus],
		DB.[Assist_Bonus],
		DB.[Weekly_Adjustments],
		PV.Volume4 AS [Qualifying_Points]
		FROM Exigo_ETL.dbo.Customers C 
		JOIN Exigo_ETL.dbo. PeriodVolumes PV ON PV.CustomerID = C.CustomerID
		JOIN Exigo_ETL.dbo.Periods P ON P.PeriodID = PV.PeriodID AND P.PeriodTypeID = PV.PeriodTypeID
		LEFT JOIN dbo.DIM_BONUS DB ON CAST(P.EndDate AS DATE) = DB.COMMISSION_PERIOD AND C.Field1 = DB.CUSTOMER_NUMBER
		WHERE (CAST(pv.Volume108 AS INT) = '1' OR CAST(volume130 AS INT) ='1')
		AND P.PeriodTypeID = '1' AND CAST(P.EndDate AS DATE) >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE)
		AND Field1 != '' 
		--AND C.Field1 = '2002'
		AND C.Field1 NOT IN ('5','777', '111', '20000224217','20000231249') --added 03-29-2018, 06-18-2018, 111 01-03-2018
		-- 04-27-2019 Removed Test Customer 20000224217,20000231249 This is a SG Test csutomer
		AND C.Field1 NOT IN (SELECT C.Field1 FROM EXIGO_ETL.dbo.Customers C WHERE C.CustomerStatusID = '0') --added 06-09-2018 Micah W.


		--print CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE)
		--PRINT CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-2, -1) AS DATE)
