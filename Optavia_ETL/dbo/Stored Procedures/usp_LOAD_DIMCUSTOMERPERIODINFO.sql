﻿
/***************************************************************************
Object: DIM_CUSTOMER_PERIOD_INFO
Date: 10/27/2017
Developer(s): Micah W.
Description: Load DIM_CUSTOMER_PERIOD_INFO from various sources
=============================================================================
MODIFICATIONS:
01-04-2018 Micah W -  Added condition to @closedPeriod CHeck ....AND PeriodTypeID = '1'
03-05-2018 Micah W -  Added Cast As Date to Adjusted Start Date and removed the customertype=1 filter for adjusted start date
03-05-2018 Micah W -  Added volume25 AS 'isFIBC', volume28 AS 'isFIBL'
03-29-2018 Micah W -  add filter to remove Medifast_Customer_Number = '5'
04-09-2018 Micah W	  Highest Achieved Rank now coming from VB13
05-10-2018 Micah W  - include Active Earning Coach in the Optavia Cube Population (since Dec 2017)
06-09-2018 Micah W  - Remove customers with CustomerStatusID = 0 (delted status)..Keeping deleted status customers caused duplicates.
06-18-2018 Micah W  - Removing Test Account 777
07-02-2018 Micah W. - Remove customers with CustomerStatusID = 0 (delted status) to another part of the query
10-03-2018 Micah W  - Removing Test Account 111
02-04-2019 Micah W  - Made Coach Longevity Adjustments to run against the new logic for adjusted srart date..work front ticket 21563177 
Micah W 04-27-2019  - Removed Test Customer 20000224217,20000231249 This is a SG/HK Test customers
Ronak S 10-27-2019  - Made Change in  Last Name to handle empty string values
=============================================================================

******************************************************************************/

CREATE PROCEDURE [dbo].[usp_LOAD_DIMCUSTOMERPERIODINFO] 



AS


	---------Load [dbo].[DIM_CUSTOMER_PERIOD_INFO] from Exigo---------------

	DELETE FROM [dbo].[DIM_CUSTOMER_PERIOD_INFO]
	WHERE COMMISSION_PERIOD >=  '11-30-2017' 
		AND COMMISSION_PERIOD >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE) --Last Day of previous month
	


	--Get Previous Month's Rank
	IF OBJECT_ID('Tempdb..#PrevRank') IS NOT NULL DROP TABLE #PrevRank
	SELECT Medifast_Customer_Number, CURRENT_RANK AS 'PaidRankID',CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, COMMISSION_PERIOD)+1, -1) AS DATE) AS COMMISSION_PERIOD
	INTO #PrevRank
	FROM EXIGO_ETL.dbo.V_Tree_View
	WHEre COMMISSION_PERIOD >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-2, -1) AS DATE) AND COMMISSION_PERIOD != CAST(EOMONTH(GETDATE()) AS DATE)
	AND Medifast_Customer_Number IN (SELECT Field1 FROM EXIGO_ETL.dbo.Customers WHERE CustomerTypeID = 1 AND Field1 != '')
	AND CustomerStatusID != '0' --added 07-02-2018



	--Adjusted Start Date
	--IF OBJECT_ID('Tempdb..#DAS') IS NOT NULL DROP TABLE #DAS
	--SELECT  C.Field1 [CUSTOMER_NUMBER] ,
	--		CASE WHEN [DATE_ADJUSTED_START] IS NOT NULL THEN [DATE_ADJUSTED_START]
	--			 ELSE CAST(C.Date2 AS DATE)   END AS [DATE_ADJUSTED_START]
	--INTO #DAS
	--FROM    OPTAVIA_CUBE.[dbo].[Historic_DATE_ADJUSTED_START] H
	--		RIGHT JOIN EXIGO_ETL.dbo.Customers C ON C.Field1 = H.[CUSTOMER_NUMBER]
	--WHERE   --C.CustomerTypeID = '1'
	--		--AND 
	--		C.Field1 != ''
	--		AND C.CustomerStatusID !='0' --added 06-09-2018;

	IF OBJECT_ID('Tempdb..#DAS') IS NOT NULL DROP TABLE #DAS
	SELECT  C.Field1 [CUSTOMER_NUMBER] ,
        CASE WHEN Date4 IS NOT NULL THEN C.Date4
             ELSE CAST(C.Date2 AS DATE)   END AS [DATE_ADJUSTED_START]   ---Added CAST(C.Date2 AS DATE)  02-26-2018
	INTO #DAS
	FROM     EXIGO_ETL.dbo.Customers C 
	WHERE   C.Field1 != ''
			AND C.CustomerStatusID != '0' 



	INSERT INTO OPTAVIA_CUBE.[dbo].[DIM_CUSTOMER_PERIOD_INFO]
	SELECT DISTINCT  C.Field1 [CUSTOMER_NUMBER] --added Distinct 06-09-2018
		  ,CAST(P.EndDate AS DATE) [COMMISSION_PERIOD]
		 -- ,C.CustomerID [EXIGO_CUSTOMER_NUMBER]
		  ,CASE WHEN cast(pv.Volume108 as int) = '1' THEN 'Health Coach'
		  ELSE 'Client' END AS [CUSTOMER_TYPE]
		  ,CAST(PV.volume130 AS INT) [IS_ACTIVE_EARNING_HEALTH_COACH]
		  , CAST(PV.volume131 AS INT) [IS_ACTIVE_HEALTH_COACH]
		  ,case when cast(pv.Volume108 as int) ='1' then '1'
		  else '0' end as [IS_HEALTH_COACH]
		  ,PV.PaidRankID [RANK_ID]
		  , CAST(PV.Volume13 AS INT) [HIGHEST_ACHIEVED_RANK_ID]  --Micah W. 04-09-2018
		  ,Case when PV.PaidRankID - PR.PaidRankID < 0 then 'Rank down'
						   when PV.PaidRankID - PR.PaidRankID > 0 then 'Rank Up'
						   when PV.PaidRankID - PR.PaidRankID = 0 then 'Same Rank'
						   when PV.PaidRankID - PR.PaidRankID is null then 'No Rank Before'
					  End as [RANK_ADVANCEMENT_STATUS] --calculate
		  ,CASE WHEN C.Field2 = '' THEN '0'
		  ELSE C.Field2 END as  [SPONSOR_NUMBER]
		  --,C.EnrollerID [EXIGO_SPONSOR_NUMBER]
		  ,C2.FirstName [SPONSOR_FIRST_NAME]
		  ,Case when C2.LastName is not null and LTRIM(RTRIM(C2.LastName))='' THEN NULL ELSE  C2.LastName END [SPONSOR_LAST_NAME]
		  , c2.FirstName +' ' +C2.LastName [SPONSOR_FULL_NAME]
		  , ISNULL(DATEDIFF(MONTH,DAS.[DATE_ADJUSTED_START],CAST(CAST(P.EndDate AS DATE)  AS DATE)),0) AS [COACH_MONTH]
		  ,ISNULL(CAST(PV.volume25 AS INT),'0') AS 'is_FIBC'
		  ,ISNULL(CAST (PV.volume28 AS INT),'0') AS 'is_FIBL'
	FROM EXIGO_ETL.dbo.Customers C
	JOIN EXIGO_ETL.dbo.PeriodVolumes PV ON PV.CustomerID = C.CustomerID
	JOIN exigo_ETL.dbo.Periods P ON P.PeriodID = PV.PeriodID AND P.PeriodTypeID = PV.PeriodTypeID AND CAST(P.EndDate AS DATE) >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE) --Last Day of previous month
	LEFT JOIN #PrevRank PR ON PR.Medifast_Customer_Number = C.Field1 AND PR.COMMISSION_PERIOD = CAST(P.EndDate AS DATE)
	LEFT JOIN EXIGO_ETL.dbo.Customers c2 ON C.EnrollerID = C2.CustomerID
	LEFT JOIN #DAS DAS ON DAS.[CUSTOMER_NUMBER] = C.Field1
	WHERE (CAST(Volume108 as int) = '1' OR CAST(volume130 AS INT) ='1')
	AND P.PeriodTypeID = '1' 
	AND C.Field1 != '' --AND C.Field1 = '2002'
	AND C.Field1 NOT IN ('5','777','111', '20000224217','20000231249') --added 03-29-2018, 06-18-2018, 10-03-2018
	-- 04-27-2019 Removed Test Customer 20000224217,20000231249 This is a SG/HK Test customer
	AND C.CustomerStatusID != '0' AND c2.CustomerStatusID !='0' -- Added 06-09-2018



