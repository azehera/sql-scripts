﻿
/***************************************************************************
Object: Load Dim Calendar
Date: 10/27/2017
Developer(s): Micah W.
Description: Load Dim Calendar from Sales Cube
=============================================================================
MODIFICATIONS:
=============================================================================

******************************************************************************/
CREATE PROCEDURE [dbo].[usp_LoadDimCalendar] 

AS

DELETE FROM [OPTAVIA_CUBE].[dbo].[DIM_CALENDAR] WHERE [COMMISSION_PERIOD] =  Cast (DATEADD(DAY, -(DAY(GETDATE())), GETDATE()) AS DATE)-- EOMONTH(GETDATE())
insert INTO [OPTAVIA_CUBE].[dbo].[DIM_CALENDAR]
SELECT distinct CAST(c.[CalendarDate] AS DATE) [COMMISSION_PERIOD]
    ,c.[CalendarMonthID]
    ,c.[CalendarQuarter]
    ,c.[CalendarYear]
FROM [BI_SSAS_Cubes].[dbo].[calendar] C
WHERE CAST(C.[CalendarDate] AS DATE) =   Cast (DATEADD(DAY, -(DAY(GETDATE())), GETDATE()) AS DATE)


DELETE FROM [OPTAVIA_CUBE].[dbo].[DIM_CALENDAR] WHERE [COMMISSION_PERIOD] =  CAST(EOMONTH(GETDATE())AS DATE)
insert INTO [OPTAVIA_CUBE].[dbo].[DIM_CALENDAR]
SELECT distinct CAST(c.[CalendarDate] AS DATE) [COMMISSION_PERIOD]
    ,c.[CalendarMonthID]
    ,c.[CalendarQuarter]
    ,c.[CalendarYear]
FROM [BI_SSAS_Cubes].[dbo].[calendar] C
WHERE CAST(C.[CalendarDate] AS DATE) =   CAST(EOMONTH(GETDATE())AS DATE)