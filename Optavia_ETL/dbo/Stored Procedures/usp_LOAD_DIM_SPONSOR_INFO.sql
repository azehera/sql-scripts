﻿
/***************************************************************************
Object: [usp_LOAD_DIM_SPONSOR_INFO]
Date: 10/30/2017
Developer(s): Micah W.
Description: [usp_LOAD_DIM_SPONSOR_INFO] from various sources
=============================================================================
MODIFICATIONS:
01-04-2017 - Added condition to @closedPeriod CHeck ....AND PeriodTypeID = '1'
03-29-2018 - add filter to remove Medifast_Customer_Number = '5'
05-10-2018  Micah W  - include Active Earning Coach in the Optavia Cube Population (since Dec 2017)
06-18-2018  Micah W  - Removing Test Account 777
Micah W 10-03-2018 - Removing Test Account 111
Micah W 04-27-2019  - Removed Test Customer 20000224217,20000231249 This is a SG/HK Test customers
Ronak S 10-27-2019  - Made Change in Last Name to handle empty string values
Atya    06-09-2021  - Made change to EXIGO_SPONSOR_NUMBER column
=============================================================================

******************************************************************************/

CREATE PROCEDURE [dbo].[usp_LOAD_DIM_SPONSOR_INFO] 

@cp DATE

AS


--SET @cp = EOMONTH(@CP)
--DECLARE @ClosedPeriod INT 

--SELECT @ClosedPeriod = CASE WHEN AcceptedDate IS NULL THEN '0' 
--ELSE '1' END 
--FROM Exigo_ETL.dbo.Periods
--WHERE CAST(EndDate AS DATE) = @cp
--AND PeriodTypeID = '1' --added 01/04/2017




---Only execute the LOAD if the commission period is closed and if the period is after 09-30-2017



		DELETE FROM OPTAVIA_CUBE.dbo.[DIM_SPONSOR_INFO]
		WHERE COMMISSION_PERIOD >=  '11-30-2017' 
		AND COMMISSION_PERIOD >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE) --Last Day of previous month

		INSERT INTO OPTAVIA_CUBE.dbo.[DIM_SPONSOR_INFO]
		SELECT  Medifast_Customer_Number [CUSTOMER_NUMBER]
			  ,[COMMISSION_PERIOD]
			  ,Medifast_Enroller_Number[SPONSOR_NUMBER]
			  --,Exigo_EnrollerID [EXIGO_SPONSOR_NUMBER]
			  ,C.CustomerID [EXIGO_SPONSOR_NUMBER] 
			  ,Medifast_Enroller_FIRST_NAME[SPONSOR_FIRST_NAME]
			  ,Case when Medifast_Enroller_LAST_NAME is not null and LTRIM(RTRIM(Medifast_Enroller_LAST_NAME))='' THEN NULL ELSE  Medifast_Enroller_LAST_NAME END [SPONSOR_LAST_NAME]
			  ,Medifast_Enroller_FULL_NAME[SPONSOR_FULL_NAME]
		  FROM EXIGO_ETL.dbo.V_Tree_View_AVB AVB
		  LEFT JOIN EXIGO_ETL.dbo.Customers C on AVB.Medifast_Enroller_Number=C.Field1
		  WHERE  COMMISSION_PERIOD >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE) --Last Day of previous month
		  AND Medifast_Customer_Number != ''
		  AND (CAST(Volume108 as int) = '1' OR CAST(volume130 AS INT) ='1')
		  AND Medifast_Customer_Number NOT IN ('5','777','111', '20000224217','20000231249') --added 03-29-2018, 06-18-2018
		  -- 04-27-2019 Removed Test Customer 20000224217,20000231249 This is a SG/HK Test customers
