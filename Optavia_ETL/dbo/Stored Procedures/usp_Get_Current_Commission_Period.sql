﻿/***************************************************************************
Object: [usp_Get_Current_Commission_Period]
Date: 07/04/2015
Developer(s): D. Smith
Description: Use to get the current commission period to be used for MTD reporting.
=============================================================================
MODIFICATIONS:
=============================================================================

******************************************************************************/
CREATE PROCEDURE [dbo].[usp_Get_Current_Commission_Period] 


AS

SET NOCOUNT ON


select top 1 EndDate as COMMISSION_PERIOD from [EXIGO_ETL].[dbo].[Periods]
Where  [PeriodTypeID] =1 AND AcceptedDate IS NOT NULL
Order By COMMISSION_PERIOD DESC