﻿


/***************************************************************************
Object: DIM_CUSTOMER_INFO
Date: 10/27/2017
Developer(s): Micah W.
Description: Load DIM_CUSTOMER_INFO from various sources
=============================================================================
MODIFICATIONS:
Micah Williams 02-26-2018  - Added Cast As Date to Adjusted Start Date and removed the customertype=1 filter for adjusted start date
Micah Williams 03-08-2018 - Added filter for DATE_LAST_KIT_PURCHASE for when...WHERE ORH_TYPE_CD = 'ORD'
Micah Williams 03-23-2018 - Removed  WHERE  CustomerTypeID = 1  filter for  DATE_LAST_KIT_PURCHASE
Micah Williams 03-29-2018 - add filter to remove Medifast_Customer_Number = '5'
Micah Williams 05-10-2018 - include Active Earning Coach in the Optavia Cube Population (since Dec 2017)
Micah Williams 06-09-2018 - Remove customers with CustomerStatusID = 0 (delted status)..Keeping deleted status customers caused duplicates.
Micah Williams 06-18-2018  - Removing Test Account 777
Micah W 10-03-2018 - Removing Test Account 111
Micah W 1-31-2019 - Fetch DLKP (Date Last Kit Purchase) from Historically from UCart going forward from Exigo.
Micah W 2-4-2019 - Add Activation Date and misc date changes. WorkFront Ticket 21563177
Micah W 04-27-2019  - Removed Test Customer 20000224217,20000231249 This is a SG/HK Test customers
Ronak S 10-27-2019  - Made Change in  Last Name to handle empty string values
=============================================================================

******************************************************************************/

CREATE PROCEDURE [dbo].[usp_LOAD_DIMCUSTOMERINFO_bck] 

AS

--- Get [DATE_LAST_KIT_PURCHASE]
IF OBJECT_ID('Tempdb..#Exigo_DLKP_PREP') IS NOT NULL DROP TABLE #Exigo_DLKP_PREP
select OH.Medifast_Customer_Number, cast( OH.Order_Date as date) as 'Order_Date', 
row_number() OVER ( PARTITION BY OH.Medifast_Customer_Number ORDER BY OH.Order_Date DESC ) AS [RowNumber]
into #Exigo_DLKP_PREP
from EXIGO.ExigoSyncSQL_PRD.custom.Order_Header OH
left join EXIGO.ExigoSyncSQL_PRD.custom.Order_Details OD on OD.Order_No = OH.Order_No
where (OD.Item_Code in ( '31011_KT', '31012_KT', '32051_KT', '31100_KT',
                        '31105_KT', '31110_KT')
 or  	OD.Item_Code in ('31011KT', '31012KT', '32051KT', '31100KT',
                      '31105KT', '31110KT' ) )
order by OH.Order_Date desc


IF OBJECT_ID('Tempdb..#Exigo_DLKP') IS NOT NULL DROP TABLE #Exigo_DLKP
select * 
into #Exigo_DLKP
from #Exigo_DLKP_PREP
where RowNumber = '1'


IF OBJECT_ID('Tempdb..#DLKP_UCART_HISTORIC') IS NOT NULL DROP TABLE #DLKP_UCART_HISTORIC
select CUSTOMER_NUMBER, DATE_LAST_KIT_PURCHASE
into #DLKP_UCART_HISTORIC
from OPTAVIA_CUBE.dbo.Historic_DATE_LAST_KIT_PURCHASE


IF OBJECT_ID('Tempdb..#DLKP_PREP') IS NOT NULL DROP TABLE #DLKP_PREP
select Medifast_Customer_Number, Order_Date
into #DLKP_PREP
from #Exigo_DLKP
union ------------ 
select CUSTOMER_NUMBER, DATE_LAST_KIT_PURCHASE
from #DLKP_UCART_HISTORIC


IF OBJECT_ID('Tempdb..#DLKP_PREP_RN') IS NOT NULL DROP TABLE #DLKP_PREP_RN
select Medifast_Customer_Number, Order_Date, 
row_number() OVER ( PARTITION BY Medifast_Customer_Number ORDER BY Order_Date DESC ) AS [RowNumber]
into #DLKP_PREP_RN
from #DLKP_PREP

IF OBJECT_ID('Tempdb..#DLKP_final') IS NOT NULL DROP TABLE #DLKP_final
select Medifast_Customer_Number [CUSTOMER_NUMBER], Order_Date [DATE_LAST_KIT_PURCHASE], RowNumber
into #DLKP_final
from #DLKP_PREP_RN
where RowNumber = '1'


--Adjusted Start Date
IF OBJECT_ID('Tempdb..#DAS') IS NOT NULL DROP TABLE #DAS
----SELECT  C.Field1 [CUSTOMER_NUMBER] ,
----        CASE WHEN [DATE_ADJUSTED_START] IS NOT NULL THEN [DATE_ADJUSTED_START]
----             ELSE CAST(C.Date2 AS DATE)   END AS [DATE_ADJUSTED_START]   ---Added CAST(C.Date2 AS DATE)  02-26-2018
----INTO #DAS
----FROM    OPTAVIA_CUBE.[dbo].[Historic_DATE_ADJUSTED_START] H
----        RIGHT JOIN EXIGO_ETL.dbo.Customers C ON C.Field1 = H.[CUSTOMER_NUMBER]
----WHERE   --C.CustomerTypeID = '1'                                             --Removed this filter 02-28-2018..We want the adjusted start date for coaches and clients
----        --AND 
----		C.Field1 != ''
----		AND C.CustomerStatusID != '0' --added 06-09-2018 Micah W.;

SELECT  C.Field1 [CUSTOMER_NUMBER] ,
        CASE WHEN Date4 IS NOT NULL THEN C.Date4
             ELSE CAST(C.Date2 AS DATE)   END AS [DATE_ADJUSTED_START]   ---Added CAST(C.Date2 AS DATE)  02-26-2018
INTO #DAS
FROM     EXIGO_ETL.dbo.Customers C 
WHERE   --C.CustomerTypeID = '1'                                             --Removed this filter 02-28-2018..We want the adjusted start date for coaches and clients
        --AND 
		C.Field1 != ''
		AND C.CustomerStatusID != '0' --added 06-09-2018 Micah W.;


--Get Customer from Historic Coaches
IF OBJECT_ID('Tempdb..#ExHist') IS NOT NULL DROP TABLE #ExHist
SELECT DISTINCT MedifastCustomerNumber 
INTO #ExHist
FROM EXIGO.ExigoSyncSQL_PRD.CommissionContext.PeriodVolumesCustomers
WHERE CustomerTypeID = '1'

--Get ACTIVE Eanring Customers
IF OBJECT_ID('Tempdb..#ExAE') IS NOT NULL DROP TABLE #ExAE
SELECT DISTINCT CustomerID
INTO #ExAE
FROM EXIGO_ETL.dbo.PeriodVolumes
WHERE volume130 = 1.00


--Get Coach List
IF OBJECT_ID('Tempdb..#CL') IS NOT NULL DROP TABLE #CL
SELECT TV.Medifast_Customer_Number
INTO #CL
FROM Exigo_ETL.dbo.V_Tree_View TV
WHERE TV.COMMISSION_PERIOD = CAST(EOMONTH(GETDATE()) AS DATE) AND TV.CustomerTypeID = '1'
 

---Populate Work Table
TRUNCATE TABLE [OPTAVIA_CUBE].[dbo].DIM_CUSTOMER_INFO_work

insert INTO [OPTAVIA_CUBE].[dbo].DIM_CUSTOMER_INFO_work
SELECT  DISTINCT Field1 [CUSTOMER_NUMBER] ,
        CustomerID [EXIGO_CUSTOMER_NUMBER] ,
        FirstName [NAME_FIRST] ,
        LastName [NAME_LAST] ,
        FirstName + ' ' + LastName [NAME_FULL] ,
        C.MainAddress1 [ADDRESS1] ,
        C.MainCity [ADDRESS_CITY] ,
        C.MainCounty [ADDRESS_COUNTY] ,
        C.MainState [ADDRESS_STATE] ,
        ISNULL(LEFT(C.MainZip,5),0) [ADDRESS_ZIP_CODE] ,
        C.MainCountry [ADDRESS_COUNTRY] ,
        NULL [DATE_TERMI NATION] ,
        Date3 [DATE_REVERSION] ,
        Date4 [DATE_REINSTATEMENT] ,
        Date1 [DATE_ENTRY] ,
        NULL [DATE_CERTIFIED] , --Rachana
        Date5 [DATE_RENEWAL] ,
        D.DATE_LAST_KIT_PURCHASE , 
        DAS.[DATE_ADJUSTED_START]  , 
        ISNULL(DATEDIFF(MONTH,DAS.[DATE_ADJUSTED_START],CAST(Cast (DATEADD(DAY, -(DAY(GETDATE())), GETDATE()) AS DATE) AS DATE)),0) [COACH_LONGEVITY] , 
		MONTH([DATE_ADJUSTED_START]) [DRAFT_MONTH],
	    YEAR([DATE_ADJUSTED_START]) [DRAFT_YEAR],
        NULL [W9_COMPLETE] , --Rachana
        NULL [W9_COMPLETE_DATE] , -- Rachana
        C.Phone [MAIN_PHONE] ,
        C.Email [EMAIL1_ADDRESS],
		Date2 [Date_Activation]

FROM    EXIGO_ETL.dbo.Customers C
        LEFT JOIN #DLKP_final D ON D.[CUSTOMER_NUMBER] = C.Field1
		LEFT JOIN #DAS DAS ON DAS.[CUSTOMER_NUMBER] = C.Field1
WHERE   Field1 != ''
        AND (C.Field1 IN (SELECT Medifast_Customer_Number FROM #CL) 
		OR C.Field1 IN (SELECT MedifastCustomerNumber FROM #ExHist)
		OR C.CustomerID IN (SELECT  CustomerID	FROM #ExAE))
		AND C.Field1 NOT IN (SELECT C.Field1 FROM EXIGO_ETL.dbo.Customers C WHERE C.CustomerStatusID = '0') --added 06-09-2018 Micah W.
		AND C.Field1 NOT IN ('5','777','111', '20000224217','20000231249') --added 03-29-2018, 06-18-2018,10-03-2018
		-- 04-27-2019 Removed Test Customer 20000224217,20000231249 This is a SG/HK Test customers


------Union Exigo Data with TSFL cube data
IF OBJECT_ID('Tempdb..#prep1') IS NOT NULL DROP TABLE #prep1
SELECT [CUSTOMER_NUMBER]
      ,[EXIGO_CUSTOMER_NUMBER]
      ,[NAME_FIRST]
      ,[NAME_LAST]
      ,[NAME_FULL]
      ,[ADDRESS1]
      ,[ADDRESS_CITY]
      ,[ADDRESS_COUNTY]
      ,[ADDRESS_STATE]
      ,[ADDRESS_ZIP_CODE]
      ,[ADDRESS_COUNTRY]
      ,[DATE_TERMINATION]
	  ,[Date_Activation]
      ,[DATE_REVERSION]
      ,[DATE_REINSTATEMENT]
     ,[DATE_ENTRY]
      ,[DATE_CERTIFIED]
      ,[DATE_RENEWAL]
      ,[DATE_LAST_KIT_PURCHASE]
      ,[DATE_ADJUSTED_START]
      ,[COACH_LONGEVITY]
      ,[DRAFT_MONTH]
      ,[DRAFT_YEAR]
      ,[W9_COMPLETE]
      ,[W9_COMPLETE_DATE]
      ,[MAIN_PHONE]
      ,[EMAIL1_ADDRESS]
	  ,'exigo' AS 'source'
INTO #prep1
FROM dbo.DIM_CUSTOMER_INFO_work

UNION 

SELECT [CUSTOMER_NUMBER]
      ,[EXIGO_CUSTOMER_NUMBER]
      ,[NAME_FIRST]
      ,[NAME_LAST]
      ,[NAME_FULL]
      ,[ADDRESS1]
      ,[ADDRESS_CITY]
      ,[ADDRESS_COUNTY]
      ,[ADDRESS_STATE]
      ,[ADDRESS_ZIP_CODE]
      ,[ADDRESS_COUNTRY]
      ,[DATE_TERMINATION]
	  ,NULL [Date_Activation]
      ,[DATE_REVERSION]
      ,[DATE_REINSTATEMENT]
     ,[DATE_ENTRY]
      ,[DATE_CERTIFIED]
      ,[DATE_RENEWAL]
      ,[DATE_LAST_KIT_PURCHASE]
      ,[DATE_ADJUSTED_START]
      ,[COACH_LONGEVITY]
      ,[DRAFT_MONTH]
      ,[DRAFT_YEAR]
      ,[W9_COMPLETE]
      ,[W9_COMPLETE_DATE]
      ,[MAIN_PHONE]
      ,[EMAIL1_ADDRESS]
	  ,'tsfl_cube' AS 'source'
FROM [dbo].[Historic_DIM_CUSTOMER_INFO]




IF OBJECT_ID('Tempdb..#prep2') IS NOT NULL DROP TABLE #prep2
SELECT * ,
ROW_NUMBER() OVER ( PARTITION BY CUSTOMER_NUMBER ORDER BY source ASC ) AS [RowNumber]
INTO #prep2
FROM #prep1


--Select All 
TRUNCATE TABLE OPTAVIA_CUBE.dbo.DIM_CUSTOMER_INFO

INSERT INTO [OPTAVIA_CUBE].[dbo].DIM_CUSTOMER_INFO
SELECT  [CUSTOMER_NUMBER] ,
        ISNULL([EXIGO_CUSTOMER_NUMBER],'') ,
        ISNULL([NAME_FIRST],'Unknown') ,
        CASE WHEN LTRIM(RTRIM([NAME_LAST]))='' OR [NAME_LAST] IS NULL THEN 'Unknown' ELSE [NAME_LAST] END AS [NAME_LAST],
		--ISNULL([NAME_LAST],'Unknown') ,
        ISNULL([NAME_FULL],'Unknown') ,
        ISNULL([ADDRESS1],'Unknown') ,
        ISNULL([ADDRESS_CITY],'Unknown') ,
        ISNULL([ADDRESS_COUNTY],'Unknown') ,
        --ISNULL([ADDRESS_STATE],'Unknown') ,
		ISNULL(NULLIF([ADDRESS_STATE], ''),'UN') ADDRESS_STATE,---Added by Daniel D to capture invisible characters. 08/05/2019
        ISNULL([ADDRESS_ZIP_CODE],'Unknown') ,
        ISNULL([ADDRESS_COUNTRY],'Unknown') ,
        [DATE_TERMINATION] ,
		[DATE_REVERSION] ,
        [DATE_REINSTATEMENT] ,
        [DATE_ENTRY] ,
        [DATE_CERTIFIED] ,
        [DATE_RENEWAL] ,
        [DATE_LAST_KIT_PURCHASE] ,
        [DATE_ADJUSTED_START] ,
        [COACH_LONGEVITY] ,
        [DRAFT_MONTH] ,
        [DRAFT_YEAR] ,
        ISNULL([W9_COMPLETE],'') ,
        [W9_COMPLETE_DATE] ,
        ISNULL([MAIN_PHONE],'') ,
        ISNULL([EMAIL1_ADDRESS],''),
		CASE WHEN C.Field3 = '1' THEN 1
		ELSE 0 END AS IS_CERTIFIED,
		CASE WHEN C.field5 != '' THEN 1
		ELSE 0 END  AS IS_HEALTHPROFESSIONAL,
		[Date_Activation]
FROM    #prep2 P
LEFT JOIN EXIGO_ETL.dbo.Customers C ON P.CUSTOMER_NUMBER = C.Field1
WHERE   RowNumber = '1'
AND [CUSTOMER_NUMBER] NOT IN ('5','777','111', '20000224217','20000231249') --added 03-28-2018, 06-18-2018, 10-03-2018
-- 04-27-2019 Removed Test Customer 20000224217, This is a SG/HL Test customers

--AND C.CustomerStatusID != '0' --Micah Williams 06-09-2018
