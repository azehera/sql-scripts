﻿
/***************************************************************************
Object: [usp_LOAD_DIM_BONUS]
Date: 10/30/2017
Developer(s): Micah W.
Description: Load Bonus data from Exigo only (odsy bonus data not in this table)
=============================================================================
MODIFICATIONS:

=============================================================================

******************************************************************************/

CREATE PROCEDURE [dbo].[usp_LOAD_DIM_BONUS] 

AS


TRUNCATE TABLE dbo.DIM_BONUS

  IF OBJECT_ID('Tempdb..#Monthly') IS NOT NULL DROP TABLE #Monthly; 
  SELECT C.Field1, cast (P.EndDate AS DATE) AS Commission_Period, CB.BonusID, CB.Amount
  INTO #Monthly
  FROM [EXIGO].[ExigoSyncSQL_PRD].[dbo].[CommissionBonuses] CB
  LEFT JOIN [EXIGO].[ExigoSyncSQL_PRD].dbo.CommissionRuns CR ON CR.CommissionRunID = CB.CommissionRunID
  LEFT JOIN [EXIGO].[ExigoSyncSQL_PRD].dbo.Periods P ON P.PeriodID = CR.PeriodID AND P.PeriodTypeID = CR.PeriodTypeID
  LEFT JOIN [EXIGO].[ExigoSyncSQL_PRD].dbo.Customers C ON C.CustomerID = CB.CustomerID
  WHERE CR.PeriodTypeID = '1'
	AND cast (P.EndDate AS DATE) >= '12-31-2017' AND p.AcceptedDate IS NOT NULL AND CR.CommissionRunStatusID = '3'
  --ORDER BY Commission_Period



  IF OBJECT_ID('Tempdb..#Weekly') IS NOT NULL DROP TABLE #Weekly; 
  SELECT C.Field1, cast (EOMONTH(P.EndDate) AS DATE) AS Commission_Period, CB.BonusID, SUM(CB.Amount) AS Amount
  INTO #Weekly
  FROM  [EXIGO].[ExigoSyncSQL_PRD].[dbo].[CommissionBonuses] CB
  LEFT JOIN  [EXIGO].[ExigoSyncSQL_PRD].dbo.CommissionRuns CR ON CR.CommissionRunID = CB.CommissionRunID
  LEFT JOIN  [EXIGO].[ExigoSyncSQL_PRD].dbo.Periods P ON P.PeriodID = CR.PeriodID AND P.PeriodTypeID = CR.PeriodTypeID
  LEFT JOIN  [EXIGO].[ExigoSyncSQL_PRD].dbo.Customers C ON C.CustomerID = CB.CustomerID
  WHERE   CR.PeriodTypeID = '2' AND p.AcceptedDate IS NOT NULL  AND CR.CommissionRunStatusID = '3'
  AND cast (EOMONTH(P.EndDate) AS DATE)  >= '12-31-2017'
  GROUP BY C.Field1, cast (EOMONTH(P.EndDate) AS DATE), CB.BonusID
 -- ORDER BY Commission_Period


  IF OBJECT_ID('Tempdb..#MonthWeek') IS NOT NULL DROP TABLE #MonthWeek; 
  SELECT * 
  INTO #MonthWeek
  FROM #Monthly
  UNION ALL
  SELECT * FROM #Weekly


  IF OBJECT_ID('Tempdb..#pivot') IS NOT NULL DROP TABLE #pivot; 
  SELECT * 
  INTO #pivot
  FROM #MonthWeek
  PIVOT (SUM (Amount) FOR BonusID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16])) AS Test


--TRUNCATE TABLE OPTAVIA_CUBE.dbo.DIM_BONUS
INSERT INTO OPTAVIA_CUBE.dbo.DIM_BONUS
        ( CUSTOMER_NUMBER ,
          COMMISSION_PERIOD ,
          First_Bonus ,
          Certification_Bonus ,
          Client_Support_Bonus_Monthly ,
          Team_Growth_Bonus ,
          Executive_Director_Generation_Bonus ,
          National_Elite_Leadership_Bonus ,
          Global_Elite_Leadership_Bonus ,
          Presidential_Elite_Leadership_Bonus ,
          Health_Coach_Consistency_Bonus ,
          FIBC_Consistency_Bonus ,
          Monthly_Adjustments ,
          Business_Builder_Bonus ,
          Client_Support_Bonus_Weekly ,
          Client_Acquisition_Bonus ,
          Assist_Bonus ,
          Weekly_Adjustments
        )
SELECT FO.CUSTOMER_NUMBER,
FO.COMMISSION_PERIOD,
ISNULL([1],0) AS [First_Bonus],
ISNULL([5],0) AS [Certification_Bonus],
ISNULL([6],0) AS [Client_Support_Bonus_Monthly],
ISNULL([7],0) AS [Team_Growth_Bonus],
ISNULL([8],0) AS [Executive_Director_Generation_Bonus],
ISNULL([9],0) AS [National_Elite_Leadership_Bonus],
ISNULL([10],0) AS [Global_Elite_Leadership_Bonus],
ISNULL([11],0) AS [Presidential_Elite_Leadership_Bonus],
ISNULL([12],0) AS [Health_Coach_Consistency_Bonus],
ISNULL([13],0) AS [FIBC_Consistency_Bonus],
ISNULL([14],0) AS [Monthly_Adjustments],
ISNULL([16],0) AS [Business_Builder_Bonus],
ISNULL([2],0) AS [Client_Support_Bonus_Weekly],
ISNULL([3],0) AS [Client_Acquisition_Bonus],
ISNULL([4],0) AS [Assist_Bonus],
ISNULL([15],0) AS [Weekly_Adjustments]
FROM #pivot P
RIGHT JOIN OPTAVIA_CUBE.dbo.FACT_OPTAVIA FO ON FO.COMMISSION_PERIOD = P.Commission_Period AND FO.CUSTOMER_NUMBER=P.Field1
WHERE FO.COMMISSION_PERIOD >='12-31-2017'