﻿
/***************************************************************************
Object: DIM_CUSTOMER_INFO
Date: 10/30/2017
Developer(s): Micah W.
Description: usp_LOAD_DIM_FIRST_UPLINE from various sources
=============================================================================
MODIFICATIONS:
01-04-2017 - Added condition to @closedPeriod CHeck ....AND PeriodTypeID = '1'
03-29-2018 - add filter to remove Field1 = '5'
05-10-2018  Micah W  - include Active Earning Coach in the Optavia Cube Population (since Dec 2017)
06-18-2018  Micah W  - Removing Test Account 777
Micah W 10-03-2018 - Removing Test Account 111
Micah W.03-01-2019 - Added First FIBC and First FIBL
Micah W 03-08-2019 - replace cast as int to cast as BIGINT
Micah W 04-27-2019  - Removed Test Customer 20000224217,20000231249 This is a SG/HK Test customers
=============================================================================

******************************************************************************/

CREATE PROCEDURE [dbo].[usp_LOAD_DIM_FIRST_UPLINE] 
@cp DATE

AS



		DELETE FROM OPTAVIA_CUBE.dbo.[DIM_FIRST_UPLINE]
		WHERE COMMISSION_PERIOD >  '11-30-2017' 
		AND COMMISSION_PERIOD >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE) --Last Day of previous month

		INSERT INTO [dbo].[DIM_FIRST_UPLINE]
		        ( [CUSTOMER_NUMBER] ,
		          [COMMISSION_PERIOD] ,
		          [FIRST_HEALTH_COACH] ,
		          [FIRST_FT_HEALTH_COACH] ,
		          [FIRST_SENIOR_COACH] ,
		          [FIRST_MANAGER] ,
		          [FIRST_ASSOCIATE_DIRECTOR] ,
		          [FIRST_DIRECTOR] ,
		          [FIRST_EXECUTIVE_DIRECTOR] ,
		          [FIRST_FI_EXECUTIVE_DIRECTOR] ,
		          [FIRST_REGIONAL_DIRECTOR] ,
		          [FIRST_FI_REGIONAL_DIRECTOR] ,
		          [FIRST_NATIONAL_DIRECTOR] ,
		          [FIRST_FI_NATIONAL_DIRECTOR] ,
		          [FIRST_GLOBAL_DIRECTOR] ,
		          [FIRST_FI_GLOBAL_DIRECTOR] ,
		          [FIRST_PRESIDENTIAL_DIRECTOR] ,
		          [FIRST_FI_PRESIDENTIAL_DIRECTOR] ,
		          [FIRST_FIBC] ,
		          [FIRST_FIBL]
		        )
		SELECT distinct C.Field1 [CUSTOMER_NUMBER]
			  ,CAST(P.EndDate AS DATE) [COMMISSION_PERIOD]
			  ,cast (PV.Volume115 as BIGINT) [FIRST_HEALTH_COACH]
			  ,NULL [FIRST_FT_HEALTH_COACH]
			  ,CAST(PV.Volume116 as BIGINT) [FIRST_SENIOR_COACH]
			  ,CAST(PV.Volume117 as BIGINT) [FIRST_MANAGER]
			  ,CAST(PV.Volume118 as BIGINT) [FIRST_ASSOCIATE_DIRECTOR]
			  ,CAST(PV.Volume119 as BIGINT)[FIRST_DIRECTOR]
			  ,CAST(PV.Volume120 as BIGINT) [FIRST_EXECUTIVE_DIRECTOR]
			  ,cast (PV.Volume121 as BIGINT) [FIRST_FI_EXECUTIVE_DIRECTOR]
			  ,cast (PV.Volume122 as BIGINT) [FIRST_REGIONAL_DIRECTOR]
			  ,cast (PV.Volume123 as BIGINT) [FIRST_FI_REGIONAL_DIRECTOR]
			  ,CAST(PV.Volume124 as BIGINT) [FIRST_NATIONAL_DIRECTOR]
			  ,CAST(PV.Volume125 as BIGINT) [FIRST_FI_NATIONAL_DIRECTOR]
			  ,CAST(PV.Volume126 as BIGINT) [FIRST_GLOBAL_DIRECTOR]
			  ,CAST(PV.Volume127 as BIGINT) [FIRST_FI_GLOBAL_DIRECTOR]
			  ,CAST(PV.Volume128 as BIGINT) [FIRST_PRESIDENTIAL_DIRECTOR]
			  ,CAST(PV.Volume129 as BIGINT) [FIRST_FI_PRESIDENTIAL_DIRECTOR]
			  ,CAST(PV.Volume150 as BIGINT) First_FIBC
			  ,CAST(PV.volume151 as BIGINT) [FIRST_FIBL]
		  FROM EXIGO_ETL.dbo.Customers C
		  JOIN EXIGO_ETL.dbo.PeriodVolumes PV ON PV.CustomerID = C.CustomerID
			JOIN exigo_ETL.dbo.Periods P ON P.PeriodID = PV.PeriodID AND P.PeriodTypeID = PV.PeriodTypeID AND CAST(P.EndDate AS DATE) >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) AS DATE) --Last Day of previous month
		WHERE C.Field1 != ''  
				AND (CAST(pv.Volume108 as BIGINT) = '1' or CAST(volume130 as BIGINT) ='1')
				AND pv.PeriodTypeID = '1' --added 01/04/2017
				AND C.Field1 NOT IN ('5','777','111','20000224217', '20000231249') --added 03-29-2018, 06-18-2018
				-- 04-27-2019 Removed Test Customer 20000224217,20000231249 This is a SG/HG Test customers

