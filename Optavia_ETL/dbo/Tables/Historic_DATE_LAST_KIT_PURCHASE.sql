﻿CREATE TABLE [dbo].[Historic_DATE_LAST_KIT_PURCHASE] (
    [CUSTOMER_NUMBER]        NVARCHAR (40) NULL,
    [DATE_LAST_KIT_PURCHASE] DATE          NULL,
    [RowNumber]              BIGINT        NULL
);

