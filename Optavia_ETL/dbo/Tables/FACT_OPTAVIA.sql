﻿CREATE TABLE [dbo].[FACT_OPTAVIA] (
    [CUSTOMER_NUMBER]                     NVARCHAR (255)  NOT NULL,
    [COMMISSION_PERIOD]                   DATE            NOT NULL,
    [PV]                                  NUMERIC (18, 4) NULL,
    [FLV]                                 NUMERIC (18, 4) NULL,
    [GV]                                  NUMERIC (18, 4) NULL,
    [NEW_HEALTH_COACHES]                  INT             NULL,
    [NEW_CLIENTS]                         INT             NULL,
    [TOTAL_COMMISSIONS]                   MONEY           NULL,
    [SC_COUNT]                            INT             NULL,
    [ED_COUNT]                            INT             NULL,
    [FIBC_COUNT]                          INT             NULL,
    [FIBL_COUNT]                          INT             NULL,
    [RANK_ID]                             INT             NULL,
    [Ordering_Entities]                   INT             NULL,
    [First_Bonus]                         MONEY           NULL,
    [Certification_Bonus]                 MONEY           NULL,
    [Client_Support_Bonus_Monthly]        MONEY           NULL,
    [Team_Growth_Bonus]                   MONEY           NULL,
    [Executive_Director_Generation_Bonus] MONEY           NULL,
    [National_Elite_Leadership_Bonus]     MONEY           NULL,
    [Global_Elite_Leadership_Bonus]       MONEY           NULL,
    [Presidential_Elite_Leadership_Bonus] MONEY           NULL,
    [Health_Coach_Consistency_Bonus]      MONEY           NULL,
    [FIBC_Consistency_Bonus]              MONEY           NULL,
    [Monthly_Adjustments]                 MONEY           NULL,
    [Business_Builder_Bonus]              MONEY           NULL,
    [Client_Support_Bonus_Weekly]         MONEY           NULL,
    [Client_Acquisition_Bonus]            MONEY           NULL,
    [Assist_Bonus]                        MONEY           NULL,
    [Weekly_Adjustments]                  MONEY           NULL,
    [Qualifying_Points]                   NUMERIC (18, 4) NULL,
    [PCV]                                 NUMERIC (18, 4) NULL,
    [FCV]                                 NUMERIC (18, 4) NULL,
    [GCV]                                 NUMERIC (18, 4) NULL,
    CONSTRAINT [PK_FACT_OPTAVIA] PRIMARY KEY CLUSTERED ([CUSTOMER_NUMBER] ASC, [COMMISSION_PERIOD] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [IX_FACT_OPTAVIA_idx1]
    ON [dbo].[FACT_OPTAVIA]([COMMISSION_PERIOD] ASC)
    INCLUDE([CUSTOMER_NUMBER]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FACT_OPTAVIA_COMMISSION_PERIOD]
    ON [dbo].[FACT_OPTAVIA]([COMMISSION_PERIOD] ASC)
    INCLUDE([CUSTOMER_NUMBER], [NEW_HEALTH_COACHES], [NEW_CLIENTS]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FACT_OPTAVIA_COMMISSION_PERIOD_idx1]
    ON [dbo].[FACT_OPTAVIA]([COMMISSION_PERIOD] ASC)
    INCLUDE([CUSTOMER_NUMBER], [NEW_HEALTH_COACHES], [NEW_CLIENTS], [TOTAL_COMMISSIONS]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FACT_OPTAVIA_RANK_ID_COMMISSION_PERIOD]
    ON [dbo].[FACT_OPTAVIA]([RANK_ID] ASC, [COMMISSION_PERIOD] ASC)
    INCLUDE([CUSTOMER_NUMBER], [PV], [FLV], [GV], [NEW_HEALTH_COACHES], [NEW_CLIENTS], [TOTAL_COMMISSIONS], [SC_COUNT], [ED_COUNT], [FIBC_COUNT], [FIBL_COUNT], [Ordering_Entities], [First_Bonus], [Certification_Bonus], [Client_Support_Bonus_Monthly], [Team_Growth_Bonus], [Executive_Director_Generation_Bonus], [National_Elite_Leadership_Bonus], [Global_Elite_Leadership_Bonus], [Presidential_Elite_Leadership_Bonus], [Health_Coach_Consistency_Bonus], [FIBC_Consistency_Bonus], [Monthly_Adjustments], [Business_Builder_Bonus], [Client_Support_Bonus_Weekly], [Client_Acquisition_Bonus], [Assist_Bonus], [Weekly_Adjustments], [Qualifying_Points], [PCV], [FCV], [GCV]) WITH (FILLFACTOR = 90);

