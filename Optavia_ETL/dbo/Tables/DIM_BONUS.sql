﻿CREATE TABLE [dbo].[DIM_BONUS] (
    [CUSTOMER_NUMBER]                     NVARCHAR (250) NOT NULL,
    [COMMISSION_PERIOD]                   DATE           NOT NULL,
    [First_Bonus]                         MONEY          NULL,
    [Certification_Bonus]                 MONEY          NULL,
    [Client_Support_Bonus_Monthly]        MONEY          NULL,
    [Team_Growth_Bonus]                   MONEY          NULL,
    [Executive_Director_Generation_Bonus] MONEY          NULL,
    [National_Elite_Leadership_Bonus]     MONEY          NULL,
    [Global_Elite_Leadership_Bonus]       MONEY          NULL,
    [Presidential_Elite_Leadership_Bonus] MONEY          NULL,
    [Health_Coach_Consistency_Bonus]      MONEY          NULL,
    [FIBC_Consistency_Bonus]              MONEY          NULL,
    [Monthly_Adjustments]                 MONEY          NULL,
    [Business_Builder_Bonus]              MONEY          NULL,
    [Client_Support_Bonus_Weekly]         MONEY          NULL,
    [Client_Acquisition_Bonus]            MONEY          NULL,
    [Assist_Bonus]                        MONEY          NULL,
    [Weekly_Adjustments]                  MONEY          NULL,
    CONSTRAINT [PK_DIM_BONUS] PRIMARY KEY CLUSTERED ([CUSTOMER_NUMBER] ASC, [COMMISSION_PERIOD] ASC) WITH (FILLFACTOR = 85)
);

