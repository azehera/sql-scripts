﻿CREATE TABLE [Custom].[RankAdvancementLegend] (
    [CurrentRank]      NVARCHAR (50) NULL,
    [CurrentRankID]    INT           NULL,
    [NextRank]         NVARCHAR (50) NULL,
    [NextRankID]       INT           NULL,
    [WhatsNextRank]    NVARCHAR (50) NULL,
    [WhatsNextRankID]  INT           NULL,
    [LinearNextRank]   NVARCHAR (50) NULL,
    [LinearNextRankID] INT           NULL
);

