﻿

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
CREATE VIEW [dbo].[V_Tree_View_Historic] 
AS 
 
-- =============================================  
-- Author:		Micah Williams 
-- Create date: 02/01/17  
-- Description:	This view shows monthly metrics for Optavia Customers 
-- Modification Micah W. 12-20-2017 - Correcting the following mapping 
--									 -FIBC_Team = volume33 
--									 -FIBL_Team = volume34 
--									 -ORDERING_ENTITIES = volume35 
--Modification Micah W. 04-23-2018   -chagned join for customertypedescription....Join using pv.volume108 instead of c.customertypedesctiption 
--Modification Micah W. 02-06-2019	 -Adding GZV (aka Volume162), GzClients (volume171), and GzCoaches (Volume170) to this View, Earned CAB (Volume118) 
--Modification Vick S. 03-15-2019 - Adding RequiredGV (aka Volume153) 
--Modification Vick S. 03-28-2019 - Modified Tree View to remove odsy volume data as it was not being used for international(kept it as a comment but not being used anywhere) and casted Current_Rank and Previous_Rank to decimal 
-- =============================================  
 
--dbo Period Volume Data 
SELECT DISTINCT 
        C.Field1 AS Medifast_Customer_Number, 
		C.CustomerID AS 'Exigo_CustomerID', 
		CAST(PV.Volume106 AS BIGINT) AS 'NestedLevel', 
        C.FirstName AS CUSTOMER_FIRST_NAME , 
        C.LastName AS CUSTOMER_LAST_NAME , 
		C.FirstName+' '+C.LastName AS CUSTOMER_FULL_NAME , 
		CAST(CAST (PV.Volume105 AS BIGINT) AS NVARCHAR(100)) AS 'Medifast_Enroller_Number', 
        C2.FirstName + ' ' + C2.LastName AS Medifast_Enroller_FULL_NAME , 
		C2.FirstName AS Medifast_Enroller_FIRST_NAME , 
		C2.LastName AS Medifast_Enroller_LAST_NAME , 
		C.EnrollerID AS Exigo_EnrollerID, 
        PV.Volume1 AS PV , 
        ISNULL(PV2.Volume1,0) AS LAST_MONTH_PV , 
		ISNULL(PV2.Volume2,0) AS LAST_MONTH_FLV , 
        CAST(PV.PaidRankID AS DECIMAL(18,2)) AS CURRENT_RANK , 
		CAST(PV2.PaidRankID AS DECIMAL(18,2)) AS Previous_RANK , 
        RM.RankDescription AS CURRENT_RANK_TEXT , 
        CAST(PV.volume13 AS DECIMAL(18,2)) AS Highest_Rank , 
        RM2.RankDescription AS Highest_RANK_TEXT , 
        PV.Volume2 AS FLV , 
        PV.Volume35 AS ORDERING_ENTITIES , 
        PV.Volume4 AS QUALIFYING_POINTS , 
        PV.Volume31 AS SC_TEAM , 
        PV.Volume32 AS ED_TEAM , 
        
        PV.Volume34 AS FIBL_TEAM , 
        PV.Volume3 AS GV , 
		P.PeriodID, 
        CAST(P.EndDate AS DATE) AS COMMISSION_PERIOD , 
        C.Email , 
        C.MainAddress1 , 
        C.MainAddress2 , 
        C.MainAddress3 , 
        C.MainCity , 
        C.MainState , 
        C.MainZip , 
        C.MainCountry , 
        C.MainCounty , 
        C.Phone, 
		CAST(PV.volume107 AS BIGINT) AS [CustomerStatusID], 
		CS.CustomerStatusDescription, 
		CAST(PV.volume108 AS BIGINT) AS [CustomerTypeID], 
		C.Date1 AS 'EntryDate', 
		C.Date2 AS 'ActivationDate', 
		C.Date3 AS 'ReversionDate', 
		C.Date4 AS 'ReinstatementDate', 
		CAST (C.Date5 AS DATE) AS 'RenewalDate', 
		C.[Field3] AS 'Certified',  
		CT.CustomerTypeDescription, 
		PV.Volume31 AS 'SeniorCoachTeam', 
		PV.Volume32 AS 'ExecutiveDirectorTeam', 
		PV.Volume33 AS 'FIBC_Team', 
		CASE WHEN C.Field5 = '' THEN '0' 
		ELSE C.Field5 END AS 'HP_Status', 
		CAST(CAST(PV.Volume52 AS BIGINT) AS NVARCHAR(100)) AS 'GlobalDirector',  --Volume52 Exigo ID 
		CAST(CAST(PV.Volume104 AS BIGINT) AS NVARCHAR(100)) AS 'PresidentialDirector', 
		PV.Volume162 AS GZV , 
		PV.volume171 AS GzClients, 
		PV.Volume170 AS GzCoaches, 
		PV.volume18 AS [Earned_CAB], 
		PV.Volume153 AS RequiredGV 
		FROM    dbo.Customers C WITH (NOLOCK) 
		INNER JOIN dbo.PeriodVolumes  AS PV WITH (NOLOCK) ON C.CustomerID = PV.CustomerID AND  PV.PeriodTypeID = 1 
        INNER JOIN ODSY.RanksMapping AS RM WITH (NOLOCK) ON RM.ExigoRankID = PV.PaidRankID 
        INNER JOIN ODSY.RanksMapping AS RM2 WITH (NOLOCK) ON RM2.ExigoRankID = CAST(PV.volume13 AS DECIMAL(18,2)) 
        INNER JOIN dbo.Periods AS P WITH (NOLOCK) ON P.PeriodID = PV.PeriodID AND P.PeriodTypeID = 1 
		--INNER JOIN dbo.EnrollerTree ET WITH (NOLOCK) ON ET.CustomerID = C.CustomerID 
		LEFT JOIN dbo.CustomerTypes CT  WITH (NOLOCK) ON CT.CustomerTypeID = CAST(PV.volume108 AS BIGINT) 
        LEFT OUTER JOIN dbo.Customers AS C2 WITH (NOLOCK) ON CAST(CAST (PV.Volume105 AS BIGINT) AS NVARCHAR(100)) = C2.Field1  
		--LEFT JOIN Customers C3 WITH (NOLOCK) ON C3.CustomerID = PV.Volume52 
		--LEFT JOIN Customers C4 WITH (NOLOCK) ON C4.CustomerID = PV.Volume105 
                                                              
        LEFT OUTER JOIN dbo.Periods AS P2 WITH (NOLOCK) ON CAST(P2.EndDate AS DATE) = DATEADD(MONTH, 
                                                              DATEDIFF(MONTH, 
                                                              -1, CAST (P.EndDate AS DATE)) 
                                                              - 1, -1) 
															  AND P2.PeriodTypeID = 1 
        LEFT OUTER JOIN dbo.PeriodVolumes AS PV2  WITH (NOLOCK) ON PV2.CustomerID = PV.CustomerID 
                                                     AND P2.PeriodID = PV2.PeriodID  AND PV2.PeriodTypeID = 1 
		LEFT JOIN dbo.CustomerStatuses CS WITH (NOLOCK) ON CAST(PV.volume107 AS BIGINT) = CS.CustomerStatusID 
		 
 
--DEV 
--WHERE CAST(P.EndDate AS DATE) BETWEEN '01-01-2015'AND '12-31-2015' AND PV.PeriodTypeID = 1		 
 
--PROD, QAT 
WHERE CAST(P.EndDate AS DATE) >= '12-01-2017' AND PV.PeriodTypeID = 1 
 
 

------------------------ 
 
 
UNION ALL 
 
 
-- ODSY Period Volume Data 
SELECT DISTINCT 
        C.Field1 AS Medifast_Customer_Number, 
		C.CustomerID AS 'Exigo_CustomerID', 
		CAST(PV.Volume106 AS BIGINT) AS 'NestedLevel', 
        C.FirstName AS CUSTOMER_FIRST_NAME , 
        C.LastName AS CUSTOMER_LAST_NAME , 
		C.FirstName+' '+C.LastName AS CUSTOMER_FULL_NAME , 
		CAST(CAST (PV.Volume105 AS BIGINT) AS NVARCHAR(100)) AS 'Medifast_Enroller_Number', 
        C2.FirstName + ' ' + C2.LastName AS Medifast_Enroller_FULL_NAME , 
		C2.FirstName AS Medifast_Enroller_FIRST_NAME , 
		C2.LastName AS Medifast_Enroller_LAST_NAME , 
		C.EnrollerID AS Exigo_EnrollerID, 
        PV.Volume1 AS PV , 
        ISNULL(PV2.Volume1,0) AS LAST_MONTH_PV , 
		ISNULL(PV2.Volume2,0) AS LAST_MONTH_FLV , 
        PV.PaidRankID AS CURRENT_RANK , 
		PV2.PaidRankID AS Previous_RANK , 
        RM.RankDescription AS CURRENT_RANK_TEXT , 
        PV.RankID AS Highest_Rank , 
        RM2.RankDescription AS Highest_RANK_TEXT , 
        PV.Volume2 AS FLV , 
        PV.Volume35 AS ORDERING_ENTITIES , 
        PV.Volume4 AS QUALIFYING_POINTS , 
        PV.Volume31 AS SC_TEAM , 
        PV.Volume32 AS ED_TEAM , 
         
        PV.Volume34 AS FIBL_TEAM , 
        PV.Volume3 AS GV , 
		P.PeriodID, 
        CAST(P.EndDate AS DATE) AS COMMISSION_PERIOD , 
        C.Email , 
        C.MainAddress1 , 
        C.MainAddress2 , 
        C.MainAddress3 , 
        C.MainCity , 
        C.MainState , 
        C.MainZip , 
        C.MainCountry , 
        C.MainCounty , 
        C.Phone, 
		CAST(PV.volume107 AS BIGINT) AS [CustomerStatusID], 
		CS.CustomerStatusDescription, 
		CAST(PV.volume108 AS BIGINT) AS [CustomerTypeID], 
		C.Date1 AS 'EntryDate', 
		C.Date2 AS 'ActivationDate', 
		C.Date3 AS 'ReversionDate', 
		C.Date4 AS 'ReinstatementDate', 
		CAST (C.Date5 AS DATE) AS 'RenewalDate', 
		C.[Field3] AS 'Certified' , 
		CT.CustomerTypeDescription, 
		PV.Volume31 AS 'SeniorCoachTeam', 
		PV.Volume32 AS 'ExecutiveDirectorTeam', 
		PV.Volume33 AS 'FIBC_Team', 
		CASE WHEN C.Field5 = '' THEN '0' 
		ELSE C.Field5 END AS 'HP_Status', 
		NULL AS 'GlobalDirector',  --Volume52 Exigo ID...We wont have this data historically. that why it's NULL 
		NULL AS  'PresidentialDirector', 
		NULL as GZV, 
		NULL AS GzClients, 
		NULL AS GzCoaches, 
		NULL as [Earned_CAB], 
		NULL AS RequiredGV 
		FROM    dbo.Customers C  WITH (NOLOCK) 
		INNER JOIN ODSY.PeriodVolumes AS PV WITH (NOLOCK) ON C.Field1 = PV.MedifastCustomerID AND  PV.PeriodTypeID = 1 
        INNER JOIN ODSY.RanksMapping AS RM  WITH (NOLOCK)ON RM.ExigoRankID = PV.PaidRankID 
        INNER JOIN ODSY.RanksMapping AS RM2  WITH (NOLOCK)ON RM2.ExigoRankID = PV.RankID 
        INNER JOIN ODSY.Periods AS P WITH (NOLOCK) ON P.PeriodID = PV.PeriodID 
		--INNER JOIN dbo.EnrollerTree ET WITH (NOLOCK) ON ET.CustomerID = C.CustomerID 
		LEFT JOIN dbo.CustomerTypes CT WITH (NOLOCK) ON CT.CustomerTypeID = cast(PV.volume108 as bigint) 
        LEFT OUTER JOIN dbo.Customers AS C2 WITH (NOLOCK) ON CAST(CAST (PV.Volume105 AS BIGINT) AS NVARCHAR(100))= C2.Field1  
		--LEFT JOIN Customers C3 WITH (NOLOCK) ON C3.CustomerID = PV.Volume52  --doesn't exist historically 
		--LEFT JOIN Customers C4 WITH (NOLOCK) ON C4.Field1 = PV.Volume105 
                                                              
        LEFT OUTER JOIN ODSY.Periods AS P2 WITH (NOLOCK) ON CAST(P2.EndDate AS DATE) = DATEADD(MONTH, 
                                                              DATEDIFF(MONTH, 
                                                              -1, CAST (P.EndDate AS DATE)) 
                                                              - 1, -1) 
															  AND P2.PeriodTypeID = 1 
        LEFT OUTER JOIN ODSY.PeriodVolumes AS PV2 WITH (NOLOCK) ON PV2.MedifastCustomerID = PV.MedifastCustomerID 
                                                     AND P2.PeriodID = PV2.PeriodID AND  PV2.PeriodTypeID = 1 
		LEFT JOIN dbo.CustomerStatuses CS WITH (NOLOCK) ON CAST(PV.volume107 AS BIGINT) = CS.CustomerStatusID 
		 
		--DEV 
		--WHERE (CAST(P.EndDate AS DATE) BETWEEN '01-01-2011' AND '12-31-2014') OR CAST(P.EndDate AS DATE) >= '01-01-2016' 
		--PROD QAT 
		WHERE CAST(P.EndDate AS DATE) < '12-01-2017' 
 
 
 
 
 
 
 
 
 
