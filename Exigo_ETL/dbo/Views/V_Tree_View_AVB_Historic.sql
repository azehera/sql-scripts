﻿

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
CREATE VIEW [dbo].[V_Tree_View_AVB_Historic] 
AS 
 
-- =============================================  
-- Author:		Micah Williams 
-- Create date: 02/01/17  
-- Description:	This view shows monthly metrics for Optavia Customers 
-- Modification Micah W. 12-20-2017 - Correcting the following mapping 
--									 -FIBC_Team = volume33 
--									 -FIBL_Team = volume34 
--									 -ORDERING_ENTITIES = volume35 
--Modification Micah W. 41-23-2018   -chagned join for customertypedescription....Join using pv.volume108 instead of c.customertypedesctiption 
--Modification Micah W. 02-06-2019	 -Adding GZV (aka Volume162), GzClients (volume171), and GzCoaches (Volume170) to this View, Earned CAB (Volume118) 
--Modification Vick S. 03-15-2019	 -Adding RequiredGV (aka Volume153)  
--Modification Vick S. 03-28-2019 - Modified Tree View to remove odsy volume data as it was not being used for international(kept it as a comment but not being used anywhere) and casted Current_Rank and Previous_Rank to decimal 
--=============================================  
 
 
 
--dbo Period Volume Data 
SELECT DISTINCT 
        C.Field1 AS Medifast_Customer_Number, 
		C.CustomerID AS 'Exigo_CustomerID', 
		CAST(PV.Volume106 AS BIGINT) AS 'NestedLevel', 
        C.FirstName AS CUSTOMER_FIRST_NAME , 
        C.LastName AS CUSTOMER_LAST_NAME , 
		C.FirstName+' '+C.LastName AS CUSTOMER_FULL_NAME , 
		CAST(CAST (PV.Volume105 AS BIGINT) AS NVARCHAR(100)) AS 'Medifast_Enroller_Number', 
        C2.FirstName + ' ' + C2.LastName AS Medifast_Enroller_FULL_NAME , 
		C2.FirstName AS Medifast_Enroller_FIRST_NAME , 
		C2.LastName AS Medifast_Enroller_LAST_NAME , 
		C.EnrollerID AS Exigo_EnrollerID, 
        PV.Volume1 AS PV , 
        ISNULL(PV2.Volume1,0) AS LAST_MONTH_PV , 
		ISNULL(PV2.Volume2,0) AS LAST_MONTH_FLV , 
        CAST(PV.PaidRankID AS DECIMAL(18,2)) AS CURRENT_RANK , 
		CAST(PV2.PaidRankID AS DECIMAL(18,2)) AS Previous_RANK , 
        RM.RankDescription AS CURRENT_RANK_TEXT , 
        CAST(PV.volume13 AS DECIMAL(18,2)) AS Highest_Rank , 
        RM2.RankDescription AS Highest_RANK_TEXT , 
        PV.Volume2 AS FLV , 
        PV.Volume35 AS ORDERING_ENTITIES , 
        PV.Volume4 AS QUALIFYING_POINTS , 
        PV.Volume31 AS SC_TEAM , 
        PV.Volume32 AS ED_TEAM , 
        
        PV.Volume34 AS FIBL_TEAM , 
        PV.Volume3 AS GV , 
		P.PeriodID, 
        CAST(P.EndDate AS DATE) AS COMMISSION_PERIOD , 
        C.Email , 
        C.MainAddress1 , 
        C.MainAddress2 , 
        C.MainAddress3 , 
        C.MainCity , 
        C.MainState , 
        C.MainZip , 
        C.MainCountry , 
        C.MainCounty , 
        C.Phone, 
		CAST(PV.volume107 AS BIGINT) AS [CustomerStatusID], 
		CS.CustomerStatusDescription, 
		CAST(PV.volume108 AS BIGINT) AS [CustomerTypeID], 
		C.Date1 AS 'EntryDate', 
		C.Date2 AS 'ActivationDate', 
		C.Date3 AS 'ReversionDate', 
		C.Date4 AS 'ReinstatementDate', 
		CAST (C.Date5 AS DATE) AS 'RenewalDate', 
		C.[Field3] AS 'Certified',  
		CT.CustomerTypeDescription, 
		PV.Volume31 AS 'SeniorCoachTeam', 
		PV.Volume32 AS 'ExecutiveDirectorTeam', 
		PV.Volume33 AS 'FIBC_Team', 
		CASE WHEN C.Field5 = '' THEN '0' 
		ELSE C.Field5 END AS 'HP_Status', 
		CAST(CAST(PV.Volume52 AS BIGINT) AS NVARCHAR(100)) AS 'GlobalDirector',  --Volume52 Exigo ID 
		CAST(CAST(PV.Volume104 AS BIGINT) AS NVARCHAR(100)) AS 'PresidentialDirector', 
		PV.Volume162 AS GZV , 
		PV.Volume171 AS GzClients, 
		PV.Volume170 AS GzCoaches, 
		PV.volume18 AS [Earned_CAB], 
		PV.Volume153 AS RequiredGV, 
		PV.[Volume1], 
		PV.[Volume2], 
		PV.[Volume3], 
		PV.[Volume4], 
		PV.[Volume5], 
		PV.[Volume6], 
		PV.[Volume7], 
		PV.[Volume8], 
		PV.[Volume9], 
		PV.[Volume10], 
		PV.[Volume11], 
		PV.[Volume12], 
		PV.[Volume13], 
		PV.[Volume14], 
		PV.[Volume15], 
		PV.[Volume16], 
		PV.[Volume17], 
		PV.[Volume18], 
		PV.[Volume19], 
		PV.[Volume20], 
		PV.[Volume21], 
		PV.[Volume22], 
		PV.[Volume23], 
		PV.[Volume24], 
		PV.[Volume25], 
		PV.[Volume26], 
		PV.[Volume27], 
		PV.[Volume28], 
		PV.[Volume29], 
		PV.[Volume30], 
		PV.[Volume31], 
		PV.[Volume32], 
		PV.[Volume33], 
		PV.[Volume34], 
		PV.[Volume35], 
		PV.[Volume36], 
		PV.[Volume37], 
		PV.[Volume38], 
		PV.[Volume39], 
		PV.[Volume40], 
		PV.[Volume41], 
		PV.[Volume42], 
		PV.[Volume43], 
		PV.[Volume44], 
		PV.[Volume45], 
		PV.[Volume46], 
		PV.[Volume47], 
		PV.[Volume48], 
		PV.[Volume49], 
		PV.[Volume50], 
		PV.[Volume51], 
		PV.[Volume52], 
		PV.[Volume53], 
		PV.[Volume54], 
		PV.[Volume55], 
		PV.[Volume56], 
		PV.[Volume57], 
		PV.[Volume58], 
		PV.[Volume59], 
		PV.[Volume60], 
		PV.[Volume61], 
		PV.[Volume62], 
		PV.[Volume63], 
		PV.[Volume64], 
		PV.[Volume65], 
		PV.[Volume66], 
		PV.[Volume67], 
		PV.[Volume68], 
		PV.[Volume69], 
		PV.[Volume70], 
		PV.[Volume71], 
		PV.[Volume72], 
		PV.[Volume73], 
		PV.[Volume74], 
		PV.[Volume75], 
		PV.[Volume76], 
		PV.[Volume77], 
		PV.[Volume78], 
		PV.[Volume79], 
		PV.[Volume80], 
		PV.[Volume81], 
		PV.[Volume82], 
		PV.[Volume83], 
		PV.[Volume84], 
		PV.[Volume85], 
		PV.[Volume86], 
		PV.[Volume87], 
		PV.[Volume88], 
		PV.[Volume89], 
		PV.[Volume90], 
		PV.[Volume91], 
		PV.[Volume92], 
		PV.[Volume93], 
		PV.[Volume94], 
		PV.[Volume95], 
		PV.[Volume96], 
		PV.[Volume97], 
		PV.[Volume98], 
		PV.[Volume99], 
		PV.[Volume100], 
		PV.[Volume101], 
		PV.[Volume102], 
		PV.[Volume103], 
		PV.[Volume104], 
		PV.[Volume105], 
		PV.[Volume106], 
		PV.[Volume107], 
		PV.[Volume108], 
		PV.[Volume109], 
		PV.[Volume110], 
		PV.[Volume111], 
		PV.[Volume112], 
		PV.[Volume113], 
		PV.[Volume114], 
		PV.[Volume115], 
		PV.[Volume116], 
		PV.[Volume117], 
		PV.[Volume118], 
		PV.[Volume119], 
		PV.[Volume120], 
		PV.[Volume121], 
		PV.[Volume122], 
		PV.[Volume123], 
		PV.[Volume124], 
		PV.[Volume125], 
		PV.[Volume126], 
		PV.[Volume127], 
		PV.[Volume128], 
		PV.[Volume129], 
		PV.[Volume130], 
		PV.[Volume131], 
		PV.[Volume132], 
		PV.[Volume133], 
		PV.[Volume134], 
		PV.[Volume135], 
		PV.[Volume136], 
		PV.[Volume137], 
		PV.[Volume138], 
		PV.[Volume139], 
		PV.[Volume140], 
		PV.[Volume141], 
		PV.[Volume142], 
		PV.[Volume143], 
		PV.[Volume144], 
		PV.[Volume145], 
		PV.[Volume146], 
		PV.[Volume147], 
		PV.[Volume148], 
		PV.[Volume149], 
		PV.[Volume150], 
		PV.[Volume151], 
		PV.[Volume152], 
		PV.[Volume153], 
		PV.[Volume154], 
		PV.[Volume155], 
		PV.[Volume156], 
		PV.[Volume157], 
		PV.[Volume158], 
		PV.[Volume159], 
		PV.[Volume160], 
		PV.[Volume161], 
		PV.[Volume162], 
		PV.[Volume163], 
		PV.[Volume164], 
		PV.[Volume165], 
		PV.[Volume166], 
		PV.[Volume167], 
		PV.[Volume168], 
		PV.[Volume169], 
		PV.[Volume170], 
		PV.[Volume171], 
		PV.[Volume172], 
		PV.[Volume173], 
		PV.[Volume174], 
		PV.[Volume175], 
		PV.[Volume176], 
		PV.[Volume177], 
		PV.[Volume178], 
		PV.[Volume179], 
		PV.[Volume180], 
		PV.[Volume181], 
		PV.[Volume182], 
		PV.[Volume183], 
		PV.[Volume184], 
		PV.[Volume185], 
		PV.[Volume186], 
		PV.[Volume187], 
		PV.[Volume188], 
		PV.[Volume189], 
		PV.[Volume190], 
		PV.[Volume191], 
		PV.[Volume192], 
		PV.[Volume193], 
		PV.[Volume194], 
		PV.[Volume195], 
		PV.[Volume196], 
		PV.[Volume197], 
		PV.[Volume198], 
		PV.[Volume199], 
		PV.[Volume200] 
		FROM    dbo.Customers C WITH (NOLOCK) 
		INNER JOIN dbo.PeriodVolumes  AS PV WITH (NOLOCK) ON C.CustomerID = PV.CustomerID AND  PV.PeriodTypeID = 1 
        INNER JOIN ODSY.RanksMapping AS RM WITH (NOLOCK) ON RM.ExigoRankID = PV.PaidRankID 
        INNER JOIN ODSY.RanksMapping AS RM2 WITH (NOLOCK) ON RM2.ExigoRankID = CAST(PV.volume13 AS DECIMAL(18,2)) 
        INNER JOIN dbo.Periods AS P WITH (NOLOCK) ON P.PeriodID = PV.PeriodID AND P.PeriodTypeID = 1 
		--INNER JOIN dbo.EnrollerTree ET WITH (NOLOCK) ON ET.CustomerID = C.CustomerID 
		LEFT JOIN dbo.CustomerTypes CT  WITH (NOLOCK) ON CT.CustomerTypeID = CAST(PV.volume108 AS BIGINT) 
        LEFT OUTER JOIN dbo.Customers AS C2 WITH (NOLOCK) ON CAST(CAST (PV.Volume105 AS BIGINT) AS NVARCHAR(100)) = C2.Field1  
		--LEFT JOIN Customers C3 WITH (NOLOCK) ON C3.CustomerID = PV.Volume52 
		--LEFT JOIN Customers C4 WITH (NOLOCK) ON C4.CustomerID = PV.Volume105 
                                                              
        LEFT OUTER JOIN dbo.Periods AS P2 WITH (NOLOCK) ON CAST(P2.EndDate AS DATE) = DATEADD(MONTH, 
                                                              DATEDIFF(MONTH, 
                                                              -1, CAST (P.EndDate AS DATE)) 
                                                              - 1, -1) 
															  AND P2.PeriodTypeID = 1 
        LEFT OUTER JOIN dbo.PeriodVolumes AS PV2  WITH (NOLOCK) ON PV2.CustomerID = PV.CustomerID 
                                                     AND P2.PeriodID = PV2.PeriodID  AND PV2.PeriodTypeID = 1 
		LEFT JOIN dbo.CustomerStatuses CS WITH (NOLOCK) ON CAST(PV.volume107 AS BIGINT) = CS.CustomerStatusID 
		 
 
--DEV 
--WHERE CAST(P.EndDate AS DATE) BETWEEN '01-01-2015'AND '12-31-2015' AND PV.PeriodTypeID = 1		 
 
--PROD, QAT 
WHERE CAST(P.EndDate AS DATE) >= '12-01-2017' AND PV.PeriodTypeID = 1 
 
 
 
------------------------ 
 
 
UNION ALL 
 
 
-- ODSY Period Volume Data 
SELECT DISTINCT 
        C.Field1 AS Medifast_Customer_Number, 
		C.CustomerID AS 'Exigo_CustomerID', 
		CAST(PV.Volume106 AS BIGINT) AS 'NestedLevel', 
        C.FirstName AS CUSTOMER_FIRST_NAME , 
        C.LastName AS CUSTOMER_LAST_NAME , 
		C.FirstName+' '+C.LastName AS CUSTOMER_FULL_NAME , 
		CAST(CAST (PV.Volume105 AS BIGINT) AS NVARCHAR(100)) AS 'Medifast_Enroller_Number', 
        C2.FirstName + ' ' + C2.LastName AS Medifast_Enroller_FULL_NAME , 
		C2.FirstName AS Medifast_Enroller_FIRST_NAME , 
		C2.LastName AS Medifast_Enroller_LAST_NAME , 
		C.EnrollerID AS Exigo_EnrollerID, 
        PV.Volume1 AS PV , 
        ISNULL(PV2.Volume1,0) AS LAST_MONTH_PV , 
		ISNULL(PV2.Volume2,0) AS LAST_MONTH_FLV , 
        PV.PaidRankID AS CURRENT_RANK , 
		PV2.PaidRankID AS Previous_RANK , 
        RM.RankDescription AS CURRENT_RANK_TEXT , 
        PV.RankID AS Highest_Rank , 
        RM2.RankDescription AS Highest_RANK_TEXT , 
        PV.Volume2 AS FLV , 
        PV.Volume35 AS ORDERING_ENTITIES , 
        PV.Volume4 AS QUALIFYING_POINTS , 
        PV.Volume31 AS SC_TEAM , 
        PV.Volume32 AS ED_TEAM , 
         
        PV.Volume34 AS FIBL_TEAM , 
        PV.Volume3 AS GV , 
		P.PeriodID, 
        CAST(P.EndDate AS DATE) AS COMMISSION_PERIOD , 
        C.Email , 
        C.MainAddress1 , 
        C.MainAddress2 , 
        C.MainAddress3 , 
        C.MainCity , 
        C.MainState , 
        C.MainZip , 
        C.MainCountry , 
        C.MainCounty , 
        C.Phone, 
		CAST(PV.volume107 AS BIGINT) AS [CustomerStatusID], 
		CS.CustomerStatusDescription, 
		CAST(PV.volume108 AS BIGINT) AS [CustomerTypeID], 
		C.Date1 AS 'EntryDate', 
		C.Date2 AS 'ActivationDate', 
		C.Date3 AS 'ReversionDate', 
		C.Date4 AS 'ReinstatementDate', 
		CAST (C.Date5 AS DATE) AS 'RenewalDate', 
		C.[Field3] AS 'Certified' , 
		CT.CustomerTypeDescription, 
		PV.Volume31 AS 'SeniorCoachTeam', 
		PV.Volume32 AS 'ExecutiveDirectorTeam', 
		PV.Volume33 AS 'FIBC_Team', 
		CASE WHEN C.Field5 = '' THEN '0' 
		ELSE C.Field5 END AS 'HP_Status', 
		NULL AS 'GlobalDirector',  --Volume52 Exigo ID...We wont have this data historically. that why it's NULL 
		NULL AS  'PresidentialDirector', 
		NULL as GZV, 
		NULL AS GzClients, 
		NULL AS GzCoaches, 
		NULL as [Earned_CAB], 
		Null AS RequiredGV, 
		PV.[Volume1], 
		PV.[Volume2], 
		PV.[Volume3], 
		PV.[Volume4], 
		PV.[Volume5], 
		PV.[Volume6], 
		PV.[Volume7], 
		PV.[Volume8], 
		PV.[Volume9], 
		PV.[Volume10], 
		PV.[Volume11], 
		PV.[Volume12], 
		PV.[Volume13], 
		PV.[Volume14], 
		PV.[Volume15], 
		PV.[Volume16], 
		PV.[Volume17], 
		PV.[Volume18], 
		PV.[Volume19], 
		PV.[Volume20], 
		PV.[Volume21], 
		PV.[Volume22], 
		PV.[Volume23], 
		PV.[Volume24], 
		PV.[Volume25], 
		PV.[Volume26], 
		PV.[Volume27], 
		PV.[Volume28], 
		PV.[Volume29], 
		PV.[Volume30], 
		PV.[Volume31], 
		PV.[Volume32], 
		PV.[Volume33], 
		PV.[Volume34], 
		PV.[Volume35], 
		PV.[Volume36], 
		PV.[Volume37], 
		PV.[Volume38], 
		PV.[Volume39], 
		PV.[Volume40], 
		PV.[Volume41], 
		PV.[Volume42], 
		PV.[Volume43], 
		PV.[Volume44], 
		PV.[Volume45], 
		PV.[Volume46], 
		PV.[Volume47], 
		PV.[Volume48], 
		PV.[Volume49], 
		PV.[Volume50], 
		PV.[Volume51], 
		PV.[Volume52], 
		PV.[Volume53], 
		PV.[Volume54], 
		PV.[Volume55], 
		PV.[Volume56], 
		PV.[Volume57], 
		PV.[Volume58], 
		PV.[Volume59], 
		PV.[Volume60], 
		PV.[Volume61], 
		PV.[Volume62], 
		PV.[Volume63], 
		PV.[Volume64], 
		PV.[Volume65], 
		PV.[Volume66], 
		PV.[Volume67], 
		PV.[Volume68], 
		PV.[Volume69], 
		PV.[Volume70], 
		PV.[Volume71], 
		PV.[Volume72], 
		PV.[Volume73], 
		PV.[Volume74], 
		PV.[Volume75], 
		PV.[Volume76], 
		PV.[Volume77], 
		PV.[Volume78], 
		PV.[Volume79], 
		PV.[Volume80], 
		PV.[Volume81], 
		PV.[Volume82], 
		PV.[Volume83], 
		PV.[Volume84], 
		PV.[Volume85], 
		PV.[Volume86], 
		PV.[Volume87], 
		PV.[Volume88], 
		PV.[Volume89], 
		PV.[Volume90], 
		PV.[Volume91], 
		PV.[Volume92], 
		PV.[Volume93], 
		PV.[Volume94], 
		PV.[Volume95], 
		PV.[Volume96], 
		PV.[Volume97], 
		PV.[Volume98], 
		PV.[Volume99], 
		PV.[Volume100], 
		PV.[Volume101], 
		PV.[Volume102], 
		PV.[Volume103], 
		PV.[Volume104], 
		PV.[Volume105], 
		PV.[Volume106], 
		PV.[Volume107], 
		PV.[Volume108], 
		PV.[Volume109], 
		PV.[Volume110], 
		PV.[Volume111], 
		PV.[Volume112], 
		PV.[Volume113], 
		PV.[Volume114], 
		PV.[Volume115], 
		PV.[Volume116], 
		PV.[Volume117], 
		PV.[Volume118], 
		PV.[Volume119], 
		PV.[Volume120], 
		PV.[Volume121], 
		PV.[Volume122], 
		PV.[Volume123], 
		PV.[Volume124], 
		PV.[Volume125], 
		PV.[Volume126], 
		PV.[Volume127], 
		PV.[Volume128], 
		PV.[Volume129], 
		PV.[Volume130], 
		PV.[Volume131], 
		PV.[Volume132], 
		PV.[Volume133], 
		PV.[Volume134], 
		PV.[Volume135], 
		PV.[Volume136], 
		PV.[Volume137], 
		PV.[Volume138], 
		PV.[Volume139], 
		PV.[Volume140], 
		PV.[Volume141], 
		PV.[Volume142], 
		PV.[Volume143], 
		PV.[Volume144], 
		PV.[Volume145], 
		PV.[Volume146], 
		PV.[Volume147], 
		PV.[Volume148], 
		PV.[Volume149], 
		PV.[Volume150], 
		PV.[Volume151], 
		PV.[Volume152], 
		PV.[Volume153], 
		PV.[Volume154], 
		PV.[Volume155], 
		PV.[Volume156], 
		PV.[Volume157], 
		PV.[Volume158], 
		PV.[Volume159], 
		PV.[Volume160], 
		PV.[Volume161], 
		PV.[Volume162], 
		PV.[Volume163], 
		PV.[Volume164], 
		PV.[Volume165], 
		PV.[Volume166], 
		PV.[Volume167], 
		PV.[Volume168], 
		PV.[Volume169], 
		PV.[Volume170], 
		PV.[Volume171], 
		PV.[Volume172], 
		PV.[Volume173], 
		PV.[Volume174], 
		PV.[Volume175], 
		PV.[Volume176], 
		PV.[Volume177], 
		PV.[Volume178], 
		PV.[Volume179], 
		PV.[Volume180], 
		PV.[Volume181], 
		PV.[Volume182], 
		PV.[Volume183], 
		PV.[Volume184], 
		PV.[Volume185], 
		PV.[Volume186], 
		PV.[Volume187], 
		PV.[Volume188], 
		PV.[Volume189], 
		PV.[Volume190], 
		PV.[Volume191], 
		PV.[Volume192], 
		PV.[Volume193], 
		PV.[Volume194], 
		PV.[Volume195], 
		PV.[Volume196], 
		PV.[Volume197], 
		PV.[Volume198], 
		PV.[Volume199], 
		PV.[Volume200] 
		FROM    dbo.Customers C  WITH (NOLOCK) 
		INNER JOIN ODSY.PeriodVolumes AS PV WITH (NOLOCK) ON C.Field1 = PV.MedifastCustomerID AND  PV.PeriodTypeID = 1 
        INNER JOIN ODSY.RanksMapping AS RM  WITH (NOLOCK)ON RM.ExigoRankID = PV.PaidRankID 
        INNER JOIN ODSY.RanksMapping AS RM2  WITH (NOLOCK)ON RM2.ExigoRankID = PV.RankID 
        INNER JOIN ODSY.Periods AS P WITH (NOLOCK) ON P.PeriodID = PV.PeriodID 
		--INNER JOIN dbo.EnrollerTree ET WITH (NOLOCK) ON ET.CustomerID = C.CustomerID 
		LEFT JOIN dbo.CustomerTypes CT WITH (NOLOCK) ON CT.CustomerTypeID = cast(PV.volume108 as bigint) 
        LEFT OUTER JOIN dbo.Customers AS C2 WITH (NOLOCK) ON CAST(CAST (PV.Volume105 AS BIGINT) AS NVARCHAR(100))= C2.Field1  
		--LEFT JOIN Customers C3 WITH (NOLOCK) ON C3.CustomerID = PV.Volume52  --doesn't exist historically 
		--LEFT JOIN Customers C4 WITH (NOLOCK) ON C4.Field1 = PV.Volume105 
                                                              
        LEFT OUTER JOIN ODSY.Periods AS P2 WITH (NOLOCK) ON CAST(P2.EndDate AS DATE) = DATEADD(MONTH, 
                                                              DATEDIFF(MONTH, 
                                                              -1, CAST (P.EndDate AS DATE)) 
                                                              - 1, -1) 
															  AND P2.PeriodTypeID = 1 
        LEFT OUTER JOIN ODSY.PeriodVolumes AS PV2 WITH (NOLOCK) ON PV2.MedifastCustomerID = PV.MedifastCustomerID 
                                                     AND P2.PeriodID = PV2.PeriodID AND  PV2.PeriodTypeID = 1 
		LEFT JOIN dbo.CustomerStatuses CS WITH (NOLOCK) ON CAST(PV.volume107 AS BIGINT) = CS.CustomerStatusID 
		 
		--DEV 
		--WHERE (CAST(P.EndDate AS DATE) BETWEEN '01-01-2011' AND '12-31-2014') OR CAST(P.EndDate AS DATE) >= '01-01-2016' 
		--PROD QAT 
		WHERE CAST(P.EndDate AS DATE) < '12-01-2017' 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
