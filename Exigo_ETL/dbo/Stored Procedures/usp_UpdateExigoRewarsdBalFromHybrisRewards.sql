﻿--USE Exigo_ETL

CREATE PROCEDURE [dbo].[usp_UpdateExigoRewarsdBalFromHybrisRewards]

AS
/**********************************************************************
Developer: Derald Smith
Date: 2/21/2019
Decsription: Used to update the rewards balanaces in Exigo SyncSQL from
Hybris customer rewards data. REF: HYBNEW-3293
--------------------------- Objects Used ------------------------------
[ECOMM].mdf_prd_database.customerrewards
[ECOMM].mdf_prd_database.users

EXIGO_UAT.ExigoSyncSQL_Sandbox5.custom.Customer_Info
-------------------------- MODIFICATIONS -------------------------------

3/4/2019 - Changed D.Smith - due to incorrect query initially provided by vendor.
 Reference: RFC-109 - Deploy modification to Hybris to Exigo Rewards Sync Process

***********************************************************************/

/********************** GET the Hybris Rewards data: **********************/

if object_id('Tempdb..#HybrisRewards') is not NULL DROP table #HybrisRewards;

--SELECT * INTO #HybrisRewards
--FROM OPENQUERY(ECOMM,'SELECT cr.createdTS as ''TranDate'',
--cast(u.p_cartcustnumber as char(255)) as ''Medifast_Customer_Number'',
--cast(u.p_uid as char(255)) as ''uid'',
--cast(cr.p_balance as decimal(30,3)) as ''Available_Rewards_Balance'',
--cr.p_expirationdate as ''Rewards_Expiration_Date''
--from users u  join customerrewards cr on cr.OwnerPkString = u.pk 
--and CAST(cr.modifiedTS AS date) >= CAST(date_add(sysdate(), INTERVAL -1 DAY) as date)')

--3/4/2019 - Changed D.Smith - due to incorrect query initially provided by vendor.
SELECT * INTO #HybrisRewards
FROM OPENQUERY(ECOMM,'SELECT cr.createdTS as ''TranDate'',
cast(u.p_cartcustnumber as char(255)) as ''Medifast_Customer_Number'',
cast(u.p_uid as char(255)) as ''uid'',
cast(cr.p_balance as decimal(30,3)) as ''Available_Rewards_Balance'',
cr.p_expirationdate as ''Rewards_Expiration_Date''
from customerrewards cr 
join users u on u.p_rewardsinformation = cr.pk 
WHERE CAST(cr.modifiedTS AS date) >= CAST(date_add(sysdate(), INTERVAL -1 DAY) as date)')


/********************* Update Exigo Rewards Balances: **************************/

UPDATE EXIGO.ExigoSyncSQL_PRD.custom.Customer_Info
--SELECT CI.Medifast_Customer_Number ,
SET  Available_Rewards_Balance = CAST(H.Available_Rewards_Balance AS MONEY),
	Rewards_Expiration_Date = H.Rewards_Expiration_Date
FROM EXIGO.ExigoSyncSQL_PRD.custom.Customer_Info CI
JOIN #HybrisRewards H ON H.[Medifast_Customer_Number] = CI.Medifast_Customer_Number




