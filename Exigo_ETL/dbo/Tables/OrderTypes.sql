﻿CREATE TABLE [dbo].[OrderTypes] (
    [OrderTypeID]          INT           NOT NULL,
    [OrderTypeDescription] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_OrderTypes] PRIMARY KEY CLUSTERED ([OrderTypeID] ASC) WITH (FILLFACTOR = 90)
);

