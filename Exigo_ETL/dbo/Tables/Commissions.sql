﻿CREATE TABLE [dbo].[Commissions] (
    [CommissionRunID] INT          NOT NULL,
    [CustomerID]      INT          NOT NULL,
    [CurrencyCode]    NVARCHAR (3) NOT NULL,
    [Earnings]        MONEY        NOT NULL,
    [PreviousBalance] MONEY        NOT NULL,
    [BalanceForward]  MONEY        NOT NULL,
    [Fee]             MONEY        NOT NULL,
    [Total]           MONEY        NOT NULL,
    CONSTRAINT [PK_Commissions] PRIMARY KEY CLUSTERED ([CommissionRunID] ASC, [CustomerID] ASC) WITH (FILLFACTOR = 85)
);

