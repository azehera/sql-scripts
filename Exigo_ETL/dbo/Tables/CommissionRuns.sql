﻿CREATE TABLE [dbo].[CommissionRuns] (
    [CommissionRunID]          INT            NOT NULL,
    [CommissionRunDescription] NVARCHAR (100) NOT NULL,
    [PeriodTypeID]             INT            NOT NULL,
    [PeriodID]                 INT            NOT NULL,
    [RunDate]                  DATETIME       NULL,
    [AcceptedDate]             DATETIME       NULL,
    [CommissionRunStatusID]    INT            NOT NULL,
    [HideFromWeb]              BIT            NOT NULL,
    [PlanID]                   INT            NULL,
    CONSTRAINT [PK_CommissionRuns] PRIMARY KEY CLUSTERED ([CommissionRunID] ASC) WITH (FILLFACTOR = 85)
);

