﻿CREATE TABLE [dbo].[CommissionBonuses] (
    [CommissionRunID] INT   NOT NULL,
    [CustomerID]      INT   NOT NULL,
    [BonusID]         INT   NOT NULL,
    [Amount]          MONEY NOT NULL,
    CONSTRAINT [PK_CommissionBonuses] PRIMARY KEY CLUSTERED ([CommissionRunID] ASC, [CustomerID] ASC, [BonusID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_CommissionBonuses_CommissionRunID_BonusID_Amount]
    ON [dbo].[CommissionBonuses]([CommissionRunID] ASC, [BonusID] ASC, [Amount] ASC)
    INCLUDE([CustomerID]) WITH (FILLFACTOR = 90);

