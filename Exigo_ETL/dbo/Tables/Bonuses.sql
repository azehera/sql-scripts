﻿CREATE TABLE [dbo].[Bonuses] (
    [BonusID]          INT           NOT NULL,
    [BonusDescription] NVARCHAR (50) NOT NULL,
    [PeriodTypeID]     INT           NOT NULL,
    CONSTRAINT [PK_Bonuses] PRIMARY KEY CLUSTERED ([BonusID] ASC) WITH (FILLFACTOR = 90)
);

