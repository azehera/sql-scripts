﻿CREATE TABLE [dbo].[CustomerStatuses] (
    [CustomerStatusID]          INT           NOT NULL,
    [CustomerStatusDescription] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_CustomerStatuses] PRIMARY KEY CLUSTERED ([CustomerStatusID] ASC) WITH (FILLFACTOR = 90)
);

