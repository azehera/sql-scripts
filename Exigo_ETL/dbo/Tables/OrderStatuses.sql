﻿CREATE TABLE [dbo].[OrderStatuses] (
    [OrderStatusID]          INT           NOT NULL,
    [OrderStatusDescription] NVARCHAR (50) NULL,
    CONSTRAINT [PK_OrderStatuses] PRIMARY KEY CLUSTERED ([OrderStatusID] ASC) WITH (FILLFACTOR = 90)
);

