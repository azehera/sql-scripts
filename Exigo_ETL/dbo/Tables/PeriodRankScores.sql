﻿CREATE TABLE [dbo].[PeriodRankScores] (
    [PeriodTypeID] INT   NOT NULL,
    [PeriodID]     INT   NOT NULL,
    [CustomerID]   INT   NOT NULL,
    [PaidRankID]   INT   NOT NULL,
    [Score]        MONEY NOT NULL,
    CONSTRAINT [PK_PeriodRankScores] PRIMARY KEY CLUSTERED ([PeriodTypeID] ASC, [PeriodID] ASC, [CustomerID] ASC, [PaidRankID] ASC) WITH (FILLFACTOR = 90)
);

