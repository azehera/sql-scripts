﻿CREATE TABLE [dbo].[Periods] (
    [PeriodTypeID]      INT           NOT NULL,
    [PeriodID]          INT           NOT NULL,
    [PeriodDescription] NVARCHAR (50) NOT NULL,
    [StartDate]         DATETIME      NOT NULL,
    [EndDate]           DATETIME      NOT NULL,
    [AcceptedDate]      DATETIME      NULL,
    CONSTRAINT [PK_Periods] PRIMARY KEY CLUSTERED ([PeriodTypeID] ASC, [PeriodID] ASC) WITH (FILLFACTOR = 85)
);

