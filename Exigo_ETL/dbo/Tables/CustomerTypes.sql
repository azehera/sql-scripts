﻿CREATE TABLE [dbo].[CustomerTypes] (
    [CustomerTypeID]          INT           NOT NULL,
    [CustomerTypeDescription] NVARCHAR (50) NOT NULL,
    [PriceTypeID]             INT           NOT NULL,
    CONSTRAINT [PK_CustomerTypes] PRIMARY KEY CLUSTERED ([CustomerTypeID] ASC) WITH (FILLFACTOR = 90)
);

