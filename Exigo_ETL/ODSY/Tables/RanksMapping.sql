﻿CREATE TABLE [ODSY].[RanksMapping] (
    [ExigoRankID]      INT             NULL,
    [RankDescription]  NVARCHAR (50)   NULL,
    [ODSY_RANKID]      DECIMAL (18, 2) NULL,
    [ODSY_DESCRIPTION] NVARCHAR (50)   NULL,
    [Cube_New_Rank_ID] INT             NULL
);

