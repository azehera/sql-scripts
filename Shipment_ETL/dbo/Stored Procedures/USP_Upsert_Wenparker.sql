﻿CREATE PROCEDURE [dbo].[USP_Upsert_Wenparker]
as
--------------------Delete Duplicates from Staging table--------------------------
with cte as
(select row_number() over (partition by OrderNo order by Filename desc) as N, * 
from  [SHIPMENT_ETL].[dbo].[wenparker_shipping_summary_STG])
delete from cte where N>1;
 
-------------------Upsert operation and deleting the old records--------------------------
MERGE [SHIPMENT_ETL].[dbo].[wenparker_shipping_summary] AS target
USING [SHIPMENT_ETL].[dbo].[wenparker_shipping_summary_STG] AS SOURCE
ON (target.OrderNo =SOURCE.OrderNo)
WHEN MATCHED 
THEN UPDATE SET target.[OrderType] = SOURCE.[OrderType] , 
                target.[Hold] = SOURCE.[Hold] ,
                target.[Cancel] = SOURCE.[Cancel] ,
                target.[ShipToRegion] =SOURCE.[ShipToRegion],
                target.[LastCarrier] = SOURCE.[LastCarrier],
                target.[DropDate] = SOURCE.[DropDate] ,
                target.[PackDate]=SOURCE.[PackDate],
                target.[ShipDate]=SOURCE.[ShipDate],
                target.[CancelDate]=SOURCE.[CancelDate],
                target.[Reason]=SOURCE.[Reason],
                target.[Modified_Date]=getdate(),
                target.[Modified_By]='ETL_User',
                target.[Filename]=SOURCE.[Filename]
WHEN NOT MATCHED BY TARGET
THEN INSERT ([OrderNo],[OrderType],[Hold],[Cancel],[ShipToRegion],[LastCarrier],[DropDate],[PackDate],[ShipDate],[CancelDate],
            [Reason],[Created_By],[Created_Date],[Filename]) 
              VALUES (SOURCE.[OrderNo],SOURCE.[OrderType],SOURCE.[Hold],SOURCE.[Cancel],SOURCE.[ShipToRegion],SOURCE.[LastCarrier],
              SOURCE.[DropDate],SOURCE.[PackDate],SOURCE.[ShipDate],SOURCE.[CancelDate],SOURCE.[Reason],
              SOURCE.[Created_By],SOURCE.[Created_Date],SOURCE.[Filename]);
