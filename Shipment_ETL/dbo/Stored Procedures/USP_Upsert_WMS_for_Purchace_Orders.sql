﻿


CREATE PROCEDURE [dbo].[USP_Upsert_WMS_for_Purchace_Orders]
as

DELETE FROM [SHIPMENT_ETL].[dbo].[PURCHASE_ORDER_RECEIPT_WAREHOUSE]
WHERE ALTERNATE_KEY IN (

SELECT ALTERNATE_KEY 
FROM [SHIPMENT_ETL].[dbo].[PURCHASE_ORDER_RECEIPT_WAREHOUSE] porw
WHERE NOT EXISTS 
(SELECT * FROM (

SELECT po_mst.invnum                                                      purchase_order_number,
       rcv.trlr_id                                                        shipment_number,
       'USA_MDC'                                                          org_code,
       po_mst.invlin                                                      po_line_number,
       CASE
           WHEN isnull (po_mst.expqty, 0) > SUM (isnull (rcv.expqty, 0)) OVER (PARTITION BY rcv.po_num, rcv.invlin) THEN
               isnull (po_mst.expqty, 0)
           ELSE
               SUM (isnull (rcv.expqty, 0)) OVER (PARTITION BY rcv.po_num, rcv.invlin)
       END                                                                ordered_quantity,
       isnull (po_mst.expqty, 0)                                          shipped_quantity,
       isnull (rcv.rcvqty, 0)                                             received_quantity,
       rcv.destination_subinventory,
       isnull (rcv.receipt_creation_date, po_mst.moddte)                  receipt_creation_date,
       rcv.receipt_date,
       po_mst.prtnum item_number,
       rcv.lot_number,
       rcv.lot_expire_dte,
       isnull (rcv.receipt_status, 'EX')								  receipt_status,
       isnull (rcv.alternate_key, po_mst.invnum + '|' + CAST(po_mst.invlin AS nvarchar))    alternate_key
  FROM (SELECT rlin.invnum, CAST(rlin.invlin AS int) invlin, rlin.prtnum, rlin.expqty, format (rlin.moddte, 'yyyy-MM-dd HH:mm:ss') moddte
          FROM [WMS].[rpwmsprdmdc].dbo.rimhdr rhdr,
               [WMS].[rpwmsprdmdc].dbo.rimlin rlin
         WHERE rhdr.invnum = rlin.invnum AND rhdr.invtyp = 'PREC') po_mst
       LEFT OUTER JOIN
       (  SELECT SUM (mdc_po.expqty)            expqty,
                 SUM (mdc_po.rcvqty)            rcvqty,
                 mdc_po.po_num,
                 mdc_po.invlin,
                 mdc_po.trlr_id,
				 format (mdc_po.receipt_creation_date, 'yyyy-MM-dd HH:mm:ss') receipt_creation_date,
                 format (mdc_po.receipt_date, 'yyyy-MM-dd HH:mm:ss') receipt_date,
                 mdc_po.receipt_status,
                 mdc_po.item_number,
                 mdc_po.lot_number,
                 format (mdc_po.lot_expire_dte, 'yyyy-MM-dd HH:mm:ss') lot_expire_dte,
                 mdc_po.destination_subinventory,
                 MAX (mdc_po.alternate_key)     alternate_key
            FROM (SELECT rclin.expqty,
                         rclin.rcvqty,
                         rci.po_num,
                         CAST(rclin.invlin AS int) invlin,
                         rct.trlr_id,
						 rct.moddte			 receipt_creation_date,
                         rct.clsdte          receipt_date,
                         rct.rcvtrk_stat     receipt_status,
                         rclin.prtnum        item_number,
                          CAST(rclin.lotnum AS NVARCHAR)
                         + CASE
                               WHEN rclin.lotnum IS NOT NULL AND rclin.expire_dte IS NOT NULL THEN
                                   '-' + format (rclin.expire_dte, 'MMddyyyy')
                               ELSE
                                   ''
                           END               lot_number,
                         rclin.expire_dte    lot_expire_dte,
                         rclin.rcvsts        destination_subinventory,
                         rclin.rcvkey        alternate_key
                    FROM [WMS].[rpwmsprdmdc].dbo.rcvinv rci,
                         [WMS].[rpwmsprdmdc].dbo.rcvtrk rct,
                         [WMS].[rpwmsprdmdc].dbo.rcvlin rclin
                   WHERE     rct.trknum = rclin.trknum
                         AND rct.trknum = rci.trknum
                         AND rci.invtyp = 'PREC'
                  UNION
                  SELECT rclin.expqty,
                         rclin.rcvqty,
                         rci.po_num,
                         CAST(rclin.invlin AS int) invlin,
                         rct.trlr_id,
						 rct.moddte			 receipt_creation_date,
                         rct.clsdte          receipt_date,
                         rct.rcvtrk_stat     receipt_status,
                         rclin.prtnum        item_number,
                          CAST(rclin.lotnum AS NVARCHAR)
                         + CASE
                               WHEN rclin.lotnum IS NOT NULL AND rclin.expire_dte IS NOT NULL THEN
                                   '-' + format (rclin.expire_dte, 'MMddyyyy')
                               ELSE
                                   ''
                           END               lot_number,
                         rclin.expire_dte    lot_expire_dte,
                         rclin.rcvsts        destination_subinventory,
                         rclin.rcvkey        alternate_key
                    FROM [WMS].[arch_wmsprdmdc].dbo.rcvinv rci,
                         [WMS].[arch_wmsprdmdc].dbo.rcvtrk rct,
                         [WMS].[arch_wmsprdmdc].dbo.rcvlin rclin
                   WHERE     rct.trknum = rclin.trknum
                         AND rct.trknum = rci.trknum
                         AND rci.invtyp = 'PREC') mdc_po
        GROUP BY mdc_po.po_num,
                 mdc_po.invlin,
                 mdc_po.trlr_id,
				 mdc_po.receipt_creation_date,
                 mdc_po.receipt_date,
                 mdc_po.receipt_status,
                 mdc_po.item_number,
                 mdc_po.lot_number,
                 mdc_po.lot_expire_dte,
                 mdc_po.destination_subinventory) rcv
           ON rcv.po_num = po_mst.invnum AND rcv.invlin = po_mst.invlin
UNION ALL
SELECT po_mst.invnum                                                      purchase_order_number,
       rcv.trlr_id                                                        shipment_number,
       'USA_ARB'                                                          org_code,
       po_mst.invlin                                                      po_line_number,
       CASE
           WHEN isnull(po_mst.expqty, 0) > SUM (isnull(rcv.expqty, 0)) OVER (PARTITION BY rcv.po_num, rcv.invlin) THEN
               isnull (po_mst.expqty, 0)
           ELSE
               SUM (isnull (rcv.expqty, 0)) OVER (PARTITION BY rcv.po_num, rcv.invlin)
       END                                                                ordered_quantity,
       isnull (po_mst.expqty, 0)                                          shipped_quantity,
       isnull (rcv.rcvqty, 0)                                             received_quantity,
       rcv.destination_subinventory,
       isnull (rcv.receipt_creation_date, po_mst.moddte)                  receipt_creation_date,
       rcv.receipt_date,
       po_mst.prtnum													  item_number,
       rcv.lot_number,
       rcv.lot_expire_dte,
       isnull (rcv.receipt_status, 'EX')								  receipt_status,
       isnull (rcv.alternate_key, po_mst.invnum + '|' + CAST(po_mst.invlin AS nvarchar))    alternate_key
  FROM (SELECT rlin.invnum, CAST(rlin.invlin AS int) invlin, rlin.prtnum, rlin.expqty, format (rlin.moddte, 'yyyy-MM-dd HH:mm:ss') moddte
          FROM [WMS].[rpwmsarb].dbo.rimhdr rhdr,
               [WMS].[rpwmsarb].dbo.rimlin rlin
         WHERE rhdr.invnum = rlin.invnum AND rhdr.invtyp = 'PREC') po_mst
       LEFT OUTER JOIN
       (  SELECT SUM (mdc_po.expqty)            expqty,
                 SUM (mdc_po.rcvqty)            rcvqty,
                 mdc_po.po_num,
                 mdc_po.invlin,
                 mdc_po.trlr_id,
				 format (mdc_po.receipt_creation_date, 'yyyy-MM-dd HH:mm:ss') receipt_creation_date,
                 format (mdc_po.receipt_date, 'yyyy-MM-dd HH:mm:ss') receipt_date,
                 mdc_po.receipt_status,
                 mdc_po.item_number,
                 mdc_po.lot_number,
                 format (mdc_po.lot_expire_dte, 'yyyy-MM-dd HH:mm:ss') lot_expire_dte,
                 mdc_po.destination_subinventory,
                 MAX (mdc_po.alternate_key)     alternate_key
            FROM (SELECT rclin.expqty,
                         rclin.rcvqty,
                         MAX(rci.po_num) po_num,
                         CAST(rclin.invlin AS int) invlin,
                         rct.trlr_id,
						 rct.moddte			 receipt_creation_date,
                         rct.clsdte          receipt_date,
                         rct.rcvtrk_stat     receipt_status,
                         rclin.prtnum        item_number,
                          CAST(rclin.lotnum AS NVARCHAR)
                         + CASE
                               WHEN rclin.lotnum IS NOT NULL AND rclin.expire_dte IS NOT NULL THEN
                                   '-' + format (rclin.expire_dte, 'MMddyyyy')
                               ELSE
                                   ''
                           END               lot_number,
                         rclin.expire_dte    lot_expire_dte,
                         rclin.rcvsts        destination_subinventory,
                         rclin.rcvkey        alternate_key
                    FROM [WMS].[rpwmsarb].dbo.rcvinv rci,
                         [WMS].[rpwmsarb].dbo.rcvtrk rct,
                         [WMS].[rpwmsarb].dbo.rcvlin rclin
                   WHERE     rct.trknum = rclin.trknum
                         AND rct.trknum = rci.trknum
                         AND rci.invtyp = 'PREC'
						 GROUP BY rclin.expqty,
                         rclin.rcvqty,
						 CAST(rclin.invlin AS int),
                         rct.trlr_id,
						 rct.moddte,
                         rct.clsdte,
                         rct.rcvtrk_stat,
                         rclin.prtnum,
                          CAST(rclin.lotnum AS NVARCHAR)
                         + CASE
                               WHEN rclin.lotnum IS NOT NULL AND rclin.expire_dte IS NOT NULL THEN
                                   '-' + format (rclin.expire_dte, 'MMddyyyy')
                               ELSE
                                   ''
                           END,
                         rclin.expire_dte,
                         rclin.rcvsts,
                         rclin.rcvkey) mdc_po
        GROUP BY mdc_po.po_num,
                 mdc_po.invlin,
                 mdc_po.trlr_id,
				 mdc_po.receipt_creation_date,
                 mdc_po.receipt_date,
                 mdc_po.receipt_status,
                 mdc_po.item_number,
                 mdc_po.lot_number,
                 mdc_po.lot_expire_dte,
                 mdc_po.destination_subinventory) rcv
           ON rcv.po_num = po_mst.invnum AND rcv.invlin = po_mst.invlin) wms_po_receipts
 WHERE ISNULL(wms_po_receipts.receipt_date, wms_po_receipts.receipt_creation_date) >= '1900-01-01 00:00:00.000'/*Replace '1900-01-01 00:00:00.000' with ISNULL/NVL(Last Run Date, 1st Jan, 1900)*/
   AND NOT (ISNULL(wms_po_receipts.receipt_status, 'EX') = 'C' AND ISNULL(wms_po_receipts.received_quantity, 0) = 0)
   AND wms_po_receipts.purchase_order_number = porw.PO_NUMBER
   AND (CAST(wms_po_receipts.alternate_key AS nvarchar) = CAST(porw.ALTERNATE_KEY AS nvarchar)
   OR CAST(wms_po_receipts.purchase_order_number AS nvarchar) + '|' + CAST(wms_po_receipts.po_line_number  AS nvarchar)= CAST(porw.ALTERNATE_KEY AS nvarchar)))
   AND porw.RECEIVED_QUANTITY > 0
   AND porw.RECEIPT_STATUS = 'Entered'
   AND porw.WAREHOUSE_NAME = 'WMS')

