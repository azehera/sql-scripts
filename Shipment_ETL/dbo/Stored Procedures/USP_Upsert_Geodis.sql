﻿
CREATE PROCEDURE [dbo].[USP_Upsert_Geodis]
as

--------------------Delete Duplicates from Staging table--------------------------
with cte as 
(select row_number() over (partition by line_, reference order by Filename desc) as N, * 
from [SHIPMENT_ETL].[dbo].[geodis_shipping_summary_STG])
delete from cte where N>1;
 
-------------------Upsert operation and deleting the old records--------------------------
MERGE [SHIPMENT_ETL].[dbo].[geodis_shipping_summary] AS target
USING [SHIPMENT_ETL].[dbo].[geodis_shipping_summary_STG] AS SOURCE
ON (target.Line_ =SOURCE.Line_ AND target.Reference = SOURCE.Reference)
WHEN MATCHED 
THEN UPDATE SET target.[Load] = SOURCE.[Load] , 
				target.[Pro] = SOURCE.[Pro] ,
				target.[Ord_Ship_Id] = SOURCE.[Ord_Ship_Id] ,
				target.[ShipDate] =SOURCE.[ShipDate],
				target.[Trailer] = SOURCE.[Trailer],
				target.[Carrier] = SOURCE.[Carrier] ,
				target.[Seals]=SOURCE.[Seals],
				target.[Item_Ord]=SOURCE.[Item_Ord],
				target.[Item_Ship]=SOURCE.[Item_Ship],
				target.[Qty_Ord]=SOURCE.[Qty_Ord],
				target.[Qty_Ship]=SOURCE.[Qty_Ship],
				target.[Dif]=SOURCE.[Dif],
				target.[Cube]=SOURCE.[Cube],
				target.[Brand] = SOURCE.[Brand],
				target.[Class] = SOURCE.[Class],
				target.[Order_Status] = SOURCE.[Order_Status],
				target.[Modified_By] = 'ETL_User',
				target.[Modified_Date] = getdate(),
				target.[Filename] = SOURCE.[Filename],
				target.[Drop_Date]=SOURCE.[Drop_Date]
WHEN NOT MATCHED BY TARGET
THEN INSERT ([Line_], [Reference], [Load], [Pro] , [Ord_Ship_Id], [ShipDate], [Trailer], [Carrier], [Seals], [Item_Ord], 
[Item_Ship], [Qty_Ord], [Qty_Ship], [Dif], [Cube], [Brand], [Class] , [Order_Status],[Created_By], [Created_Date], [Filename], [Drop_Date]) 
              VALUES (SOURCE.[Line_], SOURCE.[Reference], SOURCE.[Load], SOURCE.[Pro], SOURCE.[Ord_Ship_Id], SOURCE.[ShipDate], SOURCE.[Trailer], 
SOURCE.[Carrier], SOURCE.[Seals], SOURCE.[Item_Ord], SOURCE.[Item_Ship], SOURCE.[Qty_Ord], SOURCE.[Qty_Ship], 
SOURCE.[Dif], SOURCE.[Cube], SOURCE.[Brand],SOURCE.[Class], Source.[Order_Status], SOURCE.[Created_By], SOURCE.[Created_Date], SOURCE.[Filename], SOURCE.[Drop_Date]);
