﻿CREATE TABLE [dbo].[fact_order_tracking] (
    [id]                      BIGINT          IDENTITY (1, 1) NOT NULL,
    [tracking_number]         VARCHAR (64)    NULL,
    [medifastordernumber]     VARCHAR (64)    NULL,
    [status]                  VARCHAR (64)    NULL,
    [manifestweight]          DECIMAL (19, 4) NULL,
    [firstscandate]           DATETIME        NULL,
    [destscandate]            DATETIME        NULL,
    [finaldelvdate]           DATETIME        NULL,
    [shipmentprovider]        VARCHAR (64)    NULL,
    [ref1]                    VARCHAR (64)    NULL,
    [updatebatchid]           INT             NULL,
    [edh_modified_date]       DATETIME        NULL,
    [scheduleddeliverydate]   DATE            NULL,
    [scheduleddeliverytime]   VARCHAR (100)   NULL,
    [rescheduleddeliverydate] DATE            NULL,
    [rescheduleddeliverytime] VARCHAR (100)   NULL,
    [recordtype]              VARCHAR (500)   NULL,
    [packageactivitydate]     DATE            NULL,
    [packageactivitytime]     VARCHAR (100)   NULL,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [idx_trk_nbr]
    ON [dbo].[fact_order_tracking]([tracking_number] ASC) WITH (FILLFACTOR = 100);

