﻿CREATE TABLE [dbo].[fact_erp_dly_dvrsn_rpt] (
    [type]                 NVARCHAR (50)  NOT NULL,
    [ordernumber]          NVARCHAR (50)  NOT NULL,
    [linenumber]           NVARCHAR (50)  NOT NULL,
    [planneddc]            NVARCHAR (50)  NULL,
    [actualdc]             NVARCHAR (50)  NULL,
    [shiptostate]          NVARCHAR (50)  NULL,
    [itemnumber]           NVARCHAR (50)  NOT NULL,
    [itemdescription]      NVARCHAR (256) NULL,
    [itemcausingdiversion] NVARCHAR (50)  NULL,
    [orderdate]            DATETIME       NULL,
    [ordertime]            NVARCHAR (50)  NULL,
    [ordercreationdate]    DATETIME       NULL,
    [orderdropdate]        DATETIME       NULL,
    [delayindays]          INT            NULL,
    [orderstatus]          NVARCHAR (50)  NULL,
    [actualshipdate]       DATETIME       NULL,
    [scheduleshipdate]     DATETIME       NULL,
    [oracleshippingmethod] NVARCHAR (50)  NULL,
    [diversionreason]      NVARCHAR (50)  NULL,
    [reservableinv]        NVARCHAR (50)  NULL,
    [create_date]          DATETIME       NULL,
    [update_date]          DATETIME       NULL,
    [s3_file_name]         NVARCHAR (250) NULL,
    [updatebatchid]        BIGINT         NOT NULL,
    CONSTRAINT [PK_fact_erp_dly_dvrsn_rpt] PRIMARY KEY CLUSTERED ([ordernumber] ASC, [linenumber] ASC, [type] ASC, [itemnumber] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [ix_Diverion]
    ON [dbo].[fact_erp_dly_dvrsn_rpt]([type] ASC, [itemcausingdiversion] ASC)
    INCLUDE([ordernumber], [itemnumber], [itemdescription]);

