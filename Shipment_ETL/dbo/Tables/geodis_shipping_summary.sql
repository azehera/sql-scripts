﻿CREATE TABLE [dbo].[geodis_shipping_summary] (
    [PK]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [Line_]         VARCHAR (50)   NULL,
    [Reference]     VARCHAR (50)   NULL,
    [Load]          VARCHAR (50)   NULL,
    [Pro]           VARCHAR (50)   NULL,
    [Ord_Ship_Id]   VARCHAR (50)   NULL,
    [ShipDate]      VARCHAR (50)   NULL,
    [Carrier]       VARCHAR (50)   NULL,
    [Trailer]       VARCHAR (50)   NULL,
    [Seals]         VARCHAR (50)   NULL,
    [Item_Ord]      VARCHAR (50)   NULL,
    [Item_Ship]     VARCHAR (50)   NULL,
    [Qty_Ord]       VARCHAR (50)   NULL,
    [Qty_Ship]      VARCHAR (50)   NULL,
    [Dif]           VARCHAR (50)   NULL,
    [Cube]          VARCHAR (50)   NULL,
    [Brand]         VARCHAR (50)   NULL,
    [Class]         VARCHAR (50)   NULL,
    [Created_By]    NVARCHAR (255) NULL,
    [Created_Date]  DATETIME       NULL,
    [Modified_By]   NVARCHAR (255) NULL,
    [Modified_Date] DATETIME       NULL,
    [Filename]      NVARCHAR (255) NULL,
    [Order_Status]  NVARCHAR (50)  NULL,
    [Drop_Date]     VARCHAR (50)   NULL,
    CONSTRAINT [PK_geodis_shipping_summary] PRIMARY KEY CLUSTERED ([PK] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
);


GO
CREATE NONCLUSTERED INDEX [IX_geodis_shipping_summary_Order_Status]
    ON [dbo].[geodis_shipping_summary]([Order_Status] ASC)
    INCLUDE([Reference], [ShipDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_geodis_shipping_summary_Order_Status2]
    ON [dbo].[geodis_shipping_summary]([Order_Status] ASC)
    INCLUDE([Reference], [ShipDate], [Drop_Date]);

