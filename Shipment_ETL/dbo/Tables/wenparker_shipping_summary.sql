﻿CREATE TABLE [dbo].[wenparker_shipping_summary] (
    [Pk]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [OrderNo]       VARCHAR (50)   NULL,
    [OrderType]     VARCHAR (50)   NULL,
    [Hold]          VARCHAR (50)   NULL,
    [Cancel]        VARCHAR (50)   NULL,
    [ShipToRegion]  VARCHAR (50)   NULL,
    [LastCarrier]   VARCHAR (50)   NULL,
    [DropDate]      VARCHAR (50)   NULL,
    [PackDate]      VARCHAR (50)   NULL,
    [ShipDate]      VARCHAR (50)   NULL,
    [CancelDate]    VARCHAR (50)   NULL,
    [Reason]        VARCHAR (50)   NULL,
    [Created_By]    NVARCHAR (255) NULL,
    [Created_Date]  DATETIME       NULL,
    [Modified_By]   NVARCHAR (255) NULL,
    [Modified_Date] DATETIME       NULL,
    [Filename]      NVARCHAR (255) NULL,
    CONSTRAINT [PK_wenparker_shipping_summary] PRIMARY KEY CLUSTERED ([Pk] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_wenparker_shipping_summary_OrderNo]
    ON [dbo].[wenparker_shipping_summary]([OrderNo] ASC)
    INCLUDE([DropDate], [ShipDate]) WITH (FILLFACTOR = 90);

