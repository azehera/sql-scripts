﻿CREATE TABLE [dbo].[DataDictionaryEntryPaths] (
    [RowId]                       INT              IDENTITY (1, 1) NOT NULL,
    [DataDictionaryEntryUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [Path]                        NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_DataDictionaryEntryPaths] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_DataDictionaryEntryPaths_DataDictionaryEntries] FOREIGN KEY ([DataDictionaryEntryUniqueId]) REFERENCES [dbo].[DataDictionaryEntries] ([UniqueId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE TRIGGER TR_DataDictionaryEntryPaths_U ON [dbo].[DataDictionaryEntryPaths]
FOR UPDATE
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditEntryPaths] ([DataDictionaryEntryUniqueId], [Path], [DataDictionaryAuditUserId], [ActionType])
SELECT [DataDictionaryEntryUniqueId], [Path], @UserId, 'U' FROM [deleted]

GO
CREATE TRIGGER TR_DataDictionaryEntryPaths_D ON [dbo].[DataDictionaryEntryPaths]
FOR DELETE
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditEntryPaths] ([DataDictionaryEntryUniqueId], [Path], [DataDictionaryAuditUserId], [ActionType])
SELECT [DataDictionaryEntryUniqueId], [Path], @UserId, 'D' FROM [deleted]

GO
CREATE TRIGGER TR_DataDictionaryEntryPaths_I ON [dbo].[DataDictionaryEntryPaths]
FOR INSERT
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditEntryPaths] ([DataDictionaryEntryUniqueId], [Path], [DataDictionaryAuditUserId], [ActionType])
SELECT [DataDictionaryEntryUniqueId], [Path], @UserId, 'I' FROM [inserted]
