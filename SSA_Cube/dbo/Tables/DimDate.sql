﻿CREATE TABLE [dbo].[DimDate] (
    [DWDateKey]      DATE          NULL,
    [DayDate]        INT           NULL,
    [DayOfWeekName]  NVARCHAR (30) NULL,
    [WeekNumber]     INT           NULL,
    [MonthNumber]    INT           NULL,
    [MonthName]      NVARCHAR (30) NULL,
    [MonthShortName] NVARCHAR (4)  NULL,
    [Year]           INT           NULL,
    [QuarterNumber]  NVARCHAR (30) NULL,
    [QuarterName]    NVARCHAR (30) NULL
);

