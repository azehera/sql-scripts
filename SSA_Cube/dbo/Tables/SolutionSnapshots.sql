﻿CREATE TABLE [dbo].[SolutionSnapshots] (
    [RowId]                INT              IDENTITY (1, 1) NOT NULL,
    [SolutionId]           UNIQUEIDENTIFIER NOT NULL,
    [SnapshotStartDate]    DATETIME         NOT NULL,
    [SnapshotEndDate]      DATETIME         NOT NULL,
    [IsImport]             BIT              NOT NULL,
    [IsDependencyScanOnly] BIT              NOT NULL,
    CONSTRAINT [PK_SolutionSnapshots] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_SolutionSnapshots_Solution] FOREIGN KEY ([SolutionId]) REFERENCES [dbo].[Solution] ([SolutionId]) ON DELETE CASCADE ON UPDATE CASCADE
);

