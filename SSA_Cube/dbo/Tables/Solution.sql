﻿CREATE TABLE [dbo].[Solution] (
    [RowId]           INT              IDENTITY (1, 1) NOT NULL,
    [SolutionId]      UNIQUEIDENTIFIER NOT NULL,
    [AccountId]       UNIQUEIDENTIFIER NOT NULL,
    [Name]            NVARCHAR (255)   NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_Solution_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    [DocumentMap]     XML              NOT NULL,
    [DateLastUpdated] DATETIME         CONSTRAINT [DF_Solution_DateLastUpdated] DEFAULT (getutcdate()) NOT NULL,
    [EndpointAliases] XML              NULL,
    CONSTRAINT [PK_Solution] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Solution_Account] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Account] ([AccountId]),
    CONSTRAINT [UQ_Solution_SolutionId] UNIQUE NONCLUSTERED ([SolutionId] ASC) WITH (FILLFACTOR = 80)
);

