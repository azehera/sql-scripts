﻿CREATE TABLE [dbo].[DimItemCode] (
    [Itemcode]           NVARCHAR (255) NOT NULL,
    [Description]        NVARCHAR (255) NULL,
    [Product Group Code] NVARCHAR (100) NULL,
    [Item Category Code] NVARCHAR (100) NULL,
    [Discontinued]       NVARCHAR (5)   NULL,
    [MKTG Type]          NVARCHAR (20)  NULL,
    [MKTG Category]      NVARCHAR (20)  NULL,
    [MKTG Subcategory]   NVARCHAR (20)  NULL,
    [Launch Date]        DATETIME       NULL,
    [MKTG Status]        NVARCHAR (50)  NULL,
    [MKTG Brand]         NVARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_DimItemCode_idx1]
    ON [dbo].[DimItemCode]([Itemcode] ASC)
    INCLUDE([Description], [Product Group Code], [Item Category Code]) WITH (FILLFACTOR = 100);

