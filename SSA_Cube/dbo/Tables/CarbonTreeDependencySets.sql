﻿CREATE TABLE [dbo].[CarbonTreeDependencySets] (
    [RowId]         INT              IDENTITY (1, 1) NOT NULL,
    [CarbonTreeId]  UNIQUEIDENTIFIER NOT NULL,
    [Content]       VARBINARY (MAX)  NOT NULL,
    [EffectiveDate] DATETIME         NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_CarbonTreeDependencySets_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_CarbonTreeDependencySets] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CarbonTreeDependencySets_CarbonTree] FOREIGN KEY ([CarbonTreeId]) REFERENCES [dbo].[CarbonTree] ([CarbonTreeId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_CarbonTreeDependencySets_CarbonTreeId]
    ON [dbo].[CarbonTreeDependencySets]([CarbonTreeId] ASC)
    INCLUDE([EffectiveDate]) WITH (FILLFACTOR = 80);

