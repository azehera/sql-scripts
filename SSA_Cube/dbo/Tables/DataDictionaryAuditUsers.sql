﻿CREATE TABLE [dbo].[DataDictionaryAuditUsers] (
    [RowId]    INT            IDENTITY (1, 1) NOT NULL,
    [UserName] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_DataDictionaryAuditUsers] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UQ_DataDictionaryAuditUsers] UNIQUE NONCLUSTERED ([UserName] ASC) WITH (FILLFACTOR = 80)
);

