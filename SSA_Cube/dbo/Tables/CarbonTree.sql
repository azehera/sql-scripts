﻿CREATE TABLE [dbo].[CarbonTree] (
    [RowId]          INT              IDENTITY (1, 1) NOT NULL,
    [CarbonTreeId]   UNIQUEIDENTIFIER NOT NULL,
    [Content]        VARBINARY (MAX)  NOT NULL,
    [Checksum]       BIGINT           NOT NULL,
    [Version]        INT              NOT NULL,
    [EffectiveDate]  DATETIME         NOT NULL,
    [SolutionItemId] UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_CarbonTree_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_CarbonTree] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CarbonTree_SolutionItem] FOREIGN KEY ([SolutionItemId]) REFERENCES [dbo].[SolutionItem] ([SolutionItemId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [UQ_CarbonTree_CarbonTreeId] UNIQUE NONCLUSTERED ([CarbonTreeId] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_CarbonTree_CarbonTreeId]
    ON [dbo].[CarbonTree]([SolutionItemId] ASC)
    INCLUDE([EffectiveDate]) WITH (FILLFACTOR = 80);

