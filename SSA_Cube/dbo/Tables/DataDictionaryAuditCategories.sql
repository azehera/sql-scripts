﻿CREATE TABLE [dbo].[DataDictionaryAuditCategories] (
    [RowId]                        INT              IDENTITY (1, 1) NOT NULL,
    [UniqueId]                     UNIQUEIDENTIFIER NOT NULL,
    [Name]                         NVARCHAR (255)   NOT NULL,
    [DataType]                     INT              NOT NULL,
    [ScopeType]                    INT              NOT NULL,
    [ScopeTypeItemId]              UNIQUEIDENTIFIER NULL,
    [ProviderCompatibilityGroupId] UNIQUEIDENTIFIER NULL,
    [IsRequired]                   BIT              NOT NULL,
    [DataTypeData]                 NVARCHAR (MAX)   NULL,
    [DataDictionaryAuditUserId]    INT              NOT NULL,
    [DateChanged]                  DATETIME         CONSTRAINT [DF_DataDictionaryAuditCategories_DateChanged] DEFAULT (getutcdate()) NOT NULL,
    [ActionType]                   CHAR (1)         NOT NULL,
    CONSTRAINT [PK_DataDictionaryAuditCategories] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_DataDictionaryAuditCategories_DataDictionaryAuditUsers] FOREIGN KEY ([DataDictionaryAuditUserId]) REFERENCES [dbo].[DataDictionaryAuditUsers] ([RowId])
);

