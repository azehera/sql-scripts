﻿CREATE TABLE [dbo].[DataDictionaryAuditEntryPaths] (
    [RowId]                       INT              IDENTITY (1, 1) NOT NULL,
    [DataDictionaryEntryUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [Path]                        NVARCHAR (MAX)   NOT NULL,
    [DataDictionaryAuditUserId]   INT              NOT NULL,
    [DateChanged]                 DATETIME         CONSTRAINT [DF_DataDictionaryAuditEntryPaths_DateChanged] DEFAULT (getutcdate()) NOT NULL,
    [ActionType]                  CHAR (1)         NOT NULL,
    CONSTRAINT [PK_DataDictionaryAuditEntryPaths] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_DataDictionaryAuditEntryPaths_DataDictionaryAuditUsers] FOREIGN KEY ([DataDictionaryAuditUserId]) REFERENCES [dbo].[DataDictionaryAuditUsers] ([RowId])
);

