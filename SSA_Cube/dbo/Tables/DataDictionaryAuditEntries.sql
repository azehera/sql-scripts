﻿CREATE TABLE [dbo].[DataDictionaryAuditEntries] (
    [RowId]                          INT              IDENTITY (1, 1) NOT NULL,
    [UniqueId]                       UNIQUEIDENTIFIER NOT NULL,
    [DataDictionaryCategoryUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [ScopeType]                      INT              NOT NULL,
    [ScopeTypeItemId]                UNIQUEIDENTIFIER NULL,
    [ProviderCompatibilityGroupId]   UNIQUEIDENTIFIER NULL,
    [Value]                          NVARCHAR (MAX)   NULL,
    [IsGlobal]                       BIT              NOT NULL,
    [DataDictionaryAuditUserId]      INT              NOT NULL,
    [DateChanged]                    DATETIME         CONSTRAINT [DF_DataDictionaryAuditEntries_DateChanged] DEFAULT (getutcdate()) NOT NULL,
    [ActionType]                     CHAR (1)         NOT NULL,
    CONSTRAINT [PK_DataDictionaryAuditEntries] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_DataDictionaryAuditEntries_DataDictionaryAuditUsers] FOREIGN KEY ([DataDictionaryAuditUserId]) REFERENCES [dbo].[DataDictionaryAuditUsers] ([RowId])
);

