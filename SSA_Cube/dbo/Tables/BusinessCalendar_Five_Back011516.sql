﻿CREATE TABLE [dbo].[BusinessCalendar_Five_Back011516] (
    [DWDateKey]                  DATE          NULL,
    [DayDate]                    INT           NULL,
    [DayOfWeekName]              NVARCHAR (30) NULL,
    [WeekNumber]                 INT           NULL,
    [MonthNumber]                INT           NULL,
    [MonthName]                  NVARCHAR (30) NULL,
    [MonthShortName]             NVARCHAR (4)  NULL,
    [Year]                       INT           NULL,
    [QuarterNumber]              NVARCHAR (30) NULL,
    [QuarterName]                NVARCHAR (30) NULL,
    [RealDate]                   VARCHAR (50)  NULL,
    [Posting Date]               DATETIME      NULL,
    [CalendarYearMonth]          VARCHAR (50)  NULL,
    [CalendarYearQuarter]        VARCHAR (50)  NULL,
    [businessDay]                BIT           NULL,
    [BusinessDayOfFiscalYear]    INT           NULL,
    [BusinessDayOfFiscalPeriod]  INT           NULL,
    [BusinessDayOfFiscalQuarter] INT           NULL
);

