﻿CREATE TABLE [dbo].[DimBusinessUnits] (
    [BusinessUnits]    VARCHAR (50) NULL,
    [BusinessCategory] VARCHAR (50) NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [BusinessUnits]
    ON [dbo].[DimBusinessUnits]([BusinessUnits] ASC) WITH (FILLFACTOR = 80);

