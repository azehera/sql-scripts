﻿CREATE TABLE [dbo].[DimSubCategoryItemCode] (
    [Subcategory Itemcode]           NVARCHAR (255) NOT NULL,
    [Subcategory Description]        NVARCHAR (255) NULL,
    [Subcategory Product Group Code] NVARCHAR (100) NULL,
    [Subcategory Item Category Code] NVARCHAR (100) NULL,
    [Discontinued]                   NVARCHAR (5)   NULL,
    [SubCategory MKTG Type]          NVARCHAR (20)  NULL,
    [SubCategory MKTG Category]      NVARCHAR (20)  NULL,
    [SubCategory MKTG Subcategory]   NVARCHAR (20)  NULL,
    [SubCategory Launch Date]        DATETIME       NULL,
    [SubCategory MKTG Status]        NVARCHAR (50)  NULL,
    [SubCategory MKTG Brand]         NVARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_1]
    ON [dbo].[DimSubCategoryItemCode]([Subcategory Itemcode] ASC)
    INCLUDE([Subcategory Description], [Subcategory Product Group Code], [Subcategory Item Category Code], [Discontinued], [SubCategory MKTG Type], [SubCategory MKTG Category], [SubCategory MKTG Subcategory], [SubCategory Launch Date], [SubCategory MKTG Status], [SubCategory MKTG Brand]) WITH (FILLFACTOR = 90);

