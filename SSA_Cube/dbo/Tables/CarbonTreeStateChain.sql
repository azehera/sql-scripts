﻿CREATE TABLE [dbo].[CarbonTreeStateChain] (
    [CarbonTreeStateChainId] INT              IDENTITY (1, 1) NOT NULL,
    [ReversalPatchContent]   VARBINARY (MAX)  NOT NULL,
    [Version]                INT              NOT NULL,
    [EffectiveDate]          DATETIME         NOT NULL,
    [CarbonTreeId]           UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_CarbonTreeStateChain_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_CarbonTreeStateChain] PRIMARY KEY CLUSTERED ([CarbonTreeStateChainId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CarbonTreeStateChain_CarbonTree] FOREIGN KEY ([CarbonTreeId]) REFERENCES [dbo].[CarbonTree] ([CarbonTreeId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_CarbonTreeStateChain_CarbonTreeId]
    ON [dbo].[CarbonTreeStateChain]([CarbonTreeId] ASC)
    INCLUDE([EffectiveDate]) WITH (FILLFACTOR = 80);

