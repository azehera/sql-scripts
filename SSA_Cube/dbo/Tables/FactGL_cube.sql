﻿CREATE TABLE [dbo].[FactGL_cube] (
    [EntryNO]                 INT              NOT NULL,
    [CalendarMonthID]         INT              NULL,
    [G_L Account No_]         VARCHAR (20)     NOT NULL,
    [DocumentID]              VARCHAR (100)    NULL,
    [Document No_]            VARCHAR (20)     NULL,
    [Global Dimension 1 Code] VARCHAR (20)     NULL,
    [Source Code]             VARCHAR (10)     NULL,
    [Global Dimension 2 Code] VARCHAR (50)     NULL,
    [Transaction No_]         INT              NULL,
    [NetTotal]                DECIMAL (38, 18) NULL,
    [Budget$]                 DECIMAL (38, 18) NULL,
    [AccountsCategory]        INT              NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_FactGL_cube_idx1]
    ON [dbo].[FactGL_cube]([CalendarMonthID] DESC, [Source Code] ASC, [Global Dimension 2 Code] ASC, [NetTotal] ASC)
    INCLUDE([G_L Account No_]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FactGL_cube_idx2]
    ON [dbo].[FactGL_cube]([CalendarMonthID] ASC, [G_L Account No_] ASC, [Global Dimension 1 Code] ASC, [Source Code] ASC)
    INCLUDE([EntryNO], [Global Dimension 2 Code], [NetTotal]) WITH (FILLFACTOR = 85);

