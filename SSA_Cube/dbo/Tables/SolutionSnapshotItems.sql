﻿CREATE TABLE [dbo].[SolutionSnapshotItems] (
    [RowId]              INT              IDENTITY (1, 1) NOT NULL,
    [SolutionSnapshotId] INT              NOT NULL,
    [SolutionItemId]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_SolutionSnapshotItems] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_SolutionSnapshotItems_SolutionItem] FOREIGN KEY ([SolutionItemId]) REFERENCES [dbo].[SolutionItem] ([SolutionItemId]),
    CONSTRAINT [FK_SolutionSnapshotItems_SolutionSnapshots] FOREIGN KEY ([SolutionSnapshotId]) REFERENCES [dbo].[SolutionSnapshots] ([RowId]) ON DELETE CASCADE ON UPDATE CASCADE
);

