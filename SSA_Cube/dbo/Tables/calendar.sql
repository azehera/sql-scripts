﻿CREATE TABLE [dbo].[calendar] (
    [CalendarDate]       DATETIME     NOT NULL,
    [CalendarYear]       INT          NOT NULL,
    [CalendarMonth]      INT          NOT NULL,
    [CalendarQuarter]    INT          NOT NULL,
    [CalendarMonthID]    INT          NOT NULL,
    [CalendarDay]        INT          NOT NULL,
    [CalendarWeek]       INT          NOT NULL,
    [DayOfYear]          INT          NOT NULL,
    [DayOfWeekName]      VARCHAR (10) NOT NULL,
    [FirstDateOfWeek]    DATETIME     NOT NULL,
    [LastDateOfWeek]     DATETIME     NOT NULL,
    [FirstDateOfMonth]   DATETIME     NOT NULL,
    [LastDateOfMonth]    DATETIME     NOT NULL,
    [FirstDateOfQuarter] DATETIME     NOT NULL,
    [LastDateOfQuarter]  DATETIME     NOT NULL,
    [FirstDateOfYear]    DATETIME     NOT NULL,
    [LastDateOfYear]     DATETIME     NOT NULL,
    [Weekend]            BIT          NOT NULL,
    [Weekday]            BIT          NOT NULL,
    [CalendarMonthName]  CHAR (15)    NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_calendar_CalendarDate]
    ON [dbo].[calendar]([CalendarDate] ASC)
    INCLUDE([CalendarMonthID]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_calendar_CalendarDate_idx1]
    ON [dbo].[calendar]([CalendarDate] ASC)
    INCLUDE([CalendarYear], [CalendarQuarter], [CalendarMonthID]) WITH (FILLFACTOR = 90);

