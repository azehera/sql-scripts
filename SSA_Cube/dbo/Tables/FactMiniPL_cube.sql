﻿CREATE TABLE [dbo].[FactMiniPL_cube] (
    [EntryNO]                 INT              NOT NULL,
    [CalendarMonthID]         INT              NULL,
    [G_L Account No_]         VARCHAR (20)     NOT NULL,
    [DocumentID]              VARCHAR (100)    NULL,
    [Document No_]            VARCHAR (20)     NULL,
    [Global Dimension 1 Code] VARCHAR (20)     NULL,
    [Global Dimension 2 Code] VARCHAR (50)     NULL,
    [Transaction No_]         INT              NULL,
    [NetTotal]                DECIMAL (38, 18) NULL,
    [Budget$]                 DECIMAL (38, 18) NULL
);

