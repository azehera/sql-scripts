﻿CREATE TABLE [dbo].[MDMUpdate] (
    [UpdateNo]       INT           NOT NULL,
    [GroupNo]        INT           NOT NULL,
    [DocNo]          NVARCHAR (20) NOT NULL,
    [UpdateDateTime] DATETIME      NOT NULL,
    CONSTRAINT [PK_MDMUpdate] PRIMARY KEY CLUSTERED ([UpdateNo] ASC) WITH (FILLFACTOR = 95)
);

