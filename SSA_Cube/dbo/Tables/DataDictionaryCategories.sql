﻿CREATE TABLE [dbo].[DataDictionaryCategories] (
    [RowId]                        INT              IDENTITY (1, 1) NOT NULL,
    [UniqueId]                     UNIQUEIDENTIFIER NOT NULL,
    [Name]                         NVARCHAR (255)   NOT NULL,
    [DataType]                     INT              NOT NULL,
    [ScopeType]                    INT              NOT NULL,
    [ScopeTypeItemId]              UNIQUEIDENTIFIER NULL,
    [ProviderCompatibilityGroupId] UNIQUEIDENTIFIER NULL,
    [IsRequired]                   BIT              NOT NULL,
    [DateCreated]                  DATETIME         CONSTRAINT [DF_DataDictionaryCategories_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    [DataTypeData]                 NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_DataDictionaryCategories] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_DataDictionaryCategories] CHECK ([ScopeType]=(0) OR [ScopeTypeItemId] IS NOT NULL),
    CONSTRAINT [UQ_DataDictionaryCategories_UniqueId] UNIQUE NONCLUSTERED ([UniqueId] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE TRIGGER TR_DataDictionaryCategories_U ON [dbo].[DataDictionaryCategories]
FOR UPDATE
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditCategories] ([UniqueId], [Name], [DataType], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DataTypeData], [DataDictionaryAuditUserId], [ActionType])
SELECT [UniqueId], [Name], [DataType], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DataTypeData], @UserId, 'U' FROM [deleted]

GO
CREATE TRIGGER TR_DataDictionaryCategories_D ON [dbo].[DataDictionaryCategories]
FOR DELETE
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditCategories] ([UniqueId], [Name], [DataType], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DataTypeData], [DataDictionaryAuditUserId], [ActionType])
SELECT [UniqueId], [Name], [DataType], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DataTypeData], @UserId, 'D' FROM [deleted]

GO
CREATE TRIGGER TR_DataDictionaryCategories_I ON [dbo].[DataDictionaryCategories]
FOR INSERT
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditCategories] ([UniqueId], [Name], [DataType], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DataTypeData], [DataDictionaryAuditUserId], [ActionType])
SELECT [UniqueId], [Name], [DataType], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DataTypeData], @UserId, 'I' FROM [inserted]
