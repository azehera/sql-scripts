﻿CREATE TABLE [dbo].[BusinessCalendar_Five] (
    [DWDateKey]                  DATE          NULL,
    [DayDate]                    INT           NULL,
    [DayOfWeekName]              NVARCHAR (30) NULL,
    [WeekNumber]                 INT           NULL,
    [MonthNumber]                INT           NULL,
    [MonthName]                  NVARCHAR (30) NULL,
    [MonthShortName]             NVARCHAR (4)  NULL,
    [Year]                       INT           NULL,
    [QuarterNumber]              NVARCHAR (30) NULL,
    [QuarterName]                NVARCHAR (30) NULL,
    [RealDate]                   VARCHAR (50)  NULL,
    [Posting Date]               DATETIME      NULL,
    [CalendarYearMonth]          VARCHAR (50)  NULL,
    [CalendarYearQuarter]        VARCHAR (50)  NULL,
    [businessDay]                BIT           NULL,
    [BusinessDayOfFiscalYear]    INT           NULL,
    [BusinessDayOfFiscalPeriod]  INT           NULL,
    [BusinessDayOfFiscalQuarter] INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_BusinessCalendar_Five_Posting Date]
    ON [dbo].[BusinessCalendar_Five]([Posting Date] ASC)
    INCLUDE([BusinessDayOfFiscalPeriod], [BusinessDayOfFiscalQuarter]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_BusinessCalendar_Five_MonthNumber_Year_businessDay]
    ON [dbo].[BusinessCalendar_Five]([MonthNumber] ASC, [Year] ASC, [businessDay] ASC)
    INCLUDE([Posting Date]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_BusinessCalendar_Five_Year_businessDay]
    ON [dbo].[BusinessCalendar_Five]([Year] ASC, [businessDay] ASC)
    INCLUDE([QuarterNumber], [Posting Date]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_BusinessCalendar_Five_CalendarYearMonth]
    ON [dbo].[BusinessCalendar_Five]([CalendarYearMonth] ASC)
    INCLUDE([RealDate], [BusinessDayOfFiscalPeriod]);


GO
CREATE NONCLUSTERED INDEX [IX_BusinessCalendar_Five_CalendarYearQuarter]
    ON [dbo].[BusinessCalendar_Five]([CalendarYearQuarter] ASC)
    INCLUDE([RealDate], [BusinessDayOfFiscalQuarter]);

