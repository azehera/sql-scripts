﻿CREATE TABLE [dbo].[DataDictionaryEntries] (
    [RowId]                          INT              IDENTITY (1, 1) NOT NULL,
    [UniqueId]                       UNIQUEIDENTIFIER NOT NULL,
    [DataDictionaryCategoryUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [ScopeType]                      INT              NOT NULL,
    [ScopeTypeItemId]                UNIQUEIDENTIFIER NULL,
    [ProviderCompatibilityGroupId]   UNIQUEIDENTIFIER NULL,
    [Value]                          NVARCHAR (MAX)   NULL,
    [IsGlobal]                       BIT              NOT NULL,
    [DateCreated]                    DATETIME         CONSTRAINT [DF_DataDictionaryEntries_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_DataDictionaryEntries] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_DataDictionaryEntries] CHECK ([ScopeType]=(0) OR [ScopeTypeItemId] IS NOT NULL),
    CONSTRAINT [FK_DataDictionaryEntries_DataDictionaryCategories] FOREIGN KEY ([DataDictionaryCategoryUniqueId]) REFERENCES [dbo].[DataDictionaryCategories] ([UniqueId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [UQ_DataDictionaryEntries_UniqueId] UNIQUE NONCLUSTERED ([UniqueId] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE TRIGGER TR_DataDictionaryEntries_U ON [dbo].[DataDictionaryEntries]
FOR UPDATE
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditEntries] ([UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], [DataDictionaryAuditUserId], [ActionType])
SELECT [UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], @UserId, 'U' FROM [deleted]

GO
CREATE TRIGGER TR_DataDictionaryEntries_D ON [dbo].[DataDictionaryEntries]
FOR DELETE
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditEntries] ([UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], [DataDictionaryAuditUserId], [ActionType])
SELECT [UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], @UserId, 'D' FROM [deleted]

GO
CREATE TRIGGER TR_DataDictionaryEntries_I ON [dbo].[DataDictionaryEntries]
FOR INSERT
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditEntries] ([UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], [DataDictionaryAuditUserId], [ActionType])
SELECT [UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], @UserId, 'I' FROM [inserted]
