﻿CREATE TABLE [dbo].[BusinessCalendar] (
    [BusCalSurrKey]              VARCHAR (50) NULL,
    [DateID]                     VARCHAR (50) NULL,
    [RealDate]                   VARCHAR (50) NULL,
    [FiscalWeek]                 VARCHAR (50) NULL,
    [FiscalPeriod]               VARCHAR (50) NULL,
    [FiscalQuarter]              VARCHAR (50) NULL,
    [FiscalHalfYear]             VARCHAR (50) NULL,
    [FiscalYear]                 VARCHAR (50) NULL,
    [FiscalYearWeek]             VARCHAR (50) NULL,
    [FiscalYearW]                VARCHAR (50) NULL,
    [FiscalYearPeriod]           VARCHAR (50) NULL,
    [FiscalYearP]                VARCHAR (50) NULL,
    [FiscalYearQuarter]          VARCHAR (50) NULL,
    [FiscalYearQ]                VARCHAR (50) NULL,
    [FiscalYearHalfYear]         VARCHAR (50) NULL,
    [FiscalYearH]                VARCHAR (50) NULL,
    [CalendarWeek]               VARCHAR (50) NULL,
    [CalendarMonth]              VARCHAR (50) NULL,
    [CalendarQuarter]            VARCHAR (50) NULL,
    [CalendarHalfYear]           VARCHAR (50) NULL,
    [CalendarYear]               VARCHAR (50) NULL,
    [CalendarYearWeek]           VARCHAR (50) NULL,
    [CalendarYearW]              VARCHAR (50) NULL,
    [CalendarYearMonth]          VARCHAR (50) NULL,
    [CalendarYearM]              VARCHAR (50) NULL,
    [CalendarYearQuarter]        VARCHAR (50) NULL,
    [CalendarYearQ]              VARCHAR (50) NULL,
    [CalendarYearHalfYear]       VARCHAR (50) NULL,
    [CalendarYearH]              VARCHAR (50) NULL,
    [DayOfMonth]                 VARCHAR (50) NULL,
    [DayOfWeek]                  VARCHAR (50) NULL,
    [DayOfWeekDescription]       VARCHAR (50) NULL,
    [BusinessDay]                VARCHAR (50) NULL,
    [BusinessDayOfFiscalYear]    VARCHAR (50) NULL,
    [BusinessDayOfFiscalQuarter] VARCHAR (50) NULL,
    [BusinessDayOfFiscalPeriod]  VARCHAR (50) NULL,
    [EffectiveDate]              VARCHAR (50) NULL,
    [ExpirationDate]             VARCHAR (50) NULL,
    [CurrentRecord]              VARCHAR (50) NULL,
    [InferredMember]             VARCHAR (50) NULL,
    [Yesterday]                  VARCHAR (50) NULL,
    [MTD]                        VARCHAR (50) NULL,
    [YTD]                        VARCHAR (50) NULL,
    [QTD]                        VARCHAR (50) NULL,
    [PTD]                        VARCHAR (50) NULL,
    [PeriodEnd]                  VARCHAR (50) NULL,
    [QuarterEnd]                 VARCHAR (50) NULL,
    [Rolling5Quarters]           VARCHAR (50) NULL,
    [Rolling13Periods]           VARCHAR (50) NULL,
    [PeriodEndPlusYesterday]     VARCHAR (50) NULL,
    [QuarterEndPlusYesterday]    VARCHAR (50) NULL,
    [CurrentPeriod]              VARCHAR (50) NULL,
    [CurrentQuarter]             VARCHAR (50) NULL,
    [CurrentYear]                VARCHAR (50) NULL,
    [FinancialQuarterCompare]    VARCHAR (50) NULL,
    [LastQuarter]                VARCHAR (50) NULL,
    [LastPeriod]                 VARCHAR (50) NULL,
    [ThisQuarterLastYear]        VARCHAR (50) NULL,
    [ThisPeriodLastYear]         VARCHAR (50) NULL,
    [SameDayPreviousQuarters]    VARCHAR (50) NULL,
    [SameDayPreviousPeriods]     VARCHAR (50) NULL,
    [SameDayPreviousYears]       VARCHAR (50) NULL,
    [Posting Date]               DATETIME     NULL
);

