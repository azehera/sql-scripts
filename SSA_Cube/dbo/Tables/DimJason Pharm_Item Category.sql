﻿CREATE TABLE [dbo].[DimJason Pharm$Item Category] (
    [Item Category Code] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_DimJason Pharm$Item Category] PRIMARY KEY CLUSTERED ([Item Category Code] ASC) WITH (FILLFACTOR = 90)
);

