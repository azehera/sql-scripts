﻿CREATE TABLE [dbo].[SolutionItem] (
    [RowId]                      INT              IDENTITY (1, 1) NOT NULL,
    [SolutionItemId]             UNIQUEIDENTIFIER NOT NULL,
    [SolutionId]                 UNIQUEIDENTIFIER NOT NULL,
    [Name]                       NVARCHAR (255)   NOT NULL,
    [ProviderUniqueId]           UNIQUEIDENTIFIER NOT NULL,
    [ProviderCreationParameters] VARBINARY (MAX)  NOT NULL,
    [DateCreated]                DATETIME         CONSTRAINT [DF_SolutionItem_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    [IsManualScanOnly]           BIT              CONSTRAINT [DF_SolutionItem_IsManualScanOnly] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SolutionItem] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_SolutionItem_Solution] FOREIGN KEY ([SolutionId]) REFERENCES [dbo].[Solution] ([SolutionId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [UQ_SolutionItem_SolutionItemId] UNIQUE NONCLUSTERED ([SolutionItemId] ASC) WITH (FILLFACTOR = 80)
);

