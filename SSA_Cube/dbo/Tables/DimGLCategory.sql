﻿CREATE TABLE [dbo].[DimGLCategory] (
    [G_L Account No_] VARCHAR (20)  NOT NULL,
    [G_L Name]        VARCHAR (100) NULL,
    [RollUpName]      VARCHAR (200) NULL,
    [Category]        VARCHAR (11)  NOT NULL
);

