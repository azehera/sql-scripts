﻿CREATE TABLE [dbo].[DimSalesType] (
    [ID]        INT          NOT NULL,
    [SalesType] VARCHAR (30) NOT NULL,
    CONSTRAINT [PK_DimSalesType] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

