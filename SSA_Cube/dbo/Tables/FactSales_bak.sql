﻿CREATE TABLE [dbo].[FactSales_bak] (
    [Country]                     NVARCHAR (10)    NULL,
    [CustomerPostingGroup]        NVARCHAR (10)    NULL,
    [SalesChannel]                NVARCHAR (20)    NOT NULL,
    [ItemCategoryCode]            NVARCHAR (20)    NOT NULL,
    [ItemCategoryCodeSubcategory] NVARCHAR (50)    NULL,
    [SelltoCustomerID]            NVARCHAR (20)    NULL,
    [SelltoCustomerKey]           NVARCHAR (60)    NULL,
    [ShiptoCustomerKey]           NVARCHAR (60)    NULL,
    [ItemCode]                    NVARCHAR (50)    NULL,
    [ItemCodeSubcategory]         NVARCHAR (50)    NULL,
    [DocumentNo]                  NVARCHAR (20)    NULL,
    [LineDiscount]                DECIMAL (38, 20) NULL,
    [InvoiceDiscount]             DECIMAL (38, 20) NULL,
    [Units]                       DECIMAL (38, 20) NULL,
    [SubCategoryUnits]            DECIMAL (38, 20) NULL,
    [UnitsPerBox]                 DECIMAL (38, 20) NULL,
    [BOX]                         DECIMAL (38, 20) NULL,
    [Amount]                      DECIMAL (38, 20) NULL,
    [Gross Amount]                DECIMAL (38, 20) NULL,
    [Amount Including VAT]        DECIMAL (38, 20) NULL,
    [TaxAmount]                   DECIMAL (38, 20) NULL,
    [Tax Group Code]              NVARCHAR (10)    NULL,
    [Unit Cost (LCY)]             DECIMAL (38, 20) NULL,
    [Quantity]                    DECIMAL (38, 20) NULL,
    [SalesType]                   INT              NULL,
    [LocationID]                  INT              NULL,
    [RegionID]                    NVARCHAR (20)    NULL,
    [Posting Date]                DATETIME         NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_postingDate]
    ON [dbo].[FactSales_bak]([Posting Date] ASC)
    INCLUDE([Country], [CustomerPostingGroup], [SalesChannel], [ItemCategoryCode], [ItemCategoryCodeSubcategory], [SelltoCustomerID], [SelltoCustomerKey], [ShiptoCustomerKey], [ItemCode], [ItemCodeSubcategory], [DocumentNo], [LineDiscount], [InvoiceDiscount], [Units], [SubCategoryUnits], [UnitsPerBox], [BOX], [Amount], [Gross Amount], [Amount Including VAT], [TaxAmount], [Tax Group Code], [Unit Cost (LCY)], [Quantity], [SalesType], [LocationID], [RegionID]) WITH (FILLFACTOR = 90);

