﻿CREATE TABLE [dbo].[DimEntity] (
    [EntityID]   INT           NULL,
    [EntityName] VARCHAR (100) NOT NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [EntityID]
    ON [dbo].[DimEntity]([EntityID] ASC) WITH (FILLFACTOR = 80);

