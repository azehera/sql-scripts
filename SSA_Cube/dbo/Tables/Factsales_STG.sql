﻿CREATE TABLE [dbo].[Factsales_STG] (
    [Ship-to Country_Region Code] NVARCHAR (255)   NULL,
    [Customer Posting Group]      NVARCHAR (255)   NULL,
    [Shortcut Dimension 1 Code]   NVARCHAR (255)   NULL,
    [Sell-to Customer No_]        NVARCHAR (255)   NULL,
    [Ship-to Post Code]           NVARCHAR (255)   NULL,
    [Sell-to Post Code]           NVARCHAR (255)   NULL,
    [Posting Date]                DATETIME         NULL,
    [Order Date]                  DATETIME         NULL,
    [Item Category Code]          NVARCHAR (255)   NULL,
    [No_]                         NVARCHAR (255)   NULL,
    [Document No_]                NVARCHAR (255)   NULL,
    [Line Discount Amount]        DECIMAL (38, 20) NULL,
    [Inv_ Discount Amount]        DECIMAL (38, 20) NULL,
    [Quantity]                    DECIMAL (38, 20) NULL,
    [Qty_ per Unit of Measure]    DECIMAL (38, 20) NULL,
    [Quantity (Base)]             DECIMAL (38, 20) NULL,
    [Amount]                      DECIMAL (38, 20) NULL,
    [Order Item No_]              NVARCHAR (255)   NULL,
    [Unit of Measure]             NVARCHAR (255)   NULL,
    [Line Amount]                 DECIMAL (38, 20) NULL,
    [Amount Including VAT]        DECIMAL (38, 20) NULL,
    [Tax Group Code]              NVARCHAR (255)   NULL,
    [Unit Cost (LCY)]             DECIMAL (38, 20) NULL,
    [Location Code]               NVARCHAR (255)   NULL,
    [RecordType]                  NVARCHAR (255)   NULL
);


GO
CREATE CLUSTERED INDEX [CL_idx_DocumentNo]
    ON [dbo].[Factsales_STG]([Item Category Code] ASC, [Unit of Measure] ASC, [No_] ASC, [Order Item No_] ASC, [Document No_] ASC) WITH (FILLFACTOR = 100);

