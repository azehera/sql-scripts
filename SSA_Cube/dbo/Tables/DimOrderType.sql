﻿CREATE TABLE [dbo].[DimOrderType] (
    [OrderType] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_DimOrderType] PRIMARY KEY CLUSTERED ([OrderType] ASC) WITH (FILLFACTOR = 90)
);

