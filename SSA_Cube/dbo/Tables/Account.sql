﻿CREATE TABLE [dbo].[Account] (
    [RowId]       INT              IDENTITY (1, 1) NOT NULL,
    [AccountId]   UNIQUEIDENTIFIER NOT NULL,
    [Name]        NVARCHAR (255)   NOT NULL,
    [DateCreated] DATETIME         CONSTRAINT [DF_Account_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UQ_Account_AccountID] UNIQUE NONCLUSTERED ([AccountId] ASC) WITH (FILLFACTOR = 80)
);

