﻿CREATE TABLE [dbo].[DataDictionaryAuditCategoryPaths] (
    [RowId]                          INT              IDENTITY (1, 1) NOT NULL,
    [DataDictionaryCategoryUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [Path]                           NVARCHAR (MAX)   NOT NULL,
    [DataDictionaryAuditUserId]      INT              NOT NULL,
    [DateChanged]                    DATETIME         CONSTRAINT [DF_DataDictionaryAuditCategoryPaths_DateChanged] DEFAULT (getutcdate()) NOT NULL,
    [ActionType]                     CHAR (1)         NOT NULL,
    CONSTRAINT [PK_DataDictionaryAuditCategoryPaths] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_DataDictionaryAuditCategoryPaths_DataDictionaryAuditUsers] FOREIGN KEY ([DataDictionaryAuditUserId]) REFERENCES [dbo].[DataDictionaryAuditUsers] ([RowId])
);

