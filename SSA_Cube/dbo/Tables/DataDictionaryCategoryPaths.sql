﻿CREATE TABLE [dbo].[DataDictionaryCategoryPaths] (
    [RowId]                          INT              IDENTITY (1, 1) NOT NULL,
    [DataDictionaryCategoryUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [Path]                           NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_DataDictionaryCategoryPaths] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_DataDictionaryCategoryPaths_DataDictionaryCategories] FOREIGN KEY ([DataDictionaryCategoryUniqueId]) REFERENCES [dbo].[DataDictionaryCategories] ([UniqueId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE TRIGGER TR_DataDictionaryCategoryPaths_U ON [dbo].[DataDictionaryCategoryPaths]
FOR UPDATE
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditCategoryPaths] ([DataDictionaryCategoryUniqueId], [Path], [DataDictionaryAuditUserId], [ActionType])
SELECT [DataDictionaryCategoryUniqueId], [Path], @UserId, 'U' FROM [deleted]

GO
CREATE TRIGGER TR_DataDictionaryCategoryPaths_D ON [dbo].[DataDictionaryCategoryPaths]
FOR DELETE
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditCategoryPaths] ([DataDictionaryCategoryUniqueId], [Path], [DataDictionaryAuditUserId], [ActionType])
SELECT [DataDictionaryCategoryUniqueId], [Path], @UserId, 'D' FROM [deleted]

GO
CREATE TRIGGER TR_DataDictionaryCategoryPaths_I ON [dbo].[DataDictionaryCategoryPaths]
FOR INSERT
AS

DECLARE @UserId int;
EXEC @UserId = dbo.GetCurrentDataDictionaryAuditUserId;

INSERT INTO [dbo].[DataDictionaryAuditCategoryPaths] ([DataDictionaryCategoryUniqueId], [Path], [DataDictionaryAuditUserId], [ActionType])
SELECT [DataDictionaryCategoryUniqueId], [Path], @UserId, 'I' FROM [inserted]
