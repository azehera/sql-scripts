﻿CREATE TABLE [dbo].[BudgetData_CalendarYear] (
    [CalendarYear]          INT NULL,
    [CalendarMonth]         INT NULL,
    [CalendarMonthIDminiPL] INT NULL,
    [CalendarMonthIDGLCube] INT NULL
);

