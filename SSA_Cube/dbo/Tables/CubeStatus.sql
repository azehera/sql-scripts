﻿CREATE TABLE [dbo].[CubeStatus] (
    [CubeName]   VARCHAR (50) NULL,
    [Status]     VARCHAR (15) NULL,
    [ReportDate] DATETIME     NULL
);

