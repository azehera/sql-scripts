﻿CREATE TABLE [dbo].[DocumentControlFlagSets] (
    [RowId]       INT              IDENTITY (1, 1) NOT NULL,
    [ForUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [Key]         NVARCHAR (50)    NOT NULL,
    [Name]        NVARCHAR (255)   NOT NULL,
    [Flags]       XML              NOT NULL,
    [DateCreated] DATETIME         CONSTRAINT [DF_DocumentControlFlagSets_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_DocumentControlFlagSets] PRIMARY KEY CLUSTERED ([RowId] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_DocumentControlFlagSets_ForUniqueId]
    ON [dbo].[DocumentControlFlagSets]([ForUniqueId] ASC) WITH (FILLFACTOR = 80);

