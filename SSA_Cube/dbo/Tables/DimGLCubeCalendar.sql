﻿CREATE TABLE [dbo].[DimGLCubeCalendar] (
    [CalendarYear]      INT       NOT NULL,
    [CalendarMonth]     INT       NOT NULL,
    [CalendarQuarter]   INT       NOT NULL,
    [CalendarMonthID]   INT       NOT NULL,
    [CalendarMonthName] CHAR (15) NULL
);

