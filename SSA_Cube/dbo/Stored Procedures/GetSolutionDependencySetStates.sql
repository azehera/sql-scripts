﻿CREATE PROCEDURE [dbo].[GetSolutionDependencySetStates]
  @SolutionId [uniqueidentifier],
  @EffectiveDate [datetime]
AS 
SELECT [SolutionItemId], [Content], [EffectiveDate] FROM 
(
    SELECT [si].[SolutionItemId], [ds].[Content], [ds].[EffectiveDate], ROW_NUMBER() OVER (PARTITION BY [ds].[CarbonTreeId] ORDER BY [ds].[EffectiveDate] DESC) AS __RN
      FROM [dbo].[CarbonTree] [ct] INNER JOIN 
           [dbo].[SolutionItem] [si]
        ON [ct].[SolutionItemId] = [si].[SolutionItemId] LEFT OUTER JOIN
           [dbo].[CarbonTreeDependencySets] [ds]
        ON [ds].[CarbonTreeId] = [ct].[CarbonTreeId]                      
     WHERE [si].[SolutionId] = @SolutionId
       AND [ct].[DateCreated] <= @EffectiveDate
       AND [ds].[EffectiveDate] <= @EffectiveDate
) sets WHERE [__RN] = 1;

