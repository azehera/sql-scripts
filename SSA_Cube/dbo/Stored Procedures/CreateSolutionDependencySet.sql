﻿CREATE PROCEDURE [dbo].[CreateSolutionDependencySet]
  @Content [varbinary] (MAX),
  @SolutionItemId [uniqueidentifier]
AS 

DECLARE @CarbonTreeId [uniqueidentifier]
SELECT @CarbonTreeId = [CarbonTreeId] FROM [dbo].[CarbonTree] WHERE [SolutionItemId] = @SolutionItemId

IF @CarbonTreeId IS NOT NULL
BEGIN
    INSERT INTO [dbo].[CarbonTreeDependencySets] ([CarbonTreeId], [Content], [EffectiveDate])
    VALUES (@CarbonTreeId, @Content, GETUTCDATE())
END
