﻿CREATE PROCEDURE [dbo].[LoadSolution]
  @SolutionId [uniqueidentifier]
AS 
SELECT [SolutionId], [AccountId], [Name], [DocumentMap], [DateCreated], [DateLastUpdated], [EndpointAliases]
  FROM [dbo].[Solution]
 WHERE [SolutionId] = @SolutionId
