﻿CREATE PROCEDURE [dbo].[DeleteDataDictionaryCategory]
  @UniqueId uniqueidentifier,
  @UserName [nvarchar](128) = NULL
AS
SET NOCOUNT ON;
IF @UserName IS NOT NULL
BEGIN
  EXEC [dbo].[SetDataDictionaryAuditUserName] @UserName
END
DELETE 
  FROM [dbo].[DataDictionaryCategories]
 WHERE [UniqueId] = @UniqueId;



