﻿CREATE PROCEDURE [dbo].[InsertCarbonTreeStateChain]
  @Reversal varbinary (MAX),
  @Version int,
  @EffectiveDate datetime,
  @CarbonTreeId uniqueidentifier,
  @DateCreated datetime
AS
SET NOCOUNT ON;
INSERT INTO [dbo].[CarbonTreeStateChain] ([ReversalPatchContent], [Version], [EffectiveDate], [CarbonTreeId], [DateCreated])
VALUES (@Reversal, @Version, @EffectiveDate, @CarbonTreeId, @DateCreated);
