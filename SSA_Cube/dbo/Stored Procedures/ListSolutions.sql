﻿CREATE PROCEDURE [dbo].[ListSolutions]
  @AccountId [uniqueidentifier]
AS 
SELECT [SolutionId], [AccountId], [Name], [DocumentMap], [DateCreated], [DateLastUpdated], [EndpointAliases]
  FROM [dbo].[Solution]
 WHERE [AccountId] = @AccountId



