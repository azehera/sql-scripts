﻿CREATE PROCEDURE [dbo].[InsertSolution]
  @SolutionId uniqueidentifier,
  @AccountId uniqueidentifier,
  @Name nvarchar (255),
  @DateCreated [datetime],
  @DocumentMap [xml],
  @EndpointAliases [xml]
AS
SET NOCOUNT ON;
INSERT INTO [dbo].[Solution] ([SolutionId], [AccountId], [Name], [DateCreated], [DocumentMap], [DateLastUpdated], [EndpointAliases])
VALUES (@SolutionId, @AccountId, @Name, @DateCreated, @DocumentMap, GETUTCDATE(), @EndpointAliases)
