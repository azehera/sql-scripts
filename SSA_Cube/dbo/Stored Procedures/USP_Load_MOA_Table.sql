﻿


CREATE PROCEDURE [dbo].[USP_Load_MOA_Table] 
As

-- ============================================= 
-- Author:		Roank Shah. 
-- Create date: 04-25-2019 
-- Description:	Created to Truncate and load MOA Table to populate a list of Orders with KITS and KITS' associated SKUs.
   /*
   This Stored procedure has 2 steps
   1) Delete data of last 5 days of orders & Returns to avoid duplicate records
   2) Populate KIT orders & in Last 5 days
   */
-- Modified Date : 03-30-2020
-- Modify this sp to update Delete statment to point NAV_ETL_ARCH
-- ============================================= 

--Delete data of last 5 days of orders & Returns to avoid duplicate records
 Delete from [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION]
 where MOA_ORDER_NBR IN (select MOA_ORDER_NBR from [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION_STG])


 --Load Data From Staging to Core tables
 BEGIN TRAN

 Declare @MOA_ID BIGINT
 select @MOA_ID=MAX([MOA_ID]) from [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION]

 INSERT INTO [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION]
           ([MOA_ID]
           ,[MOA_ORDER_NBR]
           ,[MOA_ORL_ID]
           ,[MOA_SKU]
           ,[MOA_ASSOC_SKU]
           ,[MOA_SKU_UOM]
           ,[MOA_ASSOC_SKU_UOM]
           ,[MOA_QTY]
           ,[MOA_ASSOC_QTY]
           ,[MOA_SYS_TIMESTAMP]
           ,[DocumentNo])
SELECT @MOA_ID+[MOA_ID]
      ,[MOA_ORDER_NBR]
      ,[MOA_ORL_ID]
      ,[MOA_SKU]
      ,[MOA_ASSOC_SKU]
      ,[MOA_SKU_UOM]
      ,[MOA_ASSOC_SKU_UOM]
      ,[MOA_QTY]
      ,[MOA_ASSOC_QTY]
      ,[MOA_SYS_TIMESTAMP]
      ,[DocumentNo]
FROM [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION_STG]

COMMIT TRAN
