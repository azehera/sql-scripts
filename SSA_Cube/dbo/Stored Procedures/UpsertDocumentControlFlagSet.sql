﻿CREATE PROCEDURE [dbo].[UpsertDocumentControlFlagSet]
  @ForUniqueId uniqueidentifier,
  @Key nvarchar (50),
  @Name nvarchar (255),
  @Flags xml
AS
SET NOCOUNT ON;
MERGE [dbo].[DocumentControlFlagSets] AS Trg
USING (SELECT @ForUniqueId, @Key, @Name, @Flags)
AS Src ([ForUniqueId], [Key], [Name], [Flags])
ON ([Trg].[ForUniqueId] = [Src].[ForUniqueId] AND [Trg].[Name] = [Src].[Name])
WHEN MATCHED THEN
 UPDATE SET [Key] = [Src].[Key],
            [Flags] = [Src].[Flags]
WHEN NOT MATCHED BY TARGET THEN
 INSERT ([ForUniqueId], [Key], [Name], [Flags], [DateCreated])
 VALUES ([Src].[ForUniqueId], [Src].[Key], [Src].[Name], [Src].[Flags], GETUTCDATE());
