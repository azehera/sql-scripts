﻿CREATE PROCEDURE [dbo].[GetExportableTreeStates]
  @SolutionId [uniqueidentifier],
  @EffectiveDate [datetime]
AS 
SELECT [CarbonTreeId], [Content], [EffectiveDate], [ct].[SolutionItemId], [ct].[Checksum], [ct].[DateCreated], [ct].[Version]
  FROM [dbo].[CarbonTree] [ct] INNER JOIN 
       [dbo].[SolutionItem] [si] 
    ON [ct].[SolutionItemId] = [si].[SolutionItemId]
 WHERE [si].[SolutionId] = @SolutionId;

WITH 
SolutionItems (CarbonTreeId)
  AS (SELECT [CarbonTreeId]
        FROM [dbo].[CarbonTree] [ct] INNER JOIN 
             [dbo].[SolutionItem] [si] 
          ON [ct].[SolutionItemId] = [si].[SolutionItemId]
       WHERE [si].[SolutionId] = @SolutionId)
SELECT [ReversalPatchContent], [ctsc].[Version], [ctsc].[CarbonTreeId], [ctsc].[EffectiveDate], [ctsc].[DateCreated]
  FROM [dbo].[CarbonTreeStateChain] [ctsc] INNER JOIN 
       [SolutionItems] si
    ON [si].[CarbonTreeId] = [ctsc].[CarbonTreeId]
 WHERE [ctsc].[EffectiveDate] >= @EffectiveDate

