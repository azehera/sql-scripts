﻿/*
 * Delete a row in [dbo].[Solution]
 * 
 * Uses [SolutionId] as the identifying key
 */
CREATE PROCEDURE [dbo].[DeleteSolution]
  @SolutionId uniqueidentifier
AS
SET NOCOUNT ON;

DELETE
  FROM [dbo].[DataDictionaryEntries]
 WHERE [ScopeType] = 2
   AND [ScopeTypeItemId] IN (SELECT [SolutionItemId] FROM [dbo].[SolutionItem] WHERE [SolutionId] = @SolutionId)
   
DELETE
  FROM [dbo].[DataDictionaryCategories]
 WHERE [ScopeType] = 2
   AND [ScopeTypeItemId] IN (SELECT [SolutionItemId] FROM [dbo].[SolutionItem] WHERE [SolutionId] = @SolutionId)

DELETE 
  FROM [dbo].[Solution]
 WHERE [SolutionId] = @SolutionId;

DELETE
  FROM [dbo].[DataDictionaryEntries]
 WHERE [ScopeType] = 1
   AND [ScopeTypeItemId] = @SolutionId
   
DELETE
  FROM [dbo].[DataDictionaryCategories]
 WHERE [ScopeType] = 1
   AND [ScopeTypeItemId] = @SolutionId
   
