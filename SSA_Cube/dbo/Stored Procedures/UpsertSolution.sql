﻿/*
 * Insert or update a row in [dbo].[Solution]
 * 
 * Uses [SolutionId] as the identifying key
 */
CREATE PROCEDURE [dbo].[UpsertSolution]
  @SolutionId uniqueidentifier,
  @AccountId uniqueidentifier,
  @Name nvarchar (255),
  @DocumentMap [xml],
  @EndpointAliases [xml]
AS
SET NOCOUNT ON;
MERGE [dbo].[Solution] AS Trg
USING (SELECT @SolutionId, @AccountId, @Name, @DocumentMap, @EndpointAliases)
AS Src ([SolutionId], [AccountId], [Name], [DocumentMap], [EndpointAliases])
ON ([Trg].[SolutionId] = [Src].[SolutionId])
WHEN MATCHED THEN
 UPDATE SET [AccountId] = [Src].[AccountId],
            [Name] = [Src].[Name],
            [DocumentMap] = [Src].[DocumentMap],
            [DateLastUpdated] = GETUTCDATE(),
            [EndpointAliases] = [Src].[EndpointAliases]
WHEN NOT MATCHED BY TARGET THEN
 INSERT ([SolutionId], [AccountId], [Name], [DocumentMap], [EndpointAliases])
 VALUES ([Src].[SolutionId], [Src].[AccountId], [Src].[Name], [Src].[DocumentMap], [Src].[EndpointAliases]);




