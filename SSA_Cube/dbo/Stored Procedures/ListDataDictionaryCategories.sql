﻿CREATE PROCEDURE [dbo].[ListDataDictionaryCategories]
AS
SELECT [UniqueId], [Name], [DataType], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DateCreated], [DataTypeData]
  FROM [dbo].[DataDictionaryCategories]

SELECT [DataDictionaryCategoryUniqueId], [Path]
  FROM [dbo].[DataDictionaryCategoryPaths]


