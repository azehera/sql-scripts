﻿CREATE PROCEDURE [dbo].[InsertCarbonTree]
  @CarbonTreeId uniqueidentifier,
  @Content varbinary (MAX),
  @Checksum bigint,
  @Version int,
  @EffectiveDate datetime,
  @SolutionItemId uniqueidentifier,
  @DateCreated datetime
AS
SET NOCOUNT ON;
INSERT INTO [dbo].[CarbonTree] ([CarbonTreeId], [Content], [Checksum], [Version], [EffectiveDate], [SolutionItemId], [DateCreated])
VALUES (@CarbonTreeId, @Content, @Checksum, @Version, @EffectiveDate, @SolutionItemId, @DateCreated);
