﻿CREATE PROCEDURE [dbo].[GetCurrentDataDictionaryAuditUserId]
AS
BEGIN
     DECLARE @UserId int, @UserName [nvarchar] (128)
     SET @UserName = ISNULL(LEFT(CONVERT([varchar](255), CONTEXT_INFO()), 128), SUSER_SNAME())
     
     SELECT @UserId = RowId FROM [dbo].[DataDictionaryAuditUsers] WITH (HOLDLOCK) WHERE [UserName] = @UserName
     IF @UserId IS NULL
     BEGIN
        INSERT INTO [dbo].[DataDictionaryAuditUsers] ([UserName])
        VALUES (@UserName)
        SET @UserId = SCOPE_IDENTITY()
     END
     RETURN(@UserId);
END;

