﻿CREATE PROCEDURE [dbo].[InsertSolutionSnapshot]
  @SolutionId uniqueidentifier,
  @SnapshotStartDate [datetime],
  @SnapshotEndDate [datetime],
  @IsImport int,
  @IsDependencyScanOnly int,
  @UpdatedItems [dbo].[GuidTable] READONLY
AS
SET NOCOUNT ON;

DECLARE @rowId [int];

INSERT INTO [dbo].[SolutionSnapshots] ([SolutionId], [SnapshotStartDate], [SnapshotEndDate], [IsImport], [IsDependencyScanOnly])
VALUES (@SolutionId, @SnapshotStartDate, @SnapshotEndDate, @IsImport, @IsDependencyScanOnly);

SET @rowId = SCOPE_IDENTITY();

INSERT INTO [dbo].[SolutionSnapshotItems] ([SolutionSnapshotId], [SolutionItemId])
SELECT @rowId, [Value] FROM @UpdatedItems

