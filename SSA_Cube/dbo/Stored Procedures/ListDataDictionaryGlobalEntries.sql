﻿CREATE PROCEDURE [dbo].[ListDataDictionaryGlobalEntries]
AS

SELECT [UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], [DateCreated]
  FROM [dbo].[DataDictionaryEntries] 
 WHERE [IsGlobal] = 1

SELECT [DataDictionaryEntryUniqueId], [Path]
  FROM [dbo].[DataDictionaryEntryPaths] [ddep] INNER JOIN
       [dbo].[DataDictionaryEntries] [dde] ON [ddep].[DataDictionaryEntryUniqueId] = [dde].[UniqueId]
 WHERE [IsGlobal] = 1
