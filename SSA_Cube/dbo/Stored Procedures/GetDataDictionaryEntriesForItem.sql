﻿

CREATE PROCEDURE [dbo].[GetDataDictionaryEntriesForItem]
  @ItemId [uniqueidentifier]
AS

CREATE TABLE #uniqueIds (id [uniqueidentifier] PRIMARY KEY CLUSTERED)

INSERT INTO [#uniqueIds] ([id])
SELECT UniqueId FROM (
-- metabase level
SELECT [ddc].[UniqueId]
  FROM [dbo].[DataDictionaryEntries] [ddc]
 WHERE [ddc].[ScopeType] = 0
UNION ALL
-- solution level when asking for a solution item
SELECT [ddc].[UniqueId]
  FROM [dbo].[DataDictionaryEntries] [ddc] INNER JOIN
       [dbo].[SolutionItem] [si] ON [si].[SolutionId] = [ddc].[ScopeTypeItemId]
 WHERE [ddc].[ScopeType] = 1 AND [si].[SolutionItemId] = @ItemId
UNION ALL
-- solution item level when asking for a solution
SELECT [ddc].[UniqueId]
  FROM [dbo].[DataDictionaryEntries] [ddc] INNER JOIN
       [dbo].[SolutionItem] [si] ON [si].[SolutionItemId] = [ddc].[ScopeTypeItemId]
 WHERE [ddc].[ScopeType] = 2 AND [si].[SolutionId] = @ItemId
UNION ALL
-- solution / item level when asking for that level
SELECT [ddc].[UniqueId]
  FROM [dbo].[DataDictionaryEntries] [ddc]
 WHERE [ddc].[ScopeType] IN (1, 2) AND [ddc].[ScopeTypeItemId] = @ItemId
) uniqueIds GROUP BY [UniqueId]

SELECT [dde].[UniqueId], [dde].[DataDictionaryCategoryUniqueId], [dde].[ScopeType], [dde].[ScopeTypeItemId], [dde].[ProviderCompatibilityGroupId], [dde].[Value], [dde].[IsGlobal], [dde].[DateCreated]
  FROM [dbo].[DataDictionaryEntries] [dde] INNER JOIN 
       #uniqueIds u ON [u].[id] = [dde].[UniqueId]

SELECT [DataDictionaryEntryUniqueId], [Path]
  FROM [dbo].[DataDictionaryEntryPaths] [dde] INNER JOIN
       #uniqueIds u ON [u].[id] = [dde].[DataDictionaryEntryUniqueId]
 
DROP TABLE #uniqueIds



