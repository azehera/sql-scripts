﻿/*
 * Insert or Update a row in [dbo].[SolutionItem]
 * 
 * Uses [SolutionItemId] as the identifying key
 */
CREATE PROCEDURE [dbo].[UpsertSolutionItem]
  @SolutionItemId uniqueidentifier,
  @SolutionId uniqueidentifier,
  @Name nvarchar (255),
  @ProviderUniqueId uniqueidentifier,
  @ProviderCreationParameters varbinary (MAX),
  @IsManualScanOnly [bit] = 0
AS
SET NOCOUNT ON;
MERGE [dbo].[SolutionItem] AS Trg
USING (SELECT @SolutionItemId, @SolutionId, @Name, @ProviderUniqueId, @ProviderCreationParameters, @IsManualScanOnly)
AS Src ([SolutionItemId], [SolutionId], [Name], [ProviderUniqueId], [ProviderCreationParameters], [IsManualScanOnly])
ON ([Trg].[SolutionItemId] = [Src].[SolutionItemId])
WHEN MATCHED THEN
 UPDATE SET [SolutionId] = [Src].[SolutionId],
            [Name] = [Src].[Name],
            [ProviderUniqueId] = [Src].[ProviderUniqueId],
            [ProviderCreationParameters] = [Src].[ProviderCreationParameters],
            [IsManualScanOnly] = [Src].[IsManualScanOnly]
WHEN NOT MATCHED BY TARGET THEN
 INSERT ([SolutionItemId], [SolutionId], [Name], [ProviderUniqueId], [ProviderCreationParameters], [IsManualScanOnly])
 VALUES ([Src].[SolutionItemId], [Src].[SolutionId], [Src].[Name], [Src].[ProviderUniqueId], [Src].[ProviderCreationParameters], [Src].[IsManualScanOnly]);


