﻿CREATE PROCEDURE [dbo].[ListSolutionSnapshotDates]
  @SolutionId [uniqueidentifier]
AS 
SELECT [SolutionItemId], [EffectiveDate] FROM (
    SELECT [si].[SolutionId], [si].[SolutionItemId], [ctsc].[EffectiveDate] 
      FROM [dbo].[CarbonTreeStateChain] [ctsc] INNER JOIN 
           [dbo].[CarbonTree] [ct]
        ON [ctsc].[CarbonTreeId] = [ct].[CarbonTreeId] INNER JOIN 
           [dbo].[SolutionItem] [si] 
        ON [ct].[SolutionItemId] = [si].[SolutionItemId]
    UNION ALL
    SELECT [si].[SolutionId], [si].[SolutionItemId], [ct].[EffectiveDate] 
      FROM [dbo].[CarbonTree] [ct] INNER JOIN 
           [dbo].[SolutionItem] [si] 
        ON [ct].[SolutionItemId] = [si].[SolutionItemId]
) allEffectiveDates
WHERE [SolutionId] = @SolutionId
ORDER BY [EffectiveDate] DESC, [SolutionItemId]

