﻿CREATE PROCEDURE [dbo].[GetSolutionItemCurrentState]
  @SolutionItemId [uniqueidentifier]
AS 
SELECT [Content]
  FROM [dbo].[CarbonTree]
 WHERE [SolutionItemId] = @SolutionItemId

