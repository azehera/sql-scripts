﻿CREATE PROCEDURE [dbo].[InsertSolutionItem]
  @SolutionItemId uniqueidentifier,
  @SolutionId uniqueidentifier,
  @Name nvarchar (255),
  @ProviderUniqueId uniqueidentifier,
  @ProviderCreationParameters varbinary (MAX),
  @DateCreated [datetime],
  @IsManualScanOnly [bit] = 0
AS
SET NOCOUNT ON;
INSERT INTO [dbo].[SolutionItem] ([SolutionItemId], [SolutionId], [Name], [ProviderUniqueId], [ProviderCreationParameters], [DateCreated], [IsManualScanOnly])
VALUES (@SolutionItemId, @SolutionId, @Name, @ProviderUniqueId, @ProviderCreationParameters, @DateCreated, @IsManualScanOnly)   


