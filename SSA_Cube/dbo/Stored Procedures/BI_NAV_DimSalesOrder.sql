﻿

/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_DimSalesOrder] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Update Sales Order   
PARAMETERS/VARRIABLES:
NOTES:

*******************************************************************************************/


CREATE PROCEDURE [dbo].[BI_NAV_DimSalesOrder] 
as

WITH CTE as (SELECT GINID as GroupNO, MIN([Posting Date]) as FirstOrderDate
,Year( MIN([Posting Date])) as Year
,'Q'+''+Convert(Varchar(2),Datepart(QQ,MIN([Posting Date]))) as DraftQuarter
,Month(MIN([Posting Date])) as DraftMonth
FROM
(
    SELECT GINID,DocumentNo,[Posting Date]
    FROM [BI_SSAS_Cubes].[dbo].[FactSalesArchive]
    UNION ALL
    SELECT GINID,DocumentNo,[Posting Date]
    FROM [BI_SSAS_Cubes].[dbo].[FactSales_MDM]
) as subQuery
GROUP BY GINID 
)


Update b
set [Draft Year]=Year,
b.FirstOrderDate=a.FirstOrderDate,
b.DraftQuarter=a.DraftQuarter,
b.DraftMonth=a.DraftMonth
from CTE a
join BI_SSAS_Cubes.dbo.DimSalesOrder_MDM b
on a.GroupNO=b.GINID




































