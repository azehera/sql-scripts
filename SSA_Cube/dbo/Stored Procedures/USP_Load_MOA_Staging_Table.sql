﻿


CREATE PROCEDURE [dbo].[USP_Load_MOA_Staging_Table] 
As

Declare @PostingDate Date 
set @PostingDate = GETDATE()-5
 


-- ============================================= 
-- Author:		Roank Shah. 
-- Create date: 04-25-2019 
-- Description:	Created to Truncate and load Staging Tables to populate a list of Orders with KITS and KITS' associated SKUs.
   /*
   This Stored procedure has 3 steps
   1) Truncate Staging table
   2) Populate KIT orders in Last 5 days
   3) Populate KIT return orders in Last 5 days
    */
-- Modified Date : 03-30-2020
-- Modify this sp to update Delete statment to point NAV_ETL_ARCH
/*
DEHA ticket : https://medifast.atlassian.net/browse/DEHA-394
Developer Name: Gurusha
*/

-- Modified Date : 07-31-2020
-- Modify this sp to Pull data from Hybris_ETL Db instead of ECOMM_ETL_Hybris DB

-- ============================================= 


BEGIN TRY
--Truncate Data from Staging table
TRUNCATE TABLE [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION_STG]


--ORDERS
--Load Orders data of (Getdate()-5) last 5 days to Staging table
INSERT INTO [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION_STG]
           ([MOA_ORDER_NBR],[MOA_ASSOC_SKU],[MOA_SKU],[MOA_SKU_UOM],[MOA_QTY],[MOA_ASSOC_QTY],[MOA_ORL_ID],[MOA_ASSOC_SKU_UOM],[DocumentNo])
SELECT DISTINCT 
LTRIM(RTRIM(cast(K.MOA_ORDER_NBR as CHAR(40)))) as MOA_ORDER_NBR,
LTRIM(RTRIM(cast(K.MOA_ASSOC_SKU as CHAR(128)))) as MOA_ASSOC_SKU,
LTRIM(RTRIM(cast(KE.MOA_SKU as CHAR(128)))) as MOA_SKU,
LTRIM(RTRIM(cast(KE.MOA_ASSOC_SKU_UOM as CHAR(40)))) AS MOA_SKU_UOM,
KE.MOA_QTY,
K.MOA_ASSOC_QTY,
KE.MOA_ORL_ID,
'KT' AS MOA_ASSOC_SKU_UOM,
LTRIM(RTRIM(K.DocumentNo)) as DocumentNo
FROM
(
--KIT 
SELECT 
	ord.p_code AS MOA_ORDER_NBR,
	p.p_sku AS MOA_ASSOC_SKU,
	p.p_code AS SkuDescription,
	oe.p_bundleno,
	oe.p_quantity AS MOA_ASSOC_QTY,
	ABS(oe.p_kitno) AS p_kitno,
	oe.p_entrynumber AS MOA_ORL_ID
	,SH.No_ as DocumentNo
FROM [HYBRIS_ETL].mdf_prd_database.orderentries oe with(NOLOCK)
	INNER JOIN [HYBRIS_ETL].mdf_prd_database.orders ord with(NOLOCK)
			ON oe.p_order = ord.pk
	INNER JOIN [HYBRIS_ETL].mdf_prd_database.products p with(NOLOCK)
			ON oe.p_product = p.pk 
    INNER JOIN [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] SH with(NOLOCK)
	        ON SH.[External Document No_] COLLATE DATABASE_DEFAULT = ord.p_code
WHERE oe.p_bundleno = 0
		AND p_kitno IS NOT NULL
		AND SH.[Posting Date] >= @PostingDate
		--AND cast(ord.createdTS as Date) >= Getdate()-5
		AND p.p_sku not in ('79938','79948','81811_KT')  
		AND ord.p_status is not null
		AND oe.p_quantity >0.00000000
) K
INNER JOIN	
(
--KIT Explosion 
SELECT ord.p_code AS MOA_ORDER_NBR,
CASE WHEN p.p_sku Like '%[0-9A-Z]' THEN REVERSE(SUBSTRING(REVERSE(p.p_sku), 4, 20)) ELSE p.p_sku END AS MOA_SKU,
CASE WHEN p.p_sku Like '%[0-9A-Z]' THEN REVERSE(SUBSTRING(REVERSE(p.p_sku), 1, 2)) END AS MOA_ASSOC_SKU_UOM,
p.p_code AS SkuDescription,
oe.p_bundleno,
oe.p_quantity AS MOA_QTY,
ABS(oe.p_kitno) AS p_kitno,
oe.p_entrynumber AS MOA_ORL_ID
,SH.No_ as DocumentNo
FROM [HYBRIS_ETL].mdf_prd_database.orderentries oe with(NOLOCK)
	INNER JOIN [HYBRIS_ETL].mdf_prd_database.orders ord with(NOLOCK)
			ON oe.p_order = ord.pk
	INNER JOIN [HYBRIS_ETL].mdf_prd_database.products p with(NOLOCK)
			ON oe.p_product = p.pk 
    INNER JOIN [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] SH with(NOLOCK)
	        ON SH.[External Document No_] COLLATE DATABASE_DEFAULT = ord.p_code
WHERE
  oe.p_bundleno != 0
AND p_kitno IS NOT NULL
AND SH.[Posting Date] >= @PostingDate
--AND cast(ord.createdTS as Date) >= '2019-02-22'
--AND ord.Order_Category IS NULL 
AND ord.p_status is not null
AND oe.p_quantity >0.00000000
) KE
 ON K.MOA_ORDER_NBR = KE.MOA_ORDER_NBR AND
K.p_kitno = KE.p_kitno;
 
END TRY

BEGIN CATCH
		PRINT 'Error line For Orders: ' + CAST(ERROR_LINE() AS VARCHAR(25)) + ' Error number: ' + CAST(ERROR_NUMBER() AS VARCHAR(25)) + ' Error message: ' + ERROR_MESSAGE();
END CATCH
 

 BEGIN TRY
--RETRUNS
--Load Return Orders data of (Getdate()-5) last 5 days to Staging table
INSERT INTO [NAV_ETL_ARCH].[dbo].[MEDIFAST_ORDERLINE_ASSOCIATION_STG]
          ([MOA_ORDER_NBR],[MOA_ASSOC_SKU],[MOA_SKU],[MOA_SKU_UOM],[MOA_QTY],[MOA_ASSOC_QTY],[MOA_ORL_ID],[MOA_ASSOC_SKU_UOM],[DocumentNo]) 
SELECT DISTINCT 
LTRIM(RTRIM(cast(K.MOA_ORDER_NBR as CHAR(40)))) as MOA_ORDER_NBR,
LTRIM(RTRIM(cast(K.MOA_ASSOC_SKU as CHAR(128)))) as MOA_ASSOC_SKU,
LTRIM(RTRIM(cast(KE.MOA_SKU as CHAR(128)))) as MOA_SKU,
LTRIM(RTRIM(cast(KE.MOA_ASSOC_SKU_UOM as CHAR(40)))) AS MOA_SKU_UOM,
KE.MOA_QTY,
K.MOA_ASSOC_QTY,
KE.MOA_ORL_ID,
'KT' AS MOA_ASSOC_SKU_UOM,
LTRIM(RTRIM(K.DocumentNo)) AS DocumentNo
FROM
(
--KIT 
select
	ord.p_code AS MOA_ORDER_NBR,
	p.p_sku AS MOA_ASSOC_SKU,
	p.p_code AS SkuDescription,
	oe.p_bundleno,
	oe.p_quantity AS MOA_ASSOC_QTY,
	ABS(oe.p_kitno) AS p_kitno,
	oe.p_entrynumber AS MOA_ORL_ID
	,SH.No_ as DocumentNo --INTO #X
from [HYBRIS_ETL].[mdf_prd_database].[returnrequest] RR with(NOLOCK)
		INNER JOIN [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] SH with(NOLOCK)
				ON RR.p_rma COLLATE DATABASE_DEFAULT = SH.[Posting Description] 
		INNER JOIN [HYBRIS_ETL].[mdf_prd_database].[orders] ord with(NOLOCK)
				ON ord.PK = RR.p_order
        INNER JOIN [HYBRIS_ETL].[mdf_prd_database].[orderentries] oe with(NOLOCK)
		        ON oe.p_order = ord.pk
		INNER JOIN [HYBRIS_ETL].[mdf_prd_database].[products] p with(NOLOCK)
				ON oe.p_product = p.pk 
where [Posting Date] >= @PostingDate 
AND oe.p_bundleno = 0 
AND p_kitno IS NOT NULL 
AND p.p_sku not in ('79938','79948','81811_KT') 
AND oe.p_quantity >0.00000000
) K
INNER JOIN 
(
--KIT EXPLOSION
SELECT 
ord.p_code AS MOA_ORDER_NBR,
CASE WHEN p.p_sku Like '%[0-9A-Z]' THEN REVERSE(SUBSTRING(REVERSE(p.p_sku), 4, 20)) ELSE p.p_sku END AS MOA_SKU,
CASE WHEN p.p_sku Like '%[0-9A-Z]' THEN REVERSE(SUBSTRING(REVERSE(p.p_sku), 1, 2)) END AS MOA_ASSOC_SKU_UOM,
p.p_code AS SkuDescription,
oe.p_bundleno,
oe.p_quantity AS MOA_QTY,
ABS(oe.p_kitno) AS p_kitno,
oe.p_entrynumber AS MOA_ORL_ID
,SH.No_ as DocumentNo
FROM [HYBRIS_ETL].[mdf_prd_database].[returnrequest] RR with(NOLOCK)
		INNER JOIN [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] SH with(NOLOCK)
				ON RR.p_rma COLLATE DATABASE_DEFAULT = SH.[Posting Description] 
		INNER JOIN [HYBRIS_ETL].[mdf_prd_database].[orders] ord with(NOLOCK)
				ON ord.PK = RR.p_order
        INNER JOIN [HYBRIS_ETL].[mdf_prd_database].[orderentries] oe with(NOLOCK)
		        ON oe.p_order = ord.pk
		INNER JOIN [HYBRIS_ETL].[mdf_prd_database].[products] p with(NOLOCK)
				ON oe.p_product = p.pk 
WHERE
  oe.p_bundleno != 0
AND p_kitno IS NOT NULL
AND [Posting Date] >=@PostingDate
AND oe.p_quantity >0.00000000
) KE
 ON K.MOA_ORDER_NBR = KE.MOA_ORDER_NBR AND
K.p_kitno = KE.p_kitno


END TRY

BEGIN CATCH
		PRINT 'Error line For Orders: ' + CAST(ERROR_LINE() AS VARCHAR(25)) + ' Error number: ' + CAST(ERROR_NUMBER() AS VARCHAR(25)) + ' Error message: ' + ERROR_MESSAGE();
END CATCH


