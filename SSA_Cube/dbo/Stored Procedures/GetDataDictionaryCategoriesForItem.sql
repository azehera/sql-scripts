﻿CREATE PROCEDURE [dbo].[GetDataDictionaryCategoriesForItem]
  @ItemId [uniqueidentifier]
AS

CREATE TABLE #uniqueIds (id [uniqueidentifier] PRIMARY KEY CLUSTERED)

INSERT INTO [#uniqueIds] ([id])
SELECT UniqueId FROM (
-- metabase level
SELECT [ddc].[UniqueId]
  FROM [dbo].[DataDictionaryCategories] [ddc]
 WHERE [ddc].[ScopeType] = 0
UNION ALL
-- solution level when asking for a solution item
SELECT [ddc].[UniqueId]
  FROM [dbo].[DataDictionaryCategories] [ddc] INNER JOIN
       [dbo].[SolutionItem] [si] ON [si].[SolutionId] = [ddc].[ScopeTypeItemId]
 WHERE [ddc].[ScopeType] = 1 AND [si].[SolutionItemId] = @ItemId
UNION ALL
-- solution item level when asking for a solution
SELECT [ddc].[UniqueId]
  FROM [dbo].[DataDictionaryCategories] [ddc] INNER JOIN
       [dbo].[SolutionItem] [si] ON [si].[SolutionItemId] = [ddc].[ScopeTypeItemId]
 WHERE [ddc].[ScopeType] = 2 AND [si].[SolutionId] = @ItemId
UNION ALL
-- solution / item level when asking for that level
SELECT [ddc].[UniqueId]
  FROM [dbo].[DataDictionaryCategories] [ddc]
 WHERE [ddc].[ScopeType] IN (1, 2) AND [ddc].[ScopeTypeItemId] = @ItemId
) uniqueIds GROUP BY [UniqueId]

SELECT [ddc].[UniqueId], [ddc].[Name], [ddc].[DataType], [ddc].[ScopeType], [ddc].[ScopeTypeItemId], [ddc].[ProviderCompatibilityGroupId], [ddc].[IsRequired], [ddc].[DateCreated], [ddc].[DataTypeData]
  FROM [dbo].[DataDictionaryCategories] [ddc] INNER JOIN 
       #uniqueIds u ON [u].[id] = [ddc].[UniqueId]

SELECT [DataDictionaryCategoryUniqueId], [Path]
  FROM [dbo].[DataDictionaryCategoryPaths] [ddcp] INNER JOIN
       #uniqueIds u ON [u].[id] = [ddcp].[DataDictionaryCategoryUniqueId]
 
DROP TABLE #uniqueIds




