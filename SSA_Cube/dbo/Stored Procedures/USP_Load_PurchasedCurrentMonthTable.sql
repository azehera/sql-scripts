﻿

CREATE PROCEDURE [dbo].[USP_Load_PurchasedCurrentMonthTable] 

-- ============================================= 
-- Author:		Micah W. 
-- Create date: 07-06-2019 
-- Description:	Created to Truncate and load table to see which customers made purchases in current month
-- ============================================= 
 
 AS

TRUNCATE TABLE BI_SSAS_Cubes.dbo.StagePurchasedCurrentMonth

INSERT INTO BI_SSAS_Cubes.dbo.StagePurchasedCurrentMonth
SELECT DISTINCT(SelltoCustomerID), '1'
FROM BI_SSAS_Cubes.dbo.FactSales
WHERE EOMONTH([Posting Date]) = EOMONTH(GETDATE()) AND Amount >=0

