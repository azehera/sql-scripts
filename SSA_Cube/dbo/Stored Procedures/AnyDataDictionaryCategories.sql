﻿CREATE PROCEDURE [dbo].[AnyDataDictionaryCategories]
AS
DECLARE @Result [bit]
SET @Result = 0
SELECT TOP 1 @Result = 1 FROM [dbo].[DataDictionaryCategories]
SELECT @Result AS Result

