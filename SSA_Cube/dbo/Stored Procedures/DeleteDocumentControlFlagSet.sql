﻿
CREATE PROCEDURE [dbo].[DeleteDocumentControlFlagSet]
  @ForUniqueId uniqueidentifier,
  @Name nvarchar (255)
AS
SET NOCOUNT ON;
DELETE 
  FROM [dbo].[DocumentControlFlagSets]
 WHERE [ForUniqueId] = @ForUniqueId
   AND [Name] = @Name
