﻿CREATE PROCEDURE [dbo].[DeleteDataDictionaryEntry]
  @UniqueId uniqueidentifier,
  @UserName [nvarchar](128) = NULL
AS
SET NOCOUNT ON;
IF @UserName IS NOT NULL
BEGIN
  EXEC [dbo].[SetDataDictionaryAuditUserName] @UserName
END
DELETE 
  FROM [dbo].[DataDictionaryEntries]
 WHERE [UniqueId] = @UniqueId;



