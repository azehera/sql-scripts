﻿CREATE PROCEDURE [dbo].[UpsertDataDictionaryCategory]
  @UniqueId uniqueidentifier,
  @Name nvarchar (255),
  @DataType int,
  @ScopeType int,
  @ScopeTypeItemId uniqueidentifier,  
  @ProviderCompatibilityGroupId uniqueidentifier,
  @IsRequired bit,
  @DateCreated datetime,
  @DataTypeData [nvarchar](MAX),
  @Paths [dbo].[StringTable] READONLY,
  @UserName [nvarchar](128) = NULL
AS
SET NOCOUNT ON;
IF @UserName IS NOT NULL
BEGIN
  EXEC [dbo].[SetDataDictionaryAuditUserName] @UserName
END
MERGE [dbo].[DataDictionaryCategories] AS Trg
USING (SELECT @UniqueId, @Name, @DataType, @ScopeType, @ScopeTypeItemId, @ProviderCompatibilityGroupId, @IsRequired, @DateCreated, @DataTypeData)
AS Src ([UniqueId], [Name], [DataType], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DateCreated], [DataTypeData])
ON ([Trg].[UniqueId] = [Src].[UniqueId])
WHEN MATCHED THEN
 UPDATE SET [Name] = [Src].[Name],
            [DataType] = [Src].[DataType],
            [DataTypeData] = [Src].[DataTypeData],
            [ScopeType] = [Src].[ScopeType],
            [ScopeTypeItemId] = [Src].[ScopeTypeItemId],
            [ProviderCompatibilityGroupId] = [Src].[ProviderCompatibilityGroupId],
            [IsRequired] = [Src].[IsRequired]
WHEN NOT MATCHED BY TARGET THEN
 INSERT ([UniqueId], [Name], [DataType], [DataTypeData], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [IsRequired], [DateCreated])
 VALUES ([Src].[UniqueId], [Src].[Name], [Src].[DataType], [Src].[DataTypeData], [Src].[ScopeType], [Src].[ScopeTypeItemId], [Src].[ProviderCompatibilityGroupId], [Src].[IsRequired], [Src].[DateCreated]);

MERGE [dbo].[DataDictionaryCategoryPaths] AS Trg
USING (SELECT @UniqueId, [Value] FROM @Paths)
AS Src ([DataDictionaryCategoryUniqueId], [Path])
ON ([Trg].[DataDictionaryCategoryUniqueId] = [Src].[DataDictionaryCategoryUniqueId] AND [Trg].[Path] = [Src].[Path] COLLATE Latin1_General_100_CS_AS)
WHEN NOT MATCHED BY TARGET THEN
 INSERT ([DataDictionaryCategoryUniqueId], [Path])
 VALUES ([Src].[DataDictionaryCategoryUniqueId], [Src].[Path])
WHEN NOT MATCHED BY SOURCE AND [Trg].[DataDictionaryCategoryUniqueId] = @UniqueId THEN DELETE; 



