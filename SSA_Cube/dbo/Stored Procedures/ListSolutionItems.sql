﻿CREATE PROCEDURE [dbo].[ListSolutionItems]
  @SolutionId [uniqueidentifier]
AS 
SELECT [SolutionItemId], [SolutionId], [Name], [ProviderUniqueId], [ProviderCreationParameters], [DateCreated], [IsManualScanOnly]
  FROM [dbo].[SolutionItem] 
 WHERE [SolutionId] = @SolutionId



