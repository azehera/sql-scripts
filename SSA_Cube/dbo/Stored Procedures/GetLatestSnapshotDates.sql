﻿CREATE PROCEDURE [dbo].[GetLatestSnapshotDates]
  @AccountId uniqueidentifier
AS
SET NOCOUNT ON;

SELECT [ss].[SolutionId], MAX([SnapshotEndDate]) AS LastUpdateDate 
  FROM [dbo].[SolutionSnapshots] [ss] INNER JOIN 
       [dbo].[Solution] [s] 
    ON [ss].[SolutionId] = [s].[SolutionId]
 WHERE [s].[AccountId] = @AccountId
 GROUP BY [ss].[SolutionId]
