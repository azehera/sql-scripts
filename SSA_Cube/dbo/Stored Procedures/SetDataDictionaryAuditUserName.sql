﻿CREATE PROCEDURE [dbo].[SetDataDictionaryAuditUserName]
  @UserName [nvarchar] (128)
AS
DECLARE @b [varbinary](255)
SET @b = CONVERT([varbinary](255), @UserName)
SET CONTEXT_INFO @b
