﻿/*
 * Delete a row in [dbo].[SolutionItem]
 * 
 * Uses [SolutionItemId] as the identifying key
 */
CREATE PROCEDURE [dbo].[DeleteSolutionItem]
  @SolutionItemId uniqueidentifier
AS
SET NOCOUNT ON;

DELETE 
  FROM [dbo].[SolutionSnapshotItems]
 WHERE [SolutionItemId] = @SolutionItemId

DELETE 
  FROM [dbo].[SolutionItem]
 WHERE [SolutionItemId] = @SolutionItemId;
 
DELETE
  FROM [dbo].[DataDictionaryEntries]
 WHERE [ScopeType] = 2
   AND [ScopeTypeItemId] = @SolutionItemId
   
DELETE
  FROM [dbo].[DataDictionaryCategories]
 WHERE [ScopeType] = 2
   AND [ScopeTypeItemId] = @SolutionItemId
   

