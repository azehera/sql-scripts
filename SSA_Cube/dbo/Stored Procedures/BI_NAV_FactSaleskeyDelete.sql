﻿


/******************************************************************************************
OBJECT NAME:      [dbo].[BI_NAV_FactSaleskeyDelete] 
DEVELOPER:  Kalpesh Patel      
DATE:  8/23/2012           
DESCRIPTION: Maintaining History Data.     
PARAMETERS/VARRIABLES:
NOTES:Section 1: Added for another 15days for History


Modification - Micah Williams 10-15-2018 - Changing delete statement from CTE to standard delete statement

*******************************************************************************************/
CREATE PROCEDURE [dbo].[BI_NAV_FactSaleskeyDelete] 
as


Truncate table dbo.DimSalesOrder
Truncate table dbo.DimSelltoCustomerKey
Truncate table dbo.DimShiptoCustomerKey



----removed 10-15-2018
--;with cte as (Select * from dbo.FactSales 
--where [Posting Date] >=DateAdd(dd, DateDiff(dd,0,GetDate()-30), 0))
--delete  from cte 
----removed 10-15-2018


----added 10-15-2018
DELETE FROM dbo.FactSales   
where [Posting Date] >=DateAdd(dd, DateDiff(dd,0,GetDate()-30), 0)
----added 10-15-2018



--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-1), 0)

--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-2), 0)

--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-3), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-4), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-5), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-6), 0)

--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-7), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-8), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-9), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-10), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-11), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-12), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-13), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-14), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-15), 0)

--/*****Section 1 *****/


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-16), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-17), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-18), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-19), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-20), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-21), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-22), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-23), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-24), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-25), 0)

--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-26), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-27), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-28), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-29), 0)


--Delete from dbo.FactSales
--where [Posting Date] =DateAdd(dd, DateDiff(dd,0,GetDate()-30), 0)





















