﻿

CREATE PROCEDURE [dbo].[USP_Load_Infuser_PH_Staging_Tables] 

-- ============================================= 
-- Author:		Micah W. 
-- Create date: 02-06-2019 
-- Description:	Created to Truncate and load Staging Tables to see a list of customers that purchased Infusers or PH
-- ============================================= 
 
 AS

TRUNCATE TABLE BI_SSAS_Cubes.dbo.StagePurchasedInfusers
INSERT INTO BI_SSAS_Cubes.dbo.StagePurchasedInfusers
SELECT DISTINCT
        selltocustomerid ,
        1 AS [Infuser Flag]

FROM    dbo.FactSales
WHERE   itemcode IN ( '72600', '72610', '72620', '72640', '72650', '72690',
                      '72700' )


TRUNCATE TABLE BI_SSAS_Cubes.dbo.StagePurchasedPH
INSERT INTO BI_SSAS_Cubes.dbo.StagePurchasedPH
SELECT DISTINCT
        selltocustomerid ,
        1 AS [Hydration Flag]
FROM    dbo.FactSales
WHERE   itemcode IN ( SELECT DISTINCT
                                Itemcode
                      FROM      dbo.DimItemCode
                      WHERE     [MKTG Brand] = 'OPTAVIA-PH' )