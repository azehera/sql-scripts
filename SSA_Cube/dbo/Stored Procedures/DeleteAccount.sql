﻿/*
 * Delete a row in [dbo].[Account]
 * 
 * Uses [AccountId] as the identifying key
 */
CREATE PROCEDURE [dbo].[DeleteAccount]
  @AccountId uniqueidentifier
AS
SET NOCOUNT ON;
DELETE 
  FROM [dbo].[Account]
 WHERE [AccountId] = @AccountId;
