﻿CREATE PROCEDURE [dbo].[ListSolutionsAndItems]
  @AccountId [uniqueidentifier]
AS 
SELECT [SolutionId], [AccountId], [Name], [DocumentMap], [DateCreated], [DateLastUpdated], [EndpointAliases]
  FROM [dbo].[Solution]
 WHERE [AccountId] = @AccountId

SELECT [si].[SolutionItemId], [si].[SolutionId], [si].[Name], [si].[ProviderUniqueId], [si].[ProviderCreationParameters], [si].[DateCreated], [si].[IsManualScanOnly]
  FROM [dbo].[SolutionItem] [si] INNER JOIN 
       [dbo].[Solution] [s] ON [si].[SolutionId] = [s].[SolutionId]
 WHERE [AccountId] = @AccountId

