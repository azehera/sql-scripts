﻿CREATE PROCEDURE [dbo].[GetSolutionTreeStates]
  @SolutionId [uniqueidentifier],
  @EffectiveDate [datetime]
AS 
SELECT [CarbonTreeId], [Content], [EffectiveDate], [ct].[SolutionItemId], [ct].[Checksum], [ct].[DateCreated], [ct].[Version]
  FROM [dbo].[CarbonTree] [ct] INNER JOIN 
       [dbo].[SolutionItem] [si] 
    ON [ct].[SolutionItemId] = [si].[SolutionItemId]
 WHERE [si].[SolutionId] = @SolutionId
   AND [ct].[DateCreated] <= @EffectiveDate;

WITH 
SolutionItemsRequiringPatch (CarbonTreeId)
  AS (SELECT [CarbonTreeId]
        FROM [dbo].[CarbonTree] [ct] INNER JOIN 
             [dbo].[SolutionItem] [si]
          ON [ct].[SolutionItemId] = [si].[SolutionItemId] 
       WHERE [si].[SolutionId] = @SolutionId
         AND [ct].[EffectiveDate] > @EffectiveDate
         AND [ct].[DateCreated] <= @EffectiveDate),
MaximumAvailableReversalVersions (CarbonTreeId, VersionNumber)
  AS (SELECT [ctsc].[CarbonTreeId], MAX([ctsc].[Version]) + 1
        FROM [dbo].[CarbonTreeStateChain] [ctsc] INNER JOIN 
             [SolutionItemsRequiringPatch] [sirp] 
          ON [sirp].[CarbonTreeId] = [ctsc].[CarbonTreeId]
       GROUP BY [ctsc].[CarbonTreeId]),
MinimumNewerReversalVersions (CarbonTreeId, VersionNumber)
  AS (SELECT [ctsc].[CarbonTreeId], MIN([ctsc].[Version])
        FROM [dbo].[CarbonTreeStateChain] [ctsc] INNER JOIN 
             [SolutionItemsRequiringPatch] [sirp] 
          ON [sirp].[CarbonTreeId] = [ctsc].[CarbonTreeId]
       WHERE [ctsc].[EffectiveDate] > @EffectiveDate
       GROUP BY [ctsc].[CarbonTreeId]),
EffectiveReversalVersions (CarbonTreeId, VersionNumber)
  AS (SELECT [marv].[CarbonTreeId], ISNULL([mnrv].[VersionNumber], [marv].[VersionNumber]) - 1
        FROM [MaximumAvailableReversalVersions] [marv] LEFT OUTER JOIN 
             [MinimumNewerReversalVersions] [mnrv] 
          ON [mnrv].[CarbonTreeId] = [marv].[CarbonTreeId])
SELECT [ReversalPatchContent], [ctsc].[Version], [ctsc].[CarbonTreeId], [ctsc].[EffectiveDate], [ctsc].[DateCreated]
  FROM [dbo].[CarbonTreeStateChain] [ctsc] INNER JOIN 
       [EffectiveReversalVersions] [erv] 
    ON [erv].[CarbonTreeId] = [ctsc].[CarbonTreeId]
 WHERE [ctsc].[Version] >= [erv].[VersionNumber]





