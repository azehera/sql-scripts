﻿

Create PROCEDURE [dbo].[USP_Load_PurchasedPreviousMonthTable] 

-- ============================================= 
-- Author:		Micah W. 
-- Create date: 07-06-2019 
-- Description:	Created to Truncate and load table to see which customers made purchases in current month
-- ============================================= 
 
AS

TRUNCATE TABLE BI_SSAS_Cubes.dbo.StagePurchasedPreviousMonth

INSERT INTO BI_SSAS_Cubes.dbo.StagePurchasedPreviousMonth
SELECT DISTINCT(SelltoCustomerID), '1'
FROM BI_SSAS_Cubes.dbo.FactSales
WHERE EOMONTH([Posting Date]) = EOMONTH(DATEADD(month, datediff(month, 0, getdate())-1, 0)) --previousMonth
AND Amount >=0

