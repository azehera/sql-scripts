﻿
CREATE PROCEDURE [dbo].[UpsertDataDictionaryEntry]
  @UniqueId uniqueidentifier,
  @DataDictionaryCategoryUniqueId uniqueidentifier,
  @ScopeType int,
  @ScopeTypeItemId uniqueidentifier,
  @ProviderCompatibilityGroupId uniqueidentifier,
  @Value nvarchar (MAX),
  @IsGlobal bit,
  @DateCreated datetime,
  @Paths [dbo].[StringTable] READONLY,
  @UserName [nvarchar](128) = NULL
AS
SET NOCOUNT ON;
IF @UserName IS NOT NULL
BEGIN
  EXEC [dbo].[SetDataDictionaryAuditUserName] @UserName
END
MERGE [dbo].[DataDictionaryEntries] AS Trg
USING (SELECT @UniqueId, @DataDictionaryCategoryUniqueId, @ScopeType, @ScopeTypeItemId, @ProviderCompatibilityGroupId, @Value, @IsGlobal, @DateCreated)
AS Src ([UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], [DateCreated])
ON ([Trg].[UniqueId] = [Src].[UniqueId])
WHEN MATCHED THEN
 UPDATE SET [DataDictionaryCategoryUniqueId] = [Src].[DataDictionaryCategoryUniqueId],
            [ScopeType] = [Src].[ScopeType],
            [ScopeTypeItemId] = [Src].[ScopeTypeItemId],
            [ProviderCompatibilityGroupId] = [Src].[ProviderCompatibilityGroupId],
            [Value] = [Src].[Value],
            [IsGlobal] = [Src].[IsGlobal]
WHEN NOT MATCHED BY TARGET THEN
 INSERT ([UniqueId], [DataDictionaryCategoryUniqueId], [ScopeType], [ScopeTypeItemId], [ProviderCompatibilityGroupId], [Value], [IsGlobal], [DateCreated])
 VALUES ([Src].[UniqueId], [Src].[DataDictionaryCategoryUniqueId], [Src].[ScopeType], [Src].[ScopeTypeItemId], [Src].[ProviderCompatibilityGroupId], [Src].[Value], [Src].[IsGlobal], [Src].[DateCreated]);

MERGE [dbo].[DataDictionaryEntryPaths] AS Trg
USING (SELECT @UniqueId, [Value] FROM @Paths)
AS Src ([DataDictionaryEntryUniqueId], [Path])
ON ([Trg].[DataDictionaryEntryUniqueId] = [Src].[DataDictionaryEntryUniqueId] AND [Trg].[Path] = [Src].[Path] COLLATE Latin1_General_100_CS_AS)
WHEN NOT MATCHED BY TARGET THEN
 INSERT ([DataDictionaryEntryUniqueId], [Path])
 VALUES ([Src].[DataDictionaryEntryUniqueId], [Src].[Path])
WHEN NOT MATCHED BY SOURCE AND [Trg].[DataDictionaryEntryUniqueId] = @UniqueId THEN DELETE; 




