﻿CREATE PROCEDURE [dbo].[UpdateSolutionTreeState]
  @Content [varbinary] (MAX),
  @Reversal [varbinary] (MAX),
  @Checksum [bigint],
  @SolutionItemId [uniqueidentifier]
AS 
IF @Reversal IS NOT NULL
BEGIN
  INSERT INTO [dbo].[CarbonTreeStateChain] ([ReversalPatchContent], [Version], [EffectiveDate], [CarbonTreeId])
  SELECT @Reversal, [Version], [EffectiveDate], [CarbonTreeId] FROM [dbo].[CarbonTree] WHERE [SolutionItemId] = @SolutionItemId
END

MERGE [dbo].[CarbonTree] 
   AS [Trg]
USING (SELECT @Content, @Checksum, @SolutionItemId, GETUTCDATE()) 
   AS [Src] ([Content], [Checksum], [SolutionItemId], [EffectiveDate])
   ON [Trg].[SolutionItemId] = [Src].[SolutionItemId]
 WHEN MATCHED 
 THEN UPDATE SET [Trg].[Version] = [Trg].[Version] + 1, 
                 [Trg].[EffectiveDate] = [Src].[EffectiveDate],
                 [Trg].[Checksum] = [Src].[Checksum],
                 [Trg].[Content] = [Src].[Content]
 WHEN NOT MATCHED BY TARGET
 THEN INSERT ([CarbonTreeId], [Content], [Checksum], [EffectiveDate], [SolutionItemId], [Version])
      VALUES (NEWID(), [Src].[Content], [Src].[Checksum], [Src].[EffectiveDate], [Src].[SolutionItemId], 1);


