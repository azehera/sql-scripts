﻿CREATE PROCEDURE [dbo].[PurgeSolutionItemTreeStates]
  @SolutionItemId [uniqueidentifier],
  @EffectiveDate [datetime]
AS 

DECLARE @MostRecentDependencySet [datetime]
SET @MostRecentDependencySet = 
    (SELECT MAX([ctds].[EffectiveDate]) 
       FROM [dbo].[CarbonTreeDependencySets] [ctds] INNER JOIN 
            [dbo].[CarbonTree] [ct] 
         ON [ctds].[CarbonTreeId] = [ct].[CarbonTreeId]
      WHERE [ct].[SolutionItemId] = @SolutionItemId)

DELETE [dbo].[CarbonTreeStateChain]
  FROM [dbo].[CarbonTreeStateChain] [ctsc] INNER JOIN 
       [dbo].[CarbonTree] [ct] 
    ON [ctsc].[CarbonTreeId] = [ct].[CarbonTreeId]
 WHERE [ct].[SolutionItemId] = @SolutionItemId AND
       [ctsc].[EffectiveDate] < @EffectiveDate
   
DELETE [dbo].[CarbonTreeDependencySets]
  FROM [dbo].[CarbonTreeDependencySets] [ctds] INNER JOIN 
       [dbo].[CarbonTreeStateChain] [ctsc]
    ON [ctsc].[CarbonTreeId] = [ctds].[CarbonTreeId] INNER JOIN 
       [dbo].[CarbonTree] [ct] 
    ON [ctsc].[CarbonTreeId] = [ct].[CarbonTreeId]
 WHERE [ct].[SolutionItemId] = @SolutionItemId AND
       [ctds].[EffectiveDate] < @EffectiveDate AND
       [ctds].[EffectiveDate] < @MostRecentDependencySet
