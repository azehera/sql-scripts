﻿/*
 * Insert or Update a row in [dbo].[Account]
 * 
 * Uses [AccountId] as the identifying key
 */
CREATE PROCEDURE [dbo].[UpsertAccount]
  @AccountId uniqueidentifier,
  @Name nvarchar (255)
AS
SET NOCOUNT ON;
MERGE [dbo].[Account] AS Trg
USING (SELECT @AccountId, @Name)
AS Src ([AccountId], [Name])
ON ([Trg].[AccountId] = [Src].[AccountId])
WHEN MATCHED THEN
 UPDATE SET [Name] = [Src].[Name]            
WHEN NOT MATCHED BY TARGET THEN
 INSERT ([AccountId], [Name])
 VALUES ([Src].[AccountId], [Src].[Name]);

