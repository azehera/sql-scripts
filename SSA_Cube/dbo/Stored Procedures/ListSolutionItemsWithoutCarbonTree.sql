﻿CREATE PROCEDURE [dbo].[ListSolutionItemsWithoutCarbonTree]
  @SolutionId [uniqueidentifier]
AS 
SELECT [si].[SolutionItemId]
  FROM [dbo].[SolutionItem] [si] LEFT OUTER JOIN 
       [dbo].[CarbonTree] [ct] 
    ON [ct].[SolutionItemId] = [si].[SolutionItemId]
 WHERE [si].[SolutionId] = @SolutionId
   AND [ct].[CarbonTreeId] IS NULL
