﻿CREATE PROCEDURE [dbo].[ListDocumentControlFlagSets]
  @ForUniqueId uniqueidentifier
AS
SET NOCOUNT ON;
SELECT [ForUniqueId], [Key], [Name], [Flags], [DateCreated] 
  FROM [dbo].[DocumentControlFlagSets]
 WHERE [ForUniqueId] = @ForUniqueId
