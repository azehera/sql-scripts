﻿

Create View [dbo].[V_SalesOrder]
      ([Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType] 
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
      ,[Coupon_Promotion Code]
	  ,[Draft Year]
	  ,DraftMonth
	  ,DraftQuarter
	  ,LifeMonth
	  ,FirstOrderDate
	  ) as
select [Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,FM.[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,FM.[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType] 
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,FM.[GINID]
      ,[OrderType]
      ,DM.[Coupon_Promotion Code]
	  ,DM.[Draft Year]
	  ,DM.DraftMonth
	  ,DM.DraftQuarter
	  ,DM.LifeMonth
	  ,DM.FirstOrderDate
 from dbo.FactSales_MDM FM
inner join dbo.dimsalesorder_MDM DM
on FM.DocumentNo= DM.DocumentNo



