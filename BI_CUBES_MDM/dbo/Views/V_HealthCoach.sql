﻿


CREATE View [dbo].[V_HealthCoach]
      ([Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,SalesID
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
      ,CustomerName
	  ) as
select [Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType] 
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
      ,HC.CustomerName
 from dbo.FactSales_MDM FM
inner join [dbo].DimHealthCoach HC
on FM.SelltoCustomerID = HC.CustomerNumber




