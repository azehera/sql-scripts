﻿


CREATE View [dbo].[V_TaxGroupCode]
      ([Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType]
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
     ) as
select [Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,FM.[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType]
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
        from dbo.FactSales_MDM FM
inner join dbo.[Tax Group Code] TG
on FM.[Tax Group Code] = TG.[Tax Group Code]




