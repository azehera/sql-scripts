﻿


CREATE View [dbo].[V_ItemCode]
      ([Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,SalesID
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
      ,Description
	  ,[Product Group Code]
	  ,Discontinued) as
select [Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,FM.[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType] 
      ,FM.[LocationID]
      ,FM.[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
      ,DI.Description
	  ,DI.[Product Group Code]
	  ,DI.Discontinued
	   from dbo.FactSales_MDM FM
inner join [dbo].DimItemCode DI
on FM.ItemCode = DI.Itemcode




