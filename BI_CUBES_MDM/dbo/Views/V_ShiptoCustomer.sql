﻿

CREATE View [dbo].[V_ShiptoCustomer]
      ([Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,[SelltoCustomerID]
      ,[SelltoCustomerKey]
      ,[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType] 
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
      ,[Ship-to Customer Name]
	  ,[Ship-to City]
	  ,Zip5
	  ,Zip3
	  ,Zip2
	  ,[Payment Discount %]
	  ,[Currency Code]
	  ,[Currency Factor]
	  ,[Ship-to County]
	  ,Email
	  ,[DMA Code]
	  ,[DMA Name]
	  ) as
select FM.[Country]
      ,[CustomerPostingGroup]
      ,[SalesChannel]
      ,[ItemCategoryCode]
      ,[ItemCategoryCodeSubcategory]
      ,FM.[SelltoCustomerID]
      ,FM.[SelltoCustomerKey]
      ,FM.[ShiptoCustomerKey]
      ,[ItemCode]
      ,[ItemCodeSubcategory]
      ,[DocumentNo]
      ,[LineDiscount]
      ,[InvoiceDiscount]
      ,[Units]
      ,[SubCategoryUnits]
      ,[UnitsPerBox]
      ,[BOX]
      ,[Amount]
      ,[Gross Amount]
      ,[Amount Including VAT]
      ,[TaxAmount]
      ,[Tax Group Code]
      ,[Unit Cost (LCY)]
      ,[Quantity]
      ,[SalesType] 
      ,[LocationID]
      ,[RegionID]
      ,[Posting Date]
      ,[GINID]
      ,[OrderType]
      ,DS.[Ship-to Customer Name]
	  ,DS.[Ship-to City]
	  ,DS.Zip5
	  ,DS.Zip3
	  ,DS.Zip2
	  ,DS.[Payment Discount %]
	  ,DS.[Currency Code]
	  ,DS.[Currency Factor]
	  ,DS.[Ship-to County]
	  ,DS.Email
	  ,DS.[DMA Code]
	  ,DS.[DMA Name]
 from dbo.FactSales_MDM FM
inner join dbo.DimShiptoCustomerKey DS
on FM.SelltoCustomerKey = DS.ShiptoCustomerKey



