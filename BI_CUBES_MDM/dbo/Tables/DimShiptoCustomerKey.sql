﻿CREATE TABLE [dbo].[DimShiptoCustomerKey] (
    [ShiptoCustomerKey]     NVARCHAR (150)   NULL,
    [SelltoCustomerID]      NVARCHAR (60)    NULL,
    [Ship-to Customer Name] NVARCHAR (110)   NULL,
    [Ship-to City]          NVARCHAR (50)    NULL,
    [Zip5]                  NVARCHAR (20)    NULL,
    [Zip3]                  NVARCHAR (20)    NULL,
    [Zip2]                  NVARCHAR (20)    NULL,
    [Country]               NVARCHAR (10)    NULL,
    [Payment Discount %]    DECIMAL (38, 20) NULL,
    [Currency Code]         NVARCHAR (10)    NULL,
    [Currency Factor]       DECIMAL (38, 20) NULL,
    [Ship-to County]        NVARCHAR (30)    NULL,
    [Email]                 NVARCHAR (80)    NULL,
    [DMA Code]              VARCHAR (50)     NULL,
    [DMA Name]              VARCHAR (50)     NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_2]
    ON [dbo].[DimShiptoCustomerKey]([ShiptoCustomerKey] ASC)
    INCLUDE([Country], [Ship-to County]) WITH (FILLFACTOR = 100);

