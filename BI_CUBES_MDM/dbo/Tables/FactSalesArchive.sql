﻿CREATE TABLE [dbo].[FactSalesArchive] (
    [Country]                     VARCHAR (5)      NULL,
    [CustomerPostingGroup]        NVARCHAR (10)    NULL,
    [SalesChannel]                NVARCHAR (20)    NOT NULL,
    [ItemCategoryCode]            NVARCHAR (20)    NOT NULL,
    [ItemCategoryCodeSubcategory] NVARCHAR (50)    NULL,
    [SelltoCustomerID]            NVARCHAR (20)    NULL,
    [SelltoCustomerKey]           NVARCHAR (60)    NULL,
    [ShiptoCustomerKey]           NVARCHAR (60)    NULL,
    [ItemCode]                    NVARCHAR (50)    NULL,
    [ItemCodeSubcategory]         NVARCHAR (50)    NULL,
    [DocumentNo]                  NVARCHAR (20)    NULL,
    [LineDiscount]                DECIMAL (38, 20) NULL,
    [InvoiceDiscount]             DECIMAL (38, 20) NULL,
    [Units]                       DECIMAL (38, 20) NULL,
    [SubCategoryUnits]            DECIMAL (38, 20) NULL,
    [UnitsPerBox]                 DECIMAL (38, 20) NULL,
    [BOX]                         DECIMAL (38, 20) NULL,
    [Amount]                      DECIMAL (38, 20) NULL,
    [Gross Amount]                DECIMAL (38, 20) NULL,
    [Amount Including VAT]        DECIMAL (38, 20) NULL,
    [TaxAmount]                   DECIMAL (38, 20) NULL,
    [Tax Group Code]              NVARCHAR (10)    NULL,
    [Unit Cost (LCY)]             DECIMAL (38, 20) NULL,
    [Quantity]                    DECIMAL (38, 20) NULL,
    [SalesType]                   INT              NULL,
    [LocationID]                  INT              NULL,
    [RegionID]                    NVARCHAR (20)    NULL,
    [Posting Date]                DATETIME         NOT NULL,
    [GINID]                       INT              NULL,
    [OrderType]                   VARCHAR (20)     CONSTRAINT [DF_OrderType_Archive] DEFAULT ('ONDEMAND') NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_FactSalesArchive_idx1]
    ON [dbo].[FactSalesArchive]([DocumentNo] ASC, [Posting Date] ASC, [GINID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FactSalesArchive_OrderType]
    ON [dbo].[FactSalesArchive]([OrderType] ASC)
    INCLUDE([DocumentNo], [Posting Date], [GINID]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FactSalesArchive_CustomerPostingGroup_Posting Date]
    ON [dbo].[FactSalesArchive]([CustomerPostingGroup] ASC, [Posting Date] ASC)
    INCLUDE([SelltoCustomerID], [DocumentNo]);


GO
CREATE NONCLUSTERED INDEX [IX_FactSalesArchive_CustomerPostingGroup_SelltoCustomerID_Posting Date]
    ON [dbo].[FactSalesArchive]([CustomerPostingGroup] ASC, [SelltoCustomerID] ASC, [Posting Date] ASC)
    INCLUDE([DocumentNo]) WITH (FILLFACTOR = 90);

