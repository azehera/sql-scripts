﻿CREATE TABLE [dbo].[DimHealthCoach] (
    [CustomerNumber]          NVARCHAR (20)  NOT NULL,
    [CustomerName]            NVARCHAR (100) NULL,
    [CustomerType]            NVARCHAR (30)  NULL,
    [CustomerStatus]          NVARCHAR (30)  NULL,
    [CustomerRank]            NVARCHAR (30)  NULL,
    [CustomerHighestRank]     NVARCHAR (30)  NULL,
    [CustomerSponsorNumber]   NVARCHAR (20)  NULL,
    [CustomerPresidentNumber] NVARCHAR (20)  NULL,
    [CustomerGlobalNumber]    NVARCHAR (20)  NULL,
    [CustomerCubeStatus]      NVARCHAR (20)  NULL,
    CONSTRAINT [PK_DimHealthCoach] PRIMARY KEY CLUSTERED ([CustomerNumber] ASC) WITH (FILLFACTOR = 95)
);

