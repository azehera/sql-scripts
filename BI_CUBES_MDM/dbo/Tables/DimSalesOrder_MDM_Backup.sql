﻿CREATE TABLE [dbo].[DimSalesOrder_MDM_Backup] (
    [SelltoCustomerID]      NVARCHAR (20)    NULL,
    [DocumentNo]            NVARCHAR (20)    NOT NULL,
    [Coupon_Promotion Code] NVARCHAR (10)    NULL,
    [GINID]                 VARCHAR (20)     CONSTRAINT [DF_DimSalesOrder_MDM_Backup_GINID] DEFAULT ((0)) NULL,
    [Draft Year]            INT              NULL,
    [FirstOrderDate]        DATE             NULL,
    [DraftQuarter]          VARCHAR (2)      NULL,
    [DraftMonth]            INT              NULL,
    [LifeMonth]             INT              NULL,
    [LifeOrder]             INT              NULL,
    [TotalOrderAmount]      DECIMAL (38, 20) NULL,
    [Life Order Interval]   INT              DEFAULT ((0)) NULL,
    [DocumentNo2]           NVARCHAR (20)    NULL
);

