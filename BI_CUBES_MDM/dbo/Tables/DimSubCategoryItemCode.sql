﻿CREATE TABLE [dbo].[DimSubCategoryItemCode] (
    [Subcategory Itemcode]           NVARCHAR (255) NOT NULL,
    [Subcategory Description]        NVARCHAR (255) NULL,
    [Subcategory Product Group Code] NVARCHAR (100) NULL,
    [Subcategory Item Category Code] NVARCHAR (100) NULL,
    [Discontinued]                   NVARCHAR (5)   NULL,
    [SubCategory MKTG Type]          NVARCHAR (20)  NULL,
    [SubCategory MKTG Category]      NVARCHAR (20)  NULL,
    [SubCategory MKTG Subcategory]   NVARCHAR (20)  NULL,
    [SubCategory Launch Date]        DATETIME       NULL,
    [SubCategory MKTG Status]        NVARCHAR (50)  NULL,
    [SubCategory MKTG Brand]         NVARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_DimSubCategoryItemCode_Subcategory Itemcode]
    ON [dbo].[DimSubCategoryItemCode]([Subcategory Itemcode] ASC)
    INCLUDE([Subcategory Description]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DimSubCategoryItemCode_Subcategory Itemcode_idx1]
    ON [dbo].[DimSubCategoryItemCode]([Subcategory Itemcode] ASC)
    INCLUDE([Subcategory Description]) WITH (FILLFACTOR = 90);

