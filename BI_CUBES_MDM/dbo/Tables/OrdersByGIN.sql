﻿CREATE TABLE [dbo].[OrdersByGIN] (
    [GroupNo]                 INT           NULL,
    [Lifetime Order Count]    INT           NULL,
    [NoOfLifeOrder]           INT           NULL,
    [FO-Type]                 VARCHAR (20)  NULL,
    [FO-CustomerPostingGroup] NVARCHAR (20) NULL
);

