﻿CREATE TABLE [dbo].[ReturnExchangeOrder] (
    [Document No for Return Order]   VARCHAR (20)     NULL,
    [SelltoCustomerID]               VARCHAR (20)     NULL,
    [Return Order No]                VARCHAR (20)     NULL,
    [Document No for Original Order] VARCHAR (20)     NULL,
    [Original Order No Ucart]        VARCHAR (20)     NULL,
    [Document Date]                  DATETIME         NULL,
    [OriginalOrderAmount]            DECIMAL (38, 20) NULL,
    [ReturnOrderAmount]              DECIMAL (38, 20) NULL,
    [OrderType]                      VARCHAR (20)     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ReturnExchangeOrder_Document Date]
    ON [dbo].[ReturnExchangeOrder]([Document Date] ASC);

