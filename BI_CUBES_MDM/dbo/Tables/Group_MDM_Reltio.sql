﻿CREATE TABLE [dbo].[Group_MDM_Reltio] (
    [GINID]      BIGINT         NULL,
    [CustomerId] NVARCHAR (256) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CustId_GINID]
    ON [dbo].[Group_MDM_Reltio]([CustomerId] ASC)
    INCLUDE([GINID]) WITH (FILLFACTOR = 90);

