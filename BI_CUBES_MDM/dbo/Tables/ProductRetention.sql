﻿CREATE TABLE [dbo].[ProductRetention] (
    [GINID]                          BIGINT         NULL,
    [ItemCode]                       NVARCHAR (255) NULL,
    [DocumentNo]                     NVARCHAR (255) NULL,
    [Posting Date]                   DATETIME       NULL,
    [# of Times Product Ordered_GIN] INT            NULL
);

