﻿CREATE TABLE [dbo].[MKTGBrandRetention] (
    [GINID]                             BIGINT         NULL,
    [MKTG Brand]                        NVARCHAR (255) NULL,
    [DocumentNo]                        NVARCHAR (255) NULL,
    [Posting Date]                      DATETIME       NULL,
    [# of Times MKTD BRAND Ordered_GIN] INT            NULL
);

