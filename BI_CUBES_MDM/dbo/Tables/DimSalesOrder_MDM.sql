﻿CREATE TABLE [dbo].[DimSalesOrder_MDM] (
    [SelltoCustomerID]      NVARCHAR (20)    NULL,
    [DocumentNo]            NVARCHAR (20)    NOT NULL,
    [Coupon_Promotion Code] NVARCHAR (10)    NULL,
    [GINID]                 VARCHAR (20)     CONSTRAINT [DF_DimSalesOrder_MDM_GINID] DEFAULT ((0)) NULL,
    [Draft Year]            INT              NULL,
    [FirstOrderDate]        DATE             NULL,
    [DraftQuarter]          VARCHAR (2)      NULL,
    [DraftMonth]            INT              NULL,
    [LifeMonth]             INT              NULL,
    [LifeOrder]             INT              NULL,
    [TotalOrderAmount]      DECIMAL (38, 20) NULL,
    [Life Order Interval]   INT              DEFAULT ((0)) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_DimSalesOrder_MDM_LifeMonth]
    ON [dbo].[DimSalesOrder_MDM]([LifeMonth] ASC)
    INCLUDE([SelltoCustomerID], [DocumentNo]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_DimSalesOrder_MDM_FirstOrderDate]
    ON [dbo].[DimSalesOrder_MDM]([FirstOrderDate] ASC)
    INCLUDE([GINID]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_DimSalesOrder_MDM_Draft Year_DraftMonth_LifeMonth]
    ON [dbo].[DimSalesOrder_MDM]([Draft Year] ASC, [DraftMonth] ASC, [LifeMonth] ASC)
    INCLUDE([SelltoCustomerID], [DocumentNo]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_DraftYear_GINID]
    ON [dbo].[DimSalesOrder_MDM]([Draft Year] ASC)
    INCLUDE([GINID]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_GINID]
    ON [dbo].[DimSalesOrder_MDM]([GINID] ASC, [Draft Year] ASC, [FirstOrderDate] ASC, [DraftQuarter] ASC) WITH (FILLFACTOR = 95);


GO
CREATE NONCLUSTERED INDEX [IX_Document_No]
    ON [dbo].[DimSalesOrder_MDM]([DocumentNo] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_DimSalesOrder_MDM_idx1]
    ON [dbo].[DimSalesOrder_MDM]([LifeMonth] ASC)
    INCLUDE([SelltoCustomerID], [DocumentNo], [Coupon_Promotion Code], [GINID], [Draft Year], [FirstOrderDate], [DraftQuarter], [DraftMonth]) WITH (FILLFACTOR = 90);

