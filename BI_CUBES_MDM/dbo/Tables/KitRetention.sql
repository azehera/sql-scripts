﻿CREATE TABLE [dbo].[KitRetention] (
    [GINID]                      BIGINT         NULL,
    [ItemCodeSubcategory]        NVARCHAR (255) NULL,
    [DocumentNo]                 NVARCHAR (255) NULL,
    [Posting Date]               DATETIME       NULL,
    [# of Times Kit Ordered_GIN] INT            NULL
);

