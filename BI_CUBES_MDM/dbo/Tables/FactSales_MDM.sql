﻿CREATE TABLE [dbo].[FactSales_MDM] (
    [Country]                     VARCHAR (5)      NULL,
    [CustomerPostingGroup]        NVARCHAR (10)    NULL,
    [SalesChannel]                NVARCHAR (20)    NOT NULL,
    [ItemCategoryCode]            NVARCHAR (20)    NOT NULL,
    [ItemCategoryCodeSubcategory] NVARCHAR (50)    NULL,
    [SelltoCustomerID]            NVARCHAR (20)    NULL,
    [SelltoCustomerKey]           NVARCHAR (60)    NULL,
    [ShiptoCustomerKey]           NVARCHAR (60)    NULL,
    [ItemCode]                    NVARCHAR (255)   NULL,
    [ItemCodeSubcategory]         NVARCHAR (255)   NULL,
    [DocumentNo]                  NVARCHAR (20)    NULL,
    [LineDiscount]                DECIMAL (38, 20) NULL,
    [InvoiceDiscount]             DECIMAL (38, 20) NULL,
    [Units]                       DECIMAL (38, 20) NULL,
    [SubCategoryUnits]            DECIMAL (38, 20) NULL,
    [UnitsPerBox]                 DECIMAL (38, 20) NULL,
    [BOX]                         DECIMAL (38, 20) NULL,
    [Amount]                      DECIMAL (38, 20) NULL,
    [Gross Amount]                DECIMAL (38, 20) NULL,
    [Amount Including VAT]        DECIMAL (38, 20) NULL,
    [TaxAmount]                   DECIMAL (38, 20) NULL,
    [Tax Group Code]              NVARCHAR (10)    NULL,
    [Unit Cost (LCY)]             DECIMAL (38, 20) NULL,
    [Quantity]                    DECIMAL (38, 20) NULL,
    [SalesType]                   INT              NULL,
    [LocationID]                  INT              NULL,
    [RegionID]                    NVARCHAR (20)    NULL,
    [Posting Date]                DATETIME         NOT NULL,
    [GINID]                       INT              CONSTRAINT [DF_FactSales_MDM_GINID] DEFAULT ((0)) NULL,
    [OrderType]                   VARCHAR (20)     CONSTRAINT [DF_OrderType_MDM] DEFAULT ('ONDEMAND') NULL,
    [PaidAsRank]                  VARCHAR (128)    DEFAULT ('N/A') NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_FactSales_MDM_idx1]
    ON [dbo].[FactSales_MDM]([CustomerPostingGroup] ASC, [OrderType] ASC)
    INCLUDE([Amount], [Posting Date]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FactSales_MDM_idx2]
    ON [dbo].[FactSales_MDM]([DocumentNo] ASC)
    INCLUDE([OrderType]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FactSales_MDM_idx3]
    ON [dbo].[FactSales_MDM]([ItemCodeSubcategory] ASC)
    INCLUDE([ItemCode], [DocumentNo], [Amount Including VAT], [Quantity], [OrderType]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FactSales_MDM_idx4]
    ON [dbo].[FactSales_MDM]([Posting Date] ASC)
    INCLUDE([SelltoCustomerID], [DocumentNo], [GINID], [OrderType]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_FactSales_MDM_PostingDate]
    ON [dbo].[FactSales_MDM]([Posting Date] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_GINID]
    ON [dbo].[FactSales_MDM]([GINID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_SellToCustomer]
    ON [dbo].[FactSales_MDM]([SelltoCustomerID] ASC) WITH (FILLFACTOR = 90);

