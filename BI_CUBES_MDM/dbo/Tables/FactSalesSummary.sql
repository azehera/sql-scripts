﻿CREATE TABLE [dbo].[FactSalesSummary] (
    [SelltoCustomerID]     NVARCHAR (20) NULL,
    [GINID]                INT           NULL,
    [DocumentNo]           NVARCHAR (20) NULL,
    [Posting Date]         DATETIME      NOT NULL,
    [CustomerPostingGroup] NVARCHAR (10) NULL,
    [OrderType]            VARCHAR (20)  NULL,
    [SourceTable]          VARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_FactSalesSummary_Posting Date]
    ON [dbo].[FactSalesSummary]([Posting Date] ASC)
    INCLUDE([SelltoCustomerID], [GINID], [DocumentNo], [CustomerPostingGroup], [OrderType]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_FactSalesSummary_GINID_Posting Date]
    ON [dbo].[FactSalesSummary]([GINID] ASC, [Posting Date] ASC)
    INCLUDE([SelltoCustomerID]) WITH (FILLFACTOR = 100);

