﻿

CREATE Procedure [dbo].[Update_DimSalesOrder_LifeMonth_GINFix]
As

--Update LifeMonth

-------Finding Life Month-------
IF object_id('Tempdb..#LifeMonth') Is Not Null DROP TABLE #LifeMonth
CREATE TABLE #LifeMonth (SelltoCustomerID varchar(20), 
						 DocumentNo varchar(20), 
                         FirstOrderDate date, 
						 PostingDate date, 
						 LifeMonth int)

INSERT INTO #LifeMonth
SELECT distinct d.SelltoCustomerID, 
                d.DocumentNo, 
				d.FirstOrderDate, 
				f.[Posting Date], 
				DATEDIFF(MM, d.FirstOrderDate, f.[Posting Date]) as LifeMonth
FROM dbo.DimSalesOrder_MDM_Backup d INNER JOIN dbo.FactSalesSummary f
ON d.SelltoCustomerID = f.SelltoCustomerID and 
Case when d.DocumentNo  like 'ARCH%' Then SUBSTRING (d.DocumentNo,5,25) ELSE d.DocumentNo END = f.DocumentNo 
WHERE d.LifeMonth IS NULL
 

 
---- Populate LifeMonth -------------- 
UPDATE d 
SET d.LifeMonth = l.LifeMonth 
FROM dbo.DimSalesOrder_MDM_Backup d 
INNER JOIN #LifeMonth l 
ON d.SelltoCustomerID = l.SelltoCustomerID and d.DocumentNo = l.DocumentNo 
--OPTION (MAXDOP 1)

 

UPDATE d 
SET d.LifeMonth = l.LifeMonth 
FROM dbo.DimSalesOrder_MDM_Backup d 
INNER JOIN #LifeMonth l 
ON d.DocumentNo = l.DocumentNo 
where d.LifeMonth = 0  
 

UPDATE DimSalesOrder_MDM_Backup SET LifeMonth = 0
where LifeMonth IS NULL

 