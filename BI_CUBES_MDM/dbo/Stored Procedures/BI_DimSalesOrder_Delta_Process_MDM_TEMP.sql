﻿
/******************************************************************************************
OBJECT NAME:      [dbo].[BI_DimSalesOrder_Delta_Process_MDM]
DEVELOPER:  Ronak Shah  
DATE:  03/29/2019           
DESCRIPTION: Delta process for Dimsales Order MDM     
PARAMETERS/VARRIABLES:
            This process will only delete last 45 days of data and reload it. 
DATE: 04/01/2020
Change in totalOrderAmount. Round it to 4 decimal place.

Date : 05/04/2020
Note: Modify stored procedure to ignore customer ids with null Values
*******************************************************************************************/


 CREATE PROCEDURE [dbo].[BI_DimSalesOrder_Delta_Process_MDM_TEMP]
 AS

 /* Delete From FactSales_MDM from BICUBES*/

DELETE FROM dbo.DimSalesOrder_MDM  
WHERE  EXISTS (
select SelltoCustomerID,DocumentNo from dbo.FactSalesSummary F1
where SourceTable ='Factsales_MDM' and [Posting Date] >= DateAdd(dd, DateDiff(dd,0,GetDate()-184), 0)
AND F1.SelltoCustomerID = DimSalesOrder_MDM.SelltoCustomerID
AND F1.DocumentNo = DimSalesOrder_MDM.DocumentNo
)

 
 

/* Insert Last 110 days of data to DimSalesOrder_MDM*/
INSERT dbo.DimSalesOrder_MDM
(
    SelltoCustomerID,
    DocumentNo,
    [Coupon_Promotion Code],
	[GINID],
	TotalOrderAmount
)  
SELECT A.SelltoCustomerID,A.DocumentNo,ISNULL(B.[Coupon_Promotion Code],'') AS [Coupon_Promotion Code],A.GINID,ROUND(AMOUNT,4)  as TotalOrderAmount FROM 
(
SELECT 
SelltoCustomerID,DocumentNo,F1.GINID,SUM(AMOUNT)AMOUNT FROM dbo.FactSales_MDM F1
WHERE [Posting Date] >=  DateAdd(dd, DateDiff(dd,0,GetDate()-184), 0) 
AND  SelltoCustomerID IS NOT NULL
Group by SelltoCustomerID,DocumentNo,F1.GINID 
)A
LEFT OUTER JOIN
(
SELECT Distinct [No_]
      ,[Sell-to Customer No_]
      ,[Coupon_Promotion Code]
  FROM [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header]
  where [Posting Date] >=  DateAdd(dd, DateDiff(dd,0,GetDate()-184), 0)
)B
ON A.SelltoCustomerID COLLATE DATABASE_DEFAULT= B.[Sell-to Customer No_] AND A.DocumentNo COLLATE DATABASE_DEFAULT= B.[No_]
