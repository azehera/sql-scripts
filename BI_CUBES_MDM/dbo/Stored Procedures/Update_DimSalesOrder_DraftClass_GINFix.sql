﻿

CREATE Procedure [dbo].[Update_DimSalesOrder_DraftClass_GINFix]
As

--Update GINID & Draft Class
---Update GINID 
Update DM
set GINID = FS.GINID
from dbo.DimSalesOrder_MDM_Backup DM
Inner Join dbo.FactSalesSummary FS
on DM.SelltoCustomerID = FS.SelltoCustomerID 
Where DM.GINID =0
 
---Update Draft Class

TRUNCATE TABLE WorkSalesOrder
 
INSERT INTO WorkSalesOrder
select GINID,MIN([Posting Date])[Posting Date] from dbo.FactSalesSummary
Group by GINID


IF object_id('Tempdb..#WorkSalesOrder') Is Not Null DROP TABLE #WorkSalesOrder
CREATE TABLE #WorkSalesOrder ([GINID] int, FirstOrderDate [date], DraftYear int, DraftQuarter varchar(2), DraftMonth int)
INSERT INTO #WorkSalesOrder
SELECT GINID, MIN([PostingDate]) AS FirstOrderDate, YEAR(MIN([PostingDate])) AS DraftYear,
'Q' + CONVERT(VARCHAR(1), DATEPART(QQ, MIN([PostingDate]))) AS DraftQuarter, MONTH(MIN([PostingDate])) AS DraftMonth
FROM WorkSalesOrder Group By GINID


CREATE NONCLUSTERED INDEX IX_FOD_DY_DQ_DF
ON #WorkSalesOrder ([GINID])
INCLUDE ([FirstOrderDate],[DraftYear],[DraftQuarter],[DraftMonth])


UPDATE d SET d.[Draft Year] = w.DraftYear, d.FirstOrderDate = w.FirstOrderDate,
	d.DraftQuarter = w.DraftQuarter, d.DraftMonth = w.DraftMonth
FROM #WorkSalesOrder w INNER JOIN DimSalesOrder_MDM_Backup d 
ON w.GINID = d.GINID 
where d.[Draft Year] is null 



