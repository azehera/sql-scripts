﻿

Create Proc [dbo].[Update_PaidAsRank]

 @R INT = 1
AS

 
 WHILE @R > 0
BEGIN
  BEGIN TRANSACTION;
 
Update top (500000) dbo.FactSales_MDM
set paidAsRank = 'NONE'
where CustomerPostingGroup = 'TSFL' and paidAsRank is null
and [Posting Date] >= Getdate()-90 

  SET @r = @@ROWCOUNT;
 
  COMMIT TRANSACTION;
 
  -- CHECKPOINT;    -- if simple
  -- BACKUP LOG ... -- if full
END

