﻿CREATE Procedure [dbo].[Update_DimSalesOrder_LO_Interval_GINFix]
As

--LifeOrder Interval  
--Add row numbers for every order per GINIDS
IF OBJECT_ID('Tempdb..#temp2C') IS NOT NULL DROP TABLE #temp2C
Select GINID,[Posting Date],[DocumentNo],
ROW_NUMBER() OVER ( PARTITION BY GINID ORDER BY [Posting Date], DocumentNo ASC ) AS [RowNumber]
Into #temp2C
from dbo.FactSalesSummary FS
where FS.OrderType NOT IN ('RETURN','EXCHANGE')


--add +1 For Previous Order and put into new temp table
IF OBJECT_ID('Tempdb..#temp3C') IS NOT NULL DROP TABLE #temp3C
SELECT GINID,[Posting Date],[DocumentNo],
RowNumber+1 AS 'RowNumber'
INTO #temp3C
FROM #temp2C



--Add Time Elapsed
IF OBJECT_ID('Tempdb..#TEMP3') IS NOT NULL DROP TABLE #TEMP3
SELECT  T2.GINID ,
        T2.[Posting Date] ,
        T2.DocumentNo ,
		T2.RowNumber AS 'Order Occurrence',
        t3.[Posting Date] AS 'Previous Order Date' ,
        T3.DocumentNo AS 'Previous DocumentNo',
		DATEDIFF (DAY,T3.[Posting Date],T2.[Posting Date]) AS 'Time Elapsed'
INTO #TEMP3
FROM    #temp2C T2
        LEFT JOIN #temp3C T3 ON T3.GINID = T2.GINID
                                AND T3.RowNumber = T2.RowNumber
ORDER BY T2.GINID


CREATE NONCLUSTERED INDEX Ix_T3_GINID_Docno
ON #TEMP3 ([GINID],[DocumentNo])
INCLUDE ([Time Elapsed])
 

Update DM
set [Life Order Interval] =case when [Time Elapsed] is null then 0 Else [Time Elapsed] END
From [dbo].[DimSalesOrder_MDM_Backup] DM
inner join #TEMP3 T3
on DM.GINID = T3.GINID and 
DM.DocumentNo = T3.DocumentNo
 


Update DM
set [Life Order Interval] =case when [Time Elapsed] is null then 0 Else [Time Elapsed] END
From [dbo].[DimSalesOrder_MDM_Backup] DM
inner join #TEMP3 T3
on DM.GINID = T3.GINID and 
DM.DocumentNo2 = T3.DocumentNo
where  DM.Documentno like 'ARCH%'  
 
Update DM
set [Life Order Interval] = 0
From [dbo].[DimSalesOrder_MDM_Backup] DM
where [Life Order Interval] is null




