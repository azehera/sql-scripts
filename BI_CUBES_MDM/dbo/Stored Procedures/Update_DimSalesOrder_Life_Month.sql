﻿

/*********************************************************************************************
OBJECT NAME: Update_DimSalesOrder_Life_Month
DEVELOPER: Ron Baldwin      
DATE: 01/29/2015           
DESCRIPTION: Called by BICubesScheduledTasks windows service to the life month value.
Change Log::
DATE: 03/28/2019
Developer: Ronak Shah
Description: Modify this stored procedure to calculate Life Month only for new records in DimSalesorder_MDM table
**********************************************************************************************/

CREATE ProcEDURE [dbo].[Update_DimSalesOrder_Life_Month]
	@RetCode As varchar(50) output
AS

Declare @RowsDone int

-------Finding Life Month-------
IF object_id('Tempdb..#LifeMonth') Is Not Null DROP TABLE #LifeMonth
CREATE TABLE #LifeMonth (SelltoCustomerID varchar(20), DocumentNo varchar(20), FirstOrderDate date, PostingDate date, LifeMonth int)
INSERT INTO #LifeMonth
SELECT distinct d.SelltoCustomerID, d.DocumentNo, d.FirstOrderDate, f.[Posting Date], DATEDIFF(MM, d.FirstOrderDate, f.[Posting Date]) as LifeMonth
FROM DimSalesOrder_MDM d INNER JOIN FactSales_MDM f ON d.SelltoCustomerID = f.SelltoCustomerID and d.DocumentNo = f.DocumentNo 
WHERE d.LifeMonth IS NULL
--OPTION (MAXDOP 1)



---- Populate LifeMonth -------------- 
UPDATE d SET d.LifeMonth = l.LifeMonth 
FROM DimSalesOrder_MDM d INNER JOIN #LifeMonth l 
ON d.SelltoCustomerID = l.SelltoCustomerID and d.DocumentNo = l.DocumentNo 
--OPTION (MAXDOP 1)

set @RowsDone = @@RowCount

UPDATE d SET d.LifeMonth = l.LifeMonth FROM DimSalesOrder_MDM d INNER JOIN #LifeMonth l 
ON d.DocumentNo = l.DocumentNo where d.LifeMonth = 0 OPTION (MAXDOP 1)

set @RowsDone = @RowsDone + @@RowCount

UPDATE DimSalesOrder_MDM SET LifeMonth = 0 where LifeMonth IS NULL

set @RowsDone = @RowsDone + @@RowCount
Set @RetCode = 'Success: Rows Updated = ' + Convert(varchar(20), @RowsDone)
