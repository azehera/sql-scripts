﻿


 
 CREATE ProcEDURE [dbo].[Delete_factsales_MDM] 
 @r INT = 1
AS
 
WHILE @r > 0
BEGIN
  BEGIN TRANSACTION;
 
  DELETE TOP (250000) -- this will change
    dbo.FactSales_MDM
    where [Posting Date] >= DateAdd(dd, DateDiff(dd,0,GetDate()-35), 0)
 
  SET @r = @@ROWCOUNT;
 
  COMMIT TRANSACTION;
 
  -- CHECKPOINT;    -- if simple
  -- BACKUP LOG ... -- if full
END




