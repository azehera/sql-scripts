﻿
/******************************************************************************************
Change Log
OBJECT NAME:      [dbo].[Update_DimSalesOrder_LifeOrder]
DEVELOPER:  Ronak Shah  
DATE:  04/01/2019           
DESCRIPTION: Update LifeOrde column for new records
PARAMETERS/VARRIABLES:
DATE: 01/20/2020            
DESCRIPTION: OPTIMIZE this SP 
*******************************************************************************************/
CREATE Procedure [dbo].[Update_DimSalesOrder_LifeOrder]
AS

/* Bring  those GINs who has lifeorder value is null */
IF object_id('Tempdb..#GINID') Is Not Null DROP TABLE #GINID
CREATE TABLE #GINID (GINID Bigint PRIMARY KEY)
INSERT INTO #GINID
select distinct GINID from dbo.Dimsalesorder_MDM DM
where DM.LifeOrder is null 



Update  DM
set DM.LifeOrder = FM.LifeOrder 
FROM dbo.DimSalesOrder_MDM DM
INNER JOIN (
			select GINID,documentno,[posting Date] ,
			DENSE_RANK () Over ( partition by GINID  order by [posting Date],documentno )  LifeOrder
			FROM (
					select distinct GINID,documentno,[posting Date]  from dbo.FactSalesArchive where GINID IN (Select GINID from #GINID)
					 UNION ALL
					select distinct GINID,documentno,[posting Date]  from dbo.FactSales_MDM
					 where ordertype not in ('EXCHANGE','RETURN') and GINID IN (Select GINID from #GINID)
					 UNION ALL
					 select distinct GINID,documentno,[posting Date]  from [dbo].[FactSales_MDM_Archive]
					 where ordertype not in ('EXCHANGE','RETURN') and GINID IN (Select GINID from #GINID)
			 )A 
         ) FM
 ON FM.DocumentNo = DM.DocumentNo and FM.GINID = DM.GINID
 where DM.LifeOrder is null

 
 IF object_id('Tempdb..#X') Is Not Null DROP TABLE #X
select DM.SelltoCustomerID,DM.DocumentNo,DM.lifeorder,x1.[Document No for Return Order] into #X from dbo.DimSalesOrder_MDM DM
inner join dbo.ReturnExchangeOrder x1 on x1.SelltoCustomerID = DM.SelltoCustomerID and x1.[Document No for Original Order] = DM.DocumentNo
where  DM.GINID IN (Select GINID from #GINID)


Update DM1
set DM1.lifeorder = A.lifeorder
From 
( select * from #X
--select DM.SelltoCustomerID,DM.DocumentNo,DM.lifeorder,x1.[Document No for Return Order] from dbo.DimSalesOrder_MDM DM
--inner join dbo.ReturnExchangeOrder x1 on x1.SelltoCustomerID = DM.SelltoCustomerID and x1.[Document No for Original Order] = DM.DocumentNo
--where  DM.GINID IN (Select GINID from #GINID)
)A
inner join dbo.DimSalesOrder_MDM DM1
on A.[Document No for Return Order] = DM1.DocumentNo and A.SelltoCustomerID = DM1.SelltoCustomerID
Where DM1.lifeorder is null 
and DM1.GINID IN (Select GINID from #GINID)

 
