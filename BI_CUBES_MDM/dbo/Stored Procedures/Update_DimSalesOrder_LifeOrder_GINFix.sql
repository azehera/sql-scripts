﻿ 


CREATE Procedure [dbo].[Update_DimSalesOrder_LifeOrder_GINFix]
As
--Update Life Order

IF object_id('Tempdb..#GINID') Is Not Null DROP TABLE #GINID
CREATE TABLE #GINID (GINID Bigint PRIMARY KEY)
INSERT INTO #GINID
select distinct GINID from dbo.DimSalesOrder_MDM_Backup DM
where DM.LifeOrder is null 

select GINID,documentno,[posting Date],
DENSE_RANK () Over ( partition by GINID  order by [posting Date],documentno )  LifeOrder
Into #LifeOrder
from dbo.FactSalesSummary
where ordertype not in ('EXCHANGE','RETURN') and 
GINID IN (Select GINID from #GINID)


CREATE NONCLUSTERED INDEX IX_temp1
ON #LifeOrder ([GINID],[documentno])
INCLUDE ([LifeOrder])


Update  DM
set DM.LifeOrder = FM.LifeOrder 
FROM dbo.DimSalesOrder_MDM_Backup DM
INNER JOIN #LifeOrder FM
ON FM.DocumentNo = DM.DocumentNo  
and FM.GINID = DM.GINID


Update  DM
set DM.LifeOrder = FM.LifeOrder 
FROM dbo.DimSalesOrder_MDM_Backup DM
INNER JOIN #LifeOrder FM
 ON FM.DocumentNo =  [DocumentNo2]
 and FM.GINID = DM.GINID
 where  DM.Documentno like 'ARCH%'  


 
IF object_id('Tempdb..#X') Is Not Null DROP TABLE #X
select DM.SelltoCustomerID,DM.DocumentNo,DM.lifeorder,x1.[Document No for Return Order] into #X 
from dbo.DimSalesOrder_MDM_Backup DM
inner join dbo.ReturnExchangeOrder x1 
on x1.SelltoCustomerID = DM.SelltoCustomerID 
  and x1.[Document No for Original Order] = DM.DocumentNo
where  DM.GINID IN (Select GINID from #GINID)


Update DM1
set DM1.lifeorder = A.lifeorder
From 
( select * from #X
--select DM.SelltoCustomerID,DM.DocumentNo,DM.lifeorder,x1.[Document No for Return Order] from dbo.DimSalesOrder_MDM DM
--inner join dbo.ReturnExchangeOrder x1 on x1.SelltoCustomerID = DM.SelltoCustomerID and x1.[Document No for Original Order] = DM.DocumentNo
--where  DM.GINID IN (Select GINID from #GINID)
)A
inner join dbo.DimSalesOrder_MDM_Backup DM1
on A.[Document No for Return Order] = DM1.DocumentNo and A.SelltoCustomerID = DM1.SelltoCustomerID
Where DM1.lifeorder is null 
and DM1.GINID IN (Select GINID from #GINID)

 


