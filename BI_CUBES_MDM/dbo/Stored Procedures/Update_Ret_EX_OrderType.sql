﻿

/*
Update OrderType Column in Fact Sales MDM table
Include Return & Exchange in Phase 2

Developer Note: 05/04/2020
Notes: Optimize this stored procedure to use existing indexed column in Factsales_MDM table
*/
 

CREATE Procedure [dbo].[Update_Ret_EX_OrderType]
AS

   UPDATE  FM
   set  FM.OrderType = RE.OrderType
   FROM  dbo.FactSales_MDM FM 
		Inner Join dbo.ReturnExchangeOrder RE
		ON RE.[Document No for Return Order] = FM.DocumentNo and  RE.SelltoCustomerID =FM.SelltoCustomerID
		Where FM.[Posting Date] >= Getdate()-60
 
