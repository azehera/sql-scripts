﻿
/******************************************************************************************
Change Log
OBJECT NAME: [Update_DimSalesOrder_FO_TYPE]
DEVELOPER: Ronak Shah      
DATE: 04/01/2019          
DESCRIPTION: Modify this stored procedure to include FactSales_MDM_Archive table
*******************************************************************************************/

Create Proc [dbo].[Update_DimSalesOrder_FO_TYPE]
AS


Update OG
set OG.[FO-CustomerPostingGroup] = FM.[FO-CustomerPostingGroup] 
FROM dbo.OrdersByGIN OG
INNER JOIN (
			select GINID,documentno,[posting Date] ,
			FIRST_VALUE(CustomerPostingGroup) OVER (Partition BY GINID ORDER BY [Posting Date] ASC,documentno asc) AS [FO-CustomerPostingGroup]
			FROM (
					select distinct GINID,documentno,[posting Date],CustomerPostingGroup  from dbo.FactSalesArchive
					UNION
					select distinct GINID,documentno,[posting Date],CustomerPostingGroup  from dbo.FactSales_MDM_Archive
					 UNION
					select distinct GINID,documentno,[posting Date],CustomerPostingGroup  from dbo.FactSales_MDM
				 )A
		    ) FM
ON   FM.GINID = OG.GroupNo

 

Update OG
set OG.[FO-Type] = FM.[FO-Type] 
FROM dbo.OrdersByGIN OG
INNER JOIN (
			select GINID,documentno,[posting Date] ,
			FIRST_VALUE(OrderType) OVER (Partition BY GINID ORDER BY [Posting Date] ASC,documentno asc) AS [FO-Type]
			FROM (
					select distinct GINID,documentno,[posting Date],ordertype  from dbo.FactSalesArchive
					UNION
					select distinct GINID,documentno,[posting Date],ordertype  from dbo.FactSales_MDM_Archive
					 UNION
					select distinct GINID,documentno,[posting Date],ordertype  from dbo.FactSales_MDM
					
			     )A
		  ) FM
 ON  FM.GINID = OG.GroupNo


 

 
