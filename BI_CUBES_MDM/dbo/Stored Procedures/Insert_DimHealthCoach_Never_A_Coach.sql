﻿

/******************************************************************************************
OBJECT NAME: Insert_DimHealthCoach_Never_A_Coach
DEVELOPER: Ron Baldwin      
DATE: 02/02/2015           
DESCRIPTION: Called by BICubesScheduledTasks windows service to back fill missing 
'Never A Coach' rows in DimHealthCoach for referential integrity.  This stored
procedure assumes that DimHealthCoach has been loaded with current Health Coach data.
This data may be provided in a spreadsheet from Odyssey or built from Odyssey query.

Date : 05/04/2020
Note: Modify stored procedure to ignore customer ids with null Values
*******************************************************************************************/

CREATE ProcEDURE [dbo].[Insert_DimHealthCoach_Never_A_Coach]
	@RetCode As varchar(50) output
AS

Declare @RowsDone int

IF object_id('Tempdb..#DimHealthCoach') Is Not Null DROP TABLE #DimHealthCoach
CREATE TABLE #DimHealthCoach (CustNo varchar(20))
insert into #DimHealthCoach
select distinct s.SelltoCustomerID from DimSelltoCustomerKey s left join DimHealthCoach d 
on s.SelltoCustomerID = d.CustomerNumber where d.CustomerNumber IS NULL order by SelltoCustomerID

insert into DimHealthCoach
select t.CustNo, -- Cust No
(select top 1 s.[Sell-to Customer Name] from DimSellToCustomerKey s where s.SelltoCustomerID = t.CustNo), -- Full Name (No Dups)
'', -- Type
'', -- Status
'', -- Rank
'', -- Highest
'', -- Sponsor
'', -- President
'',  -- Global
'Never a Coach' -- Cube Status
from #DimHealthCoach t 
where t.CustNo is not null
order by CustNo

set @RowsDone = @@RowCount

Set @RetCode = 'Success: Rows Inserted = ' + Convert(varchar(20), @RowsDone)
