﻿

/****************************************************************
Date: 08/25/2016
Developer: Ronak Shah
Description: This stored procedure is part of MDM Phase 3 Update.
             This process will calculate life order interval within each GroupId.
			 Life Order interval means day(S) difference between first order and second order.
			 First order life order interval is 0
			 Calculation will be basedon dbo.factsales_MDM & dbo.factsalesarchive
Change Log:
Developer: Ronak Shah
Description : Add step for [FactSales_MDM_Archive]  
Date: 04/20/2020
To avoid duplicate document no use ARCHIVE criteria

****************************************************************/


CREATE Proc [dbo].[Update_DimSalesOrder_LO_Interval]

AS
--Find Orders for all GINIDS

IF OBJECT_ID('Tempdb..#Temp') IS NOT NULL DROP TABLE #Temp
SELECT DISTINCT M.GINID, M.[Posting Date], DocumentNo
into #Temp
FROM  dbo.FactSales_MDM M 
where M.OrderType NOT IN ('RETURN','EXCHANGE')

 

IF OBJECT_ID('Tempdb..#Temp_') IS NOT NULL DROP TABLE #Temp_
SELECT DISTINCT M.GINID, M.[Posting Date], DocumentNo
into #Temp_
FROM  dbo.FactSalesArchive M
where M.OrderType NOT IN ('RETURN','EXCHANGE')


IF OBJECT_ID('Tempdb..#Temp1') IS NOT NULL DROP TABLE #Temp1
SELECT DISTINCT M.GINID, M.[Posting Date], DocumentNo
into #Temp1
FROM  [dbo].[FactSales_MDM_Archive] M 
where M.OrderType NOT IN ('RETURN','EXCHANGE')


--Add row numbers for every order per GINIDS
IF OBJECT_ID('Tempdb..#temp2C') IS NOT NULL DROP TABLE #temp2C
SELECT GINID,[Posting Date],[DocumentNo],
ROW_NUMBER() OVER ( PARTITION BY GINID ORDER BY [Posting Date], DocumentNo ASC ) AS [RowNumber]
INTO #temp2C
FROM  (  select GINID,[Posting Date],[DocumentNo] from #Temp
		UNION ALL
		select GINID,[Posting Date],[DocumentNo] from #Temp_
		UNION ALL
		select GINID,[Posting Date],[DocumentNo] from #Temp1
      ) x


--add +1 For Previous Order and put into new temp table
IF OBJECT_ID('Tempdb..#temp3C') IS NOT NULL DROP TABLE #temp3C
SELECT GINID,[Posting Date],[DocumentNo],
RowNumber+1 AS 'RowNumber'
INTO #temp3C
FROM #temp2C



--Add Time Elapsed
IF OBJECT_ID('Tempdb..#TEMP3') IS NOT NULL DROP TABLE #TEMP3
SELECT  T2.GINID ,
        T2.[Posting Date] ,
        T2.DocumentNo ,
		T2.RowNumber AS 'Order Occurrence',
        t3.[Posting Date] AS 'Previous Order Date' ,
        T3.DocumentNo AS 'Previous DocumentNo',
		DATEDIFF (DAY,T3.[Posting Date],T2.[Posting Date]) AS 'Time Elapsed'
INTO #TEMP3
FROM    #temp2C T2
        LEFT JOIN #temp3C T3 ON T3.GINID = T2.GINID
                                AND T3.RowNumber = T2.RowNumber
ORDER BY T2.GINID




Update DM
set [Life Order Interval] =case when [Time Elapsed] is null then 0 Else [Time Elapsed] END
From [dbo].[DimSalesOrder_MDM] DM
inner join #TEMP3 T3
on DM.GINID = T3.GINID and Case when DM.DocumentNo like 'ARCH%' THEN SUBSTRING(DM.DocumentNo,5,25) ELSE DM.DocumentNo END = T3.DocumentNo
 

 
Update DM
set [Life Order Interval] = 0
From [dbo].[DimSalesOrder_MDM] DM
where [Life Order Interval] is null



