﻿

/*********************************************************************************************
OBJECT NAME: Update_DimSalesOrder_Draft_Info
DEVELOPER: Ron Baldwin      
DATE: 01/29/2015           
DESCRIPTION: Called by BICubesScheduledTasks windows service to the update first order date,
	 Draft Year, Draft Quarter, Draft Month based on group membership.

Change Log::
Developer: Ronak Shah
Date: 03/28/2019
DESCRIPTION: Optimize this stored procedure to capture only those newly added records to Dimsalesorder_MDM table.

Developer: Ronak Shah
Date: 02/03/2020
DESCRIPTION: Removed maxdop query hint & add index to temp table to improve query performance.

**********************************************************************************************/
 
CREATE ProcEDURE [dbo].[Update_DimSalesOrder_Draft_Info]
	@RetCode As varchar(50) output
AS

Declare @RowsDone int

TRUNCATE TABLE WorkSalesOrder

IF object_id('Tempdb..#GINID') Is Not Null DROP TABLE #GINID
Create table #GINID (GINID BIGINT)
INSERT INTO #GINID (GINID)
select distinct GINID from dbo.dimsalesorder_MDM where [Draft Year] is null

INSERT INTO WorkSalesOrder
SELECT GINID, [Posting Date] FROM [FactSalesArchive] (NOLOCK) where GINID IN (select GINID from #GINID)
GROUP BY GINID, [Posting Date]

INSERT INTO WorkSalesOrder
SELECT GINID, [Posting Date] FROM [FactSales_MDM] (NOLOCK) where GINID IN (select GINID from #GINID)
GROUP BY GINID, [Posting Date]

INSERT INTO WorkSalesOrder
SELECT GINID, [Posting Date] FROM [dbo].[FactSales_MDM_Archive] (NOLOCK) where GINID IN (select GINID from #GINID)
GROUP BY GINID, [Posting Date]
--1:27 

IF object_id('Tempdb..#WorkSalesOrder') Is Not Null DROP TABLE #WorkSalesOrder
CREATE TABLE #WorkSalesOrder ([GINID] int, FirstOrderDate [date], DraftYear int, DraftQuarter varchar(2), DraftMonth int)
INSERT INTO #WorkSalesOrder
SELECT GINID, MIN([PostingDate]) AS FirstOrderDate, YEAR(MIN([PostingDate])) AS DraftYear,
'Q' + CONVERT(VARCHAR(1), DATEPART(QQ, MIN([PostingDate]))) AS DraftQuarter, MONTH(MIN([PostingDate])) AS DraftMonth
FROM WorkSalesOrder Group By GINID

CREATE NONCLUSTERED INDEX IX_FOD_DY_DQ_DF
ON #WorkSalesOrder ([GINID])
INCLUDE ([FirstOrderDate],[DraftYear],[DraftQuarter],[DraftMonth])

UPDATE d SET d.[Draft Year] = w.DraftYear, d.FirstOrderDate = w.FirstOrderDate,
	d.DraftQuarter = w.DraftQuarter, d.DraftMonth = w.DraftMonth
FROM #WorkSalesOrder w INNER JOIN DimSalesOrder_MDM d 
ON w.GINID = d.GINID 
where d.[Draft Year] is null 
--OPTION (MAXDOP 1)

set @RowsDone = @@RowCount

Set @RetCode = 'Success: Rows Updated = ' + Convert(varchar(20), @RowsDone)
