﻿
/* 
======================================================================================
Developer Notes:

Date : 05/04/2020
Note: Modify stored procedure to ignore customer ids with null Values


======================================================================================
*/


CREATE Procedure [dbo].[Insert_FactSales_last30Days]
      
AS

INSERT [dbo].[FactSales_MDM]
([Country],[CustomerPostingGroup],[SalesChannel]
,[ItemCategoryCode],[ItemCategoryCodeSubcategory],[SelltoCustomerID]
,[SelltoCustomerKey],[ShiptoCustomerKey],[ItemCode],[ItemCodeSubcategory]
,[DocumentNo],[LineDiscount],[InvoiceDiscount],[Units]
,[SubCategoryUnits],[UnitsPerBox],[BOX],[Amount]
,[Gross Amount],[Amount Including VAT],[TaxAmount]
,[Tax Group Code],[Unit Cost (LCY)],[Quantity]
,[SalesType],[LocationID],[RegionID],[Posting Date],
GINID,OrderType
)
SELECT [Country],[CustomerPostingGroup],[SalesChannel]
      ,[ItemCategoryCode],[ItemCategoryCodeSubcategory],[SelltoCustomerID]
      ,[SelltoCustomerKey],[ShiptoCustomerKey],[ItemCode],[ItemCodeSubcategory]
      ,[DocumentNo],[LineDiscount],[InvoiceDiscount],[Units]
      ,[SubCategoryUnits],[UnitsPerBox],[BOX],[Amount]
      ,[Gross Amount],[Amount Including VAT],[TaxAmount]
      ,[Tax Group Code],[Unit Cost (LCY)],[Quantity]
      ,[SalesType],[LocationID],[RegionID],[Posting Date]
	  ,GM.GINID
 	  ,case when OT.OrderType is null then 'ONDEMAND' ELSE OT.OrderType END as OrderType
  FROM [BI_SSAS_CUBES].[dbo].[FactSales] FS
  Left outer join  dbo.Group_MDM_Reltio GM 
 on case when FS.SelltoCustomerID like 'TSFL-%' THEN SUBSTRING(FS.SelltoCustomerID,6,20)
                     when FS.SelltoCustomerID like 'TSFL%' THEN SUBSTRING(FS.SelltoCustomerID,5,20)
					 ELSE FS.SelltoCustomerID END  = GM.CustomerID
  Left outer join dbo.OrderType OT
  on OT.DocNo = FS.DocumentNo
  where [Posting Date] >=  DateAdd(dd, DateDiff(dd,0,GetDate()-35), 0)
  AND [SelltoCustomerID] IS NOT NULL



Update dbo.FactSales_MDM
set GINID = 0
where GINID is null and [Posting Date] >=  DateAdd(dd, DateDiff(dd,0,GetDate()-35), 0)
