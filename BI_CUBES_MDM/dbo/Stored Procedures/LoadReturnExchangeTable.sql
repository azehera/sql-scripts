﻿
 /* 
  This stored procedure will load data into dbo.ReturnExchangeOrder table

  Modify this stored procedure on 5/25/2017 in order to resolve COLLATION Issue. By Ronak Shah..

 */

CREATE Procedure [dbo].[LoadReturnExchangeTable]
AS


Delete From dbo.ReturnExchangeOrder
where [Document Date] >= Getdate()-60

Insert dbo.ReturnExchangeOrder
(
[Document No for Return Order],SelltoCustomerID,[Return Order No],[Document No for Original Order],[Original Order No Ucart],[Document Date] 
)
  SELECT  
 			 distinct   CH.No_ AS [Document No for Return Order] ,
			 CH.[Sell-to Customer No_] SelltoCustomerID,
            CH.[External Document No_] AS [Return Order No] ,
             IH.No_ AS [Document No for Original Order] ,
             IH.[External Document No_] AS [Original Order No Ucart],
			CH.[Document Date] 
  FROM      [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Header] CH
            JOIN [NAV_ETL].[dbo].[Jason Pharm$Sales Cr_Memo Line] CL  ON CH.[No_] COLLATE DATABASE_DEFAULT = CL.[Document No_]   COLLATE DATABASE_DEFAULT 
            JOIN [NAV_ETL].[dbo].[Jason Pharm$Sales Invoice Header] IH   ON CH.[Escalate Original Order No_] COLLATE DATABASE_DEFAULT = IH.[External Document No_]  COLLATE DATABASE_DEFAULT 
  WHERE     CH.[Escalate Original Order No_] <> '0'
            AND LEN(CH.[Escalate Original Order No_]) > 0
                     AND CH.[Document Date] >= Getdate()-60
					 order by 3

--Update OriginalOrderAmount

Update x1
set OriginalOrderAmount = FM.AMOUNT
from dbo.ReturnExchangeOrder x1
inner join 
			(select SelltoCustomerID,DocumentNo,Sum(AMOUNT) AMOUNT
			from dbo.FactSales_MDM
			Group by SelltoCustomerID,DocumentNo) FM
on x1.[Document No for Original Order] = FM.DocumentNo and x1.SelltoCustomerID = FM.SelltoCustomerID 
 where x1.[Document Date] >= Getdate()-60
 and x1.OriginalOrderAmount is null 

--Update ReturnOrderAmount

Update x1
set ReturnOrderAmount = FM.AMOUNT
from dbo.ReturnExchangeOrder x1
inner join 
			(select SelltoCustomerID,DocumentNo,Sum(AMOUNT) AMOUNT
			from dbo.FactSales_MDM
			Group by SelltoCustomerID,DocumentNo) FM
on x1.[Document No for Return Order] = FM.DocumentNo and x1.SelltoCustomerID = FM.SelltoCustomerID 
where x1.[Document Date] >= Getdate()-60
and x1.ReturnOrderAmount is null 


Update dbo.ReturnExchangeOrder 
set OrderType = case when Returnorderamount = 0.000 Then 'EXCHANGE' Else 'RETURN' END
where [Document Date] >= Getdate()-60


