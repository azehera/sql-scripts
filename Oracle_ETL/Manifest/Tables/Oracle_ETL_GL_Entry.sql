﻿CREATE TABLE [Manifest].[Oracle_ETL_GL_Entry] (
    [Amount]              DECIMAL (38, 20) NULL,
    [Posting_Date]        DATETIME         NULL,
    [Acct]                NCHAR (255)      NULL,
    [Dept]                NCHAR (255)      NULL,
    [Created_By]          NVARCHAR (255)   NULL,
    [Created_Date]        DATETIME         NULL,
    [Modified_By]         NVARCHAR (255)   NULL,
    [Modified_Date]       DATETIME         NULL,
    [Filename]            NVARCHAR (255)   NULL,
    [Filename_Start_Date] DATETIME         NULL,
    [Filename_End_Date]   DATETIME         NULL,
    [Entry_No]            NVARCHAR (100)   NULL,
    [Currency_Code]       NVARCHAR (100)   NULL
);

