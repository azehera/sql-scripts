﻿CREATE TABLE [Manifest].[Oracle_ETL_Sales_Invoice_Line] (
    [Document_No_]              NVARCHAR (255)   NULL,
    [Type]                      NVARCHAR (255)   NULL,
    [No_]                       NVARCHAR (255)   NULL,
    [Location_Code]             NVARCHAR (255)   NULL,
    [Quantity]                  DECIMAL (38, 20) NULL,
    [Line_Discount_Amount]      DECIMAL (38, 20) NULL,
    [Qty_per_Unit_of_Measure]   DECIMAL (38, 20) NULL,
    [Quantity_Base]             DECIMAL (38, 20) NULL,
    [Amount]                    DECIMAL (38, 20) NULL,
    [Line_Amount]               DECIMAL (38, 20) NULL,
    [Amount_Including_VAT]      DECIMAL (38, 20) NULL,
    [Tax_Group_Code]            NVARCHAR (255)   NULL,
    [Unit_Cost_LCY]             DECIMAL (38, 20) NULL,
    [Item_Category_Code]        NVARCHAR (255)   NULL,
    [Description]               NVARCHAR (255)   NULL,
    [Unit_of_Measure]           NVARCHAR (255)   NULL,
    [Unit_Price]                DECIMAL (38, 20) NULL,
    [Order_Item_No_]            NVARCHAR (255)   NULL,
    [Inv_Discount_Amount]       DECIMAL (38, 20) NULL,
    [Shortcut_Dimension_2_Code] NVARCHAR (255)   NULL,
    [Posting_Date]              DATETIME         NULL,
    [Shipment_Date]             DATETIME         NULL,
    [Sell_to_Customer_No_]      NVARCHAR (255)   NULL,
    [Created_By]                NVARCHAR (255)   NULL,
    [Created_Date]              DATETIME         NULL,
    [Modified_By]               NVARCHAR (255)   NULL,
    [Modified_Date]             DATETIME         NULL,
    [Filename]                  NVARCHAR (255)   NULL,
    [Filename_Start_Date]       DATETIME         NULL,
    [Filename_End_Date]         DATETIME         NULL
);

