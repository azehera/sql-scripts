﻿CREATE PROCEDURE [dbo].[USP_Upsert_Sales_Invoice_Line]
as

----------Remove Duplicates from Stage Table--------------
 With cte as (
select row_number() over (partition by Document_No_, No_,Order_Item_No_ order by Order_Item_No_ desc) as N, *
 from dbo.oracle_etl_sales_invoice_line_stg)
 delete from cte where N>1

 -----------Perform Upsert Logic--------------------
MERGE [Oracle_ETL].[dbo].[Oracle_ETL_Sales_Invoice_Line] AS target
USING [Oracle_ETL].[dbo].[Oracle_ETL_Sales_Invoice_Line_Stg] AS SOURCE
ON (target.No_ =SOURCE.No_ AND target.[Document_No_] = SOURCE.[Document_No_]
AND target.[Order_Item_No_] = SOURCE.[Order_Item_No_] AND target.Posting_Date>getdate()-45)
WHEN MATCHED
THEN UPDATE SET target.[Document_No_] = SOURCE.[Document_No_] ,
				target.[Type] = SOURCE.[Type] ,
				target.[No_] = SOURCE.[No_] ,
				target.[Location_Code] =SOURCE.[Location_Code],
				target.[Quantity] = SOURCE.[Quantity],
				target.[Line_Discount_Amount] = SOURCE.[Line_Discount_Amount] ,
				target.[Qty_per_Unit_of_Measure]=SOURCE.[Qty_per_Unit_of_Measure],
				target.[Quantity_Base]=SOURCE.[Quantity_Base],
				target.[Amount]=SOURCE.[Amount],
				target.[Line_Amount]=SOURCE.[Line_Amount],
				target.[Amount_Including_VAT]=SOURCE.[Amount_Including_VAT],
				target.[Tax_Group_Code]=SOURCE.[Tax_Group_Code],
				target.[Unit_Cost_LCY]=SOURCE.[Unit_Cost_LCY],
				target.[Item_Category_Code] = SOURCE.[Item_Category_Code],
				target.[Description] = SOURCE.[Description],
				target.[Unit_of_Measure] = SOURCE.[Unit_of_Measure],
				target.[Unit_Price] = SOURCE.[Unit_Price],
				target.[Order_Item_No_]=SOURCE.[Order_Item_No_],
				target.[Inv_Discount_Amount] = SOURCE.[Inv_Discount_Amount],
				target.[Shortcut_Dimension_2_Code] = SOURCE.[Shortcut_Dimension_2_Code],
				target.[Posting_Date] = SOURCE.[Posting_Date],
				target.[Shipment_Date] = SOURCE.[Shipment_Date],
				target.[Sell_to_Customer_No_] = SOURCE.[Sell_to_Customer_No_],
				target.[Modified_By] = 'ETL_User',
				target.[Modified_Date] = getdate(),
				target.[Filename] = SOURCE.[Filename]
WHEN NOT MATCHED BY TARGET
THEN INSERT ([Document_No_]
      ,[Type]
      ,[No_]
      ,[Location_Code]
      ,[Quantity]
      ,[Line_Discount_Amount]
      ,[Qty_per_Unit_of_Measure]
      ,[Quantity_Base]
      ,[Amount]
      ,[Line_Amount]
      ,[Amount_Including_VAT]
      ,[Tax_Group_Code]
      ,[Unit_Cost_LCY]
      ,[Item_Category_Code]
      ,[Description]
      ,[Unit_of_Measure]
      ,[Unit_Price]
      ,[Order_Item_No_]
      ,[Inv_Discount_Amount]
      ,[Shortcut_Dimension_2_Code]
      ,[Posting_Date]
      ,[Shipment_Date]
      ,[Sell_to_Customer_No_]
      ,[Created_By]
      ,[Created_Date]
      ,[Filename])
              VALUES (SOURCE.[Document_No_],
SOURCE.[Type],
SOURCE.[No_],
SOURCE.[Location_Code],
SOURCE.[Quantity],
SOURCE.[Line_Discount_Amount],
SOURCE.[Qty_per_Unit_of_Measure],
SOURCE.[Quantity_Base],
SOURCE.[Amount],
SOURCE.[Line_Amount],
SOURCE.[Amount_Including_VAT],
SOURCE.[Tax_Group_Code],
SOURCE.[Unit_Cost_LCY],
SOURCE.[Item_Category_Code],
SOURCE.[Description],
SOURCE.[Unit_of_Measure],
SOURCE.[Unit_Price],
SOURCE.[Order_Item_No_],
SOURCE.[Inv_Discount_Amount],
SOURCE.[Shortcut_Dimension_2_Code],
SOURCE.[Posting_Date],
SOURCE.[Shipment_Date],
SOURCE.[Sell_to_Customer_No_],
SOURCE.[Created_By],
SOURCE.[Created_Date],
SOURCE.[Filename]);


 
