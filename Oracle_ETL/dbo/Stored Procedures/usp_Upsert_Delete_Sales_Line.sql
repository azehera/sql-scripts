﻿

/* The stored proc is updating, inserting and deleteing data from [dbo].[Oracle_ETL_Sales_Line]
  by comparing Document_No_, target.No_, Order_Item_No_ fields in staging table and record_status */

CREATE procedure [dbo].[usp_Upsert_Delete_Sales_Line]
as


with cte as (
select row_number() over (partition by Document_No_, No_,Order_Item_No_ order by Order_Item_No_ desc) as N, *
from [ORACLE_ETL].[dbo].[Oracle_ETL_Sales_Line_STG])
delete from cte where N>1

-------------------Upsert operation and deleting the old records--------------------------
merge [ORACLE_ETL].[dbo].[Oracle_ETL_Sales_Line] as target
using [ORACLE_ETL].[dbo].[Oracle_ETL_Sales_Line_STG] as source -- staging table
on (target.Document_No_ =source.Document_No_ and target.No_ = source.No_ and target.Order_Item_No_ = source.Order_Item_No_)

/* delete data from [dbo].[Oracle_ETL_Sales_Line] for matching records with source.Record_status = 'DELETE' */
when matched and source.[RECORD_STATUS] = 'DELETE'
then delete 

/* Update the data in [dbo].[Oracle_ETL_Sales_Line] for matching records with source.Record_status = 'UPSERT' */
when matched and source.[RECORD_STATUS] = 'UPSERT'
then update set target.[Quantity_Base] = source.[Quantity_Base] , 
				target.[Location_Code] = source.[Location_Code] ,
				target.[Shipment_Date] = source.[Shipment_Date] ,
				target.[Shortcut_Dimension_2_Code]=source.[Shortcut_Dimension_2_Code],
				target.[Description]=source.[Description],
				target.[Document_Type] = source.[Document_Type] ,
				target.[Return_Qty_Received]=source.[Return_Qty_Received],
				target.[Qty_to_Invoice]=source.[Qty_to_Invoice],
				target.[Line_Amount]=source.[Line_Amount],
				target.[Amount_Including_VAT]=source.[Amount_Including_VAT],
				target.[Quantity]=source.[Quantity],
				target.[Qty_per_Unit_of_Measure]=source.[Qty_per_Unit_of_Measure],
				target.[Amount]=source.[Amount],
				target.[Unit_of_Measure_Code]=source.[Unit_of_Measure_Code],
				target.[Modified_By]='ETL_User',
				target.[Modified_Date]=getdate(),
				target.[Filename]=source.[Filename],
				target.[WMS_INTERFACE_FLAG] = source.[WMS_INTERFACE_FLAG],
				target.[RELEASED_STATUS] = source.[INVENTORY_RESERVATION],
				target.[Order_Item_No_] = source.[Order_Item_No_],
				target.[Item_Category_Code] = source.[Item_Category_Code]

/* Insert data in [dbo].[Oracle_ETL_Sales_Line] for non matching records with source.Record_status = 'UPSERT' */

when not matched by target and source.[RECORD_STATUS] = 'UPSERT'
then insert ([Document_No_], 
             [Quantity_Base], 
			 [No_], 
			 [Location_Code], 
			 [Shipment_Date], 
			 [Shortcut_Dimension_2_Code], 
			 [Description], 
			 [Document_Type], 
			 [Return_Qty_Received], 
			 [Qty_to_Invoice], 
			 [Line_Amount], 
			 [Amount_Including_VAT], 
			 [Quantity], 
			 [Qty_per_Unit_of_Measure], 
			 [Amount], 
			 [Unit_of_Measure_Code], 
			 [Created_By], 
			 [Created_Date], 
			 [Filename],
			 [WMS_INTERFACE_FLAG],
			 [RELEASED_STATUS],
			 [INVENTORY_RESERVATION],
			 [Order_Item_No_],
			 [Item_Category_Code]) 
             
			 values (source.[Document_No_], 
			         source.[Quantity_Base], 
					 source.[No_], 
					 source.[Location_Code], 
					 source.[Shipment_Date], 
					 source.[Shortcut_Dimension_2_Code], 
					 source.[Description], 
					 source.[Document_Type], 
					 source.[Return_Qty_Received], 
					 source.[Qty_to_Invoice], 
					 source.[Line_Amount], 
					 source.[Amount_Including_VAT], 
					 source.[Quantity], 
					 source.[Qty_per_Unit_of_Measure], 
					 source.[Amount], 
					 source.[Unit_of_Measure_Code], 
					 source.[Created_By], 
					 source.[Created_Date], 
					 source.[Filename],
					 source.[WMS_INTERFACE_FLAG],
					 source.[RELEASED_STATUS],
					 source.[INVENTORY_RESERVATION],
					 source.[Order_Item_No_],
					 source.[Item_Category_Code])
;

