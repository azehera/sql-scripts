﻿
-- =====================================================================================
-- Author:		Atya 
-- Create date: 6/1/2021
--DEscription: This SP Upserts/Deletes records from Oracle_ETL_Sales_Header table by comparing data against STG table
--------------------------------------- Modifications -----------------------------------

-- ======================================================================================
CREATE PROCEDURE [dbo].[usp_Upsert_Delete_Sales_Header]

as


with cte as (
select row_number() over (partition by No_ order by No_ desc) as N, *
from [ORACLE_ETL].[dbo].[Oracle_ETL_Sales_Header_STG])
Delete from cte where N>1


 -----------Perform Upsert and Delete --------------------
MERGE [Oracle_ETL].[dbo].[Oracle_ETL_Sales_Header] AS target
USING [Oracle_ETL].[dbo].[Oracle_ETL_Sales_Header_STG] AS source
ON (target.No_ = source.No_  )
WHEN MATCHED AND source.RECORD_STATUS='UPSERT'
THEN UPDATE SET 
			target.[Amount_Authorized] = source.[Amount_Authorized],
			target.[Sell_to_Customer_No_] = source.[Sell_to_Customer_No_],
			target.[External_Document_No_] = source.[External_Document_No_],
			target.[Document_Type] = source.[Document_Type],
			target.[Document_Date] = source.[Document_Date],
			target.[Return_Type] = source.[Return_Type],
			target.[Sell_to_Customer_Name] = source.[Sell_to_Customer_Name],
			target.[Your_Reference] = source.[Your_Reference],
			target.[Bill_to_County] = source.[Bill_to_County],
			target.[Bill_to_Post_Code] = source.[Bill_to_Post_Code],
			target.[Coupon_Promotion_Code] = source.[Coupon_Promotion_Code],
			target.[Customer_Posting_Group] = source.[Customer_Posting_Group],
			target.[EDI_Internal_DocNo_] = source.[EDI_Internal_DocNo_],
			target.[Location_Code] = source.[Location_Code],
			target.[No_] = source.[No_],
			target.[Order_Date] = source.[Order_Date],
			target.[Order_Time] = source.[Order_Time],
			target.[Posting_Date] = source.[Posting_Date],
			target.[Shipment_Date] = source.[Shipment_Date],
			target.[Shipment_Method_Code] = source.[Shipment_Method_Code],
			target.[Ship_to_County] = source.[Ship_to_County],
			target.[Ship_to_Post_Code] = source.[Ship_to_Post_Code],
			target.[Shortcut_Dimension_1_Code] = source.[Shortcut_Dimension_1_Code],
			target.[Modified_By] = 'ETL_User',
			target.[Modified_Date] =  getdate(),
			target.[Filename] = source.[Filename],
			target.[Bill_to_Country_Region_Code] = source.[Bill_to_Country_Region_Code],
			target.[Ship_to_Country_Region_Code] = source.[Ship_to_Country_Region_Code],
			target.[Planned_DC] = source.[Planned_DC]
WHEN NOT MATCHED BY TARGET AND source.RECORD_STATUS='UPSERT'
THEN INSERT (Amount_Authorized
			,Sell_to_Customer_No_
			,External_Document_No_
			,Document_Type
			,Document_Date
			,Return_Type
			,Sell_to_Customer_Name
			,Your_Reference
			,Bill_to_County
			,Bill_to_Post_Code
			,Coupon_Promotion_Code
			,Customer_Posting_Group
			,EDI_Internal_DocNo_
			,Location_Code
			,No_
			,Order_Date
			,Order_Time
			,Posting_Date
			,Shipment_Date
			,Shipment_Method_Code
			,Ship_to_County
			,Ship_to_Post_Code
			,Shortcut_Dimension_1_Code
			,Created_By
			,Created_Date
			,Filename
			,Bill_to_Country_Region_Code
			,Ship_to_Country_Region_Code
			,Planned_DC
)
              VALUES (source.[Amount_Authorized],
					source.[Sell_to_Customer_No_],
					source.[External_Document_No_],
					source.[Document_Type],
					source.[Document_Date],
					source.[Return_Type],
					source.[Sell_to_Customer_Name],
					source.[Your_Reference],
					source.[Bill_to_County],
					source.[Bill_to_Post_Code],
					source.[Coupon_Promotion_Code],
					source.[Customer_Posting_Group],
					source.[EDI_Internal_DocNo_],
					source.[Location_Code],
					source.[No_],
					source.[Order_Date],
					source.[Order_Time],
					source.[Posting_Date],
					source.[Shipment_Date],
					source.[Shipment_Method_Code],
					source.[Ship_to_County],
					source.[Ship_to_Post_Code],
					source.[Shortcut_Dimension_1_Code],
					source.[Created_By],
					source.[Created_Date],
					source.[Filename],
					source.[Bill_to_Country_Region_Code],
					source.[Ship_to_Country_Region_Code],
					source.[Planned_DC])
WHEN MATCHED AND source.RECORD_STATUS='DELETE'
	THEN DELETE ;					