﻿CREATE TABLE [dbo].[Oracle_ETL_Sales_Line] (
    [Document_No_]              NVARCHAR (255)   NULL,
    [Quantity_Base]             DECIMAL (38, 20) NULL,
    [No_]                       NVARCHAR (255)   NULL,
    [Location_Code]             NVARCHAR (255)   NULL,
    [Shipment_Date]             DATETIME         NULL,
    [Shortcut_Dimension_2_Code] NVARCHAR (255)   NULL,
    [Description]               NVARCHAR (255)   NULL,
    [Document_Type]             INT              NULL,
    [Return_Qty_Received]       DECIMAL (38, 20) NULL,
    [Qty_to_Invoice]            DECIMAL (38, 20) NULL,
    [Line_Amount]               DECIMAL (38, 20) NULL,
    [Amount_Including_VAT]      DECIMAL (38, 20) NULL,
    [Quantity]                  DECIMAL (38, 20) NULL,
    [Qty_per_Unit_of_Measure]   DECIMAL (38, 20) NULL,
    [Amount]                    DECIMAL (38, 20) NULL,
    [Unit_of_Measure_Code]      NVARCHAR (255)   NULL,
    [Created_By]                NVARCHAR (255)   NULL,
    [Created_Date]              DATETIME         NULL,
    [Modified_By]               NVARCHAR (255)   NULL,
    [Modified_Date]             DATETIME         NULL,
    [Filename]                  NVARCHAR (255)   NULL,
    [WMS_INTERFACE_FLAG]        NVARCHAR (255)   NULL,
    [RELEASED_STATUS]           NVARCHAR (255)   NULL,
    [INVENTORY_RESERVATION]     NVARCHAR (255)   NULL,
    [ID]                        BIGINT           IDENTITY (1, 1) NOT NULL,
    [Item_Category_Code]        NVARCHAR (255)   NULL,
    [Order_Item_No_]            NVARCHAR (255)   NULL,
    CONSTRAINT [PK_Oracle_ETL_Sales_Line] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Line_Location_Code_C0DC5]
    ON [dbo].[Oracle_ETL_Sales_Line]([Location_Code] ASC)
    INCLUDE([Document_No_], [Shortcut_Dimension_2_Code]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Line_No_]
    ON [dbo].[Oracle_ETL_Sales_Line]([No_] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Line_Document_No_]
    ON [dbo].[Oracle_ETL_Sales_Line]([Document_No_] ASC)
    INCLUDE([Location_Code], [Shortcut_Dimension_2_Code], [Document_Type], [Amount_Including_VAT]) WITH (FILLFACTOR = 100);

