﻿CREATE TABLE [dbo].[Oracle_ETL_Sales_Invoice_Header] (
    [No_]                         NVARCHAR (255)   NULL,
    [Sell_to_Customer_No_]        NVARCHAR (255)   NULL,
    [Ship_to_Name]                NVARCHAR (255)   NULL,
    [Posting_Date]                DATETIME         NULL,
    [Shortcut_Dimension_1_Code]   NVARCHAR (255)   NULL,
    [Sell_to_Customer_Name]       NVARCHAR (255)   NULL,
    [Sell_to_Address]             NVARCHAR (255)   NULL,
    [Ship_to_Country_Region_Code] NVARCHAR (255)   NULL,
    [External_Document_No_]       NVARCHAR (255)   NULL,
    [Coupon_Promotion_Code]       NVARCHAR (255)   NULL,
    [Customer_Posting_Group]      NVARCHAR (255)   NULL,
    [Sell_to_Post_Code]           NVARCHAR (255)   NULL,
    [Order_Date]                  DATETIME         NULL,
    [Ship_to_Post_Code]           NVARCHAR (255)   NULL,
    [Sell_to_Address_2]           NVARCHAR (255)   NULL,
    [Amount_Authorized]           DECIMAL (38, 20) NULL,
    [Shipment_Method_Code]        NVARCHAR (255)   NULL,
    [Location_Code]               NVARCHAR (255)   NULL,
    [Ship_to_County]              NVARCHAR (255)   NULL,
    [Order_No]                    NVARCHAR (255)   NULL,
    [Posting_Description]         NVARCHAR (255)   NULL,
    [Created_By]                  NVARCHAR (255)   NULL,
    [Created_Date]                DATETIME         NULL,
    [Modified_By]                 NVARCHAR (255)   NULL,
    [Filename]                    NVARCHAR (255)   NULL,
    [Modified_Date]               DATETIME         NULL,
    [Invoice_Completion_Flag]     VARCHAR (10)     NULL,
    [ID]                          BIGINT           IDENTITY (1, 1) NOT NULL,
    [Planned_DC]                  NVARCHAR (255)   NULL,
    CONSTRAINT [PK_Oracle_ETL_Sales_Invoice_Header] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Filename]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Filename] ASC)
    INCLUDE([No_], [Sell_to_Customer_No_], [Ship_to_Name], [Posting_Date], [Shortcut_Dimension_1_Code], [Sell_to_Customer_Name], [Sell_to_Address], [Ship_to_Country_Region_Code], [External_Document_No_], [Coupon_Promotion_Code], [Customer_Posting_Group], [Sell_to_Post_Code], [Order_Date], [Ship_to_Post_Code], [Sell_to_Address_2], [Amount_Authorized], [Shipment_Method_Code], [Location_Code], [Ship_to_County], [Order_No], [Posting_Description], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [Invoice_Completion_Flag], [ID]);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_No_]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([No_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Sell_to_Customer_No__Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Sell_to_Customer_No_] ASC, [Posting_Date] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Created_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Created_Date] ASC)
    INCLUDE([No_], [Location_Code]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_External_Document_No_]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([External_Document_No_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Posting_Date] ASC)
    INCLUDE([Shortcut_Dimension_1_Code], [Customer_Posting_Group], [Amount_Authorized], [Shipment_Method_Code], [Location_Code], [No_], [External_Document_No_]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Customer_Posting_Group]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Customer_Posting_Group] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Location_Code]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Location_Code] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Posting_Date_2]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Posting_Date] ASC)
    INCLUDE([No_], [Shortcut_Dimension_1_Code], [Customer_Posting_Group], [Ship_to_Post_Code], [Shipment_Method_Code], [Location_Code], [External_Document_No_]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Sell_to_Customer_No__Posting_Date2]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Sell_to_Customer_No_] ASC, [Posting_Date] ASC)
    INCLUDE([No_], [Coupon_Promotion_Code], [Customer_Posting_Group]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Order_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Order_Date] ASC)
    INCLUDE([No_], [External_Document_No_]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Customer_Posting_Group_Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Customer_Posting_Group] ASC, [Posting_Date] ASC)
    INCLUDE([Sell_to_Customer_No_], [Shortcut_Dimension_1_Code], [Sell_to_Customer_Name], [External_Document_No_], [Order_Date], [Amount_Authorized], [Shipment_Method_Code], [Location_Code]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Posting_Date1]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Posting_Date] ASC)
    INCLUDE([No_], [Sell_to_Customer_No_], [Order_Date]);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Customer_Posting_Group_Posting_Date_2]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Customer_Posting_Group] ASC, [Posting_Date] ASC)
    INCLUDE([Sell_to_Customer_No_], [Ship_to_Name], [Ship_to_Country_Region_Code], [Ship_to_Post_Code], [Ship_to_County]);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Header_Posting_Date_Order_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Header]([Posting_Date] ASC, [Order_Date] ASC)
    INCLUDE([External_Document_No_], [Location_Code]);

