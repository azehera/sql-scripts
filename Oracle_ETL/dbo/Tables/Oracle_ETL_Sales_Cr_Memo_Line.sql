﻿CREATE TABLE [dbo].[Oracle_ETL_Sales_Cr_Memo_Line] (
    [Document_No_]            NVARCHAR (255)   NULL,
    [Type]                    NVARCHAR (255)   NULL,
    [No_]                     NVARCHAR (255)   NULL,
    [Location_Code]           NVARCHAR (255)   NULL,
    [Item_Category_Code]      NVARCHAR (255)   NULL,
    [Line_Discount_Amount]    DECIMAL (38, 20) NULL,
    [Inv_Discount_Amount]     DECIMAL (38, 20) NULL,
    [Quantity]                DECIMAL (38, 20) NULL,
    [Qty_per_Unit_of_Measure] DECIMAL (38, 20) NULL,
    [Quantity_Base]           DECIMAL (38, 20) NULL,
    [Amount]                  DECIMAL (38, 20) NULL,
    [Amount_Including_VAT]    DECIMAL (38, 20) NULL,
    [Line_Amount]             DECIMAL (38, 20) NULL,
    [Tax_Group_Code]          NVARCHAR (255)   NULL,
    [Unit_Cost_LCY]           DECIMAL (38, 20) NULL,
    [Description]             NVARCHAR (255)   NULL,
    [Unit_of_Measure]         NVARCHAR (255)   NULL,
    [Unit_Price]              DECIMAL (38, 20) NULL,
    [Order_Item_No_]          NVARCHAR (255)   NULL,
    [Posting_Date]            DATETIME         NULL,
    [Created_By]              NVARCHAR (255)   NULL,
    [Created_Date]            DATETIME         NULL,
    [Modified_By]             NVARCHAR (255)   NULL,
    [Modified_Date]           DATETIME         NULL,
    [Filename]                NVARCHAR (255)   NULL,
    [ID]                      BIGINT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Oracle_ETL_Sales_Cr_Memo_Line] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Line_No_]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Line]([No_] ASC)
    INCLUDE([Document_No_]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Line_Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Line]([Posting_Date] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Line_Document_No_]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Line]([Document_No_] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Line_Created_Date]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Line]([Created_Date] ASC)
    INCLUDE([Document_No_], [Type], [No_], [Location_Code], [Item_Category_Code], [Line_Discount_Amount], [Inv_Discount_Amount], [Quantity], [Qty_per_Unit_of_Measure], [Quantity_Base], [Amount], [Amount_Including_VAT], [Line_Amount], [Tax_Group_Code], [Unit_Cost_LCY], [Description], [Unit_of_Measure], [Unit_Price], [Order_Item_No_], [Posting_Date], [Created_By], [Modified_By], [Modified_Date], [Filename], [ID]);

