﻿CREATE TABLE [dbo].[Oracle_ETL_Sales_Cr_Memo_Header] (
    [Customer_Posting_Group]      NVARCHAR (255)   NULL,
    [No_]                         NVARCHAR (255)   NULL,
    [Posting_Date]                DATETIME         NULL,
    [Sell_to_Customer_Name]       NVARCHAR (255)   NULL,
    [Sell_to_Address]             NVARCHAR (255)   NULL,
    [Sell_to_Customer_No_]        NVARCHAR (255)   NULL,
    [Sell_to_Post_Code]           NVARCHAR (255)   NULL,
    [Shortcut_Dimension_1_Code]   NVARCHAR (255)   NULL,
    [Ship_to_Country_Region_Code] NVARCHAR (255)   NULL,
    [Ship_to_Name]                NVARCHAR (255)   NULL,
    [External_Document_No_]       NVARCHAR (255)   NULL,
    [Sell_to_Address_Two]         NVARCHAR (255)   NULL,
    [Ship_to_Post_Code]           NVARCHAR (255)   NULL,
    [Amount_Authorized]           DECIMAL (38, 20) NULL,
    [Posting_Description]         NVARCHAR (255)   NULL,
    [Bill_to_Customer_No_]        NVARCHAR (255)   NULL,
    [Escalate_Original_Order_No_] NVARCHAR (255)   NULL,
    [Document_Date]               DATETIME         NULL,
    [Created_By]                  NVARCHAR (255)   NULL,
    [Created_Date]                DATETIME         NULL,
    [Modified_By]                 NVARCHAR (255)   NULL,
    [Modified_Date]               DATETIME         NULL,
    [Filename]                    NVARCHAR (255)   NULL,
    [Invoice_Completion_Flag]     VARCHAR (10)     NULL,
    [ID]                          BIGINT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Oracle_ETL_Sales_Cr_Memo_Header] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Header_Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Header]([Posting_Date] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Header_Customer_Posting_Group_Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Header]([Customer_Posting_Group] ASC, [Posting_Date] ASC)
    INCLUDE([Shortcut_Dimension_1_Code], [External_Document_No_]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Header_No_]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Header]([No_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Header_Sell_to_Customer_No_]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Header]([Sell_to_Customer_No_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Header_External_Document_No__Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Header]([External_Document_No_] ASC, [Posting_Date] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Cr_Memo_Header_Posting_Date1]
    ON [dbo].[Oracle_ETL_Sales_Cr_Memo_Header]([Posting_Date] ASC)
    INCLUDE([Customer_Posting_Group], [Shortcut_Dimension_1_Code], [Posting_Description]);

