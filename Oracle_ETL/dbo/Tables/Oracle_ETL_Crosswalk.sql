﻿CREATE TABLE [dbo].[Oracle_ETL_Crosswalk] (
    [CrosswalkID]     INT            IDENTITY (1, 1) NOT NULL,
    [Reference_Data]  NVARCHAR (255) NULL,
    [Canonical_Value] NVARCHAR (255) NULL,
    [Source]          NVARCHAR (255) NULL,
    [Source_Value]    NVARCHAR (255) NULL,
    [Context]         NVARCHAR (255) NULL,
    [Modified_Date]   DATETIME       DEFAULT (getdate()) NULL,
    [Modified_By]     NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([CrosswalkID] ASC) WITH (FILLFACTOR = 90)
);

