﻿CREATE TABLE [dbo].[SHIPMENT_REPORT_ORACLE] (
    [INSERT_DATE]                   DATETIME       NULL,
    [UPDATE_DATE]                   DATETIME       NULL,
    [JOB_EXECUTION_ID]              NUMERIC (22)   NULL,
    [TRANSFER_ORDER_ID]             NUMERIC (22)   NULL,
    [TRANSFER_ORDER_NUMBER]         VARCHAR (100)  NOT NULL,
    [DELIVERY_DETAIL_ID]            NUMERIC (22)   NOT NULL,
    [SOURCE_ORGANIZATION_CODE]      VARCHAR (72)   NOT NULL,
    [DESTINATION_ORGANIZATION_CODE] VARCHAR (72)   NULL,
    [SOURCE_LOCATION_CODE]          VARCHAR (50)   NOT NULL,
    [DESTINATION_LOCATION_CODE]     VARCHAR (600)  NULL,
    [TO_CREATION_DATE]              DATETIME       NULL,
    [ORDERED_DATE]                  DATE           NULL,
    [ORDERED_TIME]                  VARCHAR (200)  NULL,
    [ORDERED_DATETIME]              DATETIME       NULL,
    [LINE_NUMBER]                   NUMERIC (22)   NULL,
    [TRANSFER_ORDER_HEADER_STATUS]  VARCHAR (320)  NULL,
    [TRANSFER_ORDER_LINE_STATUS]    VARCHAR (320)  NULL,
    [TO_FULFILLMENT_STATUS]         VARCHAR (320)  NULL,
    [SCHEDULED_SHIP_DATE]           DATETIME       NULL,
    [SHIPMENT_LINE_STATUS]          VARCHAR (320)  NULL,
    [ITEM_NUMBER]                   VARCHAR (1200) NULL,
    [TO_LINE_REQUESTED_QTY]         NUMERIC (22)   NULL,
    [WDD_REQUESTED_QUANTITY]        NUMERIC (22)   NULL,
    [WDD_SHIPPED_QUANTITY]          NUMERIC (22)   NULL,
    [INTEGRATION_STATUS]            VARCHAR (320)  NULL,
    [SHIPMENT_NUM]                  VARCHAR (120)  NULL,
    [WDD_LAST_UPDATE_DATE]          DATETIME       NULL,
    [SHIPMENT_DATE]                 DATETIME       NULL,
    [INVENTORY_RESERVATION]         VARCHAR (100)  NULL,
    [WSH_INTERFACED_TO_ORACLE]      VARCHAR (100)  NULL,
    [WSH_INTERFACE_ERROR_FLAG]      VARCHAR (100)  NULL,
    [WSH_ERROR_REASON]              VARCHAR (4000) NULL,
    [WDD_BATCH_ID]                  NUMERIC (22)   NULL,
    [SHIPMENT_REQUEST_DATE]         DATETIME       NULL,
    [RECON_1_ORACLE_EXCEPTION]      VARCHAR (100)  NULL,
    [RECON_3_ORACLE_EXCEPTION]      VARCHAR (100)  NULL,
    [FILE_NAME]                     VARCHAR (500)  NULL,
    [WDD_ATTRIBUTE4]                VARCHAR (100)  NULL,
    CONSTRAINT [PK_SHIPMENT_REPORT_ORACLE] PRIMARY KEY CLUSTERED ([TRANSFER_ORDER_NUMBER] ASC, [DELIVERY_DETAIL_ID] ASC, [SOURCE_LOCATION_CODE] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
);


GO
CREATE NONCLUSTERED INDEX [IX_SHIPMENT_REPORT_ORACLE_DELIVERY_DETAIL_ID]
    ON [dbo].[SHIPMENT_REPORT_ORACLE]([DELIVERY_DETAIL_ID] ASC)
    INCLUDE([INSERT_DATE], [UPDATE_DATE], [TRANSFER_ORDER_NUMBER], [SOURCE_LOCATION_CODE], [DESTINATION_LOCATION_CODE], [TO_CREATION_DATE], [TRANSFER_ORDER_HEADER_STATUS], [WDD_ATTRIBUTE4]) WITH (FILLFACTOR = 90);

