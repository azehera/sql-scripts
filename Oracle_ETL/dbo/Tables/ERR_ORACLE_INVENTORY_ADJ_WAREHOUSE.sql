﻿CREATE TABLE [dbo].[ERR_ORACLE_INVENTORY_ADJ_WAREHOUSE] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [JOB_EXECUTION_ID]      NUMERIC (22)    NULL,
    [INSERT_DATE]           DATETIME        NULL,
    [WAREHOUSE]             VARCHAR (22)    NULL,
    [TRANSACTION_DATE]      DATETIME        NULL,
    [TRANSACTION_TYPE]      VARCHAR (250)   NULL,
    [SUBINVENTORY_FROM]     VARCHAR (25)    NULL,
    [SUBINVENTORYTO]        VARCHAR (30)    NULL,
    [ITEMNO]                VARCHAR (30)    NULL,
    [LOTNO]                 VARCHAR (30)    NULL,
    [EXPIRY_DATE]           VARCHAR (30)    NULL,
    [QUANTITY]              NUMERIC (22)    NULL,
    [UOM]                   VARCHAR (20)    NULL,
    [REASON_CODE]           VARCHAR (50)    NULL,
    [TRANSACTION_REFERENCE] NUMERIC (22)    NULL,
    [ERROR_MESSAGE]         VARCHAR (200)   NULL,
    [File_NAME]             VARCHAR (100)   NULL,
    [UNIT_COST]             DECIMAL (15, 5) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

