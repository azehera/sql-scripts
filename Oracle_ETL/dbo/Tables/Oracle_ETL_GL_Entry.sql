﻿CREATE TABLE [dbo].[Oracle_ETL_GL_Entry] (
    [Amount]        DECIMAL (38, 20) NULL,
    [Posting_Date]  DATETIME         NULL,
    [Acct]          NCHAR (255)      NULL,
    [Dept]          NCHAR (255)      NULL,
    [Created_By]    NVARCHAR (255)   NULL,
    [Created_Date]  DATETIME         NULL,
    [Modified_By]   NVARCHAR (255)   NULL,
    [Modified_Date] DATETIME         NULL,
    [Filename]      NVARCHAR (255)   NULL,
    [Entry_No]      NVARCHAR (100)   NULL,
    [Currency_Code] NVARCHAR (100)   NULL,
    [ID]            BIGINT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Oracle_ETL_GL_Entry] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_GL_Entry_Entry_No]
    ON [dbo].[Oracle_ETL_GL_Entry]([Entry_No] ASC);

