﻿CREATE TABLE [dbo].[Oracle_ETL_Sales_Invoice_Line] (
    [Document_No_]              NVARCHAR (255)   NULL,
    [Type]                      NVARCHAR (255)   NULL,
    [No_]                       NVARCHAR (255)   NULL,
    [Location_Code]             NVARCHAR (255)   NULL,
    [Quantity]                  DECIMAL (38, 20) NULL,
    [Line_Discount_Amount]      DECIMAL (38, 20) NULL,
    [Qty_per_Unit_of_Measure]   DECIMAL (38, 20) NULL,
    [Quantity_Base]             DECIMAL (38, 20) NULL,
    [Amount]                    DECIMAL (38, 20) NULL,
    [Line_Amount]               DECIMAL (38, 20) NULL,
    [Amount_Including_VAT]      DECIMAL (38, 20) NULL,
    [Tax_Group_Code]            NVARCHAR (255)   NULL,
    [Unit_Cost_LCY]             DECIMAL (38, 20) NULL,
    [Item_Category_Code]        NVARCHAR (255)   NULL,
    [Description]               NVARCHAR (255)   NULL,
    [Unit_of_Measure]           NVARCHAR (255)   NULL,
    [Unit_Price]                DECIMAL (38, 20) NULL,
    [Order_Item_No_]            NVARCHAR (255)   NULL,
    [Inv_Discount_Amount]       DECIMAL (38, 20) NULL,
    [Shortcut_Dimension_2_Code] NVARCHAR (255)   NULL,
    [Posting_Date]              DATETIME         NULL,
    [Shipment_Date]             DATETIME         NULL,
    [Sell_to_Customer_No_]      NVARCHAR (255)   NULL,
    [Created_By]                NVARCHAR (255)   NULL,
    [Created_Date]              DATETIME         NULL,
    [Modified_By]               NVARCHAR (255)   NULL,
    [Modified_Date]             DATETIME         NULL,
    [Filename]                  NVARCHAR (255)   NULL,
    [ID]                        BIGINT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Oracle_ETL_Sales_Invoice_Line] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Line_Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Line]([Posting_Date] ASC)
    INCLUDE([Document_No_], [Type], [No_], [Location_Code], [Quantity], [Line_Discount_Amount], [Qty_per_Unit_of_Measure], [Quantity_Base], [Amount], [Line_Amount], [Amount_Including_VAT], [Tax_Group_Code], [Unit_Cost_LCY], [Item_Category_Code], [Description], [Unit_of_Measure], [Unit_Price], [Order_Item_No_], [Inv_Discount_Amount], [Shortcut_Dimension_2_Code], [Shipment_Date], [Sell_to_Customer_No_]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Line_No_]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Line]([No_] ASC)
    INCLUDE([Document_No_]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Line_Document_No_]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Line]([Document_No_] ASC)
    INCLUDE([Amount_Including_VAT]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Line_Sell_to_Customer_No__Created_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Line]([Sell_to_Customer_No_] ASC, [Created_Date] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Line_No__Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Line]([No_] ASC, [Posting_Date] ASC)
    INCLUDE([Document_No_], [Type], [Location_Code], [Amount], [Line_Amount], [Tax_Group_Code], [Item_Category_Code], [Shortcut_Dimension_2_Code]);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Line_Quantity_Created_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Line]([Quantity] ASC, [Created_Date] ASC)
    INCLUDE([Document_No_], [Type], [No_], [Location_Code], [Line_Discount_Amount], [Qty_per_Unit_of_Measure], [Quantity_Base], [Amount], [Line_Amount], [Amount_Including_VAT], [Tax_Group_Code], [Unit_Cost_LCY], [Item_Category_Code], [Description], [Unit_of_Measure], [Unit_Price], [Order_Item_No_], [Inv_Discount_Amount], [Shortcut_Dimension_2_Code], [Posting_Date], [Shipment_Date], [Sell_to_Customer_No_], [Created_By], [Modified_By], [Modified_Date], [Filename]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Invoice_Line_Location_Code_Posting_Date]
    ON [dbo].[Oracle_ETL_Sales_Invoice_Line]([Location_Code] ASC, [Posting_Date] ASC)
    INCLUDE([Document_No_], [Shortcut_Dimension_2_Code]);

