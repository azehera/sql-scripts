﻿CREATE TABLE [dbo].[Oracle_ETL_Sales_Header] (
    [Amount_Authorized]           DECIMAL (38, 20) NULL,
    [Sell_to_Customer_No_]        NVARCHAR (255)   NULL,
    [External_Document_No_]       NVARCHAR (255)   NULL,
    [Document_Type]               INT              NULL,
    [Document_Date]               DATETIME         NULL,
    [Return_Type]                 INT              NULL,
    [Sell_to_Customer_Name]       NVARCHAR (255)   NULL,
    [Your_Reference]              NVARCHAR (255)   NULL,
    [Bill_to_County]              NVARCHAR (255)   NULL,
    [Bill_to_Post_Code]           NVARCHAR (255)   NULL,
    [Coupon_Promotion_Code]       NVARCHAR (255)   NULL,
    [Customer_Posting_Group]      NVARCHAR (255)   NULL,
    [EDI_Internal_DocNo_]         NVARCHAR (255)   NULL,
    [Location_Code]               NVARCHAR (255)   NULL,
    [No_]                         NVARCHAR (255)   NULL,
    [Order_Date]                  NVARCHAR (255)   NULL,
    [Order_Time]                  NVARCHAR (255)   NULL,
    [Posting_Date]                DATETIME         NULL,
    [Shipment_Date]               DATETIME         NULL,
    [Shipment_Method_Code]        NVARCHAR (255)   NULL,
    [Ship_to_County]              NVARCHAR (255)   NULL,
    [Ship_to_Post_Code]           NVARCHAR (255)   NULL,
    [Shortcut_Dimension_1_Code]   NVARCHAR (255)   NULL,
    [Created_By]                  NVARCHAR (255)   NULL,
    [Created_Date]                DATETIME         NULL,
    [Modified_By]                 NVARCHAR (255)   NULL,
    [Modified_Date]               DATETIME         NULL,
    [Filename]                    NVARCHAR (255)   NULL,
    [Bill_to_Country_Region_Code] NVARCHAR (255)   NULL,
    [Ship_to_Country_Region_Code] NVARCHAR (255)   NULL,
    [ID]                          BIGINT           IDENTITY (1, 1) NOT NULL,
    [Planned_DC]                  NVARCHAR (255)   NULL,
    CONSTRAINT [PK_Oracle_ETL_Sales_Header] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_idx1]
    ON [dbo].[Oracle_ETL_Sales_Header]([No_] ASC)
    INCLUDE([Document_Type], [Shipment_Date]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_External_Document_No_]
    ON [dbo].[Oracle_ETL_Sales_Header]([External_Document_No_] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_Customer_Posting_Group]
    ON [dbo].[Oracle_ETL_Sales_Header]([Customer_Posting_Group] ASC)
    INCLUDE([Coupon_Promotion_Code], [No_]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_Amount_Authorized_Coupon_Promotion_Code]
    ON [dbo].[Oracle_ETL_Sales_Header]([Amount_Authorized] ASC, [Coupon_Promotion_Code] ASC)
    INCLUDE([Document_Type], [Customer_Posting_Group], [Location_Code], [No_], [Order_Date], [Shipment_Method_Code], [Shortcut_Dimension_1_Code]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_Amount_Authorized]
    ON [dbo].[Oracle_ETL_Sales_Header]([Amount_Authorized] ASC)
    INCLUDE([External_Document_No_], [Document_Type], [Customer_Posting_Group], [Location_Code], [Order_Date], [Shipment_Method_Code], [Shortcut_Dimension_1_Code]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_Posting_Date_idx1]
    ON [dbo].[Oracle_ETL_Sales_Header]([Posting_Date] ASC)
    INCLUDE([Amount_Authorized]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_Sell_to_Customer_No_]
    ON [dbo].[Oracle_ETL_Sales_Header]([Sell_to_Customer_No_] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_Customer_Posting_Group_Amount_Authorized]
    ON [dbo].[Oracle_ETL_Sales_Header]([Customer_Posting_Group] ASC, [Amount_Authorized] ASC)
    INCLUDE([External_Document_No_], [Document_Type], [Location_Code], [Order_Date], [Shipment_Method_Code], [Shortcut_Dimension_1_Code]) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_Order_Date]
    ON [dbo].[Oracle_ETL_Sales_Header]([Order_Date] ASC)
    INCLUDE([Amount_Authorized], [External_Document_No_], [Customer_Posting_Group]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Oracle_ETL_Sales_Header_Document_Type_Amount_Authorized]
    ON [dbo].[Oracle_ETL_Sales_Header]([Document_Type] ASC, [Amount_Authorized] ASC)
    INCLUDE([External_Document_No_], [Customer_Posting_Group], [Location_Code], [Order_Date], [Shipment_Method_Code], [Shortcut_Dimension_1_Code]) WITH (FILLFACTOR = 100);

