﻿CREATE TABLE [dbo].[Oracle_ETL_ErrorRecords] (
    [RecordDetail] NVARCHAR (MAX) NULL,
    [Created_By]   NVARCHAR (255) NULL,
    [Created_Date] DATETIME       NULL,
    [FileName]     NVARCHAR (500) NULL,
    [Object_Name]  NVARCHAR (255) NULL
);

